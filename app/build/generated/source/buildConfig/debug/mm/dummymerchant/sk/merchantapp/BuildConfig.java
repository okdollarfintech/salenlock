/**
 * Automatically generated file. DO NOT MODIFY
 */
package mm.dummymerchant.sk.merchantapp;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "mm.dummymerchant.sk.merchantapp";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 5;
  public static final String VERSION_NAME = "1.2.2";
}
