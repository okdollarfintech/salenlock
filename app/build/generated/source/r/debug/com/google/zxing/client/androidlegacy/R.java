/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package com.google.zxing.client.androidlegacy;

public final class R {
    public static final class array {
        public static final int zxinglegacy_country_codes = 0x7f020019;
        public static final int zxinglegacy_preferences_front_light_values = 0x7f02001a;
    }
    public static final class color {
        public static final int zxinglegacy_contents_text = 0x7f0501a1;
        public static final int zxinglegacy_encode_view = 0x7f0501a2;
        public static final int zxinglegacy_possible_result_points = 0x7f0501a3;
        public static final int zxinglegacy_result_minor_text = 0x7f0501a4;
        public static final int zxinglegacy_result_points = 0x7f0501a5;
        public static final int zxinglegacy_result_text = 0x7f0501a6;
        public static final int zxinglegacy_result_view = 0x7f0501a7;
        public static final int zxinglegacy_status_text = 0x7f0501a8;
        public static final int zxinglegacy_transparent = 0x7f0501a9;
        public static final int zxinglegacy_viewfinder_laser = 0x7f0501aa;
        public static final int zxinglegacy_viewfinder_mask = 0x7f0501ab;
    }
    public static final class dimen {
        public static final int zxinglegacy_half_padding = 0x7f06009a;
        public static final int zxinglegacy_standard_padding = 0x7f06009b;
    }
    public static final class id {
        public static final int format_text_view = 0x7f080196;
        public static final int menu_encode = 0x7f080265;
        public static final int menu_help = 0x7f080266;
        public static final int menu_share = 0x7f080267;
        public static final int meta_text_view_label = 0x7f08026c;
        public static final int zxinglegacy_back_button = 0x7f080493;
        public static final int zxinglegacy_barcode_image_view = 0x7f080494;
        public static final int zxinglegacy_contents_supplement_text_view = 0x7f080495;
        public static final int zxinglegacy_contents_text_view = 0x7f080496;
        public static final int zxinglegacy_decode = 0x7f080497;
        public static final int zxinglegacy_decode_failed = 0x7f080498;
        public static final int zxinglegacy_decode_succeeded = 0x7f080499;
        public static final int zxinglegacy_done_button = 0x7f08049a;
        public static final int zxinglegacy_help_contents = 0x7f08049b;
        public static final int zxinglegacy_image_view = 0x7f08049c;
        public static final int zxinglegacy_launch_product_query = 0x7f08049d;
        public static final int zxinglegacy_meta_text_view = 0x7f08049e;
        public static final int zxinglegacy_preview_view = 0x7f08049f;
        public static final int zxinglegacy_quit = 0x7f0804a0;
        public static final int zxinglegacy_restart_preview = 0x7f0804a1;
        public static final int zxinglegacy_result_button_view = 0x7f0804a2;
        public static final int zxinglegacy_result_view = 0x7f0804a3;
        public static final int zxinglegacy_return_scan_result = 0x7f0804a4;
        public static final int zxinglegacy_status_view = 0x7f0804a5;
        public static final int zxinglegacy_time_text_view = 0x7f0804a6;
        public static final int zxinglegacy_type_text_view = 0x7f0804a7;
        public static final int zxinglegacy_viewfinder_view = 0x7f0804a8;
    }
    public static final class layout {
        public static final int zxinglegacy_capture = 0x7f0a00e0;
        public static final int zxinglegacy_encode = 0x7f0a00e1;
        public static final int zxinglegacy_help = 0x7f0a00e2;
    }
    public static final class menu {
        public static final int zxinglegacy_capture = 0x7f0b0003;
        public static final int zxinglegacy_encode = 0x7f0b0004;
    }
    public static final class raw {
        public static final int zxinglegacy_beep = 0x7f0d0021;
    }
    public static final class string {
        public static final int zxinglegacy_app_name = 0x7f0e033e;
        public static final int zxinglegacy_button_back = 0x7f0e033f;
        public static final int zxinglegacy_button_cancel = 0x7f0e0340;
        public static final int zxinglegacy_button_done = 0x7f0e0341;
        public static final int zxinglegacy_button_ok = 0x7f0e0342;
        public static final int zxinglegacy_contents_contact = 0x7f0e0343;
        public static final int zxinglegacy_contents_email = 0x7f0e0344;
        public static final int zxinglegacy_contents_location = 0x7f0e0345;
        public static final int zxinglegacy_contents_phone = 0x7f0e0346;
        public static final int zxinglegacy_contents_sms = 0x7f0e0347;
        public static final int zxinglegacy_contents_text = 0x7f0e0348;
        public static final int zxinglegacy_menu_encode_mecard = 0x7f0e0349;
        public static final int zxinglegacy_menu_encode_vcard = 0x7f0e034a;
        public static final int zxinglegacy_menu_help = 0x7f0e034b;
        public static final int zxinglegacy_menu_share = 0x7f0e034c;
        public static final int zxinglegacy_msg_camera_framework_bug = 0x7f0e034d;
        public static final int zxinglegacy_msg_default_format = 0x7f0e034e;
        public static final int zxinglegacy_msg_default_meta = 0x7f0e034f;
        public static final int zxinglegacy_msg_default_status = 0x7f0e0350;
        public static final int zxinglegacy_msg_default_time = 0x7f0e0351;
        public static final int zxinglegacy_msg_default_type = 0x7f0e0352;
        public static final int zxinglegacy_msg_encode_contents_failed = 0x7f0e0353;
        public static final int zxinglegacy_msg_unmount_usb = 0x7f0e0354;
    }
    public static final class xml {
        public static final int zxinglegacy_preferences = 0x7f110001;
    }
}
