Encrypt Cordova Applications
=============================

1) First we need to include the Dexguard library in your Cordova project. This
   is done by copying the dexguard-runtime.jar library from
   /lib/dexguard-runtime.jar to CordovaProject/libs/.

2) Now we will create a class responsible for encrypting the assets files. Make
   a new class and call it MyAssetInputStreamFactory. The contents of that class
   should like this:

   public class MyAssetInputStreamFactory
   implements com.guardsquare.dexguard.runtime.encryption.AssetInputStreamFactory
   {
        /**
        * Specifies the asset files that have to be encrypted.
        */
        @Override
        public InputStream createInputStream(AssetManager assetManager,
                                                String assetFileName)
        throws IOException
        {

        return
            assetFileName.equals("www/index.html")    ? assetManager.open("www/index.html") :
            assetFileName.equals("www/css/index.css") ? assetManager.open("www/css/index.css") :
            assetFileName.equals("www/img/logo.png")  ? assetManager.open("www/img/logo.png") :
            assetFileName.equals("www/js/index.js")   ? assetManager.open("www/js/index.js") :
                                                        assetManager.open(assetFileName);
        }
    }

    Note that it is important that you include every file that you want to see
    encrypted in the release APK. In this example only four files are being
    encrypted.

3)  Now we need to transform the MainActivity of the Cordova application. The
    following code shows how to transform the MainActivity to use Dexguard's
    EncryptedSystemWebViewClient:

    /**
     * Sample activity that shows a Cordova web view.
     */
    public class HelloWorldActivity extends CordovaActivity {

        // An arbitrary http URL prefix to refer to local assets
        // (encrypted or not).
        private static final String ENCRYPTED_ASSET_PREFIX = "file://_/";


        @Override
        public void onCreate(Bundle savedInstanceState) {
            // Start Cordova with the main page.
            super.onCreate(savedInstanceState);
            super.init();
            super.loadUrl(ENCRYPTED_ASSET_PREFIX + "www/index.html");
        }

        @Override
        protected CordovaWebViewEngine makeWebViewEngine() {
            CordovaWebViewEngine engine = super.makeWebViewEngine();

            SystemWebView webView = (SystemWebView) engine.getView();

            // Configure an encrypted SystemWebViewClient on the webView.
            EncryptedSystemWebViewClient webViewClient =
                new EncryptedSystemWebViewClient((SystemWebViewEngine) engine,
                                                 getAssets(),
                                                 ENCRYPTED_ASSET_PREFIX,
                                                 new MyAssetInputStreamFactory());
            webView.setWebViewClient(webViewClient);

            return engine;
        }
    }

    Note that we use the previously constructed MyAssetInputStreamFactory class.

4)  Finally, make sure that your dexguard-project.txt includes the option to
    encrypt all the assets files:

    -encryptassetfiles assets/**
