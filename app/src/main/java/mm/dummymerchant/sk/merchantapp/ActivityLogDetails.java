package mm.dummymerchant.sk.merchantapp;

import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.Constant;
import mm.dummymerchant.sk.merchantapp.adapter.ActivityLog_Adapter;
import mm.dummymerchant.sk.merchantapp.db.DBHelper;
import mm.dummymerchant.sk.merchantapp.model.CashierActivityLogModel;

/**
 * Created by user on 11/7/2015.
 */
public class ActivityLogDetails extends BaseActivity  implements Constant{
    DBHelper db;
    ActivityLog_Adapter adapter;
    private ListView lv;
    String cashierId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_details);
        lv = (ListView)findViewById(R.id.listView1);

        db = new DBHelper(getApplicationContext());
       // AsyncTaskLoading loading = new AsyncTaskLoading();
       // loading.execute();
        setActivitylogActionBar(String.valueOf(getResources().getText(R.string.activitylogdetails)));

        String a = AppPreference.getMasterOrCashier(this);
        ArrayList<CashierActivityLogModel> AlertList ;

        if(a.equals(MASTER))
        {
            System.out.println("Enter the If Condition with Master Login ");
            db.open();
            AlertList = db.Cashier_Activity("Master");
            db.close();
        }
        else
        {
            System.out.println("Enter the else Condition with Cashier Login ");
            db.open();
            cashierId=SliderScreen.present_Cashier_Id;
            AlertList = db.Cashier_Activity(cashierId);
            db.close();
        }

        if (AlertList != null)
        {         
            adapter =new ActivityLog_Adapter(this, AlertList);
            lv.setAdapter(adapter);
        }
        else
        {
            showToast(getString(R.string.no_record));
        }

    }

    @Override
    public <T> void response(int resultCode, T data) {

    }

    /*private class AsyncTaskLoading extends AsyncTask<Void, Void, Void> {
        /// private GameResultsAdapter adapter;

        private final ProgressDialog dialogue =new ProgressDialog(getApplicationContext());

        public AsyncTaskLoading() {
        }

        @Override
        protected void onPreExecute()
        {
            dialogue.setMessage("Loading...please wait.");
            dialogue.setIndeterminate(true);
            dialogue.setCancelable(false);
            dialogue.setCanceledOnTouchOutside(false);
            dialogue.show();
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            try {
                db.open();
                 AlertList= db.Cashier_Activity();
                *//*if (c.getCount() > 0) {
                    Log.i("ServingCell_Page", "Enter IF Contion");

                    if (c.moveToFirst()) {
                        while (c.isAfterLast() == false) {
                            cellvalue = new CashierActivityLogModel();
                            Log.i("ServingCell_Page", "READIN-Get_SevRowItem_servingcellingcell");


                            cellvalue.setcashier_Name(c.getString(2));
                            cellvalue.setcashier_Number(c.getString(3));
                            cellvalue.setlogin_Time(c.getString(4));
                            cellvalue.setlogout_Time(c.getString(5));
                            cellvalue.setlogout_Reason((c.getString(6)));

                            AlertList.add(cellvalue);

                            c.moveToNext();
                        }
                    }

                } else {
                    Toast.makeText(getApplicationContext(), "No Values", Toast.LENGTH_SHORT).show();
                }*//*
                //db.close();
                if(AlertList!=null)
                {
                    if(AlertList.size()==0)
                        Toast.makeText(getApplicationContext(), "No Values", Toast.LENGTH_SHORT).show();
                }
            }catch(Exception e){

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void res)
        {
            adapter =new ActivityLog_Adapter(getApplicationContext(), AlertList);
            lv.setAdapter(adapter);

            if (this.dialogue.isShowing())
            {
                this.dialogue.dismiss();
            }

        }
    }*/


}
