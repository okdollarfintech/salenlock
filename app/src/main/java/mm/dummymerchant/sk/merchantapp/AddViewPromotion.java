package mm.dummymerchant.sk.merchantapp;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import org.json.JSONArray;
import org.json.JSONObject;

import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.Utils;
import mm.dummymerchant.sk.merchantapp.customView.CustomButton;
import mm.dummymerchant.sk.merchantapp.customView.CustomEdittext;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
import mm.dummymerchant.sk.merchantapp.customView.LinedEditText;
import mm.dummymerchant.sk.merchantapp.db.DBHelper;
import mm.dummymerchant.sk.merchantapp.httpConnection.CallingApIS;
import mm.dummymerchant.sk.merchantapp.model.PromotionModel;

public class AddViewPromotion extends BaseActivity implements View.OnClickListener {

    private CustomEdittext mTitle, mMobileNumber, mWebsiteUrl, mFacebookUrl, mEmail, mName;
    private LinedEditText mDescription;
    private CustomTextView mLastUpdated, mLocateUs;
    private DBHelper db;
    private PromotionModel model;
    private CustomButton mDelete;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_view_promotion);
        hideKeyboard();
        mcustomHeaderBg.setBackgroundResource(R.drawable.header_bg2);
        //setMapSearchPromotionBackground();
       // mTitleOfHeader.setText(getResources().getString(R.string.Promotion));
        setPromotionActionBar();
        db = new DBHelper(this);
        model = db.getPromotionData();
        setInit();
        setData(model);

        if (model.getId().equals("")) {
            mMobileNumber.setText(Utils.getViberNumber(this));
            mEmail.setText(Utils.getDefaultEmail(this));
            //mFacebookUrl.setText(Utils.getDefaulFacebook(this));
        }

        finsihActivity();
    }

    private void finsihActivity() {
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void setData(PromotionModel model) {

        if (model != null) {
            buttonLogout.setText(getString(R.string.edit));
            buttonLogout.setVisibility(View.VISIBLE);
            mDelete.setVisibility(View.VISIBLE);
            mLastUpdated.setVisibility(View.VISIBLE);
            mTitle.setText(model.getTitle());
            mDescription.setText(model.getDescripton());
            mLastUpdated.setText(String.format("%s %s", getString(R.string.last_updated), model.getLast_updated()));
            mName.setText(model.getName());
            mTitle.setEnabled(false);
            mDescription.setEnabled(false);
            mName.setEnabled(false);
            mFacebookUrl.setEnabled(false);
            mWebsiteUrl.setEnabled(false);
            mMobileNumber.setEnabled(false);
            mEmail.setEnabled(false);

            if (!model.getFacebook().equals("") && !model.getFacebook().equals("http://www.facebook.com/")) {
                String text = "<a href='" + model.getFacebook() + "'> " + model.getFacebook() + "</a>";
                mFacebookUrl.setText(Html.fromHtml(text));
            }
            if (!model.getWebsite().equals("") && !model.getWebsite().equals("http://www.")) {
                String text = "<a href='" + model.getWebsite() + "'> " + model.getWebsite() + "</a>";
                mWebsiteUrl.setText(Html.fromHtml(text));
            }
            if (!model.getMobileNo().isEmpty()) {
                mMobileNumber.setText(numberValidation(model.getMobileNo()));
            } else
                mMobileNumber.setText(numberValidation(AppPreference.getMyMobileNo(this)));
            if (!model.getEmail().equals("")) {
                Utils.setUnderLine(model.getEmail(), mEmail);
            }
        }else
            clearAllField();

    }

    private String numberValidation(String number) {
        try {
            if (number.startsWith("+95")) {

                return number;
            }
            if (number.startsWith("0095")) {
                number = number.replaceFirst("0095", "+95");
                return number;
            }
            if (number.startsWith("+") || number.startsWith("00"))
            {
                BaseActivity baseActivity = new BaseActivity() {
                    @Override
                    public <T> void response(int resultCode, T data) {

                    }
                };
                String[] countryCodeArray = baseActivity.getCountryCodeNameAndFlagFromNumber(number);
                String scountryCode = countryCodeArray[0];
                number = countryCodeArray[3];
                if (number.contains(" "))
                {
                    number = number.trim();
                    number = number.replace(" ", "");
                }

                return "+"+scountryCode+number.substring(scountryCode.length());

            } else {
                return number;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
    private void setInit() {
        mTitle = (CustomEdittext) findViewById(R.id.add_promotion_edittext_title);
        mDescription = (LinedEditText) findViewById(R.id.add_promotion_edittext_description);
        mMobileNumber = (CustomEdittext) findViewById(R.id.add_promotion_edittext_number);
        mWebsiteUrl = (CustomEdittext) findViewById(R.id.add_promotion_edittext_website);
        mFacebookUrl = (CustomEdittext) findViewById(R.id.add_promotion_edittext_Facebook);
        mLastUpdated = (CustomTextView) findViewById(R.id.tv_last_updated);
        CustomTextView mLocateUs = (CustomTextView) findViewById(R.id.addview_promotion_locate_add);
        CustomTextView btn_viewExample = (CustomTextView) findViewById(R.id.btn_view);
        CustomTextView mQuickEdit = (CustomTextView) findViewById(R.id.addview_promotion_quickEdit);

        mName = (CustomEdittext) findViewById(R.id.add_promotion_edittext_name);
        mEmail = (CustomEdittext) findViewById(R.id.add_promotion_edittext_email);
        mDelete = (CustomButton) findViewById(R.id.btn_delete);

        buttonLogout.setOnClickListener(this);
        mDelete.setOnClickListener(this);
        mLocateUs.setOnClickListener(this);
        mQuickEdit.setOnClickListener(this);

        btn_viewExample.setOnClickListener(this);
        Utils.setEditTextFilter(mFacebookUrl);
        Utils.setEditTextFilter(mWebsiteUrl);
        setMaxLength(mMobileNumber, 17);
        setMaxLength(mTitle, 50);
        setMaxLength(mDescription, 500);
    }





    @Override
    public <T> void response(int resultCode, T data)
    {

        switch (resultCode) {
            case REQUEST_ADD_PROMOTION:
                String promotion = (String) data;
                try {
                    JSONObject response = new JSONObject(promotion);
                    String code = response.getString("Code");
                    String dataResponse = response.getString("Data");
                    if (code.equals("200")) {
                        JSONObject dataObject = new JSONObject(dataResponse);
                        JSONArray promotionArray = dataObject.getJSONArray("Promotion Details");
                        for (int i = 0; i < promotionArray.length(); i++) {
                            JSONObject object = promotionArray.getJSONObject(i);
                            String promotionID = object.getString("PromotionId");
                            AppPreference.setPromotionId(this, promotionID);
                            model.setId(promotionID);
                            model.setLast_updated(object.getString("UpdatedDate"));
                            db.insertPromotion(model);
                           // Utils.showCustomToastMsg(this, R.string.Promotion_addedSuccessful, true);
                            showToast(R.string.Promotion_addedSuccessful);
                            finish();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case REQUEST_DELETE_PROMOTION:
                String deletePromotion = (String) data;
                try {
                    JSONObject jsonRes = new JSONObject(deletePromotion);
                    if (jsonRes.getString("Code").equals("200")) {
                        db.deletePromotion();
                        clearAllField();
                        showToast(R.string.deleted_successful);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_delete:
                CallingApIS api = new CallingApIS(AddViewPromotion.this, AddViewPromotion.this, 1);
                api.deletePromotion();
                break;
            case R.id.custom_actionbar_application_right_image:
                hideKeyboard();
                if (buttonLogout.getText().toString().equalsIgnoreCase(getString(R.string.edit))) {
                    setEnableFieldEdit();
                    return;
                }
                if (!Utils.isEmpty(mTitle.getText().toString())) {
                    model = new PromotionModel();
                    model.setTitle(mTitle.getText().toString());
                    model.setDescripton(mDescription.getText().toString());
                    model.setMobileNo(mMobileNumber.getText().toString());
                    model.setWebsite(mWebsiteUrl.getText().toString());
                    model.setFacebook(mFacebookUrl.getText().toString());
                    model.setName(mName.getText().toString());
                    model.setEmail(mEmail.getText().toString());
                    CallingApIS api1 = new CallingApIS(AddViewPromotion.this, AddViewPromotion.this, 1);
                    api1.addPromotionMsg(model);
                } else {
                    if (Utils.isEmpty(mTitle.getText().toString()))
                        mTitle.setError(getResources().getString(R.string.error_promotion_titleblank));
                }
                break;
            case R.id.addview_promotion_locate_add:
                if (Utils.isReallyConnectedToInternet(this)) {
                    Intent intent = new Intent(this, Webview.class);
                    intent.putExtra("url", addTemaplteURL());
                    startActivity(intent);
                }else
                    showToast(R.string.no_internet);
                break;

            case R.id.addview_promotion_quickEdit:
                if (Utils.isReallyConnectedToInternet(this))
                {
                    Intent intentq = new Intent(this, Webview.class);
                    intentq.putExtra("url", viewTempalte());
                    startActivity(intentq);
                }else
                    showToast(R.string.no_internet);
                break;
            case R.id.btn_view:
                if (Utils.isReallyConnectedToInternet(this))
                {
                    Intent web = new Intent(this, Webview.class);
                    web.putExtra("url", makeingExmmpleUrl());
                    startActivity(web);
                }else
                    showToast(R.string.no_internet);
                break;

            case R.id.add_promotion_edittext_website:
                if(mWebsiteUrl.getText().toString().isEmpty())
                    mWebsiteUrl.setText(getString(R.string.website_link));
                break;
            case R.id.add_promotion_edittext_Facebook:
                if(mFacebookUrl.getText().toString().isEmpty())
                    mFacebookUrl.setText(getString(R.string.facebook_link));
                break;
        }
    }

    private void setEnableFieldEdit() {
        mTitle.setEnabled(true);
        mDescription.setEnabled(true);
        mName.setEnabled(true);
        mMobileNumber.setEnabled(true);
        mEmail.setEnabled(true);
        mWebsiteUrl.setEnabled(true);
        mFacebookUrl.setEnabled(true);
        mLastUpdated.setVisibility(View.GONE);
        buttonLogout.setText(R.string.Submit);
        mDelete.setVisibility(View.GONE);
    }

    private void clearAllField() {
        mTitle.setEnabled(true);
        mDescription.setEnabled(true);
        mMobileNumber.setEnabled(true);
        mName.setEnabled(true);
        mEmail.setEnabled(true);
        mWebsiteUrl.setEnabled(true);
        mFacebookUrl.setEnabled(true);
        mName.setEnabled(true);
        buttonLogout.setText(getString(R.string.Submit));
        mDelete.setVisibility(View.GONE);
        mLastUpdated.setVisibility(View.GONE);

        mTitle.setText("");
        mDescription.setText("");
        mName.setText("");
        mEmail.setText("");
        mFacebookUrl.setText(getString(R.string.facebook_link));
        mWebsiteUrl.setText(getString(R.string.website_link));
        //mMobileNumber.setText("09");
    }

    private String addTemaplteURL() {
        String url = BASE_TEMPALTE + "MarchantAdmin.aspx?MobileNumber=" + AppPreference.getMyMobileNo(this) + "&SimId=" + Utils.simSerialNumber(this) + "&Msid=" + Utils.getSimSubscriberID(this) + "&Ostype=0";
        Log.d("addTemaplteURL", url);
        return url;

    }

    private String viewTempalte() {
        String url = BASE_TEMPALTE + "MarchantView.aspx?MobileNumber=" + AppPreference.getMyMobileNo(this) + "&SimId=" + Utils.simSerialNumber(this) + "&Msid=" + Utils.getSimSubscriberID(this) + "&Ostype=0";
        Log.d("viewTempalte", url);
        return url;

    }

    private String makeingExmmpleUrl() {
        String url = BASE_TEMPALTE + "Example.aspx?MobileNumber=" + AppPreference.getMyMobileNo(this) + "&SimId=" + Utils.simSerialNumber(this) + "&Msid=" + Utils.getSimSubscriberID(this) + "&Ostype=0";
        Log.d("makeingExmmpleUrl", url);
        return url;

    }

    public void setMaxLength(EditText editText, int maxLength) {
        InputFilter filter = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {
                String blockedChars= "\'~^|{}[]/\\<>\"";
                for (int i = start; i < end; i++) {

                    if (source != null && blockedChars.contains(("" + source))) {
                        return "";
                    }
                }
                return null;
            }
        };

        InputFilter[] fArray = new InputFilter[2];
        fArray[0] = new InputFilter.LengthFilter(maxLength);
        fArray[1]= filter;
        editText.setFilters(fArray);
    }
}