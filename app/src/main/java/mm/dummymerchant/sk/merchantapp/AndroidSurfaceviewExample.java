package mm.dummymerchant.sk.merchantapp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.os.Bundle;
import android.os.Environment;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import static mm.dummymerchant.sk.merchantapp.Utils.Utils.showToast;


public class AndroidSurfaceviewExample extends Activity implements SurfaceHolder.Callback {
	TextView testView;
	public static int degrees = 0;
	Camera camera;
	SurfaceView surfaceView;
	SurfaceHolder surfaceHolder;
	PictureCallback rawCallback;
	ShutterCallback shutterCallback;
	PictureCallback jpegCallback;
	File f;
	public static String path;
	private int camId;
	private int numberOfCamera;
	private boolean previewing;
	private boolean isFrontCamera=false ;
	private static boolean safeToTakePicture=false;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main1);

		surfaceView = (SurfaceView) findViewById(R.id.surfaceView);
		surfaceHolder = surfaceView.getHolder();
		surfaceHolder.addCallback(this);
		surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		jpegCallback = new PictureCallback() {

			private boolean isImageCaptured;

			public void onPictureTaken(byte[] data, Camera camera) {
				if (data != null) {
					int screenWidth = getResources().getDisplayMetrics().widthPixels;
					int screenHeight = getResources().getDisplayMetrics().heightPixels;
					Bitmap bm = BitmapFactory.decodeByteArray(data, 0,
							(data != null) ? data.length : 0);

					if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
						// Notice that width and height are reversed
						Bitmap scaled = Bitmap.createScaledBitmap(bm,
								screenHeight, screenWidth, true);
						int w = scaled.getWidth();
						int h = scaled.getHeight();
						// Setting post rotate to 90
						Matrix mtx = new Matrix();
						if(isFrontCamera)
							mtx.postRotate(270);
						else
						mtx.postRotate(90);
						// Rotating Bitmap
						bm = Bitmap.createBitmap(scaled, 0, 0, w, h, mtx, true);

					} else {// LANDSCAPE MODE
							// No need to reverse width and height
						Bitmap scaled = Bitmap.createScaledBitmap(bm,
								screenWidth, screenHeight, true);
						bm = scaled;
					}

					ByteArrayOutputStream bytes = new ByteArrayOutputStream();
					bm.compress(Bitmap.CompressFormat.JPEG, 40, bytes);

					// you can create a new file name "test.jpg" in sdcard
					// folder.
					try {
						f = new File(Environment.getExternalStorageDirectory()
								+ File.separator + "test.jpg");
						f.createNewFile();
						// write the bytes in file
						FileOutputStream fo = new FileOutputStream(f);
						fo.write(bytes.toByteArray());

						// remember close de FileOutput
						fo.close();
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					// photoPreview.setImageBitmap(bm);
				}
				isImageCaptured = true;
				path = f.getAbsolutePath();
				Intent i = getIntent();
				i.putExtra("path", path);
				setResult(102, i);
				finish();
			}
		};

		findViewById(R.id.capture).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				try {
					if (camera != null && safeToTakePicture){
						camera.takePicture(null, null, jpegCallback);
						safeToTakePicture = false;
					}
					else
						showToast(AndroidSurfaceviewExample.this, getString(R.string.camera_error));
				} catch (Exception e) {
					showToast(AndroidSurfaceviewExample.this, getString(R.string.camera_error));
				}
			}
		});
		findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
			}
		});
	}



	public void refreshCamera() {
		if (surfaceHolder.getSurface() == null) {
			// preview surface does not exist
			return;
		}

		// stop preview before making changes
		try {
			camera.stopPreview();
		} catch (Exception e) {
			// ignore: tried to stop a non-existent preview
		}

		// set preview size and make any resize, rotate or
		// reformatting changes here
		// start preview with new settings
		try {
			camera.setPreviewDisplay(surfaceHolder);
			camera.startPreview();
			safeToTakePicture = true;
		} catch (Exception e) {

		}
	}

	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
		refreshCamera();
	}

	public void surfaceCreated(SurfaceHolder holder) {

	try {
			isFrontCamera=true;
			camera= Camera.open(CameraInfo.CAMERA_FACING_FRONT);
			camera.setPreviewDisplay(surfaceHolder);
			previewing = true;
			setCameraDisplayOrientation(180, camera);

		} catch (Exception e) {

		}

		try{
			if(camera==null) {
				isFrontCamera=false;
				camera = Camera.open(CameraInfo.CAMERA_FACING_BACK);
				camera.setPreviewDisplay(surfaceHolder);
				previewing = true;
				setCameraDisplayOrientation(180, camera);
			}
		}catch (Exception e){

		}
	}
	@SuppressLint("NewApi")
	public static void setCameraDisplayOrientation(int degrees,Camera camera) {
	CameraInfo info = new CameraInfo();
		Camera.getCameraInfo(0, info);
		int result;
		result = (info.orientation + degrees) % 360;
		result = (360 - result) % 360; // compensate the mirror
		camera.setDisplayOrientation(result);
		camera.startPreview();
		safeToTakePicture = true;
	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		// stop preview and release camera
		if (camera!=null) {
			camera.stopPreview();
			camera.release();
			camera = null;
		}
	}
	public void onCancel(View v){
		finish();
	}
}