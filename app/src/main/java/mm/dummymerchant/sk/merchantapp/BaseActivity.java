package mm.dummymerchant.sk.merchantapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.hardware.Camera;
import android.location.Location;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Vibrator;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import android.text.InputFilter;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.Reader;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.integration.android.IntentIntegrator;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.MySSLSocketFactory;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.TrustManagerFactory;

import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.Constant;
import mm.dummymerchant.sk.merchantapp.Utils.GPSLocationResponse;
import mm.dummymerchant.sk.merchantapp.Utils.Utils;
import mm.dummymerchant.sk.merchantapp.customView.CustomButton;
import mm.dummymerchant.sk.merchantapp.customView.CustomEdittext;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
import mm.dummymerchant.sk.merchantapp.db.DBHelper;
import mm.dummymerchant.sk.merchantapp.httpConnection.CallAppServerInterface;
import mm.dummymerchant.sk.merchantapp.httpConnection.CallRequestInterface;
import mm.dummymerchant.sk.merchantapp.httpConnection.HttpInterface;
import mm.dummymerchant.sk.merchantapp.httpConnection.NewAsycnTask;
import mm.dummymerchant.sk.merchantapp.httpConnection.RequestProgressDialog;
import mm.dummymerchant.sk.merchantapp.introduction.GPSTracker;
import mm.dummymerchant.sk.merchantapp.model.CashierModel;
import mm.dummymerchant.sk.merchantapp.model.ConstactModel;
import mm.dummymerchant.sk.merchantapp.model.ErrorMessageModel;
import mm.dummymerchant.sk.merchantapp.model.GPS_Location;
import mm.dummymerchant.sk.merchantapp.model.MasterDetails;
import mm.dummymerchant.sk.merchantapp.model.NetworkOperatorModel;

import static mm.dummymerchant.sk.merchantapp.Utils.Utils.getFont;
import static mm.dummymerchant.sk.merchantapp.Utils.Utils.isConnectedToInternet;
import static mm.dummymerchant.sk.merchantapp.Utils.Utils.isReallyConnectedToInternet;


/**
 * Created by user on 10/26/2015.
 */
public abstract class BaseActivity extends AppCompatActivity implements Constant, DialogInterface.OnCancelListener, HttpInterface, CallRequestInterface,CallAppServerInterface {

    private Activity contextGPS;
    protected DBHelper db;
    public ArrayList<NetworkOperatorModel> listOfCountryCodesAndNames;
    public static List<String> lwifi = new ArrayList<String>();
    public static List<String> lBlue = new ArrayList<String>();
    private static int REQUEST_ADDRESS = 0;
    public static List<ScanResult> mWifiResult;
    private Locale myLocale;
    public RequestProgressDialog mProgDailog;
    String mDestination = "";
    public static int textCount = 0;
    public static String TRANSCATON_TYPE = "";
    public static String SEARCHING_TYPE = "";
    public static int TransactionType = 0;
    public static Context baseContext;
    public View view_custombar;
    public ImageView mDefaultLang;
    public ImageView backButton;
    public CustomTextView buttonLogout;
    public Toolbar toolbar;
    public CustomTextView mTitleOfHeader;
    public RelativeLayout mcustomHeaderBg;
    private static long startTime = 60000 * 5;
    private static long interval = 1000;
    public static IdealCountTimer countDownTimer = new IdealCountTimer(startTime, interval);
    public LinearLayout custom_action_bar_lay_submit_log, custom_action_bar_lay_back;
    static int color_foreground;
    public static String BLT_SEARCHING_TYPE = "";
    public static String MasterORCashier, MasterORCashier_ID, MasterORCashier_Mobile_Number;
    private static int REQUEST_ADDRESSS = 0;
    Window window;



    AsyncHttpResponseHandler mAppServerHandler = new AsyncHttpResponseHandler() {

        @Override
        public void onStart() {
            super.onStart();


            if (REQUEST_ADDRESS == REQUEST_TYPE_GCM_REGISTRATION_APPSERVER || REQUEST_ADDRESS == REQUEST_TYPE_UPLOAD_DEVICE || REQUEST_ADDRESS == REQUEST_TYPE_ENCRYPTED_KEY || REQUEST_ADDRESS == 19292 || REQUEST_ADDRESS == 100030 || REQUEST_ADDRESS == REQUEST_TYPE_GET_CONTACTS || REQUEST_ADDRESS == REQUEST_TYPE_RETRIVE_PROFILE) {


            } else if (mProgDailog != null) {

                mProgDailog = new RequestProgressDialog(BaseActivity.this, getString(R.string.loading_msg), REQUEST_ADDRESS);
                mProgDailog.setOnCancelListener(BaseActivity.this);
                mProgDailog.setIndeterminate(true);
                mProgDailog.show();
            }
        }


        @Override
        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {


            try {
                String response = new String(responseBody, "UTF-8");
                Log.d("RESPONSE--", response);
                System.out.println("--" + response);
                switch (REQUEST_ADDRESS) {

                    case REQUEST_TYPE_ENCRYPTED_KEY:
                        System.out.println(response);
                        response(REQUEST_TYPE_ENCRYPTED_KEY, response);
                        dismissDialog();
                        break;


                    case REQUEST_TYPE_GET_CONTACTS:
                        response = response.replace("<string xmlns=\"http://schemas.microsoft.com/2003/10/Serialization/\">", "");
                        response = response.replace("</string>", "");
                        dismissDialog();
                        break;


                    case REQUEST_TYPE_UPLOAD_PHOTO:

                        response(REQUEST_ADDRESS, null);
                        dismissDialog();
                        break;

                    case REQUEST_TYPE_GCM_REGISTRATION_APPSERVER:

                        response = response.replace("<string xmlns=\"http://schemas.microsoft.com/2003/10/Serialization/\">", "");
                        response = response.replace("</string>", "");
                        JSONTokener tokener1 = new JSONTokener(response);
                        JSONObject object1 = new JSONObject(tokener1);
                        JSONObject status1 = object1.getJSONObject("status");

                        if (status1.getString("code").equals("200") && status1.getString("msg").equals("Sucess")) {

                            showToast(getString(R.string.gps_update));
                        }
                        response(REQUEST_TYPE_GCM_REGISTRATION_APPSERVER, null);
                        dismissDialog();
                        break;

                    case REQUEST_TYPE_AFTER_LOGIN:

                        response = response.replace("<string xmlns=\"http://schemas.microsoft.com/2003/10/Serialization/\">", "");
                        response = response.replace("</string>", "");
                        JSONTokener tokener11 = new JSONTokener(response);
                        JSONObject object11 = new JSONObject(tokener11);
                        JSONObject status11 = object11.getJSONObject("status");
                        JSONObject status123 = object11.getJSONObject("data");
                        JSONArray encrpted = status123.getJSONArray("Table");
                        JSONObject encrpted_o = encrpted.getJSONObject(0);

                        if (status11.getString("code").equals("200") && status11.getString("msg").equals("Sucess")) {
                            AppPreference.setEncrptedKey(BaseActivity.this, encrpted_o.getString("EncryptedKey"));
                        }
                        dismissDialog();
                        break;


                    case REQUEST_TYPE_TRANSACTION:
                        dismissDialog();
                        JSONTokener tokener = new JSONTokener(response);
                        JSONObject object = new JSONObject(tokener);
                        JSONObject status = object.getJSONObject("status");
                        DBHelper db = new DBHelper(BaseActivity.this);
                        dismissDialog();
                        break;

                    case REQUESTTYPE_GPS_CELLID:
                        GPS_Location gpsLocation = new GPS_Location();
                        gpsLocation.setGPSLocation(response);
                        GPSLocationResponse gpsLocationResponse = (GPSLocationResponse) contextGPS;
                        gpsLocationResponse.onSuccess(gpsLocation.getGpsLocation());

                        break;


                    default:
                        dismissDialog();
                        break;
                }

            } catch (Exception e) {

                e.printStackTrace();

            }
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            dismissDialog();
            if (statusCode == 404) {
                showToast(getString(R.string.failure_msg1));
            } else if (statusCode == 500) {
                showToast(getString(R.string.failure_msg2));
            } else {
            }
        }
    };

    void dismissDialog() {
        if (mProgDailog != null && mProgDailog.isShowing())
            mProgDailog.dismiss();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new DBHelper(getApplicationContext());
        listOfCountryCodesAndNames = new NetworkOperatorModel().getListOfNetworkOperatorModel(this);
        setCustomActionBar();
        changeLang(AppPreference.getLanguage(getApplicationContext()));
        baseContext = getBaseContext();
        color_foreground = getResources().getColor(R.color.secongry_text_color);
    }

    public abstract <T> void response(int resultCode, T data);

    public void showToast(String message) {
        showMessageError(message);
    }

    public void showToast1(String message) {
        showMessageSucces(message);
    }

    protected void showToast(Context context, String message) {
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        v.setTypeface(Utils.getFont(context));
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }


    private void showMessageError(String message) {
        MediaPlayer p = MediaPlayer.create(this, R.raw.error_tune);
        p.start();
        ((Vibrator) getSystemService(VIBRATOR_SERVICE)).vibrate(200);
        LayoutInflater li = LayoutInflater.from(BaseActivity.this);
        View promptsView = li.inflate(R.layout.alert_window, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(BaseActivity.this);
        alertDialogBuilder.setView(promptsView);
        final CustomTextView userInput = (CustomTextView) promptsView.findViewById(R.id.alert);
        userInput.setText(message);
        CustomButton ok = (CustomButton) promptsView.findViewById(R.id.btn_settings);
        final AlertDialog dialog1 = alertDialogBuilder.create();
        ok.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog1.dismiss();

            }
        });
        dialog1.show();
    }

    public void showMessageError1(String message) {
        MediaPlayer p = MediaPlayer.create(this, R.raw.error_tune);
        p.start();
        ((Vibrator) getSystemService(VIBRATOR_SERVICE)).vibrate(200);
        LayoutInflater li = LayoutInflater.from(BaseActivity.this);
        View promptsView = li.inflate(R.layout.alert_window, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(BaseActivity.this);
        alertDialogBuilder.setView(promptsView);
        final CustomTextView userInput = (CustomTextView) promptsView.findViewById(R.id.alert);
        userInput.setText(message);
        CustomButton ok = (CustomButton) promptsView.findViewById(R.id.btn_settings);
        final AlertDialog dialog1 = alertDialogBuilder.create();
        ok.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent in = new Intent(BaseActivity.this, NumberValidation1.class);
                startActivity(in);
                dialog1.dismiss();

            }
        });
        dialog1.show();
    }


    public void setHeadingVerifyUser() {
        buttonLogout.setText(getResources().getString(R.string.Submit));
        buttonLogout.setBackgroundResource(R.drawable.taxi_hotel_res_selector);
        mcustomHeaderBg.setBackgroundResource(R.drawable.reg_header);
        toolbar.setBackgroundColor(getResources().getColor(R.color.regHeader));
        mTitleOfHeader.setText(getResources().getString(R.string.verify_user));
        mTitleOfHeader.setTypeface(getFont(this), Typeface.BOLD);
        buttonLogout.setTextColor(getResources().getColor(R.color.white));
        backButton.setBackgroundResource(R.drawable.back_arrow_bill_icon);

        mTitleOfHeader.setTextColor(getResources().getColor(R.color.dark_blue));
        if (AppPreference.getLanguage(this).equalsIgnoreCase("my"))
            buttonLogout.setTextSize(18);
        //countDownTimer.cancel();
    }


    private void showMessageSucces(String message) {
        MediaPlayer p = MediaPlayer.create(this, R.raw.success_tune);
        p.start();
        ((Vibrator) getSystemService(VIBRATOR_SERVICE)).vibrate(200);
        LayoutInflater li = LayoutInflater.from(BaseActivity.this);
        View promptsView = li.inflate(R.layout.alert_window, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(BaseActivity.this);
        alertDialogBuilder.setView(promptsView);
        final CustomTextView userInput = (CustomTextView) promptsView.findViewById(R.id.alert);
        userInput.setText(message);
        CustomButton ok = (CustomButton) promptsView.findViewById(R.id.btn_settings);
        final AlertDialog dialog1 = alertDialogBuilder.create();
        ok.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog1.dismiss();

            }
        });
        dialog1.show();
    }


    public void showErrorMessanger(String msg) {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.show_message_registration);
        final CustomTextView userInput = (CustomTextView) dialog.findViewById(R.id.dialog_editext);
        final CustomTextView tvOK = (CustomTextView) dialog.findViewById(R.id.tv_ok);
        userInput.setText(msg);
        dialog.setCancelable(true);

        tvOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        dialog.show();

    }



    @Override
    public <T> void callRequestAPI(String url, Bundle mPrams, int address) {
        REQUEST_ADDRESS = address;
        getTest(url, mPrams);
    }

    @Override
    public <T> void asyncResponse(int resultCode, T data)
    {
        buttonLogout.setEnabled(true);
        response(resultCode, data);
        Log.e("Merchant Tag", "resultCode=" + resultCode + "Data=" + data.toString());
    }

    @Override
    public <T> void asyncResponseFail(int resultCode, T data) {
        try {
            String response = (String) data;
            ErrorMessageModel model = new ErrorMessageModel(response);
            String CellID = getLACAndCID(BaseActivity.this);
            int resultcodes = 0;
            String Lat = "0";
            String Lang = "0";
            String LatLang = getLocationFromNetwork();
            if (!LatLang.isEmpty()) {
                Lat = LatLang.substring(0, LatLang.indexOf(","));
                Lang = LatLang.substring(LatLang.indexOf(",") + 1, LatLang.length());
            }
            String resukt = model.getResultCode();
            resultcodes = Integer.parseInt(resukt);
            switch (resultcodes)
            {
                case 930:
                    Intent i = new Intent(BaseActivity.this, NewStartupActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    finish();
                    break;
                case 104:


                    break;
                case 945:


                    break;
                case 929:
                    Intent fin = new Intent(BaseActivity.this, NewStartupActivity.class);
                    fin.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(fin);
                    finish();

                    break;
                case 9:
                    if (resultCode == REQUEST_TYPE_OTP) {
                        showAlert(model.getDescription());
                    }
                    if (resultCode == REQUEST_TYPE_LOGIN) {
                        showAlert(model.getDescription());
                    }
                    if (resultCode == REQUEST_CHECK_DESTINATION_VALIDATION) {
                        response(REQUEST_CHECK_DESTINATION_VALIDATION, null);
                    }
                    break;
                case REQUEST_CHECK_DESTINATION_VALIDATION:
                    response(REQUEST_CHECK_DESTINATION_VALIDATION, null);
                case 711:
                    String msg = getResources().getString(R.string.norecord_speedkyat_server);
                    //showToastt(msg);
                    break;
                case 60:
                    showAlert(model.getDescription());
                    break;


                /*default:
                    showAlert(model.getDescription());
                    break;*/
            }
            REQUEST_ADDRESS = 19292;

            getFailureTransactionDetails(AppPreference.getMyMobileNo(BaseActivity.this), mDestination, resultcodes + "", Lat, Lang, CellID);


            if (REQUEST_ADDRESS == REQUEST_TYPE_RECEIVED_TRANSCATION) {
                response(REQUEST_ADDRESS, null);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCancel(DialogInterface dialog) {

    }

    public static String convertValidNumber(String number) {
        if (number.contains("+"))
            number = number.replace("+", "00");
        if (number.contains(" "))
            number.replace(" ", "");
        if (number.contains("-"))
            number = number.replace("-", "");
        return number;
    }


    public String[] getCountryCodeNameAndFlagFromNumber(String no) {
        String countryCode = "", countryName = "", flagStr = "";

        int flag = -1;
        if (no.startsWith("+"))
            no = no.replace("+", "00");
        no = convertValidNumber(no);
        int id = -1;
        if (no.startsWith("+") || no.startsWith("00")) {
            String code = no.substring(0, 6);
// ..is.."+code);
            for (int i = 0; i < listOfCountryCodesAndNames.size(); i++) {

                ArrayList<String> dialingCodes = listOfCountryCodesAndNames.get(i).getListOfDialingCodes();
                if (dialingCodes != null) {
                    for (int j = 0; j < dialingCodes.size(); j++) {
                        String countryId = dialingCodes.get(j).replace("+", "00");

                        if (code.contains(countryId)) {
                            System.out.println(i);
                            id = i;
                            int length = countryId.length();
                            no = no.substring(3);
                            countryCode = listOfCountryCodesAndNames.get(id).getCountryCode();
                            countryName = listOfCountryCodesAndNames.get(id).getCountryName();
                            flag = listOfCountryCodesAndNames.get(i).getCountryImageId();
                            flagStr = String.valueOf(flag);
                            break;
                        }
                    }

                } else {

                    String countryId = "00" + listOfCountryCodesAndNames.get(i).getCountryCode().substring(1);
                    if (code.contains(countryId)) {
                        System.out.println(i);
                        id = i;
                        int length = countryId.length();
                        no = no.substring(length);
                        countryCode = listOfCountryCodesAndNames.get(id).getCountryCode();
                        countryName = listOfCountryCodesAndNames.get(id).getCountryName();
                        flag = listOfCountryCodesAndNames.get(i).getCountryImageId();
                        flagStr = String.valueOf(flag);
                        break;
                    }
                }

            }
            if (countryCode.equals("+95")) {
                no = "0" + no;
            }
        }

        if (id == -1) {
            if (no.startsWith("0")) {
                if (!AppPreference.getCountryCode(this).contains("+95"))
                    no = no.substring(1);
            } else {
                no = "0" + no;
            }
            countryCode = AppPreference.getCountryCode(this);
            flag = AppPreference.getCountryImageID(this);
            countryName = AppPreference.getCountryName(this);
            flagStr = String.valueOf(flag);
        }

        return new String[]{countryCode, countryName, flagStr, no};
    }


    public void setOnGPSLocationListener(Activity context) {
        this.contextGPS = context;
    }


    public void getTest(String url, Bundle params) {

        if (isConnectedToInternet(this)) {
            try {

                if (url.contains("MERCHANTSTATUS")) {

                } else {
                    params.putString(CLIENT_IP, Utils.getLocalIpAddress());
                    params.putString(CLIENT_ADDRESS, "Android=" + Build.DEVICE);
                }
                params.putString(PARAM_CLIENT_TYPE, PARAM_CLIENT_TYPE_VALUE);
                params.putString(VENDOR_CODE, VENDOR_CODE_VALUE);

                String paramsString = "";
                boolean first = false;
                Set<String> parameters = params.keySet();
                Iterator<String> it = parameters.iterator();

                while (it.hasNext()) {

                    String key = it.next().trim();

                    if (params.containsKey(key)) {

                        if (params.containsKey(key)) {

                            if (!first) {
                                paramsString = paramsString + key + "=" + URLEncoder.encode(params.getString(key), "UTF-8");//URLEncoder.encode(params.getString(key));
                                first = true;
                            } else {
                                paramsString = paramsString + ";" + key + "=" + URLEncoder.encode(params.getString(key), "UTF-8");
                            }

                        }
                    }
                }
                url = url + ";" + paramsString;
                System.out.println(url);

                CertificateFactory cf = CertificateFactory.getInstance("X.509");
                AssetManager asstManager = getAssets();
                InputStream caInput = asstManager.open(SSL_CERTIFICATE);

                Certificate ca;
                try {
                    ca = cf.generateCertificate(caInput);
                    System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
                } finally {
                    caInput.close();
                }

                // Create a KeyStore containing our trusted CAs
                String keyStoreType = KeyStore.getDefaultType();
                KeyStore keyStore = KeyStore.getInstance(keyStoreType);
                keyStore.load(null, null);
                keyStore.setCertificateEntry("ca", ca);


                // Create a TrustManager that trusts the CAs in our KeyStore
                String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
                TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
                tmf.init(keyStore);


                MySSLSocketFactory sslSocketFactory= new     MySSLSocketFactory(keyStore);
                sslSocketFactory.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

                Log.e("requestaddress : BAse ", "" + REQUEST_ADDRESS);
                NewAsycnTask as = new NewAsycnTask(BaseActivity.this, this, REQUEST_ADDRESS);
                as.client.setTimeout(30000);
                as.client.setSSLSocketFactory(sslSocketFactory);
                as.client.post(url, as.handlerEstel);




                if (REQUEST_ADDRESSS != REQUEST_TYPE_RECEIVED_TRANSCATION && REQUEST_ADDRESSS != REQUEST_TYPE_OTP)
                    resetTimer();

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            showToast(getString(R.string.no_internet));
        }
    }

    public void showAlertWifi() {
        if (isConnectedToInternet(BaseActivity.this)) {
            if (mWifiResult != null) {
                if (mWifiResult.size() > 0)
                    try {
                        if (mWifiResult != null) {
                            for (int i = 0; i < mWifiResult.size(); i++) {
                                if (mWifiResult.get(i).SSID.equals("net")) {
                                    connectToWifi();
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
            }
        }
    }


    public void connectToWifi() {

        try {
            if (!isConnectedToInternet(this)) {
                WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
                if (!wifiManager.isWifiEnabled()) {
                    wifiManager.setWifiEnabled(true);
                }
                //wifiManager.startScan();
                WifiConfiguration wifiConfig = new WifiConfiguration();

                wifiConfig.SSID = String.format("\"%s\"", "net");
                wifiConfig.preSharedKey = String.format("\"%s\"", "23456789");
                int netId = wifiManager.addNetwork(wifiConfig);
                wifiManager.disconnect();
                wifiManager.enableNetwork(netId, true);
                wifiManager.reconnect();

            }

        } catch (
                Exception e
                )

        {
            e.printStackTrace();
        }
    }

    public void hideKeyboard() {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        }
    }

    public static CharSequence getNumberWithGreyZero(String mobileNo, boolean isMyanmarNo) {
        if (!Utils.isEmpty(mobileNo) && isMyanmarNo && mobileNo.startsWith("0")) {
            SpannableString ss1 = new SpannableString(mobileNo);

            ss1.setSpan(new ForegroundColorSpan(color_foreground), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            return ss1;
        } else {
            return mobileNo;
        }
    }

    public void setCustomActionBar() {

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater inflater = LayoutInflater.from(this);
        view_custombar = inflater.inflate(R.layout.custom_action_bar, null);
        custom_action_bar_lay_back = (LinearLayout) view_custombar.findViewById(R.id.custom_action_bar_lay_back);
        backButton = (ImageView) view_custombar.findViewById(R.id.custom_actionbar_application_left_image);
        custom_action_bar_lay_submit_log = (LinearLayout) view_custombar.findViewById(R.id.custom_action_bar_lay_pay);
        buttonLogout = (CustomTextView) view_custombar.findViewById(R.id.custom_actionbar_application_right_image);
        mTitleOfHeader = (CustomTextView) view_custombar.findViewById(R.id.custom_actionbar_application_textview_title_of_application);
        mcustomHeaderBg = (RelativeLayout) view_custombar.findViewById(R.id.custom_action_bar_application);
        mDefaultLang = (ImageView) view_custombar.findViewById(R.id.img_default_lang);



        actionBar.setCustomView(view_custombar);
        toolbar = (Toolbar) view_custombar.findViewById(R.id.actionBar);
        mTitleOfHeader.setSelected(true);
        custom_action_bar_lay_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        actionBar.setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        Toolbar parent = (Toolbar) view_custombar.getParent();//first get parent toolbar of current action bar
        parent.setContentInsetsAbsolute(0, 0);

        window = getWindow();
      /*  if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.navigationBarHome));
        }*/
    }

    public void showAlert(String message) {

        LayoutInflater li = LayoutInflater.from(BaseActivity.this);
        View promptsView = li.inflate(R.layout.alert_window, null);

        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(BaseActivity.this);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);
        CustomTextView title = (CustomTextView) promptsView.findViewById(R.id.title);
        final CustomTextView userInput = (CustomTextView) promptsView.findViewById(R.id.alert);
        userInput.setText(message);

        CustomButton ok = (CustomButton) promptsView.findViewById(R.id.btn_settings);
        if (message.contains(getResources().getText(R.string.Successful))) {
            title.setText(getResources().getText(R.string.alert));
        }

        final AlertDialog dialog1 = alertDialogBuilder.create();
        dialog1.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        dialog1.getWindow().setLayout(200, ViewGroup.LayoutParams.WRAP_CONTENT);

        // set dialog message
        ok.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog1.dismiss();
            }
        });
        dialog1.show();

    }

    public void setDashboardActionBar() {
        // custom_action_bar_lay_back.setVisibility(View.GONE);
        backButton.setVisibility(View.VISIBLE);
        backButton.setImageResource(R.drawable.menu2);
        mcustomHeaderBg.setBackgroundResource(R.drawable.header_bg2);
        // mTitleOfHeader.setBackgroundResource(R.drawable.home_screen_speedkyat_logo);
        mTitleOfHeader.setText(getResources().getText(R.string.dashboard));
        countDownTimer.cancel();
        buttonLogout.setVisibility(View.GONE);
        //countDownTimer.start();
    }

    public void setForgotPassword()
    {
        mTitleOfHeader.setText(getResources().getString(R.string.Contact_us));
        mTitleOfHeader.setTextColor(getResources().getColor(R.color.dark_blue));
    }

    public void setForgotpwdsActionBar() {
        custom_action_bar_lay_back.setVisibility(View.VISIBLE);
        backButton.setVisibility(View.VISIBLE);
        mcustomHeaderBg.setBackgroundResource(R.drawable.header_bg2);
        mTitleOfHeader.setText(getResources().getText(R.string.Contact_us));
        buttonLogout.setVisibility(View.GONE);
        countDownTimer.cancel();
    }


    public void setMapActivityActionBar()
    {
        backButton.setVisibility(View.VISIBLE);
        mcustomHeaderBg.setBackgroundColor(Color.TRANSPARENT);
        mTitleOfHeader.setText(getResources().getText(R.string.app_name));
        countDownTimer.cancel();
        buttonLogout.setVisibility(View.GONE);
    }

    public void setCashierMenuheader() {
        buttonLogout.setVisibility(View.GONE);
        mTitleOfHeader.setText(getResources().getText(R.string.cashier_title));
        mcustomHeaderBg.setBackgroundResource(R.drawable.header_bg2);
        //mcustomHeaderBg.setBackgroundResource(R.color.cell_dashboard_color_homepage_heading);
    }


    public void setCreateCashierActionBar() {
        // custom_action_bar_lay_back.setVisibility(View.GONE);
        mcustomHeaderBg.setBackgroundResource(R.drawable.header_bg2);
        //  mTitleOfHeader.setBackgroundResource(R.drawable.home_screen_speedkyat_logo);
        mTitleOfHeader.setText(getResources().getText(R.string.createcashier));
        buttonLogout.setText(getResources().getText(R.string.submit));
        countDownTimer.cancel();
        //countDownTimer.start();
    }





    public void setRegistraionActionBar() {
        custom_action_bar_lay_back.setVisibility(View.GONE);
        backButton.setVisibility(View.GONE);
        mcustomHeaderBg.setBackgroundResource(R.drawable.header_bg2);
        mTitleOfHeader.setText(getResources().getText(R.string.registration));
        //   buttonLogout.setText("Submit");
        countDownTimer.cancel();
        //countDownTimer.start();
    }

    public void setDummyMerchantActionBar() {
        custom_action_bar_lay_back.setVisibility(View.VISIBLE);
        backButton.setVisibility(View.VISIBLE);
        mcustomHeaderBg.setBackgroundResource(R.drawable.header_bg2);
        mTitleOfHeader.setText(getResources().getText(R.string.dummymerchant));
        buttonLogout.setVisibility(View.VISIBLE);
        buttonLogout.setText(getResources().getText(R.string.graph));
        countDownTimer.cancel();
    }

    public void setDummyMerchantActionBar_Submit() {
        custom_action_bar_lay_back.setVisibility(View.VISIBLE);
        backButton.setVisibility(View.VISIBLE);
        mcustomHeaderBg.setBackgroundResource(R.drawable.header_bg2);
        mTitleOfHeader.setText(getResources().getText(R.string.dummymerchant));
        buttonLogout.setVisibility(View.VISIBLE);
        buttonLogout.setText(getResources().getText(R.string.submit));
        countDownTimer.cancel();
    }




    public void setDummyMerchant_Startup(String title) {
        custom_action_bar_lay_back.setVisibility(View.GONE);
        backButton.setVisibility(View.VISIBLE);
        mcustomHeaderBg.setBackgroundResource(R.drawable.header_bg2);
        mTitleOfHeader.setText(title);
        buttonLogout.setVisibility(View.GONE);
        countDownTimer.cancel();
    }

    public void setQRCodeHeader() {
        buttonLogout.setVisibility(View.INVISIBLE);
        backButton.setBackgroundResource(R.drawable.back_arrow_etn_icon);
        // toolbar.setBackgroundColor(getResources().getColor(R.color.agent_search));
        mcustomHeaderBg.setBackgroundResource(R.drawable.header_bg2);
        mTitleOfHeader.setText(getResources().getString(R.string.qrcode_generator));
    }

    public void setSettingsActionBar() {
        custom_action_bar_lay_back.setVisibility(View.VISIBLE);
        backButton.setVisibility(View.VISIBLE);
        mcustomHeaderBg.setBackgroundResource(R.drawable.header_bg2);
        mTitleOfHeader.setText(getResources().getText(R.string.setting));
        buttonLogout.setVisibility(View.GONE);
        countDownTimer.cancel();
    }

    public void setNewCashcollectorActionBar(String title) {
        custom_action_bar_lay_back.setVisibility(View.VISIBLE);
        backButton.setVisibility(View.VISIBLE);
        mcustomHeaderBg.setBackgroundResource(R.drawable.header_bg2);
        mTitleOfHeader.setText(title);
        buttonLogout.setVisibility(View.VISIBLE);
        buttonLogout.setText(getResources().getText(R.string.submit));
        countDownTimer.cancel();
    }

    public void setSignPadActionBar() {
        custom_action_bar_lay_back.setVisibility(View.VISIBLE);
        backButton.setVisibility(View.VISIBLE);
        mcustomHeaderBg.setBackgroundResource(R.drawable.header_bg2);
        mTitleOfHeader.setText(getResources().getText(R.string.signaturepad));
        buttonLogout.setVisibility(View.GONE);
        countDownTimer.cancel();
    }

    public void setExistingCashcollectorActionBar(String title) {
        custom_action_bar_lay_back.setVisibility(View.VISIBLE);
        backButton.setVisibility(View.VISIBLE);
        mcustomHeaderBg.setBackgroundResource(R.drawable.header_bg2);
        mTitleOfHeader.setText(title);
        buttonLogout.setVisibility(View.GONE);
        countDownTimer.cancel();
    }


    public void setActivitylogActionBar(String title) {
        custom_action_bar_lay_back.setVisibility(View.VISIBLE);
        backButton.setVisibility(View.VISIBLE);
        mcustomHeaderBg.setBackgroundResource(R.drawable.header_bg2);
        mTitleOfHeader.setText(title);
        buttonLogout.setVisibility(View.GONE);
        //buttonLogout.setText("Submit");
        countDownTimer.cancel();
    }

    public void setUpdateCashierActionBar() {
        // custom_action_bar_lay_back.setVisibility(View.GONE);
        //  backButton.setVisibility(View.GONE);
        mcustomHeaderBg.setBackgroundResource(R.drawable.header_bg2);
        mTitleOfHeader.setText(getResources().getText(R.string.updatecashierdetails));
        buttonLogout.setText(getResources().getText(R.string.submit));
        countDownTimer.cancel();
        //countDownTimer.start();
    }

    public void setViewCashierActionBar() {
        // custom_action_bar_lay_back.setVisibility(View.GONE);
        mcustomHeaderBg.setBackgroundResource(R.drawable.header_bg2);
        mTitleOfHeader.setText(getResources().getText(R.string.viewcashierr));
        buttonLogout.setVisibility(View.GONE);
        countDownTimer.cancel();
        //countDownTimer.start();
    }


    public void setPromotionActionBar() {
        // custom_action_bar_lay_back.setVisibility(View.GONE);
        mcustomHeaderBg.setBackgroundResource(R.drawable.header_bg2);

        mTitleOfHeader.setText(getResources().getText(R.string.Promotion));
        buttonLogout.setText(getResources().getText(R.string.submit));
        countDownTimer.cancel();
        if (AppPreference.getLanguage(this).equalsIgnoreCase("my"))
            buttonLogout.setTextSize(18);
    }

    public void showToast(int message) {
        Toast toast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        v.setTypeface(getFont(this));
        toast.setGravity(Gravity.TOP, 0, 0);
        toast.show();
    }

    public void getFailureTransactionDetails(String lMobile, String lDestination, String lerrorCode, String lLatitude, String lLongitude, String lCellID) {
        RequestParams params = new RequestParams();
        params.put("MobileNumber", lMobile);
        params.put("Destination", lDestination);
        params.put("ErrCode", lerrorCode);
        params.put("Latitude", lLatitude);
        params.put("Longitude", lLongitude);
        params.put("CellID", lCellID);

        getAppServerAPI(BASE_APP_URL + URL_FAILURETRANS, params);
        // getAppServerAPI(TESTAPIURL + URL_FAILURETRANS, params);
    }

    private AsyncHttpClient mAppserverClient = new AsyncHttpClient();



    private void getAppServerAPI(String url, RequestParams params) {
        netChecker();
        NewAsycnTask as = new NewAsycnTask(BaseActivity.this, this, REQUEST_ADDRESSS);
        //as.client.setTimeout(20 * 1000);
        as.client.get(url, params, as.handlerAppServer);
        //  mAppserverClient.setTimeout(20 * 1000);
        //mAppserverClient.get(url, params, mAppServerHandler);

        String aa_url=url+params;
        Log.e("URL", url + params);


    }

    //newly added for automatic payment
    public void netChecker() {

        if (isConnectedToInternet(this)) {

            if (isReallyConnectedToInternet(this)) {

                if (!isConnectedToInternet(this)) {

                    showAlertOffline_(getResources().getString(R.string.online_mode));

                }

            } else {


                if (isConnectedToInternet(this)) {

                    showAlertOffline_(getResources().getString(R.string.online_mode));

                }

            }

        }
    }
    //----------------
    public String transitionDestination(String response) throws XmlPullParserException, IOException
    {

        String text;
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        XmlPullParser myparser = factory.newPullParser();
        myparser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        InputStream stream = new ByteArrayInputStream(response.getBytes());
        myparser.setInput(stream, null);
        text = parseXML(myparser);
        stream.close();

        return text;
    }


    public String getLACAndCID(Context context) {
        TelephonyManager telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (telephony.getPhoneType() == TelephonyManager.PHONE_TYPE_GSM) {
            GsmCellLocation location = (GsmCellLocation) telephony.getCellLocation();
            if (location != null) {
                return String.valueOf(location.getCid());
                //return null;
            }
        }
        if (telephony.getPhoneType() == TelephonyManager.PHONE_TYPE_CDMA) {
            CdmaCellLocation location = (CdmaCellLocation) telephony.getCellLocation();
            if (location != null) {
                return String.valueOf(location.getBaseStationId());
                //return null;
            }
        }
        return "";
    }

    public String getLocationFromNetwork() {
        GPSTracker gpsTracker = new GPSTracker(this);
        if (gpsTracker.gpsStatus()) {
            String Latitude = String.valueOf(gpsTracker.latitude);
            String Longitude = String.valueOf(gpsTracker.longitude);
            if (Latitude.equals("0.0") || (Longitude.equals("0.0"))) {
                showToast((R.string.gps_msg));
            } else {
                return Latitude + ", " + Longitude;
            }
        } else {
            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            boolean network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            if (network_enabled) {
                Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (location != null)
                    return location.getLatitude() + ", " + location.getLongitude();
            }
        }
        return "0.0, 0.0";
    }

    String parseXML(XmlPullParser myParser) throws XmlPullParserException, IOException {
        int event;
        String text = null;
        event = myParser.getEventType();
        while (event != XmlPullParser.END_DOCUMENT) {
            String name = myParser.getName();
            switch (event) {
                case XmlPullParser.START_TAG:
                    break;
                case XmlPullParser.TEXT:
                    text = myParser.getText();
                    break;
                case XmlPullParser.END_TAG:
                    if (name.equals("string"))


                        break;
            }
            event = myParser.next();
        }

        return text;
    }

    public void Check_MasterORCashier_Login() {
        Log.i("Calling Function", "Check_MasterORCashier_Login");
        MasterORCashier = AppPreference.getMasterOrCashier(this);
        if (MasterORCashier.equals(MASTER)) {
            System.out.println("Enter the IF Condition with Master Login ");
            MasterORCashier_ID = "Master";
            db.open();
            MasterDetails MD = db.getMasterDetails();
            db.close();
            MasterORCashier_Mobile_Number = MD.getUsername();
            Log.i("Login Number : Master", MasterORCashier_Mobile_Number);
        } else {
            System.out.println("Enter the else Condition with Cashier Login ");
            MasterORCashier_ID = SliderScreen.present_Cashier_Id;
            db.open();
            CashierModel CM = db.getCashierObject(MasterORCashier_ID);
            db.close();
            MasterORCashier_Mobile_Number = CM.getCashier_Number();
            Log.i("Login Number : Cashier", MasterORCashier_Mobile_Number);
        }
        System.out.println("MasterORCashier_ID : " + MasterORCashier_ID);
    }

    public void showToastt(String message) {
        Toast toast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        v.setTypeface(getFont(this));
        v.setText(message);
        toast.setGravity(Gravity.TOP, 0, 0);
        toast.show();
    }


    public void Contact_get_remote(String mob) {
        String serialNumber = "";

        try {
            TelephonyManager TelephonyMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            if (TelephonyMgr.getSimSerialNumber() != null)
                serialNumber = TelephonyMgr.getSimSerialNumber();
        } catch (Exception e) {
            e.printStackTrace();
        }
        RequestParams params = new RequestParams();
        params.put(MOBILE_NO, mob);
        params.put(SIM_ID, serialNumber);
        REQUEST_ADDRESS = REQUEST_TYPE_GET_CONTACTS;
        getAppServerAPI(BASE_APP_URL + URL_REMOTE_CONTACTS_INFO, params);
    }

    public static String getReleventImage(String mCat, String sCat) {
        try {
            String response = Utils.readTextFileFromAssets(baseContext, "business_category_icon.txt");
            JSONObject jsonObjects;
            jsonObjects = new JSONObject(response);
            if (jsonObjects.getJSONObject("status").getString("code").equals("200")) {
                JSONArray questionsObj = jsonObjects.getJSONArray("data");
                for (int i = 0; i < questionsObj.length(); i++) {
                    JSONObject categoryObj = questionsObj.getJSONObject(i);
                    if (categoryObj.getString("category_name").contains(mCat)) {
                        JSONArray jsonArrayCat = categoryObj.getJSONArray("subcategory");
                        JSONObject jsonSubCat = (JSONObject) jsonArrayCat.get(0);
                        if (!jsonSubCat.getString(String.valueOf(sCat)).isEmpty()) {
                            Log.d("emoji", "" + categoryObj.getString("category_name").substring(2) + jsonSubCat.getString(String.valueOf(sCat)));
                            return categoryObj.getString("category_name").substring(2) + jsonSubCat.getString(String.valueOf(sCat));
                        }
                    }

                }

            }

        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static class IdealCountTimer extends CountDownTimer {

        public IdealCountTimer(long startTime, long interval) {
            super(startTime, interval);

        }

        @Override
        public void onFinish() {
            Log.d("Timer", "Time's up!");
            Intent intent = new Intent(baseContext, NewStartupActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("Exit me", true);
            baseContext.startActivity(intent);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            Log.d("Timer", "Time:" + millisUntilFinished);
            if (millisUntilFinished < 60000 && millisUntilFinished > 58000) {
                showToast(baseContext, baseContext.getString(R.string.session_timeout));
                ((Vibrator) baseContext.getSystemService(VIBRATOR_SERVICE)).vibrate(100);
            }

        }

        private void showToast(Context context, String message) {
            Toast toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
            TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
            v.setTypeface(getFont(context));
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }



    public void setRegistrationBackground() {
        buttonLogout.setText(getResources().getString(R.string.Submit));
        // buttonLogout.setBackgroundResource(R.drawable.taxi_hotel_res_selector);
        // mcustomHeaderBg.setBackgroundResource(R.drawable.heading_change_language_screen);
        mTitleOfHeader.setText(getResources().getString(R.string.registration));
        mTitleOfHeader.setTypeface(getFont(this), Typeface.BOLD);
        //buttonLogout.setTextColor(getResources().getColor(R.color.white));
        mcustomHeaderBg.setBackgroundResource(R.drawable.reg_header);
        backButton.setImageResource(R.drawable.back_arrow_bill_icon);

        mTitleOfHeader.setTextColor(getResources().getColor(R.color.dark_blue));
        if (AppPreference.getLanguage(this).equalsIgnoreCase("my"))
            buttonLogout.setTextSize(22);


    }

    public void setRegistrationBackground1() {
        buttonLogout.setText(getResources().getString(R.string.Submit));
        // buttonLogout.setBackgroundResource(R.drawable.taxi_hotel_res_selector);
        // mcustomHeaderBg.setBackgroundResource(R.drawable.heading_change_language_screen);
        mTitleOfHeader.setText(getResources().getString(R.string.registration));
        mTitleOfHeader.setTypeface(getFont(this), Typeface.BOLD);
        //buttonLogout.setTextColor(getResources().getColor(R.color.white));
        mcustomHeaderBg.setBackgroundResource(R.drawable.header_bg2);
        backButton.setImageResource(R.drawable.back_arrow_bill_icon);

        mTitleOfHeader.setTextColor(getResources().getColor(R.color.white));
        if (AppPreference.getLanguage(this).equalsIgnoreCase("my"))
            buttonLogout.setTextSize(22);


    }

    public void showKeyboard(EditText editText) {
        editText.requestFocus();
        InputMethodManager manager = (InputMethodManager)
                this.getSystemService(Context.INPUT_METHOD_SERVICE);
        manager.showSoftInput(editText, 0);
    }

    public void changeLang(String lang) {
        if (lang.equalsIgnoreCase(""))
            return;
        myLocale = new Locale(lang);
        saveLocale(lang);
        Locale.setDefault(myLocale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = myLocale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }

    public void saveLocale(String lang) {
        String langPref = "Language";
        SharedPreferences prefs = getSharedPreferences("CommonPrefs", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(langPref, lang);
        editor.commit();
    }

    public String[] getCountryCodeFlagNumber(String countryCode, String no) {
        try {
            String countryName = "", flagStr = "";
            int flag = -1;
            no = convertValidNumberInRegisteration(no);
            int id = -1;

            if (no.startsWith("00") && countryCode.length() > 0) {
                String code = "00" + countryCode.substring(1);
                for (int i = 0; i < listOfCountryCodesAndNames.size(); i++) {
                    ArrayList<String> dialingCodes = listOfCountryCodesAndNames.get(i).getListOfDialingCodes();
                    if (dialingCodes != null) {
                        for (int j = 0; j < dialingCodes.size(); j++) {
                            String countryId = "00" + dialingCodes.get(j).substring(1);
                            if (no.length() >= 6) {
                                String code1 = no.substring(0, 6);
                                if (code1.startsWith(countryId)) {
                                    id = i;
                                    no = no.substring(code.length());
                                    countryCode = listOfCountryCodesAndNames.get(id).getCountryCode();
                                    countryName = listOfCountryCodesAndNames.get(id).getCountryName();
                                    flag = listOfCountryCodesAndNames.get(id).getCountryImageId();
                                    flagStr = String.valueOf(flag);
                                    break;
                                }
                            }
                        }
                    } else {

                        String countryId = "00" + listOfCountryCodesAndNames.get(i).getCountryCode().substring(1);
                        if (code.startsWith(countryId)) {
                            id = i;
                            no = no.substring(code.length());
                            countryCode = listOfCountryCodesAndNames.get(id).getCountryCode();
                            countryName = listOfCountryCodesAndNames.get(id).getCountryName();
                            flag = listOfCountryCodesAndNames.get(id).getCountryImageId();
                            flagStr = String.valueOf(flag);
                            break;
                        }
                    }
                }
                if (code.equals("0095")) {
                    no = "0" + no;
                }
            }

            if (id == -1) {
                if (no.startsWith("0")) {
                    if (!AppPreference.getCountryCode(this).equals("+95"))
                        no = no.substring(1);
                } /*else {
                    no = "0" + no;
                }*/
                countryCode = AppPreference.getCountryCode(this);
                flag = AppPreference.getCountryImageID(this);
                countryName = AppPreference.getCountryName(this);
                flagStr = String.valueOf(flag);
            }

            return new String[]{countryCode, countryName, flagStr, no};

        } catch (Exception e) {
            String countryName = "", flagStr = "";
            int flag = -1;
            countryCode = AppPreference.getCountryCode(this);
            flag = AppPreference.getCountryImageID(this);
            countryName = AppPreference.getCountryName(this);
            flagStr = String.valueOf(flag);
            return new String[]{countryCode, countryName, flagStr, ""};
        }
    }

    public static String convertValidNumberInRegisteration(String number) {
        if (number.contains(" "))
            number.replace(" ", "");
        if (number.contains("-"))
            number = number.replace("-", "");
        return number;
    }

    public String sendSerializableData(String url, String request) {

        try {
            URL url1 = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) url1.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(request);
            wr.flush();
            wr.close();

            int responseCode = conn.getResponseCode();
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(conn.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            return response.toString();

        } catch (Exception e) {
            return e.toString();
        }

    }


    @Override
    public void onConfigurationChanged(android.content.res.Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (myLocale != null) {
            newConfig.locale = myLocale;
            Locale.setDefault(myLocale);
            getBaseContext().getResources().updateConfiguration(newConfig, getBaseContext().getResources().getDisplayMetrics());
        }
    }





    public String readImageFromFile(Bitmap bmp) {
        String resultText = null;
        try {
            String path = Environment.getExternalStorageDirectory().getAbsolutePath();
            Log.i("images path", path);

            Bitmap bMap = BitmapFactory.decodeFile(path + "/screen1.jpg");

            bMap = bmp;

            LuminanceSource source = new RGBLuminanceSource(bMap);
            BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));

            Reader reader = new MultiFormatReader();
            com.google.zxing.Result result;

            result = reader.decode(bitmap);
            resultText = result.getText();


        } catch (NotFoundException e) {
            e.printStackTrace();

        } catch (ChecksumException e) {
            e.printStackTrace();

        } catch (FormatException e) {
            e.printStackTrace();
        }

        return resultText;
    }

    public void getPictureFromGallery(int requestCode) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,
                "Select Picture"), requestCode);
    }

    void deleteSharedImage() {
        try {
            String imagePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/screen1.jpg";
            File imageFileToShare = new File(imagePath);
            imageFileToShare.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    String getUniversalCountryCode(String code) {
        String accuratecode = code;
        if (accuratecode.contains("+")) {
            accuratecode = accuratecode.replace("+", "00");
        }
        return accuratecode;
    }


    String removeSpecial(String aa) {

        String c = aa;
        Pattern pt = Pattern.compile("[^a-zA-Z0-9]");
        Matcher match = pt.matcher(c);
        while (match.find()) {
            String s = match.group();
            c = c.replaceAll("\\" + s, "");
        }
        System.out.println(c);
        return c;
    }

    String getFormattedMobileNO(CustomTextView tvCountryCode, String no) {
        String countryCode = tvCountryCode.getText().toString();
        countryCode = getUniversalCountryCode(countryCode);
        countryCode = removeSpecial(countryCode);
        if(countryCode.equals("0095"))
            no = no.substring(1);
        no = countryCode + no;
        return no;
    }

    @Override
    public <T> void callRequestAPIAppServer(String url, RequestParams mPrams, int address)
    {
        REQUEST_ADDRESSS = address;
        getAppServerAPI(url, mPrams);

    }

    private void showAlertOffline_(String message) {

        LayoutInflater li = LayoutInflater.from(BaseActivity.this);
        View promptsView = li.inflate(R.layout.alert_window, null);

        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(BaseActivity.this);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);
        CustomTextView title = (CustomTextView) promptsView.findViewById(R.id.title);
        final CustomTextView userInput = (CustomTextView) promptsView.findViewById(R.id.alert);
        userInput.setText(message);

        CustomButton ok = (CustomButton) promptsView.findViewById(R.id.btn_settings);
        if (message.contains("Successful")) {
            title.setText(getResources().getText(R.string.alert));
        }

        final AlertDialog dialog1 = alertDialogBuilder.create();
        ok.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog1.dismiss();
            }
        });
        dialog1.show();

    }


    int dp2px(int dp)
    {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }

    boolean checkphonenoLength(String phoneNo)
    {
        int length=phoneNo.length();

        if(length>=7&&length<=11)
            return true;
        else
            return false;
    }

    String getCountryCodeFromFormatted_MobileNo(String phoneno)
    {
        phoneno=phoneno.substring(0,4);

        if(phoneno.contains("00"))
            phoneno=phoneno.replace("00","+");

        return phoneno;
    }

    String getDisplayNumber(String phoneno)
    {
        phoneno=phoneno.substring(4,phoneno.length());

        phoneno="0"+phoneno;

        return phoneno;
    }

    public void callMapLatLong(String mobileNumber, String cell_id) {
        RequestParams params = new RequestParams();
        params.put(PHONE_NUMBER_LAT_LONG, mobileNumber);
        params.put(CELL_ID, cell_id);
        REQUEST_ADDRESSS = REQUEST_TYPE_MAP_LAT_LONG;
        getAppServerAPI(BASE_APP_URL + HTTP_LAT_LONG, params);
    }

    public boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                showToast(getString(R.string.no_play_store_msg));
                finish();
            }
            return false;
        } else {
            // showToast(R.string.play_store_msg);
        }
        return true;
    }
    public void changePasswordHeader() {///////// Change Password
        custom_action_bar_lay_back.setVisibility(View.VISIBLE);
        backButton.setVisibility(View.VISIBLE);
        mcustomHeaderBg.setBackgroundResource(R.drawable.header_bg2);
        mTitleOfHeader.setText(getResources().getText(R.string.checkPassword));
        buttonLogout.setVisibility(View.GONE);
        countDownTimer.cancel();
    }

    public void setErrorMsg(EditText et, String message) {
        et.setError(message);
    }

    public void resetTimer() {
        countDownTimer.cancel();
        countDownTimer.start();
    }

    public void setChangeLanguageHeader() {///////// Change Languagev
        custom_action_bar_lay_back.setVisibility(View.VISIBLE);
        backButton.setVisibility(View.VISIBLE);
        mcustomHeaderBg.setBackgroundResource(R.drawable.header_bg2);
        mTitleOfHeader.setText(getResources().getText(R.string.change_language));
        buttonLogout.setVisibility(View.GONE);
        countDownTimer.cancel();
    }

    public void showExitAlert(int msg, int header) {
        final Dialog dialog = new Dialog(this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dailog);
        CustomTextView userInput = (CustomTextView) dialog.findViewById(R.id.dialog_editext);
        CustomTextView title = (CustomTextView) dialog.findViewById(R.id.tv_title);
        CustomTextView tvOK = (CustomTextView) dialog.findViewById(R.id.tv_ok);
        CustomTextView tvCancel = (CustomTextView) dialog.findViewById(R.id.tv_cancel);
        tvCancel.setVisibility(View.GONE);
        userInput.setText(msg);
        title.setText(header);
        //tvOK.setText(yes);
        //tvCancel.setText(no);
        title.setTypeface(Utils.getFont(this), Typeface.BOLD);
        dialog.setCancelable(false);
        tvOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BaseActivity.this, NewStartupActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                BaseActivity.this.startActivity(intent);
                dialog.dismiss();
            }
        });
        dialog.show();
    }
//----------------



    /* public void setMaxLength(EditText editText, int maxLength) {
         InputFilter[] fArray = new InputFilter[1];
         fArray[0] = new InputFilter.LengthFilter(maxLength);
         editText.setFilters(fArray);
     }*/
    void showSelectionDilog()
    {
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setTitle(OPTIONS);
        builder.setCancelable(false);
        builder.setItems(STARR_OPTIONS, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {


                if (which == 1) {
                    pickFromGalleryandScan();
                    dialog.dismiss();
                } else {
                    qrCodeScanner();
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    void pickFromGalleryandScan()
    {
        getPictureFromGallery(SELECT_PICTURE_SC);
    }

    public void qrCodeScanner() {
        IntentIntegrator integrator_qr = new IntentIntegrator(this);
        integrator_qr.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
        integrator_qr.setPrompt(String.valueOf("Scan QR Code"));
        integrator_qr.addExtra("SCAN_WIDTH", 1800);
        integrator_qr.addExtra("SCAN_HEIGHT", 1880);
        integrator_qr.setResultDisplayDuration(0);
        integrator_qr.setCameraId(0);  // Use a specific camera of the device
        integrator_qr.initiateScan();
    }


    public Date convertStringToDate(String dateString)
    {
        String[] dateArr=dateString.split(" ");
        dateString=dateArr[0];
        Log.e("datestring...", dateString);

        Date date = null;

        DateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        try{
            date = df.parse(dateString);

        }
        catch ( Exception ex ){
            System.out.println(ex);
        }
        return date;
    }

    public boolean checkCurrentDayorNot(Date dt)
    {
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        Date dt_curr=getCurrentDate();
        //Date dt_curr=new Date();

        Log.e("dateis...",""+dt_curr);
        Log.e("modelate...",""+dt);
        // dt_curr=convertDateFormat(dt_curr);
        Log.e("dateaf...", "" + dt_curr);

        Calendar cal1=Calendar.getInstance();
        Calendar cal2=Calendar.getInstance();

        cal1.setTime(dt);
        cal2.setTime(dt_curr);

        boolean sameDay = cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);

        return sameDay;

    }

    public Date convertDateFormat(Date dt)
    {
        DateFormat originalFormat = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss");
        DateFormat targetFormat = new SimpleDateFormat("dd-MMM-yyyy");
        Date date = null;
        try {
            date = targetFormat.parse(""+new Date());
            Log.e("formatdate...",""+date);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    Date getCurrentDate()
    {
        Date d=null;
        SimpleDateFormat targetFormat = new SimpleDateFormat("dd-MM-yyyy");

        Calendar cal=Calendar.getInstance();
        int dd= cal.get(Calendar.DATE);
        String st_date=null;
        int mm=cal.get(Calendar.MONTH);
        mm=mm+1;
        String month=null;

        if(dd<10)
            st_date="0"+dd;
        else
            st_date=""+dd;

        if(mm<10)
            month="0"+mm;
        else
            month = "" + mm;

        String year = "" + cal.get(Calendar.YEAR);

        String date = st_date + "-" + month + "-" + year;


        try {
            d = targetFormat.parse(date);
            Log.e("newdate...",""+d);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return d;

    }
    public void finishActivity() {
        backButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                finish();
            }
        });
    }



    public void setMaxLength(EditText editText, int maxLength) {
        InputFilter filter = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {
                String blockedChars= "/%#&\\;<>?[]\"^_|~";
                for (int i = start; i < end; i++) {
                    String character = String.valueOf(source.charAt(i));
                    if (character.equalsIgnoreCase("/")|| character.equalsIgnoreCase("%") || character.equalsIgnoreCase("#") || character.equalsIgnoreCase("&") || character.equalsIgnoreCase("\\") || character.equalsIgnoreCase(";") || character.equalsIgnoreCase("<") ||character.equalsIgnoreCase(">") || character.equalsIgnoreCase("?") || character.equalsIgnoreCase("[") ||character.equalsIgnoreCase("]") || character.equalsIgnoreCase("\"") || character.equalsIgnoreCase("^") ||character.equalsIgnoreCase("_") || character.equalsIgnoreCase("|") || character.equalsIgnoreCase("~") || character.equalsIgnoreCase("@") || character.equalsIgnoreCase("-")) {
                        return "";
                    }
                }
                return null;
            }
        };

        InputFilter[] fArray = new InputFilter[2];
        fArray[0] = new InputFilter.LengthFilter(maxLength);
        fArray[1]= filter;
        editText.setFilters(fArray);
    }

    public void setEditTextFilterForNRC(EditText editText, int maxLength) {
        InputFilter filter = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {

                String blockedChars= "%#&\\;<>?[]\"^_|~";
                for (int i = start; i < end; i++) {
                    String character = String.valueOf(source.charAt(i));
                    if ( character.equalsIgnoreCase("%") || character.equalsIgnoreCase("#") || character.equalsIgnoreCase("&") || character.equalsIgnoreCase("\\") || character.equalsIgnoreCase(";") || character.equalsIgnoreCase("<") ||character.equalsIgnoreCase(">") || character.equalsIgnoreCase("?") || character.equalsIgnoreCase("[") ||character.equalsIgnoreCase("]") || character.equalsIgnoreCase("\"") || character.equalsIgnoreCase("^") ||character.equalsIgnoreCase("_") || character.equalsIgnoreCase("|") || character.equalsIgnoreCase("~")) {
                        return "";
                    }
                }
                return null;
            }
        };
        InputFilter[] fArray = new InputFilter[2];
        fArray[0] = new InputFilter.LengthFilter(maxLength);
        fArray[1]= filter;
        editText.setFilters(fArray);
    }

    public String[] getCountryCodeNameAndFlagFromNumberLogin(String no) {
        ArrayList<NetworkOperatorModel> listOfCountryCodesAndNames = new NetworkOperatorModel().getListOfNetworkOperatorModel(this);
        String countryCode = "", countryName = "", flagStr = "";
        int flag = -1;
        if (no.startsWith("+"))
            no = no.replace("+", "00");
        no = convertValidNumber(no);
        int id = -1;
        if (no.startsWith("+") || no.startsWith("00")) {
            String code = no.substring(0, 6);
            for (int i = 0; i < listOfCountryCodesAndNames.size(); i++) {

                ArrayList<String> dialingCodes = listOfCountryCodesAndNames.get(i).getListOfDialingCodes();
                if (dialingCodes != null) {
                    for (int j = 0; j < dialingCodes.size(); j++) {
                        String countryId = dialingCodes.get(j).replace("+", "00");

                        if (code.contains(countryId)) {
                            System.out.println(i);
                            id = i;
                            int length = countryId.length();
                            no = no.substring(3);
                            countryCode = listOfCountryCodesAndNames.get(id).getCountryCode();
                            countryName = listOfCountryCodesAndNames.get(id).getCountryName();
                            flag = listOfCountryCodesAndNames.get(i).getCountryImageId();
                            flagStr = String.valueOf(flag);
                            break;
                        }
                    }

                } else {

                    String countryId = "00" + listOfCountryCodesAndNames.get(i).getCountryCode().substring(1);
                    if (code.contains(countryId)) {
                        System.out.println(i);
                        id = i;
                        int length = countryId.length();
                        no = no.substring(length);
                        countryCode = listOfCountryCodesAndNames.get(id).getCountryCode();
                        countryName = listOfCountryCodesAndNames.get(id).getCountryName();
                        flag = listOfCountryCodesAndNames.get(i).getCountryImageId();
                        flagStr = String.valueOf(flag);
                        break;
                    }
                }

            }
            if (countryCode.equals("+95")) {
                no = "0" + no;
            }
        }

        if (id == -1) {
            if (no.startsWith("0")) {
                if (!AppPreference.getCountryCodeLogin(this).contains("+95"))
                    no = no.substring(1);
            } else {
                no = "0" + no;
            }
            countryCode = AppPreference.getCountryCodeLogin(this);
            flag = AppPreference.getCountryImageIDLogin(this);
            countryName = AppPreference.getCountryNameLogin(this);
            flagStr = String.valueOf(flag);
        }

        return new String[]{countryCode, countryName, flagStr, no};
    }

    public String[] getCountryCodeFlagNumberLogin(String countryCode, String no) {
        try {
            ArrayList<NetworkOperatorModel> listOfCountryCodesAndNames = listOfCountryCodesAndNames = new NetworkOperatorModel().getListOfNetworkOperatorModel(this);
            String countryName = "", flagStr = "";
            int flag = -1;
            no = convertValidNumberInRegisteration(no);
            int id = -1;

            if (no.startsWith("00") && countryCode.length() > 0) {
                String code = "00" + countryCode.substring(1);
                for (int i = 0; i < listOfCountryCodesAndNames.size(); i++) {
                    ArrayList<String> dialingCodes = listOfCountryCodesAndNames.get(i).getListOfDialingCodes();
                    if (dialingCodes != null) {
                        for (int j = 0; j < dialingCodes.size(); j++) {
                            String countryId = "00" + dialingCodes.get(j).substring(1);
                            if (no.length() >= 6) {
                                String code1 = no.substring(0, 6);
                                if (code1.startsWith(countryId)) {
                                    id = i;
                                    no = no.substring(code.length());
                                    countryCode = listOfCountryCodesAndNames.get(id).getCountryCode();
                                    countryName = listOfCountryCodesAndNames.get(id).getCountryName();
                                    flag = listOfCountryCodesAndNames.get(id).getCountryImageId();
                                    flagStr = String.valueOf(flag);
                                    break;
                                }
                            }
                        }
                    } else {

                        String countryId = "00" + listOfCountryCodesAndNames.get(i).getCountryCode().substring(1);
                        if (code.startsWith(countryId)) {
                            id = i;
                            no = no.substring(code.length());
                            countryCode = listOfCountryCodesAndNames.get(id).getCountryCode();
                            countryName = listOfCountryCodesAndNames.get(id).getCountryName();
                            flag = listOfCountryCodesAndNames.get(id).getCountryImageId();
                            flagStr = String.valueOf(flag);
                            break;
                        }
                    }
                }
                if (code.equals("0095")) {
                    no = "0" + no;
                }
            }

            if (id == -1) {
                if (no.startsWith("0")) {
                    if (!AppPreference.getCountryCodeLogin(this).equals("+95"))
                        no = no.substring(1);
                } /*else {
                    no = "0" + no;
                }*/
                countryCode = AppPreference.getCountryCodeLogin(this);
                flag = AppPreference.getCountryImageIDLogin(this);
                countryName = AppPreference.getCountryNameLogin(this);
                flagStr = String.valueOf(flag);
            }

            return new String[]{countryCode, countryName, flagStr, no};

        } catch (Exception e) {
            String countryName = "", flagStr = "";
            int flag = -1;
            countryCode = AppPreference.getCountryCodeLogin(this);
            flag = AppPreference.getCountryImageIDLogin(this);
            countryName = AppPreference.getCountryNameLogin(this);
            flagStr = String.valueOf(flag);
            return new String[]{countryCode, countryName, flagStr, ""};
        }
    }

    public  boolean isFrontCameraAvailable() {
        int cameraCount;
        boolean isFrontCameraAvailable = false;
        cameraCount = Camera.getNumberOfCameras();
        while (cameraCount > 0) {
            Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
            cameraCount--;
            Camera.getCameraInfo(cameraCount, cameraInfo);
            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                isFrontCameraAvailable = true;
                break;
            }
        }
        return isFrontCameraAvailable;
    }


public boolean bothDatesAreSame(Date dt_frm,Date dt_to)
{
    Calendar cal1=Calendar.getInstance();
    Calendar cal2=Calendar.getInstance();

    cal1.setTime(dt_frm);
    cal2.setTime(dt_to);

    boolean sameDay = cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
            cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);

    return sameDay;
}

  public  void readForJsonContacts() {
        ConstactModel model = null;
        Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
        Log.d("Contact new size", "" + phones.getCount());

        ArrayList<ConstactModel> list_jsonContacts=new ArrayList<ConstactModel>();
        while (phones.moveToNext())
        {
            String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            Log.e("dispname","..."+name);
            if (name != null)
            {
                String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                Log.e("dispno","..."+phoneNumber);

                String contactUri = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));

                String id = phones.getString(phones.getColumnIndex(ContactsContract.Contacts._ID));
                String email="";

                if (phoneNumber.contains(" "))
                    phoneNumber = phoneNumber.replace(" ", "");
                if (phoneNumber.contains("-"))
                    phoneNumber = phoneNumber.replace("-", "");
                if (contactUri == null)
                    contactUri = "";

                model=new ConstactModel(name,phoneNumber,email,contactUri,id,"false");
                Log.e("mPhno","...."+model.getMobileNo());
                list_jsonContacts.add(model);
            }

        }
        //Toast.makeText(this,"size is.."+list_jsonContacts.size(),Toast.LENGTH_SHORT).show();
        String jsonContacts = new Utils().convertArraylistToJsonString(list_jsonContacts);

        if(DBHelper.getJsonContacts()==null)
            DBHelper.insertJsonContact(jsonContacts);
        else {
            DBHelper.deleteJsonContacts();
            DBHelper.insertJsonContact(jsonContacts);
        }
        phones.close();

    }


}




