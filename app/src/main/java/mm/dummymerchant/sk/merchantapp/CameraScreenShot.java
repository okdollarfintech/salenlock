package mm.dummymerchant.sk.merchantapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.hardware.Camera;
import android.location.Location;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Calendar;
import java.util.List;

import mm.dummymerchant.sk.merchantapp.wirelessService.Test;


public class CameraScreenShot extends Activity implements View.OnClickListener {
    private static final int PICK_IMAGE_REQUEST = 1;
    private static final int CHOOSE_FILE_REQUESTCODE =2 ;
    private static final int VIDEO_CAPTURE = 3;
    private static final int VIDEO_FROM_GALLERY = 4;
    SurfaceView surfaceView;
    SurfaceHolder holder;
    Button takePicture,cancel,share_other_files,gallery,getUrl,video_capture,video_capture_from_gallery;
    static Camera camera;
    Camera.PictureCallback jpegCallback;
    String latlng;

    public  Bitmap rotate(Bitmap bitmap, int degree) {

       // Bitmap photo = Bitmap.createScaledBitmap(bitmap,surfaceView.getLayoutParams().width ,surfaceView.getLayoutParams().height, false);
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();
        Matrix mtx = new Matrix();
        //       mtx.postRotate(degree);
        mtx.setRotate(degree);
        return Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_camera_screen_shot);
        takePicture = (Button) findViewById(R.id.picturee);
        takePicture.setOnClickListener(this);
        cancel = (Button) findViewById(R.id.canc);
        cancel.setOnClickListener(this);
        share_other_files = (Button) findViewById(R.id.preview);
        share_other_files.setOnClickListener(this);
        gallery = (Button) findViewById(R.id.gallery);
        gallery.setOnClickListener(this);
        surfaceView = (SurfaceView) findViewById(R.id.surfacev);
        holder = surfaceView.getHolder();
        getUrl = (Button) findViewById(R.id.getUrl);
        getUrl.setOnClickListener(this);
        video_capture = (Button) findViewById(R.id.video_capture);
        video_capture.setOnClickListener(this);
        video_capture_from_gallery = (Button)findViewById(R.id.video_capture_from_gallery);
        video_capture_from_gallery.setOnClickListener(this);
        holder.addCallback(new SurfaceHolder.Callback() {
                               @Override
                               public void surfaceCreated(SurfaceHolder holder) {
                                   camera = Camera.open();
                                   Initilizer_parameter();
                                   try {
                                       camera.setDisplayOrientation(90);
                                       camera.setPreviewDisplay(holder);
                                       camera.startPreview();
                                   } catch (Exception e) {
                                       return;
                                   }
                               }
                               @Override
                               public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
                                   refreshCamera();
                               }

                               @Override
                               public void surfaceDestroyed(SurfaceHolder holder) {
                                   try {
                                       camera.stopPreview();
                                       camera.release();
                                       camera = null;
                                   }catch (Exception e){}
                               }

                           }


        );

        jpegCallback = new Camera.PictureCallback() {
            @Override
            public void onPictureTaken(byte[] data, Camera camera) {
                latlng = getLocationFromNetwork();
                photo_Saved(data);
            }
        };
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri uri = data.getData();
            try {
                File f = new File(Environment.getExternalStorageDirectory(), "picture.jpg");
                if(f.exists())
                {f.delete();}
                FileOutputStream fileOutputStream = new FileOutputStream(f.getAbsolutePath().toString());
                InputStream imageStream = getContentResolver().openInputStream(uri);
                Bitmap yourSelectedImage = BitmapFactory.decodeStream(imageStream);
                yourSelectedImage = getResizedBitmap(yourSelectedImage,250,350);
                Bitmap workingBitmap = Bitmap.createBitmap(yourSelectedImage);

                Bitmap mutableBitmap = workingBitmap.copy(Bitmap.Config.ARGB_8888, true);
                Canvas canvas = new Canvas(mutableBitmap);
                Paint paint = new Paint();
                paint.setTextSize(12f);
                paint.setColor(Color.RED); // Text Color
                paint.setStrokeWidth(12); // Text Size
                paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER)); // Text Overlapping Pattern

                Calendar calender = Calendar.getInstance();
                canvas.drawText("Date : "+calender.getTime().toGMTString(), 10, 100, paint);
                canvas.drawText("Location : "+latlng,10, 150, paint);
                mutableBitmap.compress(Bitmap.CompressFormat.JPEG, 50, fileOutputStream);
                fileOutputStream.close();
                Intent preview = new Intent(this ,Preview.class);
                preview.putExtra("url",f.getAbsolutePath());
                startActivity(preview);
                if(camera!=null)
                {


                }
                try {
                    System.gc();
                }catch (Exception e)
                {

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }else if(requestCode == CHOOSE_FILE_REQUESTCODE )
        {
            try{
            Uri uri = data.getData();
            Intent share = new Intent(Intent.ACTION_SEND);
            // Type of file to share
            share.setType("file/");
            // Compress into png format image from 0% - 100%// Locate the image to Share
            share.putExtra(Intent.EXTRA_STREAM, uri);
            // Show the social share chooser list
            startActivity(Intent.createChooser(share, "Share Content"));
        }catch (Exception e){}

        }else if(requestCode==VIDEO_CAPTURE)
        {
            if (resultCode == RESULT_OK) {
                if(data!=null) {
                    Uri uri = data.getData();
                    Intent intent = new Intent(CameraScreenShot.this, Test.class);
                    intent.putExtra("uri", uri.toString());
                    startActivity(intent);
                    finish();
                }else
                {
                    Toast.makeText(this, "Failed to record video",
                            Toast.LENGTH_LONG).show();
                }

            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "Video recording cancelled.",
                        Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "Failed to record video",
                        Toast.LENGTH_LONG).show();
            }


        }else if(requestCode == VIDEO_FROM_GALLERY)
        {
            if(data!=null) {
                Uri uri = data.getData();
                Intent intent = new Intent(CameraScreenShot.this, Test.class);
                intent.putExtra("uri", uri.toString());
                startActivity(intent);
                finish();
            }else
            {
                Toast.makeText(this, "Something went wrong!",
                        Toast.LENGTH_LONG).show();
            }

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
       latlng =getLocationFromNetwork();

    }

    public void photo_Saved(final byte[] data)
    {
        new Thread(new Runnable() {
            @Override
            public void run() {
                FileOutputStream fileOutputStream;
                try {
                    File f = new File(Environment.getExternalStorageDirectory(), "picture.jpg");
                    fileOutputStream = new FileOutputStream(f.getAbsolutePath().toString());

                    try {
                        Bitmap realImage = BitmapFactory.decodeByteArray(data, 0, data.length);
                        realImage = getResizedBitmap(realImage,250,350);
                        ExifInterface exif = new ExifInterface(f.toString());
                        Log.d("EXIF value", exif.getAttribute(ExifInterface.TAG_ORIENTATION));
                        if (exif.getAttribute(ExifInterface.TAG_ORIENTATION).equalsIgnoreCase("6")) {
                            realImage = rotate(realImage, 90);
                        } else if (exif.getAttribute(ExifInterface.TAG_ORIENTATION).equalsIgnoreCase("8")) {
                            realImage = rotate(realImage, 270);
                        } else if (exif.getAttribute(ExifInterface.TAG_ORIENTATION).equalsIgnoreCase("3")) {
                            realImage = rotate(realImage, 180);
                        } else if (exif.getAttribute(ExifInterface.TAG_ORIENTATION).equalsIgnoreCase("0")) {
                            realImage = rotate(realImage, 90);
                        }
                       // realImage.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
                        Canvas canvas = new Canvas(realImage);
                        Paint paint = new Paint();
                        paint.setTextSize(12f);
                        paint.setColor(Color.RED); // Text Color
                        paint.setStrokeWidth(12); // Text Size
                        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER)); // Text Overlapping Pattern
                        // some more settings...
                        //canvas.drawBitmap(realImage, 0, 40, paint);
                        Calendar calender = Calendar.getInstance();
                        canvas.drawText("Date : "+calender.getTime().toGMTString(), 10, 50, paint);
                        canvas.drawText("Location : "+latlng,10, 100, paint);
                        // NEWLY ADDED CODE ENDS HERE ]
                        realImage.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
                        fileOutputStream.close();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(CameraScreenShot.this, "Picture Saved Successfully", Toast.LENGTH_LONG).show();
                                refreshCamera();
                            }
                        });

                        try {
                            Intent preview = new Intent(CameraScreenShot.this ,Preview.class);
                            preview.putExtra("url",f.getAbsoluteFile());
                            startActivity(preview);


                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    } catch (Exception e) {
                    }

                } catch (Exception e) {
                }
            }
        }).start();



    }

    public Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth)
    {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // create a matrix for the manipulation
        Matrix matrix = new Matrix();
        // resize the bit map
        matrix.postScale(scaleWidth, scaleHeight);
        // recreate the new Bitmap
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
        return resizedBitmap;
    }
    public void Initilizer_parameter() {
        Camera.Parameters parameters;
        parameters = camera.getParameters();
        Camera.Size size =  getOptimalPreviewSize(camera.getParameters().getSupportedPreviewSizes(),surfaceView.getLayoutParams().width,surfaceView.getLayoutParams().height);
        List<String> focusModes = parameters.getSupportedFocusModes();
        if (focusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        } else if (focusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
        }
        parameters.setPreviewSize(size.width, size.height);
        camera.setParameters(parameters);
        try {
            camera.autoFocus(new Camera.AutoFocusCallback() {
                @Override
                public void onAutoFocus(boolean success, Camera camera) {
                }
            });
        }catch (Exception e){

        }

    }

    public void refreshCamera() {
        if (holder.getSurface() == null) {
            return;
        }
        try {
            camera.stopPreview();
            camera.setDisplayOrientation(90);
            Initilizer_parameter();
            camera.setPreviewDisplay(holder);
            camera.startPreview();
        } catch (Exception e) {
        }

    }

    public String getLocationFromNetwork() {
        try {
            GPSTracker gpsTracker = new GPSTracker(this);
            if (gpsTracker.gpsStatus()) {
                String Latitude = String.valueOf(gpsTracker.getLatitude());
                String Longitude = String.valueOf(gpsTracker.getLongitude());
                if (Latitude.equals("0.0") || (Longitude.equals("0.0"))) {
                    // showToast((R.string.gps_msg));
                } else {
                    return Latitude + "," + Longitude;
                }
            } else {
                LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
                boolean network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                if (network_enabled) {
                    Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    if (location != null)
                        return location.getLatitude() + "," + location.getLongitude();
                }
            }
            return "0.0,0.0";
        }catch (Exception e){
            return "0.0,0.0";
        }
    }

    public void cameraTake() {
        camera.takePicture(null, null, jpegCallback);
    }

    private Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int w, int h) {
        final double ASPECT_TOLERANCE = 0.1;
        double targetRatio = (double) h / w;

        if (sizes == null)
            return null;

        Camera.Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;

        int targetHeight = h;

        for (Camera.Size size : sizes) {
            double ratio = (double) size.height / size.width;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE)
                continue;

            if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }
        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Camera.Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }

        return optimalSize;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.picturee:
                cameraTake();
                break;
            case R.id.canc:
                finish();
                break;
            case R.id.preview:
                Intent intent1 = new Intent(Intent.ACTION_GET_CONTENT);
                intent1.setType("file/*");
                try {
                    startActivityForResult(intent1, CHOOSE_FILE_REQUESTCODE);
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getApplicationContext(), "No suitable File Manager was found.", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.gallery:
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
                break;

            case R.id.getUrl:
                Intent intent2 = new Intent(CameraScreenShot.this,Enter_URL.class);
                startActivity(intent2);
                break;

            case R.id.video_capture:
               try {
                   if (camera != null) {
                       camera.release();
                   }
               }catch (Exception e){}
                File mediaFile =
                        new File(Environment.getExternalStorageDirectory().getAbsolutePath()
                                + "/ok_video.mp4");
                try {
                   if (mediaFile.exists()) {
                       mediaFile.delete();
                       mediaFile.createNewFile();
                   }
               }catch (Exception e){}
                Intent video_intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                Uri videoUri = Uri.fromFile(mediaFile);
                video_intent.putExtra(MediaStore.EXTRA_OUTPUT, videoUri);
                startActivityForResult(video_intent, VIDEO_CAPTURE);
                break;
            case R.id.video_capture_from_gallery:


                Intent intent4 = new Intent();
                intent4.setType("video/*");
                intent4.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent4, "Select Video"), VIDEO_FROM_GALLERY);
                break;
        }

    }

}
