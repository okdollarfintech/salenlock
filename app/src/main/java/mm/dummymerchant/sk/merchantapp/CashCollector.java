package mm.dummymerchant.sk.merchantapp;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.TableRow;

import mm.dummymerchant.sk.merchantapp.db.DBHelper;

/**
 * Created by user on 11/11/2015.
 */
public class CashCollector extends BaseActivity implements View.OnClickListener{
    TableRow mCreateCashier, mExistingCashier,mDigitalreceiptrow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cash_collector);
        init();
        setExistingCashcollectorActionBar(String.valueOf(getResources().getText(R.string.cashcollector)));
    }

    @Override
    public <T> void response(int resultCode, T data) {

    }

    void init()
    {
        mCreateCashier = (TableRow) findViewById(R.id.createcahier);
        mExistingCashier = (TableRow) findViewById(R.id.existingcash);
        //mDigitalreceiptrow = (TableRow) findViewById(R.id.digitalreceiptrow);

        callAllListeners();
    }
    void callAllListeners()
    {
        mCreateCashier.setOnClickListener(this);
        mExistingCashier.setOnClickListener(this);
        //mDigitalreceiptrow.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        Intent intent;
        switch (v.getId()) {

            case R.id.createcahier:
                intent = new Intent(this, NewCashcollectorActivity.class);
                startActivity(intent);
                break;

            case R.id.existingcash:

                DBHelper dbb=new DBHelper(this);
                dbb.open();
                Cursor c = dbb.Get_CASHCOLLECTOR_TABLE();
                if(c!=null)
                {
                    if(c.getCount()>0)
                    {
                        intent =new Intent(this,ExistingCashcollector.class);
                        intent.putExtra("From","CashCollector");
                        startActivity(intent);

                    }
                    else {
                        showToast(R.string.no_record);
                        c.close();
                    }
                }

                else {
                    c.close();
                    showToast(R.string.no_record);
                }

              dbb.close();
                break;

           /* case R.id.digitalreceiptrow:
                intent =new Intent(this,DigitalReceiptActivity.class);
                startActivity(intent);
                break;*/
        }
    }
}
