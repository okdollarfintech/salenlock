package mm.dummymerchant.sk.merchantapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TableRow;

import java.util.ArrayList;

import mm.dummymerchant.sk.merchantapp.db.DBHelper;
import mm.dummymerchant.sk.merchantapp.model.CashierModel;

/**
 * Created by Dell on 12/17/2015.
 */
public class CashierMenuActivity extends BaseActivity implements View.OnClickListener
{
    TableRow mCreateCashier;
    TableRow mViewCashier;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cashier_row_activity);
        init();
        setCashierMenuheader();


    }

    void init()
    {
        mCreateCashier=(TableRow)findViewById(R.id.createCashier);
        mViewCashier=(TableRow)findViewById(R.id.viewcashier);

        mViewCashier.setOnClickListener(this);
        mCreateCashier.setOnClickListener(this);
    }

    @Override
    public <T> void response(int resultCode, T data)
    {

    }

    @Override
    public void onClick(View view)
    {
        Intent intent;
        switch (view.getId())
        {
            case R.id.createCashier:
                intent=new Intent(this,CreateCashierActivity.class);
                startActivity(intent);
                break;

            case R.id.viewcashier:
               DBHelper dbb=new DBHelper(this);
            dbb.open();
            ArrayList<CashierModel> al_cashiers=dbb.getCashierDetails();
            dbb.close();
            if(al_cashiers!=null)
            {
                if(al_cashiers.size()>0)
                {
                    intent = new Intent(this, ViewCashierActivity.class);
                    startActivity(intent);
                }
                else
                {
                    callCashierNotcreatedToastMsg();
                }
            }
            else
                callCashierNotcreatedToastMsg();

            default:
                break;

        }
    }

    public void callCashierNotcreatedToastMsg()
    {
        showToast(getString(R.string.cashier_not_created));
    }
}
