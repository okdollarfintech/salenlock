package mm.dummymerchant.sk.merchantapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TableRow;

/**
 * Created by user on 11/28/2015.
 */
public class Cashout extends BaseActivity implements View.OnClickListener{

TableRow mCreatecashout, mViewcashout;

@Override
protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cash_out);
        init();
        setExistingCashcollectorActionBar(String.valueOf(getResources().getText(R.string.cashout)));
}

@Override
public <T> void response(int resultCode, T data) {

        }

        void init()
        {
                mCreatecashout = (TableRow) findViewById(R.id.createcashout);
                mViewcashout = (TableRow) findViewById(R.id.viewcashout);
        callAllListeners();
        }
        void callAllListeners()
        {
                mCreatecashout.setOnClickListener(this);
                mViewcashout.setOnClickListener(this);
        }

@Override
public void onClick(View v) {

        Intent intent;
        switch (v.getId()) {

        case R.id.createcashout:
        intent = new Intent(this,CreateCashOutActivity.class);
        startActivity(intent);
        break;

        case R.id.viewcashout:
        intent =new Intent(this,ViewCashOutActivity.class);
        intent.putExtra("From","CashCollector");
        startActivity(intent);
        break;
        }
        }
        }
