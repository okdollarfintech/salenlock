package mm.dummymerchant.sk.merchantapp;

/**
 * Created by user on 2/17/2016.
 */

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RadioButton;

import org.json.JSONException;
import org.json.JSONObject;

import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.Constant;
import mm.dummymerchant.sk.merchantapp.Utils.Utils;
import mm.dummymerchant.sk.merchantapp.customView.CustomButton;
import mm.dummymerchant.sk.merchantapp.customView.CustomDialog;
import mm.dummymerchant.sk.merchantapp.customView.CustomEdittext;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
import mm.dummymerchant.sk.merchantapp.httpConnection.HttpNewRequest;
import mm.dummymerchant.sk.merchantapp.model.ChangeLangModel;

public class ChangeLanguageActivity extends BaseActivity implements OnClickListener, CustomDialog.DialogListener, HttpNewRequest.LoginApiListener {
    CustomTextView cstextview;
    CustomEdittext edt_passwd;
    CustomButton btn_submit;
    String langItem = "";
    String selectedLang = "";
    RadioButton rbEnglish, rbBurmese;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_language);
        setChangeLanguageHeader();
        init();
        finishActivityApp();
        buttonLogout.setVisibility(View.INVISIBLE);
    }

    private void finishActivityApp() {
        backButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }


    @Override
    public <T> void response(int resultCode, T data) {

    }


    protected void init() {
        cstextview = (CustomTextView) findViewById(R.id.textview_lang);
        edt_passwd = (CustomEdittext) findViewById(R.id.edt_passwd);
        btn_submit = (CustomButton) findViewById(R.id.change_language_button_subit);
        rbEnglish = (RadioButton) findViewById(R.id.change_language_radio_english);
        rbBurmese = (RadioButton) findViewById(R.id.change_language_radio_Burmese);
        if(AppPreference.getLanguage(ChangeLanguageActivity.this).equals("my")) {
            rbBurmese.setTypeface(Utils.getFont(ChangeLanguageActivity.this));
        }
        rbBurmese.setOnClickListener(this);
        rbEnglish.setOnClickListener(this);
        btn_submit.setOnClickListener(this);
        defautLanguage();
    }

    private void defautLanguage()
    {
        if(AppPreference.getLanguage(this).equals("en")) {
            langItem=selectedLang="EN";
            rbEnglish.setChecked(true);
        }
        else {
            langItem =selectedLang="MYA";
            rbBurmese.setChecked(true);
        }
    }


    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.change_language_button_subit:
                hideKeyboard();
                if(edt_passwd.getText().toString().equals("") || edt_passwd.getText().toString().equals(null)){
                    showAlert(getResources().getString(R.string.please_enter_password));
                    edt_passwd.setError(getResources().getString(R.string.please_enter_password));
                }else if (langItem.equals(selectedLang)){
                    showAlert(getResources().getString(R.string.language_already_selected));
                }else if (!Utils.isEmpty(edt_passwd.getText().toString()) && !Utils.isEmpty(langItem)) {
                    ChangeLangModel model= new ChangeLangModel();
                    model.setMobileNumber(AppPreference.getMyMobileNo(this));
                    model.setSecureToken(AppPreference.getAuthTokem(this));
                    model.setSimId(Utils.simSerialNumber(this));
                    model.setPassword(edt_passwd.getText().toString());
                    model.setLanguage(langItem);
                    new HttpNewRequest(model, this,this, Constant.REQUEST_TYPE_LANGUAGE).start();
                }
                break;
            case R.id.change_language_radio_Burmese:
                rbEnglish.setSelected(false);
                rbBurmese.setSelected(true);
                langItem = "MYA";
                break;
            case R.id.change_language_radio_english:
                rbEnglish.setSelected(true);
                rbBurmese.setSelected(false);
                langItem = "EN";
                break;

            default:
                break;
        }

    }

    @Override
    public void yesClcked() {
        Intent i = getPackageManager().getLaunchIntentForPackage(getPackageName());
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }

    @Override
    public void responseListener(String res, int resultCode) {

        try {
            JSONObject object = new JSONObject(res);
            int code= object.getInt("Code");
            String msg= object.getString("Msg");
            System.out.println("Msg " + msg);
            if (code==200){
                showExitAlert(R.string.Yourlanguagechange, R.string.lang_changed_title);
                if(langItem.equals("MYA"))
                    AppPreference.setLanguage(this, "my");
                else
                    AppPreference.setLanguage(this, "en");
            }else {
                showToast(msg);
            }
        }catch (JSONException e){

        }
    }
}
