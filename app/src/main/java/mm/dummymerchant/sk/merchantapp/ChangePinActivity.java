package mm.dummymerchant.sk.merchantapp;

/**
 * Created by user on 2/17/2016.
 */

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.PasswordTransformationMethod;
import android.text.method.SingleLineTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;

import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.Utils;
import mm.dummymerchant.sk.merchantapp.customView.CustomButton;
import mm.dummymerchant.sk.merchantapp.customView.CustomEdittext;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
import mm.dummymerchant.sk.merchantapp.httpConnection.CallingApIS;


public class ChangePinActivity extends BaseActivity implements OnClickListener {
    CustomEdittext etOldPas, etNewPas, etConfirmPas;
    CustomButton btnSubmit;
    CheckBox mShowPassword;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_password);
        changePasswordHeader();
        init();
        buttonLogout.setVisibility(View.INVISIBLE);
        backButtonPress();
    }
    private void backButtonPress()
    {
        backButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();;
            }
        });
    }
    @Override
    public <T> void response(int resultCode, T data)
    {
        if(resultCode==REQUEST_TYPE_CHANGE_PIN)
        {
            showToast(R.string.password_success);
            cleartext();
        }
    }
    protected void init() {
        etOldPas = (CustomEdittext) findViewById(R.id.change_password_edittext_oldPassword);
        etNewPas = (CustomEdittext) findViewById(R.id.change_password_edittext_newPassword);
        etConfirmPas = (CustomEdittext) findViewById(R.id.change_password_edittext_confirmPasword);
        btnSubmit = (CustomButton) findViewById(R.id.change_password_button_subit);
        mShowPassword = (CheckBox) findViewById(R.id.chane_password_radio_button_);
        ((CustomTextView) findViewById(R.id.change_password_textview_title_)).setText(getResources().getString(R.string.checkPassword));
        btnSubmit.setOnClickListener(this);
        mShowPassword.setOnClickListener(this);
        mShowPassword.setTypeface(Utils.getFont(this));
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.change_password_button_subit:
                if(Utils.isConnectedToInternet(this)) {
                    if (!Utils.isEmpty(etOldPas.getText().toString().trim()) && !Utils.isEmpty(etNewPas.getText().toString().trim()) && !Utils.isEmpty(etConfirmPas.getText().toString().trim())) {
                        hideKeyboard();
                        if (etOldPas.getText().toString().trim().length() > 5){
                            if( etNewPas.getText().toString().trim().length() > 5 && etConfirmPas.getText().toString().trim().length() > 5)
                            {
                                if (!etOldPas.getText().toString().trim().equals(etNewPas.getText().toString().trim())) {
                                    if (etConfirmPas.getText().toString().trim().equals(etNewPas.getText().toString().trim())) {
                                        CallingApIS api = new CallingApIS(this, this);
                                        api.CallChangePinAPI(etOldPas.getText().toString().trim(), etNewPas.getText().toString().trim());
                                    } else {
                                        showToast(getResources().getString(R.string.password_not_Matched));
                                        etNewPas.setError(getResources().getString(R.string.password_not_Matched));
                                    }
                                } else {
                                    showToast(getResources().getString(R.string.password_new_password));
                                }
                            }else {
                                showToast(getResources().getString(R.string.password_length_check));
                                etConfirmPas.setError(getResources().getString(R.string.password_length_check));
                            }
                        } else {
                            showToast(getString(R.string.password_length_check));
                        }
                    } else {
                        showToast(getResources().getString(R.string.password_not_Matched));
                        if (Utils.isEmpty(etOldPas.getText().toString().trim())) {
                            //             etOldPas.setError(setErrorMsg(getString(R.string.password_length_check)));
                            setErrorMsg(etOldPas,getString(R.string.password_length_check));
                        }
                        if (Utils.isEmpty(etNewPas.getText().toString().trim())) {
                            SpannableString span1 = new SpannableString(getResources().getString(R.string.password_length_check));
                            etNewPas.setError(span1);
                        }
                        if (Utils.isEmpty(etConfirmPas.getText().toString().trim())) {
                            SpannableString span1 = new SpannableString(getResources().getString(R.string.password_length_check));
                            etNewPas.setError(span1);
                        }
                    }
                }
                else {
                    showAlert(getResources().getString(R.string.async_task_no_internet));
                }
                break;

            case R.id.chane_password_radio_button_:
                if (mShowPassword.isChecked()) {
                    etOldPas.setTransformationMethod(SingleLineTransformationMethod.getInstance());
                    etConfirmPas.setTransformationMethod(SingleLineTransformationMethod.getInstance());
                    etNewPas.setTransformationMethod(SingleLineTransformationMethod.getInstance());
                } else {
                    etOldPas.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    etConfirmPas.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    etNewPas.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
                break;

        }


    }
    private void cleartext() {
        etConfirmPas.setText("");
        etOldPas.setText("");
        etNewPas.setText("");
        AppPreference.setPasword(this, etNewPas.getText().toString());
        Intent i = new Intent(this, NewStartupActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }
    private void showMessage() {
        LayoutInflater li = LayoutInflater.from(ChangePinActivity.this);
        View promptsView = li.inflate(R.layout.alert_window, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ChangePinActivity.this);
        alertDialogBuilder.setView(promptsView);
        final  CustomTextView title=(CustomTextView)promptsView.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.msg));
        final CustomTextView userInput = (CustomTextView) promptsView.findViewById(R.id.alert);
        userInput.setText(getString(R.string.password_success));
        CustomButton ok = (CustomButton) promptsView.findViewById(R.id.btn_settings);
        final AlertDialog dialog1 = alertDialogBuilder.create();
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog1.dismiss();
                cleartext();
            }
        });
        dialog1.show();

    }
}

