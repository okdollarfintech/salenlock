package mm.dummymerchant.sk.merchantapp;

/**
 * Created by user on 7/17/2015.
 */

import android.app.AlarmManager;
import android.app.Application;
import android.app.DownloadManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import android.widget.Toast;
import java.util.ArrayList;



import mm.dummymerchant.sk.merchantapp.CrashReport.CrashErrorReport;
import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.Constant;
import mm.dummymerchant.sk.merchantapp.Utils.MySendSMS;
import mm.dummymerchant.sk.merchantapp.Utils.MySendSMSStatus;
import mm.dummymerchant.sk.merchantapp.Utils.Utils;
import mm.dummymerchant.sk.merchantapp.db.DBHelper;
import mm.dummymerchant.sk.merchantapp.httpConnection.NewAsycnTask;
import mm.dummymerchant.sk.merchantapp.model.TransationModel;
import mm.dummymerchant.sk.merchantapp.model.UserInctivityModel;
import mm.dummymerchant.sk.merchantapp.receiver.BrodCastServiceStarter;
import mm.dummymerchant.sk.merchantapp.receiver.UserInactiveService;


public class CrashActivity extends Application implements Constant,MySendSMSStatus {


    public static PendingIntent pendingIntent11, pendingIntent;
    public static AlarmManager manager1, manager11;
    public static Intent alarmIntent1;
    public static ArrayList<TransationModel> queue;
    private static DBHelper db;
    private static String boundary = "SwA" + Long.toString(System.currentTimeMillis()) + "SwA";
    private static int REQUEST_ADDRESSS = 01234567;
    private static NewAsycnTask as;
    private static DownloadManager downloadManager;
    private boolean inForeground;
    private static boolean downloading = false;
    private static CrashActivity activity;
    private Intent alarmIntent11;
    public static String version;
    private static String Url;
    private static String Type;

    private long lastInteraction;
    private Boolean isScreenOff = false;
    boolean checkPresence = false;

    public static DBHelper getDb() {
        return db;
    }

    int startTime=10000;
    int interval=1000;
 //  public static CountDownTimer_userInactive timer;


    @Override
    public void onCreate() {
        super.onCreate();
        activity = this;
        queue = new ArrayList<TransationModel>();

        try {
            alarmIntent1 = new Intent(this, BrodCastServiceStarter.class);
            pendingIntent = PendingIntent.getBroadcast(this, 0, alarmIntent1, 0);
            manager1 = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            long interval = 10 * 1000l;
            manager1.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), interval, pendingIntent);
        } catch (Exception e) {
            Toast.makeText(this, getResources().getString(R.string.error_start_service), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }


        try {
            db = new DBHelper(CrashActivity.this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        CrashErrorReport mCrashErrorReporter = CrashErrorReport.getInstance();
        mCrashErrorReporter.Init(getApplicationContext());
        mCrashErrorReporter.CheckCrashErrorAndSendMail(getApplicationContext());


       // startUserInactivityDetectThread(); // start the thread to detect inactivity

        new ScreenReceiver();
    }
    @Override
    public void onSent() {

    }

    @Override
    public void onError(String error)
    {

    }

    @Override
    public void onDelivered(String msg) {

    }

    private class ScreenReceiver extends BroadcastReceiver {

        protected ScreenReceiver() {
            // register receiver that handles screen on and screen off logic
            IntentFilter filter = new IntentFilter();
            filter.addAction(Intent.ACTION_SCREEN_ON);
            filter.addAction(Intent.ACTION_SCREEN_OFF);
            registerReceiver(this, filter);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF))
            {
                isScreenOff = true;
               startService(new Intent(getBaseContext(), UserInactiveService.class));
                Log.e("started", "...started");
            }
            else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON))
            {
                isScreenOff = false;
               try {
                   stopService(new Intent(getBaseContext(), UserInactiveService.class));
                   Log.e("stopped", "...stopped");
               }catch (Exception e)
               {
                   e.printStackTrace();
               }

            }
        }
    }

}
