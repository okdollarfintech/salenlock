package mm.dummymerchant.sk.merchantapp.CrashReport;

import android.os.AsyncTask;
import android.util.Log;

import mm.dummymerchant.sk.merchantapp.Utils.Constant;


/**
 * Created by user on 1/6/2016.
 */
public class SendEmailAsyncTask extends AsyncTask
{
    String ErrorContent;
    public SendEmailAsyncTask(String errorContent) {
        ErrorContent=errorContent;
    }

    @Override
    protected Object doInBackground(Object[] params) {
        try {
        GmailSender sender = new GmailSender(Constant.FROM_MAIL, Constant.MAIL_PWD);
        sender.sendMail("Safety Cashier Crash Report", ErrorContent + "\n",Constant.FROM_MAIL,Constant.TO_MAIL);

        } catch (Exception e) {
            Log.e("SendMail", e.getMessage(), e);
        }
        return null;
    }
}
