package mm.dummymerchant.sk.merchantapp;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import mm.dummymerchant.sk.merchantapp.customView.CustomEdittext;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
import mm.dummymerchant.sk.merchantapp.db.DBHelper;
import mm.dummymerchant.sk.merchantapp.model.CashOutModel;

/**
 * Created by user on 11/28/2015.
 */
public class CreateCashOutActivity extends BaseActivity implements View.OnClickListener{

    private CustomTextView edt_commision_value,text_final_amount;
    private CustomEdittext edt_customer_name,edt_customer_Amount,edt_radio_value;
    private DBHelper dbb;
    RadioGroup rg;
    RadioButton r1;
    private String commision,amt,radio_value,name;
    int pos;
    int pos1;
    int famt;
    private static final int CAMERA_REQUEST = 1888;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_create_cashout1);
        init();
        setNewCashcollectorActionBar(String.valueOf(getResources().getText(R.string.createcashout)));
        dbb = new DBHelper(this);
    }
    void init()
    {
        edt_customer_name= (CustomEdittext) findViewById(R.id.edt_customer_name);
        edt_customer_Amount= (CustomEdittext) findViewById(R.id.edt_cashier_Amount);
        edt_radio_value= (CustomEdittext) findViewById(R.id.edt_radio_value);
        rg = (RadioGroup) findViewById(R.id.radioGroup1);
        r1=(RadioButton) findViewById(R.id.radio0);
        edt_commision_value = (CustomTextView) findViewById(R.id.edt_commision_value);
        text_final_amount = (CustomTextView) findViewById(R.id.text_final_amount);
        commision = "Flat";
        buttonLogout.setOnClickListener(this);

        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // TODO Auto-generated method stub
                // Method 1 For Getting Index of RadioButton
                // Method 1
                pos = rg.indexOfChild(findViewById(checkedId));
                //Method 2
                pos1 = rg.indexOfChild(findViewById(rg.getCheckedRadioButtonId()));

                switch (pos) {
                    case 0:
                        r1.setChecked(true);
                        edt_radio_value.setHint(getResources().getText(R.string.flathint));
                        commision = "Flat";
                        edt_commision_value.setVisibility(View.INVISIBLE);
                        edt_radio_value.setText("");
                        break;
                    case 1:
                        edt_radio_value.setHint(getResources().getText(R.string.percenthint));
                        commision = "Percentage";
                        edt_commision_value.setVisibility(View.VISIBLE);
                        edt_radio_value.setText("");
                        break;
                    default:
                        edt_radio_value.setHint(getResources().getText(R.string.flathint));
                        commision = "Flat";
                        edt_commision_value.setVisibility(View.INVISIBLE);
                        edt_radio_value.setText("");
                        break;
                }
            }

        });


        edt_radio_value.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                amt = edt_customer_Amount.getText().toString();
                String per_value = edt_radio_value.getText().toString();
                int val = 0, val1 = 0;
                int i = 0;
                int edit_com_val = 0, edit_amt = 0;

                try {
                    val = Integer.parseInt(per_value);
                    val1 = Integer.parseInt(amt);

                } catch (NumberFormatException ex) {

                }
                if (amt.equals("")) {
                    String wrongText = getString(R.string.pls_entrblw_fields) + "\n";
                    if (amt.equalsIgnoreCase(""))
                        wrongText += getString(R.string.entr_amount) + "\n";

                    showToast(wrongText);

                } else {
                    String text = edt_radio_value.getText().toString().toLowerCase(Locale.getDefault());
                    Log.i("OnTextChanged", text);
                    if (commision.equals("Percentage")) {
                        System.out.println("Enter the if");
                        if (edt_radio_value.equals("")) {
                            // int i = 0;
                            edt_commision_value.setText(String.valueOf(i));
                            text_final_amount.setText("");
                        } else {
                            try {
                                int k = (int) ((Float.parseFloat(amt)) * (Float.parseFloat(text) / 100.0f));
                                edt_commision_value.setText(String.valueOf(k));
                                edit_com_val = Integer.parseInt(edt_commision_value.getText().toString());
                                edit_amt = Integer.parseInt(amt);
                                famt = (edit_amt) - (edit_com_val);
                                text_final_amount.setText("Final Amount : " + String.valueOf(famt));
                            } catch (NumberFormatException ex) {
                                edt_commision_value.setText(String.valueOf(i));
                                text_final_amount.setText("");
                            }
                        }
                        if (val > 14) {
                            edt_radio_value.setText("");
                            edt_commision_value.setText(String.valueOf(i));
                            text_final_amount.setText("");
                            String wrongText = getString(R.string.entr_blw_percent);
                            showToast(wrongText);
                        }
                    } else {
                        System.out.println("Enter the else");

                        try {
                            edit_com_val = Integer.parseInt(per_value);
                            edit_amt = Integer.parseInt(amt);
                            famt = (edit_amt) - (edit_com_val);
                            text_final_amount.setText(getString(R.string.final_amt) + String.valueOf(famt));
                        } catch (NumberFormatException ex) {
                            edt_commision_value.setText(String.valueOf(i));
                            text_final_amount.setText("");
                        }
                        if (val > val1) {
                            edt_radio_value.setText("");
                            edt_commision_value.setText(String.valueOf(i));
                            text_final_amount.setText("");
                            String wrongText = getString(R.string.entr_blw_cus_amount);
                            showToast(wrongText);
                        }
                    }
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });

        edt_customer_Amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {

                String text = edt_customer_Amount.getText().toString().toLowerCase(Locale.getDefault());
                Log.i("OnTextChanged", text);
                edt_radio_value.setText("");
                text_final_amount.setText("");
            }
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub
            }
            @Override
            public void afterTextChanged(Editable arg0) {
            }
        });
    }

    @Override
    public <T> void response(int resultCode, T data) {
    }

    @Override
    public void onClick(View v)
    {
        Intent intent;
        switch (v.getId())
        {
            case R.id.custom_actionbar_application_right_image:
                amt = edt_customer_Amount.getText().toString().trim();
                name = edt_customer_name.getText().toString().trim();
                radio_value = edt_radio_value.getText().toString().trim();
                edt_radio_value.getText().toString().trim();
                DateFormat targetFormat = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss");
                String Time=targetFormat.format(new Date());

                if(!amt.equalsIgnoreCase("") && !radio_value.equalsIgnoreCase("") && !name.equalsIgnoreCase("") && !amt.startsWith("0"))
                {
                    String per;
                    if (commision.equals("Percentage"))
                    {
                        per = edt_commision_value.getText().toString();
                    }
                    else {
                        per = edt_radio_value.getText().toString();
                    }
                    CashOutModel cashout_details = new CashOutModel(Time, name, amt,commision,per,String.valueOf(famt));
                    db.insertCashOutDetails(cashout_details);

                    edt_customer_name.setText("");
                    edt_customer_Amount.setText("0");
                    edt_radio_value.setText("");
                    text_final_amount.setText("");
                    edt_commision_value.setText("");
                    showToast1(getString(R.string.saved));
                    hideKeyboard();
                }
                else
                {
                    String wrongText=getString(R.string.pls_entrblw_fields) + "\n";
                    if(name.equalsIgnoreCase(""))
                        wrongText+=getString(R.string.entr_th_name) + "\n";
                    if(amt.startsWith("0"))
                        wrongText+=getString(R.string.entr_amount_invalid) + "\n";
                    if(amt.equalsIgnoreCase(""))
                        wrongText+=getString(R.string.entr_amount) + "\n";
                    if(radio_value.equalsIgnoreCase(""))
                        wrongText+=getString(R.string.flat_or_perentge) + "\n";

                    showToast(wrongText);
                    hideKeyboard();
                }

        }

    }
}
