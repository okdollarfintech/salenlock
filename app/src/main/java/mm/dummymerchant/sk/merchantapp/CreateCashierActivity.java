package mm.dummymerchant.sk.merchantapp;

import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.Utils;
import mm.dummymerchant.sk.merchantapp.Utils.WriteToExcel;
import mm.dummymerchant.sk.merchantapp.customView.CustomAutoCompleteTextView;
import mm.dummymerchant.sk.merchantapp.customView.CustomEdittext;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
import mm.dummymerchant.sk.merchantapp.model.CashierModel;

/**
 * Created by Dell on 11/3/2015.
 */
public class CreateCashierActivity extends BaseActivity
{
    CustomEdittext et_cashierName,et_CashierPassword,et_ConfirmPassword;
    ImageView imageView1;
    private static final int CAMERAA_REQUEST = 18888;
    Bitmap bmp=null;
    //new added
    private CustomAutoCompleteTextView edt_customer_mobile;
    private String numberForMatching = "", countryCode = "";
    private String _mCountryCOde = "";
    private String _mCountryName = "";
    private int _flag = -1;
    CustomTextView tvCountryCode,  tv_contacts,tvCountryCode_Confirmation;
    private boolean isConfirmationActivei = false;
    private boolean isManuallyEnterNumber = false;
    int flag;
    private CustomEdittext mConfirmMobileNumber;
    private LinearLayout mConfitmationField;
    Spinner spn_jobtype;
    final String[] starr_jobtype={"Desk Sale","Van Sale"};
    String st_jobType;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_create_cashier);

        init();
        st_jobType=starr_jobtype[0];

        setCreateCashierActionBar();

        buttonLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateAndInsert();
            }
        });


        spn_jobtype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                st_jobType=starr_jobtype[i];
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    void init()
    {
        et_cashierName=(CustomEdittext)findViewById(R.id.edt_cashier_name);
       // et_CashierNo=(CustomEdittext)findViewById(R.id.edt_cashier_no);
        et_CashierPassword=(CustomEdittext)findViewById(R.id.edt_permanentpassword);
        et_ConfirmPassword=(CustomEdittext)findViewById(R.id.edt_confirm_permanentpassword);
        imageView1=(ImageView)findViewById(R.id.imageView1);

        edt_customer_mobile=(CustomAutoCompleteTextView)findViewById(R.id.et_digital_phoneno);
        tvCountryCode = (CustomTextView) findViewById(R.id.tv_country_code);
        tv_contacts = (CustomTextView) findViewById(R.id.tv_digital_contacts);
        tvCountryCode_Confirmation = (CustomTextView) findViewById(R.id.tv_country_code_confirmaton_again);
        mConfitmationField = (LinearLayout) findViewById(R.id.send_money_confirmation_of_number);
        mConfirmMobileNumber = (CustomEdittext) findViewById(R.id.et_digital_confirm_phoneno);
        spn_jobtype=(Spinner)findViewById(R.id.spn_jobtype);

     //  findViewById(R.id.rel_createcashier_cashier_confirmpassword).setVisibility(View.GONE);

        edt_customer_mobile.setText("0");

        et_CashierPassword.addTextChangedListener(new MyTextWatcherForPassword());
        et_ConfirmPassword.addTextChangedListener(new MyTextWatcherForPassword());

        imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {   Intent   intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, CAMERA_FOR_BK_CREATECASHIER);
            }
        });


        numberValidation();
        countryCode = AppPreference.getCountryCode(this);
        tvCountryCode.setText("(" + AppPreference.getCountryCode(this) + ")");
        tvCountryCode_Confirmation.setText("(" + AppPreference.getCountryCode(this) + ")");
        if (AppPreference.getCountryImageID(this) != -1) {
           // mImgCountryCode.setBackgroundResource(AppPreference.getCountryImageID(this));
            flag = AppPreference.getCountryImageID(this);
        }
        if (!AppPreference.getCountryCode(this).equalsIgnoreCase(MYANMAR_COUNTRY_CODE)) {
            edt_customer_mobile.setText(null);
            edt_customer_mobile.setHint(getResources().getString(R.string.enterDummyMerchantNumber));
        } else {
            edt_customer_mobile.setText(Utils.getMyanmarNumber());
            edt_customer_mobile.setSelection(2);
        }
        ConfirmNumberValidation();
        //

        tv_contacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                cantactPickker();
            }
        });
        final ArrayAdapter<String> adaptr=new ArrayAdapter<String>(CreateCashierActivity.this,android.R.layout.simple_list_item_1,starr_jobtype);
        spn_jobtype.setAdapter(adaptr);

    }

    void validateAndInsert()
    {
        String cashierName=et_cashierName.getText().toString().trim();
        String cashierNo=edt_customer_mobile.getText().toString().trim();
        String password=et_CashierPassword.getText().toString().trim();
        String confirmPassword=et_ConfirmPassword.getText().toString().trim();
        Bitmap photo=((BitmapDrawable)imageView1.getDrawable()).getBitmap();

        if(!cashierName.equalsIgnoreCase("")&&!cashierNo.equalsIgnoreCase("")&&!password.equalsIgnoreCase("")&&!confirmPassword.equalsIgnoreCase("")&&cashierNo.length()>7&&bmp!=null)
        {
            if(password.length()<6)
            {
                showToast(R.string.password_length_check);
                return;
            }

            if(password.equalsIgnoreCase(confirmPassword))
            {
                DateFormat targetFormat = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss");
                String date=targetFormat.format(new Date());

               // int color = getRandomColor();
                int color=generateRandomColor();
                String colorcode = "" + color;

              //  cashierNo= Utils.getUniversalFormattedMobileNO(this,cashierNo);
                cashierNo=getFormattedMobileNO(tvCountryCode,cashierNo);
              bmp= ((BitmapDrawable)imageView1.getDrawable()).getBitmap();

          boolean status=   db.isItCashierNumberActive(cashierNo);

        if(!status)
        {
            showToast(cashierNo);
            CashierModel model = new CashierModel("", cashierName, cashierNo, password, confirmPassword, colorcode, date, bmp,CASHIER_ACTIVE,st_jobType);
            db.insertCashierDetails(model);
            WriteToExcel wf=new WriteToExcel();
            wf.convertToExcelFile(this);
            clearAllfields();
            showToast(CreateCashierActivity.this, getString(R.string.cashier_created));
            finish();
        }
           else
            showToast(getString(R.string.cash_name_already));

            }
            else
            {
                showToast(getString(R.string.pwdAndConfirmpwd));
            }
        }
        else
        {
              String wrongText="";
        if(cashierName.equalsIgnoreCase(""))
            wrongText+=getString(R.string.cashiername);
        if(cashierNo.equalsIgnoreCase(""))
        {
            wrongText += " "+getString(R.string.cashierno);
        }
        if(password.equalsIgnoreCase(""))
            wrongText+=" "+getString(R.string.pwd);

         if(bmp==null)
                wrongText+=" "+getString(R.string.photo);

        if(confirmPassword.equalsIgnoreCase(""))
            wrongText+=" "+getString(R.string.confirmpassword);
            showToast(wrongText+" "+getString(R.string.not_tobe_empty));
        }

        if(cashierNo.length()<7)
            showToast(getString(R.string.cashier_no_less_7));

    }

    @Override
    public <T> void response(int resultCode, T data) {

    }

    @Override
    public void onBackPressed() {
        finish();
    }

    int getRandomColor()
    {
        int color;
        Random rnd = new Random();
     color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));


        boolean checkcolor=db.checkColorExist("" + color);

        if(checkcolor)
        {
            color=getRandomColor();
        }

        else
        {
            return color;
        }

        return color;
    }

    public int generateRandomColor() {
        // This is the base color which will be mixed with the generated one
        final int baseColor = Color.WHITE;
        int outputColor;
        Random mRandom = new Random();

        final int baseRed = Color.red(baseColor);
        final int baseGreen = Color.green(baseColor);
        final int baseBlue = Color.blue(baseColor);

        final int red = (baseRed + mRandom.nextInt(256)) / 2;
        final int green = (baseGreen + mRandom.nextInt(256)) / 2;
        final int blue = (baseBlue + mRandom.nextInt(256)) / 2;

        outputColor=Color.rgb(red, green, blue);


        boolean checkcolor=db.checkColorExist("" + outputColor);

        if(checkcolor)
        {
            outputColor=generateRandomColor();

        }

        else
        {
            return outputColor;
        }

        return outputColor;
    }



    private class MyTextWatcherForPassword implements TextWatcher {

        public void afterTextChanged(Editable s) {

        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
           // findViewById(R.id.rel_createcashier_cashier_confirmpassword).setVisibility(View.VISIBLE);
            if (!et_CashierPassword.getText().toString().startsWith(s.toString()))
            {
                et_ConfirmPassword.setText(null);

                return;
            } else {

            }

        }
    }

    String getFormatteddate()
    {
        DateFormat targetFormat = new SimpleDateFormat("yyyyMMdd");
      String d1=  targetFormat.format(new Date());

        return d1;
    }
void clearAllfields()
{
    edt_customer_mobile.setText("0");
    et_cashierName.setText("");
    et_CashierPassword.setText("");
    et_ConfirmPassword.setText("");
    imageView1.setImageResource(R.drawable.user1);
    //findViewById(R.id.rel_createcashier_cashier_confirmpassword).setVisibility(View.GONE);
    bmp=null;

}

         protected void onActivityResult(int requestCode, int resultCode, Intent data)
             {

                 if (requestCode == CAMERA_FOR_BK_CREATECASHIER) {
                     if (data != null) {
                         try {
                             Bitmap photo = (Bitmap) data.getExtras().get("data");
                             bmp=photo;
                             imageView1.setImageBitmap(photo);
                         }catch (Exception e){}
                     }
                 }

                 if (requestCode == CALL_CONTACT_PICKER)
                 {
                     if (data != null) {
                         String name = data.getStringExtra(KEY_NAME);
                         String no = data.getStringExtra(KEY_NO);
                         numberSelectedValidation(name, no);
                         edt_customer_mobile.dismissDropDown();
                     }
                 }

    }

    private void cantactPickker() {

        Intent call = new Intent(this, NewContactPicker.class);
        Bundle b=new Bundle();
        b.putInt(CONTACT_PICKER_KEY,VALUE_CASHIERCREATE_CONTACT_PICKER);
        call.putExtras(b);
        startActivityForResult(call, CALL_CONTACT_PICKER);
    }

    private void numberSelectedValidation(String name, String number)
    {
        if (number.startsWith("+") || number.startsWith("00")) {
            String[] countryCodeArray = getCountryCodeNameAndFlagFromNumber(number);
            String scountryCode = countryCodeArray[0];
            String scountryName = countryCodeArray[1];
            int sflag = Integer.parseInt(countryCodeArray[2]);
            number = countryCodeArray[3];
            if (number.contains(" ")) {
                number = number.trim();
                number = number.replace(" ", "");
            }
            setNumberIntoTheField(name, scountryCode, sflag, number, scountryName);
            Log.e("SendMoney", "Number selected =" + countryCode);
        } else {

            //showToast("happens");
            String[] countryCodeArray = getCountryCodeNameAndFlagFromNumber(number);
            String scountryCode = countryCodeArray[0];
            String scountryName = countryCodeArray[1];
            int sflag = Integer.parseInt(countryCodeArray[2]);
            number = countryCodeArray[3];
            if (number.contains(" ")) {
                number = number.trim();
                number = number.replace(" ", "");
            }
            //TODO @_CountryCode
            Log.e("CountryCode", _mCountryCOde);
            String tet = AppPreference.getCountryCode(this);
            Log.e("lof", tet);

            if (!AppPreference.getCountryCode(this).equals(_mCountryCOde))
            {
            //    differentCountry(_mCountryCOde, _mCountryName, _flag, number, name);
                setNumberIntoTheField(name, scountryCode, sflag, number, scountryName);
                // showToast("executes");
            } else {
                setNumberIntoTheField(name, scountryCode, sflag, number, scountryName);
            }
        }
    }

    private void setNumberIntoTheField(String name, String countryCode, int flag, String number, String countryname)
    {
//
        tvCountryCode.setText("(" + countryCode + ")");
        //mCountryCode.setText(countryname + " (" + countryCode + ")");
        // tvCountryCode_Confirmation.setText("(" + countryCode + ")");
        isManuallyEnterNumber = true;
        textCount = 0;
        numberForMatching = number;
        tv_contacts.setText(name);
        et_cashierName.setText(name);

        if (countryCode.equalsIgnoreCase(MYANMAR_COUNTRY_CODE))
        {
            edt_customer_mobile.setText(getNumberWithGreyZero(number, true));
            edt_customer_mobile.setText(getNumberWithGreyZero(number, true));
            //edt_customer_name.setText(tv_contacts.getText());
            //showToast("flag.." + flag);
          //  mImgCountryCode.setBackgroundResource(flag);
            int length = edt_customer_mobile.getText().toString().length();
            edt_customer_mobile.setSelection(length);
            edt_customer_mobile.dismissDropDown();

            this.countryCode = countryCode;
            this.flag = flag;
        } else {
            String snumber = number;
            if (number.startsWith("0")) {
                snumber = number.substring(1);
                edt_customer_mobile.setText(snumber);
            }
            edt_customer_mobile.setText(getNumberWithGreyZero(number, true));
            // edt_customer_name.setText(tv_contacts.getText());
            //showToast( "flag.."+flag);
           // mImgCountryCode.setBackgroundResource(flag);
            int length = edt_customer_mobile.getText().toString().length();
            edt_customer_mobile.setSelection(length);
            edt_customer_mobile.dismissDropDown();
            this.countryCode = countryCode;
            this.flag = flag;
        }
    }

    private void differentCountry(final String countryCode, final String countryName, final int flag, final String number, final String name) {

        hideKeyboard();
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.select_number, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptsView);
        ImageView defaultImage = (ImageView) promptsView.findViewById(R.id.select_contact_defaultCountr_);
        ImageView manualImage = (ImageView) promptsView.findViewById(R.id.select_contact_changecountry);
        TextView defaultText = (TextView) promptsView.findViewById(R.id.select_number_default_country);
        TextView manually = (TextView) promptsView.findViewById(R.id.select_number_manual_country);
        defaultImage.setBackgroundResource(AppPreference.getCountryImageID(this));
        if (flag != -1) {
            manualImage.setBackgroundResource(flag);
        }
        final AlertDialog dialog1 = alertDialogBuilder.create();
        if (countryCode.equalsIgnoreCase(MYANMAR_COUNTRY_CODE))
            manually.setText("(" + countryCode + ")" + getNumberWithGreyZero(number, true));
        else
            verifyNumber(manually, number, countryCode);
        if (AppPreference.getCountryCode(this).equalsIgnoreCase(MYANMAR_COUNTRY_CODE))
            defaultText.setText("(" + AppPreference.getCountryCode(this) + ")" + getNumberWithGreyZero(number, true));
        else
            verifyNumber(defaultText, number, AppPreference.getCountryCode(this));
        defaultText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNumberIntoTheField(name, AppPreference.getCountryCode(CreateCashierActivity.this), AppPreference.getCountryImageID(CreateCashierActivity.this), number, AppPreference.getCountryName(CreateCashierActivity.this));
                _mCountryCOde = AppPreference.getCountryCode(CreateCashierActivity.this);
                edt_customer_mobile.dismissDropDown();
                dialog1.dismiss();
            }
        });
        manually.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNumberIntoTheField(name, countryCode, flag, number, countryName);
                dialog1.dismiss();
            }
        });
        dialog1.show();


    }
    private void verifyNumber(TextView tv, String number, String countryCode)
    {
        String snumber = number;
        if (number.startsWith("0")) {
            snumber = number.substring(1);
        }
        tv.setText("(" + countryCode + ")" + snumber);
    }

    private void numberValidation() {
        edt_customer_mobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().startsWith("00")) {
                    if (AppPreference.getCountryCode(CreateCashierActivity.this).equals(MYANMAR_COUNTRY_CODE)) {
                        edt_customer_mobile.setText("09");
                        edt_customer_mobile.setSelection(2);
                    } else {
                        edt_customer_mobile.setText(null);
                    }
                }
                if (TransactionType != 0 || isManuallyEnterNumber) {
                    isManuallyEnterNumber = true;
                    isConfirmationActivei = false;
                    mConfitmationField.setVisibility(View.GONE);
                } else {
                    if (s.length() > 5) {
                        isConfirmationActivei = true;
                        mConfitmationField.setVisibility(View.VISIBLE);
                        edt_customer_mobile.dismissDropDown();
                        //  edt_customer_name.setText(UNKNOWN);
                        TransactionType = 0;
                        // BLT_SEARCHING_TYPE = Type;

                        //------new code
                        try {
                            String number = edt_customer_mobile.getText().toString().trim();
                            String confirmNumber = mConfirmMobileNumber.getText().toString().trim();

                            if (number.length() < confirmNumber.length())
                                mConfirmMobileNumber.setText(null);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        //----------------

                    } else {
                        isConfirmationActivei = false;
                        mConfitmationField.setVisibility(View.GONE);
                    }
                }
                if (numberForMatching.length() != s.toString().length()) {
                    Drawable img = getResources().getDrawable(R.drawable.contact);
                    if (img != null) {
                        img.setBounds(0, 0, 80, 80);
                      // tvCountryCode.setCompoundDrawables(img, null, null, null);

                        tv_contacts.setText(getResources().getString(R.string.Unknown));
                       // et_cashierName.setText("");
                        isManuallyEnterNumber = false;
                    }
                }
                if (!isManuallyEnterNumber) {
                    if (textCount > 3) {
                        isManuallyEnterNumber = false;
                        TransactionType = 0;
                    }
                }
                textCount++;
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 6) {
                    String number = s.toString();
                    if (BLT_SEARCHING_TYPE.equals("9955") || BLT_SEARCHING_TYPE.equals("9900")) {
                        // BLT_SEARCHING_TYPE = Type;
                    }

                    if (number.startsWith("00")) {
                        String[] countryCodeArray = getCountryCodeNameAndFlagFromNumber(number);
                        String scountryCode = countryCodeArray[0];
                        String scountryName = countryCodeArray[1];
                        int sflag = Integer.parseInt(countryCodeArray[2]);
                        String no = countryCodeArray[3];
                        tvCountryCode.setText("(" + scountryCode + ")");
                     //   mImgCountryCode.setBackgroundResource(sflag);
                        flag = sflag;
                        numberForMatching = no;
                        //   mCountryCode.setText(scountryName + " (" + scountryCode + ")");
                        edt_customer_mobile.setText(no);
                        countryCode = scountryCode;
                        Log.e("SendMoney", countryCode + "and input counry code is " + scountryCode);
                    }
                }
            }
        });


    }


    private void ConfirmNumberValidation() {
        mConfirmMobileNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (isConfirmationActivei) {
                    if (!edt_customer_mobile.getText().toString().startsWith(s.toString())) {
                        mConfirmMobileNumber.setText(null);
                        return;
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }


        });
    }



}
