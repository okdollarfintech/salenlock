package mm.dummymerchant.sk.merchantapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Vibrator;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.Utils;
import mm.dummymerchant.sk.merchantapp.customView.CustomButton;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;


public class DealWithUploads extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();
        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)) {
                handleSendText(intent); // Handle text being sent
            }else if(type.startsWith("file/"))
            {
               final Uri imageUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
                final String url="http://media.api.okdollar.org/phones/"+ AppPreference.getMyMobileNo(this)+"/devices/"+ Utils.getDeviceIMEI(this)+"/files";
                final ProgressDialog progressDialog = new ProgressDialog(this);
                progressDialog.setCancelable(false);
                progressDialog.setMessage("Please wait!");
                progressDialog.show();

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        File f = new File(imageUri.getPath());
                        final String server_url= sendFileToServer(f.getAbsolutePath(), url);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if(progressDialog!=null)
                                {
                                    progressDialog.dismiss();
                                }
                                if(!server_url.equals("error"))
                                    showMessageSucces(server_url);
                                else
                                    showMessageSucces("Something went wrong!");
                            }
                        });


                    }
                }).start();

            }
        }

    }

    public String sendFileToServer(String filename, String targetUrl) {
        String response = "";
        Log.e("Image filename", filename);
        Log.e("url", targetUrl);
        HttpURLConnection connection = null;
        DataOutputStream outputStream = null;
        // DataInputStream inputStream = null;

        String pathToOurFile = filename;
        String urlServer = targetUrl;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        DateFormat df = new SimpleDateFormat("yyyy_MM_dd_HH:mm:ss");

        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024;
        try {
            FileInputStream fileInputStream = new FileInputStream(new File(
                    pathToOurFile));

            URL url = new URL(urlServer);
            connection = (HttpURLConnection) url.openConnection();

            // Allow Inputs & Outputs
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            connection.setChunkedStreamingMode(1024);
            // Enable POST method
            connection.setRequestMethod("POST");

            connection.setRequestProperty("Connection", "Keep-Alive");
            connection.setRequestProperty("Content-Type",
                    "multipart/form-data;boundary=" + boundary);

            outputStream = new DataOutputStream(connection.getOutputStream());
            outputStream.writeBytes(twoHyphens + boundary + lineEnd);

            String connstr = null;
            connstr = "Content-Disposition: form-data; name=\"uploadedfile\";filename=\""
                    + pathToOurFile + "\"" + lineEnd;
            Log.i("Connstr", connstr);

            outputStream.writeBytes(connstr);
            outputStream.writeBytes(lineEnd);

            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];

            // Read file
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            Log.e("Image length", bytesAvailable + "");
            try {
                while (bytesRead > 0) {
                    try {
                        outputStream.write(buffer, 0, bufferSize);
                    } catch (OutOfMemoryError e) {
                        e.printStackTrace();
                        response = "outofmemoryerror";
                        return response;
                    }
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                }
            } catch (Exception e) {
                e.printStackTrace();
                response = "error";
                return response;
            }
            outputStream.writeBytes(lineEnd);
            outputStream.writeBytes(twoHyphens + boundary + twoHyphens
                    + lineEnd);

            // Responses from the server (code and message)
            int serverResponseCode = connection.getResponseCode();
            String serverResponseMessage = connection.getResponseMessage();
            String urll = connection.getHeaderField("Location");
            Log.i("Server Response Code ", "" + serverResponseCode);
            Log.i("Server Response Code ", "" + serverResponseCode);
            Log.i("server_url", urll);
            if (serverResponseCode == 201) {
                response = urll;
            }
            String CDate = null;
            Date serverTime = new Date(connection.getDate());
            try {
                CDate = df.format(serverTime);
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("Date Exception", e.getMessage() + " Parse Exception");
            }
            Log.i("Server Response Time", CDate + "");

            filename = CDate
                    + filename.substring(filename.lastIndexOf("."),
                    filename.length());
            Log.i("File Name in Server : ", filename);

            fileInputStream.close();
            outputStream.flush();
            outputStream.close();
            outputStream = null;
        } catch (Exception ex) {
            // Exception handling
            response = "error";
            Log.e("Send file Exception", ex.getMessage() + "");
            ex.printStackTrace();
        }
        return response;
    }

   public void handleSendText(Intent intent) {
        String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (sharedText != null) {

        }
    }
    private void showMessageSucces(final String message) {
        MediaPlayer p = MediaPlayer.create(this, R.raw.success_tune);
        p.start();
        ((Vibrator) getSystemService(VIBRATOR_SERVICE)).vibrate(200);
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.alert_window, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptsView);
        final CustomTextView userInput = (CustomTextView) promptsView.findViewById(R.id.alert);
        userInput.setText("This is the Url : "+message);
        CustomButton ok = (CustomButton) promptsView.findViewById(R.id.btn_settings);
        final AlertDialog dialog1 = alertDialogBuilder.create();
        ok.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog1.dismiss();
               // AppPreference.setALLUrl(DealWithUploads.this,AppPreference.getALLUrl(DealWithUploads.this)+","+message);
                if(!AppPreference.getALLUrl(DealWithUploads.this).equals("")) {
                    AppPreference.setALLUrl(DealWithUploads.this, AppPreference.getALLUrl(DealWithUploads.this) + "$" + message);
                }else
                {
                    AppPreference.setALLUrl(DealWithUploads.this,message);
                }
                Intent intent = new Intent(DealWithUploads.this,Enter_URL.class);
                intent.putExtra("url_",message);
                startActivity(intent);
                finish();

            }
        });
        dialog1.show();
    }


}
