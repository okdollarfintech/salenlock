package mm.dummymerchant.sk.merchantapp;


import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;

import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.Constant;
import mm.dummymerchant.sk.merchantapp.Utils.Utils;
import mm.dummymerchant.sk.merchantapp.adapter.AutoCompleCustomAdapter;
import mm.dummymerchant.sk.merchantapp.customView.CustomAutoCompleteTextView;
import mm.dummymerchant.sk.merchantapp.customView.CustomEdittext;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
import mm.dummymerchant.sk.merchantapp.db.DBHelper;
import mm.dummymerchant.sk.merchantapp.model.ConstactModel;
import mm.dummymerchant.sk.merchantapp.model.NetworkOperatorModel;
import mm.dummymerchant.sk.merchantapp.model.PhoneContactModel;


/**
 * Created by user on 12/7/2015.
 */
public class DigitalPhoneNoActivity extends BaseActivity implements View.OnClickListener,Constant, AutoCompleCustomAdapter.AutoCompleteTextItemSelected{
    private boolean isConfirmationActivei = false;
    private boolean isManuallyEnterNumber = false;
    LinearLayout ll_selectCountryCode;
    CustomTextView tvCountryCode, mCountryCode, tv_contacts, mImgCountryCode, tvCountryCode_Confirmation;
    private LinearLayout mConfitmationField;
    private CustomEdittext mConfirmMobileNumber;
    private CustomAutoCompleteTextView edt_customer_mobile;

    NetworkOperatorModel model;
    int flag;
    private String numberForMatching = "", countryCode = "",countryName;
    private String _mCountryCOde = "";
    private String _mCountryName = "";
    private int _flag = -1;
    String Type;
    DBHelper dbb;
    int in_flag_image;

    CustomEdittext edt_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.digital_phone_noscreen);
       // setDigitalPhoneNoScreen(String.valueOf(getResources().getText(R.string.digi_title)));
        init();

        dbb = new DBHelper(this);
        _mCountryCOde = AppPreference.getCountryCode(this);
        String jsondata = db.getJsonContacts();
        Utils utils = new Utils();
        ArrayList<ConstactModel> al_contacts = utils.getContactListFromJsonString(jsondata);
        AutoCompleCustomAdapter adapter = new AutoCompleCustomAdapter(this, R.layout.custom_autocomple_layout_row, al_contacts, this);
        edt_customer_mobile.setAdapter(adapter);
        edt_customer_mobile.setThreshold(1);
        hideKeyboard();

        buttonLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validate();
            }
        });
    }
    void init()
    {
        if (Utils.getSimOperater(this) != null) {
            if (!Utils.getSimOperater(this).equals("")) {
                model = new NetworkOperatorModel().getNetworkOperatorModel(this, Utils.getSimOperater(this));
            }
        }

        edt_name=(CustomEdittext)findViewById(R.id.edt_digi_name);
        mConfitmationField = (LinearLayout) findViewById(R.id.send_money_confirmation_of_number);
        mConfirmMobileNumber = (CustomEdittext) findViewById(R.id.et_digital_confirm_phoneno);
        edt_customer_mobile= (CustomAutoCompleteTextView) findViewById(R.id.et_digital_phoneno);

        mImgCountryCode = (CustomTextView) findViewById(R.id.img_select_country_code);
        mCountryCode =(CustomTextView)findViewById(R.id.tv_digital_select_country_code);
        tvCountryCode =(CustomTextView)findViewById(R.id.tv_country_code);
        ll_selectCountryCode=(LinearLayout)findViewById(R.id.ll_select_country_code);
        tv_contacts=(CustomTextView)findViewById(R.id.tv_digital_contacts);
        tvCountryCode_Confirmation=(CustomTextView)findViewById(R.id.tv_country_code_confirmaton_again);


        numberValidation();
        countryCode = AppPreference.getCountryCode(this);
        tvCountryCode.setText("(" + AppPreference.getCountryCode(this) + ")");
        tvCountryCode_Confirmation.setText("(" + AppPreference.getCountryCode(this) + ")");



        if (AppPreference.getCountryImageID(this) != -1) {
            mImgCountryCode.setBackgroundResource(AppPreference.getCountryImageID(this));
            flag = AppPreference.getCountryImageID(this);

        }
        //showToast("flag is..."+flag);
        mCountryCode.setText(AppPreference.getCountryName(this) + " (" + AppPreference.getCountryCode(this) + ")");
        if (!AppPreference.getCountryCode(this).equalsIgnoreCase("+95")) {
            edt_customer_mobile.setText(null);
            edt_customer_mobile.setHint(getResources().getString(R.string.enterDummyMerchantNumber));
        } else {
            edt_customer_mobile.setText(Utils.getMyanmarNumber());
            edt_customer_mobile.setSelection(2);
        }
        ConfirmNumberValidation();


        ll_selectCountryCode.setOnClickListener(this);
        tv_contacts.setOnClickListener(this);


        edt_customer_mobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });
    }
    @Override
    public <T> void response(int resultCode, T data) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {


            case R.id.ll_select_country_code:
                Intent intent = new Intent(this, SelectCountryCode.class);
                startActivityForResult(intent, DIGITALRECEIPT_QR_REQUESTCODE);
                break;

            case R.id.tv_digital_contacts:
                cantactPickker();
                break;




            default:
                break;

        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode ==DIGITALRECEIPT_QR_REQUESTCODE)
        {
            if (data != null) {
                if (data.getIntExtra("POSITION", -1) != -1)
                {
                    mImgCountryCode.setVisibility(View.VISIBLE);
                    mImgCountryCode.setBackgroundResource(data.getIntExtra("POSITION", -1));
                    flag = data.getIntExtra("POSITION", -1);
                }
                mCountryCode.setText(data.getStringExtra("COUNTRY_NAME") + " (" + data.getStringExtra("COUNTRY_CODE") + ")");
                tvCountryCode.setText("(" + data.getStringExtra("COUNTRY_CODE") + ")");
                countryCode = data.getStringExtra("COUNTRY_CODE");
                countryName = data.getStringExtra("COUNTRY_NAME");

                if (!data.getStringExtra("COUNTRY_CODE").equalsIgnoreCase("+95"))
                {
                    edt_customer_mobile.setText(null);
                    edt_customer_mobile.setHint(getResources().getString(R.string.Enter_Your_Email_id));
                } else {
                    edt_customer_mobile.setText(Utils.getMyanmarNumber());
                    edt_customer_mobile.setSelection(2);
                }
            }
        }
        if (requestCode == CALL_CONTACT_PICKER) {
            if (data != null) {
                String name = data.getStringExtra("name");
                String no = data.getStringExtra("no");

                numberSelectedValidation(name, no);
                edt_customer_mobile.dismissDropDown();
            }
        }
    }

    private void cantactPickker() {

        Intent call = new Intent(this, NewContactPicker.class);
        startActivityForResult(call, CALL_CONTACT_PICKER);
    }


    private void numberSelectedValidation(String name, String number) {
        if (number.startsWith("+") || number.startsWith("00")) {
            String[] countryCodeArray = getCountryCodeNameAndFlagFromNumber(number);
            String scountryCode = countryCodeArray[0];
            String scountryName = countryCodeArray[1];
            int sflag = Integer.parseInt(countryCodeArray[2]);

            number = countryCodeArray[3];
            if(number.contains(" "))
            {
                number=number.trim();
                number=number.replace(" ","");
            }
            setNumberIntoTheField(name, scountryCode, sflag, number, scountryName);
            Log.e("SendMoney", "Number selected =" + countryCode);
        } else {


            String[] countryCodeArray = getCountryCodeNameAndFlagFromNumber(number);
            String scountryCode = countryCodeArray[0];
            String scountryName = countryCodeArray[1];
            int sflag = Integer.parseInt(countryCodeArray[2]);
            number = countryCodeArray[3];
            if(number.contains(" "))
            {
                number=number.trim();
                number=number.replace(" ","");
            }
            //TODO @_CountryCode
            Log.e("CountryCode",_mCountryCOde);
            String tet=AppPreference.getCountryCode(this);
            Log.e("lof",tet);

            if (!AppPreference.getCountryCode(this).equals(_mCountryCOde)) {
                differentCountry(_mCountryCOde, _mCountryName, _flag, number, name);

            } else {
                setNumberIntoTheField(name, scountryCode, sflag, number, scountryName);
            }
        }
    }

    private void setNumberIntoTheField(String name, String countryCode, int flag, String number, String countryname) {
//        _mCountryCOde=countryCode;
//        _flag=flag;
        tvCountryCode.setText("(" + countryCode + ")");
        mCountryCode.setText(countryname + " (" + countryCode + ")");
        // tvCountryCode_Confirmation.setText("(" + countryCode + ")");
        isManuallyEnterNumber = true;
        textCount = 0;
        numberForMatching = number;
        tv_contacts.setText(name);
        edt_name.setText(name);
        if (countryCode.equalsIgnoreCase("+95")) {
            edt_customer_mobile.setText(getNumberWithGreyZero(number, true));
            edt_customer_mobile.setText(getNumberWithGreyZero(number, true));
            //edt_customer_name.setText(tv_contacts.getText());

            mImgCountryCode.setBackgroundResource(flag);
            int length = edt_customer_mobile.getText().toString().length();
            edt_customer_mobile.setSelection(length);
            edt_customer_mobile.dismissDropDown();

            this.countryCode = countryCode;
            this.flag = flag;
        } else {
            String snumber = number;
            if (number.startsWith("0")) {
                snumber = number.substring(1);
                edt_customer_mobile.setText(snumber);
            }
            edt_customer_mobile.setText(getNumberWithGreyZero(number, true));
            //edt_customer_name.setText(tv_contacts.getText());

            mImgCountryCode.setBackgroundResource(flag);
            int length = edt_customer_mobile.getText().toString().length();
            edt_customer_mobile.setSelection(length);
            edt_customer_mobile.dismissDropDown();
            this.countryCode = countryCode;
            this.flag = flag;
        }
    }

    private void differentCountry(final String countryCode, final String countryName, final int flag, final String number, final String name) {

        hideKeyboard();
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.select_number, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptsView);
        ImageView defaultImage = (ImageView) promptsView.findViewById(R.id.select_contact_defaultCountr_);
        ImageView manualImage = (ImageView) promptsView.findViewById(R.id.select_contact_changecountry);
        TextView defaultText = (TextView) promptsView.findViewById(R.id.select_number_default_country);
        TextView manually = (TextView) promptsView.findViewById(R.id.select_number_manual_country);
        defaultImage.setBackgroundResource(AppPreference.getCountryImageID(this));
        if (flag != -1) {
            manualImage.setBackgroundResource(flag);
        }
        final AlertDialog dialog1 = alertDialogBuilder.create();
        if (countryCode.equalsIgnoreCase("+95"))
            manually.setText("(" + countryCode + ")" + getNumberWithGreyZero(number, true));
        else
            verifyNumber(manually, number, countryCode);
        if (AppPreference.getCountryCode(this).equalsIgnoreCase("+95"))
            defaultText.setText("(" + AppPreference.getCountryCode(this) + ")" + getNumberWithGreyZero(number, true));
        else
            verifyNumber(defaultText, number, AppPreference.getCountryCode(this));
        defaultText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNumberIntoTheField(name, AppPreference.getCountryCode(DigitalPhoneNoActivity.this), AppPreference.getCountryImageID(DigitalPhoneNoActivity.this), number, AppPreference.getCountryName(DigitalPhoneNoActivity.this));
                _mCountryCOde = AppPreference.getCountryCode(DigitalPhoneNoActivity.this);
                edt_customer_mobile.dismissDropDown();
                dialog1.dismiss();
            }
        });
        manually.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNumberIntoTheField(name, countryCode, flag, number, countryName);
                dialog1.dismiss();
            }
        });
        dialog1.show();


    }

    @Override
    public void getItemSelectedValues(String name, String number) {
        numberSelectedValidation(name, number);
    }

    private void verifyNumber(TextView tv, String number, String countryCode) {
        String snumber = number;
        if (number.startsWith("0")) {
            snumber = number.substring(1);
        }
        tv.setText("(" + countryCode + ")" + snumber);
    }


    private void numberValidation() {
        edt_customer_mobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().startsWith("00")) {
                    if (AppPreference.getCountryCode(DigitalPhoneNoActivity.this).equals("+95")) {
                        edt_customer_mobile.setText("09");
                        edt_customer_mobile.setSelection(2);
                    } else {
                        edt_customer_mobile.setText(null);
                    }
                }
                if (TransactionType != 0 || isManuallyEnterNumber) {
                    isManuallyEnterNumber = true;
                    isConfirmationActivei = false;
                    mConfitmationField.setVisibility(View.GONE);
                } else {
                    if (s.length() > 5) {
                        isConfirmationActivei = true;
                        mConfitmationField.setVisibility(View.VISIBLE);
                        edt_customer_mobile.dismissDropDown();
                        //edt_customer_name.setText(UNKNOWN);
                        TransactionType = 0;
                        // BLT_SEARCHING_TYPE = Type;

                    } else {
                        isConfirmationActivei = false;
                        mConfitmationField.setVisibility(View.GONE);
                    }
                }
                if (numberForMatching.length() != s.toString().length()) {
                    Drawable img = getResources().getDrawable(R.drawable.contact);
                    if (img != null) {
                        img.setBounds(0, 0, 80, 80);
                        tvCountryCode.setCompoundDrawables(img, null, null, null);
                        tv_contacts.setText(getResources().getString(R.string.Unknown));
                        edt_name.setText(R.string.Unknown);
                        isManuallyEnterNumber = false;
                    }
                }
                if (!isManuallyEnterNumber) {
                    if (textCount > 3) {
                        isManuallyEnterNumber = false;
                        TransactionType = 0;
                    }
                }
                textCount++;
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 6) {
                    String number = s.toString();
                    if (BLT_SEARCHING_TYPE.equals("9955") || BLT_SEARCHING_TYPE.equals("9900"))
                        BLT_SEARCHING_TYPE = Type;
                    if (number.startsWith("00")) {
                        String[] countryCodeArray = getCountryCodeNameAndFlagFromNumber(number);
                        String scountryCode = countryCodeArray[0];
                        String scountryName = countryCodeArray[1];
                        int sflag = Integer.parseInt(countryCodeArray[2]);
                        String no = countryCodeArray[3];
                        tvCountryCode.setText("(" + scountryCode + ")");
                        mImgCountryCode.setBackgroundResource(sflag);
                        flag = sflag;
                        in_flag_image = flag;
                        numberForMatching = no;
                        mCountryCode.setText(scountryName + " (" + scountryCode + ")");
                        edt_customer_mobile.setText(no);
                        countryCode = scountryCode;
                        Log.e("SendMoney", countryCode + "and input counry code is " + scountryCode);
                    }
                }
            }
        });


    }



    private void ConfirmNumberValidation() {
        mConfirmMobileNumber.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (isConfirmationActivei) {
                    if (!edt_customer_mobile.getText().toString().startsWith(s.toString())) {
                        mConfirmMobileNumber.setText(null);
                        return;
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }


        });
    }

    private void shareImage()
    {
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("image/*");
        String imagePath = Environment.getExternalStorageDirectory().getAbsolutePath()+ "/screen1.jpg";
        File imageFileToShare = new File(imagePath);
        Uri uri = Uri.fromFile(imageFileToShare);
        share.putExtra(Intent.EXTRA_STREAM, uri);

        startActivity(Intent.createChooser(share, "Share Image!"));

    }



    private void takeScreenshot(View v)
    {
        ScrollView sv = (ScrollView)findViewById(R.id.sendmoney_screb_bg);
        sv.scrollTo(0, sv.getBottom());

        Date now = new Date();
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);

        try {
            // image naming and path  to include sd card  appending name you choose for file
            String mPath = Environment.getExternalStorageDirectory().getAbsolutePath()+"/screen1.jpg";

            // create bitmap screen capture
            View v1 = getWindow().getDecorView().getRootView();
            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap = v1.getDrawingCache();

            File imageFile = new File(mPath);

            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();

            //  openScreenshot(imageFile);
            shareImage();
        } catch (Throwable e)
        {
            e.printStackTrace();

        }
    }

    private void openScreenshot(File imageFile) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        Uri uri = Uri.fromFile(imageFile);
        intent.setDataAndType(uri, "image/*");
        startActivity(intent);
    }


    void validate()
    {
        String unknown = getResources().getString(R.string.Unknown);
        if(isConfirmationActivei) {
            String no = edt_customer_mobile.getText().toString();
            String confirmNo = mConfirmMobileNumber.getText().toString();
            String name=edt_name.getText().toString().trim();
            String completeNo=getFormattedMobileNO(tvCountryCode, no);



            if(!checkNoExistsorNot(completeNo))
            {

                if (!no.equals(confirmNo) || name.equalsIgnoreCase("") || no.length() < 7)
                {
                    if (!no.equals(confirmNo))
                        showToast(R.string.noAndConfirmno);
                    else if (no.length() < 7)
                        showToast(R.string.digi_phoneno_limit);

                    return;
                }
                else {
                    String countryCode = tvCountryCode.getText().toString().trim();

                    in_flag_image = flag;
                    int otp= 0;
                    try {
                        otp = DigitalReceiptNew.generatePin();
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                    PhoneContactModel model = new PhoneContactModel(name, countryCode, no, in_flag_image, completeNo, ""+otp,false);
                    DigitalReceiptNew.al_phonecontcts.add(model);
                    //showToast("no length..." + no.length());
                    callDigitalNewReceipt();
                }
            }
            else
                showToast(R.string.digi_phno_selected);
        }
        else
        {
            String countryCode = tvCountryCode.getText().toString().trim();
            String name = tv_contacts.getText().toString().trim();
            String no = edt_customer_mobile.getText().toString();
            in_flag_image = flag;

            String completeNo = getFormattedMobileNO(tvCountryCode, no);


            if (!checkNoExistsorNot(completeNo))
            {
                if (!name.equalsIgnoreCase("") && no.length() >= 7 )
                {
                    int otp= 0;
                    try {
                        otp = DigitalReceiptNew.generatePin();
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                    PhoneContactModel model = new PhoneContactModel(name, countryCode, no, in_flag_image, completeNo, ""+otp,false);
                    DigitalReceiptNew.al_phonecontcts.add(model);

                    // showToast("no length..."+no.length());
                    callDigitalNewReceipt();
                } else {
                    if (no.length() < 7)
                        showToast(R.string.digi_phoneno_limit);

                    return;
                }

            }

            else
                showToast(R.string.digi_phno_selected);
        }

    }

    void callDigitalNewReceipt()
    {
        Intent intent=new Intent();
        intent.putExtra("sss","...");

        setResult(DIGITALRECEIPT_QR_REQUESTCODE,intent);
        finish();
    }




    boolean checkNoExistsorNot(String no)
    {
        for(int i=0;i<DigitalReceiptNew.al_phonecontcts.size();i++)
        {
            PhoneContactModel model=DigitalReceiptNew.al_phonecontcts.get(i);

            if(no.equals(model.getCompletePhoneNo()))
                return true;


        }

        return false;
    }




}
