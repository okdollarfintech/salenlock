package mm.dummymerchant.sk.merchantapp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.androidlegacy.Contents;
import com.scottyab.aescrypt.AESCrypt;

import java.io.File;
import java.io.FileOutputStream;
import java.security.GeneralSecurityException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.Constant;
import mm.dummymerchant.sk.merchantapp.Utils.GpsTracker1;
import mm.dummymerchant.sk.merchantapp.Utils.Utils;
import mm.dummymerchant.sk.merchantapp.adapter.AutoCompleCustomAdapter;
import mm.dummymerchant.sk.merchantapp.customView.CustomAutoCompleteTextView;
import mm.dummymerchant.sk.merchantapp.customView.CustomButton;
import mm.dummymerchant.sk.merchantapp.customView.CustomEdittext;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
import mm.dummymerchant.sk.merchantapp.db.DBHelper;
import mm.dummymerchant.sk.merchantapp.model.CashierModel;
import mm.dummymerchant.sk.merchantapp.model.ConstactModel;
import mm.dummymerchant.sk.merchantapp.model.MasterDetails;
import mm.dummymerchant.sk.merchantapp.model.NetworkOperatorModel;
import mm.dummymerchant.sk.merchantapp.model.OTPCashierModel;
import mm.dummymerchant.sk.merchantapp.qrCode.QRCodeEncoder;

/**
 * Created by user on 12/7/2015.
 */
public class DigitalReceiptActivity extends BaseActivity implements View.OnClickListener,Constant, AutoCompleCustomAdapter.AutoCompleteTextItemSelected
{
    CustomTextView text ,templete, qr_number,qr_time;
    private boolean isConfirmationActivei = false;
    private boolean isManuallyEnterNumber = false;
    private String Type;
    private CustomEdittext edt_customer_Amount;
    private CustomButton Submit, Otp_button;
    CustomTextView tvCountryCode, mCountryCode, tv_contacts, mImgCountryCode, tvCountryCode_Confirmation;
    RadioGroup rg;
    RadioButton r1;
    int pos;
    int pos1;
    private CustomEdittext mConfirmMobileNumber;
    String otpvalue = "", cashiername, cashierno, cashcollectorname, cashcollectorno, amount, cashierid, Time;
    private String customername, customerno, encryptedMsg, qrInputText, type, encryptedMsgTime, qrInputTextTime;
    String cash_id, cash_name, cash_no, master_no;
    LinearLayout ll_selectCountryCode;
    private ImageView imageView_qrcode;
    private String countryName, Generate_Latitude, Generate_Longitude, GenerateLocation;
    private ImageView mCountryImage;
    NetworkOperatorModel model;
    int flag;
    private String numberForMatching = "", countryCode = "";
    private CustomAutoCompleteTextView edt_customer_mobile;
    private String _mCountryCOde = "";
    private String _mCountryName = "";
    private int _flag = -1;
    int count = 1;
    private DBHelper dbb;
    private LinearLayout mConfitmationField;
    static String msg_unable_to_generatePin;
    LinearLayout container;
    private ArrayList<String> value = new ArrayList<String>();
    private ArrayList<String> value1 = new ArrayList<String>();
    int childCount;
    private String editinput = "?";
    private Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_digital_screen );

        msg_unable_to_generatePin = getString(R.string.unable_generate_pin);
        setExistingCashcollectorActionBar(String.valueOf(getResources().getText(R.string.generate_qr_code)));
        init();
        dbb = new DBHelper(this);
        _mCountryCOde = AppPreference.getCountryCode(this);

        Utils utils = new Utils();
        String jsondata = db.getJsonContacts();
        ArrayList<ConstactModel> al_contacts = utils.getContactListFromJsonString(jsondata);
        AutoCompleCustomAdapter adapter = new AutoCompleCustomAdapter(this, R.layout.custom_autocomple_layout_row, al_contacts, this);
        //-----------------------------------------
        edt_customer_mobile.setAdapter(adapter);
        edt_customer_mobile.setThreshold(1);
        hideKeyboard();
        Check_MasterORCashier_Login();
    }

    void init() {
        if (Utils.getSimOperater(this) != null) {
            if (!Utils.getSimOperater(this).equals("")) {
                model = new NetworkOperatorModel().getNetworkOperatorModel(this, Utils.getSimOperater(this));
            }
        }
        rg = (RadioGroup) findViewById(R.id.radioGroup1);
        r1 = (RadioButton) findViewById(R.id.radio0);
        mConfitmationField = (LinearLayout) findViewById(R.id.send_money_confirmation_of_number);
        mConfirmMobileNumber = (CustomEdittext) findViewById(R.id.et_digital_confirm_phoneno);
        edt_customer_Amount = (CustomEdittext) findViewById(R.id.et_digital_amount);
        //edt_customer_name= (CustomEdittext) findViewById(R.id.et_digital_cc_name);
        edt_customer_mobile = (CustomAutoCompleteTextView) findViewById(R.id.et_digital_phoneno);
      //  edt_customer_sub = (CustomEdittext) findViewById(R.id.et_digital_comments);
        Submit = (CustomButton) findViewById(R.id.Submit);
        Otp_button = (CustomButton) findViewById(R.id.Otp_button);
        imageView_qrcode = (ImageView) findViewById(R.id.imageView_qrcode);
        mImgCountryCode = (CustomTextView) findViewById(R.id.img_select_country_code);
        // mCountryImage=(ImageView)findViewById(R.id.)
        mCountryCode = (CustomTextView) findViewById(R.id.tv_digital_select_country_code);
        tvCountryCode = (CustomTextView) findViewById(R.id.tv_country_code);
        ll_selectCountryCode = (LinearLayout) findViewById(R.id.ll_select_country_code);
        tv_contacts = (CustomTextView) findViewById(R.id.tv_digital_contacts);
        tvCountryCode_Confirmation = (CustomTextView) findViewById(R.id.tv_country_code_confirmaton_again);
        //mCountryImage=()
        text = (CustomTextView)findViewById(R.id.text);
        templete = (CustomTextView)findViewById(R.id.templete);
        qr_number = (CustomTextView)findViewById(R.id.qr_number);
        qr_time = (CustomTextView)findViewById(R.id.qr_time);

        numberValidation();
        countryCode = AppPreference.getCountryCode(this);
        tvCountryCode.setText("(" + AppPreference.getCountryCode(this) + ")");
        tvCountryCode_Confirmation.setText("(" + AppPreference.getCountryCode(this) + ")");
        if (AppPreference.getCountryImageID(this) != -1) {
            mImgCountryCode.setBackgroundResource(AppPreference.getCountryImageID(this));
            flag = AppPreference.getCountryImageID(this);
        }
        mCountryCode.setText(AppPreference.getCountryName(this) + " (" + AppPreference.getCountryCode(this) + ")");
        if (!AppPreference.getCountryCode(this).equalsIgnoreCase(MYANMAR_COUNTRY_CODE)) {
            edt_customer_mobile.setText(null);
            edt_customer_mobile.setHint(getResources().getString(R.string.enterDummyMerchantNumber));
        } else {
            edt_customer_mobile.setText(Utils.getMyanmarNumber());
            edt_customer_mobile.setSelection(2);
        }
        ConfirmNumberValidation();

        Submit.setOnClickListener(this);
        Otp_button.setOnClickListener(this);
        ll_selectCountryCode.setOnClickListener(this);
        tv_contacts.setOnClickListener(this);
        text.setOnClickListener(this);
        templete.setOnClickListener(this);


        type = TYPE_QRCODE;
        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // TODO Auto-generated method stub
                // Method 1 For Getting Index of RadioButton
                // Method 1
                pos = rg.indexOfChild(findViewById(checkedId));
                //Method 2
                pos1 = rg.indexOfChild(findViewById(rg.getCheckedRadioButtonId()));

                switch (pos) {
                    case 0:
                        r1.setChecked(true);
                        imageView_qrcode.setVisibility(View.INVISIBLE);
                        type = TYPE_QRCODE;
                        qr_number.setText("");
                        qr_time.setText("");
                        break;
                    case 1:
                        type = TYPE_SMS;
                        imageView_qrcode.setVisibility(View.INVISIBLE);
                        otpvalue = "";
                        qr_number.setText("");
                        qr_time.setText("");
                        break;

                    default:
                        type = TYPE_QRCODE;
                        break;
                }
            }

        });

        edt_customer_Amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                imageView_qrcode.setVisibility(View.GONE);
                qr_number.setText("");
                qr_time.setText("");
            }
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub
            }
            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });
        edt_customer_mobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                imageView_qrcode.setVisibility(View.GONE);
                qr_number.setText("");
                qr_time.setText("");
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });

    }

    @Override
    public <T> void response(int resultCode, T data) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.Submit:
                if(isConfirmationActivei)
                {
                    String no=edt_customer_mobile.getText().toString();
                    String  confirmNo=mConfirmMobileNumber.getText().toString();
                    if(!no.equals(confirmNo)) {
                        showToast(R.string.noAndConfirmno);
                        return;
                    }
                }
                if(editinput == null)
                {
                    editinput = "?";
                }
                System.out.println("text editinput value " + editinput);
                System.out.println("template ArrayList Value " + value.toString());
                customername = tv_contacts.getText().toString().trim();
                customerno = edt_customer_mobile.getText().toString().trim();
                if(!customerno.equalsIgnoreCase(""))
                {
                    customerno = getFormattedMobileNO(tvCountryCode, customerno);
                }
                Get_Location();
                Log.i("Latitude after Getting", Generate_Latitude);
                Log.i("Longitude after Getting", Generate_Longitude);
                GenerateLocation = Generate_Latitude + "," + Generate_Longitude;
                Log.i("GenerateLocation", GenerateLocation);
                int customerno_len = customerno.length();
                System.out.println("Customerno length : " + customerno_len);

                if(!customername.equalsIgnoreCase("") && !customerno.equalsIgnoreCase("") && customerno_len >= 11 && customerno_len <= 21)
                {
                    amount = edt_customer_Amount.getText().toString();
                    DateFormat targetFormat = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
                    Time = targetFormat.format(new Date());
                    encryptedMsgTime = Time +"----" + customerno;

                    try {
                        otpvalue = String.valueOf(generatePin());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    String a = AppPreference.getMasterOrCashier(this);
                    if(a.equals(MASTER))
                    {
                        String Master_name = MASTER_TAG;
                        MasterDetails masterDetails = dbb.getMasterDetails();
                        master_no = masterDetails.getUsername();
                        if (type.equals(TYPE_QRCODE))
                        {
                            Finaldata(customername,customerno,Master_name,master_no,MasterORCashier_ID);
                        }
                        else {
                            Finaldata(customername,customerno,Master_name,master_no,MasterORCashier_ID);
                            takeScreenshot();
                        }
                    }
                    else {
                        cash_id = SliderScreen.present_Cashier_Id;
                        CashierModel cashierModel = dbb.getCashierModel(cash_id);
                        cash_name = cashierModel.getCashier_Name();
                        cash_no = cashierModel.getCashier_Number();
                        if (type.equals(TYPE_QRCODE))
                        {
                            Finaldata(customername,customerno,cash_name,cash_no,cash_id);
                        }
                        else {
                            Finaldata(customername,customerno,cash_name,cash_no,cash_id);
                            takeScreenshot();
                        }
                    }

                } else {
                    String wrongText = getString(R.string.pls_entrblw_fields) + "\n";
                    if (customername.equalsIgnoreCase(""))
                        wrongText += getString(R.string.entr_customer_name) + "\n";

                    if (customerno.equalsIgnoreCase(""))
                        wrongText += getString(R.string.validno) + "\n";

                    if (customerno_len >= 7)
                        wrongText += getString(R.string.validno) + "\n";

                    if (customerno_len <= 17)
                        wrongText += getString(R.string.validno) + "\n";

                    showToast(wrongText);
                }

                break;

            case R.id.Otp_button:
                if (!otpvalue.equals("")) {
                    Log.i("OTP Value ", otpvalue);
                    LayoutInflater layoutInflater = LayoutInflater.from(this);
                    View promptView = layoutInflater.inflate(R.layout.otp_prompts, null);
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                    alertDialogBuilder.setView(promptView);
                    final EditText input = (EditText) promptView.findViewById(R.id.userInput);
                    // setup a dialog window
                    alertDialogBuilder
                            .setCancelable(false)
                            .setPositiveButton(OK, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // get user input and set it to result
                                    String Edit_otp_value = input.getText().toString().trim();
                                    if (!Edit_otp_value.equalsIgnoreCase("")) {
                                        if (count <= 4) {
                                            if (Edit_otp_value.equals(otpvalue)) {
                                                OTPCashierModel cashout_details = new OTPCashierModel(Time, cashiername, cashierno, cashcollectorname, cashcollectorno, amount, SUCCESS, cashierid, "",editinput,value.toString(),"");
                                                db.insertOTPCashierDetails(cashout_details);
                                                showToast1(getString(R.string.rightotp));
                                                input.setText("");
                                                edt_customer_Amount.setText("");
                                                edt_customer_mobile.setText("");
                                                qr_number.setText("");
                                                qr_time.setText("");
                                                imageView_qrcode.setVisibility(View.GONE);
                                                text.setText("");
                                            } else {
                                                showToast(getString(R.string.wrongotp));
                                                count = count + 1;
                                                input.setText("");
                                                //qr_number.setText("");
                                                //qr_time.setText("");
                                                text.setText("");
                                            }
                                        } else {
                                            showToast(getString(R.string.wrongotp_manytimes));
                                            OTPCashierModel cashout_details = new OTPCashierModel(Time, cashiername, cashierno, cashcollectorname, cashcollectorno, amount, FAILURE, cashierid, "",editinput,value.toString(),"");
                                            db.insertOTPCashierDetails(cashout_details);
                                            input.setText("");
                                            edt_customer_Amount.setText("");
                                            edt_customer_mobile.setText("");
                                            imageView_qrcode.setVisibility(View.GONE);
                                            qr_number.setText("");
                                            qr_time.setText("");
                                            count = 1;
                                            text.setText("");
                                            hideKeyboard();
                                        }
                                    } else {
                                        String wrongText = getString(R.string.pls_entrblw_fields) + "\n";
                                        if (Edit_otp_value.equalsIgnoreCase(""))
                                            wrongText += getString(R.string.entr_otp) + "\n";
                                        showToast(wrongText);
                                        hideKeyboard();
                                    }
                                }
                            })
                            .setNegativeButton(CANCEL,

                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }

                                    });
                    // create an alert dialog
                    AlertDialog alertD = alertDialogBuilder.create();
                    alertD.show();

                } else {
                    String wrongText = getString(R.string.pls_entrblw_fields) + "\n";
                    if (otpvalue.equalsIgnoreCase(""))
                        wrongText += getString(R.string.gnr_qrcode) + "\n";
                    showToast(wrongText);
                    hideKeyboard();
                }

                break;

            case R.id.ll_select_country_code:
                Intent intent = new Intent(this, SelectCountryCode.class);
                startActivityForResult(intent, DIGITALRECEIPT_QR_REQUESTCODE);
                break;

            case R.id.tv_digital_contacts:
                cantactPickker();
                imageView_qrcode.setVisibility(View.GONE);
                qr_number.setText("");
                qr_time.setText("");
                break;

            case R.id.text:
                // Create custom dialog object
                final Dialog dialog = new Dialog(DigitalReceiptActivity.this);
                // Include dialog.xml file
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.text_prompts);
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
                CustomTextView close  = (CustomTextView) dialog.findViewById(R.id.close);
                Button reset = (Button) dialog.findViewById(R.id.reset);
                Button ok = (Button) dialog.findViewById(R.id.ok);
                final EditText input = (EditText) dialog.findViewById(R.id.input);

                if(editinput.equals("?"))
                {
                    input.setText("");
                }
                else {
                    input.setText(editinput);
                }
                // if decline button is clicked, close the custom dialog
                reset.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        input.setText("");
                        editinput = "?";
                    }
                });
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String Edit_input_value = input.getText().toString().trim();
                        dialog.dismiss();
                        // editinput = Edit_input_value;
                        if(Edit_input_value.equals(""))
                        {
                            editinput = "?";
                        }
                        else {
                            editinput = Edit_input_value;
                        }
                        text.setText(Edit_input_value);
                    }
                });
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Close dialog
                         dialog.dismiss();
                    }
                });
                input.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                        imageView_qrcode.setVisibility(View.GONE);
                        qr_number.setText("");
                        qr_time.setText("");
                    }

                    @Override
                    public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                                  int arg3) {
                        // TODO Auto-generated method stub
                    }

                    @Override
                    public void afterTextChanged(Editable arg0) {

                    }
                });

                break;

            case R.id.templete:
                final Dialog dialog1 = new Dialog(DigitalReceiptActivity.this);
                dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog1.setContentView(R.layout.template_prompts);
                dialog1.setCanceledOnTouchOutside(false);
                dialog1.show();
                CustomTextView add  = (CustomTextView) dialog1.findViewById(R.id.add);
                CustomTextView close1  = (CustomTextView) dialog1.findViewById(R.id.close);
                Button cancel = (Button) dialog1.findViewById(R.id.cancel);
                Button ok1 = (Button) dialog1.findViewById(R.id.ok);
                final CustomEdittext item = (CustomEdittext) dialog1.findViewById(R.id.item);
                final CustomEdittext qty = (CustomEdittext) dialog1.findViewById(R.id.qty);
                final CustomEdittext price = (CustomEdittext) dialog1.findViewById(R.id.price);
                container = (LinearLayout) dialog1.findViewById(R.id.container);
                int count = 0;
                for(int i=0 ; i<value.size();i++)
                {
                    try {
                        String s = value.get(0);
                        System.out.println(" 0 th" + " S value  " + s);
                        String[] separated = s.split("/");
                        item.setText(separated[0]);
                        qty.setText(separated[1]);
                        price.setText(separated[2]);
                        count++;
                    }
                    catch (ArrayIndexOutOfBoundsException e){
                    }
                }
                for(int i=0 ; i<count-1;i++)
                {
                    addFunction(container);
                }
                System.out.println("Count values "+count);
                System.out.println("value size " + value.size());
                    for(int i=0 ; i<value1.size() ;i++)
                    {
                        View childView1 = container.getChildAt(i);
                        CustomEdittext itemEdt = (CustomEdittext) (childView1.findViewById(R.id.item));
                        CustomEdittext qtyEdt = (CustomEdittext) (childView1.findViewById(R.id.qty));
                        CustomEdittext priceEdt = (CustomEdittext) (childView1.findViewById(R.id.price));
                        try {
                        String s = value1.get(i);
                       // System.out.println(i + " th" + " S value :childCount   " + s);
                        String[] separated = s.split("/");
                        /*System.out.println(i + " th" + " separated item value :childCount " + separated[0]);*/
                            if ((separated[0] != null) || !(separated[0].equals(""))) {
                                itemEdt.setText(separated[0]);
                            }
                            if ((separated[1] != null) || !(separated[1].equals(""))) {
                                qtyEdt.setText(separated[1]);
                            }
                            if ((separated[2] != null) || !(separated[2].equals(""))) {
                                priceEdt.setText(separated[2]);
                            }
                        }catch (ArrayIndexOutOfBoundsException e){
                        }
                    }
                add.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                       // dialog1.dismiss();
                        addFunction(container);
                        imageView_qrcode.setVisibility(View.GONE);
                        qr_number.setText("");
                        qr_time.setText("");
                    }
                });

                close1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showToast(getString(R.string.dntdo));
                    }
                });
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog1.dismiss();
                    }
                });

                ok1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog1.dismiss();
                        String itemvalue,qtyvalue,pricevalue;
                        itemvalue = item.getText().toString();
                        qtyvalue = qty.getText().toString();
                        pricevalue = price.getText().toString();
                        String SingleArrayIndex1,SingleArrayIndex2;
                        value.clear();
                        value1.clear();
                        childCount = container.getChildCount();
                        imageView_qrcode.setVisibility(View.GONE);
                        qr_number.setText("");
                        qr_time.setText("");
                        System.out.println("The ChildCount ok click: " + childCount);
                        if( !itemvalue.equals("") || !qtyvalue.equals("") || !pricevalue.equals(""))
                        {
                            SingleArrayIndex1 = itemvalue + "/" + qtyvalue + "/" + pricevalue ;
                            value.add(SingleArrayIndex1);
                        }
                        for (int ct = 0; ct < childCount; ct++)
                        {
                            View childView1 = container.getChildAt(ct);
                            CustomEdittext itemEdt = (CustomEdittext) (childView1.findViewById(R.id.item));
                            CustomEdittext qtyEdt = (CustomEdittext) (childView1.findViewById(R.id.qty));
                            CustomEdittext priceEdt = (CustomEdittext) (childView1.findViewById(R.id.price));

                            String item = itemEdt.getText().toString();
                            String qty = qtyEdt.getText().toString();
                            String price = priceEdt.getText().toString();

                            if( !item.equals("") || !qty.equals("") || !price.equals(""))
                            {
                                SingleArrayIndex2 = item + "/" + qty + "/" + price ;
                                value.add(SingleArrayIndex2);
                                value1.add(SingleArrayIndex2);
                            }
                            Log.i("ITEM ",item);
                            Log.i("QUANTITY ",qty);
                            Log.i("PRICE ", price);
                        }
                        Log.i("Array Value",value.toString());
                    }
                });

                break;

            default:
                break;

        }
    }

    private void Finaldata(String customername, String customerno, String cash_name, String cash_no, String cash_id)
    {
            encryptedMsg = SAFETYCASHIER_TO_CASHCOLLECTOR + "-" + customername + "-" + customerno + "-" + amount + "-" + cash_name + "-" + cash_no + "-" + cash_id + "-" + otpvalue + "-" + Time + "-" + Generate_Latitude + "-" + Generate_Longitude + "-" + editinput + "-" + value.toString();

            cashcollectorname = customername;
            cashcollectorno = customerno;
            cashiername = cash_name;
            cashierno = cash_no;
            cashierid = cash_id;
            Encryt_Data(encryptedMsg);
    }

    private void Encryt_Data(String str_encryptedMsg) {
        DateFormat targetFormat = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
        Time=targetFormat.format(new Date());
        try {
            qrInputTextTime = AESCrypt.encrypt(SPEEDKey, encryptedMsgTime);
            qrInputText = AESCrypt.encrypt(encryptedMsgTime, str_encryptedMsg);
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        }
        if (generateQRCode(qrInputText) != null) {
            imageView_qrcode.setVisibility(View.VISIBLE);
            imageView_qrcode.setImageBitmap(generateQRCode(qrInputTextTime + "---" + qrInputText));
            Log.i("otpvalue ", otpvalue);
            if (customername.equals(getResources().getString(R.string.Unknown))) {
                qr_number.setText(customerno + " - " + amount);
                qr_time.setText(Time);
            } else {
                qr_number.setText(customername + " - " + amount);
                qr_time.setText(Time);
            }
            hideKeyboard();
        }
    }

    public Bitmap generateQRCode(String qrInputText) {
        //Find screen size
        WindowManager manager = (WindowManager) getSystemService(WINDOW_SERVICE);
        Display display = manager.getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);
        int width = point.x;
        int height = point.y;
        System.out.println("width" + width);
        System.out.println("height" + height);
        int smallerDimension = width < height ? width : height;
        smallerDimension = smallerDimension * 3 / 4;
        System.out.println("smallerDimension" + smallerDimension);
        //Encode with a QR Code image
        QRCodeEncoder qrCodeEncoder = new QRCodeEncoder(qrInputText,
                null,
                Contents.Type.TEXT,
                BarcodeFormat.QR_CODE.toString(),
                1000);
        try {
            Bitmap bitmap = qrCodeEncoder.encodeAsBitmap();
            Bitmap bitLogo = BitmapFactory.decodeResource(getResources(),
                    R.drawable.ok
            );
            Bitmap bitMerged = mergeBitmaps(bitmap, bitLogo);

            return bitMerged;


        } catch (WriterException e) {
            e.printStackTrace();

            return null;
        }
    }

    public static Bitmap mergeBitmaps(Bitmap bmp1, Bitmap bmp2) {
        Bitmap bmOverlay = Bitmap.createBitmap(bmp1.getWidth(),
                bmp1.getHeight(), bmp1.getConfig());
        int centreX = (bmp1.getWidth() - bmp2.getWidth()) / 2;
        int centreY = (bmp1.getHeight() - bmp2.getHeight()) / 2;
        Canvas canvas = new Canvas(bmOverlay);
        canvas.drawBitmap(bmp1, new Matrix(), null);
        canvas.drawBitmap(bmp2, centreX, centreY, null);


        return bmOverlay;
    }

    private void  addFunction(LinearLayout container_id)
    {
        final LinearLayout Container_id = container_id;
        LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View addView = layoutInflater.inflate(R.layout.row, null);
        final CustomEdittext item = (CustomEdittext) addView.findViewById(R.id.item);
        final CustomEdittext qty = (CustomEdittext) addView.findViewById(R.id.qty);
        final CustomEdittext price = (CustomEdittext) addView.findViewById(R.id.price);
        CustomTextView remove  = (CustomTextView) addView.findViewById(R.id.close);

        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((LinearLayout) addView.getParent()).removeView(addView);
                imageView_qrcode.setVisibility(View.GONE);
                qr_number.setText("");
                qr_time.setText("");
            }
        });

        Container_id.addView(addView);
    }

    public static int generatePin() throws Exception {
        Random generator = new Random();
        generator.setSeed(System.currentTimeMillis());

        int num = generator.nextInt(99999) + 99999;
        if (num < 100000 || num > 999999) {
            num = generator.nextInt(99999) + 99999;
            if (num < 100000 || num > 999999) {
                throw new Exception(msg_unable_to_generatePin);
            }
        }
        return num;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == DIGITALRECEIPT_QR_REQUESTCODE)
        {
            if (data != null) {
                if (data.getIntExtra(POSITION, -1) != -1) {
                    mImgCountryCode.setVisibility(View.VISIBLE);
                    mImgCountryCode.setBackgroundResource(data.getIntExtra(POSITION, -1));
                    flag = data.getIntExtra(POSITION, -1);
                }
                mCountryCode.setText(data.getStringExtra(COUNTRY_NAME) + " (" + data.getStringExtra(COUNTRY_CODE) + ")");
                tvCountryCode.setText("(" + data.getStringExtra(COUNTRY_CODE) + ")");
                countryCode = data.getStringExtra(COUNTRY_CODE);
                countryName = data.getStringExtra(COUNTRY_NAME);

                if (!data.getStringExtra(COUNTRY_CODE).equalsIgnoreCase(MYANMAR_COUNTRY_CODE)) {
                    edt_customer_mobile.setText(null);
                    edt_customer_mobile.setHint(getResources().getString(R.string.Enter_Your_Email_id));
                } else {
                    edt_customer_mobile.setText(Utils.getMyanmarNumber());
                    edt_customer_mobile.setSelection(2);
                }
            }
        }
        if (requestCode == CALL_CONTACT_PICKER)
        {
            if (data != null) {
                String name = data.getStringExtra(KEY_NAME);
                String no = data.getStringExtra(KEY_NO);
                numberSelectedValidation(name, no);
                edt_customer_mobile.dismissDropDown();
            }
        }
    }

    private void cantactPickker() {

        Intent call = new Intent(this, NewContactPicker.class);
        Bundle b=new Bundle();
        b.putInt(CONTACT_PICKER_KEY,VALUE_DIGITAL_RECEIPT_CONTACTPICKER);
        call.putExtras(b);
        startActivityForResult(call, CALL_CONTACT_PICKER);
    }


    private void numberSelectedValidation(String name, String number)
    {
        if (number.startsWith("+") || number.startsWith("00")) {
            String[] countryCodeArray = getCountryCodeNameAndFlagFromNumber(number);
            String scountryCode = countryCodeArray[0];
            String scountryName = countryCodeArray[1];
            int sflag = Integer.parseInt(countryCodeArray[2]);
            number = countryCodeArray[3];
            if (number.contains(" ")) {
                number = number.trim();
                number = number.replace(" ", "");
            }
            setNumberIntoTheField(name, scountryCode, sflag, number, scountryName);
            Log.e("SendMoney", "Number selected =" + countryCode);
        } else {

            //showToast("happens");
            String[] countryCodeArray = getCountryCodeNameAndFlagFromNumber(number);
            String scountryCode = countryCodeArray[0];
            String scountryName = countryCodeArray[1];
            int sflag = Integer.parseInt(countryCodeArray[2]);
            number = countryCodeArray[3];
            if (number.contains(" ")) {
                number = number.trim();
                number = number.replace(" ", "");
            }
            //TODO @_CountryCode
            Log.e("CountryCode", _mCountryCOde);
            String tet = AppPreference.getCountryCode(this);
            Log.e("lof", tet);

            if (!AppPreference.getCountryCode(this).equals(_mCountryCOde))
            {
                //differentCountry(_mCountryCOde, _mCountryName, _flag, number, name);
                setNumberIntoTheField(name, scountryCode, sflag, number, scountryName);
                // showToast("executes");
            } else {
                setNumberIntoTheField(name, scountryCode, sflag, number, scountryName);
            }
        }
    }

    private void setNumberIntoTheField(String name, String countryCode, int flag, String number, String countryname)
    {
//
        tvCountryCode.setText("(" + countryCode + ")");
        mCountryCode.setText(countryname + " (" + countryCode + ")");
        // tvCountryCode_Confirmation.setText("(" + countryCode + ")");
        isManuallyEnterNumber = true;
        textCount = 0;
        numberForMatching = number;
        tv_contacts.setText(name);

        if (countryCode.equalsIgnoreCase(MYANMAR_COUNTRY_CODE))
        {
            edt_customer_mobile.setText(getNumberWithGreyZero(number, true));
            edt_customer_mobile.setText(getNumberWithGreyZero(number, true));
            //edt_customer_name.setText(tv_contacts.getText());
            //showToast("flag.." + flag);
            mImgCountryCode.setBackgroundResource(flag);
            int length = edt_customer_mobile.getText().toString().length();
            edt_customer_mobile.setSelection(length);
            edt_customer_mobile.dismissDropDown();

            this.countryCode = countryCode;
            this.flag = flag;
        } else {
            String snumber = number;
            if (number.startsWith("0")) {
                snumber = number.substring(1);
                edt_customer_mobile.setText(snumber);
            }
            edt_customer_mobile.setText(getNumberWithGreyZero(number, true));
            // edt_customer_name.setText(tv_contacts.getText());
            //showToast( "flag.."+flag);
            mImgCountryCode.setBackgroundResource(flag);
            int length = edt_customer_mobile.getText().toString().length();
            edt_customer_mobile.setSelection(length);
            edt_customer_mobile.dismissDropDown();
            this.countryCode = countryCode;
            this.flag = flag;
        }
    }

    private void differentCountry(final String countryCode, final String countryName, final int flag, final String number, final String name) {

        hideKeyboard();
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.select_number, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptsView);
        ImageView defaultImage = (ImageView) promptsView.findViewById(R.id.select_contact_defaultCountr_);
        ImageView manualImage = (ImageView) promptsView.findViewById(R.id.select_contact_changecountry);
        TextView defaultText = (TextView) promptsView.findViewById(R.id.select_number_default_country);
        TextView manually = (TextView) promptsView.findViewById(R.id.select_number_manual_country);
        defaultImage.setBackgroundResource(AppPreference.getCountryImageID(this));
        if (flag != -1) {
            manualImage.setBackgroundResource(flag);
        }
        final AlertDialog dialog1 = alertDialogBuilder.create();
        if (countryCode.equalsIgnoreCase(MYANMAR_COUNTRY_CODE))
            manually.setText("(" + countryCode + ")" + getNumberWithGreyZero(number, true));
        else
            verifyNumber(manually, number, countryCode);
        if (AppPreference.getCountryCode(this).equalsIgnoreCase(MYANMAR_COUNTRY_CODE))
            defaultText.setText("(" + AppPreference.getCountryCode(this) + ")" + getNumberWithGreyZero(number, true));
        else
            verifyNumber(defaultText, number, AppPreference.getCountryCode(this));
        defaultText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNumberIntoTheField(name, AppPreference.getCountryCode(DigitalReceiptActivity.this), AppPreference.getCountryImageID(DigitalReceiptActivity.this), number, AppPreference.getCountryName(DigitalReceiptActivity.this));
                _mCountryCOde = AppPreference.getCountryCode(DigitalReceiptActivity.this);
                edt_customer_mobile.dismissDropDown();
                dialog1.dismiss();
            }
        });
        manually.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNumberIntoTheField(name, countryCode, flag, number, countryName);
                dialog1.dismiss();
            }
        });
        dialog1.show();


    }

    @Override
    public void getItemSelectedValues(String name, String number) {
        numberSelectedValidation(name, number);
    }

    private void verifyNumber(TextView tv, String number, String countryCode) {
        String snumber = number;
        if (number.startsWith("0")) {
            snumber = number.substring(1);
        }
        tv.setText("(" + countryCode + ")" + snumber);
    }


    private void numberValidation() {
        edt_customer_mobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().startsWith("00")) {
                    if (AppPreference.getCountryCode(DigitalReceiptActivity.this).equals(MYANMAR_COUNTRY_CODE)) {
                        edt_customer_mobile.setText("09");
                        edt_customer_mobile.setSelection(2);
                    } else {
                        edt_customer_mobile.setText(null);
                    }
                }
                if (TransactionType != 0 || isManuallyEnterNumber) {
                    isManuallyEnterNumber = true;
                    isConfirmationActivei = false;
                    mConfitmationField.setVisibility(View.GONE);
                } else {
                    if (s.length() > 5) {
                        isConfirmationActivei = true;
                        mConfitmationField.setVisibility(View.VISIBLE);
                        edt_customer_mobile.dismissDropDown();
                        //  edt_customer_name.setText(UNKNOWN);
                        TransactionType = 0;
                        // BLT_SEARCHING_TYPE = Type;

                        //------new code
                        try {
                            String number = edt_customer_mobile.getText().toString().trim();
                            String confirmNumber = mConfirmMobileNumber.getText().toString().trim();

                            if (number.length() < confirmNumber.length())
                                mConfirmMobileNumber.setText(null);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        //----------------

                    } else {
                        isConfirmationActivei = false;
                        mConfitmationField.setVisibility(View.GONE);
                    }
                }
                if (numberForMatching.length() != s.toString().length()) {
                    Drawable img = getResources().getDrawable(R.drawable.contact);
                    if (img != null) {
                        img.setBounds(0, 0, 80, 80);
                      //  tvCountryCode.setCompoundDrawables(img, null, null, null);
                        tv_contacts.setText(getResources().getString(R.string.Unknown));
                        isManuallyEnterNumber = false;
                    }
                }
                if (!isManuallyEnterNumber) {
                    if (textCount > 3) {
                        isManuallyEnterNumber = false;
                        TransactionType = 0;
                    }
                }
                textCount++;
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 6) {
                    String number = s.toString();
                    if (BLT_SEARCHING_TYPE.equals("9955") || BLT_SEARCHING_TYPE.equals("9900"))
                        BLT_SEARCHING_TYPE = Type;
                    if (number.startsWith("00")) {
                        String[] countryCodeArray = getCountryCodeNameAndFlagFromNumber(number);
                        String scountryCode = countryCodeArray[0];
                        String scountryName = countryCodeArray[1];
                        int sflag = Integer.parseInt(countryCodeArray[2]);
                        String no = countryCodeArray[3];
                        tvCountryCode.setText("(" + scountryCode + ")");
                        mImgCountryCode.setBackgroundResource(sflag);
                        flag = sflag;
                        numberForMatching = no;
                        mCountryCode.setText(scountryName + " (" + scountryCode + ")");
                        edt_customer_mobile.setText(no);
                        countryCode = scountryCode;
                        Log.e("SendMoney", countryCode + "and input counry code is " + scountryCode);
                    }
                }
            }
        });


    }

    public void Get_Location() {
        GpsTracker1 gps = new GpsTracker1(getApplicationContext());
        Location loc = gps.getLocation();

        if (loc != null) {
            Generate_Latitude = "" + loc.getLatitude();
            Generate_Longitude = "" + loc.getLongitude();
            Log.i("Generate_Latitude", Generate_Latitude);
            Log.i("Generate_Longitude", Generate_Longitude);
        } else {
            Generate_Latitude = "0.0";
            Generate_Longitude = "0.0";
            Log.i("Generate_Latitude", Generate_Latitude);
            Log.i("Generate_Longitude", Generate_Longitude);
        }
    }

    private void ConfirmNumberValidation() {
        mConfirmMobileNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (isConfirmationActivei) {
                    if (!edt_customer_mobile.getText().toString().startsWith(s.toString())) {
                        mConfirmMobileNumber.setText(null);
                        return;
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }


        });
    }

    private void takeScreenshot()
    {
        ScrollView sv = (ScrollView)findViewById(R.id.sendmoney_screb_bg);
            sv.scrollTo(0, sv.getBottom());
        Toast.makeText(this,"scroll happened",Toast.LENGTH_SHORT).show();
        Date now = new Date();
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);

        try {
            // image naming and path  to include sd card  appending name you choose for file
            String mPath = Environment.getExternalStorageDirectory().getAbsolutePath()+"/screen1.jpg";

            // create bitmap screen capture
            View v1 = getWindow().getDecorView().getRootView();
            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap = v1.getDrawingCache();

            File imageFile = new File(mPath);

            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();

            //  openScreenshot(imageFile);
            shareImage();
        } catch (Throwable e)
        {
            e.printStackTrace();

        }
    }

    private void shareImage()
    {
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("image/*");
        String imagePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/screen1.jpg";
        File imageFileToShare = new File(imagePath);
        Uri uri = Uri.fromFile(imageFileToShare);
        share.putExtra(Intent.EXTRA_STREAM, uri);
        startActivity(Intent.createChooser(share, "Share Image!"));
    }


    public void customAlertPopup()
    {

    }
}

