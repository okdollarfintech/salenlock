package mm.dummymerchant.sk.merchantapp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.androidlegacy.Contents;
import com.scottyab.aescrypt.AESCrypt;

import java.io.File;
import java.io.FileOutputStream;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.Constant;
import mm.dummymerchant.sk.merchantapp.Utils.GpsTracker1;
import mm.dummymerchant.sk.merchantapp.Utils.Utils;
import mm.dummymerchant.sk.merchantapp.adapter.AutoCompleCustomAdapter;
import mm.dummymerchant.sk.merchantapp.customView.CustomAutoCompleteTextView;
import mm.dummymerchant.sk.merchantapp.customView.CustomButton;
import mm.dummymerchant.sk.merchantapp.customView.CustomEdittext;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
import mm.dummymerchant.sk.merchantapp.db.DBHelper;
import mm.dummymerchant.sk.merchantapp.db.Utility;
import mm.dummymerchant.sk.merchantapp.model.CashierModel;
import mm.dummymerchant.sk.merchantapp.model.ConstactModel;
import mm.dummymerchant.sk.merchantapp.model.LaterModel;
import mm.dummymerchant.sk.merchantapp.model.MasterDetails;
import mm.dummymerchant.sk.merchantapp.model.NetworkOperatorModel;
import mm.dummymerchant.sk.merchantapp.model.OTPCashierModel;
import mm.dummymerchant.sk.merchantapp.model.PhoneContactModel;
import mm.dummymerchant.sk.merchantapp.qrCode.QRCodeEncoder;

/**
 * Created by user on 12/7/2015.
 */
public class DigitalReceiptNew extends BaseActivity implements View.OnClickListener,Constant, AutoCompleCustomAdapter.AutoCompleteTextItemSelected
{


    CustomTextView text ,templete, qr_number,qr_time;
    String  OTPEnter_Latitude, OTPEnter_Longitude;

    private String Type;
    private CustomEdittext edt_customer_Amount;
    private CustomButton Submit, Otp_button;

    RadioGroup rg;
    RadioButton r1;
    int pos;
    int pos1;

    String  cashiername, cashierno, cashcollectorname, cashcollectorno, amount, cashierid;
    private String customername, customerno, encryptedMsg, qrInputText, type, encryptedMsgTime, qrInputTextTime;
    String cash_id, cash_name, cash_no, master_no;

    private ImageView imageView_qrcode;
    private String Generate_Latitude, Generate_Longitude, GenerateLocation;
   // private ImageView mCountryImage;
    NetworkOperatorModel model;
    int flag;

    private DBHelper dbb;

    static String msg_unable_to_generatePin;
    LinearLayout container;
    private ArrayList<String> value = new ArrayList<String>();
    private ArrayList<String> value1 = new ArrayList<String>();
    int childCount;
    private String editinput = "?";
    private Context context;

    //newly added
    LinearLayout ll_phoneNo_container;
    RelativeLayout rl_addnew;
    public static  ArrayList<PhoneContactModel> al_phonecontcts;
    LaterModel mod_later;

    String json_laterdata=null,  Edit_otp_value="";
    Bitmap bm_qR;
    EditText input;

    int from_home_or_list=0;
    LaterModel ll_model;
    String otpvalue = "", Time, OTPEnterTime, subject, countryName, OTPEnterLocation;
    String qrId;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_digital_screen1);

        msg_unable_to_generatePin = getString(R.string.unable_generate_pin);
        setExistingCashcollectorActionBar(String.valueOf(getResources().getText(R.string.generate_qr_code)));
        init();
        dbb = new DBHelper(this);
        Bundle b=getIntent().getExtras();
        try {
            from_home_or_list=  b.getInt(KEY_FROM_INT);
            loadDetailsFromList(b);
        }catch (Exception e)
        {
            e.printStackTrace();
        }


    }

    void init()
    {
        al_phonecontcts=new ArrayList<PhoneContactModel>();
        if (Utils.getSimOperater(this) != null) {
            if (!Utils.getSimOperater(this).equals("")) {
                model = new NetworkOperatorModel().getNetworkOperatorModel(this, Utils.getSimOperater(this));
            }
        }
        rg = (RadioGroup) findViewById(R.id.radioGroup1);
        r1 = (RadioButton) findViewById(R.id.radio0);

        edt_customer_Amount = (CustomEdittext) findViewById(R.id.et_digital_amount);

        Submit = (CustomButton) findViewById(R.id.Submit);
        Otp_button = (CustomButton) findViewById(R.id.Otp_button);
        imageView_qrcode = (ImageView) findViewById(R.id.imageView_qrcode);
        text = (CustomTextView)findViewById(R.id.text);
        templete = (CustomTextView)findViewById(R.id.templete);
        qr_number = (CustomTextView)findViewById(R.id.qr_number);
        qr_time = (CustomTextView)findViewById(R.id.qr_time);
        ll_phoneNo_container=(LinearLayout)findViewById(R.id.ll_contactlist);
        rl_addnew=(RelativeLayout)findViewById(R.id.adderlayout);



        Submit.setOnClickListener(this);
        Otp_button.setOnClickListener(this);
        rl_addnew.setOnClickListener(this);

        text.setOnClickListener(this);
        templete.setOnClickListener(this);


        type = TYPE_QRCODE;
        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // TODO Auto-generated method stub
                // Method 1 For Getting Index of RadioButton
                // Method 1
                pos = rg.indexOfChild(findViewById(checkedId));
                //Method 2
                pos1 = rg.indexOfChild(findViewById(rg.getCheckedRadioButtonId()));

                switch (pos) {
                    case 0:
                        r1.setChecked(true);
                        imageView_qrcode.setVisibility(View.INVISIBLE);
                        type = TYPE_QRCODE;
                        qr_number.setText("");
                        qr_time.setText("");
                        break;
                    case 1:
                        type = TYPE_SMS;
                        imageView_qrcode.setVisibility(View.INVISIBLE);
                        otpvalue = "";
                        qr_number.setText("");
                        qr_time.setText("");
                        break;

                    default:
                        type = TYPE_QRCODE;
                        break;
                }
            }

        });

        edt_customer_Amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                imageView_qrcode.setVisibility(View.GONE);
                qr_number.setText("");
                qr_time.setText("");
            }
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub
            }
            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });


    }

    @Override
    public <T> void response(int resultCode, T data) {

    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId()) {

            case R.id.Submit:
                submit(v);
                break;

            case R.id.Otp_button:

                if (!otpvalue.equals("")||from_home_or_list==1)
                {
                    Log.i("OTP Value ", otpvalue);
                    LayoutInflater layoutInflater = LayoutInflater.from(this);
                    View promptView = layoutInflater.inflate(R.layout.otp_prompts, null);
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                    alertDialogBuilder.setView(promptView);
                    final EditText input = (EditText) promptView.findViewById(R.id.userInput);
                    this.input=input;
                    final CustomButton btn_later=(CustomButton)promptView.findViewById(R.id.btn_later);
                    // setup a dialog window
                    alertDialogBuilder
                            .setCancelable(false)
                            .setPositiveButton(OK, new DialogInterface.OnClickListener()
                            {
                                public void onClick(DialogInterface dialog, int id)
                                {
                                    // get user input and set it to result
                                    Edit_otp_value = input.getText().toString().trim();
                                    DateFormat targetFormat = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
                                    Time = targetFormat.format(new Date());

                                    callOtp();
                                }
                            })
                            .setNegativeButton(CANCEL,

                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }

                                    });
                    // create an alert dialog
                  final  AlertDialog alertD = alertDialogBuilder.create();
                    alertD.show();

                    btn_later.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            if (bm_qR != null)
                            {   byte[] img = Utility.getBytes(bm_qR);
                                mod_later = new LaterModel(value, value1, al_phonecontcts, amount, editinput, qrId, img);

                                laterDatabaseStoring(mod_later);
                                alertD.dismiss();
                            }

                        }
                    });
                    //--------------


                }
                else {
                    String wrongText = getString(R.string.pls_entrblw_fields) + "\n";
                    if (otpvalue.equalsIgnoreCase(""))
                        wrongText += getString(R.string.gnr_qrcode) + "\n";
                    showToast(wrongText);
                    hideKeyboard();
                }


                break;





            case R.id.text:
                // Create custom dialog object
                final Dialog dialog = new Dialog(DigitalReceiptNew.this);
                // Include dialog.xml file
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.text_prompts);
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
                CustomTextView close  = (CustomTextView) dialog.findViewById(R.id.close);
                Button reset = (Button) dialog.findViewById(R.id.reset);
                Button ok = (Button) dialog.findViewById(R.id.ok);
                final EditText input = (EditText) dialog.findViewById(R.id.input);

                if(editinput.equals("?"))
                {
                    input.setText("");
                }
                else {
                    input.setText(editinput);
                }
                // if decline button is clicked, close the custom dialog
                reset.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        input.setText("");
                        editinput = "?";
                    }
                });
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {
                        String Edit_input_value = input.getText().toString().trim();
                        dialog.dismiss();
                        // editinput = Edit_input_value;
                        if(Edit_input_value.equals(""))
                        {
                            editinput = "?";
                        }
                        else {
                            editinput = Edit_input_value;
                        }
                        text.setText(Edit_input_value);
                    }
                });
                close.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v) {
                        // Close dialog
                        dialog.dismiss();
                    }
                });
                input.addTextChangedListener(new TextWatcher()
                {
                    @Override
                    public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                        imageView_qrcode.setVisibility(View.GONE);
                        qr_number.setText("");
                        qr_time.setText("");
                    }

                    @Override
                    public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                                  int arg3) {
                        // TODO Auto-generated method stub
                    }

                    @Override
                    public void afterTextChanged(Editable arg0) {

                    }
                });

                break;

            case R.id.templete:
                final Dialog dialog1 = new Dialog(DigitalReceiptNew.this);
                dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog1.setContentView(R.layout.template_prompts);
                dialog1.setCanceledOnTouchOutside(false);
                dialog1.show();
                CustomTextView add  = (CustomTextView) dialog1.findViewById(R.id.add);
                CustomTextView close1  = (CustomTextView) dialog1.findViewById(R.id.close);
                Button cancel = (Button) dialog1.findViewById(R.id.cancel);
                Button ok1 = (Button) dialog1.findViewById(R.id.ok);
                final CustomEdittext item = (CustomEdittext) dialog1.findViewById(R.id.item);
                final CustomEdittext qty = (CustomEdittext) dialog1.findViewById(R.id.qty);
                final CustomEdittext price = (CustomEdittext) dialog1.findViewById(R.id.price);
                container = (LinearLayout) dialog1.findViewById(R.id.container);
                int count = 0;
                for(int i=0 ; i<value.size();i++)
                {
                    try {
                        String s = value.get(0);
                        System.out.println(" 0 th" + " S value  " + s);
                        String[] separated = s.split("/");
                        item.setText(separated[0]);
                        qty.setText(separated[1]);
                        price.setText(separated[2]);
                        count++;
                    }
                    catch (ArrayIndexOutOfBoundsException e){
                    }
                }
                for(int i=0 ; i<count-1;i++)
                {
                    addFunction(container);
                }
                System.out.println("Count values "+count);
                System.out.println("value size " + value.size());
                for(int i=0 ; i<value1.size() ;i++)
                {
                    View childView1 = container.getChildAt(i);
                    CustomEdittext itemEdt = (CustomEdittext) (childView1.findViewById(R.id.item));
                    CustomEdittext qtyEdt = (CustomEdittext) (childView1.findViewById(R.id.qty));
                    CustomEdittext priceEdt = (CustomEdittext) (childView1.findViewById(R.id.price));
                    try {
                        String s = value1.get(i);
                        // System.out.println(i + " th" + " S value :childCount   " + s);
                        String[] separated = s.split("/");
                        /*System.out.println(i + " th" + " separated item value :childCount " + separated[0]);*/
                        if ((separated[0] != null) || !(separated[0].equals(""))) {
                            itemEdt.setText(separated[0]);
                        }
                        if ((separated[1] != null) || !(separated[1].equals(""))) {
                            qtyEdt.setText(separated[1]);
                        }
                        if ((separated[2] != null) || !(separated[2].equals(""))) {
                            priceEdt.setText(separated[2]);
                        }
                    }catch (ArrayIndexOutOfBoundsException e){
                    }
                }
                add.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // dialog1.dismiss();
                        addFunction(container);
                        imageView_qrcode.setVisibility(View.GONE);
                        qr_number.setText("");
                        qr_time.setText("");
                    }
                });

                close1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showToast(getString(R.string.dntdo));
                    }
                });
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog1.dismiss();
                    }
                });

                ok1.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v) {
                        dialog1.dismiss();
                        String itemvalue,qtyvalue,pricevalue;
                        itemvalue = item.getText().toString();
                        qtyvalue = qty.getText().toString();
                        pricevalue = price.getText().toString();
                        String SingleArrayIndex1,SingleArrayIndex2;
                        value.clear();
                        value1.clear();
                        childCount = container.getChildCount();
                        imageView_qrcode.setVisibility(View.GONE);
                        qr_number.setText("");
                        qr_time.setText("");
                        System.out.println("The ChildCount ok click: " + childCount);
                        if( !itemvalue.equals("") || !qtyvalue.equals("") || !pricevalue.equals(""))
                        {
                            SingleArrayIndex1 = itemvalue + "/" + qtyvalue + "/" + pricevalue ;
                            value.add(SingleArrayIndex1);
                        }
                        for (int ct = 0; ct < childCount; ct++)
                        {
                            View childView1 = container.getChildAt(ct);
                            CustomEdittext itemEdt = (CustomEdittext) (childView1.findViewById(R.id.item));
                            CustomEdittext qtyEdt = (CustomEdittext) (childView1.findViewById(R.id.qty));
                            CustomEdittext priceEdt = (CustomEdittext) (childView1.findViewById(R.id.price));

                            String item = itemEdt.getText().toString();
                            String qty = qtyEdt.getText().toString();
                            String price = priceEdt.getText().toString();

                            if( !item.equals("") || !qty.equals("") || !price.equals(""))
                            {
                                SingleArrayIndex2 = item + "/" + qty + "/" + price ;
                                value.add(SingleArrayIndex2);
                                value1.add(SingleArrayIndex2);
                            }
                            Log.i("ITEM ",item);
                            Log.i("QUANTITY ",qty);
                            Log.i("PRICE ", price);
                        }
                        Log.i("Array Value",value.toString());
                    }
                });

                break;

            case R.id.adderlayout:
                if(al_phonecontcts.size()<5)
                    cantactPickker();
                else
                    showToast(R.string.digi_add_members_limit);
                break;

            default:
                break;

        }
    }

    private void Finaldata(String customername, String customerno, String cash_name, String cash_no, String cash_id)
    {
        encryptedMsg = SAFETYCASHIER_TO_CASHCOLLECTOR + "-" + customername + "-" + customerno + "-" + amount + "-" + cash_name + "-" + cash_no + "-" + cash_id + "-" + getAllOtps() + "-" + Time + "-" + Generate_Latitude + "-" + Generate_Longitude + "-" + editinput + "-" + value.toString();

        cashcollectorname = customername;
        cashcollectorno = customerno;
        cashiername = cash_name;
        cashierno = cash_no;
        cashierid = cash_id;
        Encryt_Data(encryptedMsg);
    }

    private void Encryt_Data(String str_encryptedMsg) {
        DateFormat targetFormat = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
        Time=targetFormat.format(new Date());
        try {
            qrInputTextTime = AESCrypt.encrypt(SPEEDKey, encryptedMsgTime);
            qrInputText = AESCrypt.encrypt(encryptedMsgTime, str_encryptedMsg);
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        }
       /* if (generateQRCode(qrInputText) != null) {
            imageView_qrcode.setVisibility(View.VISIBLE);
            imageView_qrcode.setImageBitmap(generateQRCode(qrInputTextTime + "---" + qrInputText));
            Log.i("otpvalue ", otpvalue);
            qrId=generateQrId();
            if (cashcollectorname.equals(getResources().getString(R.string.Unknown))) {

                qrId=generateQrId();
                qr_number.setText(qrId);
                qr_time.setText(Time);
            } else {
                qr_number.setText(cashcollectorname + " - " + amount);
                qr_time.setText(Time);
            }
            hideKeyboard();
        }*/

        if (generateQRCode(qrInputText) != null) {
            imageView_qrcode.setVisibility(View.VISIBLE);
            Bitmap bm=generateQRCode(qrInputTextTime + "---" + qrInputText);
            bm_qR=bm;
            qrId=generateQrId();
            imageView_qrcode.setImageBitmap(bm_qR);

            // qr_number.setText(encryptNos() + " - " + amount);
            qr_number.setText("QR ID : "+qrId);
            qr_time.setText(Time);

            hideKeyboard();
        }
    }

    public Bitmap generateQRCode(String qrInputText) {
        //Find screen size
        WindowManager manager = (WindowManager) getSystemService(WINDOW_SERVICE);
        Display display = manager.getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);
        int width = point.x;
        int height = point.y;
        System.out.println("width" + width);
        System.out.println("height" + height);
        int smallerDimension = width < height ? width : height;
        smallerDimension = smallerDimension * 3 / 4;
        System.out.println("smallerDimension" + smallerDimension);
        //Encode with a QR Code image
        QRCodeEncoder qrCodeEncoder = new QRCodeEncoder(qrInputText,
                null,
                Contents.Type.TEXT,
                BarcodeFormat.QR_CODE.toString(),
                1000);
        try {
            Bitmap bitmap = qrCodeEncoder.encodeAsBitmap();
            Bitmap bitLogo = BitmapFactory.decodeResource(getResources(),
                    R.drawable.ok
            );
            Bitmap bitMerged = mergeBitmaps(bitmap, bitLogo);

            return bitMerged;


        } catch (WriterException e) {
            e.printStackTrace();

            return null;
        }
    }

    public static Bitmap mergeBitmaps(Bitmap bmp1, Bitmap bmp2) {
        Bitmap bmOverlay = Bitmap.createBitmap(bmp1.getWidth(),
                bmp1.getHeight(), bmp1.getConfig());
        int centreX = (bmp1.getWidth() - bmp2.getWidth()) / 2;
        int centreY = (bmp1.getHeight() - bmp2.getHeight()) / 2;
        Canvas canvas = new Canvas(bmOverlay);
        canvas.drawBitmap(bmp1, new Matrix(), null);
        canvas.drawBitmap(bmp2, centreX, centreY, null);


        return bmOverlay;
    }



    public static int generatePin() throws Exception {
        Random generator = new Random();
        generator.setSeed(System.currentTimeMillis());

        int num = generator.nextInt(99999) + 99999;
        if (num < 100000 || num > 999999) {
            num = generator.nextInt(99999) + 99999;
            if (num < 100000 || num > 999999) {
                throw new Exception(msg_unable_to_generatePin);
            }
        }
        return num;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode ==DIGITALRECEIPT_QR_REQUESTCODE)
        {
            Log.e("digienters..","enters");
            addFunction(ll_phoneNo_container, 0);
        }

    }

    @Override
    public void getItemSelectedValues(String name, String number) {

    }


   public void Get_Location() {
        GpsTracker1 gps = new GpsTracker1(getApplicationContext());
        Location loc = gps.getLocation();

        if (loc != null) {
            Generate_Latitude = "" + loc.getLatitude();
            Generate_Longitude = "" + loc.getLongitude();
            Log.i("Generate_Latitude", Generate_Latitude);
            Log.i("Generate_Longitude", Generate_Longitude);
        } else {
            Generate_Latitude = "0.0";
            Generate_Longitude = "0.0";
            Log.i("Generate_Latitude", Generate_Latitude);
            Log.i("Generate_Longitude", Generate_Longitude);
        }
    }


    private void takeScreenshot()
    {
        ScrollView sv = (ScrollView)findViewById(R.id.sendmoney_screb_bg);
        sv.scrollTo(0, sv.getBottom());
        Toast.makeText(this,"scroll happened",Toast.LENGTH_SHORT).show();
        Date now = new Date();
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);

        try {
            // image naming and path  to include sd card  appending name you choose for file
            String mPath = Environment.getExternalStorageDirectory().getAbsolutePath()+"/screen1.jpg";

            // create bitmap screen capture
            View v1 = getWindow().getDecorView().getRootView();
            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap = v1.getDrawingCache();

            File imageFile = new File(mPath);

            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();

            //  openScreenshot(imageFile);
            shareImage();
        } catch (Throwable e)
        {
            e.printStackTrace();

        }
    }

    private void shareImage()
    {
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("image/*");
        String imagePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/screen1.jpg";
        File imageFileToShare = new File(imagePath);
        Uri uri = Uri.fromFile(imageFileToShare);
        share.putExtra(Intent.EXTRA_STREAM, uri);
        startActivity(Intent.createChooser(share, "Share Image!"));
    }
    private void  addFunction(LinearLayout container_id)
    {
        final LinearLayout Container_id = container_id;
        LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View addView = layoutInflater.inflate(R.layout.row, null);
        final CustomEdittext item = (CustomEdittext) addView.findViewById(R.id.item);
        final CustomEdittext qty = (CustomEdittext) addView.findViewById(R.id.qty);
        final CustomEdittext price = (CustomEdittext) addView.findViewById(R.id.price);
        CustomTextView remove  = (CustomTextView) addView.findViewById(R.id.close);

        if(from_home_or_list==1)
        {
            item.setEnabled(false);
            qty.setEnabled(false);
            price.setEnabled(false);

            remove.setVisibility(View.GONE);
        }

        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((LinearLayout) addView.getParent()).removeView(addView);
            }
        });
        Container_id.addView(addView);
    }


    private void  addFunction(LinearLayout container_id,int j)
    {
        if( container_id.getChildCount()>0)
            container_id.removeAllViews();

        for(int i=0;i<al_phonecontcts.size();i++) {

            final LinearLayout Container_id = container_id;
            LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View addView = layoutInflater.inflate(R.layout.custom_phone_nolist, null);

            final CustomTextView tv_contact_name = (CustomTextView) addView.findViewById(R.id.tv_contact_name);
            final CustomTextView tv_phoneno = (CustomTextView) addView.findViewById(R.id.phoneno);
            final ImageView img_flag = (ImageView) addView.findViewById(R.id.flag);

            PhoneContactModel model = al_phonecontcts.get(i);
            tv_contact_name.setText(model.getName());
            tv_phoneno.setText(  model.getCountryCode()+ model.getPhoneNo());
            img_flag.setImageResource(model.getInt_flag_image());

            Container_id.addView(addView);
        }
    }


    String encryptNameandOthers()
    {
        String str="";

        for(int i=0;i<al_phonecontcts.size();i++)
        {
            if(al_phonecontcts.size()>1) {

                if (i < al_phonecontcts.size() - 1) {
                    String name = al_phonecontcts.get(i).getName();
                    String completeNo = al_phonecontcts.get(i).getCompletePhoneNo();
                    String otp = al_phonecontcts.get(i).getOtp();
                    String status = "" + al_phonecontcts.get(i).isStatus();

                    str = str  + name + ":" + otp + ":" + status+"#";
                }
                else
                {
                    String name = al_phonecontcts.get(i).getName();
                    String completeNo = al_phonecontcts.get(i).getCompletePhoneNo();
                    String otp = al_phonecontcts.get(i).getOtp();
                    String status = "" + al_phonecontcts.get(i).isStatus();

                    str = str + "" + name + ":" + otp + ":" + status;
                }
            }
            else
            {
                if(al_phonecontcts.size()==1)
                {
                    String name = al_phonecontcts.get(i).getName();
                    String completeNo = al_phonecontcts.get(i).getCompletePhoneNo();
                    String otp = al_phonecontcts.get(i).getOtp();
                    String status = "" + al_phonecontcts.get(i).isStatus();

                    str = str  + name + ":" + otp + ":" + status+"#";
                }
            }
        }
        return str;
    }


    String encryptNos()
    {
        String str="";

        for(int i=0;i<al_phonecontcts.size();i++)
        {
        /*  String completeNo=al_phonecontcts.get(i).getCompletePhoneNo();

            str=str+completeNo+":";*/

            if(al_phonecontcts.size()>1)
            {
                if (i < al_phonecontcts.size() - 1) {
                    String completeNo= al_phonecontcts.get(i).getCompletePhoneNo();

                    str = str + completeNo + ":";
                }
                else
                {
                    String completeNo = al_phonecontcts.get(i).getCompletePhoneNo();

                    str = str + completeNo ;
                }
            }
            else
            {
                if(al_phonecontcts.size()==1)
                {
                    String completeNo = al_phonecontcts.get(i).getCompletePhoneNo();

                    str = str + completeNo ;
                }
            }
        }


        return str;
    }

    String getAllOtps()
    {
        String otp="";

        for(int i=0;i<al_phonecontcts.size();i++)
        {
            if(al_phonecontcts.size()>1)
            {
                if (i < al_phonecontcts.size() - 1) {
                    String st_otp = al_phonecontcts.get(i).getOtp();

                    otp = otp + st_otp + ":";
                }

                else
                {
                    String st_otp = al_phonecontcts.get(i).getOtp();

                    otp = otp + st_otp ;
                }
            }
            else
            {
                if(al_phonecontcts.size()==1)
                {
                    String st_otp = al_phonecontcts.get(i).getOtp();

                    otp = otp + st_otp ;
                }
            }


        }
        return otp;
    }

    void submit(View v)
    {
        DateFormat targetFormat = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
        OTPEnterTime = targetFormat.format(new Date());

        System.out.println("text editinput value " + editinput);
        System.out.println("template ArrayList Value " + value.toString());

        subject = edt_customer_Amount.getText().toString().trim();
        Log.i("customersubject", subject);

        Get_Location();
        Log.i("Latitude after", Generate_Latitude);
        Log.i("Longitude after", Generate_Longitude);
        GenerateLocation = Generate_Latitude + "," + Generate_Longitude;
        Log.i("GenerateLocation", GenerateLocation);
        MasterDetails cc_profile = dbb.getMasterDetails();
        cash_name= cc_profile.getUsername();
         cash_no = AppPreference.getLoginuserName(this);

        int ScanerNumber_len = encryptNos().length();
        System.out.println("ScanerNumber length : " + ScanerNumber_len);


        amount = edt_customer_Amount.getText().toString();

        Time = targetFormat.format(new Date());
        encryptedMsgTime = Time +"----" + encryptNos();

        try {
            otpvalue = String.valueOf(generatePin());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (type.equals(TYPE_QRCODE))
        {
            Finaldata(encryptNameandOthers(),encryptNos(),cash_name,cash_no,cash_id);
        }
        else {
            Finaldata(encryptNameandOthers(),encryptNos(),cash_name,cash_no,cash_id);
            takeScreenshot();
        }


        hideKeyboard();


    }

    String getScaannerNumber(String otp)
    {
        String no=null;
        for(int i=0;i<al_phonecontcts.size();i++)
        {
            PhoneContactModel model=al_phonecontcts.get(i);
            String model_otp=model.getOtp();

            if(otp.equals(model_otp)) {
                no= model.getCompletePhoneNo();
                return no;
            }
        }
        return no;
    }

    String getScannerName(String otp)
    {
        String name=null;
        for(int i=0;i<al_phonecontcts.size();i++)
        {
            PhoneContactModel model=al_phonecontcts.get(i);
            String model_otp=model.getOtp();

            if(otp.equals(model_otp)) {
                name= model.getName();
                return name;
            }
        }
        return name;
    }

    String getOtp(String otp)
    {

        String otp_value=null;
        for(int i=0;i<al_phonecontcts.size();i++)
        {
            PhoneContactModel model=al_phonecontcts.get(i);
            String model_otp=model.getOtp();

            if(otp.equals(model_otp)) {
                otp_value= model.getOtp();
                return otp_value;
            }
        }
        return otp_value;

    }

    void setStausofOtp(String otp)
    {
        for(int i=0;i<al_phonecontcts.size();i++)
        {
            PhoneContactModel model=al_phonecontcts.get(i);
            String model_otp=model.getOtp();

            if(otp.equals(model_otp)) {
                al_phonecontcts.get(i).setStatus(true);
            }
        }

    }


    boolean checkOtp(String otpEntered)
    {
        boolean status=false;

        for(int i=0;i<al_phonecontcts.size();i++)
        {
            String otp=al_phonecontcts.get(i).getOtp();

            if(otp.equals(otpEntered))
            {
                status=true;
                return status;
            }
        }

        return status;
    }


    void updateQrCode(String otp_given)
    {
        DateFormat targetFormat = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
        subject = edt_customer_Amount.getText().toString().trim();
        Log.i("customersubject", subject);

        Get_Location();
        Log.i("Latitude after", Generate_Latitude);
        Log.i("Longitude after", Generate_Longitude);
        GenerateLocation = Generate_Latitude + "," + Generate_Longitude;
        Log.i("GenerateLocation", GenerateLocation);

        MasterDetails cc_profile = dbb.getMasterDetails();
       cash_no = cc_profile.getUsername();
        cash_name = AppPreference.getLoginuserName(this);

        int ScanerNumber_len = encryptNos().length();
        System.out.println("ScanerNumber length : " + ScanerNumber_len);


        amount = edt_customer_Amount.getText().toString();

        Time = targetFormat.format(new Date());
        encryptedMsgTime = Time +"----" + encryptNos();

        for(int i=0;i<al_phonecontcts.size();i++)
        {
            if(otp_given.equals(al_phonecontcts.get(i).getOtp()))
            {
                al_phonecontcts.get(i).setStatus(true);
                al_phonecontcts.get(i).setOtp("verified");

                Finaldata(encryptNameandOthers(), encryptNos(), cash_name, cash_no, cash_id);
                return;
            }
        }
    }

    void updateDataBaseData()
    {
        db.open();
        String jsondata=null;

        if(bm_qR!=null)
        {
            byte[] img = Utility.getBytes(bm_qR);
            mod_later = new LaterModel(value, value1, al_phonecontcts, amount, editinput, qrId, img);
            jsondata= new Utils().convertLaterModelToJsonString(mod_later);
        }
        DateFormat targetFormat = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
        Time = targetFormat.format(new Date());
        encryptedMsgTime = Time +"----" + encryptNos();

        for (int i=0;i<al_phonecontcts.size();i++)
        {
            if(!al_phonecontcts.get(i).isStatus())
            {
                String no=al_phonecontcts.get(i).getCompletePhoneNo();
                dbb.updateCollectortoCollectorScanDetails(no, jsondata);
            }
        }

        db.close();
    }




    void laterDatabaseStoring(LaterModel mod_later)
    {
        Utils utils=new Utils();
        json_laterdata="  ";
        json_laterdata= utils.convertLaterModelToJsonString(mod_later);

        db.open();

        for(int i=0;i<al_phonecontcts.size();i++)
        {
            PhoneContactModel model=al_phonecontcts.get(i);

            if(!model.isStatus())
            {

                DateFormat targetFormat = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
                OTPEnterTime = targetFormat.format(new Date());

                Get_OTPEnter_Location();
                OTPEnterLocation = OTPEnter_Latitude + "," + OTPEnter_Longitude;

                OTPCashierModel cashout_details = new OTPCashierModel(Time, cashiername, cashierno, cashcollectorname, cashcollectorno, amount, LATER, cashierid, "", editinput, value.toString(), json_laterdata);
                db.insertOTPCashierDetails(cashout_details);
                setStausofOtp(otpvalue);

                showToast(getString(R.string.rightotp));
                if(input!=null)
                    input.setText("");
                edt_customer_Amount.setText("");
                //  edt_customer_mobile.setText("");
                edt_customer_Amount.setText("");
                qr_number.setText("");
                qr_time.setText("");
                imageView_qrcode.setVisibility(View.GONE);
                text.setText("");
            }

            db.close();
        }

        // updateQrCode(Edit_otp_value);
    }

    void loadDetailsFromList(Bundle b)
    {
        json_laterdata = b.getString(KEY_FROM);
        Utils utils=new Utils();
        ll_model=   utils.getLaterModelFromJson(json_laterdata);

        value =ll_model.getValue();
        value1 = ll_model.getValue1();
        al_phonecontcts=ll_model.getAl_phonecontacts();
        amount = ll_model.getAmount();
        editinput=ll_model.getText();
        bm_qR = Utility.getPhoto(ll_model.getImage_qr());
        qrId=ll_model.getQrId();


        rl_addnew.setVisibility(View.GONE);

        addFunction(ll_phoneNo_container, 0);
        edt_customer_Amount.setText(amount);
        edt_customer_Amount.setEnabled(false);

        text.setText(editinput);
        qr_number.setText(qrId);
        // qr_time.setText("2222");
        imageView_qrcode.setVisibility(View.VISIBLE);
        imageView_qrcode.setImageBitmap(bm_qR);

    }



    void showOtp()
    {
        String text="";
        for(int i=0;i<al_phonecontcts.size();i++)
        {
            text+=    al_phonecontcts.get(i).getOtp()+":";
        }

        showToast(text);
    }

    public void Get_OTPEnter_Location()
    {
        GpsTracker1 gps = new GpsTracker1(getApplicationContext());
        Location loc = gps.getLocation();

        if (loc != null) {
            OTPEnter_Latitude = "" + loc.getLatitude();
            OTPEnter_Longitude = "" + loc.getLongitude();
            Log.i("OTPEnter_Latitude",OTPEnter_Latitude);
            Log.i("OTPEnter_Longitude",OTPEnter_Longitude);
        }
        else {
            OTPEnter_Latitude = "0.0";
            OTPEnter_Longitude = "0.0";
            Log.i("OTPEnter_Latitude", OTPEnter_Latitude);
            Log.i("OTPEnter_Longitude", OTPEnter_Longitude);
        }
    }


    void callOtp()
    {
        Log.e("Edit_otp_value..before","..."+Edit_otp_value);
        if(checkOtp(Edit_otp_value))
        {
            if(from_home_or_list==1)
                {
                    //code for stored data
                    OTPCashierModel cashout_details = new OTPCashierModel(Time, getScannerName(Edit_otp_value), getScaannerNumber(Edit_otp_value), cashcollectorname, cashcollectorno, amount, SUCCESS, cashierid, "", editinput, value.toString(), "");
                    db.updateCollectortoCollectorScanDetails(getScaannerNumber(Edit_otp_value),json_laterdata);
                    showToast1(getString(R.string.rightotp));
                    input.setText("");
                    edt_customer_Amount.setText("");

                    qr_number.setText("");
                    qr_time.setText("");
                    imageView_qrcode.setVisibility(View.GONE);
                    text.setText("");
                }
                else {
                    OTPCashierModel cashout_details = new OTPCashierModel(Time, getScannerName(Edit_otp_value), getScaannerNumber(Edit_otp_value), cashcollectorname, cashcollectorno, amount, SUCCESS, cashierid, "", editinput, value.toString(), "");
                    db.insertOTPCashierDetails(cashout_details);
                    showToast1(getString(R.string.rightotp));
                    input.setText("");
                    edt_customer_Amount.setText("");

                    qr_number.setText("");
                    qr_time.setText("");
                    imageView_qrcode.setVisibility(View.GONE);
                    text.setText("");
                }


        }
        else
        {
            String wrongText = getString(R.string.pls_entrblw_fields) + "\n";
            if (Edit_otp_value.equalsIgnoreCase("")||Edit_otp_value==null)
                wrongText += getString(R.string.entr_otp) + "\n";
            showToast(wrongText);
            hideKeyboard();
        }
    }


    private void cantactPickker()
    {
        Intent call = new Intent(this, DigitalPhoneNoActivity.class);
        startActivityForResult(call, DIGITALRECEIPT_QR_REQUESTCODE);
    }

    private SecureRandom random = new SecureRandom();

    public String generateQrId() {
        return new BigInteger(30, random).toString(32);
    }

    void backLater()
    {

        if (bm_qR != null) {
            showToast("button clicked");
            byte[] img = Utility.getBytes(bm_qR);
            mod_later = new LaterModel(value, value1, al_phonecontcts, amount, editinput, qrId, img);
            laterDatabaseStoring(mod_later);

        }
    }


    void backDialog()
    {
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.backdialog, null);
        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(this);

        alertDialogBuilder.setView(promptView);
        final CustomTextView txt = (CustomTextView) promptView.findViewById(R.id.txtmsg);
        txt.setText(R.string.digi_back_alert);


        // setup a dialog window
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton(OK, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (al_phonecontcts.size() != 0 && imageView_qrcode.getVisibility() == View.VISIBLE) {
                            if (from_home_or_list != 1)
                                backLater();
                            finish();
                        } else {
                            finish();
                        }

                    }

                })
                .setNegativeButton(CANCEL,

                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }

                        });
        // create an alert dialog
        final android.support.v7.app.AlertDialog alertD = alertDialogBuilder.create();
        alertD.show();
    }

    @Override
    public void onBackPressed() {
        backDialog();

    }



}

