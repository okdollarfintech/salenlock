package mm.dummymerchant.sk.merchantapp;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.android.Contents;

import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.Utils;
import mm.dummymerchant.sk.merchantapp.customView.CustomButton;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
import mm.dummymerchant.sk.merchantapp.qrCode.QRCodeEncoder;


public class Enter_URL extends Activity {
    Button generate,add,clear;
    ImageView image;
    LinearLayout linearLayout;
    EditText url;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter__url);
//        if(getIntent().hasExtra("url_"))
//        {
//            if(!AppPreference.getALLUrl(this).equals(""))
//            AppPreference.setALLUrl(this,getIntent().getExtras().getString("url_")+"$"+AppPreference.getALLUrl(this));
//        }
        generate = (Button) findViewById(R.id.generate_qr);
        generate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Utils.isEmpty(url.getText().toString().trim())) {
                    showMessageError("url cannot be null");
                    return;
                }
                url.setEnabled(false);
                generateQRCode(url.getText().toString());
            }
        });
        image = (ImageView) findViewById(R.id.qr_code);
        linearLayout = (LinearLayout) findViewById(R.id.linear_layout);
        clear = (Button) findViewById(R.id.clear);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                url.setText("");
                generateQRCode("");
                AppPreference.setALLUrl(Enter_URL.this, "");
                AppPreference.setMoreUrl(Enter_URL.this,false);
            }
        });
        url = (EditText) findViewById(R.id.url);
        if(!AppPreference.getALLUrl(this).equals("")) {
            url.setText(AppPreference.getALLUrl(this));
            generateQRCode(url.getText().toString());
        }
        add = (Button) findViewById(R.id.add);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMessage("Enter Or Paste Url");
            }
        });

    }



    private void generateQRCode(String qrInputText) {
        try {QRCodeEncoder qrCodeEncoder = new QRCodeEncoder(qrInputText,
                null,
                Contents.Type.TEXT,
                BarcodeFormat.QR_CODE.toString(),
                150);

        image.setImageBitmap(qrCodeEncoder.encodeAsBitmap());


        } catch (WriterException e) {
            e.printStackTrace();

        }
    }
    private void showMessageError(String message) {
        MediaPlayer p = MediaPlayer.create(this, R.raw.error_tune);
        p.start();
        ((Vibrator) getSystemService(VIBRATOR_SERVICE)).vibrate(200);
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.alert_window, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptsView);
        final CustomTextView userInput = (CustomTextView) promptsView.findViewById(R.id.alert);
        userInput.setText(message);
        CustomButton ok = (CustomButton) promptsView.findViewById(R.id.btn_settings);
        final AlertDialog dialog1 = alertDialogBuilder.create();
        ok.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog1.dismiss();

            }
        });
        dialog1.show();
    }
    private void showMessage(String message) {
        MediaPlayer p = MediaPlayer.create(this, R.raw.error_tune);
        p.start();
        ((Vibrator) getSystemService(VIBRATOR_SERVICE)).vibrate(200);
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.enter_url, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptsView);
        final CustomTextView userInput = (CustomTextView) promptsView.findViewById(R.id.alert);
        userInput.setText(message);
        CustomButton ok = (CustomButton) promptsView.findViewById(R.id.btn_settings);
        CustomButton cancel = (CustomButton) promptsView.findViewById(R.id.btn_cancel);
        CustomButton upload_files = (CustomButton) promptsView.findViewById(R.id.upload_files);


        final EditText more = (EditText) promptsView.findViewById(R.id.urll);
        final AlertDialog dialog1 = alertDialogBuilder.create();
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(more.getText()!=null) {
                    if (more.getText().toString().trim().length() > 1) {
                        url.setText(url.getText().toString() + "$" + more.getText().toString());
                        generateQRCode(url.getText().toString());
                    }
                }
                dialog1.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog1.dismiss();
            }
        });
        upload_files.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Enter_URL.this,CameraScreenShot.class);
                startActivity(intent);
                dialog1.dismiss();
                finish();

            }
        });
        dialog1.show();
    }



}
