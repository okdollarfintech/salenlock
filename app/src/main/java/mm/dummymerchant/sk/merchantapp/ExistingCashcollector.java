package mm.dummymerchant.sk.merchantapp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Locale;

import mm.dummymerchant.sk.merchantapp.db.Utility;
import mm.dummymerchant.sk.merchantapp.model.CashCollectorModel;

/**
 * Created by user on 11/11/2015.
 */
public class ExistingCashcollector extends BaseActivity
{
//implements AdapterView.OnItemClickListener
    private SwipeMenuListView lv;
  Cash_Collector_Adapter adapter;
    CashCollectorModel cellvalue;
    private ArrayList<CashCollectorModel> AlertList=new ArrayList<CashCollectorModel>();
    ArrayList<CashCollectorModel> arraylist = new ArrayList<CashCollectorModel>();
    String From,To;
    EditText inputSearch;

    public static CashCollectorModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exist_cashcollector);
        init();
        setExistingCashcollectorActionBar(String.valueOf(getResources().getText(R.string.existingcashcollector)));

        Intent i =getIntent();
        From = i.getStringExtra("From");
        Log.i("From ", From);
        loadList();
        lv.setMenuCreator(creator);

        //Quick search functionality
        inputSearch = (EditText) findViewById(R.id.editsearch);
        inputSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {

                if (inputSearch.getText().toString().equals("")) {
                    Log.i("inputSearch ", "inputSearch have no values");
                }
                String text = inputSearch.getText().toString().toLowerCase(Locale.getDefault());
                Log.i("OnTextChanged", text);
                adapter.filter(text);
            }
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub
            }
            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });

        //----------
        lv.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index)
            {
                //showToast("delete clicked");
                cellvalue = (CashCollectorModel) adapter.getItem(position);

                db.deleteEmpDetails(cellvalue);
                loadList();
                lv.setMenuCreator(creator);

              /*  dbb.open();
                dbb.deleteUpdateCashierDetails(cashierModel);
                dbb.close();
                processListFromActionBar();*/


                return false;
            }
        });

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
               // showToast("clicked");
                model=(CashCollectorModel)adapter.getItem(i);


                    Bitmap bitmap,bitmap1;
                    bitmap= model.getPhoto();
                    bitmap1 = model.getBitmap_Sign();
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    ByteArrayOutputStream baos1 = new ByteArrayOutputStream();

                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
                    bitmap1.compress(Bitmap.CompressFormat.PNG, 100, baos1);

                    byte[] b = baos.toByteArray();
                    byte[] b1 = baos1.toByteArray();

                    if(From.equals("Dashboard"))
                    {
                        Intent intent=new Intent(ExistingCashcollector.this,SingleViewActivity_Qrcode.class);
                        intent.putExtra("picture", b);
                        intent.putExtra("Name", model.getName());
                        intent.putExtra("No", model.getNumber());
                        startActivity(intent);
                    }
                    else
                    {
                        Intent intent =new Intent(ExistingCashcollector.this,Edit_CashCollector.class);
                        startActivity(intent);
                    }
                }
        });


    }
    void init()
    {
        lv = (SwipeMenuListView)findViewById(R.id.listView);
    }

    @Override
    public <T> void response(int resultCode, T data) {

    }
    public class Cash_Collector_Adapter extends BaseAdapter
    {
        Context context;
        Activity activity;
        ArrayList<CashCollectorModel> AlertList;

        public Cash_Collector_Adapter(Context context, ArrayList<CashCollectorModel> AlertList)
        {
            this.context = context;
            this.AlertList = AlertList;
            arraylist=AlertList;
        }
        private class ViewHolder
        {
            TextView name,source;
            ImageView user_photo;
        }
        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return AlertList.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return AlertList.get(position);
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return AlertList.indexOf(getItem(position));
        }

        @SuppressLint("InflateParams")
        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            final ViewHolder holder;

            LayoutInflater mInflater = (LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            if (convertView == null)
            {
                convertView = mInflater.inflate(R.layout.row_item_cashcollector, null);
                holder = new ViewHolder();

                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.source = (TextView) convertView.findViewById(R.id.source);
                holder.user_photo = (ImageView) convertView.findViewById(R.id.user_photo);

                convertView.setTag(holder);
            }
            else
            {
                holder = (ViewHolder) convertView.getTag();
            }

            final CashCollectorModel m = AlertList.get(position);

            holder.name.setText(m.getName());
            holder.source.setText(m.getSource());
            holder.user_photo.setImageBitmap(m.getPhoto());

           /* convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {

                    Bitmap bitmap,bitmap1;
                    bitmap= m.getPhoto();
                    bitmap1 = m.getBitmap_Sign();
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    ByteArrayOutputStream baos1 = new ByteArrayOutputStream();

                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
                    bitmap1.compress(Bitmap.CompressFormat.PNG, 100, baos1);

                    byte[] b = baos.toByteArray();
                    byte[] b1 = baos1.toByteArray();

                    if(From.equals("Dashboard"))
                    {
                        Intent intent=new Intent(ExistingCashcollector.this,SingleViewActivity_Qrcode.class);
                        intent.putExtra("picture", b);
                        intent.putExtra("Name", m.getName());
                        intent.putExtra("No", m.getNumber());
                        startActivity(intent);
                    }
                    else
                    {
                        // i.e old code
                      *//*  Intent intent=new Intent(ExistingCashcollector.this,SingleViewActivity.class);
                        intent.putExtra("picture", b);
                        intent.putExtra("Name", m.getName());
                        intent.putExtra("No", m.getNumber());
                        intent.putExtra("Source", m.getSource());
                        intent.putExtra("picturee", b1);
                        startActivity(intent);*//*

                        model=(CashCollectorModel)adapter.getItem(i);
                        Intent intent =new Intent(ExistingCashcollector.this,Edit_CashCollector.class);
                        startActivity(intent);

                    }
                }
            });*/
            return convertView;
        }


        public void filter(String charText)
        {
            ArrayList<CashCollectorModel> movieItems3 = new ArrayList<CashCollectorModel>();
            System.out.println("Enter filter function in adapter class");
            charText = charText.toLowerCase(Locale.getDefault());
            System.out.println("Enter arraylist size before if else -->" + arraylist.size());
            if (charText.length() == 0)
            {
                movieItems3=arraylist;
            }
            else
            {
                System.out.println("Enter else size-->"+arraylist.size());
                for (CashCollectorModel wp :arraylist)
                {
                    System.out.println("Enter for-->");
                    System.out.println("The get(0) value is-->"+wp.getName());

                    if (wp.getName().toLowerCase(Locale.getDefault()).contains(charText))
                    {
                        movieItems3.add(wp);
                    }
                }
                System.out.println("IN else End movieItems3 Value -->"+movieItems3.toString());
            }

            AlertList= movieItems3;
            System.out.println("Final OrderList Value -->" + AlertList.toString());
            notifyDataSetChanged();
        }
    }

    SwipeMenuCreator creator = new SwipeMenuCreator() {

        @Override
        public void create(SwipeMenu menu) {
            // create "open" item
            SwipeMenuItem openItem = new SwipeMenuItem(
                    getApplicationContext());
            // set item background


            // create "delete" item
            SwipeMenuItem deleteItem = new SwipeMenuItem(
                    getApplicationContext());
            // set item background
            deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                    0x3F, 0x25)));
            // set item width
            deleteItem.setWidth(dp2px(90));

            // set a icon
            deleteItem.setIcon(R.drawable.ic_delete);
            // add to menu
            menu.addMenuItem(deleteItem);
        }
    };


    ArrayList<CashCollectorModel> getArraylistCashModel()
    {
        ArrayList<CashCollectorModel> AlertList=null;

        Cursor c = db.Get_CASHCOLLECTOR_TABLE();
        if (c.getCount() > 0)
        {
            AlertList=new ArrayList<CashCollectorModel>();
            Log.i("ServingCell_Page", "Enter IF Contion");

            if (c.moveToFirst())
            {
                while (c.isAfterLast() == false)
                {
                    cellvalue = new CashCollectorModel();
                    Log.i("CashCollectorModel", "READIN-Get_CashCollectorModel");

                    byte[] blob_photo = c.getBlob(1);
                    byte[] blob_sign = c.getBlob(5);
                    cellvalue.setPhoto(Utility.getPhoto(blob_photo));
                    cellvalue.setName(c.getString(2));
                    cellvalue.setNumber(c.getString(3));
                    cellvalue.setSource(c.getString(4));
                    cellvalue.setSign(Utility.getPhoto(blob_sign));

                    AlertList.add(cellvalue);

                    c.moveToNext();
                }
            }

        } else
        {
            //Toast.makeText(getApplicationContext(), "No Values", Toast.LENGTH_SHORT).show();
            showToast(getString(R.string.no_record));
            return AlertList;
        }

        return AlertList;
    }

    void loadList()
    {
        ArrayList<CashCollectorModel> AlertList=null;

        Cursor c = db.Get_CASHCOLLECTOR_TABLE();

        if (c.getCount() > 0)
        {
            AlertList=new ArrayList<CashCollectorModel>();
            Log.i("ServingCell_Page", "Enter IF Contion");

            if (c.moveToFirst()) {
                while (c.isAfterLast() == false) {
                    cellvalue = new CashCollectorModel();
                    Log.i("CashCollectorModel", "READIN-Get_CashCollectorModel");

                    byte[] blob_photo = c.getBlob(1);
                    byte[] blob_sign = c.getBlob(6);
                    cellvalue.setId(""+c.getInt(0));
                    cellvalue.setPhoto(Utility.getPhoto(blob_photo));
                    cellvalue.setName(c.getString(2));
                    cellvalue.setNumber(c.getString(3));
                    cellvalue.setSource(c.getString(4));
                    cellvalue.setSign(Utility.getPhoto(blob_sign));
                    cellvalue.setStatus(c.getString(5));
                    //cellvalue.setStatus(c.getString(6));

                    AlertList.add(cellvalue);

                    c.moveToNext();
                }
                adapter =new Cash_Collector_Adapter(getApplicationContext(), AlertList);
                lv.setAdapter(adapter);
            }

        } else
        {
            AlertList=new ArrayList<CashCollectorModel>();
            adapter =new Cash_Collector_Adapter(getApplicationContext(), AlertList);
            lv.setAdapter(adapter);
            //Toast.makeText(getApplicationContext(), "No Values", Toast.LENGTH_SHORT).show();
            showToast(getString(R.string.no_record));
            finish();
        }

        Intent i =getIntent();
        From = i.getStringExtra("From");
    }
}
