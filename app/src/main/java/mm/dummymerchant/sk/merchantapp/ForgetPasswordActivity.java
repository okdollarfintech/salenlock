package mm.dummymerchant.sk.merchantapp;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TableRow;
import android.widget.Toast;

import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;


public class ForgetPasswordActivity extends BaseActivity implements View.OnClickListener {

    private TableRow email, facebook, viber, whatsUp;
    private CustomTextView callMobile1, callMobile2, smsMobile1, smsMobile2, callLandline;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        setForgotpwdsActionBar();
        buttonLogout.setVisibility(View.GONE);
        email= (TableRow) findViewById(R.id.email);
        facebook= (TableRow) findViewById(R.id.facebook);
        viber= (TableRow) findViewById(R.id.viber);
        whatsUp= (TableRow) findViewById(R.id.whats_up);
        callLandline= (CustomTextView) findViewById(R.id.landline);
        callMobile1= (CustomTextView) findViewById(R.id.call_mobile1);
        callMobile2= (CustomTextView) findViewById(R.id.call_mobile2);
        smsMobile1= (CustomTextView) findViewById(R.id.sms_mobile1);
        smsMobile2= (CustomTextView) findViewById(R.id.sms_mobile2);

        //setUnderLine(getResources().getString(R.string.customer_care), callLandline);
        //setUnderLine(getResources().getString(R.string.mobile_no1), callMobile1);
        //setUnderLine(getResources().getString(R.string.mobile_no2), callMobile2);
        //setUnderLine(getResources().getString(R.string.mobile_no1), smsMobile1);
        //setUnderLine(getResources().getString(R.string.mobile_no2), smsMobile2);

        callLandline.setOnClickListener(this);
        callMobile1.setOnClickListener(this);
        callMobile2.setOnClickListener(this);
        smsMobile1.setOnClickListener(this);
        smsMobile2.setOnClickListener(this);
        facebook.setOnClickListener(this);
        email.setOnClickListener(this);
        viber.setOnClickListener(this);
        whatsUp.setOnClickListener(this);
        backFinish();
    }
    @Override
    public <T> void response(int resultCode, T data) {

    }
    private void backFinish() {
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                finish();
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.email:
                openEmail();
                break;
            case R.id.facebook:
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.fb_link)));
                startActivity(browserIntent);
                break;
            case R.id.viber:
               openViber();
                break;
            case R.id.whats_up:
                onClickWhatsApp();
                break;
            case R.id.landline:
                openCallDialer("+95-1-377888");
                break;
            case R.id.call_mobile1:
                openCallDialer("+95-930000066");
                break;
            case R.id.call_mobile2:
                openCallDialer("+95-9450144455");
                break;
            case R.id.sms_mobile1:
                openMessageApp("+95-930000066");
                break;
            case R.id.sms_mobile2:
                openMessageApp("+95-9450144455");
                break;
        }
    }
    private void openEmail(){
        final Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{getResources().getString(R.string.email)});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.customer_care_feedback));
        //emailIntent.putExtra(Intent.EXTRA_TEXT, "Some body");
        startActivity(Intent.createChooser(emailIntent, "Send E-mail"));

    }

    private void openViber(){

        PackageManager pm=getPackageManager();
        try {

            Intent waIntent = new Intent(Intent.ACTION_SEND);
            waIntent.setType("text/plain");
            String text = "";
            PackageInfo info=pm.getPackageInfo("com.viber.voip", PackageManager.GET_META_DATA);
            waIntent.setPackage("com.viber.voip");
            waIntent.putExtra(Intent.EXTRA_TEXT, text);
            startActivity(Intent.createChooser(waIntent, "Share with"));

        } catch (PackageManager.NameNotFoundException e) {
            Toast.makeText(this, R.string.viber_not_install, Toast.LENGTH_SHORT)
                    .show();
        }
    }

    public void onClickWhatsApp() {

        PackageManager pm=getPackageManager();
        try {

            Intent waIntent = new Intent(Intent.ACTION_SEND);
            waIntent.setType("text/plain");
            String text = "";
            PackageInfo info=pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
            waIntent.setPackage("com.whatsapp");
            waIntent.putExtra(Intent.EXTRA_TEXT, text);
            startActivity(Intent.createChooser(waIntent, "Share with"));

        } catch (PackageManager.NameNotFoundException e) {
            Toast.makeText(this, R.string.whatsup_not_install, Toast.LENGTH_SHORT)
                    .show();
        }
    }

    private void openCallDialer(String mobileNo){
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + mobileNo));
        startActivity(intent);
    }

    private void openMessageApp(String mobileNo){
        Intent intentsms = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:" + mobileNo));
        intentsms.putExtra("sms_body", "");
        startActivity(intentsms);
    }

}