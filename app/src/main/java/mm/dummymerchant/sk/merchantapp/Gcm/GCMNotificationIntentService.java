package mm.dummymerchant.sk.merchantapp.Gcm;

import android.app.ActivityManager;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import mm.dummymerchant.sk.merchantapp.NotificationActivity;
import mm.dummymerchant.sk.merchantapp.R;
import mm.dummymerchant.sk.merchantapp.SliderScreen;
import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.Constant;
import mm.dummymerchant.sk.merchantapp.Utils.Utils;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
import mm.dummymerchant.sk.merchantapp.db.DBHelper;
import mm.dummymerchant.sk.merchantapp.model.CashierModel;
import mm.dummymerchant.sk.merchantapp.model.ConstactModel;
import mm.dummymerchant.sk.merchantapp.model.TransationModel;

/**
 * Created by user on 10/31/2015.
 */
public class GCMNotificationIntentService extends IntentService implements Animation.AnimationListener,Constant {

    public static int id = 0;
    private CustomTextView tv;
    private WindowManager wm;
    private WindowManager.LayoutParams layOutParams;
    private View view;
    private RelativeLayout llNotification;
    private LinearLayout llNotificationAnim;
    private static int test = 0;

    public GCMNotificationIntentService() {
        super("GcmIntentService");
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.notification_popup, null);
        tv = (CustomTextView) view.findViewById(R.id.text);
        llNotification = (RelativeLayout) view.findViewById(R.id.ll_notification);
        llNotificationAnim = (LinearLayout) view.findViewById(R.id.ll_notification_anim);
        llNotification.setVisibility(View.INVISIBLE);
        setNotification("");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        try {

            Bundle extras = intent.getExtras();
            GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
            final String messageType = gcm.getMessageType(intent);


            if (!extras.isEmpty()) {
                if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR
                        .equals(messageType)) {
                    sendNotification("Send error: " + extras.toString());
                } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED
                        .equals(messageType)) {
                    sendNotification("Deleted messages on server: "
                            + extras.toString());
                } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE
                        .equals(messageType)) {

                    Log.d("reg::", extras.get("message").toString());

                    sendNotification(extras.get("message").toString());

                }
            }
            GcmBroadcastReceiver.completeWakefulIntent(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void sendNotification(String msg)
    { /* if (!AppPreference.getOffNotificatino(getApplicationContext())) {
        return;
    }*/


        msg = msg.replace("Bundle", "");
        msg = msg.replace("\"", "");
        msg = msg.replace("{", "");
        Log.d("reg:: msg--", msg);
        writeToFileEstel(test + ":: " + msg + "\r\n\n");
        //notificationGenerator("OK Notification", msg);

        if (msg.contains("Ticket Number"))// Received Toll/Parking/Entrance
        {
            TransationModel model = receivedTicketMoneyArray(msg);

            if(model.getSource().equals(AppPreference.getMyMobileNo(this)))
            {
                DBHelper db = new DBHelper(this);
                if (!DBHelper.isTranscationPresent(model.getTransatId())) {
                    DBHelper.insertTransation(model);
                    DBHelper.insertRECEIVEDTransacation(model);
                    db.close();
                    if (!isForeground()) {
                        Intent call = new Intent(this, NotificationActivity.class);
                        call.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(call);
                    } else {

                        notificationGenerator("Ticket Booked", msg, model);

                    }
                }
            }

        }
        if (msg.contains("top-up by") || msg.contains("TOPUP"))// Received Toll/Parking/Entrance

        {
            setNotification(msg);
            return;

        }

        if (msg.contains("TEST you have receieved KS"))// Received Toll/Parking/Entrance
        {
            id++;
            writeToFile(msg + "\n");
            return;
        }
        if (msg.contains("Your Language has been changed"))//Your language has beem change notirfication
        {
            //tv.setText(msg);
            //  llNotification.setVisibility(View.VISIBLE);
            AppPreference.setPasword(this, "");
            AppPreference.setAuthToken(this, "");
            return;

        }
        if (msg.contains("Your wallet has been adjusted"))// Received Toll/Parking/Entrance
        {
            setNotification(msg);
            return;
        }
        if (msg.contains("Kickback amount disbursed")) {
            setNotification(msg);
            return;
        }
        if (msg.contains("Transfer To"))// Received Toll/Parking/Entrance{
        {
            setNotification(msg);
            return;
        }
        if (msg.contains("received") && msg.contains("Toll/Park")) // Received Toll/Parking/Entrance
        {
            TransationModel model = receivedTollTicket(msg);
            if(model.getSource().equals(AppPreference.getMyMobileNo(this))) {
                DBHelper db = new DBHelper(this);
                //if (!DBHelper.isTranscationPresent(model.getTransatId())) {
                DBHelper.insertTransation(model);
                DBHelper.insertRECEIVEDTransacation(model);
                db.close();
                if (!isForeground()) {
                    Intent call = new Intent(this, NotificationActivity.class);
                    call.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(call);

                } else {

                    notificationGenerator("Toll/Park Booked", msg, model);

                }
            }
            return;

        }
        if (msg.toString().contains("(Received)")||msg.toString().contains("(received)") && msg.toString().contains("TicketNo:")&& !msg.toString().contains("(paid to)"))// Pay without Id
        {
            TransationModel model = receivedTollTicket(msg);

            if(model.getSource().equals(AppPreference.getMyMobileNo(this))) {
                DBHelper db = new DBHelper(this);
                //  if (!DBHelper.isTranscationPresent(model.getTransatId())) {
                DBHelper.insertTransation(model);
                DBHelper.insertRECEIVEDTransacation(model);
                db.close();
                if (!isForeground()) {
                    Intent call = new Intent(this, NotificationActivity.class);
                    call.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(call);
                } else {

                    notificationGenerator("You received KYAT", msg, model);

                }
            }
            return;
        }


        if (msg.toString().contains("Received From") || msg.toString().contains("received from"))// Received Pay to
        {
            TransationModel model = receivedPAYToMoneyArray(msg);
            Log.e("mymobNo","..."+AppPreference.getMyMobileNo(this));
            Log.e("matchNo","..."+model.getSource());
            if(model.getSource().equals(AppPreference.getMyMobileNo(this))) {
                DBHelper db = new DBHelper(this);
                //  if (!DBHelper.isTranscationPresent(model.getTransatId())) {
                String number = "";
                if (model.getDestination().startsWith("+95")) {
                    number = number.replace("+95", "");
                }
                number = model.getDestination().substring(1);
                Log.e("payNo", "..." + number);
                List<ConstactModel> list = new ArrayList<ConstactModel>();
                Utils utils = new Utils();
                String jsondata = DBHelper.getJsonContacts();
                list = utils.getContactListFromJsonString(jsondata);
                ////////////
                ConstactModel contactModel = utils.getSelectedContactModel(number, list);

                if (contactModel == null)
                    Log.e("contactname", "...null");
                if (!contactModel.getName().equals("")) {
                    model.setDestinationPersonName(contactModel.getName());
                    model.setPhotoUrl(contactModel.getPhotoUrl());
                }

                DBHelper.insertTransation(model);
                DBHelper.insertRECEIVEDTransacation(model);
                db.close();
                if (!isForeground()) {
                    Intent call = new Intent(this, NotificationActivity.class);
                    call.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(call);
                } else {

                    notificationGenerator("You recevied KYATS", msg);

                }
            }
            return;
            // }
        }

        if (msg.toString().contains("(Received)") || msg.toString().contains("(Received)") && !msg.toString().contains("received from"))// Pay without Id
        {
            TransationModel model = receivedPayWithtoutId(msg);
            if(model.getSource().equals(AppPreference.getMyMobileNo(this))) {
                DBHelper db = new DBHelper(this);
                // if (!DBHelper.isTranscationPresent(model.getTransatId())) {
                DBHelper.insertTransation(model);
                DBHelper.insertRECEIVEDTransacation(model);
                db.close();
                if (!isForeground()) {
                    Intent call = new Intent(this, NotificationActivity.class);
                    call.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(call);
                } else {
                    notificationGenerator("You received KYAT", msg);
                }
            }
            return;
        }

        if (msg.contains("TOPUP")) {

        }
    }


    public boolean isForeground() {
        boolean isActivityRunning = false;
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> runningTaskInfo = manager.getRunningTasks(1);
        for (int i = 0; i < runningTaskInfo.size(); i++) {
            if (runningTaskInfo.get(i).topActivity.getClassName().toString().contains("NotificationActivity")) {
                isActivityRunning = true;
            }
        }

        return isActivityRunning;
    }


    public void notificationGenerator(String title, String content) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ok_icon)
                        .setContentTitle(title)
                        .setContentText(content);

        Intent resultIntent = new Intent(this, NotificationActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(NotificationActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(0, mBuilder.build());


    }



    private TransationModel receivedPAYToMoneyArray(String msg) { /// Received Pay to
        TransationModel model = new TransationModel();
        //for setting the Master and Cashier data appropriately
        if(AppPreference.getMasterOrCashier(this).equalsIgnoreCase(MASTER)|| SliderScreen.present_Cashier_Id==null)
        {
            model.setCashierId(MASTER_TAG);
            model.setCashierName(MASTER_TAG);
            model.setCashierNumber(MASTER_TAG);
        }
        else
        {
            DBHelper dbb=new DBHelper(this);
            CashierModel cm=dbb.getCashierModel(SliderScreen.present_Cashier_Id);
            dbb.close();

            model.setCashierId(SliderScreen.present_Cashier_Id);
            model.setCashierName(cm.getCashier_Name());
            model.setCashierNumber(cm.getCashier_Number());

        }
        //------------------------------------
        model.setTrnastionType("PAYTO");
        model.setCount("0");
        model.setIsCredit("Cr");
        String reeivedMoney[] = msg.split(" ");
        commonParsingForAll(reeivedMoney, model);
        return model;
    }

    private TransationModel receivedPayWithtoutId(String msg) { /// Received Pay without
        TransationModel model = new TransationModel();
        //for setting the Master and Cashier data appropriately
        if(AppPreference.getMasterOrCashier(this).equalsIgnoreCase(MASTER)|| SliderScreen.present_Cashier_Id==null)
        {
            model.setCashierId(MASTER_TAG);
            model.setCashierName(MASTER_TAG);
            model.setCashierNumber(MASTER_TAG);
        }
        else
        {
            DBHelper dbb=new DBHelper(this);
            CashierModel cm=dbb.getCashierModel(SliderScreen.present_Cashier_Id);
            dbb.close();

            model.setCashierId(SliderScreen.present_Cashier_Id);
            model.setCashierName(cm.getCashier_Name());
            model.setCashierNumber(cm.getCashier_Number());

        }
        //------------------------------------
        model.setTrnastionType("PAYWITHOUT ID");
        model.setDestination("XXXXXXXXX");
        model.setCount("0");
        model.setIsCredit("Cr");
        String reeivedMoney[] = msg.split(" ");
        commonParsingForAll(reeivedMoney, model);
        return model;
    }


    private void commonParsingForAll(String reeivedMoney[], TransationModel model) {

        try {

            for (int i = 0; i < reeivedMoney.length; i++) {

                /*if(reeivedMoney[i].startsWith("~operator~"))
                {
                    String operator=reeivedMoney[i].replace("~operator~", "");
                    model.setOperator(operator);
                }*/

                if (reeivedMoney[i].startsWith("~amount~")) {
                    String amount = reeivedMoney[i].replace("~amount~", "");
                    model.setAmount(amount);
                }
                if (reeivedMoney[i].startsWith("~unregipostbalance~")) {
                    String bal = reeivedMoney[i].replace("~unregipostbalance~", "");
                    AppPreference.setBalance(this, bal);
                }
                if (reeivedMoney[i].startsWith("~responsects~")) {
                    String date = reeivedMoney[i].replace("~responsects~", "");
                    model.setResponsects(date + " " + reeivedMoney[i + 1]);
                    model.setDate(date + " " + reeivedMoney[i + 1]);
                    SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", Locale.US);
                    model.setmTranscationDate(dateFormatter.parse(model.getDate()));
                }
                if (reeivedMoney[i].startsWith("~transid~")) {
                    String id = reeivedMoney[i].replace("~transid~", "");
                    model.setTransatId(id);
                }
                if (reeivedMoney[i].startsWith("Ref~transid~")) {
                    String id = reeivedMoney[i].replace("Ref~transid~", "");
                    model.setTransatId(id);
                }
                if (reeivedMoney[i].startsWith("~source~")) {
                    String id = reeivedMoney[i].replace("~source~", "");
                    model.setDestination(id);
                    model.setSource(AppPreference.getMyMobileNo(this));
                }
                //if(reeivedMoney[i].startsWith())
                if (reeivedMoney[i].startsWith("~comments~")) {
                    String comme = reeivedMoney[i].replace("~comments~", "");
                    if (!comme.equals("")) {
                        String comment = reeivedMoney[i].substring(0, (reeivedMoney[i].lastIndexOf(".")) - 1);
                        comment = comment.substring(0, comment.length() - 2);
                        if (!comment.equals("")) {
                            if (comment.contains("[")) {
                                String comments[] = comment.split("\\[");
                                switch (comments.length) {
                                    case 1:
                                        String onlyLat = comments[0].replace("[", "");
                                        onlyLat = onlyLat.replace("]", "");
                                        model.setLatLong(onlyLat);
                                        break;
                                    case 2:
                                        String textWithLatLong = comments[1].replace("[", "");
                                        textWithLatLong = textWithLatLong.replace("]", "");
                                        model.setLatLong(textWithLatLong);
                                        model.setComments(comments[0]);
                                        break;
                                    case 3:
                                        String text_surway_WithLatLong = comments[2].replace("[", "");
                                        text_surway_WithLatLong = text_surway_WithLatLong.replace("]", "");
                                        model.setLatLong(text_surway_WithLatLong);
                                        model.setComments(comments[0]);
                                        model.setmSurvery(comments[1]);
                                        break;
                                    default:
                                        break;

                                }
                                System.out.println(comment.length());

                            }
                        }
                    } else {
                        model.setComments("");
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setNotification(String msg) {

        layOutParams = new WindowManager.LayoutParams(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY,
                WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                PixelFormat.TRANSLUCENT);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layOutParams.gravity = Gravity.CENTER_VERTICAL;
        wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        wm.addView(view, layOutParams);
        cancelNotification(wm, view);

        Animation animFadeIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move_down);
        llNotificationAnim.startAnimation(animFadeIn);
    }

    public void cancelNotification(final WindowManager wm, final View view) {
        Timer timer = new Timer();
        final Handler handler = new Handler();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Animation animFadeIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move_right_new);
                        llNotificationAnim.startAnimation(animFadeIn);
                        animFadeIn.setAnimationListener(GCMNotificationIntentService.this);

                    }
                });
            }
        };
        timer.schedule(timerTask, 10000);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        llNotification.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }




    public void notificationGenerator(String title, String content, TransationModel model) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ok_icon)
                        .setContentTitle(title).setStyle(new NotificationCompat.BigTextStyle().bigText(content))
                        .setContentText(content)
                        .setAutoCancel(true);
        Bundle bundlee = new Bundle();
        bundlee.putString("transid", model.getTransatId());
        Intent resultIntent = new Intent(this, NotificationActivity.class);
        resultIntent.putExtras(bundlee);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(NotificationActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        (int) System.currentTimeMillis(),
                        PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        int i = (int) System.currentTimeMillis();
        mNotificationManager.notify(i, mBuilder.build());

    }

    private TransationModel receivedTicketMoneyArray(String msg) { /// Received Ticket
        TransationModel model = new TransationModel();
        model.setTrnastionType("TICKET");
        model.setDestination("XXXXXXXXX");
        model.setCount("0");
        model.setIsCredit("Cr");
        String reeivedMoney[] = msg.split(" ");
        commonParsingForAll(reeivedMoney, model);
        return model;
    }

    private void writeToFile(String data) {
        try {
            File myFile = new File("/sdcard/mysdfile.txt");
            if (!myFile.exists()) {
                myFile.createNewFile();
            }
            FileWriter fileWritter = new FileWriter(myFile.getAbsolutePath(), true);
            BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
            bufferWritter.write(data);
            bufferWritter.close();
            fileWritter.close();
        } catch (Exception e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    private void writeToFileEstel(String data) {
        Log.d("reg:: data--", data);
        try {
            File myFile = new File("/sdcard/estel.txt");
            if (!myFile.exists()) {
                myFile.createNewFile();
            }
            FileWriter fileWritter = new FileWriter(myFile.getAbsolutePath(), true);
            BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
            bufferWritter.write(data);
            bufferWritter.close();
            fileWritter.close();
        } catch (Exception e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    private TransationModel receivedTollTicket(String msg) {/// Received Toll
        TransationModel model = new TransationModel();
        model.setTrnastionType("TOLL");
        model.setDestination("XXXXXXXXX");
        model.setCount("0");
        model.setIsCredit("Cr");
        String reeivedMoney[] = msg.split(" ");
        commonParsingForAll(reeivedMoney, model);
        Comment_getter(msg, model);
        return model;
    }


    public String Comment_getter(String msg,TransationModel model)
    {
        if(msg.contains("~comments~"))
        {
            int index_of_second_close_Bracket = msg.indexOf("]", msg.indexOf("]") + 1);
            int index_of_second_open_bracket = msg.indexOf("[", msg.indexOf("[") + 1);
            int index_of_first_open_bracket = msg.indexOf("[",1);
            int index_of_first_second_close_bracket = msg.indexOf("]",1);
            int index_of_comment_tag =  msg.indexOf("~comments~",1);
            String latnlng;
            if(index_of_second_close_Bracket==-1||index_of_second_open_bracket==-1)
            {latnlng = msg.substring(index_of_first_open_bracket,index_of_first_second_close_bracket).replace("]","").replace("[","");
            }else
            {latnlng = msg.substring(index_of_second_open_bracket,index_of_second_close_Bracket).replace("]","").replace("[", "");;
            }
            latlanggetter(latnlng,model);
            String comments = msg.substring(index_of_comment_tag+10,index_of_first_open_bracket);
            model.setComments(comments);
        }

        return null;
    }

    public void latlanggetter(String comments, TransationModel model) {
        try {
            // String comment = comments.substring(comments.indexOf("["), comments.indexOf("]"));
            String parts[] = comments.split(",");
            model.setLatLong(parts[0].replace("[", "") + "," + parts[1].replace("[", ""));
            model.setCellID(parts[2]);
        } catch (Exception e) {
        }
    }

}
