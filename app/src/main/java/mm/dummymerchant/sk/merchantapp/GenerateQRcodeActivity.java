package mm.dummymerchant.sk.merchantapp;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.TableRow;

import mm.dummymerchant.sk.merchantapp.db.DBHelper;

/**
 * Created by user on 1/19/2016.
 */
public class GenerateQRcodeActivity extends BaseActivity implements View.OnClickListener{

    TableRow mDigitalreceipt,mExitingcashcollector,mSuccesslog,mFaillog;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generate_qrcode);
        init();
        setExistingCashcollectorActionBar(String.valueOf(getResources().getText(R.string.generate_qr_code)));
    }
    void init()
    {
        mDigitalreceipt = (TableRow) findViewById(R.id.digitalreceipt);
        mExitingcashcollector = (TableRow) findViewById(R.id.exitingcashcollector);
        mSuccesslog = (TableRow) findViewById(R.id.successlog);
        mFaillog = (TableRow) findViewById(R.id.faillog);
        callAllListeners();
    }
    void callAllListeners()
    {
        mDigitalreceipt.setOnClickListener(this);
        mExitingcashcollector.setOnClickListener(this);
        mSuccesslog.setOnClickListener(this);
        mFaillog.setOnClickListener(this);
    }
    @Override
    public <T> void response(int resultCode, T data) {

    }
    @Override
    public void onClick(View v) {

        Intent intent;
        switch (v.getId()) {

           case R.id.digitalreceipt:
                //intent = new Intent(this, DigitalReceiptActivity.class);
              /* intent = new Intent(this, DigitalReceiptNew.class);
                startActivity(intent);*/
                break;

            case R.id.exitingcashcollector:
                DBHelper dbb=new DBHelper(this);
                dbb.open();
                Cursor c = dbb.Get_CASHCOLLECTOR_TABLE();
                if(c!=null)
                {
                    if(c.getCount()>0)
                    {
                        intent =new Intent(this,ExistingCashcollector.class);
                        intent.putExtra("From", "Dashboard");
                        startActivity(intent);

                    }
                    else {
                        showToast(R.string.no_record);
                        c.close();
                    }
                }

                else {
                    c.close();
                    showToast(R.string.no_record);
                }
                                //

                break;

            case R.id.successlog:
                intent = new Intent(this, OTPLogDetails.class);
                intent.putExtra("From", "SUCCESS");
                startActivity(intent);
                break;

            case R.id.faillog:
                intent =new Intent(this,OTPLogDetails.class);
                intent.putExtra("From", "FAILURE");
                startActivity(intent);
                break;
        }
    }
}
