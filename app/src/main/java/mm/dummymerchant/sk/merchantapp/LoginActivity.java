package mm.dummymerchant.sk.merchantapp;



        import android.content.DialogInterface;
        import android.content.Intent;
        import android.os.Bundle;
        import android.view.View;

        import mm.dummymerchant.sk.merchantapp.Utils.Utils;
        import mm.dummymerchant.sk.merchantapp.customView.CustomButton;
        import mm.dummymerchant.sk.merchantapp.customView.CustomEdittext;
        import mm.dummymerchant.sk.merchantapp.httpConnection.CallingApIS;


public class LoginActivity extends BaseActivity implements View.OnClickListener{

    CustomEdittext mUserName,mPassword;
    CustomButton mSubmit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
     //   setContentView(R.layout.activity_login);
        init();
    }

    private void init()
    {
        /*mUserName=(CustomEdittext)findViewById(R.id.create_dummy_merchant_user_name);
        mSubmit=(CustomButton)findViewById(R.id.create_dummy_merchant_submit);
        mPassword=(CustomEdittext)findViewById(R.id.create_dummy_merchant_user_password);*/
    }

    @Override
    public <T> void response(int resultCode, T data) {

    }

    @Override
    public void onClick(View view) {

        if(!Utils.isEmpty(mUserName.getText().toString())&&!Utils.isEmpty(mPassword.getText().toString()))
        {
                CallingApIS api= new CallingApIS(this,this);

        }else
        {
            showToast(getResources().getString(R.string.Toast_msg_fill_all_details));
        }
    }

    @Override
    public <T> void callRequestAPI(String url, Bundle mPrams, int address) {

    }

    @Override
    public <T> void asyncResponse(int resultCode, T data) {

    }

    @Override
    public <T> void asyncResponseFail(int resultCode, T data) {

    }

    @Override
    public void onCancel(DialogInterface dialogInterface) {

    }

    void callPage(Class cls)
    {
        Intent intent =new Intent(this,cls);
        startActivity(intent);
    }
}

