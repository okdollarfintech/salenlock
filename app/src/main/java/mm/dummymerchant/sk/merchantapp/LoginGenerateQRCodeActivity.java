package mm.dummymerchant.sk.merchantapp;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.androidlegacy.Contents;
import com.scottyab.aescrypt.AESCrypt;

import java.security.GeneralSecurityException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
import mm.dummymerchant.sk.merchantapp.model.NetworkOperatorModel;
import mm.dummymerchant.sk.merchantapp.qrCode.QRCodeEncoder;

/**
 * Created by user on 2/29/2016.
 */
public class LoginGenerateQRCodeActivity extends BaseActivity implements View.OnClickListener {
    public static final String SPEEDKey = "m2n1shlko@$p##d";
    private String LOG_TAG = "GenerateQRCode";
    String qrInputText,encryptedMsg;
    ImageView myImage;
    CustomTextView qrMsg;
    EditText qrInput;
    String name = "";
    String tempCellID="";
    String temp="";
    String number="";
    int length=0;
    String countryCode="+95";
    String cate="9955";
    Button button1;
    String amount="";
    //  private ArrayList<NetworkOperatorModel> listOfCountryCodesAndNames;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //listOfCountryCodesAndNames  = new NetworkOperatorModel().getListOfNetworkOperatorModel(this);
        setContentView(R.layout.activity_main_qrcode);
        setQRCodeHeader();
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        ArrayList<NetworkOperatorModel> listOfCountryCodesAndNames = new NetworkOperatorModel().getListOfNetworkOperatorModel(LoginGenerateQRCodeActivity.this);
        qrMsg = (CustomTextView)findViewById(R.id.textView2);
        button1 = (Button) findViewById(R.id.button1);
        qrInput = (EditText) findViewById(R.id.qrInput);
        button1.setOnClickListener(this);
        myImage = (ImageView) findViewById(R.id.imageView1);
        backButton.setVisibility(View.VISIBLE);
        name = AppPreference.getBusinessName(this);
        System.out.println("getBusinessName " +AppPreference.getBusinessName(this));
        if (name.equals("Unknown"))
            System.out.println("getLoginuserName " +AppPreference.getLoginuserName(this));
            name = AppPreference.getLoginuserName(this);
        tempCellID = getLACAndCID(this);
        //number = AppPreference.getMyMobileNo(this); // speedkyat code
        number = AppPreference.getMyMobileNo(this);

        cate = "" + AppPreference.getBusinessMainCategory(LoginGenerateQRCodeActivity.this) + AppPreference.getBusinessSubCategory(LoginGenerateQRCodeActivity.this);
        name=name+getReleventImage(cate.substring(0,2),cate.substring(2,4));
        if(number.startsWith("00"))
        {
            String code = number.substring(0, 6);
            for (int i = 0; i < listOfCountryCodesAndNames.size(); i++)
            {
                String countryId = listOfCountryCodesAndNames.get(i).getCountryCode().replace("+", "00");
                if (code.contains(countryId))
                {
                    length = countryId.length();
                    countryCode= listOfCountryCodesAndNames.get(i).getCountryCode();
                    if(countryCode.equals("+95"))
                        temp = getString(R.string.qrcode_msg1)+name+getString(R.string.qrcode_msg2) + " ("+countryCode+") "+"<font color='gray'>0</font>"+number.substring(length);
                    else
                        temp = getString(R.string.qrcode_msg1)+name+getString(R.string.qrcode_msg2) + " ("+countryCode+") "+number.substring(length);
                    break;
                }
            }

        }else
            temp = getString(R.string.qrcode_msg1)+name+getString(R.string.qrcode_msg2) + number;

        qrMsg.setText(Html.fromHtml(temp));
        GPSTracker gpsTracker = new GPSTracker(LoginGenerateQRCodeActivity.this);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US);
        String currentDateandTime = sdf.format(new Date());
        encryptedMsg = tempCellID + "#" + name + "-" + AppPreference.getMyMobileNo(this) + "@" + amount + "&" + cate +"β"+gpsTracker.getLatitude()+"γ"+gpsTracker.getLongitude()+"α"+currentDateandTime;

        try {
            qrInputText = AESCrypt.encrypt(SPEEDKey, encryptedMsg);

        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        }
        Bitmap bitmap = generateQRCode(qrInputText);
        if(bitmap!=null) {

            myImage.setImageBitmap(bitmap);

        }
        buttonLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                qrInput.setEnabled(true);
                qrInput.requestFocus();
                qrInput.setText("");
                button1.setEnabled(true);
                qrMsg.setText(Html.fromHtml(temp));


            }
        });
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        qrInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                int result = actionId & EditorInfo.IME_MASK_ACTION;
                switch(result) {
                    case EditorInfo.IME_ACTION_DONE:
                        getQRCode();
                        break;
                    case EditorInfo.IME_ACTION_NEXT:
                        getQRCode();
                        break;
                }
                return true;
            }
        });


    }

    @Override
    public <T> void response(int resultCode, T data) {

    }


    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.button1:
                getQRCode();
                break;
        }
    }
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void getQRCode() {
        qrInputText="";
        String temp="";
        if(!qrInput.getText().toString().isEmpty())
        {
            GPSTracker gpsTracker;
            amount = qrInput.getText().toString();
            if(amount.length()<10) {
                gpsTracker = new GPSTracker(LoginGenerateQRCodeActivity.this);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
                String currentDateandTime = sdf.format(new Date());
                encryptedMsg = tempCellID + "#" + name + "-" + AppPreference.getMyMobileNo(this) + "@" + amount + "&" + cate +"β"+gpsTracker.getLatitude()+"γ"+gpsTracker.getLongitude()+"α"+currentDateandTime;
                try {
                    qrInputText = AESCrypt.encrypt(SPEEDKey, encryptedMsg);

                } catch (GeneralSecurityException e) {
                    e.printStackTrace();
                }
                Bitmap bitmap= generateQRCode(qrInputText);
                if (bitmap!= null) {

                    myImage.setImageBitmap(bitmap);
                    qrInput.setEnabled(false);
                    qrMsg.setVisibility(View.VISIBLE);
                    if (countryCode.equals("+95"))
                        temp = getString(R.string.qrcode_msg1) + name + getString(R.string.qrcode_msg2) + " (" + countryCode + ") " + "<font color='gray'>0</font>" + number.substring(length) + getString(R.string.qrcode_msg3) + amount + " " + getString(R.string.kyats);
                    else
                        temp = getString(R.string.qrcode_msg1) + name + getString(R.string.qrcode_msg2) + " (" + countryCode + ") " + number.substring(length) + getString(R.string.qrcode_msg3) + amount + " " + getString(R.string.kyats);
                    qrMsg.setText(Html.fromHtml(temp));
                    try {
                        buttonLogout.setBackground(getResources().getDrawable(R.drawable.ferry_bus_train_selector));
                    } catch (NoSuchMethodError e) {
                        e.printStackTrace();
                    }
                    buttonLogout.setText(getString(R.string.edit));
                    buttonLogout.setVisibility(View.VISIBLE);
                    button1.setEnabled(false);
                }
            }
            else{
                showToast(getString(R.string.limit_exceed));
            }
        }else {
            showToast(getString(R.string.enter_amt));
            qrInput.setEnabled(true);
            qrInput.requestFocus();
            buttonLogout.setVisibility(View.INVISIBLE);
            qrMsg.setText(Html.fromHtml(temp));
        }
    }

    public static Bitmap mergeBitmaps(Bitmap bmp1, Bitmap bmp2) {
        Bitmap bmOverlay = Bitmap.createBitmap(bmp1.getWidth(),
                bmp1.getHeight(), bmp1.getConfig());
        int centreX = (bmp1.getWidth()  - bmp2.getWidth()) /2;
        int centreY = (bmp1.getHeight() - bmp2.getHeight()) /2 ;
        Canvas canvas = new Canvas(bmOverlay);
        canvas.drawBitmap(bmp1, new Matrix(), null);
        canvas.drawBitmap(bmp2, centreX, centreY, null);


        return bmOverlay;
    }

    public Bitmap generateQRCode(String qrInputText){
        //Find screen size
        WindowManager manager = (WindowManager) getSystemService(WINDOW_SERVICE);
        Display display = manager.getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);
        int width = point.x;
        int height = point.y;
        System.out.println("width"+width);
        System.out.println("height"+height);
        int smallerDimension = width < height ? width : height;
        smallerDimension = smallerDimension * 3/4;
        System.out.println("String" + qrInputText);
        //Encode with a QR Code image
        QRCodeEncoder qrCodeEncoder = new QRCodeEncoder(qrInputText,
                null,
                Contents.Type.TEXT,
                BarcodeFormat.QR_CODE.toString(),
                1000);
        try {
            Bitmap bitmap = qrCodeEncoder.encodeAsBitmap();
            Bitmap bitLogo = BitmapFactory.decodeResource(getResources(),
                    R.drawable.ok
            );
            Bitmap bitMerged = mergeBitmaps(bitmap, bitLogo);
            /*ImageView myImage = (ImageView) findViewById(R.id.imageView1);
            myImage.setImageBitmap(bitMerged);*/

            return bitMerged;
            //    Drawable drawable = new BitmapDrawable(getResources(), bitLogo);
            //    myImage.setBackground(drawable);

            //    Graphics g = Graphics.FromImage(bitmap);

        } catch (WriterException e) {
            e.printStackTrace();

            return null;
        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN || keyCode == KeyEvent.KEYCODE_VOLUME_UP || keyCode == KeyEvent.KEYCODE_HOME || keyCode == KeyEvent.KEYCODE_POWER) {
            if(!qrInput.getText().toString().isEmpty())
            {
                Bitmap bitmap= generateQRCode("Sorry !!");
                if (bitmap!= null)
                    myImage.setImageBitmap(bitmap);
                Handler handler = new Handler();
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            getQRCode();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                };
                handler.postDelayed(runnable, 1000);
            }
            return true;
        }else
            return super.onKeyDown(keyCode, event);
    }

}

