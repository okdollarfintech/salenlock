package mm.dummymerchant.sk.merchantapp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.scottyab.aescrypt.AESCrypt;

import java.security.GeneralSecurityException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import mm.dummymerchant.sk.merchantapp.Utils.Constant;
import mm.dummymerchant.sk.merchantapp.Utils.GpsTracker1;
import mm.dummymerchant.sk.merchantapp.adapter.CashCollector_Adapter;
import mm.dummymerchant.sk.merchantapp.customView.CustomButton;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
import mm.dummymerchant.sk.merchantapp.db.DBHelper;
import mm.dummymerchant.sk.merchantapp.model.RowItem_cashcollector;

public class MainActivity extends BaseActivity implements Constant
{
    private String cashcollectorname,cashcollectorno,amount,cashiername,cashierno,cashierid,otpvalue,time;
    private String timeStamp,SC;
    DBHelper db;
    RowItem_cashcollector cellvalue;
    private ArrayList<RowItem_cashcollector> AlertList = new ArrayList<RowItem_cashcollector>();
    private ArrayList<String> childList = new ArrayList<String>();
    private ListView lv;
    CashCollector_Adapter adapter;
    private String Scan_Longitude,Scan_Latitude,Generate_Latitude,Generate_Longitude,GenerateLocation,ScanLocation;
    private String Textdata,Templatedata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_expandablelistview);
        setContentView(R.layout.qrcodescanner);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSelectionDilog();
            }
        });
        setExistingCashcollectorActionBar(String.valueOf(getResources().getText(R.string.app_name)));
        db = new DBHelper(this);
        Check_MasterORCashier_Login();
        lv=(ListView)findViewById(R.id.listView1);
        setlistview();
    }
    @Override
    public <T> void response(int resultCode, T data)
    {

    }



    public void setlistview()
    {
        AsyncTaskLoading loading = new AsyncTaskLoading();
        loading.execute();
    }

    private class AsyncTaskLoading extends AsyncTask<Void, Void, Void> {
        /// private GameResultsAdapter adapter;
        private final ProgressDialog dialogue =new ProgressDialog(MainActivity.this);
        public AsyncTaskLoading() {
        }
        @Override
        protected void onPreExecute()
        {
            this.dialogue.setMessage("Loading...please wait..");
            this.dialogue.setIndeterminate(true);
            this.dialogue.setCancelable(false);
            this.dialogue.setCanceledOnTouchOutside(false);
            this.dialogue.show();
        }
        @Override
        protected Void doInBackground(Void... params)
        {
            try {
                db.open();
                Log.i("MasterORCashier_ID ", MasterORCashier_ID);
                Cursor c = db.Get_Details(MasterORCashier_ID,"SC");
                db.close();
                if (c.getCount() > 0) {
                    Log.i("ServingCell_Page", "Enter IF Contion");

                    if (c.moveToFirst()) {
                        while (c.isAfterLast() == false) {
                            cellvalue = new RowItem_cashcollector();
                            Log.i("ServingCell_Page", "READIN-Get_SevRowItem_servingcellingcell");

                            Log.i("KEY_Id ", c.getString(0));
                            Log.i("SCANNING_TIME", c.getString(2));
                            Log.i("GENERATE_TIME", c.getString(3));
                            Log.i("OTP_ENTER_TIME", c.getString(4));
                            Log.i("KEY_Name", c.getString(5));
                            Log.i("KEY_Number", c.getString(6));
                            Log.i("KEY_Amount", c.getString(7));
                            Log.i("KEY_Status", c.getString(8));
                            Log.i("KEY_PaidSTATUS", c.getString(9));
                            Log.i("FROM_WHICH_SCAN", c.getString(10));
                            Log.i("SCANNING_LOCATION", c.getString(11));
                            Log.i("GENERATE_LOCATION", c.getString(12));
                            Log.i("OTP_ENTER_LOCATION", c.getString(13));
                            Log.i("Generater_Name", c.getString(14));
                            Log.i("Generater_No", c.getString(15));
                            Log.i("TextValue", c.getString(16));
                            Log.i("TemplateValue", c.getString(17));
                            Log.i("openStatus", c.getString(18));

                            cellvalue.SetScanning_Time(c.getString(2));
                            cellvalue.SetGenerate_Time(c.getString(3));
                            cellvalue.setOTP_Enter_Time(c.getString(4));
                            cellvalue.SetScanner_Name(c.getString(5));
                            cellvalue.SetScanner_No(c.getString(6));
                            cellvalue.SetLog_Amount(c.getString(7));
                            cellvalue.SetLog_Status(c.getString(8));
                            cellvalue.SetLog_Paid_Status(c.getString(9));
                            cellvalue.SetScanning_Location(c.getString(11));
                            cellvalue.SetGenerate_Location(c.getString(12));
                            cellvalue.SetOTP_Enter_Location(c.getString(13));
                            cellvalue.SetGenerater_Name(c.getString(14));
                            cellvalue.SetGenerater_No(c.getString(15));
                            cellvalue.setTextValue(c.getString(16));
                            cellvalue.setTemplateValue(c.getString(17));
                            cellvalue.setopenStatus(c.getString(18));

                            AlertList.add(cellvalue);
                           // childList.add(c.getString(17));
                            c.moveToNext();
                        }
                    }
                }
                else {
                    Toast.makeText(getApplicationContext(), "Record Not Found", Toast.LENGTH_SHORT).show();
                }
            }catch(Exception e){
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void res)
        {
            adapter = new CashCollector_Adapter(MainActivity.this , AlertList);
            lv.setAdapter(adapter);

            if (this.dialogue.isShowing())
            {
                this.dialogue.dismiss();
            }
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        db = new DBHelper(this);
        if(resultCode==RESULT_OK)
        {
            if (null != data)
            {
                if (scanningResult != null)
                {
                    String scanResult = scanningResult.getContents();
                    afterResult(scanResult);
                }
           } else {
                    Toast.makeText(this, "Please Try Again..!", Toast.LENGTH_LONG).show();
           }
        }
        if(requestCode==SELECT_PICTURE_SC)
        {
            Bitmap bMap=null;
            if(data!=null&&resultCode==RESULT_OK)
            {
                Uri selectedImageUri = data.getData();
                try {
                    bMap = BitmapFactory.decodeStream(
                            getContentResolver().openInputStream(selectedImageUri));
                    if(bMap!=null) {
                        String scannedText = readImageFromFile(bMap);
                        afterResult(scannedText);
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
    }


    public void PrintLogDeatils()
    {
        Log.d("Cashcollectorname ", cashcollectorname);
        Log.d("cashcollectorno ", cashcollectorno);
        Log.d("Amount ", amount);
        Log.d("cashiername ", cashiername);
        Log.d("cashierno ", cashierno);
        Log.d("cashierid ", cashierid);
        Log.d("Otpvalue ", otpvalue);
        Log.d("time ", time);
        Log.d("Generate_Latitude ", Generate_Latitude);
        Log.d("Generate_Longitude ", Generate_Longitude);
        Log.d("Scan_Latitude", Scan_Latitude);
        Log.d("Scan_Longitude", Scan_Longitude);
        Log.d("Textdata", Textdata);
        Log.d("Templatedata", Templatedata);
    }

    public void Get_Location()
    {
        GpsTracker1 gps = new GpsTracker1(getApplicationContext());
        Location loc = gps.getLocation();

        if(loc!=null) {
            Scan_Latitude = "" + loc.getLatitude();
            Scan_Longitude = "" + loc.getLongitude();
            Log.i("Scan_Latitude",Scan_Latitude);
            Log.i("Scan_Longitude",Scan_Longitude);
        }
        else {
            Scan_Latitude = "0.0";
            Scan_Longitude = "0.0";
            Log.i("Scan_Latitude",Scan_Latitude);
            Log.i("Scan_Longitude", Scan_Longitude);
        }
    }


    void afterResult(String scannedText)
    {
        String scanningResult = scannedText;
        String E1, E2,Decrypt_Time,Decrypt_No;
        String text = "", textTime = "";
            try {
                String Split[] = scanningResult.split("---");
                E1 = Split[0];
                E2 = Split[1];

                textTime = AESCrypt.decrypt(SPEEDKey, E1);
                Log.i("textTime", textTime);

                String Split1[] = textTime.split("----");
                Decrypt_Time = Split1[0];
                Decrypt_No = Split1[1];
                Log.i("Split1 [0] Time ", Decrypt_Time);
                Log.i("Split1 [1] No ", Decrypt_No);
                if(Decrypt_No.equals(MasterORCashier_Mobile_Number))
                {
                    text = AESCrypt.decrypt(textTime, E2);
                    Log.i("text", text);

                    if (!text.isEmpty()) {
                        try
                        {
                            String dataText[] = text.split("-");
                            Log.d("Enter ", "Enter IF Conditionnnnn");
                            SC = dataText[0];
                            Log.d("SC Value:", SC);
                          if(!SC.equals("OK"))
                          {
                            cashcollectorname = dataText[1];
                            cashcollectorno = dataText[2];
                            amount = dataText[3];
                            cashiername = dataText[4];
                            cashierno = dataText[5];
                            cashierid = dataText[6];
                            otpvalue = dataText[7];
                            time = dataText[8];
                            Generate_Latitude = dataText[9];
                            Generate_Longitude = dataText[10];
                            Textdata = dataText[11];
                            Templatedata = dataText[12];
                            Get_Location();
                              //Scan_Latitude = "0.0"; // for Temp because now i am using gas mobile. it cant get location
                              //Scan_Longitude = "0.0"; // for Temp because now i am using gas mobile. it cant get location
                            PrintLogDeatils();
                            GenerateLocation = Generate_Latitude + "," + Generate_Longitude;
                            ScanLocation = Scan_Latitude + "," + Scan_Longitude;
                            if (!otpvalue.equals(""))
                            {
                                if(Textdata.equals("?"))
                                {
                                    {
                                        if(Templatedata.equals("[]"))
                                        {
                                            OTPDatasDilog();
                                        }
                                        else {
                                            ConfirmTemplateDilog();
                                        }
                                    }
                                }
                                else
                                {
                                    ConfirmTextDilog();
                                }

                            } else {
                                Toast.makeText(this, "Did't Get OTP Value.Please Try Again", Toast.LENGTH_LONG).show();
                            }
                        }
                        else {
                            Toast.makeText(this, "Wrong QR Code..Please Try Again..", Toast.LENGTH_LONG).show();
                        }
                        }
                        catch (ArrayIndexOutOfBoundsException e)
                        {
                            Toast.makeText(this, "Please Try Again..!", Toast.LENGTH_LONG).show();
                        }
                    }
                    else {
                        Toast.makeText(this, "Please Try Again..!", Toast.LENGTH_LONG).show();
                    }
                }
                else {
                    Toast.makeText(this, "Phone Number Mismatch", Toast.LENGTH_LONG).show();
                }
            } catch (GeneralSecurityException e) {
                e.printStackTrace();
                Toast.makeText(this, "Please Try Again Later", Toast.LENGTH_SHORT).show();
            } catch (IllegalArgumentException e) {
                Toast.makeText(this, "Wrong QR Code..Please Try Again..!", Toast.LENGTH_SHORT).show();
            } catch (ArrayIndexOutOfBoundsException e) {
                Toast.makeText(this, "Please Try Again..!", Toast.LENGTH_LONG).show();
            }
    }

    void ConfirmTextDilog()
    {
        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_textvalue);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        CustomTextView close = (CustomTextView) dialog.findViewById(R.id.close);
        CustomButton ok = (CustomButton) dialog.findViewById(R.id.ok);
        CustomTextView input = (CustomTextView) dialog.findViewById(R.id.input);
        close.setVisibility(View.GONE);
        System.out.println("m.getTextValue " + Textdata);
        input.setText(Textdata);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if(Templatedata.equals("[]"))
                {
                    OTPDatasDilog();
                }
                else {
                    ConfirmTemplateDilog();
                }
            }
        });
    }

    void ConfirmTemplateDilog()
    {
        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_product_details_confirmpage);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        LinearLayout container = (LinearLayout) dialog.findViewById(R.id.container);
        CustomButton ok = (CustomButton) dialog.findViewById(R.id.ok);
        String i = Templatedata.replaceAll("\\[", "").replaceAll("\\]", "");
        String dataTextt[] = i.split(",");
        int len = dataTextt.length;

        for (int j = 0; j < len; j++) {
            String dataText1[] = dataTextt[j].split("/");
            LinearLayout Container_id = container;
            LayoutInflater layoutInflater = (LayoutInflater) MainActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View addView = layoutInflater.inflate(R.layout.row_dialog, null);
            CustomTextView item1 = (CustomTextView) addView.findViewById(R.id.itemvalue);
            CustomTextView qty1 = (CustomTextView) addView.findViewById(R.id.qtyvalue);
            CustomTextView price1 = (CustomTextView) addView.findViewById(R.id.pricevalue);
            Container_id.addView(addView);
            item1.setText(dataText1[0]);
            qty1.setText(dataText1[1]);
            price1.setText(dataText1[2]);
        }
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
                OTPDatasDilog();
            }
        });
    }

    void OTPDatasDilog()
    {
        Log.d("Otpvalue ", otpvalue);
        LayoutInflater layoutInflater = LayoutInflater.from(MainActivity.this);
        View promptView = layoutInflater.inflate(R.layout.activity_otp, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
        alertDialogBuilder.setView(promptView);
        final TextView amtText, noText, no1Text, nameText, name1Text, timeText, otpText;
        amtText = (TextView) promptView.findViewById(R.id.amount);
        noText = (TextView) promptView.findViewById(R.id.no);
        nameText = (TextView) promptView.findViewById(R.id.name);
        no1Text = (TextView) promptView.findViewById(R.id.no1);
        name1Text = (TextView) promptView.findViewById(R.id.name1);
        timeText = (TextView) promptView.findViewById(R.id.time);
        otpText = (TextView) promptView.findViewById(R.id.otp);

        amtText.setText("Amount : " + amount);
        noText.setText("ScannerNumber : " + cashcollectorno);
        nameText.setText("ScannerName   : " + cashcollectorname);
        no1Text.setText("GenerateNumber : " + cashierno);
        name1Text.setText("GenerateName   : " + cashiername);
        timeText.setText("Time   : " + time);
        otpText.setText("OTP    : " + otpvalue);

        // setup a dialog window
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        timeStamp = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss").format(new Date());
                        db.open();
                        db.AddCollectortoCollectorScanDetails(MasterORCashier_ID,timeStamp, time, "-", ScanLocation, GenerateLocation, "0.0", cashcollectorname, cashcollectorno, amount, SUCCESS, RECEIVED, SAFETYCASHIER_TO_CASHCOLLECTOR, cashiername, cashierno, Textdata, Templatedata,"false");
                        db.close();
                        AlertList.clear();
                        setlistview();
                        deleteSharedImage();
                    }
                })
                .setNegativeButton("ABORT", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        timeStamp = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss").format(new Date());
                        db.open();
                        db.AddCollectortoCollectorScanDetails(MasterORCashier_ID,timeStamp, time, "-", ScanLocation, GenerateLocation, "0.0", cashcollectorname, cashcollectorno, amount, FAILURE, NOTRECEIVED, SAFETYCASHIER_TO_CASHCOLLECTOR, cashiername, cashierno, Textdata, Templatedata,"false");
                        db.close();
                        AlertList.clear();
                        setlistview();
                        deleteSharedImage();
                    }
                });
        AlertDialog alertD = alertDialogBuilder.create();
        alertD.show();

    }



    @Override
    public void onBackPressed()
    {
        //super.onBackPressed();
        finish();
    }
}
