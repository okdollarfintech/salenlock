package mm.dummymerchant.sk.merchantapp;



import android.content.Intent;
import android.content.pm.PackageInfo;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;


import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.Constant;
import mm.dummymerchant.sk.merchantapp.Utils.Utils;
import mm.dummymerchant.sk.merchantapp.model.LocationModel;

/**
 * Created by abhishekmodi on 16/10/15.
 */
public class MapActivity extends BaseActivity {

    private GoogleMap map;
    private String cellid;
    private Thread first;
    public  String current_version;
    private String Name="";
    private DefaultHttpClient httpclient;
    private String MerchantNumbers;
    private Marker marker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map);
        setMapActivityActionBar();
        backButtonFinish();
        buttonLogout.setVisibility(View.INVISIBLE);
        map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
        if (map != null) {
            map.setBuildingsEnabled(true);
            map.getUiSettings().setMyLocationButtonEnabled(true);
            map.getUiSettings().setRotateGesturesEnabled(true);
            map.getUiSettings().setZoomControlsEnabled(true);
            map.setMyLocationEnabled(true);
        }

        updateIntent();

    }

    private void backButtonFinish()
    {
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();


    }

    private void updateIntent() {
        Intent data = getIntent();
        if (data != null) {
            if(data.hasExtra(BUNDLE_MAP_CELL_ID)){
                if (!data.getStringExtra(BUNDLE_MAP_CELL_ID).equals("") && !data.getStringExtra(BUNDLE_MAP_CELL_ID).equals("0.0")) {
                    cellid = data.getStringExtra(BUNDLE_MAP_CELL_ID);
                    if (Utils.isReallyConnectedToInternet(this))
                        callMapLatLong("", cellid);
                }else if(data.hasExtra(BUNDLE_MAP_LAT_LONG))
                {
                    String[] lat=null;
                    if(!data.getStringExtra(BUNDLE_MAP_LAT_LONG).equals("")) {
                        lat = data.getStringExtra(BUNDLE_MAP_LAT_LONG).split(",");
                    }
                    try{
                        myCurreenyLocation(Double.parseDouble(lat[0]),Double.parseDouble(lat[1])) ;
                    }catch (Exception e){
                        Toast.makeText(this,"Location not Available",Toast.LENGTH_LONG).show();
                    }

                }
            }
            if(data.hasExtra(Constant.MERCHANT_MOBILE_NO))
            {
                this.MerchantNumbers = data.getStringExtra(Constant.MERCHANT_MOBILE_NO);
            }

        }
    }


    @Override
    public <T> void response(int resultCode, T data) {

        if (resultCode == REQUEST_TYPE_MAP_LAT_LONG) {
            try {
                if (map == null) {
                    map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
                }
                LocationModel locationModel = (LocationModel) data;

                if(locationModel.getLatitude().equals("0")||locationModel.getLatitude().equals("0.0")) {

                    String[] lat=null;
                    Intent data1 = getIntent();


                    if(!data1.getStringExtra(BUNDLE_MAP_LAT_LONG).equals("")) {

                        lat = data1.getStringExtra(BUNDLE_MAP_LAT_LONG).split(",");
                    }

                    try{

                        myCurreenyLocation(Double.parseDouble(lat[0]),Double.parseDouble(lat[1])) ;

                    }catch (Exception e){

                        Toast.makeText(this,"Location not Available",Toast.LENGTH_LONG).show();

                    }


                }else {
                    myCurreenyLocation(Double.valueOf(locationModel.getLatitude()), Double.valueOf(locationModel.getLongitude()));
                }
            } catch (Exception e) {
            }
        }
    }

    private void myCurreenyLocation(Double latitude, Double longitude) {

        marker = map.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)));

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(latitude, longitude))      // Sets the center of the map to location user
                .zoom(17)                   // Sets the zoom
                .bearing(90)                // Sets the orientation of the camera to east
                .tilt(40)                   // Sets the tilt of the camera to 30 degrees
                .build();                   // Creates a CameraPosition from the builder
        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng arg0) {
                marker.showInfoWindow();
            }
        });
        buisiness_Caller(latitude,longitude);
    }


    public void buisiness_Caller(final Double latitude,final Double longitude)
    {
        first =  new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                    current_version = pInfo.versionName;
                }catch (Exception e)
                {
                    current_version="1.0";
                }
                after_method(CallerMethod(Constant.BASE_APP_URL + Constant.REQUEST_TYPE_BUSSINESS_DETAILS + callLatestVersionDetails(AppPreference.getMyMobileNo(getApplicationContext()), AppPreference.getSimId(getApplicationContext()), Utils.getSimSubscriberID(getApplicationContext()), "0", MerchantNumbers)),latitude,longitude);


            }
        });
        first.start();



    }

    public void after_method(String s,final Double latitude, final Double longitude)
    {

        if(s!=null)
        {
            try {
                JSONTokener tokener = new JSONTokener(s);
                JSONObject object = new JSONObject(tokener);
                if(object.getString("Code").equals("200"))
                {
                    String data1 = object.getString("Data");
                    JSONObject data = new JSONObject(new JSONTokener(data1));
                    JSONArray jsonArray = data.getJSONArray("BussinessCategory");
                    JSONObject details = jsonArray.getJSONObject(0);
                    Name = details.getString("Name");
                    //  BurmeseName = details.getString("BurmeseName");
                    //  logo = details.getString("Logo");
                    // sendBroadCastSMS1(Url,Type,version);
                }
//                else {
//                    //  Utils.showToast(context,object.getString("Msg"));
//                }
            }catch (Exception e)
            {
                Name = "No Data Available";
                /// logo ="No Data Available";
                e.printStackTrace();
            }


            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                        @Override
                        public View getInfoWindow(Marker marker) {
                            return null;
                        }

                        @Override
                        public View getInfoContents(Marker arg0) {

                            View v = getLayoutInflater().inflate(R.layout.info_window, null);

                            // Getting the position from the marker
                            LatLng latLng = arg0.getPosition();

                            // Getting reference to the TextView to set latitude
                            TextView title = (TextView) v.findViewById(R.id.title);
                            TextView number = (TextView) v.findViewById(R.id.business_category);
                            // Getting reference to the TextView to set longitude
                            // Setting the latitude
                            ImageView img = (ImageView) v.findViewById(R.id.img_cat);
                            if (!Name.isEmpty()) {
                                String catImg = Name.substring(0, 4).toLowerCase();
                                for (int i = 0; i < listOfStringImages.length; i++) {
                                    if (listOfStringImages[i].contains(catImg)) {
                                        img.setImageBitmap(BitmapFactory.decodeResource(getResources(), listOfCategoryImages[i]));
                                        break;
                                    }
                                }
                            }
                            title.setText("Category:" + Name);
                            number.setText("Number:" + MerchantNumbers);
                            // Setting the longitude
                            // Returning the view containing InfoWindow contents
                            return v;

                        }
                    });




                }
            });


        }


    }


    public String CallerMethod(String url)
    {

        httpclient = new DefaultHttpClient();
        HttpResponse response;
        String responseString = null;
        try {
            response = httpclient.execute(new HttpGet(url));
            Log.i("url version", url);
            StatusLine statusLine = response.getStatusLine();

            if(statusLine.getStatusCode() == HttpStatus.SC_OK){
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                response.getEntity().writeTo(out);
                responseString = out.toString();
                out.close();
            } else{
                //Closes the connection.
                response.getEntity().getContent().close();
                throw new IOException(statusLine.getReasonPhrase());
            }
        } catch (ClientProtocolException e) {
            //TODO Handle problems..
        } catch (IOException e) {
            e.printStackTrace();
        }
        return responseString;
    }

    public  String callLatestVersionDetails(String mobileNumber,String simid,String Msid,String OSType,String merchant_mobile) {
        return Constant.MOBILE_NO+"="+mobileNumber+"&"+Constant.SIM_IDI+"="+simid+"&"+Constant.MSID+"="+Msid+"&"+Constant.OSType+"="+OSType+"&"+Constant.MERCHANT_MOBILE_NO+"="+merchant_mobile;
    }



}



