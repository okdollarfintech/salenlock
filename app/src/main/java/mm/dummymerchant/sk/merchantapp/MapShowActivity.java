package mm.dummymerchant.sk.merchantapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by user on 2/13/2016.
 */
public class MapShowActivity extends AppCompatActivity {
    // Google Map
    private GoogleMap googleMap;
    private String loc1,loc2,loc3;
    double latitude1,longitude1,latitude2,longitude2,latitude3,longitude3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_layout);



        Intent i=getIntent();
        loc1=i.getStringExtra("Loc1");
        loc2=i.getStringExtra("Loc2");
        loc3=i.getStringExtra("Loc3");
        Log.i("SCANNING_LOCATION", loc1);
        Log.i("GENERATE_LOCATION", loc2);
        Log.i("OTPEnter_LOCATION", loc3);
        String Split1[] = loc1.split(",");
        String Split2[] = loc2.split(",");
        String Split3[] = loc3.split(",");

        if(!Split1[0].equals("0.0"))
        {
            latitude1 = Double.parseDouble( Split1[0] );
            longitude1 = Double.parseDouble( Split1[1] );
        }
        if(!Split2[0].equals("0.0"))
        {
            latitude2 = Double.parseDouble( Split2[0] );
            longitude2 = Double.parseDouble( Split2[1] );
        }
        if(!Split3[0].equals("0.0"))
        {
            latitude3 = Double.parseDouble( Split3[0] );
            longitude3 = Double.parseDouble( Split3[1] );
        }

        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
        System.out.println("status " + status);
        System.out.println("ConnectionResult.SUCCESS " + ConnectionResult.SUCCESS);
        if (status == ConnectionResult.SUCCESS) {
            try {
                // Loading map
                    initilizeMap();
            } catch (NullPointerException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            GooglePlayServicesUtil.getErrorDialog(status, this, status);
        }

        MapsInitializer.initialize(getApplicationContext());
    }
    private void initilizeMap() {
        if (googleMap == null)
        {
            googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
            // create marker
            String Split1[] = loc1.split(",");
            String Split2[] = loc2.split(",");
            String Split3[] = loc3.split(",");

            if (!Split1[0].equals("0.0"))
            {
                MarkerOptions marker = new MarkerOptions().position(new LatLng(latitude1, longitude1)).title("QR Scan Location").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
               // MarkerOptions marker = new MarkerOptions().position(new LatLng(latitude1, longitude1)).title("QR Scan Location").icon(BitmapDescriptorFactory.fromResource(R.drawable.location_icon));
                googleMap.addMarker(marker);
                CameraUpdate updatePosition1 = CameraUpdateFactory.newLatLngZoom(new LatLng(latitude1, longitude1), 17);
                googleMap.moveCamera(updatePosition1);
            }
            if (!Split2[0].equals("0.0"))
            {
                MarkerOptions marker1 = new MarkerOptions().position(new LatLng(latitude2, longitude2)).title("QR Generate Location").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
               // MarkerOptions marker1 = new MarkerOptions().position(new LatLng(latitude2, longitude2)).title("QR Generate Location").icon(BitmapDescriptorFactory.fromResource(R.drawable.location_icon));
                googleMap.addMarker(marker1);
                CameraUpdate updatePosition2 = CameraUpdateFactory.newLatLngZoom(new LatLng(latitude2, longitude2), 17);
                googleMap.moveCamera(updatePosition2);
            }
            else {
                Toast.makeText(MapShowActivity.this, "Generate Location Not Available", Toast.LENGTH_SHORT).show();
            }
            if (!Split3[0].equals("0.0"))
            {
                MarkerOptions marker = new MarkerOptions().position(new LatLng(latitude3, longitude3)).title("QR OTPEnter Location").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET));
               // MarkerOptions marker = new MarkerOptions().position(new LatLng(latitude3, longitude3)).title("QR OTPEnter Location").icon(BitmapDescriptorFactory.fromResource(R.drawable.location_icon));
                googleMap.addMarker(marker);
                CameraUpdate updatePosition1 = CameraUpdateFactory.newLatLngZoom(new LatLng(latitude3, longitude3), 17);
                googleMap.moveCamera(updatePosition1);
            }
            // check if map is created successfully or not
            if (googleMap == null) {
                Toast.makeText(getApplicationContext(),"Sorry! unable to create maps", Toast.LENGTH_SHORT).show();
            }
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        initilizeMap();
    }
}

