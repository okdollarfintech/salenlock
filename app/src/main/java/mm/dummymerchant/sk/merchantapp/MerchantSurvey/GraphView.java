package mm.dummymerchant.sk.merchantapp.MerchantSurvey;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import mm.dummymerchant.sk.merchantapp.BaseActivity;
import mm.dummymerchant.sk.merchantapp.R;
import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.Utils;
import mm.dummymerchant.sk.merchantapp.customView.CustomButton;
import mm.dummymerchant.sk.merchantapp.model.CustomerSurveyModel;
import mm.dummymerchant.sk.merchantapp.model.SurveyDetailsModel;
import mm.dummymerchant.sk.merchantapp.model.TransationModel;

/**
 * Created by user on 11/9/2015.
 */
public class GraphView extends BaseActivity {
    private ArrayList<String> descriptions=new ArrayList<>();
    private ArrayList<String> rates=new ArrayList<>();
    private BarChart barChart;
    int nweeks=0;
    int month=1;
    int year=2015;
    String day="%";
    private char g;
    int ageRange1=0;
    int ageRange2=100;
    int scoreRange1=0;
    int scoreRange2=100;
    int hourRange1=0, hourRange2=0, minRange1=0, minRange2=0;
    char gender='%';
    Spinner transTime, genderTrans, scoreTrans, ageTrans,filterTrans;
    Date dateRange1, dateRange2;
    HashMap<String, SurveyDetailsModel> dateWiseList = new HashMap<>();
    ArrayList<TransationModel> receivedMoney=new ArrayList<>();
    ArrayList<CustomerSurveyModel> customerSurveyList=new ArrayList<>();
    final ArrayList<SurveyDetailsModel> surveyDetailsArrayList = new ArrayList<>();
    SimpleDateFormat format;
    Calendar calendar;
    String lang;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph_view);
        context=GraphView.this;
        //toolbar.setBackgroundColor(getResources().getColor(R.color.statics_header_color));
        mTitleOfHeader.setText("Graph View");
        lang=AppPreference.getLanguage(getApplicationContext());
        changeLang("eng");
        initSpinner();

        buttonLogout.setVisibility(View.INVISIBLE);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeLang(lang);
            }
        });
        setupBarChart();
        calendar = Calendar.getInstance();
        month = calendar.get(Calendar.MONTH)+1;
        year = calendar.get(Calendar.YEAR);
        day= calendar.get(Calendar.DAY_OF_MONTH)+"";
        format = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", Locale.US);
        try {
            dateRange2=format.parse(getCurrentDate());
        } catch (ParseException e)
        {
            e.printStackTrace();
        }
        dateRange1= getFirstDate();
        barChart.clear();
        rates.clear();
        descriptions.clear();

        getRecordsBySelection(gender, scoreRange1, scoreRange2, ageRange1, ageRange2, dateRange1, dateRange2);

        transTime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0: //All
                        Toast.makeText(GraphView.this, "In Implementation", Toast.LENGTH_SHORT).show();
                        break;
                    case 1: //Custom
                        Toast.makeText(GraphView.this, "In Implementation", Toast.LENGTH_SHORT).show();
                        break;
                    case 2: //Hourly
                        barChart.clear();
                        descriptions.clear();
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
                        String currentDateandTime = sdf.format(new Date());
                        rates = getHourlyTransactions(currentDateandTime);
                        StaticsUtils.setBarChartData(context,barChart, 24, rates, descriptions);
                        break;
                    case 3:// Daily  transactions
                        barChart.clear();
                        rates.clear();
                        descriptions.clear();
                        rates = getRecordsByDays(month, year);
                        StaticsUtils.setBarChartData(context,barChart, descriptions.size(), rates, descriptions);
                        break;
                    case 4://Weekly
                        barChart.clear();
                        descriptions.clear();
                        nweeks = calendar.getActualMaximum(Calendar.WEEK_OF_MONTH);
                        rates = getRecordsByWeek(month, year);
                        StaticsUtils.setBarChartData(context,barChart, nweeks, rates, descriptions);
                        break;
                    case 5://Monthly
                        barChart.clear();
                        descriptions.clear();
                        rates = getRecordsByMonths(month, year);
                        StaticsUtils.setBarChartData(context,barChart, 12, rates, descriptions);
                        break;
                    case 6://Yearly-
                        Toast.makeText(GraphView.this, "In Implementation", Toast.LENGTH_SHORT).show();
                        break;

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        genderTrans.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0://No action
                        gender='%';
                        break;
                    case 1://Male
                        gender='M';
                        getRecordsBySelection(gender,scoreRange1,scoreRange2, ageRange1, ageRange2,dateRange1,dateRange2);
                        break;
                    case 2://Female
                        gender='F';
                        getRecordsBySelection(gender,scoreRange1,scoreRange2, ageRange1, ageRange2,dateRange1,dateRange2);
                        break;
                    case 3://All
                        gender='%';
                        getRecordsBySelection(gender,scoreRange1,scoreRange2, ageRange1, ageRange2,dateRange1,dateRange2);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        scoreTrans.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0: //All
                        scoreRange1 = 0;
                        scoreRange2 = 100;
                        break;
                    case 1:
                        scoreRange1 = 0;
                        scoreRange2 = 100;
                        getRecordsBySelection(gender, scoreRange1, scoreRange2, ageRange1, ageRange2, dateRange1, dateRange2);
                        break;
                    case 2:
                        scoreRange1 = 0;
                        scoreRange2 = 29;
                        getRecordsBySelection(gender, scoreRange1, scoreRange2, ageRange1, ageRange2, dateRange1, dateRange2);
                        break;
                    case 3:
                        scoreRange1 = 30;
                        scoreRange2 = 50;
                        getRecordsBySelection(gender, scoreRange1, scoreRange2, ageRange1, ageRange2, dateRange1, dateRange2);
                        break;
                    case 4:
                        scoreRange1 = 51;
                        scoreRange2 = 70;
                        getRecordsBySelection(gender, scoreRange1, scoreRange2, ageRange1, ageRange2, dateRange1, dateRange2);
                        break;
                    case 5:
                        scoreRange1 = 71;
                        scoreRange2 = 100;
                        getRecordsBySelection(gender, scoreRange1, scoreRange2, ageRange1, ageRange2, dateRange1, dateRange2);
                        break;

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ageTrans.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0: //All
                        ageRange1 = 0;
                        ageRange2 = 100;
                        break;
                    case 1:
                        ageRange1 = 0;
                        ageRange2 = 100;
                        getRecordsBySelection(gender, scoreRange1, scoreRange2, ageRange1, ageRange2, dateRange1, dateRange2);
                        break;
                    case 2:
                        ageRange1 = 0;
                        ageRange2 = 19;
                        getRecordsBySelection(gender, scoreRange1, scoreRange2, ageRange1, ageRange2, dateRange1, dateRange2);
                        break;
                    case 3:
                        ageRange1 = 20;
                        ageRange2 = 34;
                        getRecordsBySelection(gender, scoreRange1, scoreRange2, ageRange1, ageRange2, dateRange1, dateRange2);
                        break;
                    case 4:
                        ageRange1 = 35;
                        ageRange2 = 50;
                        getRecordsBySelection(gender, scoreRange1, scoreRange2, ageRange1, ageRange2, dateRange1, dateRange2);
                        break;
                    case 5:
                        ageRange1 = 51;
                        ageRange2 = 64;
                        getRecordsBySelection(gender, scoreRange1, scoreRange2, ageRange1, ageRange2, dateRange1, dateRange2);
                        break;
                    case 6:
                        ageRange1 = 51;
                        ageRange2 = 64;
                        getRecordsBySelection(gender, scoreRange1, scoreRange2, ageRange1, ageRange2, dateRange1, dateRange2);
                        break;


                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        filterTrans.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position)
                {
                    case 1:
                        final Dialog dialog1 = new Dialog(GraphView.this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
                        dialog1.setContentView(R.layout.popup_time);
                        TimePicker timePickerFrom=(TimePicker)dialog1.findViewById(R.id.timePicker_from);
                        TimePicker timePickerTo=(TimePicker)dialog1.findViewById(R.id.timePicker_to);
                        final TextView fromText = (TextView)dialog1.findViewById(R.id.textViewFrom);
                        final TextView toText=(TextView)dialog1.findViewById(R.id.textViewTO);
                        TextView dateText=(TextView)dialog1.findViewById(R.id.textViewDate);
                        dateText.setText(""+day+"/"+month+"/"+year);
                        CustomButton cancelButton=(CustomButton)dialog1.findViewById(R.id.btn_settings);
                        CustomButton okButton=(CustomButton)dialog1.findViewById(R.id.btn_ok);
                        fromText.setText(timePickerFrom.getCurrentHour()+":"+timePickerFrom.getCurrentMinute());
                        hourRange1=timePickerFrom.getCurrentHour();
                        minRange1=timePickerFrom.getCurrentMinute();
                        toText.setText(timePickerTo.getCurrentHour() + ":" + timePickerTo.getCurrentMinute());
                        hourRange2 = timePickerTo.getCurrentHour();
                        minRange2 = timePickerTo.getCurrentMinute();
                        timePickerFrom.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
                            @Override
                            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                                fromText.setText(hourOfDay+":"+minute);
                                hourRange1=hourOfDay;
                                minRange1=minute;
                            }
                        });
                        timePickerTo.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
                            @Override
                            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                                toText.setText(hourOfDay+":"+minute);
                                hourRange2=hourOfDay;
                                minRange2=minute;
                            }
                        });
                        cancelButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog1.dismiss();
                            }
                        });
                        okButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                // if(hourRange1>0 && minRange1>0)
                                dateRange1=getCustomTime(day,month,year,""+hourRange1,""+minRange1);
                                // if(hourRange2>0 && minRange2>0)
                                dateRange2=getCustomTime(day,month,year,""+hourRange2,""+minRange2);
                                long days = Utils.getDaysBetweenDates(dateRange1, dateRange2);
                                if(days>=0) {
                                    getRecordsBySelection(gender, scoreRange1, scoreRange2, ageRange1, ageRange2, dateRange1, dateRange2);
                                    dialog1.dismiss();
                                }
                                else{
                                    showToast(GraphView.this,"Sorry!! Wrong Time selection.");
                                }

                            }
                        });
                        dialog1.show();

                        break;
                    case 2:
                        final Dialog dialog2 = new Dialog(GraphView.this);
                        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog2.setContentView(R.layout.popup_date);
                        final DatePicker datePickerFrom = (DatePicker)dialog2.findViewById(R.id.datePicker_from);
                        final DatePicker datePickerTo=(DatePicker)dialog2.findViewById(R.id.datePicker_to);
                        final TextView fromText2 = (TextView)dialog2.findViewById(R.id.textViewFrom);
                        final TextView toText2=(TextView)dialog2.findViewById(R.id.textViewTO);
                        CustomButton cancelButton2=(CustomButton)dialog2.findViewById(R.id.btn_settings1);
                        CustomButton okButton2=(CustomButton)dialog2.findViewById(R.id.btn_ok1);
                        final String[] toDateTime = new String[1];
                        datePickerFrom.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View v, MotionEvent event) {

                                return true;
                            }
                        });
                        datePickerTo.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View v, MotionEvent event) {

                                return true;
                            }
                        });
                        okButton2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v)
                            {
                                long days = Utils.getDaysBetweenDates(dateRange1, dateRange2);
                                if(days>=0) {
                                    getRecordsBySelection(gender, scoreRange1, scoreRange2, ageRange1, ageRange2, dateRange1, dateRange2);
                                    dialog2.dismiss();
                                }
                                else{
                                    showToast(GraphView.this,"Sorry!! Wrong Date selection.");
                                }

                            }
                        });
                        cancelButton2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v)
                            {
                                month=datePickerFrom.getMonth()+1;
                                day=datePickerFrom.getDayOfMonth()+"";
                                year=datePickerFrom.getYear();
                                String fromDateTime=day+"-"+TransactionStatics.getMonthName(month)+"-"+year;
                                dateRange1=getCustomDateFrom(datePickerFrom.getDayOfMonth(), month, year);
                                fromText2.setText(fromDateTime);

                                toDateTime[0] =datePickerTo.getDayOfMonth()+"-"+TransactionStatics.getMonthName(datePickerTo.getMonth() + 1)+"-"+datePickerTo.getYear();
                                dateRange2=getCustomDateTo(datePickerTo.getDayOfMonth(), month, year);
                                toText2.setText(toDateTime[0]);

                            }
                        });
                        dialog2.show();
                        break;



                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void initSpinner() {
        transTime=(Spinner)findViewById(R.id.spinnerTime);
        genderTrans=(Spinner)findViewById(R.id.spinnerGender);
        scoreTrans=(Spinner)findViewById(R.id.spinnerScore);
        ageTrans=(Spinner)findViewById(R.id.spinnerAge);
        barChart = (BarChart)findViewById(R.id.barChart);
        filterTrans=(Spinner)findViewById(R.id.spinnerDateTime);

        ArrayAdapter<String> adapterAge = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.age_type_arrays));
        ageTrans.setAdapter(adapterAge);

        ArrayAdapter<String> adapterAmount = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.score_type_arrays));
        scoreTrans.setAdapter(adapterAmount);

        ArrayAdapter<String> adapterGender = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.gender_type_arrays));
        genderTrans.setAdapter(adapterGender);

        ArrayAdapter<String> adapterDateTime = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.filter_type_arrays));
        filterTrans.setAdapter(adapterDateTime);

    }

    private void getRecordsBySelection(char g, int scoreRange1, int scoreRange2, int ageRange1, int ageRange2,Date dateRange1,Date dateRange2) {
        float question1=0;
        float question2=0;
        float question3=0;
        float question4=0;
        float question5=0;
        float question6=0;
        float question7=0;
        float question8=0;
        rates.clear();
        descriptions.clear();
        barChart.clear();
        customerSurveyList =  db.getSelectedSurveyDataByRange(g, scoreRange1, scoreRange2, ageRange1, ageRange2, dateRange1,dateRange2);
        if(customerSurveyList.size()>0) {

            for (int i = 0; i < customerSurveyList.size(); i++) {
                if (!customerSurveyList.get(i).getQ1().equals("X"))
                    question1 = question1 + Float.parseFloat(customerSurveyList.get(i).getQ1());
                if (!customerSurveyList.get(i).getQ2().equals("X"))
                    question2 = question2 + Float.parseFloat(customerSurveyList.get(i).getQ2());
                if (!customerSurveyList.get(i).getQ3().equals("X"))
                    question3 = question3 + Float.parseFloat(customerSurveyList.get(i).getQ3());
                if (!customerSurveyList.get(i).getQ4().equals("X"))
                    question4 = question4 + Float.parseFloat(customerSurveyList.get(i).getQ4());
                if (!customerSurveyList.get(i).getQ5().equals("X"))
                    question5 = question5 + Float.parseFloat(customerSurveyList.get(i).getQ5());
                if (!customerSurveyList.get(i).getQ6().equals("X"))
                    question6 = question6 + Float.parseFloat(customerSurveyList.get(i).getQ6());
                if (!customerSurveyList.get(i).getQ7().equals("X"))
                    question7 = question7 + Float.parseFloat(customerSurveyList.get(i).getQ7());
                if (!customerSurveyList.get(i).getQ8().equals("X"))
                    question8 = question8 + Float.parseFloat(customerSurveyList.get(i).getQ8());
            }
            rates.add(String.valueOf(question1));
            rates.add(String.valueOf(question2));
            rates.add(String.valueOf(question3));
            rates.add(String.valueOf(question4));
            rates.add(String.valueOf(question5));
            rates.add(String.valueOf(question6));
            rates.add(String.valueOf(question7));
            rates.add(String.valueOf(question8));
            for (int k = 0; k < 8; k++) {
                Log.e("", "Ques" + (k + 1));
                descriptions.add("Ques" + (k + 1));
            }
            StaticsUtils.setBarChartData(context,barChart, descriptions.size(), rates, descriptions);
        }else
            showToast(GraphView.this,"Sorry!! No Records between this range.");
    }

    private ArrayList<String> getHourlyTransactions(String currentDateandTime) {
        HashMap<String,String> hourWiseList = new HashMap<>();
        for(int i=0; i<surveyDetailsArrayList.size();i++)
        {
            if(currentDateandTime.contains(surveyDetailsArrayList.get(i).getDate()))
            {
                String hour=surveyDetailsArrayList.get(i).getHours()+"";
                String amount=surveyDetailsArrayList.get(i).getTotal()+"";
                if(hourWiseList.containsKey(hour))
                {
                    Float temp = Float.parseFloat(hourWiseList.get(hour));
                    temp=temp+Float.parseFloat(amount);
                    hourWiseList.put(hour, ""+temp);
                }else {
                    hourWiseList.put(hour, amount);
                }

            }
        }
        ArrayList<String> list = new ArrayList<>();
        for(int j=1;j<=24;j++)
        {
            descriptions.add(""+j);
            if(hourWiseList.containsKey(""+j)){
                list.add(hourWiseList.get(""+j));
            }else {
                list.add("0");
            }
        }
        return list;
    }

    private ArrayList<String> getRecordsByMonths(int month, int year) {

        ArrayList<String> totalAmount=new ArrayList<>();//rates
        Double jan=0.0,feb=0.0,mar=0.0,apr=0.0,may=0.0,jun=0.0,jul=0.0,aug=0.0,sep=0.0,oct=0.0,nov=0.0,dec=0.0;
        int totalDays = TransactionStatics.getTotalDaysInMonth(year, month);
        String m = TransactionStatics.getMonthName(month);
        for(int i=1;i<=totalDays;i++) {
            String date = i + "-" + m + "-" + year;
            if (dateWiseList.containsKey(date)) {
                if (dateWiseList.containsKey(date)) {
                    switch (dateWiseList.get(date).getMonth()) {
                        case 1:
                            jan = jan + dateWiseList.get(date).getTotal();
                            break;
                        case 2:
                            feb = feb + dateWiseList.get(date).getTotal();
                            break;
                        case 3:
                            mar = mar + dateWiseList.get(date).getTotal();
                            break;
                        case 4:
                            apr = apr + dateWiseList.get(date).getTotal();
                            break;
                        case 5:
                            may = may + dateWiseList.get(date).getTotal();
                            break;
                        case 6:
                            jun = jun + dateWiseList.get(date).getTotal();
                            break;
                        case 7:
                            jul = jul + dateWiseList.get(date).getTotal();
                            break;
                        case 8:
                            aug = aug + dateWiseList.get(date).getTotal();
                            break;
                        case 9:
                            sep = sep + dateWiseList.get(date).getTotal();
                            break;
                        case 10:
                            oct = oct + dateWiseList.get(date).getTotal();
                            break;
                        case 11:
                            nov = nov + dateWiseList.get(date).getTotal();
                            break;
                        case 12:
                            dec = dec + dateWiseList.get(date).getTotal();
                            break;

                    }
                } else {
                    switch (TransactionStatics.getWeekOfMonth(date)) {
                        case 1:
                            jan = jan + 0;
                            break;
                        case 2:
                            feb = feb + 0;
                            break;
                        case 3:
                            mar = mar + 0;
                            break;
                        case 4:
                            apr = apr + 0;
                            break;
                        case 5:
                            may = may + 0;
                            break;
                        case 6:
                            jun = jun + 0;
                            break;
                        case 7:
                            jul = jul + 0;
                            break;
                        case 8:
                            aug = aug + 0;
                            break;
                        case 9:
                            sep = sep + 0;
                            break;
                        case 10:
                            oct = oct + 0;
                            break;
                        case 11:
                            nov = nov + 0;
                            break;
                        case 12:
                            dec = dec + 0;
                            break;
                    }
                }

            }
        }
        descriptions.add("Jan");
        descriptions.add("Feb");
        descriptions.add("Mar");
        descriptions.add("Apr");
        descriptions.add("May");
        descriptions.add("Jun");
        descriptions.add("Jul");
        descriptions.add("Aug");
        descriptions.add("Sep");
        descriptions.add("Oct");
        descriptions.add("Nov");
        descriptions.add("Dec");

        totalAmount.add(Double.toString(jan));
        totalAmount.add(Double.toString(feb));
        totalAmount.add(Double.toString(mar));
        totalAmount.add(Double.toString(apr));
        totalAmount.add(Double.toString(may));
        totalAmount.add(Double.toString(jun));
        totalAmount.add(Double.toString(jul));
        totalAmount.add(Double.toString(aug));
        totalAmount.add(Double.toString(sep));
        totalAmount.add(Double.toString(oct));
        totalAmount.add(Double.toString(nov));
        totalAmount.add(Double.toString(dec));
        return totalAmount;
    }

    private ArrayList<String> getRecordsByWeek(int month, int year) {
        ArrayList<String> totalAmount=new ArrayList<>();//rates
        int totalDays = TransactionStatics.getTotalDaysInMonth(year, month);
        Double week1=0.0, week2=0.0,week3=0.0,week4=0.0,week5=0.0,week6=0.0;
        String m = TransactionStatics.getMonthName(month);
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        for(int i=1;i<=totalDays;i++)
        {
            String date=i+"-"+m+"-"+year;
            if(dateWiseList.containsKey(date))
            {
                switch (dateWiseList.get(date).getWeek()){
                    case 1:
                        week1=week1+dateWiseList.get(date).getTotal();
                        break;
                    case 2:
                        week2=week2+dateWiseList.get(date).getTotal();
                        break;
                    case 3:
                        week3=week3+dateWiseList.get(date).getTotal();
                        break;
                    case 4:
                        week4=week4+dateWiseList.get(date).getTotal();
                        break;
                    case 5:
                        week5=week5+dateWiseList.get(date).getTotal();
                        break;
                    case 6:
                        week6=week6+dateWiseList.get(date).getTotal();
                        break;

                }
            }else{
                switch (TransactionStatics.getWeekOfMonth(date)){
                    case 1:
                        week1=week1+0;
                        break;
                    case 2:
                        week2=week2+0;
                        break;
                    case 3:
                        week3=week3+0;
                        break;
                    case 4:
                        week4=week4+0;
                        break;
                    case 5:
                        week5=week5+0;
                        break;
                    case 6:
                        week6=week6+0;
                        break;
                }
            }
        }
        descriptions.add("Week1");
        descriptions.add("Week2");
        descriptions.add("Week3");
        descriptions.add("Week4");
        descriptions.add("Week5");
        if(nweeks>5) {
            descriptions.add("Week6");
            totalAmount.add(Double.toString(week6));
        }
        totalAmount.add(Double.toString(week1));
        totalAmount.add(Double.toString(week2));
        totalAmount.add(Double.toString(week3));
        totalAmount.add(Double.toString(week4));
        totalAmount.add(Double.toString(week5));
        return totalAmount;
    }

    private void getAllRecordsByDate() {
        dateWiseList = new HashMap<>();
        receivedMoney = db.getReceivedTransationDetails();
        int j = 0;
        String date;
        int size = receivedMoney.size();
        while (size>0){
            String comment= receivedMoney.get(size-1).getmSurvery();
            Double total=0.0;
            try {
                comment = URLDecoder.decode(comment, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            if(comment.contains("{"))
            {
                SurveyDetailsModel surveyDetailsModel = new SurveyDetailsModel();
                try {
                    JSONArray jsonSurvey = new JSONArray("["+comment+"]");
                    JSONObject jsonObjQue=(JSONObject)jsonSurvey.get(8);
                    if(jsonObjQue.has("T")) {
                        total=Double.parseDouble(jsonObjQue.getString("T"));

                    }

                    date=TransactionStatics.formatDateFromString(receivedMoney.get(size - 1).getDate());
                    surveyDetailsModel.setTotal(total);
                    surveyDetailsModel.setHours(TransactionStatics.getHourFromDateTime(receivedMoney.get(size - 1).getDate()));
                    surveyDetailsModel.setDate(date);
                    surveyDetailsModel.setDay(TransactionStatics.getDAY_OF_MONTH(date));
                    surveyDetailsModel.setWeek(TransactionStatics.getWeekOfMonth(date));
                    surveyDetailsModel.setMonth(TransactionStatics.getMonthFromDate(date));
                    surveyDetailsModel.setYear(TransactionStatics.getYearOfDate(date));
                    dateWiseList.put(date,surveyDetailsModel);
                    surveyDetailsArrayList.add(surveyDetailsModel);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            size--;
        }
    }

    public void setupBarChart(){
        barChart = (BarChart)findViewById(R.id.barChart);
        barChart.setDrawBarShadow(false);
        barChart.setDrawValueAboveBar(true);

        barChart.setMaxVisibleValueCount(60);
        barChart.setDrawGridBackground(false);

        //barChart.getBarData().getYMax();

        XAxis xAxis = barChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setSpaceBetweenLabels(2);
        xAxis.setLabelsToSkip(0);
        //YAxisValueFormatter custom = new MyYAxisValueFormatter();

        YAxis leftAxis = barChart.getAxisLeft();
        leftAxis.setLabelCount(8, false);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(15f);

        YAxis rightAxis = barChart.getAxisRight();
        rightAxis.setDrawGridLines(false);
        rightAxis.setLabelCount(8, false);
        rightAxis.setSpaceTop(15f);
        rightAxis.setEnabled(false);

        Legend l = barChart.getLegend();
        l.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
        l.setForm(Legend.LegendForm.SQUARE);
        l.setFormSize(9f);
        l.setTextSize(11f);
        l.setXEntrySpace(4f);
    }

    @Override
    public <T> void response(int resultCode, T data) {

    }


    public ArrayList<String> getRecordsByDays(int month, int year)    {
        ArrayList<String> totalAmount=new ArrayList<>();//rates
        int totalDays = TransactionStatics.getTotalDaysInMonth(year, month);
        String m = TransactionStatics.getMonthName(month);
        for(int j=1;j<=totalDays;j++) {
            String date = j + "-" + m + "-" + year;
            Double total=0.0;
            for (int i=0; i < surveyDetailsArrayList.size();i++ ) {
                SurveyDetailsModel temp = surveyDetailsArrayList.get(i);
                if ((temp.getDate().contains(date)))
                {
                    total=total+temp.getTotal();
                }
            }
            totalAmount.add(""+total);
            descriptions.add("" + j);
        }
        return totalAmount;
    }

    private Date getFirstDate() {
        Date parsed = new Date();
        String inputDate = db.getLastSurveyDate();
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", Locale.US);
        try {
            parsed  = df.parse(inputDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return parsed;
    }

    public static Date getCustomDateFrom(int day, int month, int year) {
        String m = TransactionStatics.getMonthName(month);
        String inputDate=day+"-"+m+"-"+year+" 00:00:00";
        Date parsed = new Date();
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", Locale.US);
        try {
            parsed  = df.parse(inputDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return parsed;
    }

    public static Date getCustomDateTo(int day, int month, int year) {
        String m = TransactionStatics.getMonthName(month);
        String inputDate=day+"-"+m+"-"+year+" 23:59:59";
        Date parsed = new Date();
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", Locale.US);
        try {
            parsed  = df.parse(inputDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return parsed;
    }

    public static String getCurrentDate() {
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", Locale.US);
        return  df.format(Calendar.getInstance().getTime());
    }

    public static Date getCustomTime(String day, int month, int year,String hour, String mins)
    {
        String inputDateTime= day+"-"+TransactionStatics.getMonthName(month)+"-"+year+" "+hour+":"+mins+":"+"00";
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", Locale.US);
        Date parsed = new Date();
        try {
            parsed  = df.parse(inputDateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return parsed;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        changeLang(lang);
    }
}
