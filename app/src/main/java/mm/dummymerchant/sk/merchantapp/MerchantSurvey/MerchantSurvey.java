package mm.dummymerchant.sk.merchantapp.MerchantSurvey;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;

import mm.dummymerchant.sk.merchantapp.BaseActivity;
import mm.dummymerchant.sk.merchantapp.R;
import mm.dummymerchant.sk.merchantapp.adapter.SurveyAdapter;
import mm.dummymerchant.sk.merchantapp.model.CustomerSurveyModel;

/**
 * Created by user on 11/9/2015.
 */
public class MerchantSurvey extends BaseActivity {
    ListView listView;
    ArrayList<CustomerSurveyModel> customerSurveyList=new ArrayList<>();
    Boolean isRecord;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merchant_survey);
        initVals();
        customerSurveyList = db.getAllSurvey();
        if(customerSurveyList.size()>0) {
            isRecord=true;
            SurveyAdapter surveyAdapter = new SurveyAdapter(MerchantSurvey.this, customerSurveyList);
            listView.setAdapter(surveyAdapter);
        }else{
            isRecord=false;
            showToast(MerchantSurvey.this, "Sorry!! No Records.");
        }
        buttonLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isRecord) {
                    Intent intent = new Intent(MerchantSurvey.this, GraphView.class);
                    Bundle myBundle = new Bundle();
                    intent.putExtras(myBundle);
                    startActivity(intent);
                }else
                    showToast(MerchantSurvey.this, "Sorry!! No Records.");
            }
        });
    }

    private void initVals() {
        mcustomHeaderBg.setBackgroundResource(R.drawable.header_bg2);
        buttonLogout.setText(R.string.graph_view);
        mTitleOfHeader.setText("Merchant Survey");
        listView = (ListView)findViewById(R.id.survey_listView);
    }

    @Override
    public <T> void response(int resultCode, T data) {

    }

}
