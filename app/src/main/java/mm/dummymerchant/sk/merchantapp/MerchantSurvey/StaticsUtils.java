package mm.dummymerchant.sk.merchantapp.MerchantSurvey;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.formatter.YAxisValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import mm.dummymerchant.sk.merchantapp.Utils.Utils;

/**
 * Created by user on 11/9/2015.
 */
public class StaticsUtils
{
    static String maxVal = "";
    static String minVal = "";
    static float max=0;
    static float min = 0;
    static Context mContext;

    public static void setBarChartData(Context context,BarChart barChart,int count,ArrayList<String> rates,ArrayList<String> descriptions){
        mContext=context;
        ArrayList<String> xVals = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            xVals.add(descriptions.get(i));
        }

        ArrayList<BarEntry> yVals1 = new ArrayList<>();

        for (int i = 0; i < count; i++) {
            yVals1.add(new BarEntry(Float.parseFloat(rates.get(i)), i));
        }


        maxVal = Collections.max(rates);
        min = Float.parseFloat(maxVal);
        max = 0;

        for (int i = 0; i < rates.size(); i++) {

            if (Float.parseFloat(rates.get(i)) > max) {
                max = Float.parseFloat(rates.get(i));
                Log.e("Max",""+max);
            }
        }

        for (int i = 0; i < rates.size(); i++) {
            Log.e("Rate",Float.parseFloat(rates.get(i))+"");
            if (Float.parseFloat(rates.get(i)) < min && Float.parseFloat(rates.get(i))!=0) {
                min = Float.parseFloat(rates.get(i));
            }
        }


        MyBarDataSet myBarDataSet = new MyBarDataSet(yVals1, "");
        myBarDataSet.setBarSpacePercent(35f);
        myBarDataSet.setColors(new int[]{Color.BLUE, Color.RED, Color.CYAN});

        ArrayList<BarDataSet> dataSets = new ArrayList<BarDataSet>();
        dataSets.add(myBarDataSet);

        BarData data = new BarData(xVals, dataSets);
        data.setValueFormatter(new MyValueFormatter(data));
        data.setValueTextSize(10f);
        barChart.setDescription("Total");
        barChart.setData(data);
    }

    public static class MyValueFormatter implements ValueFormatter  {
        private DecimalFormat mFormat;
        BarData data;
        public MyValueFormatter(BarData data) {
            this.data=data;
            mFormat = new DecimalFormat("###,###,##0.0");
        }
        @Override
        public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {

            String val = value+"";
            return String.valueOf(Utils.formatedAmount_t(val, mContext));
           /* if(val.contains(".")) {
                return mFormat.format(value) + "";
            }
            else
                return value+"";*/
        }
    }

    public static class MyBarDataSet extends BarDataSet {

        public MyBarDataSet(List<BarEntry> yVals, String label) {
            super(yVals, label);
        }

        @Override
        public int getColor(int index) {
            if(getEntryForXIndex(index).getVal() == max)
                return mColors.get(0);
            else if(getEntryForXIndex(index).getVal() == min)
                return mColors.get(1);
            else
                return mColors.get(2);
        }
    }

    public static class YValueFormatter implements YAxisValueFormatter {
        private DecimalFormat mFormat;
        public YValueFormatter() {
            mFormat = new DecimalFormat("###,###,##0");
        }
        @Override
        public String getFormattedValue(float value, YAxis yAxis) {
            return mFormat.format(value) +" MMK";
        }
    }

}
