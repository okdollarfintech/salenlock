package mm.dummymerchant.sk.merchantapp.MerchantSurvey;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;

import mm.dummymerchant.sk.merchantapp.BaseActivity;
import mm.dummymerchant.sk.merchantapp.R;
import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.Utils;
import mm.dummymerchant.sk.merchantapp.customView.CustomButton;
import mm.dummymerchant.sk.merchantapp.db.DBHelper;
import mm.dummymerchant.sk.merchantapp.model.TransactionSurvey;
import mm.dummymerchant.sk.merchantapp.model.TransationModel;

/**
 * Created by user on 11/9/2015.
 */
public class TransactionStatics extends BaseActivity {
    ArrayList<TransationModel> mTranstionList;
    BarChart barChart;
    HashMap<String, TransactionSurvey> dateWiseList = new HashMap<>();
    ArrayList<String> dateDes = new ArrayList<>();//description
    ArrayList<String> amount = new ArrayList<>();//rates
    int nweeks = 0;
    int month = 1;
    int year = 2015;
    String day = "%";
    int ageRange1 = 0;
    int ageRange2 = 100;
    int amountRange1 = 0;
    int amountRange2 = 99999999;
    int hourRange1 = 0, hourRange2 = 0, minRange1 = 0, minRange2 = 0;
    char gender = '%';
    Date dateRange1, dateRange2;
    String table = "Dr";
    Spinner transType, transTime, genderTrans, transAmount, transDateTime, ageTrans;
    Calendar calendar;
    String lang;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        buttonLogout.setVisibility(View.VISIBLE);
        buttonLogout.setText(R.string.send_report);
        buttonLogout.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
        context=TransactionStatics.this;
        mTitleOfHeader.setText(getResources().getText(R.string.transaction_statics));
        mcustomHeaderBg.setBackgroundResource(R.drawable.header_bg2);

        lang=AppPreference.getLanguage(getApplicationContext());
        changeLang("eng");
        setContentView(R.layout.activity_transaction_statics);
        initSpinners();
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeLang(lang);
            }
        });
        initBarChart();

        buttonLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Button Clickd : ");
                sendCSVFile();

            }
        });
        //setClickListener();
        calendar = Calendar.getInstance();
        month = calendar.get(Calendar.MONTH) + 1;
        year = calendar.get(Calendar.YEAR);
        day = calendar.get(Calendar.DAY_OF_MONTH) + "";
        SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", Locale.US);
        try {
            dateRange2 = format.parse(GraphView.getCurrentDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        dateRange1 = getFirstDate();
        showSelectedRecord(table, gender, amountRange1, amountRange2, ageRange1, ageRange2, dateRange1, dateRange2);
        transType.setSelection(1);
        transType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 1) {
                    //getTransferredTransactionRecord();
                    table = "Dr";
                    calendar = Calendar.getInstance();
                    month = calendar.get(Calendar.MONTH) + 1;
                    year = calendar.get(Calendar.YEAR);
                    day = calendar.get(Calendar.DAY_OF_MONTH) + "";
                    SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", Locale.US);
                    try {
                        dateRange2 = format.parse(GraphView.getCurrentDate());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    dateRange1 = getFirstDate();
                    showSelectedRecord(table, gender, amountRange1, amountRange2, ageRange1, ageRange2, dateRange1, dateRange2);
                } else if (position == 2) {
                    //getReceivedTransactionRecord();
                    table = "Cr";
                    calendar = Calendar.getInstance();
                    month = calendar.get(Calendar.MONTH) + 1;
                    year = calendar.get(Calendar.YEAR);
                    day = calendar.get(Calendar.DAY_OF_MONTH) + "";
                    SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", Locale.US);
                    try {
                        dateRange2 = format.parse(GraphView.getCurrentDate());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    dateRange1 = getFirstDate();
                    showSelectedRecord(table, gender, amountRange1, amountRange2, ageRange1, ageRange2, dateRange1, dateRange2);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        transTime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0: //All
                        break;
                    case 1: //Custom
                        Toast.makeText(TransactionStatics.this, "In Implementation", Toast.LENGTH_SHORT).show();
                        break;
                    case 2: //Hourly
                        barChart.clear();
                        dateDes.clear();
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
                        String currentDateandTime = sdf.format(new Date());
                        amount = getHourlyTransactions(currentDateandTime);
                        StaticsUtils.setBarChartData(context,barChart, 24, amount, dateDes);
                        break;
                    case 3:// Daily  transactions
                        barChart.clear();
                        dateDes.clear();
                        int totalDays = getTotalDaysInMonth(year, month);
                        amount = getRecordsByDays(month, year);
                        StaticsUtils.setBarChartData(context,barChart, totalDays, amount, dateDes);
                        break;
                    case 4://Weekly
                        barChart.clear();
                        dateDes.clear();
                        nweeks = calendar.getActualMaximum(Calendar.WEEK_OF_MONTH);
                        amount = getRecordsByWeek(month, year);
                        StaticsUtils.setBarChartData(context,barChart, nweeks, amount, dateDes);
                        break;
                    case 5://Monthly
                        barChart.clear();
                        dateDes.clear();
                        amount = getRecordsByMonths(month, year);
                        StaticsUtils.setBarChartData(context,barChart, 12, amount, dateDes);
                        break;
                    case 6://Yearly
                        Toast.makeText(TransactionStatics.this, "In Implementation", Toast.LENGTH_SHORT).show();
                        break;

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        genderTrans.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0: //All
                        gender = '%';
                        break;
                    case 1://Male
                        gender = 'M';
                        showSelectedRecord(table, gender, amountRange1, amountRange2, ageRange1, ageRange2, dateRange1, dateRange2);
                        break;
                    case 2://Female
                        gender = 'F';
                        showSelectedRecord(table, gender, amountRange1, amountRange2, ageRange1, ageRange2, dateRange1, dateRange2);
                        break;
                    case 3://Other
                        gender = '%';
                        showSelectedRecord(table, gender, amountRange1, amountRange2, ageRange1, ageRange2, dateRange1, dateRange2);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        transAmount.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 1: //All
                        amountRange1 = 0;
                        amountRange2 = 99999999;
                        showSelectedRecord(table, gender, amountRange1, amountRange2, ageRange1, ageRange2, dateRange1, dateRange2);

                        break;
                    case 2: //0-1000
                        amountRange1 = 0;
                        amountRange2 = 1000;
                        showSelectedRecord(table, gender, amountRange1, amountRange2, ageRange1, ageRange2, dateRange1, dateRange2);
                        break;
                    case 3://1001 - 10,000
                        amountRange1 = 1001;
                        amountRange2 = 10000;
                        showSelectedRecord(table, gender, amountRange1, amountRange2, ageRange1, ageRange2, dateRange1, dateRange2);
                        break;
                    case 4:// 10,001 - 50,000
                        amountRange1 = 10001;
                        amountRange2 = 50000;
                        showSelectedRecord(table, gender, amountRange1, amountRange2, ageRange1, ageRange2, dateRange1, dateRange2);
                        break;
                    case 5:// 50,001 - 1,00,000
                        amountRange1 = 50000;
                        amountRange2 = 100000;
                        showSelectedRecord(table, gender, amountRange1, amountRange2, ageRange1, ageRange2, dateRange1, dateRange2);
                        break;
                    case 6://1,00,001 - 2,00,000
                        amountRange1 = 100001;
                        amountRange2 = 200000;
                        showSelectedRecord(table, gender, amountRange1, amountRange2, ageRange1, ageRange2, dateRange1, dateRange2);
                        break;
                    case 7://2,00,001 - 5,00,000
                        amountRange1 = 200001;
                        amountRange2 = 500000;
                        showSelectedRecord(table, gender, amountRange1, amountRange2, ageRange1, ageRange2, dateRange1, dateRange2);
                        break;
                    case 8: //5,00,000 - above
                        amountRange1 = 500000;
                        amountRange2 = 99999999;
                        showSelectedRecord(table, gender, amountRange1, amountRange2, ageRange1, ageRange2, dateRange1, dateRange2);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        transDateTime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0: //All

                        break;
                    case 1: //By Time
                        final Dialog dialog1 = new Dialog(TransactionStatics.this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
                        dialog1.setContentView(R.layout.popup_time);
                        TimePicker timePickerFrom = (TimePicker) dialog1.findViewById(R.id.timePicker_from);
                        TimePicker timePickerTo = (TimePicker) dialog1.findViewById(R.id.timePicker_to);
                        final TextView fromText = (TextView) dialog1.findViewById(R.id.textViewFrom);
                        final TextView toText = (TextView) dialog1.findViewById(R.id.textViewTO);
                        TextView dateText = (TextView) dialog1.findViewById(R.id.textViewDate);
                        dateText.setText("" + day + "/" + month + "/" + year);
                        CustomButton cancelButton = (CustomButton) dialog1.findViewById(R.id.btn_settings);
                        CustomButton okButton = (CustomButton) dialog1.findViewById(R.id.btn_ok);
                        fromText.setText(timePickerFrom.getCurrentHour() + ":" + timePickerFrom.getCurrentMinute());
                        hourRange1 = timePickerFrom.getCurrentHour();
                        minRange1 = timePickerFrom.getCurrentMinute();
                        toText.setText(timePickerTo.getCurrentHour() + ":" + timePickerTo.getCurrentMinute());
                        hourRange2 = timePickerTo.getCurrentHour();
                        minRange2 = timePickerTo.getCurrentMinute();
                        timePickerFrom.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
                            @Override
                            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                                fromText.setText(hourOfDay + ":" + minute);
                                hourRange1 = hourOfDay;
                                minRange1 = minute;
                            }
                        });
                        timePickerTo.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
                            @Override
                            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                                toText.setText(hourOfDay + ":" + minute);
                                hourRange2 = hourOfDay;
                                minRange2 = minute;
                            }
                        });
                        cancelButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog1.dismiss();
                            }
                        });
                        okButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                // if(hourRange1>0 && minRange1>0)
                                dateRange1 = GraphView.getCustomTime(day, month, year, "" + hourRange1, "" + minRange1);
                                //   if(hourRange2>0 && minRange2>0)
                                dateRange2 = GraphView.getCustomTime(day, month, year, "" + hourRange2, "" + minRange2);
                                long days = Utils.getDaysBetweenDates(dateRange1, dateRange2);
                                if (days >= 0)
                                    showSelectedRecordHourly(table, gender, amountRange1, amountRange2, ageRange1, ageRange2, dateRange1, dateRange2);
                                else {
                                    showToast(TransactionStatics.this, "Sorry!! Wrong Time selection.");
                                }
                                dialog1.dismiss();
                            }
                        });
                        dialog1.show();
                        break;
                    case 2://By Date
                        final Dialog dialog2 = new Dialog(TransactionStatics.this);
                        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog2.setContentView(R.layout.popup_date);

                        final DatePicker datePickerFrom = (DatePicker) dialog2.findViewById(R.id.datePicker_from);
                        final DatePicker datePickerTo = (DatePicker) dialog2.findViewById(R.id.datePicker_to);
                        final TextView fromTextD = (TextView) dialog2.findViewById(R.id.textViewFrom);
                        final TextView toTextD = (TextView) dialog2.findViewById(R.id.textViewTO);
                        CustomButton cancelButtonD = (CustomButton) dialog2.findViewById(R.id.btn_settings1);
                        CustomButton okButtonD = (CustomButton) dialog2.findViewById(R.id.btn_ok1);
                        final String[] toDateTime = new String[1];
                        dialog2.setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {

                            }
                        });
                        datePickerFrom.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View v, MotionEvent event) {
                                return true;
                            }
                        });
                        datePickerTo.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View v, MotionEvent event) {

                                return true;
                            }
                        });
                        okButtonD.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                int month1 = datePickerFrom.getMonth() + 1;
                                int day1 = datePickerFrom.getDayOfMonth();
                                int year1 = datePickerFrom.getYear();
                                String fromDateTime = day1 + "-" + getMonthName(month1) + "-" + year1;
                                dateRange1 = GraphView.getCustomDateFrom(day1, month1, year1);
                                fromTextD.setText(fromDateTime);

                                int day2 = datePickerTo.getDayOfMonth();
                                int month2 = datePickerTo.getMonth() + 1;
                                int year2 = datePickerTo.getYear();
                                toDateTime[0] = datePickerTo.getDayOfMonth() + "-" + getMonthName(datePickerTo.getMonth() + 1) + "-" + datePickerTo.getYear();
                                dateRange2 = GraphView.getCustomDateTo(day2, month2, year2);
                                toTextD.setText(toDateTime[0]);
                                long days = Utils.getDaysBetweenDates(dateRange1, dateRange2);
                                if (days > 1)
                                    showSelectedRecord(table, gender, amountRange1, amountRange2, ageRange1, ageRange2, dateRange1, dateRange2);
                                else if (days == 0) {
                                    dateRange1 = GraphView.getCustomTime("" + day1, month1, year1, "00", "00");
                                    dateRange2 = GraphView.getCustomTime("" + day2, month2, year2, "23", "59");
                                    showSelectedRecordHourly(table, gender, amountRange1, amountRange2, ageRange1, ageRange2, dateRange1, dateRange2);
                                } else {
                                    showToast(TransactionStatics.this, "Sorry!! Wrong Date selection.");
                                }
                                dialog2.dismiss();
                            }
                        });
                        cancelButtonD.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                int month1 = datePickerFrom.getMonth() + 1;
                                int day1 = datePickerFrom.getDayOfMonth();
                                int year1 = datePickerFrom.getYear();
                                String fromDateTime = day1 + "-" + getMonthName(month1) + "-" + year1;
                                dateRange1 = GraphView.getCustomDateFrom(day1, month1, year1);
                                fromTextD.setText(fromDateTime);

                                int month2 = datePickerTo.getMonth() + 1;
                                int year2 = datePickerTo.getYear();
                                toDateTime[0] = datePickerTo.getDayOfMonth() + "-" + getMonthName(datePickerTo.getMonth() + 1) + "-" + datePickerTo.getYear();
                                dateRange2 = GraphView.getCustomDateTo(datePickerTo.getDayOfMonth(), month2, year2);
                                toTextD.setText(toDateTime[0]);

                            }
                        });
                        dialog2.show();
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ageTrans.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0: //All
                        ageRange1 = 0;
                        ageRange2 = 100;
                        break;
                    case 1:
                        ageRange1 = 0;
                        ageRange2 = 100;
                        showSelectedRecord(table, gender, amountRange1, amountRange2, ageRange1, ageRange2, dateRange1, dateRange2);
                        break;
                    case 2:
                        ageRange1 = 0;
                        ageRange2 = 19;
                        showSelectedRecord(table, gender, amountRange1, amountRange2, ageRange1, ageRange2, dateRange1, dateRange2);
                        break;
                    case 3:
                        ageRange1 = 20;
                        ageRange2 = 34;
                        showSelectedRecord(table, gender, amountRange1, amountRange2, ageRange1, ageRange2, dateRange1, dateRange2);
                        break;
                    case 4:
                        ageRange1 = 35;
                        ageRange2 = 50;
                        showSelectedRecord(table, gender, amountRange1, amountRange2, ageRange1, ageRange2, dateRange1, dateRange2);
                        break;
                    case 5:
                        ageRange1 = 51;
                        ageRange2 = 64;
                        showSelectedRecord(table, gender, amountRange1, amountRange2, ageRange1, ageRange2, dateRange1, dateRange2);
                        break;
                    case 6:
                        ageRange1 = 51;
                        ageRange2 = 64;
                        showSelectedRecord(table, gender, amountRange1, amountRange2, ageRange1, ageRange2, dateRange1, dateRange2);
                        break;


                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void sendCSVFile()
    {
        System.out.println("Mobile NO : " + AppPreference.getMyMobileNo(TransactionStatics.this));
        String fileName = "OK_Transaction_Record" + AppPreference.getMyMobileNo(TransactionStatics.this);
        System.out.println("FileName: " + fileName);

        try {
            File file = new File(Environment.getExternalStorageDirectory() + File.separator + fileName + ".csv");
            file.createNewFile();
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.append("Date");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("Amount(MMK)");
            fileWriter.append(NEW_LINE_SEPARATOR);
            for (int i = 0; i < amount.size(); i++) {
                fileWriter.append(dateDes.get(i));
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append(amount.get(i));
                fileWriter.append(NEW_LINE_SEPARATOR);
            }
            fileWriter.flush();
            fileWriter.close();

            Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent.setType("vnd.android.cursor.dir/email");
            emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(file.getAbsolutePath())));
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "OK Transaction Statics Report");
            startActivity(Intent.createChooser(emailIntent, "Report By,\nOK Android Application"));

        } catch (IOException e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();
        }

    }



    private void initSpinners()
    {
        transType = (Spinner) findViewById(R.id.spinnerTransType);
        transTime = (Spinner) findViewById(R.id.spinnerTime);
        genderTrans = (Spinner) findViewById(R.id.spinnerGender);
        transAmount = (Spinner) findViewById(R.id.spinnerAmount);
        transDateTime = (Spinner) findViewById(R.id.spinnerDateTime);
        ageTrans = (Spinner) findViewById(R.id.spinnerAge);

        ArrayAdapter<String> adapterAge = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.age_type_arrays));
        ageTrans.setAdapter(adapterAge);

        ArrayAdapter<String> adapterAmount = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.amount_type_arrays));
        transAmount.setAdapter(adapterAmount);

        ArrayAdapter<String> adapterGender = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.gender_type_arrays));
        genderTrans.setAdapter(adapterGender);

        ArrayAdapter<String> adapterType = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.type_arrays));
        transType.setAdapter(adapterType);

        ArrayAdapter<String> adapterDateTime = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.filter_type_arrays));
        transDateTime.setAdapter(adapterDateTime);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        changeLang(lang);
    }

    private Date getFirstDate() {
        Date parsed = new Date();
        String outputDate;
        String inputDate = db.getLastSurveyDate();

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", Locale.ENGLISH);
        SimpleDateFormat df_input = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", Locale.ENGLISH);


        try {
            parsed = df.parse(inputDate);
            outputDate = df_input.format(parsed);
            parsed = df_input.parse(outputDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return parsed;
    }

    private void initBarChart() {
        barChart = (BarChart) findViewById(R.id.barChartDefault);
        barChart.setDrawBarShadow(false);
        barChart.setDrawValueAboveBar(true);

        barChart.setMaxVisibleValueCount(5000);
        barChart.setDrawGridBackground(false);

        //barChart.getBarData().getYMax();

        XAxis xAxis = barChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setSpaceBetweenLabels(2);
        xAxis.setLabelsToSkip(0);

        YAxis leftAxis = barChart.getAxisLeft();
        leftAxis.setLabelCount(8, false);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(15f);
        leftAxis.setValueFormatter(new StaticsUtils.YValueFormatter());

        YAxis rightAxis = barChart.getAxisRight();
        rightAxis.setDrawGridLines(false);
        rightAxis.setLabelCount(8, false);
        rightAxis.setSpaceTop(15f);
        rightAxis.setEnabled(false);
        rightAxis.setValueFormatter(new StaticsUtils.YValueFormatter());

        Legend l = barChart.getLegend();
        l.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
        l.setForm(Legend.LegendForm.SQUARE);
        l.setFormSize(9f);
        l.setTextSize(11f);
        l.setXEntrySpace(4f);
    }

    private void getReceivedTransactionRecord() {
        dateWiseList = new HashMap<>();
        mTranstionList = new ArrayList<>();
        db = new DBHelper(getApplicationContext());
        mTranstionList = db.getReceivedTransationDetails();
        int j = 0;
        int i = 0;
        String date;
        int mDate = 1;
        for (; i < mTranstionList.size() && j < mTranstionList.size(); ) {
            date = formatDateFromString(mTranstionList.get(i).getDate());
            Double amounts = 0.0;
            for (; j < mTranstionList.size(); ) {
                TransationModel temp = mTranstionList.get(j);
                if (temp.getDate().contains(date)) {
                    BigDecimal bAmount = new BigDecimal(mTranstionList.get(j).getAmount());
                    amounts = amounts + bAmount.doubleValue();
                    j++;
                } else {
                    i = j;
                    mDate = getDAY_OF_MONTH(date);
                    break;
                }
            }
            TransactionSurvey survey = new TransactionSurvey();
            survey.setDate(mDate);
            survey.setTotalAmount(amounts);
            survey.setWeek(getWeekOfMonth(date));
            survey.setMonth(getMonthFromDate(date));
            survey.setYear(getYearOfDate(date));
            dateWiseList.put(date, survey);
        }

        barChart.clear();
        dateDes.clear();
        int totalDays = getTotalDaysInMonth(year, month);
        amount = getRecordsByDays(month, year);
        transTime.setSelection(3);
        StaticsUtils.setBarChartData(context,barChart, totalDays, amount, dateDes);
    }

    private void getTransferredTransactionRecord() {
        dateWiseList = new HashMap<>();
        db = new DBHelper(getApplicationContext());
        mTranstionList = new ArrayList<>();
        mTranstionList = db.getTransferTransationDetails();
        int j = 0;
        String date;
        int mDate;
        for (int i = 0; i < mTranstionList.size() && j < mTranstionList.size(); ) {
            date = formatDateFromString(mTranstionList.get(i).getDate());
            Double amounts = 0.0;
            for (; j < mTranstionList.size(); ) {
                TransationModel temp = mTranstionList.get(j);
                if (temp.getDate().contains(date)) {
                    BigDecimal bAmount = new BigDecimal(mTranstionList.get(j).getAmount());
                    amounts = amounts + bAmount.doubleValue();
                    j++;
                } else {
                    i = j;
                    break;
                }
            }

            mDate = getDAY_OF_MONTH(date);
            TransactionSurvey survey = new TransactionSurvey();
            survey.setDate(mDate);
            survey.setTotalAmount(amounts);
            survey.setWeek(getWeekOfMonth(date));
            survey.setMonth(getMonthFromDate(date));
            survey.setYear(getYearOfDate(date));
            dateWiseList.put(date, survey);
        }

        barChart.clear();
        dateDes.clear();
        int totalDays = getTotalDaysInMonth(year, month);
        amount = getRecordsByDays(month, year);
        transTime.setSelection(3);
        StaticsUtils.setBarChartData(context,barChart, totalDays, amount, dateDes);
    }

    @Override
    public <T> void response(int resultCode, T data) {

    }

    public static String formatDateFromString(String inputDate) {

        String outputDate = "";
        String inputFormat = "dd-MMM-yyyy HH:mm:ss";
        String outputFormat = "d-MMM-yyyy";

        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, Locale.ENGLISH);
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, Locale.ENGLISH);

        try {
            Date parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);

        } catch (ParseException e) {
            Log.d("Date", "ParseException - dateFormat");
        }

        return outputDate;

    }

    public static int getWeekOfMonth(String date) {
        Date mDate;
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat df_output = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
        try {
            mDate = df_output.parse(date);
            cal.setTime(mDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return cal.get(Calendar.WEEK_OF_MONTH);
    }

    public static int getMonthFromDate(String date) {
        Date mDate;
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat df_output = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
        try {
            mDate = df_output.parse(date);
            cal.setTime(mDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //String monthName = new SimpleDateFormat("MMMM", Locale.ENGLISH).format(cal.getTime());
        return (cal.get(Calendar.MONTH) + 1);
    }

    public static int getDAY_OF_MONTH(String date) {
        Date mDate;
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat df_output = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
        try {
            mDate = df_output.parse(date);
            cal.setTime(mDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return cal.get(Calendar.DAY_OF_MONTH);
    }

    public static int getYearOfDate(String date) {
        Date mDate;
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat df_output = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
        try {
            mDate = df_output.parse(date);
            cal.setTime(mDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return cal.get(Calendar.YEAR);
    }

    public static int getTotalDaysInMonth(int year, int month) {
        Calendar mycal = new GregorianCalendar(year, month - 1, 1);
        return mycal.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    public static String getMonthName(int month) {
        return new DateFormatSymbols().getMonths()[month - 1].substring(0, 3);
    }

    public ArrayList<String> getRecordsByDays(int month, int year) {
        ArrayList<String> totalAmount = new ArrayList<>();//rates
        int totalDays = getTotalDaysInMonth(year, month);
        String m = getMonthName(month);
        for (int i = 1; i <= totalDays; i++) {
            String date = i + "-" + m + "-" + year;
            if (dateWiseList.containsKey(date)) {
                totalAmount.add(dateWiseList.get(date).getTotalAmount() + "");
            } else {
                totalAmount.add("0");
            }
            dateDes.add("" + i);
        }
        return totalAmount;
    }

    public ArrayList<String> getRecordsByWeek(int month, int year) {
        ArrayList<String> totalAmount = new ArrayList<>();
        Double week1 = 0.0, week2 = 0.0, week3 = 0.0, week4 = 0.0, week5 = 0.0, week6 = 0.0;
        int totalDays = getTotalDaysInMonth(year, month);
        String m = getMonthName(month);
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        for (int i = 1; i <= totalDays; i++) {
            String date = i + "-" + m + "-" + year;
            if (dateWiseList.containsKey(date)) {
                switch (dateWiseList.get(date).getWeek()) {
                    case 1:
                        week1 = week1 + dateWiseList.get(date).getTotalAmount();
                        break;
                    case 2:
                        week2 = week2 + dateWiseList.get(date).getTotalAmount();
                        break;
                    case 3:
                        week3 = week3 + dateWiseList.get(date).getTotalAmount();
                        break;
                    case 4:
                        week4 = week4 + dateWiseList.get(date).getTotalAmount();
                        break;
                    case 5:
                        week5 = week5 + dateWiseList.get(date).getTotalAmount();
                        break;
                    case 6:
                        week6 = week6 + dateWiseList.get(date).getTotalAmount();
                        break;

                }
            } else {
                switch (getWeekOfMonth(date)) {
                    case 1:
                        week1 = week1 + 0;
                        break;
                    case 2:
                        week2 = week2 + 0;
                        break;
                    case 3:
                        week3 = week3 + 0;
                        break;
                    case 4:
                        week4 = week4 + 0;
                        break;
                    case 5:
                        week5 = week5 + 0;
                        break;
                    case 6:
                        week6 = week6 + 0;
                        break;
                }
            }
        }
        dateDes.add("Week1");
        dateDes.add("Week2");
        dateDes.add("Week3");
        dateDes.add("Week4");
        dateDes.add("Week5");
        if (nweeks > 5) {
            dateDes.add("Week6");
            totalAmount.add(Double.toString(week6));
        }
        totalAmount.add(Double.toString(week1));
        totalAmount.add(Double.toString(week2));
        totalAmount.add(Double.toString(week3));
        totalAmount.add(Double.toString(week4));
        totalAmount.add(Double.toString(week5));
        return totalAmount;
    }

    public ArrayList<String> getRecordsByMonths(int month, int year) {
        ArrayList<String> totalAmount = new ArrayList<>();//rates
        Double jan = 0.0, feb = 0.0, mar = 0.0, apr = 0.0, may = 0.0, jun = 0.0, jul = 0.0, aug = 0.0, sep = 0.0, oct = 0.0, nov = 0.0, dec = 0.0;
        //String m = getMonthName(month);
        for (int m = 1; m <= 12; m++) {
            int totalDays = getTotalDaysInMonth(year, m);
            for (int i = 1; i <= totalDays; i++) {
                String date = i + "-" + getMonthName(m) + "-" + year;
                if (dateWiseList.containsKey(date)) {
                    switch (dateWiseList.get(date).getMonth()) {
                        case 1:
                            jan = jan + dateWiseList.get(date).getTotalAmount();
                            break;
                        case 2:
                            feb = feb + dateWiseList.get(date).getTotalAmount();
                            break;
                        case 3:
                            mar = mar + dateWiseList.get(date).getTotalAmount();
                            break;
                        case 4:
                            apr = apr + dateWiseList.get(date).getTotalAmount();
                            break;
                        case 5:
                            may = may + dateWiseList.get(date).getTotalAmount();
                            break;
                        case 6:
                            jun = jun + dateWiseList.get(date).getTotalAmount();
                            break;
                        case 7:
                            jul = jul + dateWiseList.get(date).getTotalAmount();
                            break;
                        case 8:
                            aug = aug + dateWiseList.get(date).getTotalAmount();
                            break;
                        case 9:
                            sep = sep + dateWiseList.get(date).getTotalAmount();
                            break;
                        case 10:
                            oct = oct + dateWiseList.get(date).getTotalAmount();
                            break;
                        case 11:
                            nov = nov + dateWiseList.get(date).getTotalAmount();
                            break;
                        case 12:
                            dec = dec + dateWiseList.get(date).getTotalAmount();
                            break;

                    }
                } else {
                    switch (getMonthFromDate(date)) {
                        case 1:
                            jan = jan + 0;
                            break;
                        case 2:
                            feb = feb + 0;
                            break;
                        case 3:
                            mar = mar + 0;
                            break;
                        case 4:
                            apr = apr + 0;
                            break;
                        case 5:
                            may = may + 0;
                            break;
                        case 6:
                            jun = jun + 0;
                            break;
                        case 7:
                            jul = jul + 0;
                            break;
                        case 8:
                            aug = aug + 0;
                            break;
                        case 9:
                            sep = sep + 0;
                            break;
                        case 10:
                            oct = oct + 0;
                            break;
                        case 11:
                            nov = nov + 0;
                            break;
                        case 12:
                            dec = dec + 0;
                            break;
                    }
                }


            }
        }
        dateDes.add("Jan");
        dateDes.add("Feb");
        dateDes.add("Mar");
        dateDes.add("Apr");
        dateDes.add("May");
        dateDes.add("Jun");
        dateDes.add("Jul");
        dateDes.add("Aug");
        dateDes.add("Sep");
        dateDes.add("Oct");
        dateDes.add("Nov");
        dateDes.add("Dec");

        totalAmount.add(Double.toString(jan));
        totalAmount.add(Double.toString(feb));
        totalAmount.add(Double.toString(mar));
        totalAmount.add(Double.toString(apr));
        totalAmount.add(Double.toString(may));
        totalAmount.add(Double.toString(jun));
        totalAmount.add(Double.toString(jul));
        totalAmount.add(Double.toString(aug));
        totalAmount.add(Double.toString(sep));
        totalAmount.add(Double.toString(oct));
        totalAmount.add(Double.toString(nov));
        totalAmount.add(Double.toString(dec));
        return totalAmount;
    }

    public static int getHourFromDateTime(String date) {
        String inputFormat = "dd-MMM-yyyy HH:mm:ss";
        Date parsed = new Date();
        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, Locale.ENGLISH);
        try {
            parsed = df_input.parse(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(parsed);
        return (calendar.get(Calendar.HOUR_OF_DAY));
    }

    public ArrayList<String> getHourlyTransactions(String date) { //date=dd-MMM-yyyy
        HashMap<String, String> hourWiseList = new HashMap<>();
        for (int i = 0; i < mTranstionList.size(); i++) {
            if (mTranstionList.get(i).getDate().contains(date)) {
                String hour = mTranstionList.get(i).getHours();
                String amount = mTranstionList.get(i).getAmount();
                if (hourWiseList.containsKey(hour)) {
                    Float temp = Float.parseFloat(hourWiseList.get(hour));
                    temp = temp + Float.parseFloat(amount);
                    hourWiseList.put(hour, "" + temp);
                } else {
                    hourWiseList.put(hour, amount);
                }

            }
        }
        ArrayList<String> list = new ArrayList<>();
        for (int j = 1; j <= 24; j++) {
            dateDes.add("" + j);
            if (hourWiseList.containsKey("" + j)) {
                list.add(hourWiseList.get("" + j));
            } else {
                list.add("0");
            }
        }
        return list;
    }

    private void showSelectedRecord(String table, char gender, int amountRange1, int amountRange2, int ageRange1, int ageRange2, Date dateRange1, Date dateRange2) {

        barChart.clear();
        dateDes.clear();
        amount.clear();
        HashMap<String, TransactionSurvey> list = db.getSelectedTransactionData(table, gender, amountRange1, amountRange2, ageRange1, ageRange2, dateRange1, dateRange2);
        ArrayList<Integer> allMonths = Utils.getAllMonthsBetweenDates(dateRange1, dateRange2);
        if (list.size() > 0) {
            for (int i = 0; i < allMonths.size(); i++) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(dateRange2);
                int year = calendar.get(Calendar.YEAR);
                int totalDays = getTotalDaysInMonth(year, allMonths.get(i));
                int currentDay=calendar.get(Calendar.DAY_OF_MONTH);
                String monthName = getMonthName(allMonths.get(i));
                for (int j = 1; j <= currentDay; j++) {
                    String date = j + "-" + monthName + "-" + year;
                    if (list.containsKey(date)) {
                        amount.add("" + list.get(date).getTotalAmount());
                    } else
                        amount.add("0");
                    String yr=year+"";
                    dateDes.add(j+"-"+monthName + "-" + yr.substring(2) );
                }

            }
            StaticsUtils.setBarChartData(context,barChart, dateDes.size(), amount, dateDes);
        } else
            showToast(TransactionStatics.this, "Sorry!! No Records between this range.");
    }

    private void showSelectedRecordHourly(String table, char gender, int amountRange1, int amountRange2, int ageRange1, int ageRange2, Date dateRange1, Date dateRange2) {

        barChart.clear();
        dateDes.clear();
        amount.clear();
        HashMap<String, TransactionSurvey> list = db.getHourlyTransactionData(table, gender, amountRange1, amountRange2, ageRange1, ageRange2, dateRange1, dateRange2);
        ArrayList<Integer> totalHours = Utils.getHoursBetweenDates(dateRange1, dateRange2);
        if (list.size() > 0) {
            for (int i = 0; i < totalHours.size(); i++) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(dateRange2);
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH) + 1;
                String monthName = getMonthName(month);

                String date = day + "-" + monthName + "-" + year + " " + totalHours.get(i);
                if (list.containsKey(date)) {
                    amount.add("" + list.get(date).getTotalAmount());
                } else
                    amount.add("0");

                dateDes.add(monthName + "/" + day + " " + totalHours.get(i) + ":00");

            }
            StaticsUtils.setBarChartData(context,barChart, dateDes.size(), amount, dateDes);
        } else
            showToast(TransactionStatics.this, "Sorry!! No Records between this range.");
    }
}
