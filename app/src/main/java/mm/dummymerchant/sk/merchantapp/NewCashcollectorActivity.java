package mm.dummymerchant.sk.merchantapp;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.Utils;
import mm.dummymerchant.sk.merchantapp.Utils.WriteToExcel;
import mm.dummymerchant.sk.merchantapp.customView.CustomAutoCompleteTextView;
import mm.dummymerchant.sk.merchantapp.customView.CustomButton;
import mm.dummymerchant.sk.merchantapp.customView.CustomEdittext;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
import mm.dummymerchant.sk.merchantapp.model.CashCollectorModel;

/**
 * Created by user on 11/11/2015.
 */
public class NewCashcollectorActivity extends BaseActivity implements View.OnClickListener {

    ImageView imageView1,imageView;
    private static final int CAMERA_REQUEST = 1888;
    private CustomButton sign;
    private CustomEdittext edt_cashier_name,edt_cashier_source;
   // CustomAutoCompleteTextView edt_cashier_no;

    private CustomAutoCompleteTextView edt_customer_mobile;
    private String numberForMatching = "", countryCode = "";
    private String _mCountryCOde = "";
    private String _mCountryName = "";
    private int _flag = -1;
    CustomTextView tvCountryCode,  tv_contacts,tvCountryCode_Confirmation;
    private boolean isConfirmationActivei = false;
    private boolean isManuallyEnterNumber = false;
    int flag;
    private CustomEdittext mConfirmMobileNumber;
    private LinearLayout mConfitmationField;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edt_cashcollector);
        init();
        setNewCashcollectorActionBar(String.valueOf(getResources().getText(R.string.createcashcollector)));
    }
    void init()
    {
        imageView1 = (ImageView) findViewById(R.id.imageView1);
        sign = (CustomButton) findViewById(R.id.Signature);
        imageView = (ImageView)findViewById(R.id.imageView);
        edt_cashier_name= (CustomEdittext) findViewById(R.id.edt_cashier_name);
        edt_customer_mobile= (CustomAutoCompleteTextView) findViewById(R.id.et_digital_phoneno);
        edt_cashier_source= (CustomEdittext) findViewById(R.id.edt_cashier_source);
        //
        edt_customer_mobile=(CustomAutoCompleteTextView)findViewById(R.id.et_digital_phoneno);
        tvCountryCode = (CustomTextView) findViewById(R.id.tv_country_code);
        tv_contacts = (CustomTextView) findViewById(R.id.tv_digital_contacts);
        tvCountryCode_Confirmation = (CustomTextView) findViewById(R.id.tv_country_code_confirmaton_again);
        mConfitmationField = (LinearLayout) findViewById(R.id.send_money_confirmation_of_number);
        mConfirmMobileNumber = (CustomEdittext) findViewById(R.id.et_digital_confirm_phoneno);


        numberValidation();
        countryCode = AppPreference.getCountryCode(this);
        tvCountryCode.setText("(" + AppPreference.getCountryCode(this) + ")");
        tvCountryCode_Confirmation.setText("(" + AppPreference.getCountryCode(this) + ")");
        if (AppPreference.getCountryImageID(this) != -1) {
            // mImgCountryCode.setBackgroundResource(AppPreference.getCountryImageID(this));
            flag = AppPreference.getCountryImageID(this);
        }
        if (!AppPreference.getCountryCode(this).equalsIgnoreCase(MYANMAR_COUNTRY_CODE)) {
            edt_customer_mobile.setText(null);
            edt_customer_mobile.setHint(getResources().getString(R.string.enterDummyMerchantNumber));
        } else {
            edt_customer_mobile.setText(Utils.getMyanmarNumber());
            edt_customer_mobile.setSelection(2);
        }
        ConfirmNumberValidation();
        //

        tv_contacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                cantactPickker();
            }
        });


        callAllListeners();
    }
    void callAllListeners()
    {
        imageView1.setOnClickListener(this);
        sign.setOnClickListener(this);
        buttonLogout.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {

            case R.id.imageView1:
                intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, CAMERA_REQUEST);
                break;

            case R.id.Signature:
                intent =new Intent(getApplicationContext(),SignaturePad.class);
                startActivityForResult(intent, 2);
                break;

            case R.id.custom_actionbar_application_right_image:
                Bitmap photo,sign;
                String name,no,source;
                name = edt_cashier_name.getText().toString().trim();
                no = edt_customer_mobile.getText().toString().trim();
                source = edt_cashier_source.getText().toString().trim();
                photo = ((BitmapDrawable)imageView1.getDrawable()).getBitmap();
                sign = ((BitmapDrawable)imageView.getDrawable()).getBitmap();

                 //------------------Adding corresponding country code for mobile no
               /* String countryCode= tvCountryCode.getText().toString();
                countryCode=getUniversalCountryCode(countryCode);
                countryCode=removeSpecial(countryCode);
                no=no.substring(1);
                no=countryCode+no;*/

                no=getFormattedMobileNO(tvCountryCode,no);
               //*************Adding corresponding country code for mobile no

              //  no=Utils.getUniversalFormattedMobileNO(this,no);

                if(!name.equalsIgnoreCase("")&&!no.equalsIgnoreCase("")&&!source.equalsIgnoreCase(""))
                {
                    String id="";
                    CashCollectorModel employee_One = new CashCollectorModel(photo, name, no,source,sign,CASHCOLLECTOR_ACTIVE,id);

                    boolean check=  db.isCashCollectorPhoneNo_Present(no);

                    if(!check)
                    {
                        db.insertEmpDetails(employee_One);
                        WriteToExcel wf = new WriteToExcel();
                        wf.convertToExcelFile(this);

                        edt_cashier_name.setText("");
                        edt_customer_mobile.setText("");
                        edt_cashier_source.setText("");
                        showToast1(getString(R.string.create_cashcollector));
                        imageView1.setImageResource(R.drawable.user1);
                        imageView.setImageResource(R.drawable.sign);
                        hideKeyboard();
                    }
                    else
                        showToast(R.string.cashcoll_number_exists);

                }
                else
                {
                    String wrongText="Please Enter below fields..." + "\n";
                    if(name.equalsIgnoreCase(""))
                        wrongText+="Name" + "\n";
                    if(no.equalsIgnoreCase(""))
                        wrongText+="Mobile Number" + "\n";
                    if(source.equalsIgnoreCase(""))
                        wrongText+="Source(Place Name)" + "\n";

                    showToast(wrongText);
                    hideKeyboard();
                }
                break;
        }
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            imageView1.setImageBitmap(photo);
        }
        if (requestCode == 2)
        {
            if (data != null)
            {
                imageView.setImageBitmap(SignaturePad.signatureBitmap);
            }
        }

        if (requestCode == CALL_CONTACT_PICKER)
        {
            if (data != null) {
                String name = data.getStringExtra(KEY_NAME);
                String no = data.getStringExtra(KEY_NO);
                numberSelectedValidation(name, no);
                edt_customer_mobile.dismissDropDown();
            }
        }
    }
    @Override
    public <T> void response(int resultCode, T data)
    {

    }

    private void cantactPickker() {

        Intent call = new Intent(this, NewContactPicker.class);
        Bundle b=new Bundle();
        b.putInt(CONTACT_PICKER_KEY, VALUE_CASHCOLLECTOR_CONTACT_PICKER);
        call.putExtras(b);
        startActivityForResult(call, CALL_CONTACT_PICKER);
    }


    private void numberSelectedValidation(String name, String number)
    {
        if (number.startsWith("+") || number.startsWith("00")) {
            String[] countryCodeArray = getCountryCodeNameAndFlagFromNumber(number);
            String scountryCode = countryCodeArray[0];
            String scountryName = countryCodeArray[1];
            int sflag = Integer.parseInt(countryCodeArray[2]);
            number = countryCodeArray[3];
            if (number.contains(" ")) {
                number = number.trim();
                number = number.replace(" ", "");
            }
            setNumberIntoTheField(name, scountryCode, sflag, number, scountryName);
            Log.e("SendMoney", "Number selected =" + countryCode);
        } else {

            //showToast("happens");
            String[] countryCodeArray = getCountryCodeNameAndFlagFromNumber(number);
            String scountryCode = countryCodeArray[0];
            String scountryName = countryCodeArray[1];
            int sflag = Integer.parseInt(countryCodeArray[2]);
            number = countryCodeArray[3];
            if (number.contains(" ")) {
                number = number.trim();
                number = number.replace(" ", "");
            }
            //TODO @_CountryCode
            Log.e("CountryCode", _mCountryCOde);
            String tet = AppPreference.getCountryCode(this);
            Log.e("lof", tet);

            if (!AppPreference.getCountryCode(this).equals(_mCountryCOde))
            {
               // differentCountry(_mCountryCOde, _mCountryName, _flag, number, name);
                setNumberIntoTheField(name, scountryCode, sflag, number, scountryName);
                // showToast("executes");
            } else {
                setNumberIntoTheField(name, scountryCode, sflag, number, scountryName);
            }
        }
    }

    private void setNumberIntoTheField(String name, String countryCode, int flag, String number, String countryname)
    {
//
        tvCountryCode.setText("(" + countryCode + ")");
        //mCountryCode.setText(countryname + " (" + countryCode + ")");
        // tvCountryCode_Confirmation.setText("(" + countryCode + ")");
        isManuallyEnterNumber = true;
        textCount = 0;
        numberForMatching = number;
        tv_contacts.setText(name);
        edt_cashier_name.setText(name);

        if (countryCode.equalsIgnoreCase(MYANMAR_COUNTRY_CODE))
        {
            edt_customer_mobile.setText(getNumberWithGreyZero(number, true));
            edt_customer_mobile.setText(getNumberWithGreyZero(number, true));
            //edt_customer_name.setText(tv_contacts.getText());
            //showToast("flag.." + flag);
            //  mImgCountryCode.setBackgroundResource(flag);
            int length = edt_customer_mobile.getText().toString().length();
            edt_customer_mobile.setSelection(length);
            edt_customer_mobile.dismissDropDown();

            this.countryCode = countryCode;
            this.flag = flag;
        } else {
            String snumber = number;
            if (number.startsWith("0")) {
                snumber = number.substring(1);
                edt_customer_mobile.setText(snumber);
            }
            edt_customer_mobile.setText(getNumberWithGreyZero(number, true));
            // edt_customer_name.setText(tv_contacts.getText());
            //showToast( "flag.."+flag);
            // mImgCountryCode.setBackgroundResource(flag);
            int length = edt_customer_mobile.getText().toString().length();
            edt_customer_mobile.setSelection(length);
            edt_customer_mobile.dismissDropDown();
            this.countryCode = countryCode;
            this.flag = flag;
        }
    }

    private void differentCountry(final String countryCode, final String countryName, final int flag, final String number, final String name) {

        hideKeyboard();
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.select_number, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptsView);
        ImageView defaultImage = (ImageView) promptsView.findViewById(R.id.select_contact_defaultCountr_);
        ImageView manualImage = (ImageView) promptsView.findViewById(R.id.select_contact_changecountry);
        TextView defaultText = (TextView) promptsView.findViewById(R.id.select_number_default_country);
        TextView manually = (TextView) promptsView.findViewById(R.id.select_number_manual_country);
        defaultImage.setBackgroundResource(AppPreference.getCountryImageID(this));
        if (flag != -1) {
            manualImage.setBackgroundResource(flag);
        }
        final AlertDialog dialog1 = alertDialogBuilder.create();
        if (countryCode.equalsIgnoreCase(MYANMAR_COUNTRY_CODE))
            manually.setText("(" + countryCode + ")" + getNumberWithGreyZero(number, true));
        else
            verifyNumber(manually, number, countryCode);
        if (AppPreference.getCountryCode(this).equalsIgnoreCase(MYANMAR_COUNTRY_CODE))
            defaultText.setText("(" + AppPreference.getCountryCode(this) + ")" + getNumberWithGreyZero(number, true));
        else
            verifyNumber(defaultText, number, AppPreference.getCountryCode(this));
        defaultText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNumberIntoTheField(name, AppPreference.getCountryCode(NewCashcollectorActivity.this), AppPreference.getCountryImageID(NewCashcollectorActivity.this), number, AppPreference.getCountryName(NewCashcollectorActivity.this));
                _mCountryCOde = AppPreference.getCountryCode(NewCashcollectorActivity.this);
                edt_customer_mobile.dismissDropDown();
                dialog1.dismiss();
            }
        });
        manually.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNumberIntoTheField(name, countryCode, flag, number, countryName);
                dialog1.dismiss();
            }
        });
        dialog1.show();


    }
    private void verifyNumber(TextView tv, String number, String countryCode)
    {
        String snumber = number;
        if (number.startsWith("0")) {
            snumber = number.substring(1);
        }
        tv.setText("(" + countryCode + ")" + snumber);
    }

    private void numberValidation() {
        edt_customer_mobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().startsWith("00")) {
                    if (AppPreference.getCountryCode(NewCashcollectorActivity.this).equals(MYANMAR_COUNTRY_CODE)) {
                        edt_customer_mobile.setText("09");
                        edt_customer_mobile.setSelection(2);
                    } else {
                        edt_customer_mobile.setText(null);
                    }
                }
                if (TransactionType != 0 || isManuallyEnterNumber) {
                    isManuallyEnterNumber = true;
                    isConfirmationActivei = false;
                    mConfitmationField.setVisibility(View.GONE);
                } else {
                    if (s.length() > 5) {
                        isConfirmationActivei = true;
                        mConfitmationField.setVisibility(View.VISIBLE);
                        edt_customer_mobile.dismissDropDown();
                        //  edt_customer_name.setText(UNKNOWN);
                        TransactionType = 0;
                        // BLT_SEARCHING_TYPE = Type;

                        //------new code
                        try {
                            String number = edt_customer_mobile.getText().toString().trim();
                            String confirmNumber = mConfirmMobileNumber.getText().toString().trim();

                            if (number.length() < confirmNumber.length())
                                mConfirmMobileNumber.setText(null);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        //----------------

                    } else {
                        isConfirmationActivei = false;
                        mConfitmationField.setVisibility(View.GONE);
                    }
                }
                if (numberForMatching.length() != s.toString().length()) {
                    Drawable img = getResources().getDrawable(R.drawable.contact);
                    if (img != null) {
                        img.setBounds(0, 0, 80, 80);
                        //tvCountryCode.setCompoundDrawables(img, null, null, null);
                      /*  if(tv_contacts.getText().toString().equals(getResources().getString(R.string.Unknown)))
                        mConfitmationField.setVisibility(View.VISIBLE);*/
                        tv_contacts.setText(getResources().getString(R.string.Unknown));
                        edt_cashier_name.setText("");
                        isManuallyEnterNumber = false;
                    }
                }
                if (!isManuallyEnterNumber) {
                    if (textCount > 3) {
                        isManuallyEnterNumber = false;
                        TransactionType = 0;
                    }
                }
                textCount++;
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 6) {
                    String number = s.toString();
                    if (BLT_SEARCHING_TYPE.equals("9955") || BLT_SEARCHING_TYPE.equals("9900")) {
                        // BLT_SEARCHING_TYPE = Type;
                    }

                    if (number.startsWith("00")) {
                        String[] countryCodeArray = getCountryCodeNameAndFlagFromNumber(number);
                        String scountryCode = countryCodeArray[0];
                        String scountryName = countryCodeArray[1];
                        int sflag = Integer.parseInt(countryCodeArray[2]);
                        String no = countryCodeArray[3];
                        tvCountryCode.setText("(" + scountryCode + ")");
                        //   mImgCountryCode.setBackgroundResource(sflag);
                        flag = sflag;
                        numberForMatching = no;
                        //   mCountryCode.setText(scountryName + " (" + scountryCode + ")");
                        edt_customer_mobile.setText(no);
                        countryCode = scountryCode;
                        Log.e("SendMoney", countryCode + "and input counry code is " + scountryCode);
                    }
                }
            }
        });


    }


    private void ConfirmNumberValidation() {
        mConfirmMobileNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (isConfirmationActivei) {
                    if (!edt_customer_mobile.getText().toString().startsWith(s.toString())) {
                        mConfirmMobileNumber.setText(null);
                        return;
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }


        });
    }




}
