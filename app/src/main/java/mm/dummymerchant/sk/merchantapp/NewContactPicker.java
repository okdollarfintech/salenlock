package mm.dummymerchant.sk.merchantapp;

import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import mm.dummymerchant.sk.merchantapp.Utils.Utils;
import mm.dummymerchant.sk.merchantapp.adapter.AlphabetListAdapter;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
import mm.dummymerchant.sk.merchantapp.db.DBHelper;
import mm.dummymerchant.sk.merchantapp.httpConnection.RequestProgressDialog;
import mm.dummymerchant.sk.merchantapp.model.ConstactModel;

public class NewContactPicker extends BaseActivity implements AlphabetListAdapter.ContactOnitemClick
{
    private AlphabetListAdapter adapter = new AlphabetListAdapter();
    private GestureDetector mGestureDetector;
    private List<Object[]> alphabet = new ArrayList<Object[]>();
    private HashMap<String, Integer> sections = new HashMap<String, Integer>();
    private int sideIndexHeight;
    private static float sideIndexX;
    private static float sideIndexY;
    private AutoCompleteTextView search;
    private int indexListSize;
    List<ConstactModel> list = new ArrayList<ConstactModel>();
    private ArrayList<ConstactModel> selecteditems=new ArrayList<ConstactModel>();
    DBHelper db;
    Filter nameFilter;
    ListView mContactListActivity;
    private RequestProgressDialog mProgDailog;

    public ImageView backButton_set;
    public CustomTextView buttonLogout_set;
    LinearLayout sideIndex;
    public CustomTextView mTitleOfHeader_set;
    public RelativeLayout mcustomHeaderBg_set;
    public LinearLayout custom_action_bar_lay_submit_log_set,custom_action_bar_lay_back_set;
    Utils utils;
    @Override
    public <T> void response(int resultCode, T data) {

    }

    class SideIndexGestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            sideIndexX = sideIndexX - distanceX;
            sideIndexY = sideIndexY - distanceY;

            if (sideIndexX >= 0 && sideIndexY >= 0) {
                displayListItem();
            }

            return super.onScroll(e1, e2, distanceX, distanceY);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_alphabet);
        setCustomActionBar_set();
        setSettingHeader_sett();
        sideIndex = (LinearLayout) findViewById(R.id.sideIndex);
        mContactListActivity = (ListView) findViewById(R.id.contact_list_Activity);
      //refiresh();
        db = new DBHelper(getApplicationContext());

      //  list = db.getContactsDetailsDB();
        //after changing to json
        utils=new Utils();
        db = new DBHelper(getApplicationContext());
        String jsondata=DBHelper.getJsonContacts();
        list=utils.getContactListFromJsonString(jsondata);
//-------------------
        for(int i=0; i<list.size();i++)
        {
            selecteditems.add(list.get(i));
        }

        contactListInitiliaze();
        refiresh();



        search = (AutoCompleteTextView) findViewById(R.id.search);
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {


                nameFilter.filter(editable.toString());
                if (!editable.toString().equals("")) {


                    sideIndex.setVisibility(View.GONE);

                } else {

                    sideIndex.setVisibility(View.VISIBLE);


                }


            }
        });

        nameFilter = new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                if (constraint != null) {
                    if(!constraint.equals("")) {
                        selecteditems.clear();
                        for (ConstactModel customer : list) {
                            if (customer.getName().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                                selecteditems.add(customer);
                            }
                        }
                    }else
                    {
                        selecteditems.clear();
                        for (ConstactModel customer : list) {
                            selecteditems.add(customer);
                        }
                    }
                    FilterResults filterResults = new FilterResults();
                    filterResults.values = selecteditems;
                    filterResults.count = selecteditems.size();
                    return filterResults;
                } else {
                    return new FilterResults();
                }
            }
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
               // adapter.notifyDataSetChanged();
                contactListInitiliaze();
            }
        };



    }



    public void contactListInitiliaze()
    {
        if (null != selecteditems ) {
            if (selecteditems.size() != 0)
            {
                try{
                    Collections.sort(selecteditems, new Comparator<ConstactModel>() {

                        @Override
                        public int compare(ConstactModel lhs, ConstactModel rhs) {
                            return lhs.getName().compareToIgnoreCase(rhs.getName());
                        }
                    });
                }catch (Exception e) {

                }
            }

            mGestureDetector = new GestureDetector(this, new SideIndexGestureListener());
            List<AlphabetListAdapter.Row> rows = new ArrayList<AlphabetListAdapter.Row>();
            int start = 0;
            int end = 0;
            String previousLetter = null;
            Object[] tmpIndexItem = null;
            Pattern numberPattern = Pattern.compile("[0-9]");
            alphabet.clear();


            for (int i = 0; i < selecteditems.size(); i++) {
                String name = selecteditems.get(i).getName();
/*               if (name == null) {
                    continue;
                }*/
                if(name!=null) {
                    if (!name.equals("")) {
                        String firstLetter = name.substring(0, 1);
                        if (numberPattern.matcher(firstLetter).matches()) {
                            firstLetter = "#";
                        }

                        if (previousLetter != null && !firstLetter.equalsIgnoreCase(previousLetter)) {
                            end = rows.size() - 1;
                            tmpIndexItem = new Object[3];
                            tmpIndexItem[0] = previousLetter.toUpperCase(Locale.US);
                            tmpIndexItem[1] = start;
                            tmpIndexItem[2] = end;
                            alphabet.add(tmpIndexItem);

                            start = end + 1;
                        }
                        if (!firstLetter.equalsIgnoreCase(previousLetter)) {
                            rows.add(new AlphabetListAdapter.Section(firstLetter.toUpperCase(Locale.US)));
                            sections.put(firstLetter.toUpperCase(Locale.US), start);
                        }

                        rows.add(new AlphabetListAdapter.Item(name, selecteditems.get(i).getMobileNo(), selecteditems.get(i).getPhotoUrl()));
                        previousLetter = firstLetter;
                    }
                }
            }

            if (previousLetter != null) {

                tmpIndexItem = new Object[3];
                tmpIndexItem[0] = previousLetter;
                tmpIndexItem[0] = previousLetter.toUpperCase(Locale.US);
                tmpIndexItem[1] = start;
                tmpIndexItem[2] = rows.size() - 1;
                alphabet.add(tmpIndexItem);
            }

            adapter.setRows(rows);
            mContactListActivity.setAdapter(adapter);
            adapter.setContactClickListerner(this);

            updateList();
            finishActivitys();


        }

    }

    private void refiresh() {
        buttonLogout_set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AsyncTaskRunner as = new AsyncTaskRunner();
                as.execute("");
            }
        });
    }


    private void finishActivitys() {
        backButton_set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return mGestureDetector.onTouchEvent(event);
    }

    public void updateList() {


        sideIndex.removeAllViews();
        indexListSize = alphabet.size();
        if (indexListSize < 1) {
            return;
        }
        int indexMaxSize = (int) Math.floor(sideIndex.getHeight() / 20);
        int tmpIndexListSize = indexListSize;
        while (tmpIndexListSize > indexMaxSize) {
            tmpIndexListSize = tmpIndexListSize / 2;
        }
        double delta;
        if (tmpIndexListSize > 0) {
            delta = indexListSize / tmpIndexListSize;
        } else {
            delta = 1;
        }

        TextView tmpTV;
        for (double i = 1; i <= indexListSize; i = i + delta) {
            Object[] tmpIndexItem = alphabet.get((int) i - 1);
            String tmpLetter = tmpIndexItem[0].toString();

            tmpTV = new TextView(this);
            tmpTV.setText(tmpLetter);
            tmpTV.setGravity(Gravity.CENTER);
            tmpTV.setTextSize(12);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1);

            tmpTV.setLayoutParams(params);
            sideIndex.addView(tmpTV);
        }

        sideIndexHeight = sideIndex.getHeight();

        sideIndex.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // now you know coordinates of touch
                sideIndexX = event.getX();
                sideIndexY = event.getY();

                // and can display a proper item it country list
                displayListItem();

                return false;
            }
        });
    }

    public void displayListItem() {

        try {
            sideIndexHeight = sideIndex.getHeight();
            // compute number of pixels for every side index item
            double pixelPerIndexItem = (double) sideIndexHeight / indexListSize;

            // compute the item index for given event position belongs to
            int itemPosition = (int) (sideIndexY / pixelPerIndexItem);

            // get the item (we can do it since we know item index)
            if (itemPosition < alphabet.size()) {
                Object[] indexItem = alphabet.get(itemPosition);
                int subitemPosition = sections.get(indexItem[0]);

                //ListView listView = (ListView) findViewById(android.R.id.list);
                mContactListActivity.setSelection(subitemPosition);
            }

        }catch(NullPointerException e){
            e.printStackTrace();
        }

    }

    @Override
    public void onContactIntemClick(View requestCode) {
        TextView textView=(TextView)requestCode.getTag();
        String number=textView.getText().toString();
        for(int i=0;i<selecteditems.size();i++)
        {
            if(selecteditems.get(i).getMobileNo().equals(number))
            {
                String mobileNo = "";
                Intent data = new Intent();
                try {
                    mobileNo = selecteditems.get(i).getMobileNo();
                    mobileNo = mobileNo.replace(" ", "");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                data.putExtra("name", selecteditems.get(i).getName());
                if(mobileNo.contains(" "))
                {
                    mobileNo=mobileNo.replace(" ","");
                }
                if(mobileNo.contains("-"))
                {
                    mobileNo=mobileNo.replace("-","");
                }
                data.putExtra("no", mobileNo);
                if(!selecteditems.get(i).getPhotoUrl().equals(""))
                {
                    data.putExtra("photo",selecteditems.get(i).getPhotoUrl());
                }

                setResult(CALL_CONTACT_PICKER,data);

                finish();
            }
        }
    }

    private class AsyncTaskRunner extends AsyncTask<String, String, String> {

        private String resp;

        @Override
        protected String doInBackground(String... params) {
            try {
                readForJsonContacts();
            } catch (Exception e) {
                e.printStackTrace();
                resp = e.getMessage();
            }
            return resp;
        }

        @Override
        protected void onPostExecute(String result) {

            if (mProgDailog != null) {
                mProgDailog.dismiss();
            }

            String jsondata=DBHelper.getJsonContacts();
            list=utils.getContactListFromJsonString(jsondata);
//------------------
            selecteditems=new ArrayList<ConstactModel>();

            //list = db.getContactsDetailsDB();

            for(int i=0; i<list.size();i++)
            {
                selecteditems.add(list.get(i));
            }

            contactListInitiliaze();
        }

        @Override
        protected void onPreExecute() {
            mProgDailog = new RequestProgressDialog(NewContactPicker.this, "Please wait....Loading", 1);
            mProgDailog.show();

        }

        @Override
        protected void onProgressUpdate(String... text) {
        }
    }



    public void setCustomActionBar_set() {

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater inflater = LayoutInflater.from(this);
        View v = inflater.inflate(R.layout.custom_bar_setting, null);
        custom_action_bar_lay_back_set=(LinearLayout) v.findViewById(R.id.custom_action_bar_lay_back_set);
        backButton_set = (ImageView) v.findViewById(R.id.custom_actionbar_application_left_image_set);
        //   custom_action_bar_lay_back=(RelativeLayout) v.findViewById(R.id.custom_action_bar_lay_back);
        custom_action_bar_lay_submit_log_set=(LinearLayout) v.findViewById(R.id.custom_action_bar_lay_pay_set);
        buttonLogout_set = (CustomTextView) v.findViewById(R.id.custom_actionbar_application_right_image_set);
        mTitleOfHeader_set = (CustomTextView) v.findViewById(R.id.custom_actionbar_application_textview_title_of_application_set);
        mcustomHeaderBg_set = (RelativeLayout) v.findViewById(R.id.custom_action_bar_application_set);
        actionBar.setCustomView(v);


        custom_action_bar_lay_back_set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        backButton_set.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        actionBar.setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        Toolbar parent = (Toolbar) v.getParent();//first get parent toolbar of current action bar
        parent.setContentInsetsAbsolute(0, 0);

    }
    public void setSettingHeader_sett()
    {
        mcustomHeaderBg_set.setBackgroundResource(R.drawable.reg_header);
        backButton_set.setBackgroundResource(R.drawable.back_arrow_etn_icon);
        mTitleOfHeader_set.setText(getResources().getString(R.string.Contactlist));
        buttonLogout_set.setText("");
        buttonLogout_set.setBackgroundResource(R.drawable.refresh_icon);

    }

}
