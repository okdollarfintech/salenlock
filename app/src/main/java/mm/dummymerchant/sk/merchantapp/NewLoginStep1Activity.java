package mm.dummymerchant.sk.merchantapp;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.JsonParser;
import mm.dummymerchant.sk.merchantapp.Utils.Utils;
import mm.dummymerchant.sk.merchantapp.customView.CustomButton;
import mm.dummymerchant.sk.merchantapp.customView.CustomEdittext;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
import mm.dummymerchant.sk.merchantapp.httpConnection.CallingApIS;
import mm.dummymerchant.sk.merchantapp.model.NetworkOperatorModel;
import mm.dummymerchant.sk.merchantapp.model.OTPModel;
import mm.dummymerchant.sk.merchantapp.model.Profile;


public class NewLoginStep1Activity extends BaseActivity implements OnClickListener {
    CustomButton btnLogin;
    CustomTextView tvForgetPassword, tvRegistration, mCountryCode, tvCountryCode;
    CustomEdittext etUserId;
    private NetworkOperatorModel model;
    String number = "";
    private LinearLayout llCountryCode;
    private int flag = -1;
    private String countryCode, countryName;
    private ImageView mCountryImage;
    private OTPModel otpModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        showAlertWifi();
        setContentView(R.layout.login);
        init();
        updateVersionName();
        /*if (!Utils.isEmpty(getIntent().getStringExtra("DEFAULT_NUMBER"))) {
            //showToast("Do you want to register with "+getIntent().getStringExtra("DEFAULT_NUMBER"));
            showDefaultMobileNo("Do you want to login with " + getIntent().getStringExtra("DEFAULT_NUMBER"), getIntent().getStringExtra("DEFAULT_NUMBER"));
        }*/

        etUserId.addTextChangedListener(new MyTextWatcherForMobileNo());
    }

    private void init() {



        String operator = Utils.getSimOperater(this);
        Log.v("Operator", operator);
        if (operator != null) {
            if (!operator.equals("")) {
                model = new NetworkOperatorModel().getNetworkOperatorModel(this, Utils.getSimOperater(this));
            }
        }
     /*   else if(Utils.getMCCAndMNC(this)!=null){
            if (!Utils.getMCCAndMNC(this).equals("")) {
                model = new NetworkOperatorModel().getNetworkOperatorModel(this, Utils.getMCCAndMNC(this));
            }
     }*/

        mCountryImage = (ImageView) findViewById(R.id.img_tv_country_code);
        mCountryCode = (CustomTextView) findViewById(R.id.select_country_code);
        llCountryCode = (LinearLayout) findViewById(R.id.ll_select_country_code);
        tvCountryCode = (CustomTextView) findViewById(R.id.tv_country_code);
        tvCountryCode.setTypeface(Utils.getFont(this), Typeface.BOLD);
        btnLogin = (CustomButton) findViewById(R.id.login_button_login);
        tvForgetPassword = (CustomTextView) findViewById(R.id.login_textview_forgetPassword);
        tvRegistration = (CustomTextView) findViewById(R.id.login_textview_createAccount);

        etUserId = (CustomEdittext) findViewById(R.id.login_edittext_username);
        btnLogin.setOnClickListener(this);
        tvForgetPassword.setOnClickListener(this);
        tvRegistration.setOnClickListener(this);
        llCountryCode.setOnClickListener(this);
        //doneKeyboardListener();

        if (model != null) {
            if (!model.getCountryCode().equalsIgnoreCase("+95")) {
                etUserId.setText(null);
            } else {
                etUserId.setText(Utils.getMyanmarNumber());
                etUserId.setSelection(2);
            }
            flag = listOfFlags[model.getImageIndex()];
            countryCode = model.getCountryCode();
            tvCountryCode.setText("(" + model.getCountryCode() + ")");
            countryName = model.getCountryName();
            mCountryImage.setVisibility(View.VISIBLE);
            mCountryImage.setImageResource(flag);
            mCountryCode.setText(countryName + " (" + countryCode + ")");
        } else {
            flag = -1;
            tvCountryCode.setText("");
            etUserId.setText(null);
            countryCode = "";
            countryName = getResources().getString(R.string.select_your_country);
            //  mCountryImage.setVisibility(View.GONE);
            mCountryCode.setText(countryName);
        }
        if (!Utils.isEmpty(Utils.getPhoneNumber(this))) {
            if (AppPreference.getCountryCode(this).equalsIgnoreCase("+1")) {
                etUserId.setText(Utils.getPhoneNumber(this).substring(1));
                etUserId.setSelection(etUserId.length());
            } else {
                etUserId.setText(Utils.getPhoneNumber(this));
                etUserId.setSelection(etUserId.length());
            }
        }
    }



    private void updateVersionName() {
        try {
            PackageManager pm = getPackageManager();
            PackageInfo pi = pm.getPackageInfo(getPackageName(), 0);
            SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM dd yyyy", Locale.US);
            String updateTime = dateFormat.format( new Date( pi.lastUpdateTime ) );
            CustomTextView versionName=(CustomTextView)findViewById(R.id.appversiontext);
            versionName.setText(String.format(getString(R.string.android_version),pi.versionName +" "+updateTime));
        } catch( Exception e ) {
            e.printStackTrace();
        }
    }

    private void validation()//
    {
        if (Utils.isReallyConnectedToInternet(this)) {
            AppPreference.setInternetOn_at_login(getApplicationContext(), true);
        } else {
            AppPreference.setInternetOn_at_login(getApplicationContext(), false);
        }

        hideKeyboard();

        if (Utils.isEmpty(countryCode)) {
            showToast(getResources().getString(R.string.error_country_code));
            return;
        }

        if (!Utils.isEmpty(etUserId.getText().toString()))
        {
            if (etUserId.getText().toString().length() >= 7 && etUserId.getText().toString().length() <= 17)
            {
                String mobileNo = etUserId.getText().toString();
                mobileNo=getFormattedMobileNO(tvCountryCode,mobileNo);
                AppPreference.setMyMobileNo(getApplicationContext(), mobileNo);

                if(db.isItCashierNumber(mobileNo))
                {
                    AppPreference.setMasterOrCashier(this,CASHIER);
                    AppPreference.setCashierMobileNo(this,mobileNo);
                    ///
                    AppPreference.setVerifyNumber(getApplicationContext(), mobileNo);
                    AppPreference.setCountryImageID(getApplicationContext(), flag);
                    AppPreference.setCountryCodeFlagName(getApplicationContext(), countryName, countryCode);
                    ///
                    Intent data = new Intent(this, NewLoginStep2ActivityNew.class);
                    data.putExtra("mobileNo", etUserId.getText().toString().trim());
                    data.putExtra("formtMobileNo", AppPreference.getMyMobileNo(getApplicationContext()));
                    startActivity(data);
                }
                else  {
                    AppPreference.setMasterOrCashier(this, MASTER);


                    AppPreference.setVerifyNumber(getApplicationContext(), mobileNo);
                    AppPreference.setCountryImageID(getApplicationContext(), flag);
                    AppPreference.setCountryCodeFlagName(getApplicationContext(), countryName, countryCode);


                    try {
                        number = Utils.getUniversalFormattedMobileNO(this, etUserId.getText().toString());

                        if (Utils.isReallyConnectedToInternet(this))
                        {
                            CallingApIS CallApi = new CallingApIS(this, this);
                            CallApi.callOtpRequest(number);

                        } else if (db.getMasterDetails()!=null)
                        {
                            if(number.equals(db.getMasterDetails().getUsername()))
                            {
                                Intent data = new Intent(this, NewLoginStep2ActivityNew.class);
                                data.putExtra("mobileNo", etUserId.getText().toString().trim());
                                data.putExtra("formtMobileNo", AppPreference.getMyMobileNo(getApplicationContext()));
                                startActivity(data);
                                finish();
                            }
                            else
                                showToast(R.string.number_not_exists);
                        } else
                        {
                            showAlert(getResources().getString(R.string.async_task_no_internet));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            } else {
                showAlert(getResources().getString(R.string.please_select_proper_number));
            }


        } else {
            showToast(R.string.enter_mobile_no);

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.login_button_login:
                validation();
                break;

            case R.id.login_textview_forgetPassword:
                break;

            case R.id.login_textview_createAccount:
                break;

            case R.id.ll_select_country_code:
                Intent intent = new Intent(this, SelectCountryCode.class);
                startActivityForResult(intent, 100);
                break;

            default:
                break;
        }
    }

    private void callOptVerification(Object objet) {
        OTPModel model = (OTPModel) objet;
        if (Utils.VerifySuccessResponse(model.getDescription())) {
            Intent data = new Intent(this, NewLoginStep2ActivityNew.class);
            data.putExtra("mobileNo", etUserId.getText().toString());
            data.putExtra(SERIALIZABLE_CLASS, model);
            data.putExtra("formtMobileNo", number);
            startActivity(data);
            finish();
        } else {
            showMessage(model.getDescription());
        }
    }

    @Override
    public <T> void response(int resultCode, T data)
    {
        //callOptVerification(data);

        switch (resultCode) {


            case REQUEST_TYPE_OTP:
                otpModel = (OTPModel) data;

                if (Utils.VerifySuccessResponse(otpModel.getDescription())) {

                    Intent data1 = new Intent(this, NewLoginStep2ActivityNew.class);
                    data1.putExtra("mobileNo", etUserId.getText().toString());
                    data1.putExtra(SERIALIZABLE_CLASS, otpModel);
                    data1.putExtra("formtMobileNo", number);
                    startActivity(data1);
                    finish();
                } /*else {
                    showAlertClientNotFound(otpModel.getDescription(), etUserId.getText().toString());
                }*/


        }
    }

    private void showMessage(String message) {

        LayoutInflater li = LayoutInflater.from(NewLoginStep1Activity.this);
        View promptsView = li.inflate(R.layout.alert_window, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(NewLoginStep1Activity.this);

        alertDialogBuilder.setView(promptsView);

        final CustomTextView userInput = (CustomTextView) promptsView.findViewById(R.id.alert);
        userInput.setText(message);

        CustomButton ok = (CustomButton) promptsView.findViewById(R.id.btn_settings);


        final AlertDialog dialog1 = alertDialogBuilder.create();
        ok.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog1.dismiss();
            }
        });
        dialog1.show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100) {
            if (data != null) {
                if (data.getIntExtra("POSITION", -1) != -1) {
                    mCountryImage.setVisibility(View.VISIBLE);
                    mCountryImage.setImageResource(data.getIntExtra("POSITION", -1));
                    flag = data.getIntExtra("POSITION", -1);
                }
                mCountryCode.setText(data.getStringExtra("COUNTRY_NAME") + " (" + data.getStringExtra("COUNTRY_CODE") + ")");
                tvCountryCode.setText("(" + data.getStringExtra("COUNTRY_CODE") + ")");
                countryCode = data.getStringExtra("COUNTRY_CODE");
                countryName = data.getStringExtra("COUNTRY_NAME");

                if (!data.getStringExtra("COUNTRY_CODE").equalsIgnoreCase("+95"))
                {
                    etUserId.setText(null);
                    etUserId.setHint(getResources().getString(R.string.Enter_Your_Email_id));
                } else {
                    etUserId.setText(Utils.getMyanmarNumber());
                    etUserId.setSelection(2);
                }
            }
        }
    }

    public void showDefaultMobileNo(String msg, final String mobileNo) {

        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.gps_alert_dialog, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptsView);
        CustomTextView title = (CustomTextView) promptsView.findViewById(R.id.title);
        CustomTextView alert = (CustomTextView) promptsView.findViewById(R.id.alert);
        title.setVisibility(View.GONE);
        alert.setText(msg);
        CustomButton yes = (CustomButton) promptsView.findViewById(R.id.btn_settings);
        CustomButton no = (CustomButton) promptsView.findViewById(R.id.btn_cancel);
        yes.setText(R.string.dailog_yes);
        no.setText(R.string.dailog_no);
        final AlertDialog dialog1 = alertDialogBuilder.create();
        yes.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setNumberWithFlag(mobileNo);
                dialog1.dismiss();
            }
        });
        no.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog1.dismiss();
            }
        });
        dialog1.show();
    }

    private void setNumberWithFlag(String no) {
        if (no.startsWith("+"))
            no = no.replace("+", "00");
        no = convertValidNumber(no);
        int id = -1;
        if (no.startsWith("+") || no.startsWith("00")) {
            String code = no.substring(0, 6);
            for (int i = 0; i < listOfCountryCodesAndNames.size(); i++) {

                String countryId = listOfCountryCodesAndNames.get(i).getCountryCode().replace("+", "00");
                if (code.contains(countryId)) {
                    System.out.println(i);
                    id = i;
                    int length = countryId.length();
                    no = no.substring(length);
                    countryCode = listOfCountryCodesAndNames.get(id).getCountryCode();
                    countryName = listOfCountryCodesAndNames.get(id).getCountryName();
                    flag = listOfCountryCodesAndNames.get(i).getCountryImageId();

                    break;
                }
            }
            if (countryCode.equalsIgnoreCase("+95"))
                no = "0" + no;
        }

        if (id == -1) {
            if (no.startsWith("0")) {
                if (!AppPreference.getCountryCode(this).contains("+95"))
                    no = no.substring(1);
            } else {
                no = "0" + no;
            }
            countryCode = AppPreference.getCountryCode(this);
            countryName = AppPreference.getCountryName(this);
            flag = AppPreference.getCountryImageID(this);
        }

        if (flag != -1) {
            mCountryImage.setVisibility(View.VISIBLE);
            mCountryImage.setImageResource(flag);
        }
        mCountryCode.setText("(" + countryCode + ")");
        mCountryCode.setText(countryName + " (" + countryCode + ")");
        tvCountryCode.setText("(" + countryCode + ")");

        if (countryCode.equalsIgnoreCase("+95")) {
            etUserId.setText(getNumberWithGreyZero(no, true));

        } else {
            etUserId.setText(no);
        }
        etUserId.setSelection(no.length());
    }


    private class MyTextWatcherForMobileNo implements TextWatcher {

        public void afterTextChanged(Editable s) {

        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            try {
                if (s.length() < 2 && countryCode.equalsIgnoreCase("+95")) {
                    etUserId.setText(Utils.getMyanmarNumber());
                    etUserId.setSelection(etUserId.length());
                }
                if (s.toString().startsWith("0")) {
                    if (Utils.isValidForAutoFire(s.length() - 1, countryCode, model.getOperatorName())) {
                        btnLogin.performClick();
                    }
                } else if (Utils.isValidForAutoFire(s.length(), countryCode, model.getOperatorName())) {
                    btnLogin.performClick();
                }

            } catch (Exception e) {

            }
        }
    }


}
