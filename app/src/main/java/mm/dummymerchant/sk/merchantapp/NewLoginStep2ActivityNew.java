package mm.dummymerchant.sk.merchantapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.MySendSMS;
import mm.dummymerchant.sk.merchantapp.Utils.MySendSMSStatus;
import mm.dummymerchant.sk.merchantapp.Utils.Utils;
import mm.dummymerchant.sk.merchantapp.Utils.WriteToExcel;
import mm.dummymerchant.sk.merchantapp.customView.CustomButton;
import mm.dummymerchant.sk.merchantapp.customView.CustomEdittext;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
import mm.dummymerchant.sk.merchantapp.db.DBHelper;
import mm.dummymerchant.sk.merchantapp.httpConnection.CallingApIS;
import mm.dummymerchant.sk.merchantapp.httpConnection.HttpNewRequest;
import mm.dummymerchant.sk.merchantapp.introduction.CircularProgressBarLayout;
import mm.dummymerchant.sk.merchantapp.model.CashierActivityLogModel;
import mm.dummymerchant.sk.merchantapp.model.CashierModel;
import mm.dummymerchant.sk.merchantapp.model.LoginAPI;
import mm.dummymerchant.sk.merchantapp.model.MasterDetails;
import mm.dummymerchant.sk.merchantapp.model.OTPModel;
import mm.dummymerchant.sk.merchantapp.model.Profile;
import mm.dummymerchant.sk.merchantapp.wirelessService.WirelessUtils;

import static mm.dummymerchant.sk.merchantapp.Utils.Utils.getFont;

public class NewLoginStep2ActivityNew extends BaseActivity implements OnClickListener, CircularProgressBarLayout.OnRetriveClickListener, HttpNewRequest.LoginApiListener, MySendSMSStatus {

    public static CustomEdittext etOpt, etPassword, etMobileNo;
    public static String BROADCAST_INTENT = "com.opt.request";
    public static String OTP_NUMBER = "";
    public static NewLoginStep2ActivityNew activity;
    static boolean check = false;
   // static boolean check = true;
    static DBHelper db;
    static long mSendingDate = 0;
    static String mobilenNo = "";
    private static Activity context;
    private static CircularProgressBarLayout progressDialog;
    private RelativeLayout btnSubmit;
    private ImageView status_show_hide;
    CustomTextView tvHeader, mForgetPassword, tvCountryCode, tvPasswordCount /*, select_to_show_account*/;
    OTPModel model;
    int address = 101;
    LoginAPI loginApi;
    private Intent data;
    String mobileWithoutCountryCode = "";
    String senderMobileNumber = "";
    CashierModel cashierModel_login2;
    String user_mobileno;
    String password;

    BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {

                if (intent.hasExtra(PARAM_OTP)) {
                    if (intent.hasExtra("verify")) {
                        String verify = intent.getStringExtra("verify");
                        if (verify.equals("true")||verify.equals("swap"))
                        {
                            applicationValidation();
                            check = true;
                            etOpt.setText(OTP_NUMBER + "");
                            AppPreference.setOTP(NewLoginStep2ActivityNew.this, "");
                            AppPreference.setVerifyNumberWithCountryCode(NewLoginStep2ActivityNew.this, "");
                            AppPreference.setMyMobileNoWithoutCountryCode(NewLoginStep2ActivityNew.this, mobileWithoutCountryCode);
                            if (progressDialog != null) {
                                progressDialog.dismiss();
                            }
                        }/* else if (verify.equals("swap")) {
                            AppPreference.setOTP(context, "");
                            showAlert(getString(R.string.sim_swap));
                            voiceSwapSIM(R.raw.sim_card2);
                            dismissDialog();
                        } */else {
                            //showMessage(getString(R.string.sim_check_msg));

                        }
                    }

                }

            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        AppPreference.setAuthToken(this, "XX");
        showAlertWifi();
        setContentView(R.layout.otp_request_code);
        getSupportActionBar().hide();
        context = activity = this;
        db = new DBHelper(getApplicationContext());
        init();
        IntentData();
        RegisterBroadCast();
        updateVersionName();

        if (db.getSimId().equals("")) {
            progressDialog = new CircularProgressBarLayout(NewLoginStep2ActivityNew.this, address);
            progressDialog.Set(NewLoginStep2ActivityNew.this);
            progressDialog.setCancelable(false);
            progressDialog.show();

            // old code
           /* new android.os.Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    senderMobileNumber = Utils.getUniversalFormattedMobileNOWithCountryCode(AppPreference.getCountryCode(NewLoginStep2ActivityNew.this), mobileWithoutCountryCode);
                    AppPreference.setVerifyNumberWithCountryCode(NewLoginStep2ActivityNew.this, senderMobileNumber);
                    long OTP = get4DigitRandomNumber();
                    OTP_NUMBER = getResources().getString(R.string.sms_otp1) + " " + OTP_NUMBER +""+new   Utils().getDeviceIMEI(NewLoginStep2ActivityNew.this)+ getResources().getString(R.string.sms_otp2);
                    AppPreference.setOTP(NewLoginStep2ActivityNew.this, OTP_NUMBER);
                    new MySendSMS(NewLoginStep2ActivityNew.this).sendSMS(senderMobileNumber, OTP_NUMBER);
                }
            }, 1000);*/

            // new code -- apply verify number
            new android.os.Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    senderMobileNumber = Utils.getUniversalFormattedMobileNOWithCountryCode(AppPreference.getCountryCode(NewLoginStep2ActivityNew.this), mobileWithoutCountryCode);
                    AppPreference.setVerifyNumberWithCountryCode(NewLoginStep2ActivityNew.this, senderMobileNumber);
                    String msg= Utils.randomGeneratorNumber();
                    AppPreference.setOTP(NewLoginStep2ActivityNew.this, msg);
                    new MySendSMS(NewLoginStep2ActivityNew.this).sendSMS(senderMobileNumber, msg);
                }
            }, 1000);
        } else {
            if (db.getSimId().equals(Utils.simSerialNumber(getApplicationContext()))) {
                etOpt.setVisibility(View.GONE);
                check = true;
               // tvHeader.setText(getString(R.string.login_page));
            } else {
                showMessage();
            }
        }

    }

    private static long get4DigitRandomNumber() {
        String time = System.currentTimeMillis() + "";
        long number = Long.parseLong(time);
        return number;

    }

    private void updateVersionName() {
        try {
            PackageManager pm = getPackageManager();
            PackageInfo pi = pm.getPackageInfo(getPackageName(), 0);
            SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM dd yyyy", Locale.US);
            String updateTime = dateFormat.format( new Date( pi.lastUpdateTime ) );
            CustomTextView versionName=(CustomTextView)findViewById(R.id.appversiontext);
            versionName.setText(String.format(getString(R.string.android_version),pi.versionName +" "+updateTime));
        } catch( Exception e ) {
            e.printStackTrace();
        }
    }
    private void IntentData() {
        Intent data = getIntent();
        if (data != null) {
            mobileWithoutCountryCode = data.getStringExtra("mobileNo");
            etMobileNo.setText(data.getStringExtra("mobileNo"));
            mobilenNo = data.getStringExtra("formtMobileNo");
            if (mobilenNo == null) {
                mobilenNo = AppPreference.getMyMobileNo(this);

                if (!mobilenNo.startsWith("00")) {
                    mobilenNo = Utils.getUniversalFormattedMobileNO(this, mobilenNo);
                }
            }

        } else {
            mobilenNo = AppPreference.getMyMobileNo(this);
            etMobileNo.setText(mobilenNo);
        }
       // Contact_get_remote(data.getStringExtra("mobileNo"));
    }

    @Override
    protected void onPause() {
        try {
            unregisterReceiver(receiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onPause();
    }

    private void init() {
        status_show_hide = (ImageView) findViewById(R.id.status_show_hide);


        btnSubmit = (RelativeLayout) findViewById(R.id.otp_button_otp);
        etOpt = (CustomEdittext) findViewById(R.id.otp_edittext_username);
        etPassword = (CustomEdittext) findViewById(R.id.otp_edit_text_password);
        btnSubmit.setOnClickListener(this);
        etMobileNo = (CustomEdittext) findViewById(R.id.otp_edittext_user_mobileNo);
        tvHeader = (CustomTextView) findViewById(R.id.otp_textview_Title);
        mForgetPassword = (CustomTextView) findViewById(R.id.otp_textview_forgetPassword);
        tvCountryCode = (CustomTextView) findViewById(R.id.tv_country_code);
        tvPasswordCount = (CustomTextView) findViewById(R.id.otp_text_passwrodCount);
        ImageView imgCountryCode = (ImageView) findViewById(R.id.img_tv_country_code);
        mForgetPassword.setOnClickListener(this);
        mForgetPassword.setTypeface(getFont(this), Typeface.BOLD);

        tvCountryCode.setText("(" + AppPreference.getCountryCode(this) + ")");
        tvCountryCode.setTypeface(getFont(this), Typeface.BOLD);
        if (AppPreference.getCountryImageID(this) != -1)
            imgCountryCode.setImageResource(AppPreference.getCountryImageID(this));
        if (!AppPreference.getMerchantStatus(NewLoginStep2ActivityNew.this)) {
            BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            boolean isEnabled = bluetoothAdapter.isEnabled();
            Log.d("BLT", "BLT NEW Status:" + isEnabled);
            AppPreference.setBLTStatus(NewLoginStep2ActivityNew.this, isEnabled);
        }
        passWordOntextChangeListner();
        status_show_hide.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    etMobileNo.setText(mobileWithoutCountryCode);
                    //status_show_hide.setText(getResources().getString(R.string.hide));
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    String text = "";
                    for (int i = 0; i < mobileWithoutCountryCode.length(); i++) {
                        text += "x";
                    }
                    etMobileNo.setText(text);

                }
                return true;
            }
        });

        if(!Utils.isReallyConnectedToInternet(this))
        {
            showAlertcustom(getString(R.string.offline_transaction_login));
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        RegisterBroadCast();
    }

    @Override
    public <T> void response(int resultCode, T data) {


        switch (resultCode) {
            case REQUEST_TYPE_RETRIVE_PROFILE:
                Profile profile = (Profile) data;
               // db.deleteProfile();
                if (profile != null)
                {
                 //   db.deleteProfile();
                    if (db.insertProfile(profile)) {
                        AppPreference.setIsLoginFirstTime(this, false);
                        if (AppPreference.getMasterOrCashier(this).equalsIgnoreCase(MASTER)) {
                            Intent intent = new Intent(this, SliderScreen.class);
                            startActivity(intent);
                            finish();
                        }
                    }

                    Log.v("Tag", "profile!=null");
                } else {
                    Log.v("Tag", "profile=null");
                }


                break;
        }

    }

    private void RegisterBroadCast() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(BROADCAST_INTENT);
        registerReceiver(receiver, filter);
        super.onResume();
    }


    @Override
    public void onClick(View v) {
        hideKeyboard();
        switch (v.getId()) {

            case R.id.otp_button_otp:

                if (Utils.isReallyConnectedToInternet(this))
                {
                    AppPreference.setInternetOn_at_login(this, true);
                    if (check && !Utils.isEmpty(etPassword.getText().toString()))
                    {
                        loginApi = new LoginAPI();

                        if (AppPreference.getMasterOrCashier(this).equalsIgnoreCase(MASTER))
                        {
                            loginApi.setMobileNumber(mobilenNo);
                            loginApi.setPassword(etPassword.getText().toString());
                            MasterDetails mm = db.getMasterDetails();
                            if (mm == null) {
                                db.insertMasterMobileNumber(mobilenNo, etPassword.getText().toString());
                            } else
                                db.updateMasterMobileNumber(mobilenNo, etPassword.getText().toString());

                            loginApi.setSimId(Utils.simSerialNumber(this));
                            Log.e("SimIdFromsim2", "..." + Utils.simSerialNumber(this));
                            loginApi.setMsid(Utils.getSimSubscriberID(this));
                            loginApi.setOsType(0);
                            loginApi.setAppId(1);
                            HttpNewRequest callApi = new HttpNewRequest(loginApi, NewLoginStep2ActivityNew.this, this, APP_SERVER_PARAMS_LOGIN);
                            callApi.start();

                        } else {
                            user_mobileno = etMobileNo.getText().toString().trim();
                            user_mobileno = Utils.getUniversalFormattedMobileNO(this, user_mobileno);
                            password = etPassword.getText().toString().trim();
                            CashierModel cm = db.checkCashierLogin(user_mobileno, password);

                            MasterDetails mm = db.getMasterDetails();
                            if (cm != null)
                            {
                                if (mm != null)
                                {
                                    String mNo = mm.getUsername();
                                    String password = mm.getPassword();
                                    loginApi.setMobileNumber(mNo);
                                    loginApi.setPassword(password);

                                    Log.e("masterno", "..." + mNo);
                                    Log.e("masterpwd","..."+password);

                                    loginApi.setSimId(Utils.simSerialNumber(this));
                                    loginApi.setMsid(Utils.getSimSubscriberID(this));
                                    loginApi.setOsType(0);
                                    loginApi.setAppId(1);
                                    HttpNewRequest callApi = new HttpNewRequest(loginApi, NewLoginStep2ActivityNew.this, this, APP_SERVER_PARAMS_LOGIN);
                                    callApi.start();
                                } else
                                {
                                    // showToast("You have entered wrong password");
                                }
                            } else
                                showToast("You have entered wrong password");
                        }


                    } else {
                        if (!check)
                            showToast(getString(R.string.otp_verfication));
                        if (Utils.isEmpty(etPassword.getText().toString()))
                            showToast(R.string.please_enter_password);
                    }
                }


                else if (mobilenNo.equals(AppPreference.getMyMobileNo(getApplicationContext())) && AppPreference.getMasterOrCashier(this).equals(MASTER))
                {
                elseIfConditionForMaster();
                }

                else if (mobilenNo.equals(AppPreference.getMyMobileNo(getApplicationContext())) && AppPreference.getMasterOrCashier(this).equals(CASHIER)) {
                    elseConditionForCashiers();
                } else {
                    showAlert(getString(R.string.offline_msg1));
                }
                break;
            case R.id.otp_textview_forgetPassword:
                Intent call = new Intent(this, ForgetPasswordActivity.class);
                startActivity(call);
                break;

        }
    }


    public static void fadeIn(Context ctx, View v) {
        Animation a = AnimationUtils.loadAnimation(ctx, R.anim.activity_fade_in);
        if (a != null) {
            a.reset();
            if (v != null) {
                v.clearAnimation();
                v.startAnimation(a);
                v.setVisibility(View.VISIBLE);
            }
        }
    }

    public static void fadeOut(Context ctx, View v) {
        Animation a = AnimationUtils.loadAnimation(ctx, R.anim.activity_fade_out);
        if (a != null) {
            a.reset();
            if (v != null) {
                v.clearAnimation();
                v.startAnimation(a);
                v.setVisibility(View.INVISIBLE);
            }
        }
    }

    @Override
    public void onBackPressed() {
        WirelessUtils.getInstance().setBluetooth(false, NewLoginStep2ActivityNew.this);
        finish();
    }

    public long getDate() {

        Calendar calender = Calendar.getInstance();
        return calender.getTimeInMillis();


    }

    private void applicationValidation() {

        TelephonyManager TelephonyMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (db.getSimId().equals("")) {
            String number = mobileWithoutCountryCode;
            AppPreference.setMyMobileNo(context, mobilenNo);
            AppPreference.setSimId(context, TelephonyMgr.getSimSerialNumber());
            Log.d("idd--", TelephonyMgr.getSimSerialNumber());
            AppPreference.setIsAppFirstTime(context, true);
            DBHelper.insertDeviceInfor(Utils.getDeviceIMEI(context), TelephonyMgr.getSimSerialNumber(), mobilenNo, TelephonyMgr.getSimSerialNumber());

            if (progressDialog != null) {
                progressDialog.dismiss();
                CircularProgressBarLayout.condownTimer.cancel();
            }

        } else {
            if (AppPreference.getSimId(context).equals(TelephonyMgr.getSimSerialNumber()) && mobilenNo.equals(AppPreference.getMyMobileNo(context))) {

            } else {
                showMessage();
            }
        }
    }

    private void showMessage() {

        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.alert_window, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final CustomTextView userInput = (CustomTextView) promptsView.findViewById(R.id.alert);
        userInput.setText(context.getString(R.string.sim_check));
        CustomButton ok = (CustomButton) promptsView.findViewById(R.id.btn_settings);
        ok.setText(R.string.dialog_ok);
        CustomButton cancel = (CustomButton) promptsView.findViewById(R.id.btn_cancel);
        cancel.setText(R.string.dialog_cancel);
        cancel.setVisibility(View.VISIBLE);


        final AlertDialog dialog1 = alertDialogBuilder.create();
        // set dialog message
        ok.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                callSettingScreen();
                context.finish();
                dialog1.dismiss();

            }
        });
        cancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                context.finish();
                dialog1.dismiss();
            }
        });
        dialog1.show();

    }

    private void callSettingScreen() {
        Intent callGPSSettingIntent = new Intent(android.provider.Settings.ACTION_SETTINGS);
        context.startActivity(callGPSSettingIntent);
    }

    private void readAllMessage(String number) {
        StringBuilder smsBuilder = new StringBuilder();
        final String SMS_URI_INBOX = "content://sms/inbox";
        final String SMS_URI_ALL = "content://sms/";
        try {
            Uri uri = Uri.parse(SMS_URI_INBOX);
            String[] projection = new String[]{"_id", "address", "person", "body", "date", "type"};
            Cursor cur = context.getContentResolver().query(uri, projection, "address='" + number + "'", null, "date desc");
            if (cur.moveToFirst()) {
                int index_Address = cur.getColumnIndex("address");
                int index_Person = cur.getColumnIndex("person");
                int index_Body = cur.getColumnIndex("body");
                int index_Date = cur.getColumnIndex("date");
                int index_Type = cur.getColumnIndex("type");
                do {
                    String strAddress = cur.getString(index_Address);
                    int intPerson = cur.getInt(index_Person);
                    String strbody = cur.getString(index_Body);
                    long longDate = cur.getLong(index_Date);
                    int int_Type = cur.getInt(index_Type);

                    smsBuilder.append("[ ");
                    smsBuilder.append(strAddress + ", ");
                    smsBuilder.append(intPerson + ", ");
                    smsBuilder.append(strbody + ", ");
                    smsBuilder.append(longDate + ", ");
                    smsBuilder.append(int_Type);
                    smsBuilder.append(" ]\n\n");
                    if (strbody.contains(OTP_NUMBER + "")&&(strbody.substring(13).equals(new Utils().getDeviceIMEI(NewLoginStep2ActivityNew.this))))  {

                        applicationValidation();
                        check = true;
                        etOpt.setText(OTP_NUMBER + "");
                        AppPreference.setOTP(this, "");
                        AppPreference.setVerifyNumberWithCountryCode(this, "");
                        AppPreference.setMyMobileNoWithoutCountryCode(this, mobileWithoutCountryCode);

                        if (progressDialog != null) {
                            progressDialog.dismiss();
                            CircularProgressBarLayout.condownTimer.cancel();
                        }
                        showToast(getString(R.string.enter_password));
                        return;
                    }
                } while (cur.moveToNext());

                if (!cur.isClosed()) {
                    cur.close();
                    cur = null;
                }
            } else {
                smsBuilder.append("no result!");
            } // end if

        } catch (SQLiteException ex) {
            Log.d("SQLiteException", ex.getMessage());
        }
    }


    @Override
    public void onRetrive(int id) {
        if (id == address) {
            senderMobileNumber = Utils.getUniversalFormattedMobileNOWithCountryCode(AppPreference.getCountryCode(NewLoginStep2ActivityNew.this), mobileWithoutCountryCode);
            AppPreference.setVerifyNumberWithCountryCode(getApplicationContext(), senderMobileNumber);
            String msg = Utils.randomGeneratorNumber();
            //long OTP = get4DigitRandomNumber();
            //OTP_NUMBER = getResources().getString(R.string.sms_otp1) + " " + OTP_NUMBER +""+new   Utils().getDeviceIMEI(NewLoginStep2ActivityNew.this)+ getResources().getString(R.string.sms_otp2);
            AppPreference.setOTP(NewLoginStep2ActivityNew.this, msg);
            new MySendSMS(NewLoginStep2ActivityNew.this).sendSMS(senderMobileNumber, msg);
        }
    }

    @Override
    public void backKeyPressed() {
        dismissDialog();
//        Intent call = new Intent(NewLoginStep2ActivityNew.this, NewStartUpActivty.class);
//        call.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        startActivity(call);
        //      finish();
    }

    @Override
    public void responseListener(String res, int resultCode) {


        String secureToken = "", agentName = "", agentType = "", agentLevel = "", udv1 = "", agentAuthCode = "";
        boolean authCodeStatus = false;
        if (resultCode == APP_SERVER_PARAMS_LOGIN)
        {

            try {
                JSONObject jsonObject = new JSONObject(res);
                String statusCode = jsonObject.getString("Code");
                String msg = jsonObject.getString("Msg");

                if (statusCode.equalsIgnoreCase("200")) {

                    String data = jsonObject.getString("Data");
                    JSONObject jsonObject1 = new JSONObject(data);
                    JSONArray jsonArray = jsonObject1.getJSONArray("AgentDetails");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                        secureToken = jsonObject2.getString("securetoken");
                        agentName = jsonObject2.getString("agentname");
                        agentType = jsonObject2.getString("agenttype");
                        agentLevel = jsonObject2.getString("agentLevel");
                        udv1 = jsonObject2.getString("udv1");
                        agentAuthCode = jsonObject2.getString("AuthCode");
                        authCodeStatus = jsonObject2.getBoolean("AuthCodeStatus");
                    }
                    AppPreference.setUserLevelType(this, agentLevel);
                    AppPreference.setUserType(this, agentType);
                    AppPreference.setAgentAuthCode(this, agentAuthCode);
                    AppPreference.setAgentAuthCodeStatus(this, authCodeStatus);

                    AppPreference.setCountryCodeFlagNameLogin(this, AppPreference.getCountryName(this), AppPreference.getCountryCode(this));
                    AppPreference.setCountryImageIDLogin(this, AppPreference.getCountryImageID(this));
                    AppPreference.setVerifyNumber(context, mobileWithoutCountryCode);
                    AppPreference.setMyMobileNoWithoutCountryCode(this, mobileWithoutCountryCode);
                    AppPreference.setLoginUserName(getApplicationContext(), agentName);
                    AppPreference.setAuthToken(getApplicationContext(), secureToken);
                    AppPreference.setPasword(getApplicationContext(), etPassword.getText().toString());
                    Log.e("loginpass","..."+etPassword.getText().toString());
                    Calendar calender = Calendar.getInstance();
                    AppPreference.setSmsPaymentActive(getApplicationContext(), true);
                    AppPreference.setLoginTime(getApplicationContext(), calender.getTimeInMillis());
                    //etPassword.setText("");

                    if (AppPreference.getIsLoginFirstTime(this) || !AppPreference.getMyMobileNo(this).equals(mobilenNo)) {
                        CallingApIS api = new CallingApIS(this, this, 0);
                        api.callRetriveProfile(mobilenNo, Utils.simSerialNumber(this));
                    } else {
                        if (AppPreference.getMasterOrCashier(this).equalsIgnoreCase(MASTER)) {
                            Intent call = new Intent(this, SliderScreen.class);
                            AppPreference.setOfflineLogged(this, 0);
                            call.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(call);
                            finish();
                        } else {
                            cashierModel_login2 = db.checkCashierLogin(user_mobileno, password);
                            Log.e("new", cashierModel_login2.getCashier_Name());
                            if (cashierModel_login2 != null)
                            {
                                DateFormat targetFormat = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss");
                                String loginTime = targetFormat.format(new Date());
                                String logoutTime = "Not yet";
                                String cashierId = cashierModel_login2.getCashier_Id();
                                SliderScreen.present_Cashier_Id = cashierId;
                                int status = db.isCashierPresent(cashierId);
                                if (status == 99)
                                {
                                    SliderScreen.present_Cashier_Id = cashierId;
                                    CashierModel cash_model = db.getCashierModel(cashierId);
                                    String cashier_Name = cash_model.getCashier_Name();
                                    String cashier_Number = cash_model.getCashier_Number();
                                    String logout_reason = "Not yet";

                                    CashierActivityLogModel csl = new CashierActivityLogModel("", cashierId, cashier_Name, cashier_Number, loginTime, logoutTime, logout_reason, STATUS_LOGGEDIN);
                                    db.insertCashierDetails(csl);
                                    WriteToExcel wf = new WriteToExcel();//for writing activity log on the  excel file
                                    wf.convertToExcelFile(this);
                                    Intent call = new Intent(this, SliderScreen.class);
                                    AppPreference.setOfflineLogged(this, 0);
                                    call.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(call);
                                    finish();
                                } else {
                                    if (status == 1) {
                                        SliderScreen.present_Cashier_Id = cashierId;
                                        Intent call = new Intent(this, SliderScreen.class);
                                        AppPreference.setOfflineLogged(this, 0);
                                        call.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(call);
                                        finish();
                                        //  showToast(getString(R.string.cashier_not_created));
                                    }
                                }

                            }
                        }
                    }

                    //

                    //

                    AppPreference.setMyMobileNo(getApplicationContext(), mobilenNo);

                } else if (msg.equalsIgnoreCase("Invalid PIN")) {
                    Utils.showCustomToastMsg(this, R.string.invalid_pin, false);
                } else if (statusCode.equalsIgnoreCase("-1")) {
                    Utils.showCustomToastMsg(this, R.string.api_error2, false);
                } else if (statusCode.equalsIgnoreCase("300")) {
                    AppPreference.setVerifyNumber(context, mobileWithoutCountryCode);
                    Intent call = new Intent(this, VerifyUser.class);
                    call.putExtra(MO_NUMBER, mobileWithoutCountryCode);
                    call.putExtra("mo", mobilenNo);
                    call.putExtra("COUNTRY_CODE", tvCountryCode.getText().toString());
                    call.putExtra("IMAGE_ID", AppPreference.getCountryImageID(this));
                    startActivity(call);
                    Utils.showCustomToastMsg(this, R.string.device_changed, false);
                }else if(statusCode.equalsIgnoreCase("902")) {
                    showToast("Invalid Pin. Please Try Again.");
                }
                else if(statusCode.equalsIgnoreCase("308"))
                {
                    showToast(ADV_MER_NOT_APPROVED);
                }
                else if(statusCode.equalsIgnoreCase("309"))
                {
                    showToast(USER_NT_REG);
                }
                else if(statusCode.equalsIgnoreCase("310"))
                {
                    showToast(LOGIN_FAIL_FOR_OTHERS);
                }
                else {
                    showToast(msg);
                }

            } catch (Exception e) {

            }
        }
    }

    private void passWordOntextChangeListner() {
        etPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s != null) {
                    if (s.toString().length() > 0) {
                        tvPasswordCount.setText(s.toString().length() + "");
                    } else {
                        tvPasswordCount.setText("");
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onSent() {

    }

    @Override
    public void onError(String error) {

    }

    @Override
    public void onDelivered(String msg) {
        if (!check)
            readAllMessage(senderMobileNumber);
    }

    public void voiceSwapSIM(int audioFile) {
        ((Vibrator) getSystemService(Context.VIBRATOR_SERVICE)).vibrate(500);
        MediaPlayer p = MediaPlayer.create(this, audioFile);
        p.start();
    }

    public void dismissDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            CircularProgressBarLayout.condownTimer.cancel();
        }
    }

    void elseConditionForCashiers() {

        AppPreference.setVerifyNumber(context, mobileWithoutCountryCode);
        try {
            if (etPassword.getText() != null) {
                if (etPassword.getText().toString().trim().length() < 6) {
                    showAlert(getResources().getString(R.string.password_length_check));
                    return;
                }
            } else {
                showAlert(getResources().getString(R.string.password_length_check));
                return;
            }
        } catch (Exception e) {
        }

        user_mobileno = etMobileNo.getText().toString().trim();
        user_mobileno = Utils.getUniversalFormattedMobileNO(this, user_mobileno);

        Log.e("checkno...", user_mobileno);
        Log.e("checkpwd...", etPassword.getText().toString());

        MasterDetails mm = db.getMasterDetails();
        if (mm == null) {
            showToast(R.string.no_internet);
            return;
        }


        if (db.checkCashierLogin(user_mobileno, etPassword.getText().toString()) != null) {
            Calendar calender = Calendar.getInstance();

            if (AppPreference.getOfflineLogged(this) == 0) {
                if (AppPreference.getLoginAttempt(this) < 6) {
                    cashierModel_login2 = db.checkCashierLogin(user_mobileno, etPassword.getText().toString().trim());

                    if (cashierModel_login2 != null) {
                        DateFormat targetFormat = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss");
                        String loginTime = targetFormat.format(new Date());
                        String logoutTime = "Not yet";
                        String cashierId = cashierModel_login2.getCashier_Id();
                        SliderScreen.present_Cashier_Id = cashierId;
                        int status = db.isCashierPresent(cashierId);

                        if (status == 99 || status == 1) {
                            SliderScreen.present_Cashier_Id = cashierId;
                            CashierModel cash_model = db.getCashierModel(cashierId);
                            String cashier_Name = cash_model.getCashier_Name();
                            String cashier_Number = cash_model.getCashier_Number();

                            if (status == 99) {
                                String logout_reason = "Not yet";
                                CashierActivityLogModel csl = new CashierActivityLogModel("", cashierId, cashier_Name, cashier_Number, loginTime, logoutTime, logout_reason, STATUS_LOGGEDIN);
                                db.insertCashierDetails(csl);
                                WriteToExcel wf = new WriteToExcel();//for writing activity log on the  excel file
                                wf.convertToExcelFile(this);
                            }

                            Intent call = new Intent(this, SliderScreen.class);
                            call.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            AppPreference.setLoginAttempt(this, 0);
                            AppPreference.setTwoMin_Login_Date(this, 0);
                            AppPreference.setOfflineLogged(this, 1);
                            AppPreference.setAuthToken(this, "00");
                            startActivity(call);
                            finish();
                        }
                    }
                } else {
                    showAlert(getResources().getString(R.string.reached_wrong_password_limit));
                }
            } else {
                cashierModel_login2 = db.checkCashierLogin(user_mobileno, etPassword.getText().toString().trim());
                if (calender.getTimeInMillis() - AppPreference.getLoginTime(this) < 1000 * 60 * 60 * 72) {
                    if (AppPreference.getLoginAttempt(this) < 6) {
                        if (cashierModel_login2 != null) {
                            DateFormat targetFormat = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss");
                            String loginTime = targetFormat.format(new Date());
                            String logoutTime = "Not yet";
                            String cashierId = cashierModel_login2.getCashier_Id();
                            SliderScreen.present_Cashier_Id = cashierId;
                            int status = db.isCashierPresent(cashierId);

                            if (status == 99 || status == 1) {
                                SliderScreen.present_Cashier_Id = cashierId;
                                CashierModel cash_model = db.getCashierModel(cashierId);
                                String cashier_Name = cash_model.getCashier_Name();
                                String cashier_Number = cash_model.getCashier_Number();

                                if (status == 99) {
                                    String logout_reason = "Not yet";
                                    CashierActivityLogModel csl = new CashierActivityLogModel("", cashierId, cashier_Name, cashier_Number, loginTime, logoutTime, logout_reason, STATUS_LOGGEDIN);
                                    db.insertCashierDetails(csl);
                                    WriteToExcel wf = new WriteToExcel();//for writing activity log on the  excel file
                                    wf.convertToExcelFile(this);
                                }

                                Intent call = new Intent(this, SliderScreen.class);
                                call.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                AppPreference.setLoginAttempt(this, 0);
                                AppPreference.setTwoMin_Login_Date(this, 0);
                                AppPreference.setOfflineLogged(this, 1);
                                AppPreference.setAuthToken(this, "00");
                                startActivity(call);
                                finish();
                            }

                        }
                    } else {
                        showAlert(getResources().getString(R.string.reached_wrong_password_limit));
                    }
                } else {
                    showToast(getString(R.string.offline_msg));
                }
            }
        } else {
            if (5 - AppPreference.getLoginAttempt(this) > 0) {
                if (5 - AppPreference.getLoginAttempt(this) == 2) {
                    showAlert(getResources().getString(R.string.two_attempt_left));
                    if (AppPreference.getTwoMin_login_Date(this) == 0) {
                        AppPreference.setTwoMin_Login_Date(this, getDate());
                        int login = AppPreference.getLoginAttempt(this);
                        AppPreference.setLoginAttempt(this, ++login);
                    }
                } else {
                    if (getDate() - AppPreference.getTwoMin_login_Date(this) < 1000 * 60 * 2) {
                        showAlert(getResources().getString(R.string.two_attempt_left));
                        return;
                    } else {
                        showToast(getString(R.string.pwd_check) + "\n" + getResources().getString(R.string.attempt_left) + " " + String.valueOf(5 - AppPreference.getLoginAttempt(this)));
                        int login = AppPreference.getLoginAttempt(this);
                        AppPreference.setLoginAttempt(this, ++login);
                        AppPreference.setTwoMin_Login_Date(this, 0);
                    }
                }
            } else {
                showAlert(getResources().getString(R.string.reached_wrong_password_limit));
            }
        }
        AppPreference.setInternetOn_at_login(this, false);
    }

    void elseIfConditionForMaster()
    {
        AppPreference.setVerifyNumber(context, mobileWithoutCountryCode);
        try {
            if (etPassword.getText()!=null){
                if(etPassword.getText().toString().trim().length() < 6) {
                    showAlert(getResources().getString(R.string.password_length_check));
                    return;
                }
            }
            else
            {showAlert(getResources().getString(R.string.password_length_check));
               return;
            }
        }catch (Exception e){
        }
        user_mobileno=Utils.getUniversalFormattedMobileNO(this, etMobileNo.getText().toString().trim());
        if (etPassword.getText().toString().trim().equals(db.getMasterDetails().getPassword())&&user_mobileno.equals(db.getMasterDetails().getUsername())) {
            Calendar calender = Calendar.getInstance();
            if (AppPreference.getOfflineLogged(this) == 0) {
                if (AppPreference.getLoginAttempt(this) < 6) {
                    Intent call = new Intent(this, SliderScreen.class);
                    call.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    AppPreference.setLoginAttempt(this, 0);
                    AppPreference.setTwoMin_Login_Date(this, 0);
                    AppPreference.setOfflineLogged(this, 1);
                    AppPreference.setAuthToken(this, "00");
                    startActivity(call);
                    finish();
                } else {
                    showAlert(getResources().getString(R.string.reached_wrong_password_limit));
                }
            }else {
                if (calender.getTimeInMillis() - AppPreference.getLoginTime(this) < 1000 * 60 * 60 * 72) {
                    if (AppPreference.getLoginAttempt(this) < 6) {
                        Intent call = new Intent(this, SliderScreen.class);
                        call.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        AppPreference.setLoginAttempt(this, 0);
                        AppPreference.setTwoMin_Login_Date(this, 0);
                        AppPreference.setAuthToken(this, "00");
                        startActivity(call);
                        finish();
                    } else {
                        showAlert(getResources().getString(R.string.reached_wrong_password_limit));
                    }
                } else {
                    showToast(getString(R.string.offline_msg));
                }
            }
        } else {
            if (5 - AppPreference.getLoginAttempt(this) > 0) {
                if (5 - AppPreference.getLoginAttempt(this) == 2) {
                    showAlert(getResources().getString(R.string.two_attempt_left));
                    if (AppPreference.getTwoMin_login_Date(this) == 0) {
                        AppPreference.setTwoMin_Login_Date(this, getDate());
                        int login = AppPreference.getLoginAttempt(this);
                        AppPreference.setLoginAttempt(this, ++login);
                    }
                } else {
                    if (getDate() - AppPreference.getTwoMin_login_Date(this) < 1000 * 60 * 2) {
                        showAlert(getResources().getString(R.string.two_attempt_left));
                        return;
                    } else {
                        showToast(getString(R.string.pwd_check) + "\n" + getResources().getString(R.string.attempt_left) + " " + String.valueOf(5 - AppPreference.getLoginAttempt(this)));
                        int login = AppPreference.getLoginAttempt(this);
                        AppPreference.setLoginAttempt(this, ++login);
                        AppPreference.setTwoMin_Login_Date(this, 0);
                    }
                }
            } else {
                showAlert(getResources().getString(R.string.reached_wrong_password_limit));
            }
        }
        AppPreference.setInternetOn_at_login(this, false);
    }


    public void showAlertcustom(String message) {
        final Dialog dialog = new Dialog(this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.show_message_registration);
        CustomTextView userInput = (CustomTextView) dialog.findViewById(R.id.dialog_editext);
        CustomTextView tvOK = (CustomTextView) dialog.findViewById(R.id.tv_ok);
        CustomTextView tv_cancel = (CustomTextView) dialog.findViewById(R.id.tv_cancel);
        tv_cancel.setVisibility(View.VISIBLE);
        tvOK.setText(R.string.goto_setting);
        tv_cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });


        userInput.setText(message);
        userInput.setTypeface(getFont(this), Typeface.BOLD);
        dialog.setCancelable(true);
        tvOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);

            }
        });
        dialog.show();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}
