package mm.dummymerchant.sk.merchantapp;

        import android.accounts.Account;
        import android.accounts.AccountManager;
        import android.annotation.SuppressLint;
        import android.annotation.TargetApi;
        import android.app.AlertDialog;
        import android.app.Dialog;
        import android.content.Context;
        import android.content.DialogInterface;
        import android.content.Intent;
        import android.content.SharedPreferences;
        import android.database.Cursor;
        import android.graphics.Bitmap;
        import android.graphics.BitmapFactory;
        import android.graphics.Color;
        import android.graphics.PixelFormat;
        import android.graphics.Typeface;
        import android.graphics.drawable.BitmapDrawable;
        import android.net.Uri;
        import android.os.Build;
        import android.os.Bundle;
        import android.os.CountDownTimer;
        import android.os.Handler;
        import android.provider.MediaStore;
        import android.support.v4.widget.DrawerLayout;

        import android.text.Editable;
        import android.text.Html;
        import android.text.Spanned;
        import android.text.TextWatcher;
        import android.util.Base64;
        import android.util.Log;

        import android.view.ActionMode;
        import android.view.Gravity;
        import android.view.LayoutInflater;
        import android.view.Menu;
        import android.view.MenuItem;
        import android.view.MotionEvent;
        import android.view.View;
        import android.view.Window;
        import android.view.WindowManager;
        import android.view.animation.AlphaAnimation;
        import android.view.animation.Animation;
        import android.view.animation.AnimationUtils;
        import android.view.animation.LinearInterpolator;
        import android.widget.AdapterView;
        import android.widget.ArrayAdapter;
        import android.widget.Button;
        import android.widget.CheckBox;
        import android.widget.CompoundButton;
        import android.widget.DatePicker;
        import android.widget.ImageView;
        import android.widget.LinearLayout;
        import android.widget.ListView;
        import android.widget.RadioButton;
        import android.widget.RadioGroup;
        import android.widget.RelativeLayout;
        import android.widget.ScrollView;
        import android.widget.TableLayout;
        import android.widget.TextView;

        import com.github.gcacace.signaturepad.views.SignaturePad;
        import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

        import org.json.JSONObject;

        import java.io.ByteArrayOutputStream;
        import java.io.File;
        import java.text.SimpleDateFormat;
        import java.util.ArrayList;
        import java.util.Calendar;
        import java.util.Locale;
        import java.util.Timer;
        import java.util.TimerTask;

        import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
        import mm.dummymerchant.sk.merchantapp.Utils.Constant;
        import mm.dummymerchant.sk.merchantapp.Utils.GPSLocationResponse;
        import mm.dummymerchant.sk.merchantapp.Utils.Utils;
        import mm.dummymerchant.sk.merchantapp.adapter.SliderListAdapter;
        import mm.dummymerchant.sk.merchantapp.customView.CustomEdittext;
        import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
        import mm.dummymerchant.sk.merchantapp.db.DBHelper;
        import mm.dummymerchant.sk.merchantapp.httpConnection.HttpNewRequest;
        import mm.dummymerchant.sk.merchantapp.model.BusinessCategoryModel;
        import mm.dummymerchant.sk.merchantapp.model.CityList_Model;
        import mm.dummymerchant.sk.merchantapp.model.GPS_Location;
        import mm.dummymerchant.sk.merchantapp.model.NetworkOperatorModel;
        import mm.dummymerchant.sk.merchantapp.model.Profile;
        import mm.dummymerchant.sk.merchantapp.model.TownShip_Model;
        import pl.droidsonroids.gif.GifImageView;


public class NewRegistrationActivity1 extends BaseActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener, View.OnTouchListener, AdapterView.OnItemClickListener, View.OnFocusChangeListener, RadioGroup.OnCheckedChangeListener, SignaturePad.OnSignedListener, GPSLocationResponse, HttpNewRequest.LoginApiListener {
    private static String mPrefixName = "";
    private static CustomEdittext mCity;
    private static SignaturePad mSignaturePad;
    private static String mMalePrefixData[];
    private static String mFemalePrefixData[];
    private static CustomEdittext muserNameEdit;
    private static CustomTextView mDateOfBirth;
    private boolean isWindowOpen = false;
    private LinearLayout mMaleFemaleRegfix, mFemaleRegfixLinear, mLinearUserName, mLinearLyoutd, llFatherName, mLinearBusinessCategory, mLinearPredefineNo;
    private CustomTextView mMale, mFemale, mPrefixNameText, mClear, mAddressClear, mOtherIdPrefix, mNrcClear, mCarClear, mFatherNamePrefix;

    private DBHelper db;
    private String cityCode = "";
    private String identityType = "";
    private CheckBox chIAccept;
    private CustomEdittext mMobileNo, mNRCEdittext, mOtherId, mFatherName, mConfirmRecommendedBy, mConfirmPredefineNo, mEmail, mFacebook,
            mNoOrRoadorWard, mPredefinedNumber, mRecommendedBy, mPassword, mConfirmPassword, mOtherIdEdit;
    private ArrayList<String> list = new ArrayList<>();
    private ArrayList<String> listOfEmoji = new ArrayList<>();
    private ArrayList<CityList_Model> mMainDvisisionListNew = new ArrayList<>();
    private ArrayList<TownShip_Model> mTownShipModelListNew = new ArrayList<>();
    private ArrayList<String> townShipList = new ArrayList<>();
    private CustomTextView mDivision, mState, mTownship;

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private Button btnBack, btnGoTo;
    private CustomTextView tvHeader;
    private String type = "";
    private boolean isOpened = false;
    private CheckBox cbTerm;
    private Animation animation;

    private String mobileNo;
    private ImageView mContactPicker, mContactPickerRecom, mUserPic;
    private MyTextWatcherForRecommended myTextWatcherForRecommended;
    private CustomTextView mBusinessCategory, mBusinessSubCategory, mBusinessClear, mDivisionIDText, mRecommendedClear;
    private BusinessCategoryModel businessCategoryModel;
    private CustomEdittext mAgentCode, mBusinessName, mEtAdressType, mRecommdedByDisplayName, mPredefineNoDisplayName, mStateProvice, mCityArea, mStreetNo, mPostalCode,merchant_mobileno;
    private RadioGroup mAddressType;
    private RadioButton mHome, mBusinessAccount, mPersonalAccount, mBusinessAccount1;
    private CustomTextView mTapHere, mName;
    private CustomEdittext mCountry;
    private final int[] colorsCode = {Color.BLACK, Color.BLUE, Color.CYAN, Color.GREEN, Color.GRAY, Color.RED, Color.YELLOW, Color.WHITE};
    private LinearLayout llAccept, llTerms, llTakePhoto, llAgentCode, llAgentCodeChild;
    private boolean isSigned = false, isResetClicked = false, isPredefineTextChanged = false, isResetClickedRecommended = false,
            isResetClickedPassword = false, isRegistered = false;
    private GifImageView mSignatureDemo;
    private CustomTextView tv, btnSubmit;

    private View windwoView;
    private ScrollView llNotificationAnim;
    private int mCategoryPos = -1, mSubCategoryPos = -1;
    private Profile profile, preProfile;
    private int[] listOfCategoryImage = null;
    private String latitude = "0.0", longitude = "0.0";
    private boolean gender = false;

    private GifImageView gif;
    private boolean isBack = false;
    private int isBackCount = 0, isMySelf = -1, isProfileUpdate = -1;
    private SharedPreferences sharedPref;
    private RelativeLayout relativeSlider, rlSignatureReset, rlSignatureBox, llPassword;
    private WindowManager wm;
    private LinearLayout llNotificationAnimPink;
    private CustomTextView tvCountryCodePredefine, tvCountryCodeRecommended, mCountryCodePredefine, mCountryCodeRecommended;
    private ImageView mCountryImagePredefine, mCountryImageRecommended, mSignatureImage, mCountryImage;
    private LinearLayout llCountryCodePredefine, llCountryCodeRecommended;
    private static int flag = -1, flagPredefine = -1, flagRecommended = -1;
    private String countryCode, countryName, countryCodeRecommended, countryCodePredefine, countryNameRecommended, countryNamePredefine;

    private CustomTextView tvOR, imgBusinessSubcategory, imgBusinessName1, imgBusinessName2, mCountryCodePredefineConfirm, mCountryCodeRecommendedConfirm;
    private LinearLayout llBusinessSubcategory, llBusinessName, llRecommendedByAll, llPredefineConfirm, llRecommendedConfirm,
            llAddressMyanmar, llAddressDefault, llReadyToRegister;

    private boolean isShown = true, isLanguageChanged = false, isEdit = false;
    private LinearLayout llNonMandatory, llIDProof;
    private CheckBox cbNonMandatory, mNoID;
    private String mDivisionId = "";
    private String cellId = "";
    private boolean isRecommendedNoEditable = false, isPredefineNoEditable = false;
    private boolean isBusinessAccount=false;
    private CustomTextView tvPass;
    private int count=0;
    private static NetworkOperatorModel model;

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        db = new DBHelper(this);
        getDefaultEmail();
        getDefaulFacebook();
        wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        setContentView(R.layout.activity_new_registration);
        sharedPref = getSharedPreferences(AppPreference.APP_PREF_REGISTER, 0);
        setRegistrationBackground();
        mMalePrefixData = getResources().getStringArray(R.array.male_prefix);
        mFemalePrefixData = getResources().getStringArray(R.array.female_prefix);
        init();
        callNoPasting();
        backFinish();

        cellId = getLACAndCID(this);
        String mobileNumber = Utils.getUniversalFormattedMobileNOLogin(this, AppPreference.getVerifyNumber(getApplicationContext()));
        AppPreference.setRegsiterNumber(this, mobileNumber);
        ArrayList<Profile> listOfModel = db.getProfileData();
        if (listOfModel.size() > 0) {
            preProfile = listOfModel.get(0);
        }
        isProfileUpdate = getIntent().getIntExtra("IS_FROM_SETTINGS", -1);
        if (isProfileUpdate == 1) {
            merchant_mobileno.setEnabled(false);
            mMobileNo.setEnabled(false);
            disableProfile();
            if (listOfModel.size() > 0) {
                Profile viewProfile = listOfModel.get(0);
                viewProfile.setProfilePic("");
                viewProfile.setsignature("");
                viewProfile.setPassword("");
                viewProfile.setAgentAuthCode("");

                setDataOnPreProfileData(viewProfile);
                buttonLogout.setVisibility(View.VISIBLE);
                llNonMandatory.setVisibility(View.GONE);
                //   llAgentCode.setVisibility(View.GONE);
                mNoID.setVisibility(View.GONE);
                mNoID.setChecked(false);
            }
        } else if (preProfile != null) {
            if (preProfile.getMobileNumber().equalsIgnoreCase(mobileNumber))
                setDataOnPreProfileData(preProfile);
            else {
                sharedPref.edit().clear().apply();
                db.deleteProfile();
            }
            AppPreference.setAuthToken(this, "XX");
        } else if (isProfileUpdate != 1) {
            AppPreference.setAuthToken(this, "XX");
        }

      /* if(!Utils.isReallyConnectedToInternet(this)){
            Utils.showCustomToastMsg(this, R.string.async_task_no_internet, false);
        }else if(!Utils.isEmpty(cellId)){
            new CallingApIS(this, this, 0).callGPSFromCellID(mobileNumber, cellId);
        }*/
        setOnGPSLocationListener(this);
        buttonAnimation();

        isReadytoRegister();

    }

    @Override
    protected void onResume() {
        super.onResume();
        mNRCEdittext.clearFocus();
        if (isWindowOpen)
            cancelNotification();

    }

    private void backFinish() {
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                finish();
            }
        });
    }

    private void init() {

        String mccAndmnc = Utils.getSimOperater(this);
        if (mccAndmnc != null) {
            if (!mccAndmnc.equals("")) {
                model = new NetworkOperatorModel().getNetworkOperatorModel(this, mccAndmnc);
            }
        }

        tvPass = (CustomTextView) findViewById(R.id.tv_pass);
        tvPass.setSelected(true);

        //    CustomTextView tvRecommended = (CustomTextView) findViewById(R.id.tv_recommended);
        //     tvRecommended.setSelected(true);

        CustomTextView enterName = (CustomTextView) findViewById(R.id.registration_page_title1);
        enterName.setTypeface(Utils.getFont(this), Typeface.BOLD);
        CustomTextView signText = (CustomTextView) findViewById(R.id.sign_text);
        signText.setTypeface(Utils.getFont(this), Typeface.BOLD);

      /*  llAgentCode = (LinearLayout) findViewById(R.id.ll_agent_code);
        llAgentCodeChild=(LinearLayout) findViewById(R.id.ll_agent_code_child);
        cbAgentCode = (CheckBox) findViewById(R.id.cb_agent_code);
        cbAgentCode1 = (CheckBox) findViewById(R.id.cb_agent_code1);
        mAgentCode = (CustomEdittext) findViewById(R.id.agent_code);*/
        merchant_mobileno = (CustomEdittext) findViewById(R.id.merchant_mobileNo);
        mLinearPredefineNo = (LinearLayout) findViewById(R.id.ll_car_bike);
        mLinearBusinessCategory = (LinearLayout) findViewById(R.id.ll_bussiness_category);
        mDivisionIDText = (CustomTextView) findViewById(R.id.division_id);
        mFatherNamePrefix = (CustomTextView) findViewById(R.id.registration_page_edittext_fatherName_prefix);
        llFatherName = (LinearLayout) findViewById(R.id.ll_father_name);
        mMaleFemaleRegfix = (LinearLayout) findViewById(R.id.registration_page_linear_layout_add_name_prefix);
        mFemaleRegfixLinear = (LinearLayout) findViewById(R.id.registration_page_linear_layout_add_name__femail);
        mLinearUserName = (LinearLayout) findViewById(R.id.registration_page_linear_username);
        mLinearLyoutd = (LinearLayout) findViewById(R.id.registration_page_linear_Id);

        mCarClear = (CustomTextView) findViewById(R.id.registration_page__car_clear);
        mRecommendedClear = (CustomTextView) findViewById(R.id.registration_page__car_clear1);

        mMale = (CustomTextView) findViewById(R.id.registration_page_textview_text_gender_male);
        mPrefixNameText = (CustomTextView) findViewById(R.id.registration_page_textview_text_gender_prefix_name);
        mFemale = (CustomTextView) findViewById(R.id.registration_page_textview_text_gender_female);
        muserNameEdit = (CustomEdittext) findViewById(R.id.registration_page_textview_text_userName_input);

        mDivision = (CustomTextView) findViewById(R.id.registration_spinner_dvision);
        mState = (CustomTextView) findViewById(R.id.registration_spinner_state);
        mTownship = (CustomTextView) findViewById(R.id.registration_spinner_township);
        mNRCEdittext = (CustomEdittext) findViewById(R.id.registration_page_edittext_NRC);
        mMobileNo = (CustomEdittext) findViewById(R.id.registration_page_edittext_mobileNo);
        mOtherId = (CustomEdittext) findViewById(R.id.registration_page_edittext_otherIdentity);
        mFatherName = (CustomEdittext) findViewById(R.id.registration_page_edittext_fatherName);
        mOtherIdEdit = (CustomEdittext) findViewById(R.id.registration_page_textview_text_otherId_input);
        mCity = (CustomEdittext) findViewById(R.id.registration_page_edittext_city);
        mNoOrRoadorWard = (CustomEdittext) findViewById(R.id.registration_page_road_ward_none);

        mPredefinedNumber = (CustomEdittext) findViewById(R.id.registration_page_edittext_redefineNo);
        mRecommendedBy = (CustomEdittext) findViewById(R.id.registration_page_edittext_recommendedby);
        mPassword = (CustomEdittext) findViewById(R.id.registration_page_edittext_password);
        mConfirmPassword = (CustomEdittext) findViewById(R.id.registration_page_edittext_confirm_password);
        CustomTextView mPasswordClear= (CustomTextView) findViewById(R.id.registration_page_pass_clear);
        llPassword=(RelativeLayout) findViewById(R.id.ll_pass);

        mClear = (CustomTextView) findViewById(R.id.registration_page_clear);
        mNrcClear = (CustomTextView) findViewById(R.id.registration_page__nrc_clear);
        mAddressClear = (CustomTextView) findViewById(R.id.registration_page__address_clear);
        mOtherIdPrefix = (CustomTextView) findViewById(R.id.registration_page_textview_text_otherId_prefix);
        cbTerm = (CheckBox) findViewById(R.id.cb_terms);
        btnSubmit = (CustomTextView) findViewById(R.id.btn_submit);
        mNoID = (CheckBox) findViewById(R.id.no_id);

        chIAccept = (CheckBox) findViewById(R.id.registration_page_checkbox_iaccept);
        mDateOfBirth = (CustomTextView) findViewById(R.id.registration_date);

        mContactPicker = (ImageView) findViewById(R.id.iv_contact_picker);
        mContactPickerRecom = (ImageView) findViewById(R.id.iv_contact_picker_recom);
        mConfirmPredefineNo = (CustomEdittext) findViewById(R.id.predefine_no_confirmation);
        mConfirmRecommendedBy = (CustomEdittext) findViewById(R.id.recommended_by_confirmation);
        mBusinessCategory = (CustomTextView) findViewById(R.id.bussiness_category);
        mBusinessSubCategory = (CustomTextView) findViewById(R.id.bussiness_sub_category);
        mBusinessName = (CustomEdittext) findViewById(R.id.business_name);
        mBusinessClear = (CustomTextView) findViewById(R.id.business_clear);

        mCountryImagePredefine = (ImageView) findViewById(R.id.img_tv_country_code_predefine);
        mCountryCodePredefine = (CustomTextView) findViewById(R.id.select_country_code_predefine);
        llCountryCodePredefine = (LinearLayout) findViewById(R.id.ll_select_country_code_predefine);
        tvCountryCodePredefine = (CustomTextView) findViewById(R.id.tv_country_code_predefine);
        tvCountryCodePredefine.setTypeface(null, Typeface.BOLD);

        mCountryImageRecommended = (ImageView) findViewById(R.id.img_tv_country_code_recommended);
        mCountryCodeRecommended = (CustomTextView) findViewById(R.id.select_country_code_recommended);
        llCountryCodeRecommended = (LinearLayout) findViewById(R.id.ll_select_country_code_recommended);
        tvCountryCodeRecommended = (CustomTextView) findViewById(R.id.tv_country_code_recommended);
        tvCountryCodeRecommended.setTypeface(null, Typeface.BOLD);
        llRecommendedByAll = (LinearLayout) findViewById(R.id.ll_recommended_by_whole);

        mRecommdedByDisplayName = (CustomEdittext) findViewById(R.id.recommended_name);
        mPredefineNoDisplayName = (CustomEdittext) findViewById(R.id.predefine_name);

        mAddressType = (RadioGroup) findViewById(R.id.address_type);
        mHome = (RadioButton) findViewById(R.id.home);
        RadioButton mOffice = (RadioButton) findViewById(R.id.office);
        RadioButton mOther = (RadioButton) findViewById(R.id.other);
        RadioGroup mAccountType = (RadioGroup) findViewById(R.id.account_type);
        mBusinessAccount = (RadioButton) findViewById(R.id.business_account);
        mPersonalAccount = (RadioButton) findViewById(R.id.personal_account);
        mBusinessAccount1 = (RadioButton) findViewById(R.id.business_account1);

        mEtAdressType = (CustomEdittext) findViewById(R.id.et_other);
        mSignaturePad = (SignaturePad) findViewById(R.id.signature_pad);
        mSignatureDemo = (GifImageView) findViewById(R.id.gif_sign_demo);
        mSignatureImage = (ImageView) findViewById(R.id.signature_image);
        mTapHere = (CustomTextView) findViewById(R.id.tap_here_text);
        CustomTextView mSignReset = (CustomTextView) findViewById(R.id.sign_reset);
        llTerms = (LinearLayout) findViewById(R.id.ll_terms);
        llAccept = (LinearLayout) findViewById(R.id.ll_accept);
        llTakePhoto = (LinearLayout) findViewById(R.id.take_your_pic);
        mUserPic = (ImageView) findViewById(R.id.user_photo);
        ScrollView scrollView = (ScrollView) findViewById(R.id.scrollView);

        rlSignatureReset = (RelativeLayout) findViewById(R.id.rl_signature_reset);
        rlSignatureBox = (RelativeLayout) findViewById(R.id.rl_signature_box);

        mCountryCodePredefineConfirm = (CustomTextView) findViewById(R.id.tv_country_code_predefine_confirm);
        mCountryCodeRecommendedConfirm = (CustomTextView) findViewById(R.id.tv_country_code_recommended_confirm);
        llPredefineConfirm = (LinearLayout) findViewById(R.id.ll_predefine_no_confirm);
        llRecommendedConfirm = (LinearLayout) findViewById(R.id.ll_recommended_by_confirm);

        ImageView mcountryImage = (ImageView) findViewById(R.id.img_tv_country_code);
        CustomTextView mcountryCode = (CustomTextView) findViewById(R.id.tv_country_code);
        imgBusinessSubcategory = (CustomTextView) findViewById(R.id.img_bussiness_sub_category);
        llBusinessSubcategory = (LinearLayout) findViewById(R.id.ll_bussiness_sub_category);
        imgBusinessName1 = (CustomTextView) findViewById(R.id.img_bussiness_name1);
        imgBusinessName2 = (CustomTextView) findViewById(R.id.img_bussiness_name2);
        llBusinessName = (LinearLayout) findViewById(R.id.ll_bussiness_name);
        llAddressMyanmar = (LinearLayout) findViewById(R.id.address_myanmar);
        llAddressDefault = (LinearLayout) findViewById(R.id.address_default);
        mStateProvice = (CustomEdittext) findViewById(R.id.state_province);
        mCityArea = (CustomEdittext) findViewById(R.id.city_area);
        mStreetNo = (CustomEdittext) findViewById(R.id.street_no);
        mPostalCode = (CustomEdittext) findViewById(R.id.postal_code);

        scrollView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (isWindowOpen)
                    cancelNotification();
                return false;
            }
        });

        countryCode = AppPreference.getCountryCodeLogin(getApplicationContext());
        countryName = AppPreference.getCountryNameLogin(getApplicationContext());
        countryCodePredefine = countryCodeRecommended = countryCode;
        countryNamePredefine = countryNameRecommended = countryName;
        mCountryCodePredefineConfirm.setText("(" + countryCode + ")");
        mCountryCodePredefine.setText(countryName + " (" + countryCode + ")");
        mCountryCodeRecommendedConfirm.setText("(" + countryCode + ")");
        mCountryCodeRecommended.setText(countryName + " (" + countryCode + ")");
        tvCountryCodeRecommended.setText("(" + countryCode + ")");
        tvCountryCodePredefine.setText("(" + countryCode + ")");
        mcountryCode.setTypeface(Utils.getFont(this), Typeface.BOLD);
        mcountryCode.setText("(" + AppPreference.getCountryCodeLogin(getApplicationContext()) + ")");
        mMobileNo.setTypeface(Utils.getFont(this), Typeface.BOLD);
        if (countryCode.equalsIgnoreCase("+95")) {
            Log.e("setNumberCountryCode","..."+countryCode);
            mMobileNo.setText(getNumberWithGreyZero(AppPreference.getVerifyNumber(getApplicationContext()), true));
        } else {
            mMobileNo.setText(AppPreference.getVerifyNumber(getApplicationContext()));
        }
        try {
            if (AppPreference.getCountryImageIDLogin(getApplicationContext()) != -1) {
                flag = AppPreference.getCountryImageIDLogin(getApplicationContext());
                flagPredefine = flagRecommended = flag;
                mcountryImage.setImageResource(flag);
                mCountryImagePredefine.setImageResource(flag);
                mCountryImageRecommended.setImageResource(flag);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        llNonMandatory = (LinearLayout) findViewById(R.id.ll_non_mandatory);
        llIDProof = (LinearLayout) findViewById(R.id.ll_id_proof);
        cbNonMandatory = (CheckBox) findViewById(R.id.cb_non_mandatory);
        llReadyToRegister = (LinearLayout) findViewById(R.id.ready_register);
        tvOR = (CustomTextView) findViewById(R.id.text_or);
        mName = (CustomTextView) findViewById(R.id.name);
        mEmail = (CustomEdittext) findViewById(R.id.registration_page_edittext_email);
        mFacebook = (CustomEdittext) findViewById(R.id.registration_page_edittext_facebook);
        mCountry = (CustomEdittext) findViewById(R.id.country);
        mCountryImage = (ImageView) findViewById(R.id.img_country);

        mName.setTypeface(Utils.getFont(this), Typeface.BOLD);
        muserNameEdit.setTypeface(Utils.getFont(this), Typeface.BOLD);
        mPrefixNameText.setTypeface(Utils.getFont(this), Typeface.BOLD);
        mFatherNamePrefix.setTypeface(Utils.getFont(this), Typeface.BOLD);
        mName.setOnClickListener(this);

        mPasswordClear.setOnClickListener(this);
        llCountryCodePredefine.setOnClickListener(this);
        llCountryCodeRecommended.setOnClickListener(this);
        mContactPickerRecom.setOnClickListener(this);
        mContactPicker.setOnClickListener(this);
        mMale.setOnClickListener(this);
        mOtherId.setOnClickListener(this);
        mFemale.setOnClickListener(this);
        mClear.setOnClickListener(this);
        mNrcClear.setOnClickListener(this);
        mAddressClear.setOnClickListener(this);
        mCarClear.setOnClickListener(this);
        mRecommendedClear.setOnClickListener(this);
        mBusinessSubCategory.setOnClickListener(this);
        mBusinessCategory.setOnClickListener(this);
        mBusinessClear.setOnClickListener(this);
        mSignReset.setOnClickListener(this);
        mSignatureDemo.setOnClickListener(this);
        llTakePhoto.setOnClickListener(this);

        nrcOntextCnageLisner();
        mTownship.setVisibility(View.GONE);
        mFatherName.setOnFocusChangeListener(this);
        mRecommendedBy.setOnFocusChangeListener(this);
        mPredefinedNumber.setOnFocusChangeListener(this);
        mConfirmRecommendedBy.setOnFocusChangeListener(this);
        mConfirmPredefineNo.setOnFocusChangeListener(this);
        mNRCEdittext.setOnFocusChangeListener(this);
        mOtherIdEdit.setOnFocusChangeListener(this);
        mPredefinedNumber.setOnFocusChangeListener(this);
        muserNameEdit.setOnFocusChangeListener(this);
        mPassword.setOnFocusChangeListener(this);
        mConfirmPassword.setOnFocusChangeListener(this);
        mBusinessName.setOnFocusChangeListener(this);
        mCity.setOnFocusChangeListener(this);
        mNoOrRoadorWard.setOnFocusChangeListener(this);

        setDateListener();
        mMale.setSelected(true);
        mDivision.setSelected(true);
        mState.setSelected(true);
        mTownship.setSelected(true);
        mFemale.setSelected(true);
        mBusinessCategory.setSelected(true);
        mBusinessSubCategory.setSelected(true);
        mBusinessAccount.setSelected(true);
        mBusinessAccount1.setSelected(true);
        mPersonalAccount.setSelected(true);

        mOtherId.setClickable(true);
        mOtherId.setFocusable(false);
        setOnCheckedChangeForAccept();

        btnSubmit.setOnClickListener(this);
        mRecommendedBy.setOnClickListener(this);
        mMobileNo.setOnClickListener(this);
        mDivision.setOnClickListener(this);
        mState.setOnClickListener(this);
        mTownship.setOnClickListener(this);
        mAccountType.setOnCheckedChangeListener(this);
        llTerms.setOnClickListener(this);
        llAccept.setOnClickListener(this);

        btnBack = (Button) findViewById(R.id.btn_back);
        btnGoTo = (Button) findViewById(R.id.btn_goto);
        tvHeader = (CustomTextView) findViewById(R.id.tv_header);
        relativeSlider = (RelativeLayout) findViewById(R.id.relative_slider);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        mDrawerList.setOnItemClickListener(this);
        btnBack.setOnClickListener(this);
        btnGoTo.setOnClickListener(this);
        myTextWatcherForRecommended = new MyTextWatcherForRecommended();
        mPredefinedNumber.addTextChangedListener(new MyTextWatcherForPredefineNo());

        mConfirmPassword.addTextChangedListener(new MyTextWatcherForPassword());
        mConfirmPredefineNo.addTextChangedListener(new MyTextWatcherForConfirmPredefine());
        mConfirmRecommendedBy.addTextChangedListener(new MyTextWatcherForConfirmRecommended());
        mPassword.addTextChangedListener(new MyTextWatcherForOPassword());
        muserNameEdit.addTextChangedListener(new MyTextWatcherForName());
        buttonLogout.setVisibility(View.GONE);
        btnSubmit.setTypeface(Utils.getFont(this), Typeface.BOLD);
        chIAccept.setTypeface(Utils.getFont(this));
        mHome.setTypeface(Utils.getFont(this));
        mOffice.setTypeface(Utils.getFont(this));
        mOther.setTypeface(Utils.getFont(this));

        mBusinessAccount1.setTypeface(Utils.getFont(this), Typeface.BOLD);
        mBusinessAccount.setTypeface(Utils.getFont(this), Typeface.BOLD);
        mPersonalAccount.setTypeface(Utils.getFont(this), Typeface.BOLD);
        mCountryCodePredefineConfirm.setTypeface(Utils.getFont(this), Typeface.BOLD);
        mCountryCodeRecommendedConfirm.setTypeface(Utils.getFont(this), Typeface.BOLD);
        mNoID.setTypeface(Utils.getFont(this), Typeface.BOLD);
        cbNonMandatory.setTypeface(Utils.getFont(this), Typeface.BOLD);
        /*cbAgentCode.setTypeface(Utils.getFont(this));
        cbAgentCode1.setTypeface(Utils.getFont(this));*/

        mSignaturePad.setOnSignedListener(this);
        businessCategoryModel = new BusinessCategoryModel();
        mDateOfBirth.setTypeface(Utils.getFont(this), Typeface.BOLD);
        mFatherName.setTypeface(Utils.getFont(this), Typeface.BOLD);
        mPredefinedNumber.setTypeface(Utils.getFont(this));
        mConfirmPredefineNo.setTypeface(Utils.getFont(this));
        mPassword.setTypeface(Utils.getFont(this), Typeface.BOLD);
        mConfirmPassword.setTypeface(Utils.getFont(this), Typeface.BOLD);
        mDateOfBirth.setFocusable(true);
        mRecommendedBy.setFocusable(true);

        mobileNo = mMobileNo.getText().toString();
        mobileNo = Utils.getUniversalFormattedMobileNOLogin(this, mobileNo);
        mEmail.setText(AppPreference.getDefaultEmail(this));
        mFacebook.setText(AppPreference.getFacebookID(this));

        mBusinessCategory.setVisibility(View.GONE);
        llBusinessSubcategory.setVisibility(View.GONE);
        llBusinessName.setVisibility(View.GONE);
        imgBusinessSubcategory.setText(null);
        imgBusinessName2.setText(null);
        getlatlong();
        setMaxLength(mPassword, 20);
        setMaxLength(mConfirmPassword, 20);
        setMaxLength(muserNameEdit, 60);
        setMaxLength(mFatherName, 60);
        setMaxLength(mPredefinedNumber, 17);
        setMaxLength(mConfirmPredefineNo, 17);
        setMaxLength(mRecommendedBy, 17);
        setMaxLength(mConfirmRecommendedBy, 17);
        setMaxLength(mCity, 60);
        setMaxLength(mNoOrRoadorWard, 60);
        setMaxLength(mEtAdressType, 60);
        setEditTextFilterForNRC(mNRCEdittext, 30);
        setEditTextFilterForNRC(mOtherIdEdit, 30);
        setMaxLength(mBusinessName, 40);
        setMaxLength(mStateProvice, 50);
        setMaxLength(mCityArea, 50);
        setMaxLength(mPostalCode, 50);
        setMaxLength(mStreetNo, 70);

        mHome.setVisibility(View.GONE);

        if (!model.getCountryCode().equalsIgnoreCase("+95")) {
            merchant_mobileno.setText(null);
        } else {
            merchant_mobileno.setText(Utils.getMyanmarNumber());
        }

        merchant_mobileno.setSelection(merchant_mobileno.length());
        merchant_mobileno.addTextChangedListener(new MyTextWatcherForMobileNo());

        callWebService();
    }

    private void callWebService() {
        buttonLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard();
                if (isEdit) {
                    enableProfile();
                } else {
                    submitForRegistration();
                }
            }
        });
    }

    private void nrcOntextCnageLisner() {
        mNRCEdittext.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.toString().equals("") && !isResetClicked) {
                    mOtherId.setVisibility(View.VISIBLE);

                } else {
                    mOtherId.setVisibility(View.GONE);
                }
                if (s.toString().length() > 2) {
                    cancelNotification();
                } else if (!isWindowOpen && !isResetClicked) {
                    setNotification(R.string.nrc_no_text, "NONE");
                }
                if (isResetClicked)
                    isResetClicked = false;

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private String getDivisionAndState() {
        SharedPreferences.Editor prefsEditor = sharedPref.edit();
        String mStateAndDividionName = "";
        if (!mDivision.getText().toString().equalsIgnoreCase(getResources().getString(R.string.select_div_text))) {
            mStateAndDividionName = mDivision.getText().toString();
            prefsEditor.putInt("IS_DIVISION", 1);
            prefsEditor.apply();
            return mStateAndDividionName;
        }
        if (!mState.getText().toString().equalsIgnoreCase(getResources().getString(R.string.select_state_text))) {
            mStateAndDividionName = mState.getText().toString();
            prefsEditor.putInt("IS_DIVISION", 0);
            prefsEditor.apply();
            return mStateAndDividionName;
        }

        prefsEditor.putInt("IS_DIVISION", -1);
        prefsEditor.apply();
        return mStateAndDividionName;
    }

    @Override
    public <T> void response(int resultCode, T data) {

    }


    @Override
    public void onClick(View v) {

        if (isWindowOpen)
            cancelNotification();

        switch (v.getId()) {
            case R.id.cb_non_mandatory:

                break;
            case R.id.name:
                mName.setVisibility(View.GONE);
                break;
            case R.id.registration_page_textview_text_gender_male:
                gender = true;

                mMale.setVisibility(View.GONE);
                mFemale.setVisibility(View.GONE);
                mLinearUserName = (LinearLayout) findViewById(R.id.registration_page_linear_username);
                mMaleFemaleRegfix.setVisibility(View.VISIBLE);
                mFemaleRegfixLinear.setVisibility(View.GONE);
                addMalePrefixIntheList();

                break;
            case R.id.registration_page_textview_text_gender_female:
                gender = false;

                mMale.setVisibility(View.GONE);
                mFemale.setVisibility(View.GONE);
                mLinearUserName = (LinearLayout) findViewById(R.id.registration_page_linear_username);
                mMaleFemaleRegfix.setVisibility(View.GONE);
                mFemaleRegfixLinear.setVisibility(View.VISIBLE);
                addFeMalePrefixIntheList();

                break;

            case R.id.registration_page_clear:
                isResetClicked = true;
                mMale.setVisibility(View.VISIBLE);
                mFemale.setVisibility(View.VISIBLE);
                mMaleFemaleRegfix.setVisibility(View.GONE);
                mFemaleRegfixLinear.setVisibility(View.GONE);
                mLinearUserName.setVisibility(View.GONE);
                mPrefixName = "";
                muserNameEdit.setText("");
                muserNameEdit.clearFocus();
                mDateOfBirth.setVisibility(View.GONE);
                llFatherName.setVisibility(View.GONE);
                mDateOfBirth.setText(null);
                mFatherName.setText(null);
                //mPassword.setText(null);
                //mConfirmPassword.setVisibility(View.GONE);
                mName.setVisibility(View.VISIBLE);
                mUserPic.setImageDrawable(null);
                hideKeyboard();
                break;

            case R.id.registration_page_pass_clear:
                isResetClicked = true;
                mPassword.setText(null);
                mConfirmPassword.setVisibility(View.GONE);
                hideKeyboard();
                break;

            case R.id.registration_page__address_clear:
                hideKeyboard();
                mState.setVisibility(View.VISIBLE);
                mDivision.setVisibility(View.VISIBLE);
                mCity.setVisibility(View.GONE);
                mNoOrRoadorWard.setVisibility(View.GONE);
                mState.setClickable(true);
                mDivision.setClickable(true);
                mTownship.setVisibility(View.GONE);
                mDivision.setText(getResources().getString(R.string.select_div_text));
                mState.setText(getResources().getString(R.string.select_state_text));
                mTownship.setText(getResources().getString(R.string.select_town_text));
                mCity.setText("");
                mCity.setEnabled(false);
                mNoOrRoadorWard.setText("");
                mNoOrRoadorWard.setEnabled(false);
                mEtAdressType.setText("");
                mHome.setChecked(true);
                mDivisionIDText.setText("");
                mDivisionId = "";
                cityCode = "";

                break;

            case R.id.registration_page_edittext_otherIdentity:
                select_identtity();
                break;

            case R.id.registration_page__nrc_clear:

                isResetClicked = true;
                mNRCEdittext.setVisibility(View.VISIBLE);
                mLinearLyoutd.setVisibility(View.GONE);
                mNRCEdittext.setText("");
                mOtherIdEdit.setText("");
                mOtherIdEdit.setEnabled(true);
                mNoID.setChecked(false);
                mOtherId.setVisibility(View.VISIBLE);
                hideKeyboard();
                break;

            case R.id.registration_page__car_clear:

                isResetClicked = true;
                isResetClickedPassword = true;
                hideKeyboard();
                isOpened = false;
                mPredefinedNumber.setText(null);
                mPredefinedNumber.addTextChangedListener(new MyTextWatcherForPredefineNo());

                mPredefinedNumber.setFocusableInTouchMode(true);
                mConfirmPredefineNo.setFocusableInTouchMode(true);

                if (mPredefineNoDisplayName.getVisibility() == View.VISIBLE) {
                    mPredefineNoDisplayName.setText("");
                    mPredefineNoDisplayName.setVisibility(View.GONE);
                }

                if (isWindowOpen)
                    cancelNotification();

                llPredefineConfirm.setVisibility(View.GONE);

                mCountryCodePredefineConfirm.setText("(" + countryCode + ")");
                mCountryCodePredefine.setText(countryName + " (" + countryCode + ")");
                tvCountryCodePredefine.setText("(" + countryCode + ")");
                flagPredefine = flag;

                countryCodePredefine = countryCode;
                countryNamePredefine = countryName;
                mCountryImagePredefine.setImageResource(flag);

                break;

            case R.id.registration_page__car_clear1:

                isMySelf = -1;
                isResetClicked = true;
                isResetClickedRecommended = true;
                isResetClickedPassword = true;
                hideKeyboard();
                isOpened = false;
                mRecommendedBy.setText(null);
                mRecommendedBy.addTextChangedListener(new MyTextWatcherForRecommended());
                llRecommendedByAll.setVisibility(View.VISIBLE);
                if (llRecommendedConfirm.getVisibility() == View.VISIBLE) {
                    mConfirmRecommendedBy.setText("");
                    llRecommendedConfirm.setVisibility(View.GONE);
                }
                mConfirmPredefineNo.setFocusableInTouchMode(true);

                if (mRecommdedByDisplayName.getVisibility() == View.VISIBLE) {
                    mRecommdedByDisplayName.setText("");
                    mRecommdedByDisplayName.setVisibility(View.GONE);
                }
                mRecommendedBy.setOnClickListener(this);
                mRecommendedBy.setFocusable(false);
                if (isWindowOpen)
                    cancelNotification();
                mCountryCodeRecommendedConfirm.setText("(" + countryCode + ")");
                mCountryCodeRecommended.setText(countryName + " (" + countryCode + ")");
                tvCountryCodeRecommended.setText("(" + countryCode + ")");
                flagRecommended = flag;
                countryCodeRecommended = countryCode;
                countryNameRecommended = countryName;
                mCountryImageRecommended.setImageResource(flag);

                break;

            case R.id.registration_spinner_dvision:
                hideKeyboard();
                setSlider("DIVISION", false);
                break;
            case R.id.registration_spinner_state:
                hideKeyboard();
                setSlider("STATE", false);
                break;
            case R.id.registration_spinner_township:
                hideKeyboard();
                setSlider("TOWNSHIP", false);
                break;
            case R.id.registration_page_edittext_recommendedby:
                if (!isOpened) {
                    singleChoiceDialog();
                    hideKeyboard();
                } else
                    showKeyboard(mRecommendedBy);
                //singleChoiceDialog();
                break;
            case R.id.btn_back:
                mDrawerLayout.closeDrawer(Gravity.RIGHT);
                break;

            case R.id.btn_goto:

                if (type.equalsIgnoreCase("DIVISION"))
                    setSlider("STATE", true);
                else if (type.equalsIgnoreCase("STATE"))
                    setSlider("DIVISION", true);
                else
                    btnGoTo.setVisibility(View.GONE);
                break;

            case R.id.iv_contact_picker:
                if (isWindowOpen)
                    cancelNotification();
                Intent call = new Intent(this, NewContactPicker.class);
                startActivityForResult(call, 1);
                break;

            case R.id.iv_contact_picker_recom:
                if (isWindowOpen)
                    cancelNotification();
                Intent call1 = new Intent(this, NewContactPicker.class);
                startActivityForResult(call1, 2);

                break;

            case R.id.bussiness_category:
                hideKeyboard();
                setSlider("MAIN_CATEGORY", false);
                break;

            case R.id.bussiness_sub_category:
                hideKeyboard();
                setSlider("SUB_CATEGORY", false);
                break;

            case R.id.business_clear:
                hideKeyboard();
                llBusinessName.setVisibility(View.GONE);
                mBusinessCategory.setText(getResources().getString(R.string.business_category));
                mBusinessSubCategory.setText(getResources().getString(R.string.business_subcategory));
                llBusinessSubcategory.setVisibility(View.GONE);
                mBusinessName.setText("");

                //mBusinessName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.business_name, 0, 0, 0);
                mBusinessCategory.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow1, 0);
                mBusinessSubCategory.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow1, 0);
                mCategoryPos = -1;
                mSubCategoryPos = -1;
              /*  flagPredefine = -1;
                flagRecommended = -1;*/

                break;

            case R.id.sign_reset:
                mSignatureDemo.setVisibility(View.VISIBLE);
                mTapHere.setVisibility(View.VISIBLE);
                hideKeyboard();
                mSignatureImage.setImageBitmap(null);
                chIAccept.setChecked(false);
                cbTerm.setChecked(false);
                isSigned = false;
                break;
            case R.id.ll_terms:
                hideKeyboard();
                showTermSAndCondition(TERMS_CONDITION, false);
                cbTerm.setChecked(true);
                cbTerm.setClickable(true);
                mSignaturePad.setOnTouchListener(this);
                break;

            case R.id.ll_accept:
                hideKeyboard();
                if (chIAccept.isChecked())
                    chIAccept.setChecked(false);
                else
                    chIAccept.setChecked(true);
                break;
            case R.id.gif_sign_demo:
                hideKeyboard();
                if (isWindowOpen)
                    cancelNotification();
                Intent i = new Intent(getApplicationContext(), PotraitSignaturePad.class);
                startActivityForResult(i, 200);
                break;
            case R.id.take_your_pic:
                hideKeyboard();
                if (isWindowOpen)
                    cancelNotification();
                Intent   intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, 108);

                break;
            case R.id.registration_page_edittext_mobileNo:
                hideKeyboard();
                /*if(!isWindowOpen){
                    setNotification(R.string.phone_popup_text);
                }else{
                    cancelNotification();
                }*/
                break;

            case R.id.ll_select_country_code_predefine:
                /*if (!countryCode.equalsIgnoreCase("+95")) {
                    Intent intentPredefine = new Intent(this, SelectCountryCode.class);
                    startActivityForResult(intentPredefine, 101);
                }*/
                break;
            case R.id.ll_select_country_code_recommended:
                /*if (!countryCode.equalsIgnoreCase("+95")) {
                    Intent intentRecommended = new Intent(this, SelectCountryCode.class);
                    startActivityForResult(intentRecommended, 1001);
                }*/
                break;

            case R.id.btn_submit:
                hideKeyboard();
                if (isEdit) {
                    enableProfile();
                } else {
                    submitForRegistration();
                }
                break;
            default:
                break;

        }
    }


    private void addMalePrefixIntheList() {

        mMaleFemaleRegfix.removeAllViews();
        ImageView imageView = new ImageView(this);
        imageView.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT, 1f));
        imageView.setImageResource(R.drawable.male);
        imageView.setBackgroundResource(R.drawable.registration_gender_bg);
        imageView.setPadding(5, 5, 5, 5);
        mMaleFemaleRegfix.addView(imageView);
        // for (int i = 0; i < mMalePrefixData.length; i++) {
        for (String prefix : mMalePrefixData) {
            CustomTextView textView = new CustomTextView(this);
            textView.setText(prefix);
            textView.setTextSize(18);
            textView.setGravity(Gravity.CENTER);
            textView.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT, 1f));
            textView.setBackgroundResource(R.drawable.registration_gender_bg);
            textView.setPadding(5, 5, 5, 5);
            mMaleFemaleRegfix.addView(textView);
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CustomTextView text = (CustomTextView) v;
                    mPrefixName = text.getText().toString();
                    mLinearUserName.setVisibility(View.VISIBLE);
                    mMaleFemaleRegfix.setVisibility(View.GONE);
                    mPrefixNameText.setText(mPrefixName);
                    muserNameEdit.requestFocus();
                    mDateOfBirth.setVisibility(View.VISIBLE);
                    llFatherName.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    private void addFeMalePrefixIntheList() {

        mFemaleRegfixLinear.removeAllViews();
        ImageView imageView = new ImageView(this);
        imageView.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT, 1f));
        imageView.setImageResource(R.drawable.female);
        imageView.setBackgroundResource(R.drawable.registration_gender_bg);
        imageView.setPadding(5, 5, 5, 5);
        mFemaleRegfixLinear.addView(imageView);

        for (String prefix : mFemalePrefixData) {
            CustomTextView textView = new CustomTextView(this);
            textView.setText(prefix);
            textView.setTextSize(18);
            textView.setGravity(Gravity.CENTER);
            textView.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT, 1f));
            textView.setBackgroundResource(R.drawable.registration_gender_bg);
            textView.setPadding(5, 5, 5, 5);
            mFemaleRegfixLinear.addView(textView);
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CustomTextView text = (CustomTextView) v;
                    mPrefixName = text.getText().toString();
                    mLinearUserName.setVisibility(View.VISIBLE);
                    mFemaleRegfixLinear.setVisibility(View.GONE);
                    mPrefixNameText.setText(mPrefixName);
                    muserNameEdit.requestFocus();
                    mDateOfBirth.setVisibility(View.VISIBLE);
                    llFatherName.setVisibility(View.VISIBLE);
                }
            });
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i("entered", "entered");
      /*  try {
            if (resultCode == 102) {
                String path = data.getExtras().getString("path");
                Bitmap bitmapImage = BitmapFactory.decodeFile(path);
                int nh = (int) (bitmapImage.getHeight() * (200.0 / bitmapImage.getWidth()));
                Bitmap scaled = Bitmap.createScaledBitmap(bitmapImage, 200, nh, true);
                mUserPic.setImageBitmap(scaled);
                File f = new File(path);
                if (f.exists()) {
                    f.delete();
                }
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        /*if (resultCode == 106) {
            Intent cameraIntent = new Intent(this, AndroidSurfaceviewExample.class);

            startActivityForResult(cameraIntent, 102);
        }*/

        if (resultCode == 110) {
            showAlert(getResources().getString(R.string.error_camera_no_space));
        }

        if (requestCode == 108) {
            if (data != null) {
                try {
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    mUserPic.setImageBitmap(photo);
                }catch (Exception e){}
            }
        }

        if (requestCode == 1) {
            mPredefinedNumber.removeTextChangedListener(new MyTextWatcherForPredefineNo());
            llPredefineConfirm.setVisibility(View.GONE);
            mConfirmPredefineNo.setFocusableInTouchMode(true);
            //llRecommendedConfirm.setVisibility(View.GONE);
            if (data != null) {
                String no = data.getStringExtra("no");
                String name = data.getStringExtra("name");

                String[] countryCodeArray = getCountryCodeNameAndFlagFromNumberLogin(no);
                countryCodePredefine = countryCodeArray[0];
                countryNamePredefine = countryCodeArray[1];
                flagPredefine = Integer.parseInt(countryCodeArray[2]);
                no = countryCodeArray[3];

                if (!countryCodePredefine.equals(countryCode)) {
                    Utils.showCustomToastMsg(this, R.string.error_msg_pick_phone_home, false);
                    countryCodePredefine=countryCode;
                    return;
                }else if (countryCodePredefine.equals("+95")) {
                    if (!no.startsWith("09")) {
                        Utils.showCustomToastMsg(this, R.string.correct_predefined_no, false);

                        return;
                    }
                }
                if (AppPreference.getCountryImageIDLogin(this) != -1) {
                    mCountryImagePredefine.setImageResource(flagPredefine);
                }
                mCountryCodePredefineConfirm.setText("(" + countryCodePredefine + ")");
                mCountryCodePredefine.setText(countryNamePredefine + " (" + countryCodePredefine + ")");
                tvCountryCodePredefine.setText("(" + countryCodePredefine + ")");

                mPredefineNoDisplayName.setText(name);
                if (countryCodePredefine.equalsIgnoreCase("+95")) {
                    mPredefinedNumber.setText(getNumberWithGreyZero(no, true));
                } else {
                    mPredefinedNumber.setText(no);
                }

                llPredefineConfirm.setVisibility(View.GONE);
                mPredefinedNumber.setFocusable(false);
                mPredefinedNumber.setFocusableInTouchMode(false);
                mPredefineNoDisplayName.setVisibility(View.VISIBLE);

                return;
            } else {
                showToastPopUp(R.string.error_msg_pick_phone);
            }
        }

        if (requestCode == 2) {

            if (data != null) {
                String no = data.getStringExtra("no");
                String name = data.getStringExtra("name");

                String[] countryCodeArray = getCountryCodeNameAndFlagFromNumberLogin(no);
                countryCodeRecommended = countryCodeArray[0];
                countryNameRecommended = countryCodeArray[1];
                flagRecommended = Integer.parseInt(countryCodeArray[2]);
                no = countryCodeArray[3];

                if (!countryCodeRecommended.equals(countryCode)) {
                    Utils.showCustomToastMsg(this, R.string.error_msg_pick_phone_home, false);
                    countryCodeRecommended=countryCode;
                    return;
                }else if (countryCodeRecommended.equals("+95")) {
                    if (!no.startsWith("09")) {
                        Utils.showCustomToastMsg(this, R.string.correct_recommended_no, false);
                        return;
                    }
                }

                if (AppPreference.getCountryImageIDLogin(this) != -1) {
                    mCountryImageRecommended.setImageResource(flagRecommended);
                }
                mCountryCodeRecommendedConfirm.setText("(" + countryCodeRecommended + ")");
                mCountryCodeRecommended.setText(countryNameRecommended + " (" + countryCodeRecommended + ")");
                tvCountryCodeRecommended.setText("(" + countryCodeRecommended + ")");
                mRecommendedBy.removeTextChangedListener(new MyTextWatcherForRecommended());

                mRecommdedByDisplayName.setText(name);
                if (countryCodeRecommended.equalsIgnoreCase("+95")) {
                    mRecommendedBy.setText(getNumberWithGreyZero(no, true));
                } else {
                    mRecommendedBy.setText(no);
                }
                llRecommendedConfirm.setVisibility(View.GONE);
                mRecommendedBy.setFocusable(false);
                mRecommdedByDisplayName.setVisibility(View.VISIBLE);
                mRecommendedBy.setOnClickListener(null);
                return;
            } else {
                showToastPopUp(R.string.error_msg_pick_phone);
            }
        }

        if (requestCode == 101) {
            mPredefineNoDisplayName.setVisibility(View.GONE);
            mPredefinedNumber.setFocusable(true);
            mPredefinedNumber.setFocusableInTouchMode(true);
            mConfirmPredefineNo.setFocusableInTouchMode(true);
            mPredefinedNumber.requestFocus();
            if (data != null) {
                if (data.getIntExtra("POSITION", -1) != -1) {
                    mCountryImagePredefine.setImageResource(data.getIntExtra("POSITION", -1));
                    flagPredefine = data.getIntExtra("POSITION", -1);
                }
                mCountryCodePredefine.setText(data.getStringExtra("COUNTRY_NAME") + " (" + data.getStringExtra("COUNTRY_CODE") + ")");
                tvCountryCodePredefine.setText("(" + data.getStringExtra("COUNTRY_CODE") + ")");
                countryCodePredefine = data.getStringExtra("COUNTRY_CODE");
                countryNamePredefine = data.getStringExtra("COUNTRY_NAME");
                mCountryCodePredefineConfirm.setText("(" + countryCodePredefine + ")");

                if (!data.getStringExtra("COUNTRY_CODE").equalsIgnoreCase("+95")) {
                    mPredefinedNumber.setText(null);
                    //mNumber.setHint(getResources().getString(R.string.Enter_Your_Email_id));
                } else {
                    mPredefinedNumber.setText(Utils.getMyanmarNumber());
                    mPredefinedNumber.setSelection(2);
                }
            }
        }

        if (requestCode == 1001) {
            mRecommdedByDisplayName.setVisibility(View.GONE);
            mRecommendedBy.setFocusable(false);
            mRecommendedBy.setFocusableInTouchMode(true);
            mConfirmRecommendedBy.setFocusableInTouchMode(true);
            llRecommendedConfirm.setVisibility(View.VISIBLE);

            mRecommendedBy.requestFocus();
            if (data != null) {
                if (data.getIntExtra("POSITION", -1) != -1) {
                    mCountryImageRecommended.setImageResource(data.getIntExtra("POSITION", -1));
                    flagRecommended = data.getIntExtra("POSITION", -1);
                }
                mCountryCodeRecommended.setText(data.getStringExtra("COUNTRY_NAME") + " (" + data.getStringExtra("COUNTRY_CODE") + ")");
                tvCountryCodeRecommended.setText("(" + data.getStringExtra("COUNTRY_CODE") + ")");
                countryCodeRecommended = data.getStringExtra("COUNTRY_CODE");
                countryNameRecommended = data.getStringExtra("COUNTRY_NAME");
                mCountryCodeRecommendedConfirm.setText("(" + countryCodeRecommended + ")");

                if (!data.getStringExtra("COUNTRY_CODE").equalsIgnoreCase("+95")) {
                    mRecommendedBy.setText(null);
                    //mNumber.setHint(getResources().getString(R.string.Enter_Your_Email_id));
                } else {
                    mRecommendedBy.setText(Utils.getMyanmarNumber());
                    mRecommendedBy.setSelection(2);
                }
            }

            mRecommendedBy.setOnClickListener(null);
        }

        if (requestCode == 200) {
            if (data != null) {

                mTapHere.setVisibility(View.GONE);
                mSignatureDemo.setVisibility(View.GONE);
                mSignatureImage.setVisibility(View.VISIBLE);
                mSignatureImage.setImageBitmap(PotraitSignaturePad.signatureBitmap);
                isSigned = true;
                if (AppPreference.getLanguage(this).equals("my")) {
                    changeLang("my");
                } else if (AppPreference.getLanguage(this).equals("en")) {
                    changeLang("en");
                }
            } else if (AppPreference.getLanguage(this).equals("my")) {
                changeLang("my");
            } else if (AppPreference.getLanguage(this).equals("en")) {
                changeLang("en");
            }
        }

    }

    private void select_identtity() {
        final String[] items = {getResources().getString(R.string.None), getResources().getString(R.string.identity_college), getResources().getString(R.string.companyId), getResources().getString(R.string.Passport), getResources().getString(R.string.drivingLinId), getResources().getString(R.string.other)};
        AlertDialog.Builder builder = new AlertDialog.Builder(NewRegistrationActivity1.this);

        @SuppressLint("InflateParams")
        View view = getLayoutInflater().inflate(R.layout.custom_title_dialog, null, false);
        CustomTextView tv = (CustomTextView) view.findViewById(R.id.title);
        tv.setText(getResources().getString(R.string.select_identity_card));
        tv.setTypeface(Utils.getFont(this), Typeface.BOLD);
        builder.setCustomTitle(view);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.single_choice_dailog_custom, R.id.custom_tv, items);
        builder.setSingleChoiceItems(adapter, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                switch (item) {
                    case 0:
                        /*identityType = items[item];
                        mNRCEdittext.setVisibility(View.GONE);
                        mLinearLyoutd.setVisibility(View.VISIBLE);
                        mOtherIdEdit.setText(identityType);
                        mOtherIdEdit.setEnabled(false);
                        mOtherIdPrefix.setVisibility(View.GONE);
                        mOtherId.setVisibility(View.GONE);*/
                        mNoID.setVisibility(View.VISIBLE);
                        mNoID.setChecked(true);
                        dialog.dismiss();
                        break;
                    case 1:
                        mOtherIdEdit.setEnabled(true);
                        mOtherIdPrefix.setVisibility(View.VISIBLE);
                        identityType = items[item];
                        dialog.dismiss();
                        mOtherIdPrefix.setText(identityType);
                        mLinearLyoutd.setVisibility(View.VISIBLE);
                        mNRCEdittext.setVisibility(View.GONE);
                        mOtherId.setVisibility(View.GONE);
                        mOtherIdEdit.setHint(getResources().getString(R.string.enter_college_id));
                        mOtherIdEdit.requestFocus();

                        Utils.playAudio(NewRegistrationActivity1.this, R.raw.why_id);
                        showAlert(getString(R.string.id_msg));
                        break;
                    case 2:
                        mOtherIdEdit.setEnabled(true);
                        mOtherIdPrefix.setVisibility(View.VISIBLE);
                        identityType = items[item];
                        dialog.dismiss();
                        mLinearLyoutd.setVisibility(View.VISIBLE);
                        mNRCEdittext.setVisibility(View.GONE);
                        mOtherIdPrefix.setText(identityType);
                        mOtherId.setVisibility(View.GONE);
                        mOtherIdEdit.setHint(getResources().getString(R.string.enter_company_id));
                        mOtherIdEdit.requestFocus();
                        Utils.playAudio(NewRegistrationActivity1.this, R.raw.why_id);
                        showAlert(getString(R.string.id_msg));
                        break;
                    case 3:
                        mOtherIdEdit.setEnabled(true);
                        mOtherIdPrefix.setVisibility(View.VISIBLE);
                        identityType = items[item];
                        dialog.dismiss();
                        mNRCEdittext.setVisibility(View.GONE);
                        mLinearLyoutd.setVisibility(View.VISIBLE);
                        mOtherIdPrefix.setText(identityType);
                        mOtherId.setVisibility(View.GONE);
                        mOtherIdEdit.setHint(getResources().getString(R.string.enter_passort_no));
                        mOtherIdEdit.requestFocus();
                        Utils.playAudio(NewRegistrationActivity1.this, R.raw.why_id);
                        showAlert(getString(R.string.id_msg));
                        break;
                    case 4:
                        mOtherIdEdit.setEnabled(true);
                        mOtherIdPrefix.setVisibility(View.VISIBLE);
                        identityType = items[item];
                        dialog.dismiss();
                        mNRCEdittext.setVisibility(View.GONE);
                        mLinearLyoutd.setVisibility(View.VISIBLE);
                        mOtherIdPrefix.setText(identityType);
                        mOtherId.setVisibility(View.GONE);
                        mOtherIdEdit.setHint(getResources().getString(R.string.enter_driving_id));
                        mOtherIdEdit.requestFocus();
                        Utils.playAudio(NewRegistrationActivity1.this, R.raw.why_id);
                        showAlert(getString(R.string.id_msg));
                        break;
                    case 5:
                        mOtherIdEdit.setEnabled(true);
                        identityType = items[item];
                        dialog.dismiss();
                        mNRCEdittext.setVisibility(View.GONE);
                        mLinearLyoutd.setVisibility(View.VISIBLE);
                        mOtherId.setVisibility(View.GONE);
                        mOtherIdPrefix.setText(identityType);
                        mOtherIdEdit.setHint(getResources().getString(R.string.select_others_text));
                        mOtherIdPrefix.setVisibility(View.GONE);
                        mOtherIdEdit.requestFocus();
                        Utils.playAudio(NewRegistrationActivity1.this, R.raw.why_id);
                        showAlert(getString(R.string.id_msg));
                        break;


                    default:
                        break;
                }


                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }


    private void showTermSAndCondition(String file, final boolean isAgent) {

        Spanned html = Html.fromHtml(Utils.readHTMLFile(this, file));
        if (isWindowOpen)
            cancelNotification();
        final Dialog dialog = new Dialog(NewRegistrationActivity1.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.terms_and_conditions);
        final TextView userInput = (TextView) dialog.findViewById(R.id.dialog_editext);
        userInput.setText(html, TextView.BufferType.SPANNABLE);
        final TextView imgCancel = (TextView) dialog.findViewById(R.id.img_cancel);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        imgCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (!isAgent)
                    llTerms.clearAnimation();
            }

        });
        dialog.show();

    }

    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year, int monthOfYear, int dayOfMonth) {

    }

    private void setDateListener() {

        mDateOfBirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //hideKeyboard();
                if (isWindowOpen)
                    cancelNotification();
                if (AppPreference.getLanguage(getApplicationContext()).equalsIgnoreCase("my")) {
                    changeLang("en");
                    isLanguageChanged = true;
                } else
                    isLanguageChanged = false;


                showDatePicker();


          /*      Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.YEAR, -5);
                android.app.DatePickerDialog datePickerDialog=new android.app.DatePickerDialog(getApplicationContext(), R.style.CustomTheme, null , calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.setContentView(R.layout.custom_date_picker);
                datePickerDialog.show();
*/

              /* DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getSupportFragmentManager(), "datePicker");*/
            }
        });
    }

    private void setOnCheckedChangeForAccept() {
        cbTerm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    cbTerm.setChecked(false);
                    cbTerm.setClickable(false);
                    cbTerm.setFocusable(true);
                    chIAccept.setChecked(false);
                }
            }
        });
        chIAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isWindowOpen)
                    cancelNotification();
                chIAccept.setChecked(true);
                submitForRegistration();
            }
        });

        mNoID.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    mNrcClear.performClick();
                    mOtherId.setVisibility(View.VISIBLE);
                } else {
                    mNRCEdittext.setVisibility(View.GONE);
                    mNRCEdittext.setText("");
                    mLinearLyoutd.setVisibility(View.GONE);
                    mOtherId.setVisibility(View.GONE);
                    mOtherIdEdit.setText("");
                }
            }
        });

        cbNonMandatory.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    llIDProof.setVisibility(View.VISIBLE);
                    //mNRCEdittext.requestFocus();
                    llReadyToRegister.setVisibility(View.GONE);
                    tvOR.setVisibility(View.GONE);
                } else {
                    llIDProof.setVisibility(View.GONE);
                    mNRCEdittext.clearFocus();
                    //mBusinessClear.performClick();
                    mCarClear.performClick();
                    mAddressClear.performClick();
                    //mNrcClear.performClick();
                    llReadyToRegister.setVisibility(View.VISIBLE);
                    tvOR.setVisibility(View.VISIBLE);
                }
            }
        });
       /* cbAgentCode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    llAgentCodeChild.setVisibility(View.VISIBLE);
                    hideKeyboard();
                    showTermSAndCondition(TERMS_CONDITION_AGENT, true);
                    mAgentCode.requestFocus();
                } else {
                    llAgentCodeChild.setVisibility(View.GONE);
                    mAgentCode.clearFocus();
                    mAgentCode.setText(null);
                }
            }
        });
        cbAgentCode1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                cbAgentCode1.setChecked(true);
                openCustomerCareDialog();
            }
        });*/



        mAddressType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (group.getCheckedRadioButtonId() == R.id.other)
                    mEtAdressType.setVisibility(View.VISIBLE);
                else {
                    mEtAdressType.setVisibility(View.GONE);
                    mEtAdressType.setText(null);
                }
            }
        });
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        switch (v.getId()) {

            case R.id.registration_page_edittext_recommendedby:
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (!isOpened) {

                        singleChoiceDialog();
                        hideKeyboard();
                    } else
                        showKeyboard(mRecommendedBy);
                }

                break;


            default:
                break;

        }

        return true;
    }

    private void singleChoiceDialog() {
        if (isWindowOpen)
            cancelNotification();
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.single_choice_dailog_custom, R.id.custom_tv, new String[]{getResources().getString(R.string.select_myself_text), getResources().getString(R.string.select_others_text)});
        builder.setSingleChoiceItems(adapter, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        isMySelf = 0;
                        isOpened = true;
                        llRecommendedByAll.setVisibility(View.GONE);
                        mRecommendedBy.removeTextChangedListener(myTextWatcherForRecommended);
                        mRecommendedBy.setText(mMobileNo.getText().toString());
                        //mRecommendedBy.setText("00000000");
                        break;
                    case 1:
                        isMySelf = 1;
                        isOpened = true;
                        mRecommendedBy.addTextChangedListener(myTextWatcherForRecommended);
                        llRecommendedByAll.setVisibility(View.VISIBLE);
                        mRecommendedBy.setFocusableInTouchMode(true);
                        llRecommendedConfirm.setVisibility(View.VISIBLE);
                        mConfirmRecommendedBy.setText("");
                        showKeyboard(mRecommendedBy);
                        break;

                }
                dialog.dismiss();
            }
        });

        builder.setCancelable(true);
        builder.show();

    }

    private void setSlider(final String which, boolean isGoTo) {
        tvHeader.setBackgroundColor(getResources().getColor(R.color.slider_header));
        relativeSlider.setBackgroundColor(getResources().getColor(R.color.background_material_light));
        btnBack.setBackgroundResource(R.drawable.rounded_btn);
        listOfCategoryImage = null;
        listOfEmoji = null;

        if (isWindowOpen)
            cancelNotification();
        type = which;
        boolean isColor = false;

        if (AppPreference.getLanguage(this).equalsIgnoreCase("my")) {
            if (which.equalsIgnoreCase("DIVISION")) {
                tvHeader.setText(getResources().getString(R.string.select_div_text));
                list = db.getNAME_OF_DIVISIONLISTNAME_BURMESE("1");
                mMainDvisisionListNew = db.getNAME_OF_DIVISIONLIST("1");
                btnGoTo.setText(getResources().getString(R.string.goto_state));
                btnGoTo.setVisibility(View.VISIBLE);
            } else if (which.equalsIgnoreCase("STATE")) {
                tvHeader.setText(getResources().getString(R.string.select_state_text));
                list = db.getNAME_OF_DIVISIONLISTNAME_BURMESE("0");
                mMainDvisisionListNew = db.getNAME_OF_DIVISIONLIST("0");
                btnGoTo.setText(getResources().getString(R.string.goto_div));
                btnGoTo.setVisibility(View.VISIBLE);
            } else if (which.equalsIgnoreCase("TOWNSHIP")) {
                list = townShipList;
                tvHeader.setText(getResources().getString(R.string.select_town_text));
                btnGoTo.setVisibility(View.GONE);
            } else if (which.equalsIgnoreCase("MAIN_CATEGORY")) {
                getMainCategory();
                tvHeader.setText(getResources().getString(R.string.select_business_category));
                btnGoTo.setVisibility(View.GONE);
                listOfCategoryImage = Constant.listOfCategoryImages;
            } else if (which.equalsIgnoreCase("SUB_CATEGORY")) {
                getSubCategory(mBusinessCategory.getText().toString());
                tvHeader.setText(getResources().getString(R.string.select_business_subcategory));
                btnGoTo.setVisibility(View.GONE);
                tvHeader.setBackgroundColor(getResources().getColor(R.color.light_dim_grey));
                btnBack.setBackgroundResource(R.drawable.shape);
                relativeSlider.setBackgroundColor(getResources().getColor(R.color.bookstore_listview_bg));
            }

        } else {

            if (which.equalsIgnoreCase("DIVISION")) {
                tvHeader.setText(getResources().getString(R.string.select_div_text));
                list = db.getNAME_OF_DIVISIONLISTNAME("1");
                mMainDvisisionListNew = db.getNAME_OF_DIVISIONLIST("1");
                btnGoTo.setText(getResources().getString(R.string.goto_state));
                btnGoTo.setVisibility(View.VISIBLE);
            } else if (which.equalsIgnoreCase("STATE")) {
                tvHeader.setText(getResources().getString(R.string.select_state_text));
                list = db.getNAME_OF_DIVISIONLISTNAME("0");
                mMainDvisisionListNew = db.getNAME_OF_DIVISIONLIST("0");
                btnGoTo.setText(getResources().getString(R.string.goto_div));
                btnGoTo.setVisibility(View.VISIBLE);
            } else if (which.equalsIgnoreCase("TOWNSHIP")) {
                list = townShipList;
                tvHeader.setText(getResources().getString(R.string.select_town_text));
                btnGoTo.setVisibility(View.GONE);
            } else if (which.equalsIgnoreCase("MAIN_CATEGORY")) {
                getMainCategory();
                tvHeader.setText(getResources().getString(R.string.select_business_category));
                btnGoTo.setVisibility(View.GONE);
                listOfCategoryImage = Constant.listOfCategoryImages;

            } else if (which.equalsIgnoreCase("SUB_CATEGORY")) {
                getSubCategory(mBusinessCategory.getText().toString());
                tvHeader.setText(getResources().getString(R.string.select_business_subcategory));
                btnGoTo.setVisibility(View.GONE);
                tvHeader.setBackgroundColor(getResources().getColor(R.color.light_dim_grey));
                btnBack.setBackgroundResource(R.drawable.shape);
                relativeSlider.setBackgroundColor(getResources().getColor(R.color.bookstore_listview_bg));
            }
        }

        mDrawerList.setAdapter(new SliderListAdapter(this, list, colorsCode, isColor, listOfCategoryImage, listOfEmoji));
        if (!isGoTo)
            mDrawerLayout.openDrawer(Gravity.RIGHT);

        mDrawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                // currentFocusedView.clearFocus();
                //isSliderOpened = true;
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);

            }

            @Override
            public void onDrawerClosed(View drawerView) {
                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {


        if (type.equalsIgnoreCase("DIVISION")) {
            addressAlert();

            mTownship.setVisibility(View.VISIBLE);
            mDivisionId = mMainDvisisionListNew.get(position).getId();
            mDivisionIDText.setText(mDivisionId);
            //mState.setClickable(false);
            mState.setVisibility(View.GONE);
            mTownship.setText(getResources().getString(R.string.select_town_text));
            mDivision.setVisibility(View.VISIBLE);
            mCity.setVisibility(View.VISIBLE);
            mNoOrRoadorWard.setVisibility(View.VISIBLE);
            mNoOrRoadorWard.setEnabled(false);

            if (AppPreference.getLanguage(this).equalsIgnoreCase("my")) {
                String mDivisionName = mMainDvisisionListNew.get(position).getBName();
                //mStateAndDividionName = mMainDvisisionListNew.get(position).getBName();
                mDivision.setText(mDivisionName);
                townShipList = db.getSELECTED_TOWNSHIP_NAME_BURMESE(mDivisionId);
                mTownShipModelListNew = db.getTownList(mDivisionId);
                //cityCode = mTownShipModelListNew.get(position).getTownId();

            } else {
                String mDivisionName = mMainDvisisionListNew.get(position).getName();
                //mStateAndDividionName = mMainDvisisionListNew.get(position).getName();
                mDivision.setText(mDivisionName);
                townShipList = db.getSELECTED_TOWNSHIP_NAME(mDivisionId);
                mTownShipModelListNew = db.getTownList(mDivisionId);
                //cityCode = mTownShipModelListNew.get(position).getTownId();
            }

            cityCode = "";
            mCity.setText(null);
            mNoOrRoadorWard.setText(null);

        } else if (type.equalsIgnoreCase("STATE")) {

            addressAlert();
            mTownship.setVisibility(View.VISIBLE);
            mDivisionId = mMainDvisisionListNew.get(position).getId();
            mDivisionIDText.setText(mDivisionId);
            //mDivision.setClickable(false);
            mDivision.setVisibility(View.GONE);
            mTownship.setText(getResources().getString(R.string.select_town_text));
            //mCity.setText("");
            mState.setVisibility(View.VISIBLE);
            mCity.setVisibility(View.VISIBLE);
            mNoOrRoadorWard.setVisibility(View.VISIBLE);
            mNoOrRoadorWard.setEnabled(false);

            if (AppPreference.getLanguage(this).equalsIgnoreCase("my")) {
                String mStateName = mMainDvisisionListNew.get(position).getBName();
                //mStateAndDividionName = mMainDvisisionListNew.get(position).getBName();
                mState.setText(mStateName);
                townShipList = db.getSELECTED_TOWNSHIP_NAME_BURMESE(mDivisionId);
                mTownShipModelListNew = db.getTownList(mDivisionId);
                //cityCode = mTownShipModelListNew.get(position).getTownId();
            } else {
                String mStateName = mMainDvisisionListNew.get(position).getName();
                //mStateAndDividionName = mMainDvisisionListNew.get(position).getName();
                mState.setText(mStateName);
                townShipList = db.getSELECTED_TOWNSHIP_NAME(mDivisionId);
                mTownShipModelListNew = db.getTownList(mDivisionId);
                //cityCode = mTownShipModelListNew.get(position).getTownId();
            }
            cityCode = "";
            mCity.setText(null);
            mNoOrRoadorWard.setText(null);

        } else if (type.equalsIgnoreCase("TOWNSHIP")) {
            String mTownshipName = list.get(position);
            mTownship.setText(mTownshipName);
            mTownship.setVisibility(View.VISIBLE);
            cityCode = mTownShipModelListNew.get(position).getTownId();
            if (mTownShipModelListNew.get(position).getIsDefaultCity().equalsIgnoreCase("1")) {
                if (AppPreference.getLanguage(this).equalsIgnoreCase("my")) {
                    mCity.setText(mTownShipModelListNew.get(position).getDefaultCityBName());
                } else {
                    mCity.setText(mTownShipModelListNew.get(position).getDefaultCityName());
                }
                mCity.setEnabled(false);
            } else {

                if (AppPreference.getLanguage(this).equalsIgnoreCase("my")) {
                    singleChoiceDialogForCity(mTownShipModelListNew.get(position).getBCityBName());
                } else {
                    singleChoiceDialogForCity(mTownShipModelListNew.get(position).getBCityName());
                }
                mCity.setEnabled(true);
                mCity.setText(null);
            }
            mNoOrRoadorWard.setEnabled(true);
            mNoOrRoadorWard.requestFocus();
            mNoOrRoadorWard.setText(null);
        } else if (type.equalsIgnoreCase("MAIN_CATEGORY")) {
            mBusinessCategory.setText(list.get(position));
            llBusinessSubcategory.setVisibility(View.VISIBLE);
            // mNoID.setVisibility(View.GONE);
            mBusinessSubCategory.setText(getResources().getString(R.string.business_subcategory));
            mCategoryPos = position;
            imgBusinessSubcategory.setText(null);
            imgBusinessName2.setText(null);
            mBusinessCategory.setCompoundDrawablesWithIntrinsicBounds(listOfCategoryImage[position], 0, R.drawable.arrow1, 0);
            imgBusinessName1.setBackgroundResource(listOfCategoryImage[position]);
            mSubCategoryPos = -1;
            mBusinessName.setText("");
            setSlider("SUB_CATEGORY", false);
            return;

        } else if (type.equalsIgnoreCase("SUB_CATEGORY")) {
            if (mCategoryPos == -1)
                mCategoryPos = sharedPref.getInt("CAT_POS", -1);
            mBusinessSubCategory.setText(list.get(position));
            mBusinessSubCategory.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow1, 0);
            mSubCategoryPos = position;
            llBusinessName.setVisibility(View.VISIBLE);
            imgBusinessName2.setText(listOfEmoji.get(position));
            imgBusinessSubcategory.setText(listOfEmoji.get(position));
            mBusinessName.requestFocus();
        }
        mDrawerLayout.closeDrawer(Gravity.RIGHT);
    }

    private void singleChoiceDialogForCity(final String cityName) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = getLayoutInflater().inflate(R.layout.custom_title_dialog, null, false);
        CustomTextView tv = (CustomTextView) view.findViewById(R.id.title);
        tv.setText(getResources().getString(R.string.select_city_region));
        tv.setTypeface(Utils.getFont(this), Typeface.BOLD);
        builder.setCustomTitle(view);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.single_choice_dailog_custom, R.id.custom_tv, new String[]{cityName, getResources().getString(R.string.region_text)});

        builder.setSingleChoiceItems(adapter, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        mCity.setText(cityName);
                        mCity.setEnabled(false);
                        mCity.setTextColor(getResources().getColor(R.color.black));
                        mNoOrRoadorWard.requestFocus();
                        break;
                    case 1:
                        mCity.setEnabled(true);
                        mCity.setText("");
                        mCity.requestFocus();
                        mCity.setHint(getResources().getString(R.string.region_text));
                        break;

                }
                mCity.setVisibility(View.VISIBLE);
                mNoOrRoadorWard.setVisibility(View.VISIBLE);
                dialog.dismiss();
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                mCity.setEnabled(true);
                mCity.setText("");
                mCity.requestFocus();
            }
        });

        builder.setCancelable(true);
        builder.show();

    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {

        switch (view.getId()) {
            case R.id.registration_page_edittext_fatherName:
                if (hasFocus) {
                    cancelNotification();
                }
                break;
            case R.id.business_name:
                if (hasFocus) {
                    cancelNotification();
                }
                break;

            case R.id.registration_page_edittext_city:
                if (hasFocus) {
                    cancelNotification();
                }
                break;
            case R.id.registration_page_road_ward_none:
                if (hasFocus) {
                    cancelNotification();
                }
                break;


            case R.id.registration_page_edittext_recommendedby:
                if (hasFocus && mRecommendedBy.getText().length() == 0) {
                    if (countryCodeRecommended.equalsIgnoreCase("+95")) {
                        mRecommendedBy.setText(Utils.getMyanmarNumber());
                        mRecommendedBy.setSelection(2);
                    }
                    if (isWindowOpen) {
                        animationView("NONE");
                        tv.setText(R.string.error_recommeded_by);
                    } else
                        setNotification(R.string.error_recommeded_by, "NONE");

                } else if (isWindowOpen) {
                    cancelNotification();
                }
                break;
            case R.id.registration_page_edittext_redefineNo:
                if (hasFocus) {
                    llPredefineConfirm.setVisibility(View.VISIBLE);
                    if (/*!isPredefineTextChanged &&*/ countryCodePredefine.equalsIgnoreCase("+95")) {
                        mPredefinedNumber.setText(Utils.getMyanmarNumber());
                        mPredefinedNumber.setSelection(mPredefinedNumber.length());
                    }
                   /* if (isWindowOpen) {
                        animationView("PREDEFINE_NO");
                        tv.setText(R.string.predefined_popup_text);
                    } else
                        setNotification(R.string.predefined_popup_text, "PREDEFINE_NO");*/
                    showAlert(getString(R.string.predefined_popup_text));
                } else if (isWindowOpen) {
                    cancelNotification();
                }
                break;
            case R.id.predefine_no_confirmation:
                if (hasFocus) {
                    if (countryCodePredefine.equalsIgnoreCase("+95")) {
                        mConfirmPredefineNo.setText(Utils.getMyanmarNumber());
                        //mConfirmPredefineNo.setSelection(2);
                    }
                    try {
                        if (mConfirmPredefineNo.getText().length() != 0 && llPredefineConfirm.getVisibility() == View.VISIBLE)
                            mConfirmPredefineNo.setSelection(2);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (isWindowOpen) {
                    cancelNotification();
                }
                break;
            case R.id.recommended_by_confirmation:
                if (hasFocus && mConfirmRecommendedBy.getText().length() == 0) {
                    if (countryCodeRecommended.equalsIgnoreCase("+95")) {
                        mConfirmRecommendedBy.setText(Utils.getMyanmarNumber());
                        //mConfirmRecommendedBy.setSelection(2);
                    }

                    try {
                        if (mConfirmRecommendedBy.getText().length() != 0 && llRecommendedConfirm.getVisibility() == View.VISIBLE)
                            mConfirmRecommendedBy.setSelection(2);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (isWindowOpen) {
                    cancelNotification();
                }
                break;
            case R.id.registration_page_edittext_NRC:

                if (hasFocus) {
                    if (isWindowOpen) {
                        cancelNotification();
                    }
                    if (count==0) {
                        Utils.playAudio(this, R.raw.why_id);
                        count++;
                    }
                    showAlert(getString(R.string.id_msg));
                }
                break;

            case R.id.registration_page_textview_text_userName_input:
                if (hasFocus) {
                    if (isWindowOpen) {
                        animationView("NONE");
                        tv.setText(R.string.error_enter_name);
                    } else
                        setNotification(R.string.error_enter_name, "NONE");
                }
                break;

            case R.id.registration_page_edittext_password:
                if (hasFocus) {

                    if (isWindowOpen) {
                        animationView("NONE");
                        tv.setText(R.string.pass_popup_text);
                    } else
                        setNotification(R.string.pass_popup_text, "NONE");

                }
                break;

            case R.id.registration_page_edittext_confirm_password:
                if (hasFocus) {
                    if (isWindowOpen) {
                        animationView("NONE");
                        tv.setText(R.string.pls_note_down);
                    } else
                        setNotification(R.string.pls_note_down, "NONE");
                }
                break;
        }
    }

    private void getMainCategory() {
        list.clear();
        ArrayList<BusinessCategoryModel> listOfModel = businessCategoryModel.getBusinessCategoryModel(this);
        for (BusinessCategoryModel model : listOfModel) {
            list.add(model.getMainCategory());
        }
        listOfModel.clear();

    }

    private void getSubCategory(String mainCategory) {
        list.clear();
        //listOfEmoji.clear();
        ArrayList<BusinessCategoryModel> listOfModel = businessCategoryModel.getBusinessCategoryModel(this);
        for (BusinessCategoryModel model : listOfModel) {
            if (model.getMainCategory().equalsIgnoreCase(mainCategory)) {
                list = model.getListOfSubCategory();
                listOfEmoji = model.getListOfSubCategoryEmoji();
            }
        }
        listOfModel.clear();
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        /*if (radioGroup.getCheckedRadioButtonId() == R.id.other)
            mEtAdressType.setVisibility(View.VISIBLE);
        else
            mEtAdressType.setVisibility(View.GONE);*/

        if (radioGroup.getCheckedRadioButtonId() == R.id.business_account || radioGroup.getCheckedRadioButtonId() == R.id.business_account1) {
            mBusinessCategory.setText(getResources().getString(R.string.business_category));
            mBusinessCategory.setVisibility(View.VISIBLE);
            llBusinessName.setVisibility(View.GONE);
            mBusinessName.setText(null);
            llBusinessSubcategory.setVisibility(View.GONE);
            mLinearBusinessCategory.setVisibility(View.VISIBLE);
            imgBusinessSubcategory.setText(null);
            imgBusinessName2.setText(null);
            mBusinessCategory.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow1, 0);

            if (isWindowOpen) {
                cancelNotification();
            }
            mCategoryPos = -1;
            mSubCategoryPos = -1;
            mLinearPredefineNo.setVisibility(View.GONE);
            //  llAgentCode.setVisibility(View.VISIBLE);
            cbNonMandatory.setChecked(true);

            mNoID.setVisibility(View.GONE);
            mNoID.setChecked(false);

            isBusinessAccount=true;

            mCarClear.performClick();

        } else if (radioGroup.getCheckedRadioButtonId() == R.id.personal_account) {
            mLinearBusinessCategory.setVisibility(View.GONE);
            mBusinessCategory.setVisibility(View.GONE);
            llBusinessSubcategory.setVisibility(View.GONE);
            llBusinessName.setVisibility(View.GONE);
            imgBusinessSubcategory.setText(null);
            imgBusinessName2.setText(null);
            if (isWindowOpen) {
                tv.setText(R.string.personal_account_text);
            } else
                setNotification(R.string.personal_account_text, "NONE");
            mCategoryPos = -1;
            mSubCategoryPos = -1;
            mLinearPredefineNo.setVisibility(View.VISIBLE);
            llAgentCode.setVisibility(View.GONE);
            //mAgentCode.setVisibility(View.GONE);
            //cbAgentCode.setChecked(false);

            isBusinessAccount=false;

            isAgelower18();

        }
    }

    private void getlatlong() {
        GPSTracker gpsTracker = new GPSTracker(this);
        if (gpsTracker.gpsStatus()) {
            latitude = String.valueOf(gpsTracker.latitude);
            longitude = String.valueOf(gpsTracker.longitude);
            String[] address = Utils.getAddressFromGPSLocation(this, gpsTracker.latitude, gpsTracker.longitude);
            if (address.length == 4) {
                mPostalCode.setText(address[3]);
                mStateProvice.setText(address[2]);
                mCity.setText(address[1]);
                mStreetNo.setText(address[0]);
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public void onSigned() {
        //Event triggered when the pad is signed
        //mSignaturePad.setEnabled(false);
        isSigned = true;
        if (isWindowOpen)
            cancelNotification();

    }

    @Override
    public void onClear() {
        //Event triggered when the pad is cleared
        isSigned = false;
        //mSignaturePad.setEnabled(true);
        mSignaturePad.setOnTouchListener(null);
    }

    private void submitForRegistration() {

        String IDType = mOtherIdPrefix.getText().toString();
        String otherIDNo = mOtherIdEdit.getText().toString();

        String userName = muserNameEdit.getText().toString();
        String DOB = mDateOfBirth.getText().toString();
        String Email = mEmail.getText().toString();
        String nrcNo = mNRCEdittext.getText().toString();
        String fatherName = mFatherName.getText().toString();
        String predefineNo = Utils.getUniversalFormattedNoWithoutPlus(Utils.getUniversalFormattedMobileNOWithCountryCode(countryCodePredefine, mPredefinedNumber.getText().toString()));
        String confirmPredefineNo = Utils.getUniversalFormattedNoWithoutPlus(Utils.getUniversalFormattedMobileNOWithCountryCode(countryCodePredefine, mConfirmPredefineNo.getText().toString()));
        String password = mPassword.getText().toString();
        String confirmPassword = mConfirmPassword.getText().toString();
        String recomendedBy = Utils.getUniversalFormattedNoWithoutPlus(Utils.getUniversalFormattedMobileNOWithCountryCode(countryCodeRecommended, mRecommendedBy.getText().toString()));
        String confirmRecomendedBy = Utils.getUniversalFormattedNoWithoutPlus(Utils.getUniversalFormattedMobileNOWithCountryCode(countryCodeRecommended, mConfirmRecommendedBy.getText().toString()));

        if (isValid(userName, DOB, fatherName, nrcNo, IDType, otherIDNo, password, confirmPassword, Email, true)) {

            if (!Utils.mobileNumberValidation(mPredefinedNumber.getText().toString()) && !Utils.isEmpty(mPredefinedNumber.getText().toString())) {
                showToastPopUp(R.string.error_predefine_no);
                return;
            }

            if (countryCodePredefine.equalsIgnoreCase("+95")){
                if (!mPredefinedNumber.getText().toString().startsWith("09") && !Utils.isEmpty(mPredefinedNumber.getText().toString())) {
                    showToastPopUp(R.string.correct_predefined_no);
                    return;
                }
            }

            if ((!predefineNo.equalsIgnoreCase(confirmPredefineNo)) && llPredefineConfirm.getVisibility() == View.VISIBLE) {
                showToastPopUp(R.string.error_predefine_no_confirmation);
                return;
            }

            if ((!Utils.mobileNumberValidation(mRecommendedBy.getText().toString()) && !Utils.isEmpty(mRecommendedBy.getText().toString()))) {
                showToastPopUp(R.string.error_recommended_no);
                return;
            }
            if(countryCodeRecommended.equalsIgnoreCase("+95")) {
                if (!mRecommendedBy.getText().toString().startsWith("09") && !Utils.isEmpty(mRecommendedBy.getText().toString())) {
                    showToastPopUp(R.string.correct_recommended_no);
                    return;
                }
            }

            if ((!recomendedBy.equalsIgnoreCase(confirmRecomendedBy)) && llRecommendedConfirm.getVisibility() == View.VISIBLE) {
                showToastPopUp(R.string.error_recommended_no_confirmation);
                return;
            }

            if (password.length() < 6 && isProfileUpdate != 1) {
                showToastPopUp(R.string.passwordmustbe6fotit);
                return;
            }

            if (!password.equalsIgnoreCase(confirmPassword) && isProfileUpdate != 1) {
                showToastPopUp(R.string.error_confirm_password);
                return;
            }
            if (!Utils.isEmpty(mRecommendedBy.getText().toString()) && !Utils.isEmpty(mPredefinedNumber.getText().toString())) {
                if (mRecommendedBy.getText().toString().equals(mPredefinedNumber.getText().toString())) {
                    showToastPopUp(R.string.recommdedNumber_not_be_Same);
                    return;
                }

            }
            //---------------------- -----------------------------  for merchant no
            if(merchant_mobileno.getText().toString()==null) {
                showToastPopUp(R.string.entr_merchant_no);
                return;
            }

            else if(merchant_mobileno.getText().toString().equals("09")||merchant_mobileno.getText().toString().equals("")) {
                showToastPopUp(R.string.entr_merchant_no);
                return;
            }



            //--------------------------------------------------------------

            if (cbTerm.isChecked() && isSigned && isProfileUpdate != 1) {
                chIAccept.setChecked(true);

            } else if (isProfileUpdate != 1) {//TODO Adding seprate validation for Signature and Terms and condition @Abhishek
                chIAccept.setChecked(false);
                if (!cbTerm.isChecked() && !isSigned) {
                    showToastPopUp(R.string.error_terms_conditions);
                } else {
                    if (!cbTerm.isChecked()) {
                        showToastPopUp(R.string.accept_terms_conditions);
                    } else {
                        showToastPopUp(R.string.error_please_sing_your_signature);
                    }
                }
                return;
            }
            if (chIAccept.isChecked()) {

                if (Utils.isReallyConnectedToInternet(this)) {
                    setDataIntoProfile(userName, fatherName, nrcNo, recomendedBy, predefineNo, DOB);
                    if (isProfileUpdate == 1) {
                       /* profile.setMsisdn(db.getCashBankOutNoData());
                        profile.setBankDetails(db.getBankDetailData());*/
                        Log.e("setMobileNumber1","..."+profile.getMobileNumber());
                        profile.setSecureToken(AppPreference.getAuthTokem(NewRegistrationActivity1.this));
                        new HttpNewRequest(profile, this, this, Constant.APP_SERVER_UPDATE_PROFILE).start();
                    } else {
                        try {

                            if(profile.getMobileNumber().startsWith("0095095"))
                            {
                                String number=profile.getMobileNumber();
                                String [] starr_mno=number.split("0095095");
                                Log.e("setMobileNumber","..."+profile.getMobileNumber());

                                showToast("You have entered Wrong Number");
                                return;
                            }
                        }catch(Exception e){
                            e.printStackTrace();
                        }


                        new HttpNewRequest(profile, this, this, Constant.APP_SERVER_CREATE_PROFILE).start();
                    }
                } else
                    Utils.showCustomToastMsg(this, R.string.async_task_no_internet, false);
            } else {
                showToastPopUp(R.string.accept_terms_conditions);
            }
        }
    }

    private boolean isValid(String userName, String DOB, String fatherName, String nrcNo, String IDType, String otherIDNo, String password, String confirmPassword, String Email, boolean isValidationBackground) {

        String errorMessage = getResources().getString(R.string.pls_enter_your) + "\n";
        String oldErrorMessage = getResources().getString(R.string.pls_enter_your) + "\n";

        if (!(mPersonalAccount.isChecked() || mBusinessAccount1.isChecked() || mBusinessAccount.isChecked()) && isProfileUpdate != 1) {
            errorMessage = errorMessage + getResources().getString(R.string.business_type) + ",\n";
        }
        if (Utils.isEmpty(userName)) {
            errorMessage = errorMessage + getResources().getString(R.string.name) + ",\n";
        }
        if (Utils.isEmpty(DOB)) {
            errorMessage = errorMessage + getResources().getString(R.string.dob) + ",\n";
        }
        if (mUserPic.getDrawable() == null && isProfileUpdate != 1) {
            errorMessage = errorMessage + getResources().getString(R.string.take_your_photo) + ",\n";
        }
        if (Utils.isEmpty(fatherName)) {
            errorMessage = errorMessage + getResources().getString(R.string.father_name) + ",\n";
        }
        if (mBusinessAccount.isChecked() || mBusinessAccount1.isChecked()) {
            if (mCategoryPos == -1) {
                errorMessage = errorMessage + getResources().getString(R.string.select_business_category) + ",\n";
            }
            if (mSubCategoryPos == -1) {
                errorMessage = errorMessage + getResources().getString(R.string.select_business_subcategory) + ",\n";
            }
            if (Utils.isEmpty(nrcNo) && Utils.isEmpty(otherIDNo)) {
                errorMessage = errorMessage + getString(R.string.id_info) + ",\n";
            }
        }
        if (!Utils.isEmpty(DOB)) {
            if (Utils.getAge(DOB) && mPersonalAccount.isChecked()) {
               /* if (Utils.isEmpty(nrcNo) && Utils.isEmpty(otherIDNo)) {
                    errorMessage = errorMessage + getString(R.string.nrc_or_passport) + ",\n";
                } else if (!IDType.equals(getResources().getString(R.string.Passport))) {
                    if (!Utils.isEmpty(otherIDNo))
                        errorMessage = errorMessage + getString(R.string.nrc_or_passport) + ",\n";
                }*/

                if (Utils.isEmpty(nrcNo) && Utils.isEmpty(otherIDNo)) {
                    errorMessage = errorMessage + getString(R.string.id_info) + ",\n";
                }
            }
        }

        if (Utils.isEmpty(password) && isProfileUpdate != 1) {
            errorMessage = errorMessage + getResources().getString(R.string.password) + ",\n";
        }
        if (Utils.isEmpty(confirmPassword) && isProfileUpdate != 1) {
            errorMessage = errorMessage + getResources().getString(R.string.Confirm_Password) + ",\n";
        }
        if (Email.length() > 0) {
            String[] emails = Email.split(",");
            if(emails.length>0) {
                if (!Utils.isValidEmail(emails[0].trim())) {
                    errorMessage = errorMessage + getResources().getString(R.string.valid_email) + ",\n";
                }
            }
        }
        if (oldErrorMessage.equalsIgnoreCase(errorMessage)) {
            return true;
        } else {
            if (isValidationBackground) {
                showAlert(errorMessage.substring(0, errorMessage.length() - 2));
                chIAccept.setChecked(false);
            }
            return false;
        }
    }

    private void setNotification(int msgID, String isAnim) {
        isWindowOpen = true;
        String msg = getResources().getString(msgID);
        WindowManager.LayoutParams layOutParams = new WindowManager.LayoutParams(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                        | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                        | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                PixelFormat.TRANSLUCENT);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        windwoView = inflater.inflate(R.layout.error_pop_up, null);
        tv = (CustomTextView) windwoView.findViewById(R.id.text);
        gif = (GifImageView) windwoView.findViewById(R.id.gif_anim);
        LinearLayout llNotification = (LinearLayout) windwoView.findViewById(R.id.ll_notification);
        llNotificationAnim = (ScrollView) windwoView.findViewById(R.id.ll_notification_anim);
        llNotificationAnimPink = (LinearLayout) windwoView.findViewById(R.id.ll_notification_pink);

        llNotification.setVisibility(View.VISIBLE);
        layOutParams.gravity = Gravity.TOP;

        wm.addView(windwoView, layOutParams);
        tv.setText(msg);
        animationView(isAnim);
    }

    private void cancelNotification() {
        Timer timer = new Timer();
        final Handler handler = new Handler();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (isWindowOpen) {
                            wm.removeView(windwoView);
                            isWindowOpen = false;
                        }

                    }
                });
            }
        };
        timer.schedule(timerTask, 0);
    }

    private void cancelNotificationWithAnimation() {
        Timer timer = new Timer();
        final Handler handler = new Handler();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {

                        Animation animFadeIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);
                        llNotificationAnim.startAnimation(animFadeIn);
                        animFadeIn.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                if (isWindowOpen) {
                                    wm.removeView(windwoView);
                                    isWindowOpen = false;
                                }
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });
                    }
                });
            }
        };
        timer.schedule(timerTask, 5000);
    }

    private void showToastPopUp(int msgID) {
        if (!isWindowOpen) {
            setNotification(msgID, "NONE");
            cancelNotificationWithAnimation();
        }
    }

    @Override
    public void onBackPressed() {
        if (isProfileUpdate == 1) {
            finish();
        } else {
            if (isWindowOpen) {
                cancelNotification();
            } else {
                timerForBack();
            }
            if (isBack && isBackCount >= 2) {
                finish();
            }
        }

    }

    private void setDataIntoProfile(String userName, String fatherName, String nrcNo, String recomendedBy, String predefineNo, String DOB) {
        profile = new Profile();
        profile.setLanguage(AppPreference.getLanguage(this).toUpperCase(Locale.UK));
        //  profile.setAgentAuthCode(mAgentCode.getText().toString());
        if (mUserPic.getDrawable() != null) {
            Bitmap bitmapImage = ((BitmapDrawable) mUserPic.getDrawable()).getBitmap();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmapImage.compress(Bitmap.CompressFormat.JPEG, 80, stream);
            byte[] byteArrayImage = stream.toByteArray();
           // profile.setProfilePic(Base64.encodeToString(byteArrayImage, Base64.DEFAULT)); RAJESH COMMENT FOR TESTING
            profile.setProfilePic("");
        }
        if (mSignaturePad.getVisibility() == View.VISIBLE) {
            Bitmap signatureBitmap = mSignaturePad.getTransparentSignatureBitmap();
            int nh = (int) (signatureBitmap.getHeight() * (200.0 / signatureBitmap.getWidth()));
            Bitmap scaled = Bitmap.createScaledBitmap(signatureBitmap, 200, nh, true);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            scaled.compress(Bitmap.CompressFormat.JPEG, 5, stream);
            byte[] byteArray = stream.toByteArray();
       //     profile.setsignature(Base64.encodeToString(byteArray, Base64.DEFAULT)); RAJESH COMMENT FOR TESTING

            profile.setsignature("");
        }

        if (isSigned) {
            Bitmap signatureBitmap = ((BitmapDrawable) mSignatureImage.getDrawable()).getBitmap();
            int nh = (int) (signatureBitmap.getHeight() * (200.0 / signatureBitmap.getWidth()));
            Bitmap scaled = Bitmap.createScaledBitmap(signatureBitmap, 200, nh, true);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            scaled.compress(Bitmap.CompressFormat.PNG, 5, stream);
            byte[] byteArray = stream.toByteArray();
            profile.setsignature(Base64.encodeToString(byteArray, Base64.DEFAULT));

        }

        if (mBusinessAccount.isChecked() || mBusinessAccount1.isChecked())
            profile.setAccountType(0);//Business Account
        else if (mPersonalAccount.isChecked())
            profile.setAccountType(1);// Personal Account


        if (mCategoryPos < 10 && mCategoryPos >= 0) {
            profile.setBusinessCategory("0" + String.valueOf(mCategoryPos));
            //profile.setBusinessCategory("SpeedKyat");
        } else {
            profile.setBusinessCategory(String.valueOf(mCategoryPos));
            //profile.setBusinessCategory("SpeedKyat");
        }

        if (mSubCategoryPos < 10 && mSubCategoryPos >= 0) {
            profile.setBusinessType("0" + String.valueOf(mSubCategoryPos));
            //profile.setBusinessType("SpeedKyat");
        } else {
            profile.setBusinessType(String.valueOf(mSubCategoryPos));
            //profile.setBusinessType("SpeedKyat");
        }

        if (mSubCategoryPos == -1) {
            profile.setBusinessType("");
        }
        if (mCategoryPos == -1) {
            profile.setBusinessCategory("");
        }

        profile.setGender(gender);
        profile.setKickback(true);
        profile.setName(mPrefixNameText.getText().toString() + "," + userName);
        profile.setDateOfBirth(DOB);
        profile.setFather(mFatherNamePrefix.getText().toString() + "," + fatherName);
        profile.setCountry(countryName);
        profile.setCountryCode(countryCode);
        profile.setCodeAlternate(countryCodePredefine);

        try {
            if (Utils.isEmpty(nrcNo)) {
                String nrc = mOtherIdEdit.getText().toString().replaceAll("/", "@");
                if (nrc.length() != 0) {
                    profile.setIDType(mOtherIdPrefix.getText().toString());
                    profile.setNRC(nrc);
                } else {
                    profile.setIDType("NRC");
                    profile.setNRC("NA");
                }
            } else {
                profile.setIDType("NRC");
                profile.setNRC(nrcNo.replaceAll("/", "@"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        getIDType();
        profile.setState(mDivisionId);
        profile.setTownship(cityCode);
        profile.setMobileNumber(mobileNo);
        profile.setParentAccount(Utils.getUniversalFormattedMobileNO(this, merchant_mobileno.getText().toString().trim()));

        if (recomendedBy.length() == 0) {
            profile.setCodeRecommended(countryCode);
            profile.setRecommended(mobileNo);
        } else {
            profile.setCodeRecommended(countryCodeRecommended);
            profile.setRecommended(recomendedBy);
        }

        profile.setPhone(predefineNo);
        profile.setEncrypted(Utils.getEncryptionValue(this));
        profile.setIMEI(Utils.getDeviceIMEI(this));

        profile.setDeviceId(Utils.getAndroidDeviceUniqueID(this));
        profile.setPhoneModel(Utils.getPhoneModel());
        profile.setPhonebrand(Utils.getPhoneBrand());
        profile.setOsVersion(Utils.getOSVersion());

        profile.setSimID(Utils.simSerialNumber(this));
        Log.v("SIMID_MOBILE", profile.getSimID() + "," + profile.getMobileNumber());
        profile.setAppID(Utils.getVersionCode(this));
        profile.setGcmID("");
        profile.setMSID(Utils.getSimSubscriberID(this));
        profile.setOSType(0);// Android 0 iOS 1
      /*  if (profile.getAccountType() == 1)
            profile.setlType("SUBSBR");
        else
            profile.setlType("MER");*/
        profile.setlType("DUMMY");
        profile.setPassword(mPassword.getText().toString());
        profile.setLatitude(latitude);
        profile.setLongitude(longitude);

        profile.setEmailID(mEmail.getText().toString());
        profile.setFBEmailId(mFacebook.getText().toString());
        RadioButton radioButton = (RadioButton) findViewById(mAddressType.getCheckedRadioButtonId());
        String addressType = radioButton.getText().toString();
        if (!(addressType.equalsIgnoreCase(getResources().getString(R.string.home_text)) || addressType.equalsIgnoreCase(getResources().getString(R.string.business_text)))) {
            addressType = mEtAdressType.getText().toString();
        }
        profile.setBusinessName(mBusinessName.getText().toString());
        profile.setAddressType(addressType);
        profile.setCellTowerID(cellId);
        if (profile.getPassword().length() == 0)
            profile.setPassword(AppPreference.getPasword(this));
        SharedPreferences.Editor editor = sharedPref.edit();
        //editor.putString("PREFIX_NAME", mPrefixNameText.getText().toString());
        editor.putString("PASSWORD", mPassword.getText().toString());
        editor.putString("CONFIRM_PASSWORD", mConfirmPassword.getText().toString());
        editor.putString("CONFIRM_PREDEFINE_NO", mConfirmPredefineNo.getText().toString());
        editor.putString("CONFIRM_RECOMMENDED", mConfirmRecommendedBy.getText().toString());
        editor.putBoolean("IS_READ", cbTerm.isChecked());
       /* editor.putString("PREDEFINE_COUNTRY_CODE", countryCodePredefine);
        editor.putString("RECOMMENDED_COUNTRY_CODE", countryCodeRecommended);
        editor.putString("PREDEFINE_COUNTRY_NAME", countryNamePredefine);
        editor.putString("RECOMMENDED_COUNTRY_NAME", countryNameRecommended);
        editor.putInt("PREDEFINE_FLAG", flagPredefine);
        editor.putInt("RECOMMENDED_FLAG", flagRecommended);*/
        editor.putString("RECOMMENDED_NAME", mRecommdedByDisplayName.getText().toString());
        editor.putString("PREDEFINE_NAME", mPredefineNoDisplayName.getText().toString());
        editor.putBoolean("IS_NON_MANDATORY", cbNonMandatory.isChecked());
        editor.putString("MAIN_CATEGORY", mBusinessCategory.getText().toString());
        editor.putString("SUB_CATEGORY", mBusinessSubCategory.getText().toString());

        //editor.putBoolean("Kickback", true);
        editor.putInt("CAT_POS", mCategoryPos);
        editor.putInt("SUB_CAT_POS", mSubCategoryPos);

        // if(countryCode.equals("+95")){

        editor.putString("DIVISION_NAME", getDivisionAndState());
        editor.putString("CITY_NAME", mTownship.getText().toString());
        profile.setAddress1(mCity.getText().toString());
        profile.setAddress2(mNoOrRoadorWard.getText().toString());

        // }
        /*else{

            editor.putString("DIVISION_NAME", mStateProvice.getText().toString());
            editor.putString("CITY_NAME", mCityArea.getText().toString());
            profile.setAddress1(mStreetNo.getText().toString());
            profile.setAddress2(mPostalCode.getText().toString());
        }
*/
        if (isMySelf != -1) {
            editor.putInt("IS_MYSELF", isMySelf);
        } else {
            editor.putInt("IS_MYSELF", isMySelf);
        }
        editor.apply();
    }

    private void timerForBack() {

        new CountDownTimer(5000, 1000) {

            public void onTick(long millisUntilFinished) {
                isBack = true;
                isBackCount++;
            }

            public void onFinish() {
                isBack = false;
                isBackCount = 0;
            }

        }.start();
    }

    private void setDataOnPreProfileData(Profile preProfile) {
        cityCode = preProfile.getTownship();
        mDivisionId = preProfile.getState();
        gender = preProfile.getGender();

        String agentCode = preProfile.getAgentAuthCode();
        if (agentCode.length() > 0) {
            //cbAgentCode.setChecked(true);
            mAgentCode.setVisibility(View.VISIBLE);
            mAgentCode.setText(agentCode);
        }

        Log.v("pCountryCode", preProfile.getCodeAlternate());
        String[] predefineNoFlagAndCode = getCountryCodeFlagNumberLogin(preProfile.getCodeAlternate(), preProfile.getPhone());
        countryCodePredefine = predefineNoFlagAndCode[0];
        countryNamePredefine = predefineNoFlagAndCode[1];
        flagPredefine = Integer.parseInt(predefineNoFlagAndCode[2]);
        String predefinedNumber = predefineNoFlagAndCode[3];

        Log.v("pRecommended", preProfile.getCodeRecommended());
        String[] recommendedNoFlagAndCode = getCountryCodeFlagNumberLogin(preProfile.getCodeRecommended(), preProfile.getRecommended());
        countryCodeRecommended = recommendedNoFlagAndCode[0];
        countryNameRecommended = recommendedNoFlagAndCode[1];
        flagRecommended = Integer.parseInt(recommendedNoFlagAndCode[2]);
        String recomendedNumber = recommendedNoFlagAndCode[3];

        Log.v("recommended1", recomendedNumber);
        if (AppPreference.getLanguage(this).equalsIgnoreCase("my")) {
            townShipList = db.getSELECTED_TOWNSHIP_NAME_BURMESE(mDivisionId);
            mTownShipModelListNew = db.getTownList(mDivisionId);
        } else {
            townShipList = db.getSELECTED_TOWNSHIP_NAME(mDivisionId);
            mTownShipModelListNew = db.getTownList(mDivisionId);
        }
        mDivisionIDText.setText(sharedPref.getString("DIVISION_ID", ""));

        if (sharedPref.getBoolean("IS_NON_MANDATORY", false)) {
            llIDProof.setVisibility(View.VISIBLE);
            cbNonMandatory.setChecked(true);
        }

        try {
            String[] details = getCountryCodeNameAndFlagFromNumber(preProfile.getParentAccount());
            merchant_mobileno.setText(details[3]);
        }catch (Exception e){}
        mDivisionIDText.setText(sharedPref.getString("DIVISION_ID", ""));

        if (sharedPref.getBoolean("IS_NON_MANDATORY", false)) {
            llIDProof.setVisibility(View.VISIBLE);
            cbNonMandatory.setChecked(true);
        }

        if (!Utils.isEmpty(preProfile.getName())) {
            String[] name = preProfile.getName().split(",");
            if (name.length >= 2) {
                mMale.setVisibility(View.GONE);
                mFemale.setVisibility(View.GONE);
                mMaleFemaleRegfix.setVisibility(View.GONE);
                mFemaleRegfixLinear.setVisibility(View.GONE);
                mLinearUserName.setVisibility(View.VISIBLE);
                mPrefixNameText.setText(name[0]);
                muserNameEdit.setText(name[1]);
                mDateOfBirth.setVisibility(View.VISIBLE);
                llFatherName.setVisibility(View.VISIBLE);
            }
        }

        String fatherName = preProfile.getFather();
        if (!Utils.isEmpty(fatherName)) {
            String[] name = fatherName.split(",");
            if (name.length >= 2) {
                mFatherNamePrefix.setText(name[0]);
                mFatherName.setText(name[1]);
            }
        }

        if(isProfileUpdate==1) {
            mBusinessAccount1.setVisibility(View.GONE);
            mBusinessAccount.setText(R.string.update_personal_business_account);
        }
        if (preProfile.getAccountType() == 0) {
            mBusinessAccount.setChecked(true);//Business Account
            mBusinessCategory.setVisibility(View.VISIBLE);
            mLinearBusinessCategory.setVisibility(View.VISIBLE);
        } else if (preProfile.getAccountType() == 1) {//personal Account
            mPersonalAccount.setChecked(true);
        }

        BusinessCategoryModel model = new BusinessCategoryModel().findBusinessCategoryAndSubCategory(preProfile.getBusinessCategory(), preProfile.getBusinessType(), this);
        if (model != null && preProfile.getAccountType() == 0) {
            int category = Integer.parseInt(preProfile.getBusinessCategory());
            if (model.getMainCategory().length() > 0)
                mBusinessCategory.setText(model.getMainCategory());
            else
                mBusinessCategory.setText(getResources().getString(R.string.business_category));

            if (model.getListOfSubCategory().size() > 0) {
                mBusinessSubCategory.setText(model.getListOfSubCategory().get(0));
                imgBusinessSubcategory.setText(model.getListOfSubCategoryEmoji().get(0));
                imgBusinessName2.setText(model.getListOfSubCategoryEmoji().get(0));
                if (category != -1) {
                    int categoryImageID = Constant.listOfCategoryImages[category];
                    imgBusinessName1.setBackgroundResource(categoryImageID);
                    mBusinessCategory.setCompoundDrawablesWithIntrinsicBounds(categoryImageID, 0, R.drawable.arrow1, 0);
                }
            } else {
                mBusinessSubCategory.setText(getResources().getString(R.string.business_subcategory));
                imgBusinessSubcategory.setText(null);
                imgBusinessName2.setText(null);
            }

            llBusinessSubcategory.setVisibility(View.VISIBLE);
            llBusinessName.setVisibility(View.VISIBLE);

        }

        mDateOfBirth.setText(preProfile.getDateOfBirth());
        if (preProfile.getNRC().equalsIgnoreCase("NA")) {//TODO Id ne not set the ID proof and come back to again registration we need to allow him to enter the ID Changed by Boss @Abhishek
            mNoID.setChecked(false);
            mNRCEdittext.setVisibility(View.VISIBLE);
            mOtherId.setVisibility(View.VISIBLE);
        } else if (preProfile.getIDType().equalsIgnoreCase("01")) {
            mNRCEdittext.setText(preProfile.getNRC().replaceAll("@", "/"));
            mOtherId.setVisibility(View.GONE);
        } else {
            mLinearLyoutd.setVisibility(View.VISIBLE);
            mOtherIdEdit.setText(preProfile.getNRC().replaceAll("@", "/"));
            //mOtherIdPrefix.setText(preProfile.getIDType());
            mNRCEdittext.setVisibility(View.GONE);
            mOtherId.setVisibility(View.GONE);
            setIDType();
        }

        if (preProfile.getNRC().equalsIgnoreCase(getResources().getString(R.string.None))) {
            mOtherIdPrefix.setText(null);
            mOtherIdPrefix.setVisibility(View.GONE);
            mOtherIdEdit.setEnabled(false);
        }

        if (countryCodePredefine.equalsIgnoreCase("+95")) {
            mPredefinedNumber.setText(getNumberWithGreyZero(predefinedNumber, true));
            mConfirmPredefineNo.setText(getNumberWithGreyZero(sharedPref.getString("CONFIRM_PREDEFINE_NO", ""), true));

        } else {
            mPredefinedNumber.setText(predefinedNumber);
            mConfirmPredefineNo.setText(sharedPref.getString("CONFIRM_PREDEFINE_NO", ""));
        }
        if (predefinedNumber.length() != 0) {
            isPredefineNoEditable = true;
        }
        // if(countryCode.equals("+95")) {
        llAddressMyanmar.setVisibility(View.VISIBLE);
        llAddressDefault.setVisibility(View.GONE);
        Log.v("Div and Town", mDivisionId + " " + cityCode);

        String[] divAndIsState = db.getDivisionORStateNameFromID(mDivisionId);
        String[] townshipName = db.getTownshipNameFromID(cityCode);
        if (divAndIsState.length >= 3 && townshipName.length >= 1) {
            if (AppPreference.getLanguage(this).equalsIgnoreCase("my")) {
                if (divAndIsState[1].equals("1")) {
                    mDivision.setText(divAndIsState[2]);
                    mState.setVisibility(View.GONE);

                } else if (divAndIsState[1].equals("0")) {
                    mState.setText(divAndIsState[2]);
                    mDivision.setVisibility(View.GONE);
                }
                mTownship.setText(townshipName[1]);
            } else {
                if (divAndIsState[1].equals("1")) {
                    mDivision.setText(divAndIsState[0]);
                    mState.setVisibility(View.GONE);

                } else if (divAndIsState[1].equals("0")) {
                    mState.setText(divAndIsState[0]);
                    mDivision.setVisibility(View.GONE);
                }
                mTownship.setText(townshipName[0]);
            }
            mTownship.setVisibility(View.VISIBLE);
            mCity.setText(preProfile.getAddress1());
            mNoOrRoadorWard.setText(preProfile.getAddress2());
            if (!isEdit) {
                mNoOrRoadorWard.setEnabled(true);
                mCity.setEnabled(true);
            }
            mCity.setVisibility(View.VISIBLE);
            mNoOrRoadorWard.setVisibility(View.VISIBLE);

            //}

        }/*else{
            llAddressMyanmar.setVisibility(View.GONE);
            llAddressDefault.setVisibility(View.VISIBLE);
            mPostalCode.setText(preProfile.getAddress1());
            mStreetNo.setText(preProfile.getAddress2());
            mCityArea.setText(sharedPref.getString("CITY_NAME", ""));
            mStateProvice.setText(sharedPref.getString("DIVISION_NAME", ""));
        }*/


        mCountryCodePredefine.setText(countryNamePredefine + " (" + countryCodePredefine + ")");
        tvCountryCodePredefine.setText("(" + countryCodePredefine + ")");
        mCountryCodePredefineConfirm.setText("(" + countryCodePredefine + ")");
        mCountryImagePredefine.setImageResource(flagPredefine);

        mCountryCodeRecommended.setText(countryNameRecommended + " (" + countryCodeRecommended + ")");
        tvCountryCodeRecommended.setText("(" + countryCodeRecommended + ")");
        mCountryCodeRecommendedConfirm.setText("(" + countryCodeRecommended + ")");
        mCountryImageRecommended.setImageResource(flagRecommended);


        if (!Utils.isEmpty(sharedPref.getString("PREDEFINE_NAME", ""))) {
            mPredefineNoDisplayName.setVisibility(View.VISIBLE);
            mPredefineNoDisplayName.setText(sharedPref.getString("PREDEFINE_NAME", ""));
            llPredefineConfirm.setVisibility(View.GONE);
            mPredefinedNumber.setFocusable(false);

        }

        mBusinessName.setText(preProfile.getBusinessName());
        mPassword.setText(preProfile.getPassword());
        mConfirmPassword.setText(sharedPref.getString("CONFIRM_PASSWORD", ""));

        if (Utils.isEmpty(preProfile.getPassword()))
            mConfirmPassword.setVisibility(View.GONE);
        else
            mConfirmPassword.setVisibility(View.VISIBLE);

        // String addressType = sharedPref.getString("ADDRESS_TYPE", "");
        String addressType = preProfile.getAddressType();
        if (addressType.equalsIgnoreCase(getResources().getString(R.string.home_text))) {
            RadioButton rb = (RadioButton) mAddressType.getChildAt(0);
            rb.setChecked(true);
        } else if (addressType.equalsIgnoreCase(getResources().getString(R.string.business_text))) {
            RadioButton rb = (RadioButton) mAddressType.getChildAt(1);
            rb.setChecked(true);
        } else {
            RadioButton rb = (RadioButton) mAddressType.getChildAt(2);
            rb.setChecked(true);
            mEtAdressType.setText(addressType);
        }

        try {
            if (!Utils.isEmpty(preProfile.getProfilePic())) {
                byte[] decodedBytes = Base64.decode(preProfile.getProfilePic(), Base64.DEFAULT);
                Bitmap decodedBitmap = BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
                int nh = (int) (decodedBitmap.getHeight() * (100.0 / decodedBitmap.getWidth()));
                Bitmap scaled = Bitmap.createScaledBitmap(decodedBitmap, 100, nh, true);
                mUserPic.setImageBitmap(scaled);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        isMySelf = sharedPref.getInt("IS_MYSELF", -1);
        if (sharedPref.getInt("IS_MYSELF", -1) == 0) {
            llRecommendedByAll.setVisibility(View.GONE);
            //mRecommendedBy.setText(preProfile.getRecommended());
            mRecommendedBy.setText(recomendedNumber);
            Log.v("recommended1", recomendedNumber);

        } else if (sharedPref.getInt("IS_MYSELF", -1) == 1) {
            if (countryCodeRecommended.equalsIgnoreCase("+95")) {
                Log.v("recommended3", recomendedNumber);
                mRecommendedBy.setText(getNumberWithGreyZero(recomendedNumber, true));
                mConfirmRecommendedBy.setText(getNumberWithGreyZero(sharedPref.getString("CONFIRM_RECOMMENDED", ""), true));
            } else {
                mRecommendedBy.setText(recomendedNumber);
                mConfirmRecommendedBy.setText(sharedPref.getString("CONFIRM_RECOMMENDED", ""));
                Log.v("recommended2", recomendedNumber);
            }

            if (!Utils.isEmpty(sharedPref.getString("RECOMMENDED_NAME", ""))) {
                llRecommendedConfirm.setVisibility(View.GONE);
                mRecommdedByDisplayName.setVisibility(View.VISIBLE);
            }
            llRecommendedByAll.setVisibility(View.VISIBLE);
            mRecommendedBy.setFocusableInTouchMode(true);
            mRecommendedBy.setFocusable(true);
            mConfirmRecommendedBy.setFocusable(true);
            mRecommendedBy.setOnClickListener(null);

        } else if (!Utils.isEmpty(recomendedNumber)) {
            llRecommendedByAll.setVisibility(View.VISIBLE);
            if (countryCodeRecommended.equalsIgnoreCase("+95")) {
                mRecommendedBy.setText(getNumberWithGreyZero(recomendedNumber, true));
                mConfirmRecommendedBy.setText(getNumberWithGreyZero(sharedPref.getString("CONFIRM_RECOMMENDED", ""), true));
            } else {
                mRecommendedBy.setText(recomendedNumber);
                mConfirmRecommendedBy.setText(sharedPref.getString("CONFIRM_RECOMMENDED", ""));
            }

            if (!Utils.isEmpty(sharedPref.getString("RECOMMENDED_NAME", ""))) {
                llRecommendedConfirm.setVisibility(View.GONE);
                mRecommdedByDisplayName.setText(sharedPref.getString("RECOMMENDED_NAME", ""));
                mRecommdedByDisplayName.setVisibility(View.VISIBLE);
            }
            mRecommendedBy.setFocusable(true);
            mConfirmRecommendedBy.setFocusable(true);
            mRecommendedBy.setOnClickListener(null);

            isRecommendedNoEditable = true;

        }
        String emailIDs = preProfile.getEmailID();
        if (!Utils.isEmpty(emailIDs)) {
            mEmail.setText(emailIDs);
        } else {
            mEmail.setText(AppPreference.getDefaultEmail(this));
        }
        String FBEmailIDs = preProfile.getFBEmailId();
        if (!Utils.isEmpty(FBEmailIDs)) {
            mFacebook.setText(FBEmailIDs);
        } else {
            mFacebook.setText(AppPreference.getFacebookID(this));
        }

        try {
            if (preProfile.getBusinessType().length() == 0) {
                mSubCategoryPos = -1;
            } else {
                mSubCategoryPos = Integer.parseInt(preProfile.getBusinessType());
            }
            if (preProfile.getBusinessCategory().length() == 0) {
                mCategoryPos = -1;
            } else {
                mCategoryPos = Integer.parseInt(preProfile.getBusinessCategory());
            }
        } catch (Exception e) {
            mSubCategoryPos = mCategoryPos = -1;
        }

    }

    private class MyTextWatcherForPredefineNo implements TextWatcher {
        public void afterTextChanged(Editable s) {

        }
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            if (isProfileUpdate != 1)
                llPredefineConfirm.setVisibility(View.VISIBLE);

            if (mMobileNo.getText().toString().equalsIgnoreCase(s.toString())) {
                Utils.showCustomToastMsg(NewRegistrationActivity1.this, R.string.error_predifine_no, false);
                mPredefinedNumber.setText(null);
                mPredefineNoDisplayName.setText(null);
                return;
            }

            // mConfirmPredefineNo.setText("");

            /*if (s.toString().length() > 2) {
                cancelNotification();
            } else if (!isWindowOpen && !isResetClicked) {
                setNotification(R.string.predefined_popup_text, "PREDEFINE_NO");
            }*/
            if (isResetClicked) {
                isResetClicked = false;
                isPredefineTextChanged = true;
            }
        }
    }

    private class MyTextWatcherForRecommended implements TextWatcher {

        public void afterTextChanged(Editable s) {

        }
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {

            if (mMobileNo.getText().toString().equalsIgnoreCase(s.toString()) && (mRecommendedBy.getVisibility() == View.VISIBLE)) {
                showToastPopUp(R.string.error_recommeded_by);
                return;
            }
            mConfirmRecommendedBy.setText("");

            if (s.toString().length() > 2) {
                cancelNotification();
            } else if (!isWindowOpen && !isResetClickedRecommended) {
                setNotification(R.string.error_recommeded_by, "NONE");
            }

            if (isResetClickedRecommended)
                isResetClickedRecommended = false;
        }
    }

    private class MyTextWatcherForPassword implements TextWatcher {

        public void afterTextChanged(Editable s) {

        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (!mPassword.getText().toString().startsWith(s.toString())) {
                showToastPopUp(R.string.error_confirm_password);
                mConfirmPassword.setText(null);
                isShown = false;
                //return;
            } else {
                if (isWindowOpen && isShown) {
                    cancelNotification();
                }
            }

        }
    }

    private class MyTextWatcherForOPassword implements TextWatcher {

        public void afterTextChanged(Editable s) {

        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (isProfileUpdate != -1) {
                mConfirmPassword.setVisibility(View.GONE);
            } else {
                mConfirmPassword.setText("");
                mConfirmPassword.setVisibility(View.VISIBLE);
            }
            /*if (isPredefineTextChanged)
                return;*/

            if (s.toString().length() > 2) {
                cancelNotification();
            } else if (!isWindowOpen && !isResetClickedPassword) {
                setNotification(R.string.pass_popup_text, "NONE");
            }
            if (isResetClickedPassword)
                isResetClickedPassword = false;
        }
    }

    private class MyTextWatcherForConfirmRecommended implements TextWatcher {
        public void afterTextChanged(Editable s) {

        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {

            if (!mRecommendedBy.getText().toString().startsWith(s.toString())) {
                showToastPopUp(R.string.error_recommended_no_confirmation);
                mConfirmRecommendedBy.setText(null);
                //return;
            }

        }
    }

    private class MyTextWatcherForConfirmPredefine implements TextWatcher {

        public void afterTextChanged(Editable s) {

        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {

            if (!mPredefinedNumber.getText().toString().startsWith(s.toString())) {
                showToastPopUp(R.string.error_predefine_no_confirmation);
                mConfirmPredefineNo.setText(null);
                //return;
            }

        }
    }

    private class MyTextWatcherForName implements TextWatcher {

        public void afterTextChanged(Editable s) {

        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {

            if (s.toString().length() > 2) {
                cancelNotification();
            } else if (!isWindowOpen && !isResetClicked) {
                setNotification(R.string.error_name_msg, "NONE");
            }
            if (isResetClicked)
                isResetClicked = false;
        }
    }

    private void animationView(String isAnim) {

        if (isAnim.equalsIgnoreCase("PREDEFINE_NO")) {
            gif.setVisibility(View.VISIBLE);
            gif.setBackgroundResource(R.drawable.predifine_no_gif);
        } else if (isAnim.equalsIgnoreCase("CAR")) {
            gif.setVisibility(View.VISIBLE);
            gif.setBackgroundResource(R.drawable.toll_gate);
        } else {
            gif.setVisibility(View.GONE);
        }
        Animation move = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move_down);
        llNotificationAnim.startAnimation(move);
        move.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

                final Animation animFadeOut = AnimationUtils.loadAnimation(getApplicationContext(),
                        R.anim.fad_out);
                final Animation animFadeIn = AnimationUtils.loadAnimation(getApplicationContext(),
                        R.anim.fade_in);
                llNotificationAnimPink.startAnimation(animFadeIn);
                animFadeOut.setAnimationListener(new Animation.AnimationListener()
                {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        llNotificationAnimPink.setBackgroundColor(getResources().getColor(R.color.pink));
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        llNotificationAnimPink.startAnimation(animFadeIn);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });

                animFadeIn.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        llNotificationAnimPink.setBackgroundColor(getResources().getColor(R.color.blue));
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        llNotificationAnimPink.startAnimation(animFadeOut);

                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

    }

    @Override
    protected void onStop() {
        if (isWindowOpen)
            cancelNotification();

        if (isProfileUpdate != 1) {
            if (!isRegistered) {
                String predefineNo = Utils.getUniversalFormattedNoWithoutPlus(Utils.getUniversalFormattedMobileNOWithCountryCode(countryCodePredefine, mPredefinedNumber.getText().toString()));
                String recomendedNo = Utils.getUniversalFormattedNoWithoutPlus(Utils.getUniversalFormattedMobileNOWithCountryCode(countryCodeRecommended, mRecommendedBy.getText().toString()));
                setDataIntoProfile(muserNameEdit.getText().toString(), mFatherName.getText().toString(), mNRCEdittext.getText().toString(), recomendedNo, predefineNo, mDateOfBirth.getText().toString());
                db.deleteProfile();
                db.insertProfile(profile);
            }
        }


        super.onStop();
    }

    private void languageReset() {
        if (isLanguageChanged) {
            changeLang("my");
            isLanguageChanged = false;
        }
    }

    private void showDatePicker() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, -5);
        final AlertDialog.Builder mDateTimeDialog = new AlertDialog.Builder(NewRegistrationActivity1.this);
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        final View view = inflater.inflate(R.layout.custom_date_picker, null);
        final DatePicker datePickerView = (DatePicker) view.findViewById(R.id.datePickerView);
        datePickerView.setDescendantFocusability(DatePicker.FOCUS_BLOCK_DESCENDANTS);
        datePickerView.updateDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        mDateTimeDialog.setView(view);
        mDateTimeDialog.setPositiveButton("Set", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                view.clearFocus();
                int day = datePickerView.getDayOfMonth();
                int month = datePickerView.getMonth();
                int year = datePickerView.getYear();

                if (Utils.isEmpty(muserNameEdit.getText().toString())) {
                    languageReset();
                    showToastPopUp(R.string.error_enter_name_first);
                    return;
                }
                Calendar calendar = Calendar.getInstance();
                calendar.set(year, month, day);
                Calendar preCalendar = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);
                calendar.set(Calendar.DAY_OF_MONTH, day);

                preCalendar.add(Calendar.YEAR, -5);

                if (calendar.getTimeInMillis() <= preCalendar.getTimeInMillis()) {
                    mDateOfBirth.setText(sdf.format(calendar.getTime()));
                    isAgelower18();
                    mFatherName.requestFocus();
                    languageReset();
                } else {
                    languageReset();
                    mDateOfBirth.setHint(getResources().getString(R.string.dob_text));
                    showToastPopUp(R.string.dob_error);
                    return;
                }

                preCalendar.add(Calendar.YEAR, +5);

                if (mPrefixName.equalsIgnoreCase(mMalePrefixData[0]) || mPrefixName.equalsIgnoreCase(mFemalePrefixData[0])) {
                    preCalendar.add(Calendar.YEAR, -30);
                    mDateOfBirth.setText(sdf.format(calendar.getTime()));
                    languageReset();
                    isAgelower18();
                    mFatherName.requestFocus();
                    if (!(calendar.getTimeInMillis() <= preCalendar.getTimeInMillis())) {
                        languageReset();
                        showToastPopUp(R.string.error_title_dob);
                    }
                    return;
                }

                if (mPrefixName.equalsIgnoreCase(mMalePrefixData[1])) {
                    preCalendar.add(Calendar.YEAR, -15);
                    mDateOfBirth.setText(sdf.format(calendar.getTime()));
                    isAgelower18();
                    mFatherName.requestFocus();
                    languageReset();
                    if (!(calendar.getTimeInMillis() >= preCalendar.getTimeInMillis())) {
                        languageReset();
                        showToastPopUp(R.string.error_title_dob);
                    }
                    return;
                }

                if (mPrefixName.equalsIgnoreCase(mFemalePrefixData[1])) {
                    preCalendar.add(Calendar.YEAR, -30);
                    mDateOfBirth.setText(sdf.format(calendar.getTime()));
                    languageReset();
                    isAgelower18();
                    mFatherName.requestFocus();
                    if (!(calendar.getTimeInMillis() >= preCalendar.getTimeInMillis())) {
                        languageReset();
                        showToastPopUp(R.string.error_title_dob);
                    }
                    return;
                }

                if (mPrefixName.equalsIgnoreCase(mMalePrefixData[mMalePrefixData.length - 1])) {
                    preCalendar.add(Calendar.YEAR, -22);
                    mDateOfBirth.setText(sdf.format(calendar.getTime()));
                    languageReset();
                    isAgelower18();
                    mFatherName.requestFocus();
                    if (!(calendar.getTimeInMillis() <= preCalendar.getTimeInMillis())) {
                        languageReset();
                        showToastPopUp(R.string.error_title_dob);
                    }
                    //return;
                }


            }
        });
        mDateTimeDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                languageReset();
            }
        });
        mDateTimeDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                languageReset();
            }
        });
        mDateTimeDialog.show();

    }

    private void disableProfile() {
        tvPass.setVisibility(View.GONE);

        mPredefinedNumber.setFocusable(false);
        mRecommendedBy.setFocusable(false);

        btnSubmit.setVisibility(View.GONE);
        mTitleOfHeader.setText(getResources().getString(R.string.update_profile));
        buttonLogout.setText(getResources().getString(R.string.edit));
        buttonLogout.setTextSize(16);
        muserNameEdit.setEnabled(false);
        mFatherName.setEnabled(false);
        mDateOfBirth.setEnabled(false);
        llTakePhoto.setEnabled(false);
        mPassword.setVisibility(View.GONE);
        mConfirmPassword.setVisibility(View.GONE);
        llTerms.setVisibility(View.GONE);
        rlSignatureBox.setVisibility(View.GONE);
        rlSignatureReset.setVisibility(View.GONE);
        llTakePhoto.setVisibility(View.GONE);
        mClear.setVisibility(View.INVISIBLE);
        mCarClear.setVisibility(View.INVISIBLE);
        mRecommendedClear.setVisibility(View.INVISIBLE);
        llPassword.setVisibility(View.GONE);

        isEdit = true;
        llNonMandatory.performClick();
        mClear.setEnabled(false);
        mBusinessAccount1.setEnabled(false);
        mBusinessAccount.setEnabled(false);
        mPersonalAccount.setEnabled(false);
        mBusinessCategory.setEnabled(false);
        mBusinessSubCategory.setEnabled(false);
        mBusinessName.setEnabled(false);
        mNRCEdittext.setEnabled(false);
        mOtherId.setEnabled(false);
        mOtherIdPrefix.setEnabled(false);
        mOtherIdEdit.setEnabled(false);
        mNrcClear.setEnabled(false);
        mAddressClear.setEnabled(false);
        mDivision.setEnabled(false);
        mTownship.setEnabled(false);
        mState.setEnabled(false);
        mCity.setEnabled(false);
        mNoOrRoadorWard.setEnabled(false);
        mEtAdressType.setEnabled(false);
        mAddressType.setEnabled(false);
        mCarClear.setEnabled(false);
        mRecommendedClear.setEnabled(false);
        mBusinessClear.setEnabled(false);

        llCountryCodePredefine.setEnabled(false);
        llPredefineConfirm.setEnabled(false);
        mConfirmPredefineNo.setEnabled(false);
        llCountryCodeRecommended.setEnabled(false);
        llRecommendedConfirm.setEnabled(false);
        mConfirmRecommendedBy.setEnabled(false);
        mEmail.setEnabled(false);
        mFacebook.setEnabled(false);
        mPredefinedNumber.setEnabled(false);
        mRecommendedBy.setEnabled(false);
        mContactPicker.setEnabled(false);
        mContactPickerRecom.setEnabled(false);
        mNoID.setEnabled(false);
        cbNonMandatory.setChecked(true);
        cbNonMandatory.setEnabled(false);
        llPredefineConfirm.setVisibility(View.GONE);
        llRecommendedConfirm.setVisibility(View.GONE);
        llAccept.setVisibility(View.GONE);

        mStateProvice.setEnabled(false);
        mCityArea.setEnabled(false);
        mStreetNo.setEnabled(false);
        mPostalCode.setEnabled(false);

        for (int i = 0; i < mAddressType.getChildCount(); i++) {
            View view = mAddressType.getChildAt(i);
            view.setEnabled(false);
        }
    }

    private void enableProfile() {
        buttonLogout.setVisibility(View.GONE);
        btnSubmit.setVisibility(View.VISIBLE);
        mTitleOfHeader.setText(getResources().getString(R.string.edit_profile));
        buttonLogout.setText(getResources().getString(R.string.Submit));
        buttonLogout.setTextSize(16);
        isEdit = false;
        cbNonMandatory.setEnabled(true);
        mBusinessCategory.setEnabled(true);
        mBusinessSubCategory.setEnabled(true);
        mBusinessName.setEnabled(true);
        mNRCEdittext.setEnabled(true);
        mOtherId.setEnabled(true);
        mOtherIdPrefix.setEnabled(true);
        mOtherIdEdit.setEnabled(true);
        mNrcClear.setEnabled(true);
        mAddressClear.setEnabled(true);
        mDivision.setEnabled(true);
        mTownship.setEnabled(true);
        mState.setEnabled(true);
        mCity.setEnabled(true);
        mNoOrRoadorWard.setEnabled(true);
        mEtAdressType.setEnabled(true);
        mAddressType.setEnabled(true);
        mBusinessClear.setEnabled(true);

        if (!isPredefineNoEditable) {
            llCountryCodePredefine.setEnabled(true);
            llPredefineConfirm.setEnabled(true);
            mConfirmPredefineNo.setEnabled(true);
            mPredefinedNumber.setEnabled(true);
            mCarClear.setEnabled(true);
            mContactPicker.setEnabled(true);
            mCarClear.setVisibility(View.VISIBLE);

            mPredefinedNumber.setFocusableInTouchMode(true);
        }
        if (!isRecommendedNoEditable) {
            llCountryCodeRecommended.setEnabled(true);
            llRecommendedConfirm.setEnabled(true);
            mConfirmRecommendedBy.setEnabled(true);
            mRecommendedBy.setEnabled(true);
            mRecommendedClear.setEnabled(true);
            mContactPickerRecom.setEnabled(true);
            mRecommendedClear.setVisibility(View.VISIBLE);

            mRecommendedBy.setFocusableInTouchMode(true);
        }

        mEmail.setEnabled(true);
        mFacebook.setEnabled(true);
        mRecommendedBy.setOnClickListener(this);
        mNoID.setEnabled(true);
        llAccept.setVisibility(View.VISIBLE);

        mStateProvice.setEnabled(true);
        mCityArea.setEnabled(true);
        mStreetNo.setEnabled(true);
        mPostalCode.setEnabled(true);

        for (int i = 0; i < mAddressType.getChildCount(); i++) {
            View view = mAddressType.getChildAt(i);
            view.setEnabled(true);
        }

        isAgelower18();
    }

    private void setAddressFormat() {

        if (!countryCode.equals("+95")) {
            llAddressMyanmar.setVisibility(View.GONE);
            llAddressDefault.setVisibility(View.VISIBLE);
            mCountry.setText(countryName);
            mCountryImage.setImageResource(flag);
        }
    }

    @Override
    public void onSuccess(GPS_Location response) {
        if (response != null) {
            if (!Utils.isEmpty(response.getStateCode()) && !Utils.isEmpty(response.getTownshipCode())) {
                mDivisionId = response.getStateCode();
                cityCode = response.getTownshipCode();

                mTownShipModelListNew = db.getTownList(mDivisionId);

                if (AppPreference.getLanguage(this).equals("my")) {
                    if (response.getIs_Divison()) {
                        mState.setVisibility(View.GONE);
                        mDivision.setText(response.getStateBurmese());
                    } else {
                        mDivision.setVisibility(View.GONE);
                        mState.setText(response.getStateBurmese());
                    }
                    mTownship.setText(response.getTownshipBurmese());
                    townShipList = db.getSELECTED_TOWNSHIP_NAME_BURMESE(mDivisionId);
                } else {
                    if (response.getIs_Divison()) {
                        mState.setVisibility(View.GONE);
                        mDivision.setText(response.getState());
                    } else {
                        mDivision.setVisibility(View.GONE);
                        mState.setText(response.getState());
                    }
                    mTownship.setText(response.getTownship());
                    townShipList = db.getSELECTED_TOWNSHIP_NAME(mDivisionId);
                }

                mTownship.setVisibility(View.VISIBLE);
                mCity.setVisibility(View.VISIBLE);
                mCity.setEnabled(true);
                mNoOrRoadorWard.setVisibility(View.VISIBLE);
                mNoOrRoadorWard.setEnabled(true);
                mCity.setText(response.getAddressLine1());
                mNoOrRoadorWard.setText(response.getAddressLine2());
            }
        }

    }

    @Override
    public void onDateSet(android.app.DatePickerDialog datePickerDialog, int year, int monthOfYear, int dayOfMonth) {

    }

    @Override
    public void onFail() {

    }

    private void getDefaultEmail() {

        String emails = "";
        AccountManager am = AccountManager.get(this);
        Account[] accounts = am.getAccounts();
        for (Account ac : accounts) {
            String acname = ac.name;
            String actype = ac.type;
            if (actype.equals("com.google") && acname.length() > 0) {
                emails = emails + acname + ",";

            }
        }
        if (emails.length() > 0)
            emails = emails.substring(0, emails.length() - 1);
        AppPreference.setDefaultEmail(this, emails);
    }

    private void getDefaulFacebook() {
        AccountManager am = AccountManager.get(this);
        Account[] accounts = am.getAccounts();
        for (Account ac : accounts) {
            String acname = ac.name;
            String actype = ac.type;
            if (actype.equals("com.facebook.auth.login")) {
                AppPreference.setFacebookID(this, acname);

                return;
            }
        }
    }

    @Override
    public void responseListener(String res, int resultCode) {
        Log.e("responseFromRegis",res);
        Log.e("resultcode","..."+resultCode);
        switch (resultCode)
        {


            case APP_SERVER_CREATE_PROFILE:
                try {
                    JSONObject jsonObject = new JSONObject(res);
                    String code = jsonObject.getString("Code");
                    String msg = jsonObject.getString("Msg");

                    if (code.equalsIgnoreCase("200")) {
                        Utils.playAudio(this, R.raw.app);
                        sharedPref.edit().clear().apply();
                        isRegistered = true;

                        db.deleteProfile();
                        db.insertProfile(profile);

                        AppPreference.setCountryImageID(this, AppPreference.getCountryImageIDLogin(this));
                        AppPreference.setCountryCodeFlagName(this, AppPreference.getCountryNameLogin(this), AppPreference.getCountryCodeLogin(this));
                        String mobileNo = Utils.getUniversalFormattedMobileNOLogin(NewRegistrationActivity1.this, mMobileNo.getText().toString());
                        AppPreference.setIsLoginFirstTime(this, false);
                        AppPreference.setIssRegister(NewRegistrationActivity1.this, true);
                        AppPreference.setMyMobileNo(getApplicationContext(), mobileNo);
                        AppPreference.setPasword(getApplicationContext(), mPassword.getText().toString());
                        AppPreference.setIsAppFirstTime(getApplicationContext(), true);
                        AppPreference.setLoginUserName(getApplicationContext(), muserNameEdit.getText().toString());
                        DBHelper.insertDeviceInfor(Utils.getDeviceIMEI(getApplicationContext()), Utils.simSerialNumber(NewRegistrationActivity1.this), mobileNo, Utils.getSimSubscriberID(NewRegistrationActivity1.this));
                        AppPreference.setMyMobileNoWithoutCountryCode(this, mMobileNo.getText().toString());
                        AppPreference.setSimId(getApplicationContext(), Utils.simSerialNumber(this));
                        Utils.showCustomToastMsg(this, R.string.congratulation_msg_register, true);

                        Intent call = new Intent(NewRegistrationActivity1.this, NewLoginStep2ActivityNew.class);
                        AppPreference.setMasterOrCashier(this, MASTER);
                        call.putExtra("mobileNo", mMobileNo.getText().toString());
                        call.putExtra("formtMobileNo", mobileNo);
                        startActivity(call);
                        finish();

                    } else if (code.equalsIgnoreCase("-1")) {
                        Utils.showCustomToastMsg(this, R.string.api_error2, false);
                    } else {
                        showToast(msg);
                    }
                } catch (Exception e) {
                    //showToast(e.toString);
                    e.printStackTrace();
                }
                break;
            case APP_SERVER_UPDATE_PROFILE:
                try {
                    JSONObject jsonObject1 = new JSONObject(res);
                    String code = jsonObject1.getString("Code");
                    String msg = jsonObject1.getString("Msg");
                    if (code.equalsIgnoreCase("200")) {
                        sharedPref.edit().clear().apply();
                        db.deleteProfile();
                        db.insertProfile(profile);
                        if (AppPreference.getUserType(this).equals("SUBSBR"))
                            Utils.showCustomToastMsg(this, R.string.update_profile_success, true);
                        else
                            Utils.showCustomToastMsg(this, R.string.update_profile_success_mer, true);
                        finish();
                    } else if (code.equalsIgnoreCase("-1")) {
                        Utils.showCustomToastMsg(this, R.string.api_error2, false);
                    } else {
                        showToast(msg);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    private void getIDType() {
        String IDType = profile.getIDType();
        if (IDType.equalsIgnoreCase(getResources().getString(R.string.None))) {
            profile.setIDType("00");
        } else if (IDType.equalsIgnoreCase("NRC")) {
            profile.setIDType("01");
        } else if (IDType.equalsIgnoreCase(getResources().getString(R.string.identity_college))) {
            profile.setIDType("02");
        } else if (IDType.equalsIgnoreCase(getResources().getString(R.string.companyId))) {
            profile.setIDType("03");
        } else if (IDType.equalsIgnoreCase(getResources().getString(R.string.Passport))) {
            profile.setIDType("04");
        } else if (IDType.equalsIgnoreCase(getResources().getString(R.string.drivingLinId))) {
            profile.setIDType("05");
        } else if (IDType.equalsIgnoreCase(getResources().getString(R.string.other))) {
            profile.setIDType("06");
        }
    }

    private void setIDType() {
        String IDType = preProfile.getIDType();
        if (IDType.equalsIgnoreCase("00")) {
            mOtherIdPrefix.setText(getResources().getString(R.string.None));
        }/*else if (IDType.equalsIgnoreCase("01")){
            //profile.setIDType("01");
        }*/ else if (IDType.equalsIgnoreCase("02")) {
            mOtherIdPrefix.setText(getResources().getString(R.string.identity_college));
        } else if (IDType.equalsIgnoreCase("03")) {
            mOtherIdPrefix.setText(getResources().getString(R.string.companyId));
        } else if (IDType.equalsIgnoreCase("04")) {
            mOtherIdPrefix.setText(getResources().getString(R.string.Passport));
        } else if (IDType.equalsIgnoreCase("05")) {
            mOtherIdPrefix.setText(getResources().getString(R.string.drivingLinId));
        } else if (IDType.equalsIgnoreCase("06")) {
            mOtherIdPrefix.setText(getResources().getString(R.string.other));
        }
    }



    private void addressAlert() {
        if (!countryCode.equals("+95"))
            showAlert(getString(R.string.enter_myanmar_address));
    }

    private void openCustomerCareDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        String[] items = new String[]{getResources().getString(R.string.customer_care), getResources().getString(R.string.mobile_no1), getResources().getString(R.string.mobile_no2)};
        View view = getLayoutInflater().inflate(R.layout.custom_title_dialog, null, false);
        CustomTextView tv = (CustomTextView) view.findViewById(R.id.title);
        tv.setText(R.string.call_care);
        tv.setTypeface(Utils.getFont(this), Typeface.BOLD);
        builder.setCustomTitle(view);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.single_choice_dailog_custom, R.id.custom_tv, items);

        builder.setSingleChoiceItems(adapter, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int pos) {
                dialog.dismiss();
                switch (pos) {
                    case 0:
                        openCallDialer("+95-1-377888");
                        break;
                    case 1:
                        openCallDialer("+95-930000066");
                        break;
                    case 2:
                        openCallDialer("+95-9450144455");
                        break;
                }
                //cbAgentCode1.setChecked(false);
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void openCallDialer(String mobileNo) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + mobileNo));
        startActivity(intent);
    }

    /**
     * Todo @Abhishek
     * Checking phone contain front camera or not
     */
  /*  private boolean isFrontCameraAvailable() {
        int cameraCount;
        boolean isFrontCameraAvailable = false;
        cameraCount = Camera.getNumberOfCameras();
        while (cameraCount > 0) {
            Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
            cameraCount--;
            Camera.getCameraInfo(cameraCount, cameraInfo);
            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                isFrontCameraAvailable = true;
                break;
            }
        }
        return isFrontCameraAvailable;
    }*/

    private void buttonAnimation() {
        animation = new AlphaAnimation(2, 0.3f); // Change alpha from fully visible to invisible
        animation.setDuration(500); // duration - half a second
        animation.setInterpolator(new LinearInterpolator()); // do not alter animation rate
        animation.setRepeatCount(Animation.INFINITE); // Repeat animation infinitely
        animation.setRepeatMode(Animation.REVERSE); // Reverse animation at the end so the button will fade back in
        btnSubmit.startAnimation(animation);
        llAccept.startAnimation(animation);
        llTerms.startAnimation(animation);
    }

    private void isReadytoRegister() {
        Timer t = new Timer();
        t.scheduleAtFixedRate(new TimerTask() {
                                  @Override
                                  public void run() {

                                      runOnUiThread(new Runnable() {
                                          @Override
                                          public void run() {
                                              String IDType = mOtherIdPrefix.getText().toString();
                                              String otherIDNo = mOtherIdEdit.getText().toString();
                                              String userName = muserNameEdit.getText().toString();
                                              String DOB = mDateOfBirth.getText().toString();
                                              String Email = mEmail.getText().toString();
                                              String nrcNo = mNRCEdittext.getText().toString();
                                              String fatherName = mFatherName.getText().toString();
                                              String password = mPassword.getText().toString();
                                              String confirmPassword = mConfirmPassword.getText().toString();

                                              if (isValid(userName, DOB, fatherName, nrcNo, IDType, otherIDNo, password, confirmPassword, Email, false)) {
                                                  if (isSigned && cbTerm.isChecked() && isProfileUpdate != 1) {
                                                      btnSubmit.clearAnimation();
                                                      llAccept.clearAnimation();
                                                  } else if (isProfileUpdate == 1) {
                                                      btnSubmit.clearAnimation();
                                                      llAccept.clearAnimation();
                                                  }
                                              } else {
                                                  btnSubmit.startAnimation(animation);
                                                  llAccept.startAnimation(animation);
                                              }
                                          }
                                      });

                                  }
                              },
                0,
                1000);
    }

    private void isAgelower18(){
        String dob="";
        if (preProfile!=null){
            dob=preProfile.getDateOfBirth().toString();
        }else {
            dob = mDateOfBirth.getText().toString();
        }
        if (!Utils.isEmpty(dob)) {
            if (Utils.getAge(dob)) {
                mNoID.setVisibility(View.GONE);
            } else {
                mNoID.setVisibility(View.VISIBLE);
            }
        } else {
            mNoID.setVisibility(View.VISIBLE);
        }
        if (isBusinessAccount) {
            mNoID.setVisibility(View.GONE);
        }

    }

    private class MyTextWatcherForMobileNo implements TextWatcher {

        public void afterTextChanged(Editable s) {

        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            try {
                if (s.length() < 2 && countryCode.equalsIgnoreCase("+95")) {
                    merchant_mobileno.setText(Utils.getMyanmarNumber());
                    merchant_mobileno.setSelection(merchant_mobileno.length());
                }

            } catch (Exception e) {

            }
        }
    }




    void callNoPasting()
    {
        mMobileNo.setOnLongClickListener(onLong);
        mNRCEdittext.setOnLongClickListener(onLong);
        mOtherId.setOnLongClickListener(onLong);
        mFatherName.setOnLongClickListener(onLong);
        mConfirmRecommendedBy.setOnLongClickListener(onLong);
        mConfirmPredefineNo.setOnLongClickListener(onLong);
        mEmail.setOnLongClickListener(onLong);
        mFacebook.setOnLongClickListener(onLong);
        mNoOrRoadorWard.setOnLongClickListener(onLong);
        mCity.setOnLongClickListener(onLong);
        mCityArea.setOnLongClickListener(onLong);
        muserNameEdit.setOnLongClickListener(onLong);
        mPassword.setOnLongClickListener(onLong);
        mConfirmPassword.setOnLongClickListener(onLong);
        mRecommendedBy.setOnLongClickListener(onLong);
        mOtherIdEdit.setOnLongClickListener(onLong);
        mPredefinedNumber.setOnLongClickListener(onLong);



    }


    View.OnLongClickListener onLong=new View.OnLongClickListener()
    {
        public boolean onLongClick(View v)
        {
            return true;
        }
    };
}

