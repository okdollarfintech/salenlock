package mm.dummymerchant.sk.merchantapp;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ToggleButton;

import mm.dummymerchant.sk.merchantapp.MerchantSurvey.MerchantSurvey;
import mm.dummymerchant.sk.merchantapp.MerchantSurvey.TransactionStatics;
import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.Utils;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
import mm.dummymerchant.sk.merchantapp.db.DBHelper;
import mm.dummymerchant.sk.merchantapp.httpConnection.CallingApIS;
import mm.dummymerchant.sk.merchantapp.maills.MailBroadCast;
import mm.dummymerchant.sk.merchantapp.model.MailDetails;
import mm.dummymerchant.sk.merchantapp.model.TransationModel;
import mm.dummymerchant.sk.merchantapp.wirelessService.WirelessUtils;

import static mm.dummymerchant.sk.merchantapp.Utils.Utils.getFont;

/**
 * Created by user on 11/9/2015.
 */
public class NewSettingActivity extends BaseActivity implements View.OnClickListener{
    public LinearLayout mupdate_profile;
    public LinearLayout camera_shot,mTranscationHistory,survey_layout,mTransactionStatics,personDetails,sendData,mAddPromotion,mChangePassword,mChangeLanguage,mLogout;
    Context con;
    int updateOrinsertt;
    private ToggleButton toggleNotification, toggleNotification_email, toggleNotification_sms;
    Boolean ischecked = false;
    Boolean isAddPromotionON=false;

    @Override
    public <T> void response(int resultCode, T data)
    {
        if (resultCode == REQUEST_TYPE_ADD_EMAIL_SETTINGS) {
            AppPreference.setOffEmailNotification(NewSettingActivity.this, ischecked);
            toggleNotification_email.setChecked(ischecked);
        } else if (resultCode == REQUEST_TYPE_ADD_SMS_SETTINGS) {
            AppPreference.setOffSMSNotification(NewSettingActivity.this, ischecked);
            toggleNotification_sms.setChecked(ischecked);
        }
        else if (resultCode == REQUEST_TYPE_LOGOUT) {
            TransationModel model = (TransationModel) data;
            if (model.getResultcode().equalsIgnoreCase("0")) {
                showToast(R.string.logout_sucessful);
                AppPreference.setFinishActivity(this, true);
                AppPreference.setAuthToken(NewSettingActivity.this, "XX");

                if (!AppPreference.isOnByMerchant(NewSettingActivity.this)) {
                    WirelessUtils.getInstance().setBluetooth(false, NewSettingActivity.this);
                }
                Intent i = new Intent(this, NewStartupActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                if(isAddPromotionON)
                    i.putExtra("AutoPayment",true);
                startActivity(i);
                finish();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_setting);
        setSettingsActionBar();
        con=this;
        init();
        final CallingApIS api = new CallingApIS(this, this, 0);

        toggleNotification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                AppPreference.setOffNotofication(NewSettingActivity.this, isChecked);
                if (!AppPreference.getIsPayments_alertsShow(NewSettingActivity.this))
                    showIntroductionData(R.string.payments_alerts, R.string.payments_alerts_intro, R.drawable.notification_ic, R.id.newactivituy_setting_screennotification_settin);

            }
        });
        toggleNotification_email.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                api.addProfileEmailAlerts(isChecked);
                ischecked = isChecked;
            }
        });

        toggleNotification_sms.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                api.addProfileSMSAlerts(isChecked);
                ischecked = isChecked;
            }
        });

    }

    private void init() {

        survey_layout = (LinearLayout)findViewById(R.id.survey_layout);
        mupdate_profile = (LinearLayout)findViewById(R.id.profile_update);
        mupdate_profile.setVisibility(View.VISIBLE);
        mTransactionStatics=(LinearLayout)findViewById(R.id.transactionSurvey_layout);
        mTranscationHistory = (LinearLayout) findViewById(R.id.newactivituy_setting_screen_transcation_history);
        personDetails=(LinearLayout)findViewById(R.id.personal_details);
        sendData=(LinearLayout)findViewById(R.id.senddata);
        mAddPromotion=(LinearLayout)findViewById(R.id.setting_promotion);
        mChangePassword = (LinearLayout) findViewById(R.id.newactivituy_setting_screen_check_change_password);
        mChangeLanguage = (LinearLayout) findViewById(R.id.newactivituy_setting_screen_change_labgyage);
        mLogout = (LinearLayout) findViewById(R.id.newactivituy_setting_check_logout);
        toggleNotification = (ToggleButton) findViewById(R.id.toggle_notification);
        toggleNotification_email = (ToggleButton) findViewById(R.id.toggle_notification_email);
        toggleNotification_sms = (ToggleButton) findViewById(R.id.toggle_notification_sms);
        camera_shot = (LinearLayout) findViewById(R.id.camera_shot);

        camera_shot.setOnClickListener(this);
        mupdate_profile.setOnClickListener(this);
        survey_layout.setOnClickListener(this);
        mTransactionStatics.setOnClickListener(this);
        mTranscationHistory.setOnClickListener(this);
        personDetails.setOnClickListener(this);
        sendData.setOnClickListener(this);
        mAddPromotion.setOnClickListener(this);
        mChangePassword.setOnClickListener(this);
        mChangeLanguage.setOnClickListener(this);
        mLogout.setOnClickListener(this);

        if (AppPreference.getOffNotificatino(this)) {
            toggleNotification.setChecked(true);
        } else {
            toggleNotification.setChecked(false);
        }

        if (AppPreference.getOffEmailNotification(NewSettingActivity.this))
            toggleNotification_email.setChecked(true);
        else
            toggleNotification_email.setChecked(false);

        if (AppPreference.getOffSMSNotificatino(NewSettingActivity.this))
            toggleNotification_sms.setChecked(true);
        else
            toggleNotification_sms.setChecked(false);
    }


    @Override
    public void onClick(View v) {
        Intent call;
        switch (v.getId())
        {
            case R.id.camera_shot:
                call = new Intent(this, CameraScreenShot.class);
                startActivity(call);
                break;
            case R.id.survey_layout:
               // Toast.makeText(NewSettingActivity.this, "survey page", Toast.LENGTH_SHORT).show();
                call = new Intent(this, MerchantSurvey.class);
                startActivity(call);
                break;

            case R.id.transactionSurvey_layout:
               // Toast.makeText(NewSettingActivity.this, "transactionSurvey page", Toast.LENGTH_SHORT).show();
                call = new Intent(this, TransactionStatics.class);
                startActivity(call);
                break;

            case R.id.newactivituy_setting_screen_transcation_history:
                call = new Intent(this, NewTransationDitails.class);
                startActivity(call);
                break;

            case R.id.personal_details:
                DBHelper dab=new DBHelper(this);
                dab.open();
               MailDetails mdd=dab.getMaildetails();

                if(mdd!=null)
                    updateOrinsertt=1;
                else
                updateOrinsertt=2;

                call=new Intent(this,PersonalDetailsActivity.class);
                Bundle bb=new Bundle();
                bb.putInt(BUNDLE_UPDATEORINSERT,updateOrinsertt);
                call.putExtras(bb);
                startActivity(call);
                dab.close();
                break;

            case R.id.senddata:

                DBHelper dbb=new DBHelper(this);
                dbb.open();
                MailDetails md=dbb.getMaildetails();
                if(md!=null)
                {
                    String fromMail=md.getFromMailId();
                    String pwd=md.getPassword();
                    String toMailId=md.getToMailId();

                    Intent intent = new Intent(this, MailBroadCast.class);
                    Bundle b=new Bundle();
                    b.putString(BUNDLE_KEY_FROM_EMAIL_ID,fromMail);
                    b.putString(BUNDLE_KEY_PWD,pwd);
                    b.putString(BUNDLE_KEY_TO_EMAIL_ID,toMailId);
                    intent.putExtras(b);
                    startService(intent);
                    showToast(R.string.mailsend);
                }
                else
                {
                    //showToast("Please Fill the personal details");
                    showAlert(getString(R.string.fill_personal_details));
                }
            dbb.close();
                break;

            case R.id.setting_promotion:
                if (AppPreference.getCurrentShopID(this).equals("XXXXXX")) {
                    //showAlert(getString(R.string.add_shop_first));
                    showAddShopAlert(getString(R.string.add_shop_first));
                    Log.i("ShopId ", AppPreference.getCurrentShopID(getApplicationContext()));
                    //showAlert(AppPreference.getCurrentShopID(getApplicationContext()));
                }
                else{
                    call = new Intent(this, AddViewPromotion.class);
                    startActivity(call);
                    Log.i("ShopId ", AppPreference.getCurrentShopID(getApplicationContext()));
                    //showAlert(AppPreference.getCurrentShopID(getApplicationContext()));
                }
                break;

            case R.id.newactivituy_setting_screen_check_change_password:
                call = new Intent(this, ChangePinActivity.class);
                startActivity(call);
                resetTimer();
                break;

            case R.id.profile_update:
                call = new Intent(this, NewRegistrationActivity1.class);
                call.putExtra("IS_FROM_SETTINGS",1);
                startActivity(call);
                resetTimer();
                break;

            case R.id.newactivituy_setting_screen_change_labgyage:
                call = new Intent(this, ChangeLanguageActivity.class);
                startActivity(call);
                resetTimer();
                break;

            case R.id.newactivituy_setting_check_logout:
                if (Utils.isReallyConnectedToInternet(this)) {
                    CallingApIS api1 = new CallingApIS(this, this);
                    api1.callLogoutAPI();
                } else {
                    showToast(R.string.logout_sucessful);
                    AppPreference.setFinishActivity(this, true);
                    AppPreference.setAuthToken(NewSettingActivity.this, "XX");
                    if (!AppPreference.isOnByMerchant(NewSettingActivity.this))
                    {
                        WirelessUtils.getInstance().setBluetooth(false, NewSettingActivity.this);
                    }
                    Intent intent = new Intent(this, NewStartupActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }
                break;

            default:
                break;
        }

    }
    public void showAddShopAlert(String message) {
        final Dialog dialog = new Dialog(this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.show_message_registration);
        CustomTextView userInput = (CustomTextView) dialog.findViewById(R.id.dialog_editext);
        CustomTextView tvOK = (CustomTextView) dialog.findViewById(R.id.tv_ok);
        CustomTextView tvCancel = (CustomTextView)dialog.findViewById(R.id.tv_cancel);
        userInput.setText(message);
        userInput.setTypeface(getFont(this), Typeface.BOLD);
        tvCancel.setVisibility(View.VISIBLE);
        tvOK.setText(R.string.setup_autopayment);
        dialog.setCancelable(true);
        tvOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isConnectedToInternet(NewSettingActivity.this)) {
                    CallingApIS api1 = new CallingApIS(NewSettingActivity.this, NewSettingActivity.this);
                    api1.callLogoutAPI();
                    isAddPromotionON=true;
                } else {
                    showToast(R.string.logout_sucessful);
                    AppPreference.setFinishActivity(NewSettingActivity.this, true);
                    AppPreference.setAuthToken(NewSettingActivity.this, "XX");
                    if (!AppPreference.isOnByMerchant(NewSettingActivity.this)) {
                        WirelessUtils.getInstance().setBluetooth(false, NewSettingActivity.this);
                    }
                    Intent intent = new Intent(NewSettingActivity.this, NewStartupActivity.class);
                    intent.putExtra("AutoPayment",true);
                    startActivity(intent);
                    finish();
                }
                dialog.dismiss();
            }
        });
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    void showIntroductionData(int title, int content, int imageId, final int viewId) {
        final Dialog dialog = new Dialog(this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.introduction_layout);
        CustomTextView textViewTitle = (CustomTextView) dialog.findViewById(R.id.textViewIntroTitle);
        ImageView imageViewTitle = (ImageView) dialog.findViewById(R.id.imageViewTitleIcon);
        CustomTextView textViewContent = (CustomTextView) dialog.findViewById(R.id.introData);
        CustomTextView footer = (CustomTextView) dialog.findViewById(R.id.textViewOk);
        textViewContent.setText(getResources().getString(content));
        textViewTitle.setText(getResources().getString(title));
        dialog.setCancelable(false);
        if (imageId != 0)
            imageViewTitle.setImageResource(imageId);
        footer.setOnClickListener(new View.OnClickListener()
                                  {
                                      @Override
                                      public void onClick(View v) {
                                          Intent call;
                                          switch (viewId) {

                                              case R.id.newactivituy_setting_screennotification_settin:
                                                  AppPreference.setIsPayments_alertsShow(NewSettingActivity.this,true);
                                                  break;

                                              default:
                                                  break;
                                          }
                                          dialog.dismiss();
                                      }
                                  }

        );
        dialog.show();
    }



}
