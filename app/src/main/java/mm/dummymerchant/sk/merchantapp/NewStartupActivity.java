package mm.dummymerchant.sk.merchantapp;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableRow;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import mm.dummymerchant.sk.merchantapp.ReceivedMoney.NewReceivedMoney;
import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.CallUpdateAppInterface;
import mm.dummymerchant.sk.merchantapp.Utils.Constant;
import mm.dummymerchant.sk.merchantapp.Utils.Utils;
import mm.dummymerchant.sk.merchantapp.adapter.BusinessAreaListAdapter;
import mm.dummymerchant.sk.merchantapp.customView.CustomButton;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
import mm.dummymerchant.sk.merchantapp.db.DBHelper;
import mm.dummymerchant.sk.merchantapp.httpConnection.CallingApIS;
import mm.dummymerchant.sk.merchantapp.introduction.GPSTracker;
import mm.dummymerchant.sk.merchantapp.model.MerchantShopDetailsModel;
import mm.dummymerchant.sk.merchantapp.model.PromotionModel;
import mm.dummymerchant.sk.merchantapp.wirelessService.AutoBroadcastService;
import mm.dummymerchant.sk.merchantapp.wirelessService.AutoGPSLocationUpdate;
import mm.dummymerchant.sk.merchantapp.wirelessService.AutoPaymentInterface;
import mm.dummymerchant.sk.merchantapp.wirelessService.AutoPaymentTemplate;
import mm.dummymerchant.sk.merchantapp.wirelessService.WirelessUtils;

import static mm.dummymerchant.sk.merchantapp.Utils.AppPreference.APP_PREF_REGISTER;
/**
 * Created by Dell on 10/26/2015.
 */
public class NewStartupActivity extends BaseActivity implements View.OnClickListener, AutoPaymentInterface.onAutoPaymentInterface,CallUpdateAppInterface.callUpdateInterface {
    TableRow mRegistration, mLoginpage;
    private String defaultNo = "";
    public boolean isWindowOpen = false, isNewBusinessArea = false;
    RelativeLayout wirelessOption;
    View rowLine, loginline;
    CheckBox merchantStatus;
    BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    private String codeFormat, codeContent;
    CustomTextView merchant_Status;
    PopupWindow popupMessage;
    ImageView mEnglish, mBurmese;
    private String businessName, businessCellID;
    public Dialog dialogBusinessView;
    WindowManager wm;
    private View windwoView;
    ImageView imgDemo;
    LinearLayout ll_transparentLine;
    ImageView imgShopLocation;
    AutoGPSLocationUpdate brLocationUpdate;
    String userType = "";
    PromotionModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_newstartup_activity);


        if (Utils.isReallyConnectedToInternet(this)) {
            AppPreference.setInternetOn_at_login(getApplicationContext(), true);
        } else {
            AppPreference.setInternetOn_at_login(getApplicationContext(), false);
        }

        AutoPaymentInterface.getInstance().setListener(this);
        CallUpdateAppInterface.getInstance().setUpdateAppListner(this);
        AppPreference.setAuthToken(this, "XX");
        countDownTimer.cancel();
        setDummyMerchant_Startup(String.valueOf(getResources().getText(R.string.dummymerchant)));
        AppPreference.setFinishActivity(this, false);
        backButton.setVisibility(View.GONE);
        buttonLogout.setVisibility(View.INVISIBLE);
        db = new DBHelper(this);
        init();
      //  wirelessOption.setVisibility(View.GONE);

        if (getIntent().getBooleanExtra("AutoPayment", false)) {
            AutoPaymentTemplate.dialogAddNewMerchant(NewStartupActivity.this);
            final GPSTracker gpsTracker = new GPSTracker(NewStartupActivity.this);
            if (!gpsTracker.gpsStatus()) {
                Context context = NewStartupActivity.this;
                if (AppPreference.getBusinessMainCategory(context).equals("04") && AppPreference.getBusinessSubCategory(context).equals("00") || AppPreference.getBusinessSubCategory(context).equals("01") || AppPreference.getBusinessSubCategory(context).equals("02"))
                    gpsTracker.showSettingsAlert(NewStartupActivity.this, true);
                else
                    gpsTracker.showSettingsAlert(NewStartupActivity.this, false);
            }
        }
        if (db.getMasterDetails()!=null)
        {    mRegistration.setVisibility(View.GONE);
            wirelessOption.setVisibility(View.VISIBLE);
            rowLine.setVisibility(View.VISIBLE);


            userType = AppPreference.getUserType(this);

            if (!userType.equals("SUBSBR") && !userType.isEmpty())
            {
                wirelessOption.setVisibility(View.VISIBLE);
                rowLine.setVisibility(View.VISIBLE);
                brLocationUpdate = new AutoGPSLocationUpdate(this);
                if (AppPreference.getMerchantStatus(NewStartupActivity.this))
                {
                    if (DBHelper.getAllAreaCount() > 0)
                    {   imgShopLocation.setVisibility(View.VISIBLE);
                        if (!isMyServiceRunning(AutoBroadcastService.class))
                        {
                            Intent background = new Intent(NewStartupActivity.this, AutoBroadcastService.class);
                            NewStartupActivity.this.startService(background);
                        }
                    }

                }
            }
        }
        else
        {
          /*  mLoginpage.setVisibility(View.GONE);
           loginline.setVisibility(View.GONE);*/
        }

        mDefaultLang.setVisibility(View.VISIBLE);
        if (AppPreference.getLanguage(this).equalsIgnoreCase("my")) {
            mDefaultLang.setImageResource(R.drawable.myanmar_flag);
        } else {
            mDefaultLang.setImageResource(R.drawable.uk);
        }

        getViberNumber();
        popupInit();
        if (!AppPreference.getLanguageAlert(this))
            showLanguageAlert();

        try {
            copyAppDbToDownloadFolder();
        } catch (Exception e) {
            e.printStackTrace();
        }

        String mobileNo = AppPreference.getMyMobileNo(getApplicationContext());
        System.out.println("The Mobile No : " + mobileNo);

        if (Utils.isReallyConnectedToInternet(this))
        {
            if(mobileNo.equals(""))
            {
                System.out.println("Didnt get Mobile No");
            }
            else {
                System.out.println("Get Mobile No"+ mobileNo);
                Intent i = new Intent(getApplicationContext(), UpdateStater.class);
                startService(i);
            }
        }
        else
        {
            System.out.println("Dnt have Internet connection");
        }
    }

    private void getViberNumber() {
        AccountManager am = AccountManager.get(this);
        Account[] accounts = am.getAccounts();
        for (Account ac : accounts) {
            String acname = ac.name;
            String actype = ac.type;
            if (actype.equals("com.viber.voip")) {
                if (acname.startsWith(AppPreference.getCountryCode(this)))
                    defaultNo = acname;
                else
                    defaultNo = "";
                return;
            }
        }
    }

    public void copyAppDbToDownloadFolder() throws IOException {
        Log.i("Enter Function", "copyAppDbToDownloadFolder");
        File backupDB = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "DATABASE_COPY");

        File currentDB = getApplicationContext().getDatabasePath(DBHelper.DATABASE_NAME);

        if (currentDB.exists()) {
            FileChannel src = new FileInputStream(currentDB).getChannel();
            FileChannel dst = new FileOutputStream(backupDB).getChannel();
            dst.transferFrom(src, 0, src.size());
            src.close();
            dst.close();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

        if (db.getMasterDetails()!=null) {
            mRegistration.setVisibility(View.GONE);
            userType = AppPreference.getUserType(this);
            if (!userType.equals("SUBSBR")&& !userType.isEmpty()) {
                wirelessOption.setVisibility(View.VISIBLE);
                rowLine.setVisibility(View.VISIBLE);
                if (AppPreference.getMerchantStatus(NewStartupActivity.this)) {
                    if (DBHelper.getAllAreaCount() > 0) {
                        setPromotion();
                        if (!isMyServiceRunning(AutoBroadcastService.class)) {
                            Intent background = new Intent(NewStartupActivity.this, AutoBroadcastService.class);
                            NewStartupActivity.this.startService(background);
                        }else
                            AutoPaymentInterface.getInstance().findMerchantStatus();
                    }
                }
            }
        }
    }

    void init() {
        // mQRCodeGenerator = (TableRow) findViewById(R.id.qrGenerator);
        mRegistration = (TableRow) findViewById(R.id.registration);
        wirelessOption = (RelativeLayout) findViewById(R.id.wirelesssection);
        merchantStatus = (CheckBox) findViewById(R.id.checkBox);
        rowLine = findViewById(R.id.registrationLine);
        loginline = findViewById(R.id.loginLine);
        merchant_Status = (CustomTextView) findViewById(R.id.txtMerchantShare);
        imgShopLocation = (ImageView) findViewById(R.id.imageViewMap);
        imgDemo = (ImageView) findViewById(R.id.imageViewDemo);
        ll_transparentLine = (LinearLayout) findViewById(R.id.outline);

        mLoginpage = (TableRow) findViewById(R.id.login);
        setClickListener();
    }

    private void setClickListener() {
        //mQRCodeGenerator.setOnClickListener(this);
        mRegistration.setOnClickListener(this);
        mLoginpage.setOnClickListener(this);
        wirelessOption.setOnClickListener(this);
        mDefaultLang.setOnClickListener(this);
        imgDemo.setOnClickListener(this);
        imgShopLocation.setOnClickListener(this);
        ll_transparentLine.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (isWindowOpen) {
            try {
                wm.removeView(windwoView);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
        Intent intent;
        switch (v.getId()) {


            case R.id.registration:

                    intent = new Intent(this, NumberValidation1.class);
                    intent.putExtra("DEFAULT_NUMBER", defaultNo);
                    startActivity(intent);
                    isWindowOpen = false;

                break;

            case R.id.login:
                String sim1 = db.getSimId();
                String sIM2 = Utils.simSerialNumber(getApplicationContext());
                System.out.println(sim1 + "" + sIM2);
                if (db.getSimId().equals(Utils.simSerialNumber(getApplicationContext()))) {
                    intent = new Intent(this, NewLoginStep1Activity.class);
                    intent.putExtra("DEFAULT_NUMBER", defaultNo);
                    startActivity(intent);

                } else {
                    if (db.getSimId().equals("")) {
                        intent = new Intent(this, NewLoginStep1Activity.class);
                        intent.putExtra("DEFAULT_NUMBER", defaultNo);
                        startActivity(intent);

                    } else {
                        SimError();
                    }

                }

                isWindowOpen = false;
                break;
            case R.id.receivedMoney:
                callPage(NewReceivedMoney.class);
                break;


            case R.id.wirelesssection:
                isWindowOpen = false;
                setupAutoPayment();
                break;
            case R.id.new_strt_up_imageview_english_button:
                if (!AppPreference.getLanguage(this).equals("en")) {
                    AppPreference.setLanguage(this, "en");
                    yesClcked();
                }
                isWindowOpen = false;
                break;
            case R.id.new_strt_up_imageview_burmese_button:
                if (!AppPreference.getLanguage(this).equals("my")) {
                    AppPreference.setLanguage(this, "my");
                    yesClcked();
                }
                isWindowOpen = false;
                break;
            case R.id.img_default_lang:
                popupMessage.showAsDropDown(mDefaultLang, (int) mDefaultLang.getX(), (int) mDefaultLang.getY() - 10);

                break;
            case R.id.imageViewDemo:
                if (isWindowOpen) {
                    try {
                        wm.removeView(windwoView);
                        isWindowOpen = false;
                    } catch (IllegalArgumentException e) {
                        e.printStackTrace();
                    }

                } else {
                    wm = (WindowManager) getSystemService(WINDOW_SERVICE);
                    WindowManager.LayoutParams layOutParams = new WindowManager.LayoutParams(WindowManager.LayoutParams.MATCH_PARENT,
                            WindowManager.LayoutParams.WRAP_CONTENT,
                            WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
                            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                                    | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                                    | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                            PixelFormat.TRANSLUCENT);
                    LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    windwoView = inflater.inflate(R.layout.error_pop_up, null);
                    ScrollView llNotificationAnim = (ScrollView) windwoView.findViewById(R.id.ll_notification_anim);

                    llNotificationAnim.setVisibility(View.GONE);
                    layOutParams.gravity = Gravity.TOP;

                    wm.addView(windwoView, layOutParams);
                    ImageView gif = (ImageView) windwoView.findViewById(R.id.gif_anim);
                    gif.setMinimumHeight(120);
                    gif.setVisibility(View.VISIBLE);
                    gif.setBackgroundResource(R.drawable.new_pay);
                    isWindowOpen = true;
                }
                break;

            case R.id.outline:
                isWindowOpen = false;
                break;
            case R.id.imageViewMap:
                Intent callLocation = new Intent(this, MapActivity.class);
                callLocation.putExtra(MERCHANT_MOBILE_NO,AppPreference.getMyMobileNo(this));
                callLocation.putExtra(BUNDLE_MAP_CELL_ID,"");
                callLocation.putExtra(BUNDLE_MAP_LAT_LONG,DBHelper.getShopLatLang(AppPreference.getCurrentShopID(this)));
                startActivity(callLocation);
                isWindowOpen = false;
                break;

            default:
                break;

        }
    }

    public void yesClcked() {
        getSharedPreferences(APP_PREF_REGISTER, 0).edit().clear().commit();
        Intent i = getPackageManager().getLaunchIntentForPackage(getPackageName());
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(i);
        finish();
    }

    void callPage(Class cls) {
        Intent intent = new Intent(this, cls);
        startActivity(intent);
    }











    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private void updateBLTstatus() {
        if (!AppPreference.isOnByMerchant(NewStartupActivity.this)) {
            boolean isEnabled = bluetoothAdapter.isEnabled();
            Log.d("BLT", "BLT NEW Status:" + isEnabled);
            AppPreference.setBLTStatus(NewStartupActivity.this, isEnabled);
            AppPreference.setIsONbyMerchant(NewStartupActivity.this, true);
        }
    }


    @Override
    public void updateMerchantStatus(String areaName) {
        switch (areaName) {
            case "All location":
                merchantStatus.setChecked(true);
                merchant_Status.setText(getString(R.string.autopayment_all_loactions));
                rowLine.setVisibility(View.VISIBLE);
                imgShopLocation.setVisibility(View.VISIBLE);
                Log.d("TestBLT", "in Check Merchant Status for every location");
                break;
            case "Not Found":
                merchant_Status.setText(getString(R.string.auto_payment_msg2));
                merchantStatus.setChecked(false);
                ((CustomTextView) findViewById(R.id.newStartUp_Screen_promotiontextview)).setVisibility(View.GONE);

                break;
            default:
                merchantStatus.setChecked(true);
                String temp = String.format(getString(R.string.auto_payment_msg), areaName);
                merchant_Status.setText(Html.fromHtml(temp));
                rowLine.setVisibility(View.VISIBLE);
                imgShopLocation.setVisibility(View.VISIBLE);
                break;
        }

    }


    boolean getFormatteddate(String transDate, String createDate) {
        boolean status = false;

        DateFormat targetFormat = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss");

        try {
            Date d1 = targetFormat.parse(transDate);
            Date d2 = targetFormat.parse(createDate);

            if (d1.after(d2)) {
                status = true;
                return status;
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }


        return status;
    }

    public void popupInit() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layoutOfPopup = (View) inflater.inflate(R.layout.popup_languge_change, null);
        popupMessage = new PopupWindow(layoutOfPopup, LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        popupMessage.setContentView(layoutOfPopup);
        popupMessage.setOutsideTouchable(true);
        popupMessage.setFocusable(true);
        popupMessage.setBackgroundDrawable(new BitmapDrawable());
        mEnglish = (ImageView) layoutOfPopup.findViewById(R.id.new_strt_up_imageview_english_button);
        mBurmese = (ImageView) layoutOfPopup.findViewById(R.id.new_strt_up_imageview_burmese_button);
        mEnglish.setOnClickListener(this);
        mBurmese.setOnClickListener(this);
        // popupMessage.showAsDropDown(mDefaultLang, 0, 0);
    }


    @Override
    public <T> void response(int resultCode, T data) {

        switch (resultCode){
            case REQUEST_FOR_LUCKEY_DRAW:
                showToast("Uploaded success");
                break;
            case REQUEST_TYPE_ADD_SHOPS:
                try {
                    JSONObject jsonObject = new JSONObject((String) data);
                    brLocationUpdate = new AutoGPSLocationUpdate(this);
                    if (jsonObject.getString("Code").equals("200") && !jsonObject.getString("Data").isEmpty()) {
                        String shopID = jsonObject.getString("Data");
                        Log.e("testlat...",""+brLocationUpdate.getLatitude());
                        Log.e("testlong...",""+brLocationUpdate.getLatitude());

                        if (SaveNearByLocationDetails(businessName, shopID, businessCellID,brLocationUpdate.getLatitude()+"",brLocationUpdate.getLongitude()+""))
                        {
                            if (AppPreference.getBusinessMainCategory(this).equals("04") && AppPreference.getBusinessSubCategory(this).equals("00") || AppPreference.getBusinessSubCategory(this).equals("01") || AppPreference.getBusinessSubCategory(this).equals("02"))
                                AppPreference.setAutoPayment(NewStartupActivity.this, true);
                            merchantStatus.setChecked(true);
                            rowLine.setVisibility(View.VISIBLE);
                            AppPreference.setCurrentShopID(NewStartupActivity.this, shopID);
                            AppPreference.setMerchantStatus(NewStartupActivity.this, true);
                            String temp = String.format(getString(R.string.auto_payment_msg), DBHelper.getShopName(shopID));
                            // ImageSpan imagespan = new ImageSpan(this, ressource);
                            // merchant_Status.setSpan(imagespan, i, i + strLength, 0);
                            merchant_Status.setText(Html.fromHtml(temp));
                            hideKeyboard();
                            isNewBusinessArea = true;
                            Intent background = new Intent(NewStartupActivity.this, AutoBroadcastService.class);
                            NewStartupActivity.this.startService(background);
                            createPromotionContent();
                        } else
                            Utils.showCustomToastMsg(NewStartupActivity.this, R.string.unable_to_procced_request, false);

                    } else if (jsonObject.getString("Code").equals("302")) {
                        SaveNearByLocationDetails(businessName, "XXXXX", businessCellID,brLocationUpdate.getLatitude()+"",brLocationUpdate.getLongitude()+"");
                        String message = jsonObject.getString("Msg");
                        showAlert(message);
                    } else if (jsonObject.getString("Code").equals("301")) {
                        final GPSTracker gpsTracker = new GPSTracker(NewStartupActivity.this);
                        gpsTracker.showSettingsAlert(NewStartupActivity.this,true);
                        String message = jsonObject.getString("Msg");
                        showAlert(message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    showToast("excption Happened");
                }
                break;
            case REQUEST_TYPE_DELETE_SHOPS:
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject((String) data);
                    if (jsonObject.getString("Code").equals("200")) {
                        if (DBHelper.removeShopArea()) {
                            AppPreference.setMerchantStatus(NewStartupActivity.this, false);
                            Intent background = new Intent(NewStartupActivity.this, AutoBroadcastService.class);
                            stopService(background);
                            AppPreference.setIsONbyMerchant(NewStartupActivity.this, false);
                            AppPreference.setCurrentShopID(NewStartupActivity.this, "XXXXXX");
                            AutoPaymentInterface.getInstance().updateMerchantModule("Not Found");
                            CallingApIS api = new CallingApIS(this, this, 1);
                            api.deletePromotion();
                            imgShopLocation.setVisibility(View.INVISIBLE);
                        } else
                            Utils.showCustomToastMsg(NewStartupActivity.this, R.string.delete_shop_error, false);

                    } else {
                        String message = jsonObject.getString("Msg");
                        showAlert(message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                dialogBusinessView.dismiss();
                break;
            case REQUEST_CHECK_STATUS_SHOPS:
                JSONObject jsonObj = null;
                try {
                    jsonObj = new JSONObject((String) data);
                    if (jsonObj.getString("Code").equals("200")) {
                        String shopDetails = jsonObj.getString("Data");
                        JSONArray jsonArray = new JSONObject(shopDetails).getJSONArray("ShopStatus");
                        Log.d("Data", shopDetails);
                        int size = jsonArray.length();
                        String shopName = DBHelper.getShopName("XXXXX");
                        while (size > 0) {
                            size--;
                            jsonObj = (JSONObject) jsonArray.get(size);
                            if (shopName.equals(jsonObj.getString("ShopName"))) {
                                if (jsonObj.getString("ShopStatus").equals("-1")) {
                                    Utils.showCustomToastMsg(NewStartupActivity.this, R.string.shop_not_approved, false);
                                    break;
                                } else {
                                    if (DBHelper.updateNotApprovedShopStatus(shopName, jsonObj.getString("ShopId"), "ON") > 0) {
                                        String shopID = jsonObj.getString("ShopId");
                                        merchantStatus.setChecked(true);
                                        rowLine.setVisibility(View.VISIBLE);
                                        AppPreference.setCurrentShopID(NewStartupActivity.this, shopID);
                                        AppPreference.setMerchantStatus(NewStartupActivity.this, true);
                                        String temp = String.format(getString(R.string.auto_payment_msg), DBHelper.getShopName(shopID));
                                        merchant_Status.setText(Html.fromHtml(temp));
                                        isNewBusinessArea = true;
                                        Intent background = new Intent(NewStartupActivity.this, AutoBroadcastService.class);
                                        NewStartupActivity.this.startService(background);
                                    } else
                                        Utils.showCustomToastMsg(NewStartupActivity.this, R.string.delete_shop_error, false);

                                    break;
                                }

                            }

                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case REQUEST_ADD_PROMOTION:
                String promotion = (String) data;
                try {
                    JSONObject response = new JSONObject(promotion);
                    String code= response.getString("Code");
                    String dataResponse= response.getString("Data");
                    if (code.equals("200")) {
                        JSONObject dataObject = new JSONObject(dataResponse);
                        JSONArray promotionArray= dataObject.getJSONArray("Promotion Details");
                        for (int i=0; i<promotionArray.length(); i++){
                            JSONObject object= promotionArray.getJSONObject(i);
                            String promotionID= object.getString("PromotionId");
                            AppPreference.setPromotionId(this, promotionID);
                            model.setId(promotionID);
                            model.setLast_updated(object.getString("UpdatedDate"));
                            DBHelper.insertPromotion(model);
                            setPromotion();
                            showAlert(getString(R.string.promotion_info));
                            //finish();
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
            case REQUEST_DELETE_PROMOTION:
                String deletePromotion = (String) data;
                try{
                    JSONObject jsonRes= new JSONObject(deletePromotion);
                    if (jsonRes.getString("Code").equals("200")) {
                       // ((CustomTextView) findViewById(R.id.newStartUp_Screen_promotiontextview)).setVisibility(View.GONE);
                        ((CustomTextView) findViewById(R.id.newStartUp_Screen_promotiontextview)).setText("");
                        db.deletePromotion();
                        //showToast(R.string.deleted_successful);
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }
                break;
            default:
                break;

        }

    }

    private boolean SaveNearByLocationDetails(String businessName, String shopID, String businessCellID, String lat, String lang) {

        String name="";
        if (shopID.equals("XXXXX"))
            DBHelper.insertShopDetails(shopID, businessName, businessCellID, "NOT_APPROVED", name, lat, lang);
        else
            DBHelper.insertShopDetails(shopID, businessName, businessCellID, "ON", name, lat, lang);
        if (mWifiResult != null) {
            int size = mWifiResult.size() - 1;
            if (size > 0) {
                while (size >= 0) {
                    name = mWifiResult.get(size).BSSID;
                    int level = mWifiResult.get(size).level;
                    if (level > -60) {
                        if (shopID.equals("XXXXX"))
                            DBHelper.insertShopDetails(shopID, businessName, businessCellID, "NOT_APPROVED", name,lat,lang);
                        else
                            DBHelper.insertShopDetails(shopID, businessName, businessCellID, "ON", name,lat,lang);

                    }
                    size--;
                }
                return true;
            }
        }
        return true;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (isWindowOpen) {
            try {
                wm.removeView(windwoView);
                isWindowOpen = false;
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }

    }

    private void showLanguageAlert() {
        final Dialog dialog = new Dialog(this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.language_alert);
        CustomTextView okBtn = (CustomTextView) dialog.findViewById(R.id.textOkBtn);
        CustomTextView cancelBtn = (CustomTextView) dialog.findViewById(R.id.buttonNo);
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                AppPreference.setLanguage(NewStartupActivity.this, "en");
                AppPreference.setLanguageAlert(NewStartupActivity.this, true);
                yesClcked();
            }
        });
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                AppPreference.setLanguageAlert(NewStartupActivity.this, true);
            }
        });
        dialog.show();

    }

    @Override
    public void updateBusinessName(String name) {

        if (!DBHelper.getIsShopNameExist(name)) {
            String LatLang = getLocationFromNetwork();
            String Lat = LatLang.substring(0, LatLang.indexOf(","));
            String Lang = LatLang.substring(LatLang.indexOf(",") + 1, LatLang.length());
            businessName = name;
            businessCellID = getLACAndCID(NewStartupActivity.this);
            CallingApIS callingApIS = new CallingApIS(this, this, 0);
            callingApIS.addMerchantShopDetails(businessName, Lat, Lang, businessCellID);

        } else
            showToast(getString(R.string.name_already_exixts));
    }

    private void setupAutoPayment() {
        if (merchantStatus.isChecked()) {
            showBusinessArea(DBHelper.getShopList());

        } else {
            String tempCellID = getLACAndCID(NewStartupActivity.this);
            if (!tempCellID.isEmpty())
                tempCellID = tempCellID.substring(0, tempCellID.length() - 2);
            if (tempCellID.length() > 3) {
                if (DBHelper.getShopList().size() > 0) {
                    showBusinessArea(DBHelper.getShopList());

                } else {
                    final Dialog dialog = new Dialog(this);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.auto_payment_dialog);
                    CustomTextView title = (CustomTextView) dialog.findViewById(R.id.textView19Title);
                    CustomTextView alert_message = (CustomTextView) dialog.findViewById(R.id.textView20Message);
                    CustomTextView yes = (CustomTextView) dialog.findViewById(R.id.buttonYes);
                    final CustomTextView no = (CustomTextView) dialog.findViewById(R.id.buttonNo);
                    final CustomTextView btnView = (CustomTextView) dialog.findViewById(R.id.buttonView);
                    title.setText(getString(R.string.autopayment_title));
                    title.setTypeface(Utils.getFont(NewStartupActivity.this), Typeface.BOLD);
                    alert_message.setText(getString(R.string.autopayment_msg));
                    final String finalTempCellID = tempCellID;
                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            if (DBHelper.getAllAreaCount() < 1) {
                                if (!DBHelper.getIsShopLocationExist(finalTempCellID)) {
                                    AutoPaymentTemplate.dialogAddNewMerchant(NewStartupActivity.this);
                                    final GPSTracker gpsTracker = new GPSTracker(NewStartupActivity.this);
                                    if (!gpsTracker.gpsStatus()) {
                                        Context context = NewStartupActivity.this;
                                        if (AppPreference.getBusinessMainCategory(context).equals("04") && AppPreference.getBusinessSubCategory(context).equals("00") || AppPreference.getBusinessSubCategory(context).equals("01") || AppPreference.getBusinessSubCategory(context).equals("02"))
                                            gpsTracker.showSettingsAlert(NewStartupActivity.this, true);
                                        else
                                            gpsTracker.showSettingsAlert(NewStartupActivity.this, false);
                                    }
                                } else
                                    showAlert(getString(R.string.location_already));
                                btnView.setVisibility(View.VISIBLE);
                                no.setVisibility(View.GONE);
                                btnView.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        showBusinessArea(DBHelper.getShopList());
                                        dialog.dismiss();
                                    }
                                });
                            } else
                                showAlert(getString(R.string.only_two_location));
                            dialog.dismiss();
                        }
                    });

                    no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            merchantStatus.setChecked(false);
                            dialog.dismiss();
                        }
                    });

                    dialog.show();

                }
            } else if (mWifiResult != null) {
                int size = mWifiResult.size() - 1;
                while (size >= 0) {
                    final String name = mWifiResult.get(size).BSSID;
                    int level = mWifiResult.get(size).level;
                    if (level > -80) {
                        final Dialog dialog = new Dialog(this);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.auto_payment_dialog);
                        CustomTextView title = (CustomTextView) dialog.findViewById(R.id.textView19Title);
                        CustomTextView alert_message = (CustomTextView) dialog.findViewById(R.id.textView20Message);
                        CustomTextView yes = (CustomTextView) dialog.findViewById(R.id.buttonYes);
                        final CustomTextView no = (CustomTextView) dialog.findViewById(R.id.buttonNo);
                        final CustomTextView btnView = (CustomTextView) dialog.findViewById(R.id.buttonView);
                        title.setTypeface(Utils.getFont(NewStartupActivity.this), Typeface.BOLD);
                        title.setText(getString(R.string.autopayment_title));
                        alert_message.setText(getString(R.string.autopayment_msg));

                        yes.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                if (DBHelper.getAllAreaCount() < 1) {
                                    if (!DBHelper.getIsShopLocationExist(name))
                                    {
                                        AutoPaymentTemplate.dialogAddNewMerchant(NewStartupActivity.this);
                                        final GPSTracker gpsTracker = new GPSTracker(NewStartupActivity.this);
                                        if (!gpsTracker.gpsStatus()) {
                                            Context context = NewStartupActivity.this;
                                            if (AppPreference.getBusinessMainCategory(context).equals("04") && AppPreference.getBusinessSubCategory(context).equals("00") || AppPreference.getBusinessSubCategory(context).equals("01") || AppPreference.getBusinessSubCategory(context).equals("02"))
                                                gpsTracker.showSettingsAlert(NewStartupActivity.this, true);
                                            else
                                                gpsTracker.showSettingsAlert(NewStartupActivity.this, false);
                                        }
                                    } else
                                        showAlert(getString(R.string.location_already));
                                    btnView.setVisibility(View.VISIBLE);
                                    no.setVisibility(View.GONE);
                                    btnView.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            showBusinessArea(DBHelper.getShopList());
                                            dialog.dismiss();
                                        }
                                    });
                                } else
                                    showAlert(getString(R.string.only_two_location));
                                dialog.dismiss();
                            }
                        });
                        no.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                merchantStatus.setChecked(false);
                                dialog.dismiss();
                            }
                        });
                        if (DBHelper.getAllAreaCount() > 0) {
                            btnView.setVisibility(View.VISIBLE);
                            no.setVisibility(View.GONE);
                            btnView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    showBusinessArea(DBHelper.getShopList());
                                    dialog.dismiss();
                                }
                            });
                        }

                        dialog.show();
                        break;
                    }
                    size--;
                }
            } else {
                final Dialog dialog = new Dialog(this);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.gps_alert_dialog);
                CustomTextView title = (CustomTextView) dialog.findViewById(R.id.title);
                CustomTextView alert = (CustomTextView) dialog.findViewById(R.id.alert);
                title.setText(R.string.autopayment_title);
                alert.setText(R.string.autopayment_msg2);
                CustomButton yes = (CustomButton) dialog.findViewById(R.id.btn_settings);
                CustomButton no = (CustomButton) dialog.findViewById(R.id.btn_cancel);
                yes.setText(R.string.dailog_yes);
                no.setText(R.string.dailog_no);

                yes.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        updateBLTstatus();
                        final WifiManager wifiManagers = (WifiManager) getSystemService(Context.WIFI_SERVICE);
                        if (wifiManagers.isWifiEnabled()) {
                            //do nothing
                        } else {
                            WirelessUtils.getInstance().setHotSpotON(NewStartupActivity.this);
                        }
                        WirelessUtils.getInstance().setBluetooth(true, NewStartupActivity.this);
                        WirelessUtils.getInstance().changeBluetoothName(true, NewStartupActivity.this);
                        AppPreference.setMerchantStatus(NewStartupActivity.this, true);
                        merchantStatus.setChecked(true);
                        merchant_Status.setText(R.string.autopayment_active);
                        dialog.dismiss();
                    }
                });
                no.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        merchantStatus.setChecked(false);
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        }
    }


    private void setPromotion() {
        PromotionModel model = db.getPromotionData();
        CustomTextView promotionText = ((CustomTextView) findViewById(R.id.newStartUp_Screen_promotiontextview));
        if (model.getTitle().equals("")) {
            promotionText.setVisibility(View.GONE);
        } else {
            Animation marquee = AnimationUtils.loadAnimation(this, R.anim.marque);
            promotionText.startAnimation(marquee);
            promotionText.setVisibility(View.VISIBLE);
            promotionText.setText(model.getTitle());
        }
    }

    private void createPromotionContent() {
        model = new PromotionModel();
        model.setTitle("No promotion set");
        model.setDescripton("");
        model.setMobileNo(AppPreference.getMyMobileNo(this));
        model.setWebsite("");
        model.setFacebook("");
        model.setName(AppPreference.getLoginuserName(this));
        model.setEmail(Utils.getDefaultEmail(this));
        CallingApIS api1 = new CallingApIS(this, this, 1);
        api1.addPromotionMsg(model);

    }

    private void showBusinessArea(final ArrayList<MerchantShopDetailsModel> shopDetails) throws RuntimeException {
        dialogBusinessView = new Dialog(NewStartupActivity.this);
        dialogBusinessView.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogBusinessView.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogBusinessView.setContentView(R.layout.bluetooth_devices);
        CustomTextView tv_title = (CustomTextView) dialogBusinessView.findViewById(R.id.tv_title);
        ImageView imageClose = (ImageView) dialogBusinessView.findViewById(R.id.imageClose);
        tv_title.setText(getString(R.string.auto_payment_title));
        tv_title.setTypeface(Utils.getFont(NewStartupActivity.this), Typeface.BOLD);
        final ListView listBusinessFound = (ListView) dialogBusinessView.findViewById(R.id.listdevicesfound);
        final RelativeLayout autoPaymentClose = (RelativeLayout) dialogBusinessView.findViewById(R.id.footer);
        final CustomTextView removeBusinessArea = (CustomTextView) dialogBusinessView.findViewById(R.id.tv_status);
        final CustomTextView autoPaymentBT = (CustomTextView) dialogBusinessView.findViewById(R.id.moreMerchantStatus);
        final RelativeLayout autoPaymentLayout = (RelativeLayout) dialogBusinessView.findViewById(R.id.merchant_status);
        final CheckBox autoPaymentCheck = (CheckBox) dialogBusinessView.findViewById(R.id.checkBox3);
        removeBusinessArea.setText(getString(R.string.close));
        autoPaymentBT.setText(getString(R.string.click_button));
        autoPaymentBT.setGravity(Gravity.START);
        imageClose.setVisibility(View.GONE);
       /* if(AppPreference.getBusinessMainCategory(NewStartUpActivty.this).equals("04"))
        {
            autoPaymentLayout.setVisibility(View.VISIBLE);
            autoPaymentCheck.setVisibility(View.VISIBLE);
        }*/
        if (AppPreference.getAutoPayment(NewStartupActivity.this)) {
            autoPaymentBT.setTypeface(autoPaymentBT.getTypeface(), Typeface.BOLD);
            autoPaymentBT.setText(getString(R.string.turn_off_autopayments));
            autoPaymentCheck.setChecked(true);
        } else {
            autoPaymentBT.setText(getString(R.string.click_button));
            autoPaymentBT.setTypeface(autoPaymentBT.getTypeface(), Typeface.NORMAL);
            autoPaymentBT.setTextColor(getResources().getColor(R.color.black));
            autoPaymentCheck.setChecked(false);
        }
        CallingApIS callingApIS = new CallingApIS(NewStartupActivity.this, NewStartupActivity.this, 0);
        final BusinessAreaListAdapter adp = new BusinessAreaListAdapter(this, shopDetails, callingApIS, dialogBusinessView);
        listBusinessFound.setAdapter(adp);

        autoPaymentCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppPreference.getAutoPayment(NewStartupActivity.this)) {
                    AppPreference.setAutoPayment(NewStartupActivity.this, false);
                    autoPaymentBT.setText(getString(R.string.click_button));
                    autoPaymentBT.setTypeface(autoPaymentBT.getTypeface(), Typeface.NORMAL);
                    autoPaymentCheck.setChecked(false);
                    AutoPaymentInterface.getInstance().findMerchantStatus();
                } else {
                    AppPreference.setAutoPayment(NewStartupActivity.this, true);
                    autoPaymentBT.setText(getString(R.string.turn_off_autopayments));
                    autoPaymentBT.setTypeface(autoPaymentBT.getTypeface(), Typeface.BOLD);
                    autoPaymentCheck.setChecked(true);
                    AutoPaymentInterface.getInstance().findMerchantStatus();
                }
            }
        });
        autoPaymentClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogBusinessView.dismiss();
                // AutoPaymentInterface.getInstance().findMerchantStatus();
            }
        });
        dialogBusinessView.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                // AutoPaymentInterface.getInstance().findMerchantStatus();
            }
        });
        dialogBusinessView.show();
    }

    /*public void dialogAddNewMerchant(final String cellID) {

        final GPSTracker gpsTracker = new GPSTracker(NewStartupActivity.this);
        final Dialog dialog = new Dialog(NewStartupActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.search_dialog);
        CustomTextView textView = (CustomTextView) dialog.findViewById(R.id.textViewTitle);
        final CustomEdittext edittext = (CustomEdittext) dialog.findViewById(R.id.et_search);
        CustomTextView btnSearch = (CustomTextView) dialog.findViewById(R.id.btn_search);
        textView.setText(getString(R.string.auto_payment_title));
        final CallingApIS callingApIS = new CallingApIS(this, this, 0);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!edittext.getText().toString().isEmpty()) {
                    if (!DBHelper.getIsShopNameExist(edittext.getText().toString())) {
                        businessName = edittext.getText().toString();
                        businessCellID = cellID;
                        callingApIS.addMerchantShopDetails(businessName, gpsTracker.getLatitude() + "", "" + gpsTracker.getLongitude(), cellID);

                        dialog.dismiss();
                    } else
                        showToast(getString(R.string.name_already_exixts));
                }
            }
        });

        dialog.show();
    }
*/

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        //retrieve scan result
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        System.out.println("scanningResult--" + scanningResult);
        if (scanningResult != null) {
            //we have a result
            codeContent = scanningResult.getContents();
            codeFormat = scanningResult.getFormatName();

            // display it on screen
            // formatTxt.setText("FORMAT: " + codeFormat);
            // contentTxt.setText("CONTENT: " + codeContent);
            showToast(codeContent);

        } else {
            showToast((R.string.no_scan_data));
        }
    }

    private void SimError() {

        LayoutInflater li = LayoutInflater.from(NewStartupActivity.this);
        View promptsView = li.inflate(R.layout.alert_window, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(NewStartupActivity.this);
        alertDialogBuilder.setView(promptsView);
        final CustomTextView userInput = (CustomTextView) promptsView.findViewById(R.id.alert);
        userInput.setText(getResources().getString(R.string.no_sim));
        CustomButton ok = (CustomButton) promptsView.findViewById(R.id.btn_settings);
        ok.setText(R.string.dialog_ok);

        final AlertDialog dialog1 = alertDialogBuilder.create();
        // set dialog message
        ok.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog1.dismiss();
                finish();
            }
        });
        dialog1.show();
    }

    @Override
    public void showNewUpdateApp(final File files, String message, boolean enable, final int version) {

        final Dialog dialog = new Dialog(this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.newupdate_dialog);
        CustomTextView userInput = (CustomTextView) dialog.findViewById(R.id.message);
        CustomTextView tvOK = (CustomTextView) dialog.findViewById(R.id.tv_ok);
        CustomTextView tvCancel = (CustomTextView) dialog.findViewById(R.id.tv_cancel);
        tvCancel.setVisibility(View.GONE);
        userInput.setTypeface(Utils.getFont(this), Typeface.BOLD);
        dialog.setCancelable(false);
        userInput.setText(message);
        tvOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(Intent.ACTION_VIEW);
                intent1.setDataAndType(Uri.fromFile(files), "application/vnd.android.package-archive");
                intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent1);
                try {
                    System.exit(1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialog.dismiss();

            }
        });
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppPreference.setVersionLast(NewStartupActivity.this, version);
            }
        });
        if (enable) {
            tvCancel.setVisibility(View.VISIBLE);
            dialog.setCancelable(true);
        }

        dialog.show();
    }
}