package mm.dummymerchant.sk.merchantapp;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.DatePicker;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.GetNumberWithCountryCode;
import mm.dummymerchant.sk.merchantapp.Utils.Utils;
import mm.dummymerchant.sk.merchantapp.adapter.TransationAdapter;
import mm.dummymerchant.sk.merchantapp.adapter.UpdateVariable;
import mm.dummymerchant.sk.merchantapp.customView.CustomEdittext;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
import mm.dummymerchant.sk.merchantapp.db.DBHelper;
import mm.dummymerchant.sk.merchantapp.httpConnection.CallingApIS;
import mm.dummymerchant.sk.merchantapp.model.CashierModel;
import mm.dummymerchant.sk.merchantapp.model.ConstactModel;
import mm.dummymerchant.sk.merchantapp.model.TransationModel;



public class NewTransationDitails extends BaseActivity  implements SwipeRefreshLayout.OnRefreshListener, UpdateVariable {

    public static int search_Paramter = 3;
    public ArrayList<TransationModel> mTranstionList;
    public ArrayList<TransationModel> mTransition_selected = new ArrayList<TransationModel>();
    public ImageView backButton;
    public CustomEdittext search2;
    public ImageView buttonLogout;
    public CustomTextView mTitleOfHeader,mTotal,mTotalAmount;
    public RelativeLayout mcustomHeaderBg;
    public CustomTextView amount_, date_;
    public LinearLayout custom_action_bar_lay_submit_log, custom_action_bar_lay_back,ll_total;
    ListView mTramsationDetails;
    ArrayList<TransationModel> mTranstionListTemp = new ArrayList<TransationModel>();
    TransationAdapter adapter;
    DBHelper db;
    SpannableString span3;
    int year, month, day;
    PopupWindow popupWindow;
    Filter nameFilter;
    private SwipeRefreshLayout swipeView;
    private boolean calling=true;
    String last_search_amount="", last_search_contact="", last_search_date="";
    private int preLast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.last_transaction_details);
        span3 = new SpannableString(getResources().getString(R.string.kyats));
        db = new DBHelper(getApplicationContext());
       // setTransactionDetails();
        setCustomActionBar_details();


        init();//  initializing variable and search parameter


        swipeView = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        swipeView.setOnRefreshListener(this);
        swipeView.setColorSchemeColors(Color.GRAY, Color.GREEN, Color.BLUE,
                Color.RED, Color.CYAN);
        swipeView.setDistanceToTriggerSync(20);// in dips
        swipeView.setSize(SwipeRefreshLayout.DEFAULT);// LARGE also can



        mTramsationDetails.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {
            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                final int lastItem = firstVisibleItem + visibleItemCount;
                if (lastItem == totalItemCount) {
                    if (preLast != lastItem) {
                        if (calling)
                        {
                            if (last_search_amount.equals("") && last_search_contact.equals("") && last_search_date.equals("")) {
                                CallingApIS api = new CallingApIS(NewTransationDitails.this, NewTransationDitails.this);
                                api.CallLastTransationHistoryNEW(AppPreference.getPasword(NewTransationDitails.this), String.valueOf(mTransition_selected.size()), String.valueOf(10));
                                preLast = lastItem;
                            }

                        }
                        else
                            showToast(R.string.no_internet);

                    }
                }
            }
        });


        setTotal();
    }

    /**
     * ******************** back button to finish activity**********************************
     */
  //  private void


    /**
     * ******************** Touch event Handler to dismiss popwindow on outside touch **********************************
     */


    @Override
    public boolean dispatchTouchEvent(@NonNull MotionEvent ev) {

        if (popupWindow != null) {
            if (popupWindow.isShowing())
                popupWindow.dismiss();
            popupWindow = null;
        }

        return super.dispatchTouchEvent(ev);
    }

    /**
     * ******************** filter for search , both search by date and search by amount **********************************
     */

    public void filterInit() {

        nameFilter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                if (constraint != null) {
                    if (mTranstionList == null) {
                        return null;
                    } else if (mTranstionList.size() == 0) {
                        return null;
                    }

                    mTransition_selected.clear();
                    if (constraint.equals("")) {
                        for (TransationModel temp : mTranstionList) {
                            mTransition_selected.add(temp);
                        }
                    }


                    for (TransationModel customer : mTranstionList) { // generate filter arraylist according to constraint
                        if (search_Paramter == 1) {
                            if (customer.getMerchantName().toLowerCase().contains(constraint.toString().toLowerCase())) {
                                mTransition_selected.add(customer);
                            }
                        }

                        if (search_Paramter == 3) {
                            String amount = customer.getAmount();
                            if (Utils.formatedAmountwithoutMMK(customer.getAmount(), NewTransationDitails.this).toString().trim().equals(constraint.toString().trim())) {
                                mTransition_selected.add(customer);
                            }
                        }
                        if (search_Paramter == 2) {
                            if (customer.getDate().toLowerCase().contains(constraint.toString().toLowerCase())) {
                                mTransition_selected.add(customer);
                            }
                        }
                    }

                    if (mTransition_selected != null) {

                        if (mTransition_selected.size() > 0) {

                            Collections.sort(mTranstionList);

                        }

                    }

                    FilterResults filterResults = new FilterResults();
                    filterResults.values = mTransition_selected;
                    filterResults.count = mTransition_selected.size();
                    return filterResults;
                } else {
                    return new FilterResults();
                }
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    adapter.notifyDataSetChanged();
                    setTotal();
                } else {
                    adapter.notifyDataSetChanged();
                    setTotal();
                }
            }
        };


    }

    /**
     * ******************** intialize param **********************************
     */

    private void init() {

        adapter = new TransationAdapter(mTransition_selected,this);
        mTramsationDetails = (ListView) findViewById(R.id.last_tranation_historydetails_listview);
        ((TextView) findViewById(R.id.last_transation_history_textview_row_sender)).setTypeface(Utils.getFont(this));
        ((TextView) findViewById(R.id.last_transation_history_textview_destination)).setTypeface(Utils.getFont(this));
        ((TextView) findViewById(R.id.last_transation_history_textview_row_transation_type)).setTypeface(Utils.getFont(this));
        ((TextView) findViewById(R.id.last_transation_history_textview_row_amount)).setTypeface(Utils.getFont(this));
        ((TextView) findViewById(R.id.last_transation_history_textview_row_transation_date)).setTypeface(Utils.getFont(this));

        amount_ = (CustomTextView) findViewById(R.id.amount_);
       // amount_.setText(getResources().getString(R.string.amount_a) + " " + "\ud83d\udcb0");
        date_ = (CustomTextView) findViewById(R.id.date_);
        date_.setText(getResources().getString(R.string.date) + " " + "\ud83d\udcc5");

        if (Utils.isConnectedToInternet(this)) {
            CallingApIS api= new CallingApIS(this,this);

           api.CallLastTransationHistoryNEW(AppPreference.getPasword(NewTransationDitails.this),"0",String.valueOf("10"));
            mTransition_selected.clear();

            mTranstionList = db.getTransationDetails(MASTER_TAG,this);

            if(AppPreference.getMasterOrCashier(this).equalsIgnoreCase(CASHIER))
                mTranstionList=db.getTransationDetails(SliderScreen.present_Cashier_Id,this);

            if (mTranstionList != null)
                for (TransationModel temp1 : mTranstionList) {
                    mTransition_selected.add(temp1);
                }
            if (mTranstionList.size() > 0) {
                Collections.sort(mTranstionList);
                adapter = new TransationAdapter(mTransition_selected,this);
                mTramsationDetails.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                setTotal();
            }
        } else {

            showToast(R.string.no_internet);

            mTransition_selected.clear();
            mTranstionList = db.getTransationDetails(MASTER_TAG,this);

            if(AppPreference.getMasterOrCashier(this).equalsIgnoreCase(CASHIER)) {
                mTranstionList = db.getTransationDetails(SliderScreen.present_Cashier_Id,this);

                int size = db.getTransationDetails(SliderScreen.present_Cashier_Id,this).size();

            }

            if (mTranstionList != null)
                for (TransationModel temp1 : mTranstionList) {
                    mTransition_selected.add(temp1);
                }
            if (mTranstionList.size() > 0) {
                Collections.sort(mTranstionList);
                adapter = new TransationAdapter(mTransition_selected,this);
                mTramsationDetails.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                setTotal();
            }
        }
        filterInit();//  filter initialization

    }

    @Override
    public <T> void response(int resultCode, T data) {


        if (resultCode == REQUEST_TYPE_TRANSINFO_NEW)
        {

            mTransition_selected.clear();
            mTranstionList.clear();

            mTranstionList = db.getTransationDetails(MASTER_TAG,this);

            if(AppPreference.getMasterOrCashier(this).equalsIgnoreCase(CASHIER))
                mTranstionList=db.getTransationDetails(SliderScreen.present_Cashier_Id,this);

            if (mTranstionList != null)
                for (TransationModel temp1 : mTranstionList) {
                    mTransition_selected.add(temp1);
                }

            if (mTranstionList.size() > 0) {
                Collections.sort(mTranstionList);
                adapter = new TransationAdapter(mTransition_selected,this);
                mTramsationDetails.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                setTotal();
            }
        }
    }



    public void showPopup() {
        if (popupWindow == null) {
            LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
            View popupView = layoutInflater.inflate(R.layout.popup_list, null);
            popupWindow = new PopupWindow(popupView, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            final CustomEdittext txtSearchbycontact = (CustomEdittext) popupView.findViewById(R.id.txtSearchbycontact);
            final CustomTextView sortByDateTxtView = (CustomTextView) popupView.findViewById(R.id.txtSortDate);
            final CustomEdittext sortByAmountTxtView = (CustomEdittext) popupView.findViewById(R.id.txtSortAmount);
            final CustomTextView refershTxtView = (CustomTextView) popupView.findViewById(R.id.txtRefersh);
            Drawable img = getResources().getDrawable(R.drawable.searchby_date_icon);
            if (img != null) {
                img.setBounds(0, 0, 55, 55);
                sortByDateTxtView.setCompoundDrawables(img, null, null, null);
            }
            Drawable img1 = getResources().getDrawable(R.drawable.searchby_phone_icon);
            if (img1 != null) {
                img1.setBounds(0, 0, 55, 55);
                txtSearchbycontact.setCompoundDrawables(img1, null, null, null);
            }
            Drawable img2 = getResources().getDrawable(R.drawable.searchby_amount_icon);
            if (img2 != null) {
                img2.setBounds(0, 0, 55, 55);
                sortByAmountTxtView.setCompoundDrawables(img2, null, null, null);
            }
            Drawable img3 = getResources().getDrawable(R.drawable.refresh_icon_r);
            if (img3 != null) {
                img3.setBounds(0, 0, 55, 55);
                refershTxtView.setCompoundDrawables(img3, null, null, null);
            }
            txtSearchbycontact.setImeOptions(EditorInfo.IME_ACTION_DONE);
            txtSearchbycontact.setInputType(InputType.TYPE_CLASS_PHONE);
            refershTxtView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    calling=true;
                    CallingApIS api = new CallingApIS(NewTransationDitails.this, NewTransationDitails.this);
                    api.CallLastTransationHistoryNEW(AppPreference.getPasword(NewTransationDitails.this),String.valueOf(mTransition_selected.size()),String.valueOf(10));
                    last_search_contact = "";
                    last_search_amount = "";
                    last_search_date = "";
                    txtSearchbycontact.setText("");
                    sortByAmountTxtView.setText("");
                    sortByDateTxtView.setText("");
                    //  mTransition_selected.clear();
                    if (popupWindow != null) {
                        if (popupWindow.isShowing())
                            popupWindow.dismiss();
                        popupWindow = null;
                    }
                }
            });
            txtSearchbycontact.setImeOptions(EditorInfo.IME_ACTION_DONE);
            txtSearchbycontact.setInputType(InputType.TYPE_CLASS_PHONE);
            txtSearchbycontact.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    nameFilter.filter(editable.toString());
                    last_search_contact = editable.toString();
                }
            });
            txtSearchbycontact.setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (search_Paramter != 1) {
                        mTransition_selected.clear();

                        search_Paramter = 1;
                        txtSearchbycontact.setInputType(InputType.TYPE_CLASS_PHONE);

                        if (!sortByDateTxtView.equals("")) {
                            sortByDateTxtView.setText("");
                        }
                        if (!sortByAmountTxtView.equals("")) {
                            sortByAmountTxtView.setText("");
                        }

                        sortByAmountTxtView.setText("");
                        sortByDateTxtView.setText("");
                        last_search_amount = "";
                        last_search_date = "";

                    }

                    return false;
                }
            });


            sortByDateTxtView.setInputType(InputType.TYPE_CLASS_TEXT);
            sortByDateTxtView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    search_Paramter = 2;
                    mTransition_selected.clear();
                    sortByDateTxtView.setInputType(InputType.TYPE_CLASS_TEXT);
                    txtSearchbycontact.setText("");
                    sortByAmountTxtView.setText("");
                    if (!txtSearchbycontact.equals("")) {
                        txtSearchbycontact.setText("");
                    }
                    if (!sortByAmountTxtView.equals("")) {
                        sortByAmountTxtView.setText("");
                    }
                    last_search_contact = "";
                    last_search_amount = "";
                    last_search_date = "";


                    DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {


                        @Override
                        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {

                            if (arg0.isShown()) {
                                search_Paramter = 2;
                                SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
                                Calendar newDate = Calendar.getInstance();
                                newDate.set(arg1, arg2, arg3);
                                year = arg1;
                                month = arg2;
                                day = arg3;
                                final DBHelper dbHelper = new DBHelper(NewTransationDitails.this);
                                mTransition_selected.clear();
                                /*********************** Get data from Database by date***********************************/
                                mTransition_selected = dbHelper.getTransationDetailsByDate("date", dateFormatter.format(newDate.getTime()));
                                /*****************************************************************************************/
                                sortByDateTxtView.setText(newDate.getTime().toString());
                                last_search_date = newDate.getTime().toString();

                                if (mTransition_selected.size() > 0) {
                                    Collections.sort(mTransition_selected);
                                    adapter = new TransationAdapter(mTransition_selected, NewTransationDitails.this);
                                    mTramsationDetails.setAdapter(adapter);
                                    adapter.notifyDataSetChanged();
                                } else {
                                    mTranstionListTemp.clear();
                                    mTransition_selected.clear();
                                    adapter = new TransationAdapter(mTransition_selected, NewTransationDitails.this);
                                    mTramsationDetails.setAdapter(adapter);
                                    adapter.notifyDataSetChanged();
                                }
                                setTotal();
                                year = newDate.get(Calendar.YEAR);
                                month = newDate.get(Calendar.MONTH) + 1;
                                day = newDate.get(Calendar.DAY_OF_MONTH);
                                newDate.set(arg1, arg2, arg3);
                            }
                        }


                    };
                    Calendar calendar = Calendar.getInstance();
                    DatePickerDialog Datedialog = new DatePickerDialog(NewTransationDitails.this, myDateListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                    Datedialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            if (which == DialogInterface.BUTTON_NEGATIVE)
                            {
                                last_search_date = "";
                                sortByDateTxtView.setText("");
                                mTranstionList.clear();
                                mTransition_selected.clear();
                            //    mTranstionList = db.getTransationDetails();
                                if(AppPreference.getMasterOrCashier(NewTransationDitails.this).equalsIgnoreCase(CASHIER))
                                    mTranstionList=db.getTransationDetails(SliderScreen.present_Cashier_Id,NewTransationDitails.this);
                                else
                                    mTranstionList=db.getTransationDetails(MASTER_TAG,NewTransationDitails.this);

                                if (mTranstionList != null) {
                                    for (TransationModel temp1 : mTranstionList) {
                                        mTransition_selected.add(temp1);
                                    }
                                }
                                if (mTranstionList != null) {
                                    if (mTranstionList.size() > 0) {
                                        Collections.sort(mTranstionList);
                                        adapter = new TransationAdapter(mTransition_selected, NewTransationDitails.this);
                                        mTramsationDetails.setAdapter(adapter);
                                        adapter.notifyDataSetChanged();
                                        setTotal();
                                    }
                                }
                            }
                        }
                    });
                    Datedialog.show();
                    search_Paramter = 2;
                    popupWindow.dismiss();
                    popupWindow = null;
                }
            });

            sortByAmountTxtView.setImeOptions(EditorInfo.IME_ACTION_DONE);
            sortByAmountTxtView.setInputType(InputType.TYPE_CLASS_PHONE);
            sortByAmountTxtView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (search_Paramter != 3) {
                        mTransition_selected.clear();
                        if (!sortByDateTxtView.equals("")) {
                            sortByDateTxtView.setText("");
                        }
                        if (!txtSearchbycontact.equals("")) {
                            txtSearchbycontact.setText("");
                        }

                        search_Paramter = 3;
                        sortByAmountTxtView.setInputType(InputType.TYPE_CLASS_PHONE);
                        txtSearchbycontact.setText("");
                        sortByDateTxtView.setText("");
                        last_search_contact = "";
                        last_search_date = "";
                    }
                    return false;
                }

            });

            sortByAmountTxtView.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    nameFilter.filter(editable.toString());
                    last_search_amount = editable.toString();
                }
            });
            sortByAmountTxtView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (popupWindow != null)
                        if (popupWindow.isShowing()) {
                            popupWindow.dismiss();
                        }
                    return false;
                }
            });
            txtSearchbycontact.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (popupWindow != null)
                        if (popupWindow.isShowing()) {
                            popupWindow.dismiss();
                        }
                    return false;
                }
            });


            if (popupWindow.isShowing()) {
                popupWindow.dismiss();
            } else {
                if (last_search_date != null) {
                    if (!last_search_date.equals("")) {
                        sortByDateTxtView.setText(last_search_date);
                    }
                }
                if (last_search_amount != null) {
                    if (!last_search_amount.equals("")) {
                        sortByAmountTxtView.setText(last_search_amount);
                    }
                }
                if (last_search_contact != null) {
                    if (!last_search_contact.equals("")) {
                        txtSearchbycontact.setText(last_search_contact);
                    }
                }
                popupWindow.setOutsideTouchable(true);
                popupWindow.setFocusable(true);
                popupWindow.setBackgroundDrawable(new BitmapDrawable());
                popupWindow.update();
                popupWindow.showAsDropDown(buttonLogout, -250, 0);
            }
        } else {
            popupWindow.dismiss();
            popupWindow = null;
        }
    }


    /**
     * ******************** Action bar customization**********************************
     */
    private void setCustomActionBar_details() {

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater inflater = LayoutInflater.from(this);
        View v = inflater.inflate(R.layout.header_recieved_money, null);
        custom_action_bar_lay_back = (LinearLayout) v.findViewById(R.id.custom_action_bar_lay_back);
        backButton = (ImageView) v.findViewById(R.id.custom_actionbar_application_left_image);
        //   custom_action_bar_lay_back=(RelativeLayout) v.findViewById(R.id.custom_action_bar_lay_back);
        custom_action_bar_lay_submit_log = (LinearLayout) v.findViewById(R.id.custom_action_bar_lay_pay);
        buttonLogout = (ImageView) v.findViewById(R.id.custom_actionbar_application_right_image);
        mTitleOfHeader = (CustomTextView) v.findViewById(R.id.custom_actionbar_application_textview_title_of_application);
        mcustomHeaderBg = (RelativeLayout) v.findViewById(R.id.custom_action_bar_application);

        ll_total=(LinearLayout)v.findViewById(R.id.custom_total_today1);
        mTotal=(CustomTextView)v.findViewById(R.id.custom_tv_total1);
        mTotalAmount=(CustomTextView)v.findViewById(R.id.custom_tv_total2);

        actionBar.setCustomView(v);

        ll_total.setVisibility(View.VISIBLE);
        mTotal.setVisibility(View.VISIBLE);
        mTotalAmount.setVisibility(View.VISIBLE);
       // mTotal.setSelected(true);
       // mTotal.setText("hsfllllllllllkhvklvugsfiygsifgliczsitfsoshlzb");

        mTitleOfHeader.setText(getResources().getString(R.string.transaction_detail));
        search2 = (CustomEdittext) v.findViewById(R.id.search_transaction_1);
        search2.setVisibility(View.INVISIBLE);
        search2.setHint(getResources().getString(R.string.search_byamount));

        buttonLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopup();
            }
        });


        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        actionBar.setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        Toolbar parent = (Toolbar) v.getParent();//first get parent toolbar of current action bar
        parent.setContentInsetsAbsolute(0, 0);
        buttonLogout.setVisibility(View.VISIBLE);
        buttonLogout.setImageDrawable(getResources().getDrawable(R.drawable.more_icon));
        mcustomHeaderBg.setBackgroundColor(getResources().getColor(R.color.metro_blue));





    }

    public void Converter(String no, ImageView v, CustomTextView ctv, String type) {
        String countryCode = "", countryName = "", flagStr = "";

        int flag = -1;
        if (no.startsWith("+"))
            no = no.replace("+", "00");
        no = convertValidNumber(no);
        int id = -1;

        if (no.startsWith("+") || no.startsWith("00")) {

            if(no.length()>6) {
                String code = no.substring(0, 6);

                for (int i = 0; i < listOfCountryCodesAndNames.size(); i++) {

                    ArrayList<String> dialingCodes = listOfCountryCodesAndNames.get(i).getListOfDialingCodes();

                    if (dialingCodes != null) {

                        for (int j = 0; j < dialingCodes.size(); j++) {

                            String countryId = dialingCodes.get(j).replace("+", "00");

                            if (code.contains(countryId)) {
                                System.out.println(i);
                                id = i;
                                int length = countryId.length();
                                no = no.substring(3);
                                countryCode = listOfCountryCodesAndNames.get(id).getCountryCode();
                                countryName = listOfCountryCodesAndNames.get(id).getCountryName();
                                flag = listOfCountryCodesAndNames.get(i).getCountryImageId();
                                flagStr = String.valueOf(flag);
                                ctv.setText("  (" + countryCode + ")" + no);
                                Drawable img = NewTransationDitails.this.getResources().getDrawable(flag);
                                img.setBounds(0, 0, 60, 60);
                                ctv.setCompoundDrawables(img, null, null, null);

                                break;
                            }
                        }

                    } else {

                        String countryId = "00" + listOfCountryCodesAndNames.get(i).getCountryCode().substring(1);

                        if (code.contains(countryId)) {
                            System.out.println(i);
                            id = i;
                            int length = countryId.length();
                            no = no.substring(length);
                            countryCode = listOfCountryCodesAndNames.get(id).getCountryCode();
                            countryName = listOfCountryCodesAndNames.get(id).getCountryName();
                            flag = listOfCountryCodesAndNames.get(i).getCountryImageId();
                            flagStr = String.valueOf(flag);
                            ctv.setText("  (" + countryCode + ")" + no);
                            Drawable img = NewTransationDitails.this.getResources().getDrawable(flag);
                            img.setBounds(0, 0, 60, 60);
                            ctv.setCompoundDrawables(img, null, null, null);

                            break;
                        }
                    }


                }
            }

            if (countryCode.equals("+95")) {
                no = "0" + no;
            }

            if (id == -1) {
                if (no.startsWith("0")) {
                    if (!AppPreference.getCountryCode(this).contains("+95"))
                        no = no.substring(1);
                } else {
                    no = "0" + no;
                }
                countryCode = AppPreference.getCountryCode(this);
                flag = AppPreference.getCountryImageID(this);
                countryName = AppPreference.getCountryName(this);
                flagStr = String.valueOf(flag);
                Drawable img = NewTransationDitails.this.getResources().getDrawable(flag);
                img.setBounds(0, 0, 60, 60);
                ctv.setCompoundDrawables(img, null, null, null);
                ctv.setText("(" + AppPreference.getCountryCode(this) + ")" + no);
            }


            if (type.equals("Dr")) {
                Drawable img = NewTransationDitails.this.getResources().getDrawable(R.drawable.get);
                img.setBounds(0, 0, 40, 40);
                v.setImageDrawable(img);
            } else if (type.equals("Cr")) {
                Drawable img = NewTransationDitails.this.getResources().getDrawable(R.drawable.blu);
                img.setBounds(0, 0, 40, 40);
                v.setImageDrawable(img);
            }


        }


    }

    @Override
    public void onRefresh() {

        swipeView.postDelayed(new Runnable() {

            @Override
            public void run() {
                              init();
                swipeView.setRefreshing(false);
            }
        }, 1000);

    }



    public void hideKeyboard() {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }


    @Override
    public void onBackPressed()
    {
       finish();
    }

    @Override
    public void callingReset(boolean calling) {

        this.calling = calling;
    }


    public double getTotalofToday()
    {
        double total=0.0;

        for(int i=0;i<mTranstionList.size();i++)
        {
            TransationModel model=mTranstionList.get(i);
         String amount=   model.getAmount();
            if(model.getDate()!=null) {
                if(!model.getDate().equalsIgnoreCase("")) {
                    Date dt = convertStringToDate(model.getDate());
                    if (checkCurrentDayorNot(dt)) {
                        try {
                            double amt = Double.parseDouble(amount);
                            total = total + amt;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

        }

        return total;
    }

  void  setTotal()
  {
      mTotalAmount.setText(""+getTotalofToday());
  }


}
