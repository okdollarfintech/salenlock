package mm.dummymerchant.sk.merchantapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;


import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.Utils;
import mm.dummymerchant.sk.merchantapp.customView.CustomButton;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
import mm.dummymerchant.sk.merchantapp.customView.RoundedImageView;
import mm.dummymerchant.sk.merchantapp.db.DBHelper;
import mm.dummymerchant.sk.merchantapp.httpConnection.AllInerfaces;

import mm.dummymerchant.sk.merchantapp.model.ConstactModel;
import mm.dummymerchant.sk.merchantapp.model.NetworkOperatorModel;
import mm.dummymerchant.sk.merchantapp.model.Remarks;
import mm.dummymerchant.sk.merchantapp.model.TransationModel;

public class NotificationActivity extends Activity implements OnClickListener,AllInerfaces.CallFinishInterface,AllInerfaces.CallSplashInterface,AllInerfaces.CallRemarkInterface {

    ImageView imageClose;
    ArrayList<TransationModel> receivedTras = new ArrayList<TransationModel>();
    CustomTextView mTransId, mAgentNumber, mDate, mAmount, mName, mCountryCode,mTime,Header,notofication_transId;
    ImageView iv;
    int id = 0;
    DBHelper db;
    ArrayList<TransationModel> unreadTranscationList;
    CustomButton mPrevious, mNext,notification_remarks;
    RoundedImageView oks;
    int totalSize = 0;
    RoundedImageView mUserPhoto;
    int autoPaymentCounter = 0;
    public ArrayList<NetworkOperatorModel> listOfCountryCodesAndNames;
    private CustomTextView mmk;
    String contactNo="";
    String transID;



    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification_screen1);
        listOfCountryCodesAndNames = new NetworkOperatorModel().getListOfNetworkOperatorModel(this);
        db = new DBHelper(this);
        init();
        unreadTranscationList = db.receivedUnreadTranscation();
        totalSize = unreadTranscationList.size();
        if (totalSize > 0) {
            updateText(0);
            id = 1;
        }
    }

    private void init() {
        mmk = (CustomTextView) findViewById(R.id.mmk);
        notofication_transId= (CustomTextView) findViewById(R.id.notofication_transId);
        mTransId = (CustomTextView) findViewById(R.id.notofication_transId_input);
        mCountryCode = (CustomTextView) findViewById(R.id.received_money_text_view_countrycode);
        mAgentNumber = (CustomTextView) findViewById(R.id.notofication_agnetNumber_input);
        imageClose = (ImageView) findViewById(R.id.image_close);
        mDate = (CustomTextView) findViewById(R.id.notofication_date_re);
        Header = (CustomTextView) findViewById(R.id.one);
        mTime = (CustomTextView) findViewById(R.id.notofication_transId_date_re_input);
        mAmount = (CustomTextView) findViewById(R.id.notofication_transId_amount_input);
        mName = (CustomTextView) findViewById(R.id.notofication_agnetName_input);
        mUserPhoto = (RoundedImageView) findViewById(R.id.received_money_imagge_View_user_image);
        imageClose.setOnClickListener(this);
        iv = (ImageView) findViewById(R.id.arrow);
        iv.setOnClickListener(this);
        mPrevious = (CustomButton) findViewById(R.id.notofication_Previous);
        mNext = (CustomButton) findViewById(R.id.notofication_transId_Next);
        notification_remarks= (CustomButton) findViewById(R.id.notofication_remarks);
        mPrevious.setOnClickListener(this);
        mNext.setOnClickListener(this);
        notification_remarks.setOnClickListener(this);
        VibrateandNotification();
        Header.setTypeface(Utils.getFont(this));
        mTransId.setTypeface(Utils.getFont(this));
        mAgentNumber.setTypeface(Utils.getFont(this));
        mDate.setTypeface(Utils.getFont(this));
        mAmount.setTypeface(Utils.getFont(this));
        mName.setTypeface(Utils.getFont(this));
        mCountryCode.setTypeface(Utils.getFont(this));
        mTime.setTypeface(Utils.getFont(this));
        Header.setTypeface(Utils.getFont(this));
        mPrevious.setTypeface(Utils.getFont(this));
        notification_remarks.setTypeface(Utils.getFont(this));
        mNext.setTypeface(Utils.getFont(this));
        notofication_transId.setTypeface(Utils.getFont(this));
        mmk.setTypeface(Utils.getFont(this));

    }

    @Override
    protected void onResume() {
        super.onResume();
        this.overridePendingTransition(R.anim.activity_slide_down, R.anim.activity_slide_right);
        //cancelNotification();
    }

    private void VibrateandNotification() {
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(1000);
        v.vibrate(1000);
        v.vibrate(1000);
        sound();
        sound();
        autoPaymentCounter = autoPaymentCounter + 1;


        //Will be used in future
           /* if (autoPaymentCounter > 3) {
                WirelessUtils.getInstance().setBluetooth(true, NotificationActivity.this);
                WirelessUtils.getInstance().changeBluetoothName(true, NotificationActivity.this);
                final WifiManager

                s.isWifiEnabled())
                WirelessUtils.getInstance().setHotSpotON(NotificationActivity.this);
                AppPreference.setMerchantStatus(NotificationActivity.this, true);
                Intent background = new Intent(NotificationActivity.this, CheckCellID.class);
                NotificationActivity.this.startService(background);
            }*/


    }

    private void sound() {
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.image_close:
                finish();
                break;

            case R.id.notofication_Previous:
                boolean checkk=false;

                checkk=isContactPresent(contactNo);
                if(!checkk) {
                    new Utils().addContactDialog(NotificationActivity.this, contactNo, this,this,2);
                }
                else
                callSplash();
                break;

            case R.id. notofication_remarks:

               new Utils().addRemarksDialog(NotificationActivity.this,getRemarks(),this);

                break;

            case R.id.notofication_transId_Next:
                Log.e("closecalled", "...closecalled");
                boolean check=false;

               check=isContactPresent(contactNo);

            if(!check) {
                new Utils().addContactDialog(NotificationActivity.this, contactNo, this,this,1);
            }
                else
                finish();
            break;


        }
    }

    private void updateText(int id)  {
        if (id >= 0 && id < unreadTranscationList.size()) {
            if (unreadTranscationList.get(id).getDestination().equals("")) {
                mAgentNumber.setVisibility(View.GONE);
                mName.setVisibility(View.GONE);
//                mUserPhoto.setVisibility(View.GONE);
            } else {
                mAgentNumber.setVisibility(View.VISIBLE);
                mName.setVisibility(View.VISIBLE);
                //        mUserPhoto.setVisibility(View.VISIBLE);
            }
            mName.setText(getString(R.string.unknown));
            mTransId.setText(unreadTranscationList.get(id).getTransatId());
            matchCountryCode(unreadTranscationList.get(id).getDestination());
            mAmount.setText(Utils.formatedAmountAbhi(unreadTranscationList.get(id).getAmount(), this));
            String split[]= unreadTranscationList.get(id).getDate().trim().split(" ");
            mDate.setText(split[0]);
            mTime.setText(split[1].replace(".",""));
            // mDate.setText(unreadTranscationList.get(id).getDate());
            transID=unreadTranscationList.get(id).getTransatId();
            setPhoto(id);
        }
    }

    private void matchCountryCode(String no)
    {
        String countryCode = "", countryName = "", flagStr = "";
        int flag = -1;
        try {
            int id = -1;
            if (no.startsWith("00")) {
                String code = no.substring(0, 6);
                mCountryCode.setVisibility(View.VISIBLE);

                for (int i = 0; i < listOfCountryCodesAndNames.size(); i++) {

                    ArrayList<String> dialingCodes = listOfCountryCodesAndNames.get(i).getListOfDialingCodes();
                    if (dialingCodes != null) {
                        for (int j = 0; j < dialingCodes.size(); j++) {
                            String countryId = dialingCodes.get(j).replace("+", "00");

                            if (code.contains(countryId))
                            {
                                System.out.println(i);
                                id = i;
                                int length = countryId.length();
                                no = no.substring(3);
                                countryCode = listOfCountryCodesAndNames.get(id).getCountryCode();
                                countryName = listOfCountryCodesAndNames.get(id).getCountryName();
                                flag = listOfCountryCodesAndNames.get(i).getCountryImageId();
                                break;
                            }
                        }

                    } else {

                        String countryId = "00" + listOfCountryCodesAndNames.get(i).getCountryCode().substring(1);
                        if (code.contains(countryId)) {
                            System.out.println(i);
                            id = i;
                            int length = countryId.length();
                            no = no.substring(length);
                            countryCode = listOfCountryCodesAndNames.get(id).getCountryCode();
                            countryName = listOfCountryCodesAndNames.get(id).getCountryName();
                            flag = listOfCountryCodesAndNames.get(i).getCountryImageId();
                            break;
                        }
                    }
                }

                if (countryCode.equals("+95")) {
                    no = "0" + no;
                }
            }
            if (id == -1) {
                if (no.startsWith("0")) {
                    mCountryCode.setVisibility(View.VISIBLE);
                    if (no.startsWith("0")) {
                        if (!AppPreference.getCountryCode(this).contains("+95"))
                            no = no.substring(1);
                    } else {
                        no = "0" + no;
                    }
                    countryCode = AppPreference.getCountryCode(this);
                    flag = AppPreference.getCountryImageID(this);
                    countryName = AppPreference.getCountryName(this);

                }
            }
            if (no.contains("X")) {
                mCountryCode.setVisibility(View.GONE);
            }else {
                mCountryCode.setText("(" + countryCode + ")");
                if (AppPreference.getCountryImageID(this) != -1) {
                    Drawable img = getResources().getDrawable(flag);
                    img.setBounds(0, 0, 60, 60);
                    mCountryCode.setCompoundDrawables(img, null, null, null);
                }
            }
            mAgentNumber.setText(no);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setPhoto(int id)
    {
        Log.e("notificheck", "..." + unreadTranscationList.get(id).getDestination());
        String number = unreadTranscationList.get(id).getDestination();
        contactNo=number;
        number=number.substring(1);

        if (number.startsWith("09"))
        {
                    // number = "+95" + number;
            if(number!=null)
            {
                if(!number.equals("") && !number.contains("X")){
                    // ConstactModel model = db.getSelectedContact(number);
                    ///////
                    List<ConstactModel> list = new ArrayList<ConstactModel>();
                    Utils utils=new Utils();
                    String jsondata=DBHelper.getJsonContacts();
                    list=utils.getContactListFromJsonString(jsondata);
                    ////////////
                    Log.e("listSizeNot","..."+list.size());
                    /*for(ConstactModel cm:list)
                    {
                        Log.e("modelNotNo","..."+cm.getMobileNo());
                    }*/

                    ConstactModel model = utils.getSelectedContactModel(number,list);
                    Log.e("ncname","..."+model.getName());

                    if (model.getName().equals(""))
                    {
                        // model = db.getSelectedContact(unreadTranscationList.get(id).getDestination());
                        model = utils.getSelectedContactModel(number,list);


                        try {
                            if (!model.getPhotoUrl().equals("")) {
                                Uri u = Uri.parse(model.getPhotoUrl());
                                InputStream inputStream = getContentResolver().openInputStream(u);
                                Drawable d = Drawable.createFromStream(inputStream, u.toString());
                                // d.setBounds(0, 0, 60, 60);
                                mUserPhoto.setImageDrawable(d);

                            } else {
                                mUserPhoto.setImageDrawable(getResources().getDrawable(R.drawable.new_ok_logo));
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            if (!model.getPhotoUrl().equals(""))
                            {
                                Uri u = Uri.parse(model.getPhotoUrl());
                                InputStream inputStream = getContentResolver().openInputStream(u);
                                Drawable d = Drawable.createFromStream(inputStream, u.toString());
                                //d.setBounds(0, 0, 60, 60);
                                mUserPhoto.setImageDrawable(d);
                                mName.setText(model.getName());


                            } else {
                                mUserPhoto.setImageDrawable(getResources().getDrawable(R.drawable.new_ok_logo));
                                mName.setText(model.getName());
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                   // mName.setText(model.getName());

                }
            }


        }
    }

    public static Bitmap getRoundedRectBitmap(Bitmap bitmap) {
        Bitmap result = null;
        try {
            result = Bitmap.createBitmap(200, 200, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(result);

            int color = 0xff424242;
            Paint paint = new Paint();
            Rect rect = new Rect(0, 0, 200, 200);

            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(color);
            canvas.drawCircle(50, 50, 50, paint);
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            canvas.drawBitmap(bitmap, rect, rect, paint);

        } catch (NullPointerException e) {
        } catch (OutOfMemoryError o) {
        }
        return result;


    }

    @Override
    public void finish() {
        super.finish();
        Log.e("notifyFinish1111", "........finish called");

        overridePendingTransition(R.anim.activity_slide_down, R.anim.activity_slide_right);
    }


    @Override
    protected void onStop() {
        super.onStop();
        Log.e("notifystop", "........finish called");

    }

    @Override
    public void callFinish() {

        finish();
    }

  boolean isContactPresent(String contactNo)
  {
      DBHelper db=new DBHelper(NotificationActivity.this);
      db.open();
      String jsonContact=db.getJsonContacts();
      db.close();

      ArrayList<ConstactModel> alist =new Utils().getContactListFromJsonString(jsonContact);
      String checkNo="0"+contactNo.substring(4);

      boolean check=false;

      for(int i=0;i<alist.size();i++)
      {
          ConstactModel model=alist.get(i);

          if(checkNo.equals(model.getMobileNo()))
          {
              Log.e("notifyreturn","...returned");
             return true;

          }
      }
    return  false;
  }


    @Override
    public void callSplash() {
        Intent intent = new Intent(this, SplashScreenActivity.class);
        startActivity(intent);

    }

    @Override
    public void callRemarks() {
    finish();
    }

    public Remarks getRemarks()
    {
        Remarks rem_model;
        String mcc=
        "",mnc="";
        GPSTracker gps=new GPSTracker(this);
        Location loc= gps.getLocation();
        double lat=0.0;
        double longi=0.0;
        if(loc!=null)
        {
            lat = loc.getLatitude();
            longi = loc.getLongitude();
        }
        String cellId=new Utils().getLACAndCID(this);

        TelephonyManager telephonyMgr = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        String networkOperator = telephonyMgr.getNetworkOperator();
        String operator=telephonyMgr.getNetworkOperatorName();


        if (networkOperator != null) {
            mcc = "" + Integer.parseInt(networkOperator.substring(0, 3));
            mnc = "" + Integer.parseInt(networkOperator.substring(3));
        }

        rem_model=new Remarks(transID,cellId,""+lat,""+longi,mnc,mcc,operator);
        return rem_model;
    }
}
