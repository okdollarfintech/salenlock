package mm.dummymerchant.sk.merchantapp;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.Constant;
import mm.dummymerchant.sk.merchantapp.Utils.MySendSMSStatus;
import mm.dummymerchant.sk.merchantapp.Utils.MySendSMS_New;
import mm.dummymerchant.sk.merchantapp.Utils.Utils;
import mm.dummymerchant.sk.merchantapp.customView.CustomButton;
import mm.dummymerchant.sk.merchantapp.customView.CustomEdittext;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
import mm.dummymerchant.sk.merchantapp.introduction.CircularProgressBarLayout;
import mm.dummymerchant.sk.merchantapp.model.NetworkOperatorModel;



public class NumberValidation1 extends BaseActivity implements View.OnClickListener, MySendSMSStatus, CircularProgressBarLayout.OnRetriveClickListener {
    static CustomEdittext mNumber;
    CustomButton mSubmit;
    public static String BROADCAST_INTENT = "com.opt.request";
    public static long OTP_NUMBER = 0;
    boolean isVerified = false;
    static long mSendingDate = 0;
    static CustomTextView  mCountryCode;
    private static ImageView mCountryImage;
    private static NumberValidation1 context;
    private static NetworkOperatorModel model;

    public CircularProgressBarLayout progressDialog;
    private CustomTextView tvCountryCode, tvPressHere;
    static String mobileNumber = "";
    private LinearLayout llCountryCode;
    private static int flag = -1;
    private String countryCode = "", countryName = "";
    int address = 909;
    public static long SENDING_SMS_TIME = 0;
    Calendar calendar;
    boolean isVery = false;
    boolean isSMSReceived = false;
    private ArrayList<NetworkOperatorModel> listOfCountryCodesAndNames;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppPreference.setAuthToken(this, "XX");
        setContentView(R.layout.number_verification);
        listOfCountryCodesAndNames = new NetworkOperatorModel().getListOfNetworkOperatorModel(this);
        init();
        setRegistrationBackground1();
        keyboardDoneButtonCLickLister();
        context = NumberValidation1.this;
        calendar = GregorianCalendar.getInstance();
        Intent intent = getIntent();



        if (AppPreference.getVerifiedIMSINo(this).equalsIgnoreCase(Utils.getSimSubscriberID(this)) && AppPreference.getVerifiedSIMSerial(this).equalsIgnoreCase(Utils.simSerialNumber(this))) {
            Intent call = new Intent(context, NewRegistrationActivity1.class);
            call.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(call);
            context.finish();
        } else {
            voiceSwapSIM(R.raw.sim_card1);
        }
        backFinish();

    }

    private void RegisterBroadCast() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(BROADCAST_INTENT);
        registerReceiver(receiver, filter);
    }

    private void keyboardDoneButtonCLickLister() {
        mNumber.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    validation();
                }
                return false;
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        RegisterBroadCast();
    }

    @Override
    protected void onRestart() {
        super.onRestart();

    }

    BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                if (intent.hasExtra(PARAM_OTP)) {
                    if (intent.hasExtra("verify")) {
                        String verify = intent.getStringExtra("verify");
                        if (verify.equals("true")||verify.equals("swap")){
                            dismissDialog();
                                isVerified = true;
                                AppPreference.setVerifiedSIMSerial(context, Utils.simSerialNumber(context));
                                AppPreference.setVerifiedIMSINo(context, Utils.getSimSubscriberID(context));
                                //AppPreference.setVerifyNumberWithCountryCode(getApplicationContext(), mobileNumber);
                                AppPreference.setVerifyNumber(context, mNumber.getText().toString());
                                AppPreference.setCountryImageIDLogin(context, flag);
                                AppPreference.setCountryCodeFlagNameLogin(getApplicationContext(), countryName, countryCode);
                                Intent call = new Intent(context, NewRegistrationActivity1.class);
                                call.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(call);
                                finish();
                       //         Utils.showCustomToastMsg(context, R.string.verified_no, true);

                        } /*else if (verify.equals("swap")) {
                            AppPreference.setOTP(context, "");
                            showAlert(getString(R.string.sim_swap));
                            voiceSwapSIM(R.raw.sim_card2);
                            dismissDialog();
                        } */else {
                            showMessage(getString(R.string.sim_check_msg));
                            MySendSMS_New.flag++;
                        }
                    }
                }
            }
        }
    };

    @Override
    protected void onStop() {
        Utils.prevLenght = 0;
        dismissDialog();
        try {
            unregisterReceiver(receiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onStop();
    }

    private void init() {
        String mccAndmnc = Utils.getSimOperater(this);
        if (mccAndmnc != null) {
            if (!mccAndmnc.equals("")) {
                model = new NetworkOperatorModel().getNetworkOperatorModel(this, mccAndmnc);

            }
        }

        mCountryImage = (ImageView) findViewById(R.id.img_tv_country_code);
        mCountryCode = (CustomTextView) findViewById(R.id.select_country_code);
        llCountryCode = (LinearLayout) findViewById(R.id.ll_select_country_code);
        mNumber = (CustomEdittext) findViewById(R.id.login_edittext_username);
        mSubmit = (CustomButton) findViewById(R.id.login_button_login);
        tvPressHere = (CustomTextView) findViewById(R.id.btn_press_here);
        tvCountryCode = (CustomTextView) findViewById(R.id.tv_country_code);
        tvCountryCode.setTypeface(null, Typeface.BOLD);
        CustomTextView tvOperatorName = (CustomTextView) findViewById(R.id.tv_operator_name);

        if (model != null && AppPreference.getLanguage(this).equalsIgnoreCase("my"))
        {
            tvCountryCode.setText("(" + model.getCountryCode() + ")");

            String styledText= "သင့္ <font color='#70008000'>SIM Slot 1 (Master SIM)</font>၏ <font color='#70008000'>"+ model.getOperatorName() + " " + model.getCountryName()+ "</font> မိုဘိုင္း ဖုန္းနံပါတ္ကို ႐ိုက္ထည့္ပါ။  <font color= 'red'>သင္အေကာင့္ ျပဳလုပ္ရန္ သင့္ဖုန္းသည္ SIM ကဒ္၂ ကဒ္သံုးျဖစ္ပါက SIM Slot 2 မွ SIM card ကို ခဏ ျဖဳတ္ထားပါ။;</font>";
            //String styledText = "အေကာင့္ဖြင့္ရန္ <font color='red'>SIM Slot 1</font> တြင္ ထည့္ထားေသာ သင္၏ <font color='green'>" + model.getOperatorName() + " " + model.getCountryName() + "</font>  ဖုန္းနံပါတ္ကို ေအာက္တြင္ ႐ိုက္ထည့္ပါ";
            tvOperatorName.setText(Html.fromHtml(styledText), TextView.BufferType.SPANNABLE);

            flag = Constant.listOfFlags[model.getImageIndex()];
            countryCode = model.getCountryCode();
            countryName = model.getCountryName();

            Log.e("modelcountrycode","..."+countryCode);
            Log.e("modelcountryName","..."+countryName);

            mCountryImage.setVisibility(View.VISIBLE);
            mCountryImage.setImageResource(flag);

            if (!model.getCountryCode().equalsIgnoreCase("+95")) {
                mNumber.setText(null);
            } else {
                mNumber.setText(Utils.getMyanmarNumber());
            }
            mCountryCode.setText(countryName + " (" + countryCode + ")");
            mNumber.setSelection(mNumber.length());


        } else if (model != null) {
            tvCountryCode.setText("(" + model.getCountryCode() + ")");
            String styledText = "Please enter your <font color='green'>" + model.getOperatorName() + " " + model.getCountryName() + "</font> mobile number active in <font color='green'>SIM Slot 1 (Master SIM)</font>. <font color= 'red'>To register your account please take out SIM 2 If phone have two SIM</font>";
            tvOperatorName.setText(Html.fromHtml(styledText), TextView.BufferType.SPANNABLE);

            countryCode = model.getCountryCode();
            flag = Constant.listOfFlags[model.getImageIndex()];
            countryName = model.getCountryName();

            mCountryImage.setVisibility(View.VISIBLE);
            mCountryImage.setImageResource(flag);

            if (!model.getCountryCode().equalsIgnoreCase("+95")) {
                mNumber.setText(null);
            } else {
                mNumber.setText(Utils.getMyanmarNumber());
            }
            mCountryCode.setText(countryName + " (" + countryCode + ")");
            mNumber.setSelection(mNumber.length());

        } else if (model == null)
        {
            flag = -1;
            tvOperatorName.setText(getResources().getString(R.string.error_no_network_service));
            tvCountryCode.setText("");
            mNumber.setText(null);
            countryCode = "";
            countryName = getResources().getString(R.string.select_your_country);
            mCountryCode.setText(countryName);
            mCountryImage.setVisibility(View.GONE);
            mCountryCode.setTypeface(Utils.getFont(this), Typeface.BOLD);
        }

        if (!Utils.isEmpty(Utils.getPhoneNumber(this))) {
            if (AppPreference.getCountryCodeLogin(this).equalsIgnoreCase("+1")) {
                mNumber.setText(Utils.getPhoneNumber(this).substring(1));
                mNumber.setSelection(mNumber.length());
            }
        }
        buttonLogout.setOnClickListener(this);
        tvPressHere.setOnClickListener(this);
        llCountryCode.setOnClickListener(this);
        mNumber.addTextChangedListener(new MyTextWatcherForMobileNo());


    }

    @Override
    public <T> void response(int resultCode, T data) {

    }

    private void showMessage(String message)
    {
        final Dialog dialog = new Dialog(this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dailog);
        CustomTextView userInput = (CustomTextView) dialog.findViewById(R.id.dialog_editext);
        CustomTextView title = (CustomTextView) dialog.findViewById(R.id.tv_title);
        CustomTextView tvOK = (CustomTextView) dialog.findViewById(R.id.tv_ok);
        CustomTextView tvCancel = (CustomTextView) dialog.findViewById(R.id.tv_cancel);
        userInput.setText(message);
        title.setVisibility(View.GONE);
        //tvOK.setText(R.string.dailog_yes);
        //tvCancel.setText(R.string.dailog_no);
        title.setTypeface(Utils.getFont(this), Typeface.BOLD);
        dialog.setCancelable(true);
        tvOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                if (progressDialog != null) {
                    progressDialog.dismiss();
                    CircularProgressBarLayout.condownTimer.cancel();
                }

                progressDialog = new CircularProgressBarLayout(NumberValidation1.this, address);
                progressDialog.setCancelable(false);
                progressDialog.Set(NumberValidation1.this);
                progressDialog.show();

                mobileNumber = Utils.getUniversalFormattedMobileNOWithCountryCode(countryCode, mNumber.getText().toString());

                AppPreference.setCountryImageIDLogin(context, flag);
                AppPreference.setVerifyNumberWithCountryCode(getApplicationContext(), mobileNumber);
                //OTP_NUMBER = get4DigitRandomNumber();
               //final String msg = getResources().getString(R.string.sms_otp1) + " " + OTP_NUMBER +""+new   Utils().getDeviceIMEI(NumberValidation1.this)+ getResources().getString(R.string.sms_otp2);

                //AppPreference.setOTP(NumberValidation1.this, msg);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        String msg= Utils.randomGeneratorNumber();
                        AppPreference.setOTP(NumberValidation1.this, msg);
                        new MySendSMS_New(context).sendSMS(mobileNumber, msg);

                    }
                }, 1000);

            }
        });
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    private void validation() {
        hideKeyboard();

        if (Utils.isEmpty(countryCode)) {
            showToast((R.string.error_country_code));
            return;
        }
        if (!Utils.isEmpty(mNumber.getText().toString())) {
            if (mNumber.getText().toString().length() >= 7 && mNumber.getText().toString().length() <= 17) {

                progressDialog = new CircularProgressBarLayout(NumberValidation1.this, address);
                progressDialog.setCancelable(false);
                progressDialog.Set(NumberValidation1.this);
                progressDialog.show();
                Log.e("countrycodebfrsms", "..." + countryCode);
                Log.e("mNumberbfrsms", "..." + mNumber.getText().toString());
                mobileNumber = Utils.getUniversalFormattedMobileNOWithCountryCode(countryCode, mNumber.getText().toString());
                AppPreference.setVerifyNumberWithCountryCode(getApplicationContext(), mobileNumber);
                AppPreference.setCountryImageIDLogin(context, flag);
                AppPreference.setVerifyNumber(getApplicationContext(), mNumber.getText().toString());

                AppPreference.setCountryCodeFlagNameLogin(getApplicationContext(), countryName, countryCode);
                //OTP_NUMBER = get4DigitRandomNumber();
                //final String msg = getResources().getString(R.string.sms_otp1) + " " + OTP_NUMBER +""+new   Utils().getDeviceIMEI(NumberValidation1.this)+ getResources().getString(R.string.sms_otp2);
                //Log.e("smsmsg1","..."+msg);
               // AppPreference.setOTP(this, msg);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        String msg= Utils.randomGeneratorNumber();
                        AppPreference.setOTP(NumberValidation1.this, msg);
                        new MySendSMS_New(context).sendSMS(mobileNumber, msg);
                    }
                }, 1000);

            } else {
                showAlert(  getResources().getString(R.string.please_select_proper_number));
            }
        } else {
            showToast((R.string.enter_mobile_no));

        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.custom_actionbar_application_right_image:
                validation();
                break;
            case R.id.btn_press_here:
                validation();
                break;
            case R.id.ll_select_country_code:
                Intent intent = new Intent(this, SelectCountryCode.class);
                startActivityForResult(intent, 100);
                break;
            default:
                break;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100) {
            if (data != null) {
                if (data.getIntExtra("POSITION", -1) != -1) {
                    mCountryImage.setVisibility(View.VISIBLE);
                    mCountryImage.setImageResource((data.getIntExtra("POSITION", -1)));
                    flag = data.getIntExtra("POSITION", -1);
                }
                mCountryCode.setText(data.getStringExtra("COUNTRY_NAME") + " (" + data.getStringExtra("COUNTRY_CODE") + ")");
                tvCountryCode.setText("(" + data.getStringExtra("COUNTRY_CODE") + ")");
                countryCode = data.getStringExtra("COUNTRY_CODE");
                countryName = data.getStringExtra("COUNTRY_NAME");
                if (!data.getStringExtra("COUNTRY_CODE").equalsIgnoreCase("+95")) {
                    mNumber.setText("");
                    mNumber.setHint(getResources().getString(R.string.Enter_Your_Email_id));
                } else {
                    mNumber.setText(Utils.getMyanmarNumber());
                    mNumber.setSelection(2);
                }
            }
        }
    }

    private static long get4DigitRandomNumber() {
        String time = System.currentTimeMillis() + "";
        long number = Long.parseLong(time);
        return number;
    }

    private void readAllMessage(String number) {
        StringBuilder smsBuilder = new StringBuilder();
        final String SMS_URI_INBOX = "content://sms/inbox";
        try {
            Uri uri = Uri.parse(SMS_URI_INBOX);
            String[] projection = new String[]{"_id", "address", "person", "body", "date", "type"};
            Cursor cur = context.getContentResolver().query(uri, projection, "address='" + number + "'", null, "date desc");
            if(cur.getCount()==0){
                number=mNumber.getText().toString();
                cur = context.getContentResolver().query(uri, projection, "address='" + number + "'", null, "date desc");
            }
            if (cur.moveToFirst()) {
                int index_Body = cur.getColumnIndex("body");
                do {

                    String strbody = cur.getString(index_Body);
                    String OTP = String.valueOf(AppPreference.getOTP(context));
                    if (strbody.equals(OTP)&&(strbody.substring(13).equals(new Utils().getDeviceIMEI(NumberValidation1.this)))) {
                        dismissDialog();
                        isVerified = true;
                        mobileNumber = Utils.getUniversalFormattedMobileNOWithCountryCode(countryCode, mNumber.getText().toString());
                        AppPreference.setVerifiedSIMSerial(this, Utils.simSerialNumber(this));
                        AppPreference.setVerifiedIMSINo(this, Utils.getSimSubscriberID(this));
                        AppPreference.setCountryImageIDLogin(context, flag);
                        AppPreference.setCountryCodeFlagNameLogin(getApplicationContext(), countryName, countryCode);

                        Intent call = new Intent(context, NewRegistrationActivity1.class);
                        call.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        context.startActivity(call);
                        context.finish();
                        Utils.showCustomToastMsg(context, R.string.verified_no, true);
                        //Toast.makeText(context, "reall all meassage"+number, Toast.LENGTH_LONG).show();
                        return;
                    }
                } while (cur.moveToNext());

                if (!cur.isClosed()) {
                    cur.close();
                    cur = null;
                }
            }
        } catch (SQLiteException ex) {
            Log.d("SQLiteException", ex.getMessage());
            dismissDialog();
        }
    }


    public void dismissDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            CircularProgressBarLayout.condownTimer.cancel();
        }
    }

    public void showDefaultMobileNo(String msg, final String mobileNo) {

        final Dialog dialog = new Dialog(this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dailog);
        CustomTextView userInput = (CustomTextView) dialog.findViewById(R.id.dialog_editext);
        CustomTextView title = (CustomTextView) dialog.findViewById(R.id.tv_title);
        CustomTextView tvOK = (CustomTextView) dialog.findViewById(R.id.tv_ok);
        CustomTextView tvCancel = (CustomTextView) dialog.findViewById(R.id.tv_cancel);
        userInput.setText(msg);
        title.setVisibility(View.GONE);
        tvOK.setText(R.string.dailog_yes);
        tvCancel.setText(R.string.dailog_no);
        title.setTypeface(Utils.getFont(this), Typeface.BOLD);
        dialog.setCancelable(true);

        tvOK.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setNumberWithFlag(mobileNo);
                dialog.dismiss();
            }
        });
        tvCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void setNumberWithFlag(String no) {

        if (no.startsWith("+"))
            no = no.replace("+", "00");
        no = convertValidNumber(no);
        int id = -1;
        if (no.startsWith("+") || no.startsWith("00")) {
            String code = no.substring(0, 6);
            for (int i = 0; i < listOfCountryCodesAndNames.size(); i++) {

                String countryId = listOfCountryCodesAndNames.get(i).getCountryCode().replace("+", "00");
                if (code.contains(countryId)) {
                    System.out.println(i);
                    id = i;
                    int length = countryId.length();
                    no = no.substring(length);
                    countryCode = listOfCountryCodesAndNames.get(id).getCountryCode();
                    countryName = listOfCountryCodesAndNames.get(id).getCountryName();
                    flag = listOfCountryCodesAndNames.get(i).getCountryImageId();

                    break;
                }
            }
            if (countryCode.equalsIgnoreCase("+95"))
                no = "0" + no;
        }

        if (id == -1) {
            if (no.startsWith("0")) {
                if (!AppPreference.getCountryCodeLogin(this).contains("+95"))
                    no = no.substring(1);
            } else {
                no = "0" + no;
            }
            countryCode = AppPreference.getCountryCodeLogin(this);
            countryName = AppPreference.getCountryNameLogin(this);
            flag = AppPreference.getCountryImageID(this);
        }

        if (flag != -1) {
            mCountryImage.setVisibility(View.VISIBLE);
            mCountryImage.setImageResource(flag);
        }
        mCountryCode.setText("(" + countryCode + ")");
        mCountryCode.setText(countryName + " (" + countryCode + ")");
        tvCountryCode.setText("(" + countryCode + ")");

        if (countryCode.equalsIgnoreCase("+95")) {
            mNumber.setText(getNumberWithGreyZero(no, true));
        } else {
            mNumber.setText(no);
        }
        mNumber.setSelection(no.length());
    }

    @Override
    public void onSent() {

    }

    @Override
    public void onError(String error) {

    }

    @Override
    public void onDelivered(String msg) {
        if (!isVerified)
            readAllMessage(mobileNumber);
    }

    @Override
    public void onRetrive(int id) {

        if (id == address) {
            mobileNumber = Utils.getUniversalFormattedMobileNOWithCountryCode(countryCode, mNumber.getText().toString());
            AppPreference.setVerifyNumberWithCountryCode(getApplicationContext(), mobileNumber);
            AppPreference.setCountryImageIDLogin(context, flag);
            AppPreference.setCountryCodeFlagNameLogin(getApplicationContext(), countryName, countryCode);
            //AppPreference.setVerifyNumber(getApplicationContext(), mNumber.getText().toString());
            //OTP_NUMBER = get4DigitRandomNumber();
           // final String msg = getResources().getString(R.string.sms_otp1) + " " + OTP_NUMBER +""+new   Utils().getDeviceIMEI(NumberValidation1.this)+ getResources().getString(R.string.sms_otp2);
            //Log.e("smsmsg","..."+msg);
            //AppPreference.setOTP(this, msg);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    String msg= Utils.randomGeneratorNumber();
                    AppPreference.setOTP(NumberValidation1.this, msg);
                    new MySendSMS_New(context).sendSMS(mobileNumber, msg);

                }
            }, 1000);
        }
    }

    @Override
    public void backKeyPressed() {
        dismissDialog();
    }


    private class MyTextWatcherForMobileNo implements TextWatcher {

        public void afterTextChanged(Editable s) {

        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            try {
                if (s.length() < 2 && countryCode.equalsIgnoreCase("+95")) {
                    mNumber.setText(Utils.getMyanmarNumber());
                    mNumber.setSelection(mNumber.length());
                }
                if (s.toString().startsWith("0")) {
                    if (Utils.isValidForAutoFire(s.length() - 1, countryCode, model.getOperatorName())) {
                        buttonLogout.performClick();
                    }
                } else if (Utils.isValidForAutoFire(s.length(), countryCode, model.getOperatorName())) {
                    buttonLogout.performClick();
                }

            } catch (Exception e) {

            }
        }
    }

    private void backFinish() {
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                finish();
            }
        });
    }

    public void voiceSwapSIM(int audioFile) {
        ((Vibrator) getSystemService(Context.VIBRATOR_SERVICE)).vibrate(500);
        MediaPlayer p = MediaPlayer.create(this, audioFile);
        p.start();
    }

    public boolean isLowerVersionDevice(){
        if (android.os.Build.MODEL.contains("HUAWEI")|| android.os.Build.MODEL.contains("Huawei") || Build.BRAND.equalsIgnoreCase("Huawei") || Build.VERSION.SDK_INT<16) {
            showAlert(getString(R.string.take_out_sim2));
        }
        return true;
    }
}


