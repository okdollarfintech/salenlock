package mm.dummymerchant.sk.merchantapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import mm.dummymerchant.sk.merchantapp.Utils.Constant;
import mm.dummymerchant.sk.merchantapp.db.DBHelper;
import mm.dummymerchant.sk.merchantapp.model.OTPCashierModel;
import mm.dummymerchant.sk.merchantapp.model.RowItem_cashcollector;

/*import android.widget.AdapterView;*/

/**
 * Created by user on 12/3/2015.
 */
public class OTPLogDetails extends BaseActivity  implements Constant {
    DBHelper db;
    Activity_OtpLog_Adapter adapter;
    private ListView lv;
    String successORfail;
    ArrayList<OTPCashierModel> AlertList=new ArrayList<OTPCashierModel>();
    OTPCashierModel cellvalue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_log_details);
        lv = (ListView)findViewById(R.id.listView1);

        db = new DBHelper(getApplicationContext());
        setActivitylogActionBar(String.valueOf(getResources().getText(R.string.viewlogdetails)));
        Intent i = getIntent();
        successORfail = i.getStringExtra("From");
        System.out.println("The From values : " + successORfail);
        Check_MasterORCashier_Login();

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                new AsyncTaskLoading().execute();
            }
        });

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                RowItem_cashcollector item = (RowItem_cashcollector) adapter.getItem(i);

                if(item.GetLog_Paid_Status().equals(LATER))
                {
                    Intent intent=new Intent(OTPLogDetails.this,DigitalReceiptNew.class);
                    Bundle b=new Bundle();
                    b.putString(KEY_FROM,item.getJson_data());
                    b.putInt(KEY_FROM_INT, 1);
                    intent.putExtras(b);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    @Override
    public <T> void response(int resultCode, T data) {

    }

    private class AsyncTaskLoading extends AsyncTask<Void, Void, Void> {
        /// private GameResultsAdapter adapter;
        private final ProgressDialog dialogue =new ProgressDialog(OTPLogDetails.this);
        public AsyncTaskLoading() {
        }
        @Override
        protected void onPreExecute()
        {
            this.dialogue.setMessage("Loading...please wait.");
            this.dialogue.setIndeterminate(true);
            this.dialogue.setCancelable(false);
            this.dialogue.setCanceledOnTouchOutside(false);
            this.dialogue.show();
        }
        @Override
        protected Void doInBackground(Void... params)
        {
            try {

                db.open();
                Cursor c = db.Get_Generate_QR_Log_Details(successORfail, MasterORCashier_ID);
                System.out.println("Cursor Count() " +c.getCount());
                db.close();

                if (c.getCount() > 0) {
                    Log.i("AsyncTaskLoading", "Enter IF Contion");

                    if (c.moveToFirst()) {
                        while (c.isAfterLast() == false) {
                            cellvalue = new OTPCashierModel();
                            Log.i("AsyncTaskLoading", "READIN-AsyncTaskLoading");

                            /*
                            Log.i("OTP_LOG_ID ", c.getString(0));
                            Log.i("OTP_LOG_TIME ", c.getString(1));
                            Log.i("OTP_LOG_CASHIER_NAME", c.getString(2));
                            Log.i("OTP_LOG_CASHIER_NO", c.getString(3));
                            Log.i("OTP_CASHCOLLECTOR_NAME", c.getString(4));
                            Log.i("OTP_CASHCOLLECTOR_NO", c.getString(5));
                            Log.i("OTP_LOG_AMT", c.getString(6));
                            Log.i("OTP_LOG_STATUS", c.getString(7));
                            Log.i("OTP_LOG_CASHIER_ID", c.getString(8));
                            Log.i("OTP_LOG_TextValue", c.getString(9));
                            Log.i("OTP_LOG_TemplateValue", c.getString(10));
                            */

                            cellvalue.setTransid(c.getString(0));
                            cellvalue.setTime(c.getString(1));
                            cellvalue.setCashiername(c.getString(2));
                            cellvalue.setCashierno(c.getString(3));
                            cellvalue.setCashcollectorname(c.getString(4));
                            cellvalue.setCashcollectorno(c.getString(5));
                            cellvalue.setAmount(c.getString(6));
                            cellvalue.setStatus(c.getString(7));
                            cellvalue.setCashierid(c.getString(8));
                            cellvalue.setTextValue(c.getString(9));
                            cellvalue.setTemplateValue(c.getString(10));
                            cellvalue.setJsondata(c.getString(11));

                            AlertList.add(cellvalue);
                            c.moveToNext();
                        }
                    }
                }
                else {
                    Toast.makeText(getApplicationContext(), "Record Not Found", Toast.LENGTH_SHORT).show();
                }
            }catch(Exception e){

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void res)
        {
            System.out.println("AlertList.size() "+AlertList.size());
            if (AlertList.size() != 0)
            {
                adapter =new Activity_OtpLog_Adapter(OTPLogDetails.this, AlertList);
                lv.setAdapter(adapter);
            }
            else
            {
                showToast(getString(R.string.no_record));
            }

            if (this.dialogue.isShowing())
            {
                this.dialogue.dismiss();
            }
        }
    }


}
