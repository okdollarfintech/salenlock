package mm.dummymerchant.sk.merchantapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.GridView;
import android.widget.TableRow;

import java.util.ArrayList;

import mm.dummymerchant.sk.merchantapp.ReceivedMoney.NewReceivedMoney;
import mm.dummymerchant.sk.merchantapp.adapter.Dashboard_adapter;
import mm.dummymerchant.sk.merchantapp.fragments.DashBoardFragment;
import mm.dummymerchant.sk.merchantapp.model.dashboard_model;

/**
 * Created by user on 1/21/2016.
 */
public class OkDollarTransactionsActivity extends BaseActivity implements View.OnClickListener{
    TableRow mTransactionHistory,mReceivedMoney,mReadqrcode,mQRCodeGenerator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.grid_okdollar_transactions);
     // init();
        setExistingCashcollectorActionBar(String.valueOf(getResources().getText(R.string.okdollar)));
        callNewInit();
    }

    @Override
    public <T> void response(int resultCode, T data)
    {

    }

    void init()
    {
        mReadqrcode = (TableRow) findViewById(R.id.readqrcode);
        mTransactionHistory = (TableRow)findViewById(R.id.transactionhistory);
        mReceivedMoney =(TableRow)findViewById(R.id.receivedMoney);
        mQRCodeGenerator = (TableRow) findViewById(R.id.qrGenerator);

        callAllListeners();
    }
    void callAllListeners()
    {
        mTransactionHistory.setOnClickListener(this);
        mReadqrcode.setOnClickListener(this);
        mReceivedMoney.setOnClickListener(this);
        mQRCodeGenerator.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        Intent intent;
        switch (v.getId()) {

            case R.id.transactionhistory:
                intent =new Intent(this,NewTransationDitails.class);
                startActivity(intent);
                break;

            case R.id.readqrcode:
                intent =new Intent(this,ScanQRcodeFromOK.class);
                startActivity(intent);
                break;

               case R.id.receivedMoney:
                intent =new Intent(this,NewReceivedMoney.class);
                startActivity(intent);
                break;

            case R.id.qrGenerator:
                intent = new Intent(this, LoginGenerateQRCodeActivity.class);
                startActivity(intent);
                break;

        }
    }


    void callNewInit()
    {

        ArrayList<dashboard_model> alist=new ArrayList<dashboard_model>();
        GridView gv=(GridView)findViewById(R.id.gridView);

        alist.add(new dashboard_model(getString(R.string.transhistory), "0", R.drawable.transaction_history));
        alist.add(new dashboard_model(getString(R.string.ReceivedMoney),"1",R.drawable.startup_recevied_money_received_button));
        alist.add(new dashboard_model(getString(R.string.readqrcode), "2", R.drawable.digital_receipt));

        alist.add(new dashboard_model(getString(R.string.qrcode_generator), "3", R.drawable.qrcode));
        alist.add(new dashboard_model(getString(R.string.trans_summary), "4", R.drawable.transaction_history));


        Dashboard_adapter adapter=new Dashboard_adapter(OkDollarTransactionsActivity.this,alist,2);
        gv.setAdapter(adapter);
    }

    public void callBasedOnIndex(String id)
    {
        Intent intent;
        switch(id)
        {
            case "0":
                intent =new Intent(OkDollarTransactionsActivity.this,NewTransationDitails.class);
                startActivity(intent);
                break;

            case "1":
                intent =new Intent(OkDollarTransactionsActivity.this,NewReceivedMoney.class);
                startActivity(intent);
                break;

            case "2":
                intent =new Intent(OkDollarTransactionsActivity.this,ScanQRcodeFromOK.class);
                startActivity(intent);
                break;

            case "3":
                intent =new Intent(OkDollarTransactionsActivity.this,LoginGenerateQRCodeActivity.class);
                startActivity(intent);
                break;

            case"4":
                intent =new Intent(OkDollarTransactionsActivity.this,TransactionSummary.class);
                startActivity(intent);
                break;

            default:
                break;
        }

    }
}


