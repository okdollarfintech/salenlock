package mm.dummymerchant.sk.merchantapp;

import android.os.Bundle;
import android.view.View;

import mm.dummymerchant.sk.merchantapp.customView.CustomEdittext;
import mm.dummymerchant.sk.merchantapp.db.DBHelper;
import mm.dummymerchant.sk.merchantapp.model.MailDetails;

/**
 * Created by Dell on 11/25/2015.
 */
public class PersonalDetailsActivity extends BaseActivity
{
CustomEdittext et_emailId;
CustomEdittext et_passWord;
CustomEdittext  et_toMailId;
    int updateOrInsert;
    DBHelper dbb;

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.personal_details);
      setDummyMerchantActionBar_Submit();
      Bundle b= getIntent().getExtras();
      updateOrInsert=b.getInt(BUNDLE_UPDATEORINSERT);
      dbb=new DBHelper(this);
      dbb.open();

         init();

      buttonLogout.setOnClickListener

              (new View.OnClickListener() {
          @Override
          public void onClick(View view)
          {
            callValidation();
          }
      });

   dbb.close();

  }

    void init()
    {
    et_emailId=(CustomEdittext)findViewById(R.id.email_id);
    et_passWord=(CustomEdittext)findViewById(R.id.password);
    et_toMailId=(CustomEdittext)findViewById(R.id.to_email);

        if(updateOrInsert==1)
        {
            MailDetails mm=dbb.getMaildetails();

            et_emailId.setText(mm.getFromMailId());
            et_passWord.setText(mm.getPassword());
            et_toMailId.setText(mm.getToMailId());
        }
    }

    void callValidation()
    {
        String emailId=et_emailId.getText().toString().trim();
        String pwd=et_passWord.getText().toString().trim();
        String toMailId=et_toMailId.getText().toString().trim();

        if(emailId!=null&&pwd!=null&&toMailId!=null)
        {
            if(!emailId.equalsIgnoreCase("")&&!pwd.equalsIgnoreCase("")&&!toMailId.equalsIgnoreCase(""))
            {

                MailDetails md=new MailDetails(emailId,toMailId,pwd);
                if(updateOrInsert==2) {
                    dbb.insertMailDetails(md);
                    showToast("Details saved successfully");
                }
                if(updateOrInsert==1)
                {
                    dbb.updateMailDetails(md);
                    showToast("Details updated  successfully");
                }
                finish();
            }
            else
            {
                showToast("Enter details properly");
            }
        }

    }

    @Override
    public <T> void response(int resultCode, T data) {

    }
}
