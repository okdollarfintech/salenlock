package mm.dummymerchant.sk.merchantapp;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.github.gcacace.signaturepad.views.SignaturePad;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.Utils;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
import pl.droidsonroids.gif.GifImageView;


public class PotraitSignaturePad extends Activity implements SignaturePad.OnSignedListener, View.OnClickListener {

    public static SignaturePad mSignaturePad;
    private boolean isSigned = false;
    private TextView datetext,Time;
    private String timeStamp,currentdate;
    public static Bitmap signatureBitmap;
    private String getdate;
    private GifImageView gifImageView;
    private Date expiry;
    private CustomTextView mTapHere, set,clear,Cancel;;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        changeLang(AppPreference.getLanguage(getApplicationContext()));
        setContentView(R.layout.signature_pad1);
        mSignaturePad = (SignaturePad) findViewById(R.id.signature_pad);
        set =(CustomTextView) findViewById(R.id.set);
        clear =(CustomTextView) findViewById(R.id.clear);
        Cancel =(CustomTextView) findViewById(R.id.Cancel);
        datetext = (TextView) findViewById(R.id.date);
        Time = (TextView) findViewById(R.id.Time);
        gifImageView=(GifImageView) findViewById(R.id.gif_sign_demo);
        mTapHere=(CustomTextView) findViewById(R.id.tap_here_text);
        mSignaturePad.setOnSignedListener(this);
        set.setOnClickListener(this);
        clear.setOnClickListener(this);
        Cancel.setOnClickListener(this);
        gifImageView.setOnClickListener(this);

        timeStamp = new SimpleDateFormat("HH:mm:ss").format(new Date());
        Time.setText(timeStamp);

        long date = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd, yyyy");
        currentdate = sdf.format(date);
        datetext.setText(currentdate);

    }

    @Override
    public void onSigned() {
        isSigned = true;
    }

    @Override
    public void onClear() {
        isSigned = false;
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId()) {
            case R.id.clear:
                mSignaturePad.clear();
                break;
            case R.id.Cancel:
                finish();
                break;
            case R.id.set:

                if(!isSigned){
                    Utils.showToast(this, getResources().getString(R.string.error_enter_signature));
                    return;
                }
                signatureBitmap = mSignaturePad.getTransparentSignatureBitmap();
                String sign= "Signed "+ datetext.getText().toString();
                Intent i=new Intent(this, NewRegistrationActivity1.class);
                i.putExtra("Set", sign);
                setResult(2, i);
                finish();
                break;
            case R.id.gif_sign_demo:
                mTapHere.setVisibility(View.GONE);
                gifImageView.setVisibility(View.GONE);
                mSignaturePad.setVisibility(View.VISIBLE);
                break;
        }
    }
    public void changeLang(String lang) {
        if (lang.equalsIgnoreCase(""))
            return;
        Locale myLocale = new Locale(lang);
        saveLocale(lang);
        Locale.setDefault(myLocale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = myLocale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }
    public void saveLocale(String lang) {
        String langPref = "Language";
        SharedPreferences prefs = getSharedPreferences("CommonPrefs", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(langPref, lang);
        editor.commit();
    }
}
