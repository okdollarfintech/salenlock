package mm.dummymerchant.sk.merchantapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Vibrator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.Utils;
import mm.dummymerchant.sk.merchantapp.customView.CustomButton;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
import mm.dummymerchant.sk.merchantapp.introduction.CircularProgressBarLayout;


public class Preview extends Activity implements View.OnClickListener{

    ImageView preview_image;
    Button use,cancel,use_server;
    File f;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_preview);
        use = (Button) findViewById(R.id.use);
        use.setOnClickListener(this);
        use_server = (Button) findViewById(R.id.use_server);
        use_server.setOnClickListener(this);
        cancel = (Button) findViewById(R.id.cancel_);
        cancel.setOnClickListener(this);
        preview_image= (ImageView)findViewById(R.id.preview_image);
        f = new File(Environment.getExternalStorageDirectory(), "picture.jpg");
        Bitmap myBitmap = BitmapFactory.decodeFile(f.getAbsolutePath());
        preview_image.setImageBitmap(myBitmap);

    }

//    class OnTokenAcquired implements AccountManagerCallback<Bundle> {
//        boolean alreadyTriedAgain;
//        public OnTokenAcquired() {
//            // TODO Auto-generated constructor stub
//        }
//        protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//            if (requestCode == 3025) {
//                switch (resultCode) {
//                    case RESULT_OK:
//                        AccountManager am = AccountManager.get(getApplicationContext());
//                        am.getAuthToken(am.getAccounts()[0],
//                                "ouath2:" + DriveScopes.DRIVE,
//                                new Bundle(),
//                                true,
//                                new OnTokenAcquired(),
//                                null);
//                        break;
//                    case RESULT_CANCELED:
//                        // This probably means the user refused to log in. Explain to them why they need to log in.
//                        break;
//                    default:
//                        // This isn't expected... maybe just log whatever code was returned.
//                        break;
//                }
//            } else {
//                // Your application has other intents that it fires off besides the one for Drive's log in if it ever reaches this spot. Handle it here however you'd like.
//            }
//        }
//        @Override
//        public void run(AccountManagerFuture<Bundle> result) {
//            try {
//                final String token = result.getResult().getString(AccountManager.KEY_AUTHTOKEN);
//                HttpTransport httpTransport = new NetHttpTransport();
//                JacksonFactory jsonFactory = new JacksonFactory();
//                Drive.Builder b = new Drive.Builder(httpTransport, jsonFactory, null);
//                b.setJsonHttpRequestInitializer(new JsonHttpRequestInitializer() {
//                    @Override
//                    public void initialize(JsonHttpRequest request) throws IOException {
//                        DriveRequest driveRequest = (DriveRequest) request;
//                        driveRequest.setPrettyPrint(true);
//                        driveRequest.setKey("my number here");
//                        driveRequest.setOauthToken(token);
//                    }
//                });
//
//                final Drive drive = b.build();
//
//                final com.google.api.services.drive.model.File body = new com.google.api.services.drive.model.File();
//                body.setTitle("My Test File");
//                body.setDescription("A Test File");
//                body.setMimeType("text/plain");
//                File newFile = new File("this");
//                final FileContent mediaContent = new FileContent("text/plain", newFile);
//                new Thread(new Runnable() {
//                    public void run() {
//                        try {
//                            com.google.api.services.drive.model.File file = drive.files().insert(body, mediaContent).execute();
//                            alreadyTriedAgain = false; // Global boolean to make sure you don't repeatedly try too many times when the server is down or your code is faulty... they'll block requests until the next day if you make 10 bad requests, I found.
//                        } catch (IOException e) {
//                            if (!alreadyTriedAgain) {
//                                alreadyTriedAgain = true;
//                                AccountManager am = AccountManager.get(getApplicationContext());
//                                am.invalidateAuthToken(am.getAccounts()[0].type, null); // Requires the permissions MANAGE_ACCOUNTS & USE_CREDENTIALS in the Manifest
//                                am.getAuthToken(am.getAccounts()[0],
//                                        "ouath2:" + DriveScopes.DRIVE,
//                                        new Bundle(),
//                                        true,
//                                        new OnTokenAcquired(),
//                                        null);
//                            } else {
//                                // Give up. Crash or log an error or whatever you want.
//                            }
//                        }
//                    }
//                }).start();
//                Intent launch = (Intent)result.getResult().get(AccountManager.KEY_INTENT);
//                if (launch != null) {
//                    startActivityForResult(launch, 3025);
//                    return; // Not sure why... I wrote it here for some reason. Might not actually be necessary.
//                }
//            }
//                // Handle it...
//            catch (Exception e) {
//                // Handle it...
//            }
//        }
//
//    }
//    private java.io.File downloadGFileToJFolder(Drive drive, String token, File gFile, java.io.File jFolder) throws IOException {
//        if (gFile.toURI() != null && gFile.toURI().toString().length() > 0 ) {
//            if (jFolder == null) {
//                jFolder = Environment.getExternalStorageDirectory();
//                jFolder.mkdirs();
//            }
//            try {
//
//                HttpClient client = new DefaultHttpClient();
//                HttpGet get = new HttpGet(gFile.toURI());
//                get.setHeader("Authorization", "Bearer " + token);
//                org.apache.http.HttpResponse response = client.execute(get);
//
//                InputStream inputStream = response.getEntity().getContent();
//                jFolder.mkdirs();
//                java.io.File jFile = new java.io.File(jFolder.getAbsolutePath() + "/" + gFile.getName()); // getGFileName() is my own method... it just grabs originalFilename if it exists or title if it doesn't.
//                FileOutputStream fileStream = new FileOutputStream(jFile);
//                byte buffer[] = new byte[1024];
//                int length;
//                while ((length=inputStream.read(buffer))>0) {
//                    fileStream.write(buffer, 0, length);
//                }
//                fileStream.close();
//                inputStream.close();
//                return jFile;
//            } catch (IOException e) {
//                // Handle IOExceptions here...
//                return null;
//            }
//        } else {
//            // Handle the case where the file on Google Drive has no length here.
//            return null;
//        }
//    }

    public class FileUploader extends AsyncTask<String,String,String>
    {
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {

            return sendFileToServer(params[0],params[1],this);

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(Preview.this);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Please wait!");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setProgress(0);
            progressDialog.show();


        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(s!=null)
            {
                if(progressDialog!=null)
                {
                    progressDialog.dismiss();
                }
                if(!s.equals("error"))
                    showMessageSucces(s);
                else
                    showMessageSucces("Something went wrong!");
            }else
            {
                showMessageSucces("Something went wrong!");
            }

        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            progressDialog.setProgress(Integer.parseInt(values[0]));
        }

        public void update(String progress)
        {
            publishProgress(progress);
        }
    }



    public String sendFileToServer(String filename, String targetUrl,FileUploader context) {
        String response = "";
        Log.e("Image filename", filename);
        Log.e("url", targetUrl);
        HttpURLConnection connection = null;
        DataOutputStream outputStream = null;
        // DataInputStream inputStream = null;

        String pathToOurFile = filename;
        String urlServer = targetUrl;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        DateFormat df = new SimpleDateFormat("yyyy_MM_dd_HH:mm:ss");

        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024;
        try {
            FileInputStream fileInputStream = new FileInputStream(new File(
                    pathToOurFile));

            URL url = new URL(urlServer);
            connection = (HttpURLConnection) url.openConnection();

            // Allow Inputs & Outputs
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            connection.setChunkedStreamingMode(1024);
            // Enable POST method
            connection.setRequestMethod("POST");

            connection.setRequestProperty("Connection", "Keep-Alive");
            connection.setRequestProperty("Content-Type",
                    "multipart/form-data;boundary=" + boundary);

            outputStream = new DataOutputStream(connection.getOutputStream());
            outputStream.writeBytes(twoHyphens + boundary + lineEnd);

            String connstr = null;
            connstr = "Content-Disposition: form-data; name=\"uploadedfile\";filename=\""
                    + pathToOurFile + "\"" + lineEnd;
            Log.i("Connstr", connstr);

            outputStream.writeBytes(connstr);
            outputStream.writeBytes(lineEnd);

            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];

            // Read file
            int amount = (int)bytesAvailable/100;
            final int total_bytes = fileInputStream.available();
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            Log.e("Image length", bytesAvailable + "");
            try {
                while (bytesRead > 0) {
                    try {
                        outputStream.write(buffer, 0, bufferSize);
                    } catch (OutOfMemoryError e) {
                        e.printStackTrace();
                        response = "outofmemoryerror";
                        return response;
                    }
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                    try {
                        int uploaded = total_bytes - bytesAvailable;
                        int progress =(int)uploaded / amount;
                        context.update(String.valueOf(progress));
                    }catch (Exception e){}
                }
            } catch (Exception e) {
                e.printStackTrace();
                response = "error";
                return response;
            }
            outputStream.writeBytes(lineEnd);
            outputStream.writeBytes(twoHyphens + boundary + twoHyphens
                    + lineEnd);

            // Responses from the server (code and message)
            int serverResponseCode = connection.getResponseCode();
            String serverResponseMessage = connection.getResponseMessage();
            String urll = connection.getHeaderField("Location");
            Log.i("Server Response Code ", "" + serverResponseCode);
            Log.i("Server Response Code ", "" + serverResponseCode);
            Log.i("server_url", urll);
            if (serverResponseCode == 201) {
                response = urll;
            }
            String CDate = null;
            Date serverTime = new Date(connection.getDate());
            try {
                CDate = df.format(serverTime);
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("Date Exception", e.getMessage() + " Parse Exception");
            }
            Log.i("Server Response Time", CDate + "");

            filename = CDate
                    + filename.substring(filename.lastIndexOf("."),
                    filename.length());
            Log.i("File Name in Server : ", filename);

            fileInputStream.close();
            outputStream.flush();
            outputStream.close();
            outputStream = null;
        } catch (Exception ex) {
            // Exception handling
            response = "error";
            Log.e("Send file Exception", ex.getMessage() + "");
            ex.printStackTrace();
        }
        return response;
    }




    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.use:
                try {// Share Intent
                    Intent share = new Intent(Intent.ACTION_SEND);
                    // Type of file to share
                    share.setType("image/jpeg");
                    // Compress into png format image from 0% - 100%// Locate the image to Share
                    Uri uri = Uri.fromFile(f);
                    share.putExtra(Intent.EXTRA_STREAM, uri);
                    // Show the social share chooser list
                    startActivity(Intent.createChooser(share, "Share Content"));
                }catch (Exception e){}
                break;
            case R.id.cancel_:
                finish();
                break;
            case R.id.use_server:
                String url="http://media.api.okdollar.org/phones/"+ AppPreference.getMyMobileNo(Preview.this)+"/devices/"+ Utils.getDeviceIMEI(Preview.this)+"/files";
                new FileUploader().execute(f.getAbsolutePath(), url);
                break;

        }
    }
    private void showMessageSucces(final String message) {
        MediaPlayer p = MediaPlayer.create(this, R.raw.success_tune);
        p.start();
        ((Vibrator) getSystemService(VIBRATOR_SERVICE)).vibrate(200);
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.alert_window, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptsView);
        final CustomTextView userInput = (CustomTextView) promptsView.findViewById(R.id.alert);
        userInput.setText("This is the Url : "+message);
        CustomButton ok = (CustomButton) promptsView.findViewById(R.id.btn_settings);
        final AlertDialog dialog1 = alertDialogBuilder.create();
        ok.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog1.dismiss();
                if(!AppPreference.getALLUrl(Preview.this).equals("")) {
                    AppPreference.setALLUrl(Preview.this, AppPreference.getALLUrl(Preview.this) + "$" + message);
                }else
                {
                    AppPreference.setALLUrl(Preview.this,message);
                }
                Intent intent = new Intent(Preview.this,Enter_URL.class);
                intent.putExtra("url_",message);
                startActivity(intent);
                finish();

            }
        });
        dialog1.show();
    }
}
