package mm.dummymerchant.sk.merchantapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TableRow;

/**
 * Created by user on 12/3/2015.
 */
public class QRcodeActivity extends BaseActivity implements View.OnClickListener{
    TableRow mGenerateqrcode,mReadqrcode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrcode);
        init();
        setExistingCashcollectorActionBar(String.valueOf(getResources().getText(R.string.generate_qr_code)));
    }

    @Override
    public <T> void response(int resultCode, T data) {

    }

    void init()
    {
        mGenerateqrcode = (TableRow) findViewById(R.id.generateqrcode);
        mReadqrcode = (TableRow) findViewById(R.id.readqrcode);

        callAllListeners();
    }
    void callAllListeners()
    {
        mGenerateqrcode.setOnClickListener(this);
        mReadqrcode.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        Intent intent;
        switch (v.getId()) {

            case R.id.generateqrcode:
               /* intent =new Intent(this,GenerateQRcodeActivity.class);
                startActivity(intent);*/
                showToast("under Construction");
                break;

            case R.id.readqrcode:
                /*intent =new Intent(this,ScanQRcodeActivity.class);
                startActivity(intent);*/
                showToast("under Construction");
                break;
        }
    }
}

