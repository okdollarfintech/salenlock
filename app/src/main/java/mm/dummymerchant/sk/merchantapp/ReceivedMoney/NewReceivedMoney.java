package mm.dummymerchant.sk.merchantapp.ReceivedMoney;

/**
 * Created by Dell on 10/27/2015.
 */

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import mm.dummymerchant.sk.merchantapp.BaseActivity;
import mm.dummymerchant.sk.merchantapp.R;
import mm.dummymerchant.sk.merchantapp.SliderScreen;
import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.Utils;
import mm.dummymerchant.sk.merchantapp.adapter.EndlessRecyclerOnScrollListener;
import mm.dummymerchant.sk.merchantapp.adapter.SimpleAdapter;
import mm.dummymerchant.sk.merchantapp.adapter.UpdateVariable;
import mm.dummymerchant.sk.merchantapp.customView.CustomButton;
import mm.dummymerchant.sk.merchantapp.customView.CustomEdittext;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
import mm.dummymerchant.sk.merchantapp.db.DBHelper;
import mm.dummymerchant.sk.merchantapp.httpConnection.CallingApIS;
import mm.dummymerchant.sk.merchantapp.model.TransationModel;


public class NewReceivedMoney extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener,UpdateVariable {

    public ImageView backButton;
 //   public CustomEdittext search2;
    public ImageView buttonLogout;
    public CustomTextView mTitleOfHeader;
    public RelativeLayout mcustomHeaderBg;
    public LinearLayout custom_action_bar_lay_submit_log, custom_action_bar_lay_back;
    public Filter nameFilter;
    public SimpleAdapter mAdapter;
    public CustomTextView amount_, date_;
    RecyclerView mRecyclerView;
    PopupWindow popupWindow;
    ArrayList<TransationModel> mTransition_selected = new ArrayList<TransationModel>();
    ArrayList<TransationModel> receivedMoney = new ArrayList<TransationModel>();
    ArrayList<TransationModel> mTranstionListTemp = new ArrayList<TransationModel>();

    int search_Paramter = 3;
    int year, month, day;

    private List<Date> myList = new ArrayList<Date>();
    private List<String> sectionIndexToday = new ArrayList<String>();
    private List<String> sectionIndexYesterday = new ArrayList<String>();
    private List<String> sectionIndexOld = new ArrayList<String>();

    private SwipeRefreshLayout swipeView;
    String last_search_amount="", last_search_contact="", last_search_date="";

    private LinearLayoutManager mLayoutManager;
    boolean calling= true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_received_money);

        setMcustomHeader();
       // setRecievedMonetActionBar();
        init();
        filterInitialize();
        searchInit();

        swipeView = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        swipeView.setOnRefreshListener(this);
        swipeView.setColorSchemeColors(Color.GRAY, Color.GREEN, Color.BLUE,
                Color.RED, Color.CYAN);
        swipeView.setDistanceToTriggerSync(20);// in dips
        swipeView.setSize(SwipeRefreshLayout.DEFAULT);// LARGE also can
        swipeView.setRefreshing(false);

    }

    /**
     * **************** Initializing variables *************************************
     */


    // By default, we add 10 objects for first time.
    private void loadData() {
        calling=true;
        CallingApIS api = new CallingApIS(NewReceivedMoney.this, NewReceivedMoney.this);
        int start_index;
        start_index = 0;
        int last_index = 10;
        api.CallReceivedTransation(start_index, last_index);
    }

    // adding 10 object creating dymically to arraylist and updating recyclerview when ever we reached last item
    private void loadMoreData(int no_list) {

        CallingApIS api = new CallingApIS(NewReceivedMoney.this, NewReceivedMoney.this);
        int start_index;
        start_index = no_list;
        int last_index = 10;
        api.CallReceivedTransation(start_index, last_index);

    }


    public void init() {

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        amount_ = (CustomTextView) findViewById(R.id.amount_);
       // amount_.setText(getResources().getString(R.string.amount_a) + " " + "\ud83d\udcb0");
        date_ = (CustomTextView) findViewById(R.id.date_);
        date_.setText(getResources().getString(R.string.date) + " " + "\ud83d\udcc5");


        mRecyclerView.setOnScrollListener(new EndlessRecyclerOnScrollListener(
                mLayoutManager, mTransition_selected) {
            @Override
            public void onLoadMore() {
              //  Toast.makeText(getApplicationContext(),"size is..."+mTransition_selected.size(),Toast.LENGTH_LONG).show();

                if (calling) {
                    if (last_search_amount.equals("") && last_search_contact.equals("") && last_search_date.equals("")) {
                        loadMoreData(mTransition_selected.size());

                    }
                    calling = false;
                }
            }
        });

        if (Utils.isConnectedToInternet(this)) {
            /*CallingApIS api = new CallingApIS(this, this);
            api.CallReceivedTransation();*/
            loadData();
            if (AppPreference.getMasterOrCashier(this).equalsIgnoreCase(CASHIER))
                receivedMoney = duplicateRemover(db.getReceivedTransationDetails(SliderScreen.present_Cashier_Id, this));
            else
                receivedMoney = duplicateRemover(db.getReceivedTransationDetails(MASTER_TAG, this));

            for (TransationModel temp1 : receivedMoney) {
                mTransition_selected.add(temp1);
            }
        } else {

            showToast(R.string.no_internet);

            if (AppPreference.getMasterOrCashier(this).equalsIgnoreCase(CASHIER)) {
                if (db.getReceivedTransationDetails(SliderScreen.present_Cashier_Id, this).size() >= 0) {
                    receivedMoney = db.getReceivedTransationDetails(SliderScreen.present_Cashier_Id, this);
                    for (TransationModel temp1 : receivedMoney) {
                        mTransition_selected.add(temp1);
                    }
                }
            } else {
                if (db.getReceivedTransationDetails(MASTER_TAG, this).size() >= 0) {
                    receivedMoney = db.getReceivedTransationDetails(MASTER_TAG, this);
                    for (TransationModel temp1 : receivedMoney) {
                        mTransition_selected.add(temp1);
                    }
                }
            }
        }

        listGenerator();

    }


    /**
     * **************** Filter Intializer *************************************
     */

    public void filterInitialize() {
        nameFilter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                if (constraint != null) {
                    if (!constraint.equals("")) {
                        if (receivedMoney == null) {
                            return null;
                        } else if (receivedMoney.size() == 0) {
                            return null;
                        }
                        mTransition_selected.clear();
                        for (TransationModel customer : receivedMoney) {
                            if (search_Paramter == 1) {
                                if (customer.getMerchantName().toLowerCase().contains(constraint.toString().toLowerCase())) {
                                    mTransition_selected.add(customer);
                                }
                            }
                            if (search_Paramter == 3) {
                                if (Utils.formatedAmountwithoutMMK(customer.getAmount(), NewReceivedMoney.this).toString().trim().equals(constraint.toString().trim())) {
                                    mTransition_selected.add(customer);
                                }
                            }
                            if (search_Paramter == 2) {
                                if (customer.getDate().toLowerCase().contains(constraint.toString().toLowerCase())) {
                                    mTransition_selected.add(customer);
                                }
                            }
                        }
                        FilterResults filterResults = new FilterResults();
                        filterResults.values = mTransition_selected;
                        filterResults.count = mTransition_selected.size();
                        return filterResults;

                    } else {
                        mTransition_selected = new ArrayList<TransationModel>();
                        receivedMoney = duplicateRemover(receivedMoney);
                        for (TransationModel temp1 : receivedMoney) {

                            SpannableString famount = Utils.formatedAmountwithoutMMK(temp1.getDbAmount(), NewReceivedMoney.this);
                            temp1.setDbAmount(famount.toString());
                            mTransition_selected.add(temp1);
                        }
                        FilterResults filterResults = new FilterResults();
                        filterResults.values = mTransition_selected;
                        filterResults.count = mTransition_selected.size();
                        return filterResults;
                    }

                } else {
                    return new FilterResults();
                }
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                updateList(mTransition_selected);
                try {
                    if (mAdapter != null) {
                        mAdapter.notifyDataSetChanged();
                    }
                } catch (Exception e) {

                }

            }
        };

    }

    /**
     * **************** Initializing search *************************************
     */


    public void searchInit() {
        nameFilter = new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                if (constraint != null) {
                    if (!constraint.equals("")) {
                        if (receivedMoney == null) {
                            return null;
                        } else if (receivedMoney.size() == 0) {
                            return null;
                        }
                        mTransition_selected.clear();
                        for (TransationModel customer : receivedMoney) {
                            if (search_Paramter == 1) {
                                if (customer.getMerchantName().toLowerCase().contains(constraint.toString().toLowerCase())) {
                                    mTransition_selected.add(customer);
                                }
                            }
                            if (search_Paramter == 3) {
                                if (Utils.formatedAmountwithoutMMK(customer.getAmount(), NewReceivedMoney.this).toString().trim().equals(constraint.toString().trim())) {
                                    mTransition_selected.add(customer);
                                }
                            }
                            if (search_Paramter == 2) {
                                if (customer.getDate().toLowerCase().contains(constraint.toString().toLowerCase())) {
                                    mTransition_selected.add(customer);
                                }
                            }
                        }
                        FilterResults filterResults = new FilterResults();
                        filterResults.values = mTransition_selected;
                        filterResults.count = mTransition_selected.size();
                        return filterResults;

                    } else {

                        mTransition_selected = new ArrayList<TransationModel>();
                        receivedMoney = duplicateRemover(receivedMoney);
                        for (TransationModel temp1 : receivedMoney) {
                            SpannableString famount = Utils.formatedAmountwithoutMMK(temp1.getDbAmount(), NewReceivedMoney.this);
                            temp1.setDbAmount(famount.toString());
                            mTransition_selected.add(temp1);
                        }
                        FilterResults filterResults = new FilterResults();
                        filterResults.values = mTransition_selected;
                        filterResults.count = mTransition_selected.size();
                        return filterResults;
                    }
                } else {
                    return new FilterResults();
                }
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                updateList(mTransition_selected);
                try {
                    if (mAdapter != null) {
                        mAdapter.notifyDataSetChanged();
                    }
                } catch (Exception e) {
                }

            }
        };

      /*  search2.setHint(getResources().getString(R.string.search_byamount));
        search2.setVisibility(View.VISIBLE);
        search2.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    hideKeyboard();
                }
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    hideKeyboard();
                }
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    hideKeyboard();
                }
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    hideKeyboard();
                }
                return false;
            }
        });


        search2.setInputType(InputType.TYPE_CLASS_PHONE);
        search2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (popupWindow != null) {
                    if (popupWindow.isShowing())
                        popupWindow.dismiss();
                    popupWindow = null;
                }
                return false;
            }
        });
        search2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (popupWindow != null) {
                    if (popupWindow.isShowing())
                        popupWindow.dismiss();
                    popupWindow = null;
                }
                if (search_Paramter == 2) {
                    DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {

                            search_Paramter = 2;

                            SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
                            Calendar newDate = Calendar.getInstance();
                            newDate.set(arg1, arg2, arg3);
                            year = arg1;
                            month = arg2;
                            day = arg3;
                            final DBHelper dbHelper = new DBHelper(NewReceivedMoney.this);
                            mTransition_selected = dbHelper.getReceivedTransactionDetailsByDate("date", dateFormatter.format(newDate.getTime()));
                            search2.setText(newDate.getTime().toString());
                            ArrayList<TransationModel> temp = new ArrayList<TransationModel>();

                            if (mTransition_selected != null) {
                                if (mTransition_selected.size() != 0) {
                                    for (int i = 0; i < mTransition_selected.size(); i++) {
                                        if (temp.size() == 0) {
                                            temp.add(mTransition_selected.get(i));
                                        } else {
                                            boolean duplicate = false;
                                            for (TransationModel t : temp) {
                                                if (mTransition_selected.get(i).getTransatId().trim().equals(t.getTransatId().trim())) {
                                                    duplicate = true;
                                                    break;
                                                }
                                            }
                                            if (duplicate == false) {
                                                temp.add(mTransition_selected.get(i));
                                            }
                                        }
                                    }
                                }
                            }
                            mTransition_selected.clear();

                            for (TransationModel m : temp) {
                                mTransition_selected.add(m);
                            }
                            updateList(mTransition_selected);
                            year = newDate.get(Calendar.YEAR);
                            month = newDate.get(Calendar.MONTH) + 1;
                            day = newDate.get(Calendar.DAY_OF_MONTH);
                            newDate.set(arg1, arg2, arg3);
                            if (popupWindow != null) {
                                if (popupWindow.isShowing()) {
                                    popupWindow.dismiss();

                                }
                            }
                        }
                    };
                    Calendar calendar = Calendar.getInstance();
                    DatePickerDialog Datedialog = new DatePickerDialog(NewReceivedMoney.this, myDateListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                    Datedialog.show();

                }


            }
        });

        *//******************* applying filter on text type **************************************//*

        search2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (search_Paramter != 2) {
                    nameFilter.filter(editable.toString());
                }
            }
        });
*/

    }

    /**
     * **************** setting custom header for the activity *************************************
     */

    public void setMcustomHeader() {
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater inflater = LayoutInflater.from(this);
        View v = inflater.inflate(R.layout.header_recieved_money, null);
        custom_action_bar_lay_back = (LinearLayout) v.findViewById(R.id.custom_action_bar_lay_back);
        backButton = (ImageView) v.findViewById(R.id.custom_actionbar_application_left_image);
        custom_action_bar_lay_submit_log = (LinearLayout) v.findViewById(R.id.custom_action_bar_lay_pay);
        buttonLogout = (ImageView) v.findViewById(R.id.custom_actionbar_application_right_image);
        mTitleOfHeader = (CustomTextView) v.findViewById(R.id.custom_actionbar_application_textview_title_of_application);
        mcustomHeaderBg = (RelativeLayout) v.findViewById(R.id.custom_action_bar_application);
        actionBar.setCustomView(v);
        mTitleOfHeader.setText(getResources().getString(R.string.ReceivedMoney));
      //  search2 = (CustomEdittext) v.findViewById(R.id.search_transaction_1);
        custom_action_bar_lay_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        actionBar.setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        Toolbar parent = (Toolbar) v.getParent();//first get parent toolbar of current action bar
        parent.setContentInsetsAbsolute(0, 0);
        buttonLogout.setVisibility(View.VISIBLE);
        buttonLogout.setImageDrawable(getResources().getDrawable(R.drawable.more_icon));
        mcustomHeaderBg.setBackgroundColor(getResources().getColor(R.color.metro_blue));
        buttonLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showPopup();
            }
        });


    }


    /**
     * **************** common method for list generaation *************************************
     */

    public void listGenerator()

    {
        myList = new ArrayList<Date>();
        if (mTransition_selected.size() > 0) {
            List<String> myDateList = new ArrayList<String>();
            myDateList.clear();
            mAdapter = new SimpleAdapter(this, mTransition_selected, myDateList);
            mRecyclerView.setAdapter(mAdapter);
        }
    }

    /**
     * **************** This method removes duplicate if present while taking data from database *************************************
     */

    public ArrayList<TransationModel> duplicateRemover(ArrayList<TransationModel> duplicated) {

        ArrayList<TransationModel> temp = new ArrayList<TransationModel>();

        if (duplicated != null) {

            if (duplicated.size() != 0) {

                for (int i = 0; i < duplicated.size(); i++) {

                    if (temp.size() == 0) {
                        temp.add(duplicated.get(i));
                    } else {
                        boolean duplicate = false;
                        for (TransationModel t : temp) {
                            if (duplicated.get(i).getTransatId().trim().equals(t.getTransatId().trim())) {
                                duplicate = true;
                                break;
                            }
                        }

                        if (duplicate == false) {
                            temp.add(duplicated.get(i));
                        }
                    }
                }
            }
        }

        return temp;
    }

    /**
     * **************** Touch event handler to dismiss popup on outside touch  *************************************
     */


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {

        if (popupWindow != null) {
            if (popupWindow.isShowing())
                popupWindow.dismiss();
            popupWindow = null;
        }

        return super.dispatchTouchEvent(ev);
    }

    @Override
    public <T> void response(int resultCode, T data) {


        if (resultCode == REQUEST_TYPE_RECEIVED_TRANSCATION) {

            if (AppPreference.getMasterOrCashier(this).equalsIgnoreCase(MASTER)) {
                if (db.getReceivedTransationDetails(MASTER_TAG, this).size() >= 0) {

                    receivedMoney = db.getReceivedTransationDetails(MASTER_TAG, this);
                    mTransition_selected = new ArrayList<TransationModel>();
                    receivedMoney = duplicateRemover(receivedMoney);
                    for (TransationModel temp1 : receivedMoney) {
                        mTransition_selected.add(temp1);
                    }

                    listGenerator();
                }
            } else {

                String cashierId = SliderScreen.present_Cashier_Id;

                if (db.getReceivedTransationDetails(cashierId, this).size() >= 0) {

                    receivedMoney = db.getReceivedTransationDetails(cashierId, this);
                    mTransition_selected = new ArrayList<TransationModel>();
                    receivedMoney = duplicateRemover(receivedMoney);
                    for (TransationModel temp1 : receivedMoney) {
                        mTransition_selected.add(temp1);
                    }

                    listGenerator();
                }
            }
        }
    }

    /**
     * **************** Not in use but may be in use in future DO NOT DELETE *************************************
     */

    public void getTimeDiff(Date olderDate) {

        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Calendar cal = Calendar.getInstance();
        String currentTime = dateFormat.format(cal.getTime());
        String olderTime = dateFormat.format(olderDate.getTime());

        try {
            Date currentDate = dateFormat.parse(currentTime);

            long diffInMillis = currentDate.getTime() - olderDate.getTime();

            if (diffInMillis < 86400000) {
                sectionIndexToday.add(olderTime);
            } else if (diffInMillis >= 86400000 && diffInMillis < 172800000) {
                sectionIndexYesterday.add(olderTime);
            } else if (diffInMillis >= 172800000) {
                sectionIndexOld.add(olderTime);
            }

        } catch (Exception e) {

        }

    }

    /**
     * **************** Pop Window with search filter  *************************************
     */

  /*  public void showPopup() {

        if (popupWindow == null) {
            LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
            View popupView = layoutInflater.inflate(R.layout.popup_list, null);
            popupWindow = new PopupWindow(popupView, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            CustomTextView sortByNameTxtView = (CustomTextView) popupView.findViewById(R.id.txtSortName);
            CustomTextView sortByDateTxtView = (CustomTextView) popupView.findViewById(R.id.txtSortDate);
            CustomEdittext sortByAmountTxtView = (CustomEdittext) popupView.findViewById(R.id.txtSortAmount);
            CustomTextView refershTxtView = (CustomTextView) popupView.findViewById(R.id.txtRefersh);
            Calendar calendar;

            refershTxtView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                  *//*  CallingApIS api = new CallingApIS(NewReceivedMoney.this, NewReceivedMoney.this);
                    api.CallReceivedTransation();*//*
                    loadData();
                    if (popupWindow != null) {
                        if (popupWindow.isShowing())
                            popupWindow.dismiss();
                        popupWindow = null;
                    }
                }
            });

            sortByNameTxtView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    search_Paramter = 1;
                    popupWindow.dismiss();
                }
            });

            sortByDateTxtView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    search2.setInputType(InputType.TYPE_CLASS_TEXT);
                    search_Paramter = 2;
                    search2.setHint(getResources().getString(R.string.search_bydate));
                    search2.setText("");
                    hideKeyboard();
                    search2.setFocusable(false);
                    mTransition_selected.clear();
                    ArrayList<TransationModel> temprature = duplicateRemover(receivedMoney);
                    for (TransationModel temp1 : temprature) {
                        mTransition_selected.add(temp1);
                    }
                    updateList(mTransition_selected);
                    if (popupWindow != null) {
                        if (popupWindow.isShowing())
                            popupWindow.dismiss();
                        popupWindow = null;
                    }
                }
            });
            sortByAmountTxtView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mTransition_selected.clear();
                    search2.setFocusableInTouchMode(true);
                    search_Paramter = 3;
                    search2.setInputType(InputType.TYPE_CLASS_PHONE);
                    search2.setHint(getResources().getString(R.string.search_byamount));
                    search2.setText("");
                    if (popupWindow != null) {
                        if (popupWindow.isShowing())
                            popupWindow.dismiss();
                        popupWindow = null;
                    }
                }
            });
            if (popupWindow.isShowing()) {
                popupWindow.dismiss();
            } else
                popupWindow.showAsDropDown(buttonLogout, -150, 0);
        } else {

            popupWindow.dismiss();
            popupWindow = null;

        }
    }
*/


    public void showPopup() {
        if (popupWindow == null) {
            LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);

            final View popupView = layoutInflater.inflate(R.layout.popup_list, null);
            popupWindow = new PopupWindow(popupView, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            final CustomEdittext txtSearchbycontact = (CustomEdittext) popupView.findViewById(R.id.txtSearchbycontact);
            final CustomTextView sortByDateTxtView = (CustomTextView) popupView.findViewById(R.id.txtSortDate);
            final CustomEdittext sortByAmountTxtView = (CustomEdittext) popupView.findViewById(R.id.txtSortAmount);
            final CustomTextView refershTxtView = (CustomTextView) popupView.findViewById(R.id.txtRefersh);
            Drawable img = getResources().getDrawable(R.drawable.searchby_date_icon);
            if (img != null) {
                img.setBounds(0, 0, 55, 55);
                sortByDateTxtView.setCompoundDrawables(img, null, null, null);
            }
            Drawable img1 = getResources().getDrawable(R.drawable.searchby_phone_icon);
            if (img1 != null) {
                img1.setBounds(0, 0, 55, 55);
                txtSearchbycontact.setCompoundDrawables(img1, null, null, null);
            }
            Drawable img2 = getResources().getDrawable(R.drawable.searchby_amount_icon);
            if (img2 != null) {
                img2.setBounds(0, 0, 55, 55);
                sortByAmountTxtView.setCompoundDrawables(img2, null, null, null);
            }
            Drawable img3 = getResources().getDrawable(R.drawable.refresh_icon_r);
            if (img3 != null) {
                img3.setBounds(0, 0, 55, 55);
                refershTxtView.setCompoundDrawables(img3, null, null, null);
            }


            refershTxtView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //  CallingApIS api = new CallingApIS(NewReceivedMoney.this, NewReceivedMoney.this);
                    //  api.CallReceivedTransation();
                    loadData();
                    last_search_contact = "";
                    last_search_amount = "";
                    last_search_date = "";
                    txtSearchbycontact.setText("");
                    sortByAmountTxtView.setText("");
                    sortByDateTxtView.setText("");
                    if (popupWindow != null) {
                        if (popupWindow.isShowing())
                            popupWindow.dismiss();
                        popupWindow = null;
                    }
                }
            });

            txtSearchbycontact.setImeOptions(EditorInfo.IME_ACTION_DONE);
            txtSearchbycontact.setInputType(InputType.TYPE_CLASS_PHONE);
            txtSearchbycontact.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    // popupWindow.setFocusable(false);
                }

                @Override
                public void afterTextChanged(Editable editable) {
                    nameFilter.filter(editable.toString());
                    last_search_contact = editable.toString();
                }
            });


            txtSearchbycontact.setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (search_Paramter != 1) {
                        search_Paramter = 1;
                        txtSearchbycontact.setInputType(InputType.TYPE_CLASS_PHONE);
                        mTransition_selected.clear();

                        if (!sortByDateTxtView.equals("")) {
                            sortByDateTxtView.setText("");
                        }
                        if (!sortByAmountTxtView.equals("")) {
                            sortByAmountTxtView.setText("");
                        }

                        sortByAmountTxtView.setText("");
                        sortByDateTxtView.setText("");
                        last_search_amount = "";
                        last_search_date = "";
                        //popupWindow.update();
                    }

                    return false;
                }
            });


            sortByDateTxtView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    search_Paramter = 2;
                    mTransition_selected.clear();
                    sortByDateTxtView.setInputType(InputType.TYPE_CLASS_TEXT);
                    txtSearchbycontact.setText("");
                    sortByAmountTxtView.setText("");
                    if (!txtSearchbycontact.equals("")) {
                        txtSearchbycontact.setText("");
                    }
                    if (!sortByAmountTxtView.equals("")) {
                        sortByAmountTxtView.setText("");
                    }
                    last_search_contact = "";
                    last_search_amount = "";
                    last_search_date = "";

                    DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
                            hideKeyboard();
                            search_Paramter = 2;
                            SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
                            Calendar newDate = Calendar.getInstance();
                            newDate.set(arg1, arg2, arg3);
                            year = arg1;
                            month = arg2;
                            day = arg3;
                            final DBHelper dbHelper = new DBHelper(NewReceivedMoney.this);
                            mTransition_selected = dbHelper.getReceivedTransactionDetailsByDate("date", dateFormatter.format(newDate.getTime()));
                            sortByDateTxtView.setText(newDate.getTime().toString());
                            last_search_date = newDate.getTime().toString();
                            ArrayList<TransationModel> temp = new ArrayList<>();
                            if (mTransition_selected != null) {
                                if (mTransition_selected.size() != 0) {
                                    for (int i = 0; i < mTransition_selected.size(); i++) {
                                        if (temp.size() == 0) {
                                            temp.add(mTransition_selected.get(i));
                                        } else {
                                            boolean duplicate = false;
                                            for (TransationModel t : temp) {
                                                if (mTransition_selected.get(i).getTransatId().trim().equals(t.getTransatId().trim())) {
                                                    duplicate = true;
                                                    break;
                                                }
                                            }
                                            if (!duplicate) {
                                                temp.add(mTransition_selected.get(i));
                                            }
                                        }
                                    }
                                }
                            }

                            if (mTransition_selected != null) {
                                mTransition_selected.clear();
                            }
                            for (TransationModel m : temp) {
                                if (mTransition_selected != null) {
                                    mTransition_selected.add(m);
                                }
                            }
                            if (mTransition_selected != null) {
                                Collections.sort(mTransition_selected);
                            }
                            updateList(mTransition_selected);
                            year = newDate.get(Calendar.YEAR);
                            month = newDate.get(Calendar.MONTH) + 1;
                            day = newDate.get(Calendar.DAY_OF_MONTH);
                            newDate.set(arg1, arg2, arg3);
                            if (popupWindow != null) {
                                if (popupWindow.isShowing()) {
                                    popupWindow.dismiss();
                                }
                            }
                        }
                    };
                    Calendar calendar = Calendar.getInstance();
                    DatePickerDialog Datedialog = new DatePickerDialog(NewReceivedMoney.this, myDateListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                    Datedialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            if (which == DialogInterface.BUTTON_NEGATIVE) {
                                last_search_date = "";
                                sortByDateTxtView.setText("");
                                receivedMoney = db.getReceivedTransationDetails();
                                mTransition_selected = new ArrayList<>();
                                receivedMoney = duplicateRemover(receivedMoney);
                                for (TransationModel temp1 : receivedMoney) {
                                    mTransition_selected.add(temp1);
                                }
                                Collections.sort(mTransition_selected);
                                listGenerator();
                            }
                            hideKeyboard();
                        }
                    });
                    Datedialog.show();

                    return false;
                }
            });


            sortByAmountTxtView.setImeOptions(EditorInfo.IME_ACTION_DONE);
            sortByAmountTxtView.setInputType(InputType.TYPE_CLASS_PHONE);
            sortByAmountTxtView.setOnTouchListener(new View.OnTouchListener() {


                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (search_Paramter != 3) {
                        mTransition_selected.clear();
                        if (!sortByDateTxtView.equals("")) {
                            sortByDateTxtView.setText("");
                        }
                        if (!txtSearchbycontact.equals("")) {
                            txtSearchbycontact.setText("");
                        }

                        search_Paramter = 3;
                        sortByAmountTxtView.setInputType(InputType.TYPE_CLASS_PHONE);
                        txtSearchbycontact.setText("");
                        sortByDateTxtView.setText("");
                        last_search_contact = "";
                        last_search_date = "";
                    }
                    // popupWindow.update();
                    return false;
                }

            });

            sortByAmountTxtView.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    // popupWindow.setFocusable(false);
                }

                @Override
                public void afterTextChanged(Editable editable) {
                    nameFilter.filter(editable.toString());
                    last_search_amount = editable.toString();
                }
            });

            sortByAmountTxtView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    hideKeyboard();
                    if (popupWindow != null)
                        if (popupWindow.isShowing()) {
                            popupWindow.dismiss();
                        }
                    return false;
                }
            });
            txtSearchbycontact.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    hideKeyboard();
                    if (popupWindow != null)
                        if (popupWindow.isShowing()) {
                            popupWindow.dismiss();
                        }
                    return false;
                }
            });
            if (popupWindow.isShowing()) {
                popupWindow.dismiss();
            } else {
                if (last_search_date != null) {
                    if (!last_search_date.equals("")) {
                        sortByDateTxtView.setText(last_search_date);
                    }
                }
                if (last_search_amount != null) {
                    if (!last_search_amount.equals("")) {
                        sortByAmountTxtView.setText(last_search_amount);
                    }
                }
                if (last_search_contact != null) {
                    if (!last_search_contact.equals("")) {
                        txtSearchbycontact.setText(last_search_contact);
                    }
                }
                popupWindow.setOutsideTouchable(true);
                popupWindow.setFocusable(true);
                popupWindow.setBackgroundDrawable(new BitmapDrawable());
                popupWindow.update();
                popupWindow.showAsDropDown(buttonLogout, -250, 0);
            }
        } else {
            popupWindow.dismiss();
            popupWindow = null;
        }
    }

    /**
     * **************** Update the list from the database *************************************
     */

    public void updateList(final ArrayList<TransationModel> receivedMoney) {

        if (receivedMoney.size() > 0) {
            List<String> myDateList = new ArrayList<String>();
            for (int i = 0; i < receivedMoney.size(); i++) {
                try {
                    SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
                    String date[] = receivedMoney.get(i).getDate().split(" ");
                    myList.add(df.parse(date[0].trim()));
                } catch (Exception e)

                {
                    e.printStackTrace();
                }
            }
            for (int i = 0; i < receivedMoney.size(); i++) {

                try {
                    getTimeDiff(myList.get(i));
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            myDateList.clear();
            final SimpleAdapter mAdapter = new SimpleAdapter(this, mTransition_selected, myDateList);

            mRecyclerView.setAdapter(mAdapter);


        } else {
            List<String> myDateList = new ArrayList<String>();
            final SimpleAdapter mAdapter = new SimpleAdapter(this, mTransition_selected, myDateList);
            mRecyclerView.setAdapter(mAdapter);

            if (mAdapter != null) {
                mAdapter.notifyDataSetChanged();
            }


        }

    }

    public void hideKeyboard() {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }


    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onRefresh() {

        swipeView.postDelayed(new Runnable() {

            @Override
            public void run() {


                loadData();
                mTransition_selected.clear();
                receivedMoney = db.getReceivedTransationDetails();
                mTransition_selected = new ArrayList<>();
                receivedMoney = duplicateRemover(receivedMoney);
                for (TransationModel temp1 : receivedMoney) {
                    mTransition_selected.add(temp1);
                }
                Collections.sort(mTransition_selected);

                listGenerator();


                last_search_contact = "";
                last_search_amount = "";
                last_search_date = "";
                // mTransition_selected.clear();
                if (popupWindow != null) {
                    if (popupWindow.isShowing())
                        popupWindow.dismiss();
                    popupWindow = null;
                }
                swipeView.setRefreshing(false);
            }
        }, 1000);

    }


    @Override
    public void callingReset(boolean calling) {

        this.calling=calling;

    }

    public void showSearchDialog(final String field){
        final Dialog dialog = new Dialog(NewReceivedMoney.this);
        dialog.setContentView(R.layout.search_dialog);
        dialog.setTitle(getString(R.string.search));
        final CustomEdittext edittext = (CustomEdittext)dialog.findViewById(R.id.et_search);
        CustomButton btnSearch=(CustomButton)dialog.findViewById(R.id.btn_search);
        final DBHelper dbHelper = new DBHelper(NewReceivedMoney.this);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!edittext.getText().toString().isEmpty()){
                    dialog.dismiss();
                    mTranstionListTemp = dbHelper.getReceivedTransactionDetails(field,edittext.getText().toString());
                    if (mTranstionListTemp.size() > 0) {
                        updateList(mTranstionListTemp);
                    }else{
                        showToast((R.string.no_records));
                    }
                }
            }
        });
        dialog.show();
    }
}