package mm.dummymerchant.sk.merchantapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import mm.dummymerchant.sk.merchantapp.adapter.Activity_ScanLogDetails_Adapter;
import mm.dummymerchant.sk.merchantapp.db.DBHelper;
import mm.dummymerchant.sk.merchantapp.model.RowItem_cashcollector;

/**
 * Created by user on 1/20/2016.
 */
public class ScanLogDetailsActivity extends BaseActivity implements View.OnClickListener{

    private ListView lv;
    DBHelper db;
    Activity_ScanLogDetails_Adapter adapter;
    RowItem_cashcollector cellvalue;
    ArrayList<RowItem_cashcollector> AlertList;
    String successORfail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_logdetails);
        setActivitylogActionBar(String.valueOf(getResources().getText(R.string.viewlogdetails)));

        db = new DBHelper(this);
        Intent i = getIntent();
        successORfail = i.getStringExtra("From");
        System.out.println("The From values : " + successORfail);
        Check_MasterORCashier_Login();
        lv=(ListView)findViewById(R.id.listView1);
        setlistview();
        AlertList = new ArrayList<RowItem_cashcollector>();
    }

    public void setlistview()
    {
        AsyncTaskLoading loading = new AsyncTaskLoading();
        loading.execute();
    }

    private class AsyncTaskLoading extends AsyncTask<Void, Void, Void> {
        /// private GameResultsAdapter adapter;

        private final ProgressDialog dialogue =new ProgressDialog(ScanLogDetailsActivity.this);
        public AsyncTaskLoading() {
        }

        @Override
        protected void onPreExecute()
        {
            this.dialogue.setMessage("Loading...please wait.");
            this.dialogue.setIndeterminate(true);
            this.dialogue.setCancelable(false);
            dialogue.setCanceledOnTouchOutside(false);
            this.dialogue.show();
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            try {
                db.open();
                Cursor c = db.Get_SUCCESS_Scan_Log_Details(successORfail,MasterORCashier_ID);
                db.close();
                if (c.getCount() > 0) {
                    Log.i("ScanLogDetailsActivity", "Enter IF Contion");

                    if (c.moveToFirst()) {
                        while (c.isAfterLast() == false) {
                            cellvalue = new RowItem_cashcollector();
                            Log.i("ScanLogDetailsActivity", "READIN-RowItem_cashcollector");

                            Log.i("OTP_LOG_ID ", c.getString(0));
                            Log.i("OK_MasterORCashier_ID ", c.getString(1));
                            Log.i("SCANNING_TIME", c.getString(2));
                            Log.i("GENERATE_TIME", c.getString(3));
                            Log.i("OTP_ENTER_TIME", c.getString(4));
                            Log.i("OTP_LOG_CASHIER_NAME", c.getString(5));
                            Log.i("OTP_LOG_CASHIER_NO", c.getString(6));
                            Log.i("OTP_LOG_AMT", c.getString(7));
                            Log.i("OTP_LOG_STATUS", c.getString(8));
                            Log.i("OTP_LOG_PaidSTATUS", c.getString(9));
                            Log.i("FROM_WHICH_SCAN", c.getString(10));
                            Log.i("FROM_WHICH_SCAN", c.getString(11));
                            Log.i("FROM_WHICH_SCAN", c.getString(12));
                            Log.i("FROM_WHICH_SCAN", c.getString(13));
                            Log.i("GENERATER_NAME", c.getString(14));
                            Log.i("GENERATER_NO", c.getString(15));
                            Log.i("SCAN_LOG_TEXTVALUE", c.getString(16));
                            Log.i("SCAN_LOG_TEMPLATEVALUE", c.getString(17));

                            cellvalue.SetLog_Trans_Id(c.getString(0));
                            cellvalue.SetScanner_Name(c.getString(5));
                            cellvalue.SetScanner_No(c.getString(6));
                            cellvalue.SetGenerater_Name(c.getString(14));
                            cellvalue.SetGenerater_No(c.getString(15));
                            cellvalue.SetLog_Amount(c.getString(7));
                            cellvalue.SetScanning_Time(c.getString(2));
                            cellvalue.SetGenerate_Time(c.getString(3));
                            cellvalue.SetOTP_Enter_Location(c.getString(4));
                            cellvalue.SetLog_Paid_Status(c.getString(9));
                            cellvalue.SetLog_Status(c.getString(8));
                            cellvalue.SetScanning_Location(c.getString(11));
                            cellvalue.SetGenerate_Location(c.getString(12));
                            cellvalue.SetOTP_Enter_Location(c.getString(13));
                            cellvalue.setTextValue(c.getString(16));
                            cellvalue.setTemplateValue(c.getString(17));

                            AlertList.add(cellvalue);
                            c.moveToNext();
                        }
                    }
                }
                else {
                    Toast.makeText(getApplicationContext(), "Record Not Found", Toast.LENGTH_SHORT).show();
                }

            }catch(Exception e){

            }
            // dbb.close();
            return null;
        }

        @Override
        protected void onPostExecute(Void res)
        {
            System.out.println("AlertList.size() "+AlertList.size());
            if (AlertList.size() != 0)
            {
                adapter =new Activity_ScanLogDetails_Adapter(ScanLogDetailsActivity.this, AlertList);
                lv.setAdapter(adapter);
            }
            else
            {
                showToast(getString(R.string.no_record));
            }

            if (this.dialogue.isShowing())
            {
                this.dialogue.dismiss();
            }
        }
    }

    @Override
    public <T> void response(int resultCode, T data) {

    }
    @Override
    public void onClick(View v) {

    }
}
