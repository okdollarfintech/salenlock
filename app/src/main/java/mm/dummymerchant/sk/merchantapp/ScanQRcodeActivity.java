package mm.dummymerchant.sk.merchantapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TableRow;

/**
 * Created by user on 1/19/2016.
 */
public class ScanQRcodeActivity extends BaseActivity implements View.OnClickListener {

    TableRow mScannerdetails,mSuccesslog,mFaillog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_qrcode);
        init();
        setExistingCashcollectorActionBar(String.valueOf(getResources().getText(R.string.readqrcode)));
    }
    void init()
    {
        mScannerdetails = (TableRow) findViewById(R.id.scannerdetails);
        mSuccesslog = (TableRow) findViewById(R.id.successlog);
        mFaillog = (TableRow) findViewById(R.id.faillog);
        callAllListeners();
    }
    void callAllListeners()
    {
        mScannerdetails.setOnClickListener(this);
        mSuccesslog.setOnClickListener(this);
        mFaillog.setOnClickListener(this);
    }
    @Override
    public <T> void response(int resultCode, T data) {

    }
    @Override
    public void onClick(View v) {

        Intent intent;
        switch (v.getId()) {

            case R.id.scannerdetails:
                intent = new Intent(this, MainActivity1.class);
               // intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                break;

            case R.id.successlog:
                intent = new Intent(this, ScanLogDetailsActivity.class);
                intent.putExtra("From", "SUCCESS");
                startActivity(intent);
                break;

            case R.id.faillog:
                intent = new Intent(this, ScanLogDetailsActivity.class);
                intent.putExtra("From", "FAILURE");
                startActivity(intent);
                break;
        }
    }
}
