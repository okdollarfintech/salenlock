package mm.dummymerchant.sk.merchantapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import mm.dummymerchant.sk.merchantapp.Utils.Constant;
import mm.dummymerchant.sk.merchantapp.adapter.ScanQR_FromOK_LogDetails_Adapter;
import mm.dummymerchant.sk.merchantapp.db.DBHelper;
import mm.dummymerchant.sk.merchantapp.model.RowItem_ScanFrom_OK;

/**
 * Created by user on 1/21/2016.
 */
public class ScanQRcodeFromOK_LogDetails extends BaseActivity implements Constant
{
    private String timeStamp,SC;
    DBHelper db;
    RowItem_ScanFrom_OK cellvalue;
    private ArrayList<RowItem_ScanFrom_OK> AlertList=new ArrayList<RowItem_ScanFrom_OK>();
    private ListView lv;
    ScanQR_FromOK_LogDetails_Adapter adapter;
    private String Scan_Longitude,Scan_Latitude,Generate_Latitude,Generate_Longitude,GenerateLocation,ScanLocation;
    private String OK,Receiver_Name,Amount,Date,Transid,Sender_No,New_Amount,Trans_Type;
    private String Receiver_No,Kick_Back,Locality,Location,Des_Key,Cellid,Gender,Age,City;
    String successORfail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ok_scandetails);
        lv = (ListView)findViewById(R.id.listView1);
        setActivitylogActionBar(String.valueOf(getResources().getText(R.string.viewlogdetails)));
        db = new DBHelper(this);

        Intent i = getIntent();
        successORfail = i.getStringExtra("From");
        System.out.println("The From values : " + successORfail);
        Check_MasterORCashier_Login();

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                new AsyncTaskLoading().execute();
            }
        });
    }

    @Override
    public <T> void response(int resultCode, T data)
    {

    }

    private class AsyncTaskLoading extends AsyncTask<Void, Void, Void> {
        /// private GameResultsAdapter adapter;
        private final ProgressDialog dialogue =new ProgressDialog(ScanQRcodeFromOK_LogDetails.this);
        public AsyncTaskLoading() {
        }
        @Override
        protected void onPreExecute()
        {
            this.dialogue.setMessage("Loading...please wait.");
            this.dialogue.setIndeterminate(true);
            this.dialogue.setCancelable(false);
            dialogue.setCanceledOnTouchOutside(false);
            this.dialogue.show();
        }
        @Override
        protected Void doInBackground(Void... params)
        {
            try {

                db.open();
                Cursor c = db.Get_Log_Details_From_OK(successORfail,MasterORCashier_ID);
                System.out.println("Cursor value " +c.toString());
                System.out.println("Cursor Count() " +c.getCount());
                db.close();

                if (c.getCount() > 0) {
                    Log.i("AsyncTaskLoading", "Enter IF Contion");

                    if (c.moveToFirst()) {
                        while (c.isAfterLast() == false) {
                            cellvalue = new RowItem_ScanFrom_OK();
                            Log.i("AsyncTaskLoading", "READIN-AsyncTaskLoading");

                            Log.i("OK_ID ", c.getString(0));
                            Log.i("OK_MasterORCashier_ID ", c.getString(1));
                            Log.i("OK_TIME_STAMP", c.getString(2));
                            Log.i("OK_DATE", c.getString(3));
                            Log.i("OK_RECEIVER_NAME", c.getString(4));
                            Log.i("OK_AMOUNT", c.getString(5));
                            Log.i("OK_TRANSID", c.getString(6));
                            Log.i("OK_SENDER_NO", c.getString(7));
                            Log.i("OK_RECEIVER_NO", c.getString(8));
                            Log.i("OK_TRANS_TYPE", c.getString(9));
                            Log.i("OK_KICK", c.getString(10));
                            Log.i("OK_LOCALITY", c.getString(11));
                            Log.i("OK_LOCATION", c.getString(12));
                            Log.i("OK_CELL_ID", c.getString(13));
                            Log.i("OK_GENDER", c.getString(14));
                            Log.i("OK_AGE", c.getString(15));
                            Log.i("GENERATE_LOCATION", c.getString(16));
                            Log.i("SCANNING_LOCATION", c.getString(17));
                            Log.i("OK_STATUS", c.getString(18));

                            cellvalue.SetOK_ID(c.getString(0));
                            cellvalue.SetOK_TIME_STAMP(c.getString(2));
                            cellvalue.SetOK_DATE(c.getString(3));
                            cellvalue.SetOK_RECEIVER_NAME_No(c.getString(4));
                            cellvalue.SetOK_AMOUNT(c.getString(5));
                            cellvalue.SetOK_TRANSID(c.getString(6));
                            cellvalue.SetOK_SENDER_NO(c.getString(7));
                            cellvalue.SetOK_RECEIVER_NO(c.getString(8));
                            cellvalue.SetOK_TRANS_TYPE(c.getString(9));
                            cellvalue.SetOK_KICK(c.getString(10));
                            cellvalue.SetOK_LOCALITY(c.getString(11));
                            cellvalue.SetOK_LOCATION(c.getString(12));
                            cellvalue.SetOK_CELL_ID(c.getString(13));
                            cellvalue.SetOK_GENDER(c.getString(14));
                            cellvalue.SetOK_AGE(c.getString(15));
                            cellvalue.SetGENERATE_LOCATION(c.getString(16));
                            cellvalue.SetSCANNING_LOCATION(c.getString(17));
                            cellvalue.SetOK_STATUS(c.getString(18));

                            AlertList.add(cellvalue);
                            c.moveToNext();
                        }
                    }
                }
                else {
                    Toast.makeText(getApplicationContext(), "Record Not Found", Toast.LENGTH_SHORT).show();
                }
            }catch(Exception e){

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void res)
        {
            System.out.println("AlertList.size() " + AlertList.size());
            if (AlertList.size() != 0)
            {
                adapter =new ScanQR_FromOK_LogDetails_Adapter(ScanQRcodeFromOK_LogDetails.this, AlertList);
                lv.setAdapter(adapter);
            }
            else
            {
                showToast(getString(R.string.no_record));
            }
            if (this.dialogue.isShowing())
            {
                this.dialogue.dismiss();
            }

        }
    }

    @Override
    public void onBackPressed()
    {
        //super.onBackPressed();
        finish();
    }
}
