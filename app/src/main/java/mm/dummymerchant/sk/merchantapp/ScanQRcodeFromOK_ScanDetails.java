package mm.dummymerchant.sk.merchantapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.scottyab.aescrypt.AESCrypt;

import java.security.GeneralSecurityException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import mm.dummymerchant.sk.merchantapp.Utils.Constant;
import mm.dummymerchant.sk.merchantapp.Utils.GpsTracker1;
import mm.dummymerchant.sk.merchantapp.adapter.ScanQR_FromOK_Adapter;
import mm.dummymerchant.sk.merchantapp.db.DBHelper;
import mm.dummymerchant.sk.merchantapp.model.RowItem_ScanFrom_OK;

/**
 * Created by user on 1/21/2016.
 */
public class ScanQRcodeFromOK_ScanDetails extends BaseActivity implements Constant
{

    private String timeStamp,SC;
    DBHelper db;
    RowItem_ScanFrom_OK cellvalue;
    private ArrayList<RowItem_ScanFrom_OK> AlertList=new ArrayList<RowItem_ScanFrom_OK>();
    private ListView lv;
    ScanQR_FromOK_Adapter adapter;
    private String Scan_Longitude,Scan_Latitude,Generate_Latitude,Generate_Longitude,GenerateLocation,ScanLocation;
    private String OK,Receiver_Name,Amount,Date,Transid,Sender_No,New_Amount,Trans_Type;
    private String Receiver_No,Kick_Back,Locality,Location,Des_Key,Cellid,Gender,Age,City;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.qrcodescanner);
        setExistingCashcollectorActionBar(String.valueOf(getResources().getText(R.string.app_name)));
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //showSelectionDilog();
                qrCodeScanner();
            }
        });
        db = new DBHelper(this);
        Check_MasterORCashier_Login();

     /* Boolean delete_bol = db.Delete(FAILURE);
        System.out.println(" Boolean Delete "+delete_bol); */

        lv=(ListView)findViewById(R.id.listView1);
        setlistview();
    }

    public void qrCodeScanner() {
        IntentIntegrator integrator_qr = new IntentIntegrator(this);
        integrator_qr.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
        integrator_qr.setPrompt(String.valueOf("Scan QR Code"));
        integrator_qr.addExtra("SCAN_WIDTH", 1800);
        integrator_qr.addExtra("SCAN_HEIGHT", 1880);
        integrator_qr.setResultDisplayDuration(0);
        integrator_qr.setCameraId(0);  // Use a specific camera of the device
        integrator_qr.initiateScan();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        System.out.println("dataaaaaaaaaa " + data);

        if(resultCode==RESULT_OK)
        {
            if (null != data)
            {
                if (scanningResult != null)
                {
                    String scanResult = scanningResult.getContents();
                    afterResult(scanResult);
                }
            }
            } else {
                Toast.makeText(this, "Wrong QR Code..Please Try Again..!", Toast.LENGTH_LONG).show();
            }
    }

    public void setlistview()
    {
        AsyncTaskLoading loading = new AsyncTaskLoading();
        loading.execute();
    }

    private class AsyncTaskLoading extends AsyncTask<Void, Void, Void> {
        /// private GameResultsAdapter adapter;
        private final ProgressDialog dialogue =new ProgressDialog(ScanQRcodeFromOK_ScanDetails.this);
        public AsyncTaskLoading() {
        }
        @Override
        protected void onPreExecute()
        {
            this.dialogue.setMessage("Loading...please wait.");
            this.dialogue.setIndeterminate(true);
            this.dialogue.setCancelable(false);
            dialogue.setCanceledOnTouchOutside(false);
            this.dialogue.show();
        }
        @Override
        protected Void doInBackground(Void... params)
        {
            try {
                db.open();
               // Cursor c = db.Get_Details_From_OK_ScanDetails(SUCCESS);
                Cursor c = db.Get_Details_From_OK_ScanDetails(MasterORCashier_ID);
                db.close();
                if (c.getCount() > 0) {
                    Log.i("ServingCell_Page", "Enter IF Contion");

                    if (c.moveToFirst()) {
                        while (c.isAfterLast() == false) {
                            cellvalue = new RowItem_ScanFrom_OK();
                            Log.i("ServingCell_Page", "READIN-Get_SevRowItem_servingcellingcell");

                            Log.i("OK_ID ", c.getString(0));
                            Log.i("OK_MasterORCashier_ID ", c.getString(1));
                            Log.i("OK_TIME_STAMP", c.getString(2));
                            Log.i("OK_DATE", c.getString(3));
                            Log.i("OK_RECEIVER_NAME", c.getString(4));
                            Log.i("OK_AMOUNT", c.getString(5));
                            Log.i("OK_TRANSID", c.getString(6));
                            Log.i("OK_SENDER_NO", c.getString(7));
                            Log.i("OK_RECEIVER_NO", c.getString(8));
                            Log.i("OK_TRANS_TYPE", c.getString(9));
                            Log.i("OK_KICK", c.getString(10));
                            Log.i("OK_LOCALITY", c.getString(11));
                            Log.i("OK_LOCATION", c.getString(12));
                            Log.i("OK_CELL_ID", c.getString(13));
                            Log.i("OK_GENDER", c.getString(14));
                            Log.i("OK_AGE", c.getString(15));
                            Log.i("GENERATE_LOCATION", c.getString(16));
                            Log.i("SCANNING_LOCATION", c.getString(17));
                            Log.i("OK_STATUS", c.getString(18));

                            cellvalue.SetOK_ID(c.getString(0));
                            cellvalue.SetOK_TIME_STAMP(c.getString(2));
                            cellvalue.SetOK_DATE(c.getString(3));
                            cellvalue.SetOK_RECEIVER_NAME_No(c.getString(4));
                            cellvalue.SetOK_AMOUNT(c.getString(5));
                            cellvalue.SetOK_TRANSID(c.getString(6));
                            cellvalue.SetOK_SENDER_NO(c.getString(7));
                            cellvalue.SetOK_RECEIVER_NO(c.getString(8));
                            cellvalue.SetOK_TRANS_TYPE(c.getString(9));
                            cellvalue.SetOK_KICK(c.getString(10));
                            cellvalue.SetOK_LOCALITY(c.getString(11));
                            cellvalue.SetOK_LOCATION(c.getString(12));
                            cellvalue.SetOK_CELL_ID(c.getString(13));
                            cellvalue.SetOK_GENDER(c.getString(14));
                            cellvalue.SetOK_AGE(c.getString(15));
                            cellvalue.SetGENERATE_LOCATION(c.getString(16));
                            cellvalue.SetSCANNING_LOCATION(c.getString(17));
                            cellvalue.SetOK_STATUS(c.getString(18));

                            AlertList.add(cellvalue);
                            c.moveToNext();
                        }
                    }
                }
                else {
                    Toast.makeText(getApplicationContext(), "Record Not Found", Toast.LENGTH_SHORT).show();
                }
            }catch(Exception e){

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void res)
        {
            System.out.println("AlertList.size() " + AlertList.size());
            if (AlertList.size() != 0)
            {
                adapter =new ScanQR_FromOK_Adapter(ScanQRcodeFromOK_ScanDetails.this, AlertList);
                lv.setAdapter(adapter);
            }
            else
            {
                showToast(getString(R.string.no_record));
            }
            if (this.dialogue.isShowing())
            {
                this.dialogue.dismiss();
            }
        }
    }

    public void Get_Location()
    {
        GpsTracker1 gps = new GpsTracker1(getApplicationContext());
        android.location.Location loc = gps.getLocation();

        if(loc!=null) {
            Scan_Latitude = "" + loc.getLatitude();
            Scan_Longitude = "" + loc.getLongitude();
            Log.i("Scan_Latitude",Scan_Latitude);
            Log.i("Scan_Longitude",Scan_Longitude);
        }
        else {
            Scan_Latitude = "0.0";
            Scan_Longitude = "0.0";
            Log.i("Scan_Latitude",Scan_Latitude);
            Log.i("Scan_Longitude", Scan_Longitude);
        }
    }

    void afterResult(String scannedText)
    {
        String E1, E2,Decrypt_Time,Decrypt_No;
        String text = "", textTime = "";
        try {
            String Split[] = scannedText.split("---");
            E1 = Split[0];
            E2 = Split[1];

            textTime = AESCrypt.decrypt(SPEEDKey, E1);
            Log.i("E1", textTime);
            String Split1[] = textTime.split("----");
            Decrypt_Time = Split1[0];
            Decrypt_No = Split1[1];
            Log.i("Split1 [0] Time ", Decrypt_Time);
            Log.i("Split1 [1] No ", Decrypt_No);

            if(Decrypt_No.equals(MasterORCashier_Mobile_Number))
            {
                text = AESCrypt.decrypt(textTime, E2);
                Log.i("E2", text);
                if (!text.isEmpty())
                {
                    try {
                        String dataText[] = text.split("-");
                        Log.d("Enter ", "Enter IF Conditionnnnn");
                        SC = dataText[0];
                        Log.d("OK Value: ", SC);
                        if (SC.equals("OK"))
                        {
                            Receiver_Name = dataText[1];
                            Amount = dataText[2];
                            Date = dataText[3];
                            Transid = dataText[4];
                            Sender_No = dataText[5];
                            New_Amount = dataText[6];
                            Des_Key = dataText[7];
                            Receiver_No = dataText[8];
                            Trans_Type = dataText[9];
                            Kick_Back = dataText[10];
                            Locality = dataText[11];
                            Location = dataText[12];
                            Cellid = dataText[13];
                            Gender = dataText[14];
                            Age = dataText[15];

                            Get_Location();
                            Log.d("OK String ", SC);
                            Log.d("Receiver_Name ", Receiver_Name);
                            Log.d("Amount ", Amount);
                            Log.d("Date ", Date);
                            Log.d("Transid ", Transid);
                            Log.d("Sender_No ", Sender_No);
                            Log.d("New_Amount ", New_Amount);
                            Log.d("Des_Key ", Des_Key);
                            Log.d("Receiver_No ", Receiver_No);
                            Log.d("Trans_Type ", Trans_Type);
                            Log.d("Kick_Back", Kick_Back);
                            Log.d("Locality", Locality);
                            Log.d("Location", Location);
                            Log.d("Cellid", Cellid);
                            Log.d("Gender", Gender);
                            Log.d("Age", Age);

                            GenerateLocation = Location;
                            ScanLocation = Scan_Latitude + "," + Scan_Longitude;

                            Boolean TransIdPresentORNot = db.isTranscationPresent_QRcode(Transid);
                            System.out.println("TransIdPresentORNot Value "+TransIdPresentORNot);
                            if(TransIdPresentORNot == false)
                            {
                                LayoutInflater layoutInflater = LayoutInflater.from(this);
                                View promptView = layoutInflater.inflate(R.layout.activity_scan_pop_up, null);
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                                alertDialogBuilder.setView(promptView);
                                final TextView amtText, noText, no1Text, nameText, name1Text, timeText, otpText,subject;
                                final Button button;
                                amtText = (TextView) promptView.findViewById(R.id.amount);
                                noText = (TextView) promptView.findViewById(R.id.no);
                                nameText = (TextView) promptView.findViewById(R.id.name);
                                no1Text = (TextView) promptView.findViewById(R.id.no1);
                                name1Text = (TextView) promptView.findViewById(R.id.name1);
                                timeText = (TextView) promptView.findViewById(R.id.time);
                                otpText = (TextView) promptView.findViewById(R.id.otp);
                                //subject = (TextView) promptView.findViewById(R.id.subject);
                                button = (Button) promptView.findViewById(R.id.button);

                                amtText.setText("Amount  : " + Amount);
                                noText.setText("Sender No  : " + Sender_No);
                                nameText.setText("Sender Name  : " + Receiver_Name);
                                no1Text.setText("Receiver No  : " + Receiver_No);
                                name1Text.setText("Trans ID  : " + Transid);
                                timeText.setText("Date  : " + Date);
                                otpText.setText("Gender  : " + Gender);

                                final AlertDialog alertD = alertDialogBuilder.create();
                                alertD.show();
                                button.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        timeStamp = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date());
                                        db.open();
                                        db.Add_OkToSafetyCashier_ScanDetails(MasterORCashier_ID, timeStamp, Date, Receiver_Name, Amount, Transid, Sender_No, Receiver_No, Trans_Type, Kick_Back, Locality, Location, Cellid, Gender, Age, GenerateLocation, ScanLocation, SUCCESS);
                                        db.close();
                                        AlertList.clear();
                                        setlistview();
                                        alertD.dismiss();
                                    }
                                });
                            }
                            else {
                                showToast("Already Scanned..!");
                            }
                        }
                        else {
                            Toast.makeText(this, "Please Scan OK App QRCode", Toast.LENGTH_LONG).show();
                        }
                    }
                    catch (ArrayIndexOutOfBoundsException e)
                    {
                        Toast.makeText(this, "Please Try Again..!", Toast.LENGTH_LONG).show();
                    }
                }
            }
            else {
                Toast.makeText(this, "Phone Number MisMatch..!", Toast.LENGTH_SHORT).show();
            }

        } catch (GeneralSecurityException e) {
            e.printStackTrace();
            Toast.makeText(this, "Please Try Again Later", Toast.LENGTH_SHORT).show();
        } catch (IllegalArgumentException e) {
            Toast.makeText(this, "Please Scan OK App QRCode only..", Toast.LENGTH_SHORT).show();
        } catch (ArrayIndexOutOfBoundsException e)
        {
            Toast.makeText(this, "Please Try Again..!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public <T> void response(int resultCode, T data) {

    }
}

