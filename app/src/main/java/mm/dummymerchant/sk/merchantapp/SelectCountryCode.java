package mm.dummymerchant.sk.merchantapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import mm.dummymerchant.sk.merchantapp.Utils.Constant;
import mm.dummymerchant.sk.merchantapp.adapter.CountryCodeAdapter;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
import mm.dummymerchant.sk.merchantapp.model.NetworkOperatorModel;


public class SelectCountryCode extends BaseActivity implements AdapterView.OnItemClickListener {
    private ArrayList<NetworkOperatorModel> listOfCountryCodesAndNames;
    private AutoCompleteTextView search;
    private ArrayList<NetworkOperatorModel> selecteditems;
    private ArrayList<NetworkOperatorModel> itemsAll;
    CountryCodeAdapter code;
    View view_actionBar;

    Filter nameFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_select_country_code);
        listOfCountryCodesAndNames = new NetworkOperatorModel().getListOfNetworkOperatorModel(this);
        ListView listView = (ListView) findViewById(R.id.list);
        if (listOfCountryCodesAndNames!=null)
        selecteditems = (ArrayList<NetworkOperatorModel>) listOfCountryCodesAndNames.clone();
        code = new CountryCodeAdapter(this, Constant.listOfFlags, selecteditems);

        listView.setAdapter(code);

    //    buttonLogout.setVisibility(View.INVISIBLE);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        view_actionBar = LayoutInflater.from(this).inflate(R.layout.custom_action_bar_country, null);
        CustomTextView custom_action_bar_application = (CustomTextView) view_actionBar.findViewById(R.id.name);
        RelativeLayout actibar_bg = (RelativeLayout) view_actionBar.findViewById(R.id.custom_action_bar_application);
        actibar_bg.setBackgroundColor(getResources().getColor(R.color.pink));

        ImageView topup_back_btn = (ImageView) view_actionBar.findViewById(R.id.topup_back_btn_id);
        topup_back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    finish();

            }
        });
        custom_action_bar_application.setText("Select Country");
        CustomTextView topup_submit_btn = (CustomTextView) view_actionBar.findViewById(R.id.topup_submit_btn_id);
        topup_submit_btn.setVisibility(View.GONE);

        getSupportActionBar().setCustomView(view_actionBar);
        Toolbar parent = (Toolbar) view_actionBar.getParent();
        parent.setContentInsetsAbsolute(0, 0);
    //    actionBar = getSupportActionBar();
    //    mTitleOfHeader.setText("Select Country");

        listView.setOnItemClickListener(this);

        search = (AutoCompleteTextView) findViewById(R.id.search);

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                nameFilter.filter(editable.toString());


            }
        });


        nameFilter = new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                if (constraint != null) {
                    selecteditems.clear();
                    for (NetworkOperatorModel customer : listOfCountryCodesAndNames) {
                        if (customer.getCountryName().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                            selecteditems.add(customer);
                        }
                    }
                    FilterResults filterResults = new FilterResults();
                    filterResults.values = selecteditems;
                    filterResults.count = selecteditems.size();
                    return filterResults;
                } else {
                    return new FilterResults();
                }
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                code.notifyDataSetChanged();
            }
        };

    }

    @Override
    public <T> void response(int resultCode, T data) {

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent intent = new Intent();
        intent.putExtra("COUNTRY_CODE", selecteditems.get(i).getCountryCode());
        intent.putExtra("COUNTRY_NAME", selecteditems.get(i).getCountryName());
        intent.putExtra("POSITION", selecteditems.get(i).getCountryImageId());
        setResult(100, intent);
        finish();
    }
}
