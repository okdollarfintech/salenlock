package mm.dummymerchant.sk.merchantapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by user on 11/12/2015.
 */
public class SignaturePad extends BaseActivity implements com.github.gcacace.signaturepad.views.SignaturePad.OnSignedListener, View.OnClickListener
{
    public static com.github.gcacace.signaturepad.views.SignaturePad mSignaturePad;
    private Button set,clear,Cancel;
    private boolean isSigned = false;
    public static Bitmap signatureBitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signature_pad);

        mSignaturePad = (com.github.gcacace.signaturepad.views.SignaturePad) findViewById(R.id.signature_pad);
        set =(Button) findViewById(R.id.set);
        clear =(Button) findViewById(R.id.clear);
        Cancel =(Button) findViewById(R.id.Cancel);

        mSignaturePad.setOnSignedListener(this);
        set.setOnClickListener(this);
        clear.setOnClickListener(this);
        Cancel.setOnClickListener(this);
        setSignPadActionBar();

    }


    @Override
    public <T> void response(int resultCode, T data) {

    }

    @Override
    public void onSigned() {
        isSigned = true;
    }

    @Override
    public void onClear() {
        isSigned = false;
    }

    @Override
    public void onClick(View v) {


        switch (v.getId())
        {
            case R.id.clear:
                mSignaturePad.clear();
                break;
            case R.id.Cancel:
                onBackPressed();
                break;

            case R.id.set:
                signatureBitmap = mSignaturePad.getSignatureBitmap();
                Intent i=new Intent(SignaturePad.this, NewCashcollectorActivity.class);
                setResult(2, i);
                finish();
        }


    }
}
