package mm.dummymerchant.sk.merchantapp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;

import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;

/**
 * Created by user on 11/13/2015.
 */
public class SingleViewActivity extends BaseActivity{

    private CustomTextView edt_cashier_name,edt_cashier_no,edt_cashier_source;
    private ImageView imageView1,imageView;
    private String name,no,source;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_collectordetails);
        init();
        setNewCashcollectorActionBar(String.valueOf(getResources().getText(R.string.cashcollector_details)));
    }
    void init()
    {
        Bundle extras = getIntent().getExtras();
        byte[] b = extras.getByteArray("picture");
        byte[] b1 = extras.getByteArray("picturee");
        name = extras.getString("Name");
        no = extras.getString("No");
        source = extras.getString("Source");
        Bitmap bmp = BitmapFactory.decodeByteArray(b, 0, b.length);
        Bitmap bmp1 = BitmapFactory.decodeByteArray(b1, 0, b1.length);

        imageView1 = (ImageView) findViewById(R.id.imageView1);
        imageView = (ImageView)findViewById(R.id.imageView);
        edt_cashier_name= (CustomTextView) findViewById(R.id.edt_cashier_name);
        edt_cashier_no= (CustomTextView) findViewById(R.id.edt_cashier_no);
        edt_cashier_source= (CustomTextView) findViewById(R.id.edt_cashier_source);

        edt_cashier_name.setText(name);
        edt_cashier_no.setText(no);
        edt_cashier_source.setText(source);
        imageView1.setImageBitmap(bmp);
        imageView.setImageBitmap(bmp1);
    }

    @Override
    public <T> void response(int resultCode, T data) {

    }


}