package mm.dummymerchant.sk.merchantapp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Point;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.androidlegacy.Contents;
import com.scottyab.aescrypt.AESCrypt;

import java.io.File;
import java.io.FileOutputStream;
import java.security.GeneralSecurityException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.GpsTracker1;
import mm.dummymerchant.sk.merchantapp.customView.CustomButton;
import mm.dummymerchant.sk.merchantapp.customView.CustomEdittext;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
import mm.dummymerchant.sk.merchantapp.db.DBHelper;
import mm.dummymerchant.sk.merchantapp.model.CashierModel;
import mm.dummymerchant.sk.merchantapp.model.MasterDetails;
import mm.dummymerchant.sk.merchantapp.model.OTPCashierModel;
import mm.dummymerchant.sk.merchantapp.qrCode.QRCodeEncoder;

/**
 * Created by user on 11/17/2015.
 */
public class SingleViewActivity_Qrcode extends BaseActivity implements View.OnClickListener{

    private CustomTextView edt_cashier_name,edt_cashier_no,text,templete,qr_number,qr_time;;
    private ImageView imageView1,imageView_qrcode;
    private String name,no,encryptedMsg,qrInputText,type,encryptedMsgTime,qrInputTextTime;
    private CustomEdittext edt_cashier_Amount;
    private CustomButton Submit,Otp_button;
    private DBHelper dbb;
    String cash_id,cash_name,cash_no,master_no;
    RadioGroup rg;
    RadioButton r1;
    int pos;
    int pos1;
    String otpvalue="",cashiername,cashierno,cashcollectorname,cashcollectorno,cashierid,Time,amount;
    int count = 1;
    static String msg_unable_to_generatePin;
    private String  Generate_Latitude,Generate_Longitude,GenerateLocation;
    private String editinput;
    LinearLayout container;
    private ArrayList<String> value = new ArrayList<String>();
    private ArrayList<String> value1 = new ArrayList<String>();
    int childCount;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_collectordetails_qrcode);
        msg_unable_to_generatePin=getString(R.string.unable_generate_pin);

        init();
        setExistingCashcollectorActionBar(String.valueOf(getResources().getText(R.string.cashcollector_details)));
        dbb = new DBHelper(this);

    }
    void init()
    {
        edt_cashier_Amount= (CustomEdittext) findViewById(R.id.edt_cashier_Amount);
        Submit= (CustomButton) findViewById(R.id.Submit);
        Otp_button= (CustomButton) findViewById(R.id.Otp_button);
        rg = (RadioGroup) findViewById(R.id.radioGroup1);
        r1=(RadioButton) findViewById(R.id.radio0);
        Bundle extras = getIntent().getExtras();
        byte[] b = extras.getByteArray("picture");
        name = extras.getString("Name");
        no = extras.getString("No");
        Bitmap bmp = BitmapFactory.decodeByteArray(b, 0, b.length);

        imageView1 = (ImageView) findViewById(R.id.imageView1);
        imageView_qrcode = (ImageView)findViewById(R.id.imageView_qrcode);
        edt_cashier_name= (CustomTextView) findViewById(R.id.edt_cashier_name);
        edt_cashier_no= (CustomTextView) findViewById(R.id.edt_cashier_no);
        text = (CustomTextView)findViewById(R.id.text);
        templete = (CustomTextView)findViewById(R.id.templete);
        qr_number = (CustomTextView)findViewById(R.id.qr_number);
        qr_time = (CustomTextView)findViewById(R.id.qr_time);

        edt_cashier_name.setText(name);
        edt_cashier_no.setText(no);
        type = TYPE_QRCODE;

        imageView1.setImageBitmap(bmp);
        Otp_button.setOnClickListener(this);
        Submit.setOnClickListener(this);
        text.setOnClickListener(this);
        templete.setOnClickListener(this);

        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // TODO Auto-generated method stub
                // Method 1 For Getting Index of RadioButton
                // Method 1
                pos = rg.indexOfChild(findViewById(checkedId));
                //Method 2
                pos1 = rg.indexOfChild(findViewById(rg.getCheckedRadioButtonId()));

                switch (pos) {
                    case 0:
                        r1.setChecked(true);
                        imageView_qrcode.setVisibility(View.INVISIBLE);
                        type = TYPE_QRCODE;
                        break;
                    case 1:
                        type = TYPE_SMS;
                        imageView_qrcode.setVisibility(View.INVISIBLE);
                        otpvalue = "";
                        break;
                    default:
                        type = TYPE_QRCODE;
                        // imageView_qrcode.setVisibility(View.VISIBLE);
                        break;
                }
            }

        });


        edt_cashier_Amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {

                imageView_qrcode.setVisibility(View.GONE);
                qr_number.setText("");
                qr_time.setText("");
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });
    }

    public Bitmap generateQRCode(String qrInputText){
        //Find screen size
        WindowManager manager = (WindowManager) getSystemService(WINDOW_SERVICE);
        Display display = manager.getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);
        int width = point.x;
        int height = point.y;
        int smallerDimension = width < height ? width : height;
        smallerDimension = smallerDimension * 3/4;
        System.out.println("smallerDimension" + smallerDimension);
        //Encode with a QR Code image
        QRCodeEncoder qrCodeEncoder = new QRCodeEncoder(qrInputText,
                null,
                Contents.Type.TEXT,
                BarcodeFormat.QR_CODE.toString(),
                1000);
        try {
            Bitmap bitmap = qrCodeEncoder.encodeAsBitmap();
            Bitmap bitLogo = BitmapFactory.decodeResource(getResources(),
                    R.drawable.ok
            );
            Bitmap bitMerged = mergeBitmaps(bitmap, bitLogo);

            return bitMerged;


        } catch (WriterException e) {
            e.printStackTrace();

            return null;
        }
    }

    public static Bitmap mergeBitmaps(Bitmap bmp1, Bitmap bmp2) {
        Bitmap bmOverlay = Bitmap.createBitmap(bmp1.getWidth(),
                bmp1.getHeight(), bmp1.getConfig());
        int centreX = (bmp1.getWidth()  - bmp2.getWidth()) /2;
        int centreY = (bmp1.getHeight() - bmp2.getHeight()) /2 ;
        Canvas canvas = new Canvas(bmOverlay);
        canvas.drawBitmap(bmp1, new Matrix(), null);
        canvas.drawBitmap(bmp2, centreX, centreY, null);

        return bmOverlay;
    }

    @Override
    public <T> void response(int resultCode, T data) {

    }

    public static int generatePin() throws Exception {
        Random generator = new Random();
        generator.setSeed(System.currentTimeMillis());

        int num = generator.nextInt(99999) + 99999;
        if (num < 100000 || num > 999999) {
            num = generator.nextInt(99999) + 99999;
            if (num < 100000 || num > 999999)
            {
                throw new Exception(msg_unable_to_generatePin);
            }
        }
        return num;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.Submit:
                amount = edt_cashier_Amount.getText().toString().trim();
                Get_Location();
                Log.i("Latitude after Getting", Generate_Latitude);
                Log.i("Longitude after Getting", Generate_Longitude);
                GenerateLocation = Generate_Latitude + "," + Generate_Longitude;
                Log.i("GenerateLocation", GenerateLocation);
                Log.i("Amount : ", amount);

                System.out.println("text editinput value " + editinput);
                System.out.println("template ArrayList Value " + value.toString());

                if(!amount.equalsIgnoreCase(""))
                {
                    Log.i("Click Submit amount : ", amount);
                    DateFormat targetFormat = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
                    Time=targetFormat.format(new Date());
                    encryptedMsgTime = Time +"----" + no;
                    try
                    {
                        otpvalue = String.valueOf(generatePin());
                    }
                    catch(Exception e)
                    {
                        e.printStackTrace();
                    }
                    String a = AppPreference.getMasterOrCashier(this);
                    if(a.equals(MASTER))
                    {
                        String Master_name = MASTER_TAG,id="MASTER_ID";
                        MasterDetails masterDetails =dbb.getMasterDetails();
                        master_no = masterDetails.getUsername();
                        if (type.equals(TYPE_QRCODE))
                        {
                            Finaldata(name,no,Master_name,master_no,MasterORCashier_ID);
                        }
                        else {
                            Finaldata(name,no,Master_name,master_no,MasterORCashier_ID);
                            takeScreenshot();
                        }
                    }
                    else {
                        cash_id = SliderScreen.present_Cashier_Id;
                        CashierModel cashierModel = dbb.getCashierModel(cash_id);
                        cash_name  = cashierModel.getCashier_Name();
                        cash_no = cashierModel.getCashier_Number();
                        if (type.equals(TYPE_QRCODE))
                        {
                            Finaldata(name,no,cash_name,cash_no,cash_id);
                        }
                        else
                        {
                            Finaldata(name,no,cash_name,cash_no,cash_id);
                            takeScreenshot();
                        }
                    }

                }
                else
                {
                    String wrongText=getString(R.string.pls_entrblw_fields) + "\n";
                    if(amount.equalsIgnoreCase(""))
                        wrongText+=getString(R.string.entr_amount) + "\n";

                    showToast(wrongText);
                }

                break;

            case R.id.Otp_button:
                amount = edt_cashier_Amount.getText().toString().trim();
                if(!amount.equalsIgnoreCase("") && !otpvalue.equals(""))
                {
                    Log.i("OTP Value ",otpvalue);
                    LayoutInflater layoutInflater = LayoutInflater.from(this);
                    View promptView = layoutInflater.inflate(R.layout.otp_prompts, null);
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                    alertDialogBuilder.setView(promptView);
                    final EditText input = (EditText) promptView.findViewById(R.id.userInput);
                    // setup a dialog window
                    alertDialogBuilder
                            .setCancelable(false)
                            .setPositiveButton(OK, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // get user input and set it to result
                                    String Edit_otp_value = input.getText().toString().trim();

                                    if(!Edit_otp_value.equalsIgnoreCase(""))
                                    {
                                        if(count <=4)
                                        {
                                            if(Edit_otp_value.equals(otpvalue))
                                            {
                                                OTPCashierModel cashout_details = new OTPCashierModel(Time,cashiername,cashierno,cashcollectorname,cashcollectorno,amount,SUCCESS,cashierid,"",editinput,value.toString(),"");
                                                db.insertOTPCashierDetails(cashout_details);
                                                showToast1(getString(R.string.rightotp));
                                                input.setText("");
                                                edt_cashier_Amount.setText("");
                                                imageView_qrcode.setVisibility(View.GONE);
                                                qr_number.setText("");
                                                qr_time.setText("");
                                                text.setText("");
                                            }
                                            else {
                                                showToast(getString(R.string.wrongotp));
                                                count = count + 1;
                                                input.setText("");
                                                qr_number.setText("");
                                                qr_time.setText("");
                                                text.setText("");
                                            }
                                        }
                                        else {
                                            showToast(getString(R.string.wrongotp_manytimes));
                                            OTPCashierModel cashout_details = new OTPCashierModel(Time,cashiername,cashierno,cashcollectorname,cashcollectorno,amount,FAILURE,cashierid,"",editinput,value.toString(),"");
                                            db.insertOTPCashierDetails(cashout_details);
                                            input.setText("");
                                            edt_cashier_Amount.setText("");
                                            imageView_qrcode.setVisibility(View.GONE);
                                            qr_number.setText("");
                                            qr_time.setText("");
                                            count = 1;
                                        }
                                    }
                                    else {
                                        String wrongText=getString(R.string.pls_entrblw_fields) + "\n";
                                        if(Edit_otp_value.equalsIgnoreCase(""))
                                            wrongText+=getString(R.string.entr_otp) + "\n";

                                        showToast(wrongText);
                                    }

                                }

                            })
                            .setNegativeButton(getString(R.string.cancel),

                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }

                                    });
                    // create an alert dialog
                    AlertDialog alertD = alertDialogBuilder.create();
                    alertD.show();

                }
                else
                {
                    String wrongText=getString(R.string.pls_entrblw_fields) + "\n";
                    if(amount.equalsIgnoreCase(""))
                        wrongText+=getString(R.string.entr_amount) + "\n";
                    if(otpvalue.equalsIgnoreCase(""))
                        wrongText+=getString(R.string.and_gnr_qrcode) + "\n";
                    showToast(wrongText);
                }

                break;

            case R.id.text:
                // Create custom dialog object
                final Dialog dialog = new Dialog(SingleViewActivity_Qrcode.this);
                // Include dialog.xml file
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.text_prompts);
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
                CustomTextView close  = (CustomTextView) dialog.findViewById(R.id.close);
                Button reset = (Button) dialog.findViewById(R.id.reset);
                Button ok = (Button) dialog.findViewById(R.id.ok);
                final EditText input = (EditText) dialog.findViewById(R.id.input);

                input.setText(editinput);
                // if decline button is clicked, close the custom dialog
                reset.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        input.setText("");
                        editinput = "";
                    }
                });
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String Edit_input_value = input.getText().toString().trim();
                        dialog.dismiss();
                        editinput = Edit_input_value;
                        text.setText(Edit_input_value);
                    }
                });
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Close dialog
                        dialog.dismiss();
                    }
                });
                input.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                        imageView_qrcode.setVisibility(View.GONE);
                        qr_number.setText("");
                        qr_time.setText("");
                    }
                    @Override
                    public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                                  int arg3) {
                        // TODO Auto-generated method stub
                    }
                    @Override
                    public void afterTextChanged(Editable arg0) {

                    }
                });

                break;

            case R.id.templete:
                final Dialog dialog1 = new Dialog(SingleViewActivity_Qrcode.this);
                dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog1.setContentView(R.layout.template_prompts);
                dialog1.setCanceledOnTouchOutside(false);
                dialog1.show();
                CustomTextView add  = (CustomTextView) dialog1.findViewById(R.id.add);
                CustomTextView close1  = (CustomTextView) dialog1.findViewById(R.id.close);
                Button cancel = (Button) dialog1.findViewById(R.id.cancel);
                Button ok1 = (Button) dialog1.findViewById(R.id.ok);
                final CustomEdittext item = (CustomEdittext) dialog1.findViewById(R.id.item);
                final CustomEdittext qty = (CustomEdittext) dialog1.findViewById(R.id.qty);
                final CustomEdittext price = (CustomEdittext) dialog1.findViewById(R.id.price);
                container = (LinearLayout) dialog1.findViewById(R.id.container);
                int count = 0;
                for(int i=0 ; i<value.size();i++)
                {
                    try {
                        String s = value.get(0);
                        //System.out.println(" 0 th" + " S value  " + s);
                        String[] separated = s.split("/");
                        item.setText(separated[0]);
                        qty.setText(separated[1]);
                        price.setText(separated[2]);
                        count++;
                    }
                    catch (ArrayIndexOutOfBoundsException e){
                    }
                }
                for(int i=0 ; i<count-1;i++)
                {
                    addFunction(container);
                }
                System.out.println("Count values "+count);
                System.out.println("value size " + value.size());
                System.out.println("value1 size " + value1.size());

                for(int i=0 ; i<value1.size() ;i++)
                {
                    View childView1 = container.getChildAt(i);
                    CustomEdittext itemEdt = (CustomEdittext) (childView1.findViewById(R.id.item));
                    CustomEdittext qtyEdt = (CustomEdittext) (childView1.findViewById(R.id.qty));
                    CustomEdittext priceEdt = (CustomEdittext) (childView1.findViewById(R.id.price));
                    try {
                        String s = value1.get(i);
                        // System.out.println(i + " th" + " S value :childCount   " + s);
                        String[] separated = s.split("/");
                        /*System.out.println(i + " th" + " separated item value :childCount " + separated[0]);*/
                        if ((separated[0] != null) || !(separated[0].equals(""))) {
                            itemEdt.setText(separated[0]);
                        }
                        if ((separated[1] != null) || !(separated[1].equals(""))) {
                            qtyEdt.setText(separated[1]);
                        }
                        if ((separated[2] != null) || !(separated[2].equals(""))) {
                            priceEdt.setText(separated[2]);
                        }
                    }catch (ArrayIndexOutOfBoundsException e){
                    }
                }
                add.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // dialog1.dismiss();
                        addFunction(container);
                        imageView_qrcode.setVisibility(View.GONE);
                        qr_number.setText("");
                        qr_time.setText("");
                    }
                });

                close1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showToast("Don't do like this");
                    }
                });
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog1.dismiss();
                    }
                });

                ok1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog1.dismiss();
                        String itemvalue,qtyvalue,pricevalue;
                        itemvalue = item.getText().toString();
                        qtyvalue = qty.getText().toString();
                        pricevalue = price.getText().toString();
                        String SingleArrayIndex1,SingleArrayIndex2;
                        value.clear();
                        value1.clear();
                        childCount = container.getChildCount();
                        imageView_qrcode.setVisibility(View.GONE);
                        qr_number.setText("");
                        qr_time.setText("");
                        System.out.println("The ChildCount ok click: " + childCount);
                        if( !itemvalue.equals("") || !qtyvalue.equals("") || !pricevalue.equals(""))
                        {
                            SingleArrayIndex1 = itemvalue + "/" + qtyvalue + "/" + pricevalue ;
                            value.add(SingleArrayIndex1);
                        }
                        for (int ct = 0; ct < childCount; ct++)
                        {
                            View childView1 = container.getChildAt(ct);
                            CustomEdittext itemEdt = (CustomEdittext) (childView1.findViewById(R.id.item));
                            CustomEdittext qtyEdt = (CustomEdittext) (childView1.findViewById(R.id.qty));
                            CustomEdittext priceEdt = (CustomEdittext) (childView1.findViewById(R.id.price));

                            String item = itemEdt.getText().toString();
                            String qty = qtyEdt.getText().toString();
                            String price = priceEdt.getText().toString();

                            if( !item.equals("") || !qty.equals("") || !price.equals(""))
                            {
                                SingleArrayIndex2 = item + "/" + qty + "/" + price ;
                                value.add(SingleArrayIndex2);
                                value1.add(SingleArrayIndex2);
                            }
                            Log.i("ITEM ",item);
                            Log.i("QUANTITY ",qty);
                            Log.i("PRICE ", price);
                        }
                        Log.i("Array Value",value.toString());
                    }
                });

                break;



        }
    }

    private void  addFunction(LinearLayout container_id)
    {
        final LinearLayout Container_id = container_id;
        LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View addView = layoutInflater.inflate(R.layout.row, null);
        final CustomEdittext item = (CustomEdittext) addView.findViewById(R.id.item);
        final CustomEdittext qty = (CustomEdittext) addView.findViewById(R.id.qty);
        final CustomEdittext price = (CustomEdittext) addView.findViewById(R.id.price);
        CustomTextView remove  = (CustomTextView) addView.findViewById(R.id.close);

        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((LinearLayout) addView.getParent()).removeView(addView);
                imageView_qrcode.setVisibility(View.GONE);
                qr_number.setText("");
                qr_time.setText("");
            }
        });

        Container_id.addView(addView);
    }

    public void Get_Location()
    {
        GpsTracker1 gps = new GpsTracker1(getApplicationContext());
        Location loc = gps.getLocation();

        if(loc!=null) {
            Generate_Latitude = "" + loc.getLatitude();
            Generate_Longitude = "" + loc.getLongitude();
            Log.i("Generate_Latitude",Generate_Latitude);
            Log.i("Generate_Longitude",Generate_Longitude);
        }
        else {
            Generate_Latitude = "0";
            Generate_Longitude = "0";
            Log.i("Generate_Latitude",Generate_Latitude);
            Log.i("Generate_Longitude",Generate_Longitude);
        }
    }


    private void Finaldata(String customername, String customerno, String cash_name, String cash_no, String cash_id)
    {
        encryptedMsg = SAFETYCASHIER_TO_CASHCOLLECTOR + "-" + customername + "-" + customerno + "-" + amount + "-" + cash_name + "-" + cash_no + "-" + cash_id + "-" + otpvalue + "-" + Time + "-" + Generate_Latitude + "-" + Generate_Longitude + "-" + editinput + "-" + value.toString();

       // encryptedMsg = SAFETYCASHIER_TO_CASHCOLLECTOR + "-" + name + "-" + no+"-"+ amount+ "-"+Master_name + "-" + master_no + "-"+ id + "-" + otpvalue + "-" + Time + "-" + Generate_Latitude + "-" + Generate_Longitude + "-" + editinput + "-" + value.toString();
        cashcollectorname = customername;
        cashcollectorno = customerno;
        cashiername = cash_name;
        cashierno = cash_no;
        cashierid = cash_id;
        Encryt_Data(encryptedMsg);
    }

    private void Encryt_Data(String str_encryptedMsg) {

        DateFormat targetFormat = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss");
        Time=targetFormat.format(new Date());
        try {
            qrInputTextTime = AESCrypt.encrypt(SPEEDKey, encryptedMsgTime);
            qrInputText = AESCrypt.encrypt(encryptedMsgTime, str_encryptedMsg);
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        }
        if (generateQRCode(qrInputText) != null) {
            imageView_qrcode.setVisibility(View.VISIBLE);
            imageView_qrcode.setImageBitmap(generateQRCode(qrInputTextTime + "---" + qrInputText));
            Log.i("otpvalue ", otpvalue);
            if (name.equals(getResources().getString(R.string.Unknown))) {
                qr_number.setText(no + " - " + amount);
                qr_time.setText(Time);
            } else {
                qr_number.setText(no + " - " + amount);
                qr_time.setText(Time);
            }
            hideKeyboard();
        }
    }

    private void takeScreenshot()
    {
              ScrollView sv = (ScrollView)findViewById(R.id.scroll1);
        sv.scrollTo(0, sv.getBottom());


        Toast.makeText(this, "scroll happened", Toast.LENGTH_SHORT).show();


        Date now = new Date();
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);

        try {
            // image naming and path  to include sd card  appending name you choose for file
            String mPath = Environment.getExternalStorageDirectory().getAbsolutePath()+"/screen3.jpg";

            // create bitmap screen capture
            View v1 = getWindow().getDecorView().getRootView();
            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap = v1.getDrawingCache();

            File imageFile = new File(mPath);

            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();

            //  openScreenshot(imageFile);
            shareImage();
        } catch (Throwable e)
        {
            e.printStackTrace();

        }
    }



    private void shareImage()
    {
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("image/*");
        String imagePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/screen2.jpg";
        File imageFileToShare = new File(imagePath);
        Uri uri = Uri.fromFile(imageFileToShare);
        share.putExtra(Intent.EXTRA_STREAM, uri);
        startActivity(Intent.createChooser(share, "Share Image!"));
    }

}
