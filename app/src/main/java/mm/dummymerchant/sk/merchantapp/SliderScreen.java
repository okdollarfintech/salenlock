package mm.dummymerchant.sk.merchantapp;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SlidingPaneLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.util.ArrayList;

import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.WriteToExcel;
import mm.dummymerchant.sk.merchantapp.adapter.Dashboard_adapter;
import mm.dummymerchant.sk.merchantapp.adapter.SliderAdapter;
import mm.dummymerchant.sk.merchantapp.customView.AlertDialogRadio;
import mm.dummymerchant.sk.merchantapp.customView.CustomButton;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
import mm.dummymerchant.sk.merchantapp.customView.DrawerArrowDrawable;
import mm.dummymerchant.sk.merchantapp.db.DBHelper;
import mm.dummymerchant.sk.merchantapp.fragments.DashBoardFragment;
import mm.dummymerchant.sk.merchantapp.httpConnection.CallingApIS;
import mm.dummymerchant.sk.merchantapp.httpConnection.RequestProgressDialog;
import mm.dummymerchant.sk.merchantapp.model.CashierActivityLogModel;
import mm.dummymerchant.sk.merchantapp.model.CashierModel;
import mm.dummymerchant.sk.merchantapp.model.GCM_RegistrationModel;
import mm.dummymerchant.sk.merchantapp.model.MasterDetails;
import mm.dummymerchant.sk.merchantapp.model.dashboard_model;
import mm.dummymerchant.sk.merchantapp.wirelessService.WirelessUtils;

/**
 * Created by Dell on 11/17/2015.
 */
public class SliderScreen extends BaseActivity implements AlertDialogRadio.AlertPositiveListener,SlidingPaneLayout.PanelSlideListener{
    SlidingPaneLayout mSlidingLayout;
    private ListView lv;
    ArrayList<CashierActivityLogModel> al_cashierActivityModel;
    SliderAdapter adapter;
    public static String present_Cashier_Id;
    CustomButton btn_addcashier;
    private RequestProgressDialog prgDialog;
    private GoogleCloudMessaging gcmObj;

    int position = 0;
    public  View.OnClickListener listener;
    String[] code;
    private String regID = "";
    AlertDialog dialog1;

    private DrawerArrowDrawable drawerArrowDrawable;
    private float offset;
    private boolean flipped;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.slider_screen);

        db=new DBHelper(getApplicationContext());
        db.open();
        init();
        loadItemsforList();
        loadAdapter();
        setDashboardActionBar();
        callFragment(new DashBoardFragment());
        code = getResources().getStringArray(R.array.temp_logout_arrays);

    if(AppPreference.getMasterOrCashier(this).equalsIgnoreCase(MASTER))
        btn_addcashier.setVisibility(View.INVISIBLE);


        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                slide();

            }
        });

        custom_action_bar_lay_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                slide();

            }
        });


        btn_addcashier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //callNewStartupActivity();
                finish();
            }
        });


        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                CashierActivityLogModel clm = (CashierActivityLogModel) adapter.getItem(i);
                present_Cashier_Id = clm.getCashier_Id();
                loadItemsforList();
                loadAdapter();
            }
        });

        db.close();

        if (!AppPreference.getGCMRegister(SliderScreen.this)) {
            registerApplication();
        }

        mSlidingLayout.setPanelSlideListener(this);

        final Resources resources = getResources();

        drawerArrowDrawable = new DrawerArrowDrawable(resources);
        drawerArrowDrawable.setStrokeColor(resources.getColor(R.color.white));
        backButton.setImageDrawable(drawerArrowDrawable);

    }

    void init()
    {
        mSlidingLayout = (SlidingPaneLayout)findViewById(R.id.sliding_pane_layout);
        lv= (ListView) findViewById(R.id.list_slidermenu);
        btn_addcashier=(CustomButton)findViewById(R.id.btn_addcashier);

        listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /** Getting the fragment manager */
                FragmentManager manager = getFragmentManager();
                /** Instantiating the DialogFragment class */
                AlertDialogRadio alert = new AlertDialogRadio();
                /** Creating a bundle object to store the selected item's index */
                Bundle b  = new Bundle();
                /** Storing the selected item's index in the bundle object */
                b.putInt("position", position);
                /** Setting the bundle object to the dialog fragment object */
                alert.setArguments(b);
                /** Creating the dialog fragment object, which will in turn open the alert dialog window */
                alert.show(manager, "alert_dialog_radio");
                //Toast.makeText(getApplicationContext(), "position " + position, Toast.LENGTH_SHORT).show();
            }
        };

    }

    void   loadItemsforList()
    {
        if(AppPreference.getMasterOrCashier(this).equalsIgnoreCase(CASHIER))
        al_cashierActivityModel=db.getNumberOfCashiers();
        else {
            al_cashierActivityModel = new ArrayList<CashierActivityLogModel>();
         MasterDetails mm= db.getMasterDetails();
            String number = "0" + mm.getUsername().substring(4);
            CashierActivityLogModel model = new CashierActivityLogModel("", MASTER_TAG, MASTER_TAG, number, "", "", "", "");
            al_cashierActivityModel.add(model);
        }


    }

    void loadAdapter()
    {
        if(al_cashierActivityModel!=null) {

            if(al_cashierActivityModel.size()==1)
                present_Cashier_Id=al_cashierActivityModel.get(0).getCashier_Id();

            if(al_cashierActivityModel.size()==0)
            {
                if(AppPreference.getMasterOrCashier(this).equalsIgnoreCase(CASHIER)) {
                   // callNewStartupActivity();
                    finish();
                }
                else
                {

                }
            }
           if(AppPreference.getMasterOrCashier(this).equals(CASHIER))
           {
               if (al_cashierActivityModel.size() >= 3)
                   btn_addcashier.setVisibility(View.INVISIBLE);
               else
                   btn_addcashier.setVisibility(View.VISIBLE);
           }

            adapter = new SliderAdapter(al_cashierActivityModel, this);
            lv.setAdapter(adapter);
        }
        else
        {
            if(AppPreference.getMasterOrCashier(this).equalsIgnoreCase(CASHIER)) {
               // callNewStartupActivity();
                finish();
            }
            else
            {

            }
        }
    }

    public void callCallingApIS()
    {
        CallingApIS api= new CallingApIS(this,this);
        api.callLogoutAPI();

    }

    @Override
    public <T> void response(int resultCode, T data) {

        switch (resultCode) {
            case REQUEST_CHECK_BALANCE_LOGIN:
                if (!AppPreference.getGCMRegister(SliderScreen.this)) {
                    registerApplication();
                }

                break;
            case REQUEST_TYPE_GCM_REGISTRATION:
                AppPreference.setGCMRegister(SliderScreen.this, true);
                GCM_RegistrationModel model = (GCM_RegistrationModel) data;
                if (model.getResultcode().equalsIgnoreCase("0")) {
                    showToast((R.string.gcm_success_msg));
                    AppPreference.setRegistrationID(this, regID);
                    CallingApIS api = new CallingApIS(this, this,1);
                    Log.d("reg:: estel--", regID);
                    api.callWebService_ForApp_Service(regID);
                    //callWebService_ForApp_Service(regID);
//                    if (Utils.isEmpty(AppPreference.getEncryptionValue(this))) {
//                        CallEncryptedKey();
//                    }
                }
                break;

            case REQUEST_TYPE_GCM_REGISTRATION_APPSERVER:

                break;

            case REQUEST_TYPE_LOGOUT:
                showToast(R.string.logout_sucessful);
                AppPreference.setAuthToken(SliderScreen.this, "XX");
                AppPreference.setNewContact(SliderScreen.this, "");
                AppPreference.setFinishActivity(SliderScreen.this, true);
                if (!AppPreference.isOnByMerchant(SliderScreen.this))
                {
                    WirelessUtils.getInstance().setBluetooth(false, SliderScreen.this);
                }
                countDownTimer.cancel();
                dialog1.dismiss();
                finish();
                break;

            default:
                break;
        }

    }

    void callFragment(Fragment fgm)
    {
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager .beginTransaction().replace(R.id.frame_container, fgm).commit();
    }

    public Context getContextforFragment()
    {
        return this;
    }

    @Override
    public void onBackPressed()
    {
    /* DashBoardFragment fgm=(DashBoardFragment) getFragmentManager().findFragmentById(R.id.frame_container);
        fgm.exitAPp();*/
      //  exitAPp();
    }

  public  void callLogout(String reason)
    {
        if(AppPreference.getMasterOrCashier(this).equalsIgnoreCase(CASHIER)) {
            CashierModel cm = db.getCashierObject(present_Cashier_Id);
            db.updateCashierAttendanceDetails(present_Cashier_Id, reason);

            WriteToExcel wf=new WriteToExcel();//for writing activity log on the  excel file
            wf.convertToExcelFile(this);

            loadItemsforList();
            if (al_cashierActivityModel != null) {
                if (al_cashierActivityModel.size() > 1) {
                    CashierActivityLogModel csm = al_cashierActivityModel.get(0);
                    present_Cashier_Id = csm.getCashier_Id();
                }
            }
            loadAdapter();
        }
        else
        {
            finish();
        }

    }

    @Override
    public void onPositiveClick(int position) {
        this.position = position;
        /** Getting the reference of the textview from the main layout */
       // final TextView tv = (TextView)findViewById(R.id.tv_android);
        /** Setting the selected android version in the textview */
     //   tv.setText("Your Other Reason : " + code[this.position]);

        if (code[this.position].equals("Others"))
        {
            //Toast.makeText(getApplicationContext(), "Clicked Others", Toast.LENGTH_SHORT).show();

            LayoutInflater layoutInflater = LayoutInflater.from(this);
            View promptView = layoutInflater.inflate(R.layout.prompts, null);
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setView(promptView);
            final EditText input = (EditText) promptView.findViewById(R.id.userInput);
            // setup a dialog window
            alertDialogBuilder
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // get user input and set it to result
                            String reason = input.getText().toString();
                            callLogout(reason);
                           // tv.setText("Your Other Reason : "+input.getText());
                        }

                    })
                    .setNegativeButton("Cancel",

                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }

                            });
            // create an alert dialog
            AlertDialog alertD = alertDialogBuilder.create();
            alertD.show();
        }
        else
            callLogout(code[this.position]);



    }

    void callNewStartupActivity()
    {
        Intent intent=new Intent(this,NewStartupActivity.class);
        startActivity(intent);
        finish();
    }

    public void callCashierNotcreatedToastMsg() {
        showToast(getString(R.string.cashier_not_created));
    }

    private void registerInBackground() {

        new AsyncTask<Void, Void, String>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                prgDialog = new RequestProgressDialog(SliderScreen.this);
                prgDialog.show();
            }

            @Override
            protected String doInBackground(Void... params) {
                String msg;
                try {
                   // System.out.println("gcmObj : " + gcmObj);
                    if (gcmObj == null) {
                        gcmObj = GoogleCloudMessaging
                                .getInstance(getApplicationContext());
                        System.out.println("gcmObj after assign : " + gcmObj);
                    }
                           try
                           {
                                gcmObj.unregister();
                           }
                           catch (Exception e)
                           {
                              System.out.println("Exception" + e);
                           }

                    regID = gcmObj.register(GOOGLE_PROJ_ID);

                    System.out.println("regID from gcmObj : " + regID);

                    msg = "Registration ID :" + regID;
                    System.out.println("TEST rec- msg-" + msg +"&*");
                    AppPreference.setRegistrationID(SliderScreen.this, regID);
                } catch (Exception ex) {
                    msg = "Error :" + ex.getMessage();

                }
                return msg;

            }

            @Override
            protected void onPostExecute(String msg) {
                prgDialog.dismiss();
                if (!TextUtils.isEmpty(regID)) {
                    CallingApIS CallApi = new CallingApIS(SliderScreen.this, SliderScreen.this);
                    CallApi.callWebServiceForGCM("Comments here", GOOGLE_PROJ_ID, regID);
                }
            }
        }.execute(null, null, null);
    }


    public void registerApplication() {
        if (checkPlayServices()) {
            registerInBackground();
        }
    }

    void slide()
    {
        if (!mSlidingLayout.isOpen()) {
            mSlidingLayout.openPane();

        }
        else {
            mSlidingLayout.closePane();

        }
    }


    public void exitAPp() {
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.gps_alert_dialog, null);

        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptsView);

        CustomTextView title = (CustomTextView) promptsView.findViewById(R.id.title);
        CustomTextView alert = (CustomTextView) promptsView.findViewById(R.id.alert);
        title.setText(R.string.logout_title);
        alert.setText(R.string.logout_msg);
        CustomButton yes = (CustomButton) promptsView.findViewById(R.id.btn_settings);
        CustomButton no = (CustomButton) promptsView.findViewById(R.id.btn_cancel);
        yes.setText(R.string.dailog_yes);
        no.setText(R.string.dailog_no);

        dialog1 = alertDialogBuilder.create();
        yes.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                AppPreference.setAuthToken(SliderScreen.this, "");
                AppPreference.setFinishActivity(SliderScreen.this, true);
                if (AppPreference.getMerchantStatus(SliderScreen.this)) {
                    WirelessUtils.getInstance().setBluetooth(false, SliderScreen.this);
                    WirelessUtils.getInstance().changeBluetoothName(false, SliderScreen.this);
                    WirelessUtils.getInstance().setHotspotOFF(SliderScreen.this);

                } else {
                    WirelessUtils.getInstance().setBluetooth(false, SliderScreen.this);
                }

                if (al_cashierActivityModel.size() == 1) {
                    callLogout(LOGOUT_PERMANENT_MSG);
                    callCallingApIS();
                } else {

                }

            }
        });
        no.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog1.dismiss();
            }
        });
        dialog1.show();

    }


   public void callListener()
    {
        FragmentManager manager = getFragmentManager();
        /** Instantiating the DialogFragment class */
        AlertDialogRadio alert = new AlertDialogRadio();
        /** Creating a bundle object to store the selected item's index */
        Bundle b  = new Bundle();
        /** Storing the selected item's index in the bundle object */
        b.putInt("position", position);
        /** Setting the bundle object to the dialog fragment object */
        alert.setArguments(b);
        /** Creating the dialog fragment object, which will in turn open the alert dialog window */
        alert.show(manager, "alert_dialog_radio");
        //Toast.makeText(getApplicationContext(), "position " + position, Toast.LENGTH_SHORT).show();
    }

    public void callBasedOnIndex(String id)
    {
        Intent intent;
        switch(id)
        {
            case "0":
                intent =new Intent(SliderScreen.this,CashierMenuActivity.class);
                startActivity(intent);
                break;

            case "1":
                intent =new Intent(SliderScreen.this,ActivityLogDetails.class);
                startActivity(intent);
                break;

            case "2":
                intent =new Intent(SliderScreen.this,OkDollarTransactionsActivity.class);
                startActivity(intent);
                break;

            case "3":
                intent =new Intent(SliderScreen.this,CashCollector.class);
                startActivity(intent);
                break;

            case "4":
                intent =new Intent(SliderScreen.this,QRcodeActivity.class);
                startActivity(intent);
                break;
            case "5":
                DashBoardFragment fragment = (DashBoardFragment) getFragmentManager().findFragmentById(R.id.frame_container);
                fragment.exitAPp();
                break;
            case "6":
                callListener();
                break;
            case "7":
                intent =new Intent(SliderScreen.this,Cashout.class);
                startActivity(intent);
                break;
            case "8":
                intent =new Intent(SliderScreen.this,NewSettingActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }

    }




    @Override
    public void onPanelSlide(View panel, float slideOffset) {
        offset = slideOffset;

        // Sometimes slideOffset ends up so close to but not quite 1 or 0.
        if (slideOffset >= .995) {
            flipped = true;
            drawerArrowDrawable.setFlip(flipped);
        } else if (slideOffset <= .005) {
            flipped = false;
            drawerArrowDrawable.setFlip(flipped);
        }

        drawerArrowDrawable.setParameter(offset);
    }

    @Override
    public void onPanelOpened(View panel) {

    }

    @Override
    public void onPanelClosed(View panel) {

    }



    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return false;
    }
}


