package mm.dummymerchant.sk.merchantapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import org.json.JSONArray;

import java.util.ArrayList;

import mm.dummymerchant.sk.merchantapp.Utils.JsonParser;
import mm.dummymerchant.sk.merchantapp.Utils.Utils;
import mm.dummymerchant.sk.merchantapp.db.DBHelper;
import mm.dummymerchant.sk.merchantapp.httpConnection.StoreOkContacts;
import mm.dummymerchant.sk.merchantapp.model.CityList_Model;
import mm.dummymerchant.sk.merchantapp.model.ConstactModel;
import mm.dummymerchant.sk.merchantapp.model.TownShip_Model;



public class SplashScreenActivity extends BaseActivity {


	private static int SPLASH_TIME_OUT =1000;
	DBHelper dbb = null;
	Utils utils;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getSupportActionBar().hide();
		/*if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
			window.setStatusBarColor(getResources().getColor(R.color.AppColor));
		}*/
		setContentView(R.layout.act_splash_screen);
		utils=new Utils();
		dbb=new	DBHelper(this);
		new AsyncTaskRunner().execute("");

		/**
		 * To Store ok Dollar Contacts
		 */
		new StoreOkContacts(this).execute("");



		final WifiManager wifi  = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		wifi.startScan();
	new Handler().postDelayed(new Runnable() {
		@Override
		public void run() {
			dbb.open();
			try {
				if (dbb.getJsonCityCount() == 0)
					insertCityInformation();
				if (dbb.getJsonTownshipCount() == 0)
					insertTownshipInfor();
			} catch (Exception e) {
				e.printStackTrace();
			}


			try {
				dbb.close();
			} catch (Exception e) {
				e.printStackTrace();
			}


			BaseActivity.mWifiResult = wifi.getScanResults();
			Intent i = new Intent(SplashScreenActivity.this, NewStartupActivity.class);
			startActivity(i);

			finish();
		}
	}, SPLASH_TIME_OUT);
	}

	@Override
	public <T> void response(int resultCode, T data) {

	}


	private void insertCityInformation() {
		try {
			ArrayList<CityList_Model> al_citymodel=new ArrayList<CityList_Model>();
			JSONArray array = new JsonParser().varifyArrayResponse(Utils.readTextFileFromAssets(this, "city_list_json.txt"));
			for (int i = 0; i < array.length(); i++)
			{
				DBHelper.insertCity(new CityList_Model(array.getJSONObject(i)));
				al_citymodel.add(new CityList_Model(array.getJSONObject(i)));
			}

		String jsonCityString=	utils.convertCityListArraylistToJsonString(al_citymodel);
			if(DBHelper.getJsonCity()==null)
			DBHelper.insertJsonCityModel(jsonCityString);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void insertTownshipInfor() {
		try {
			ArrayList<TownShip_Model> al_township=new ArrayList<TownShip_Model>();
			JSONArray array = new JsonParser().varifyArrayResponse(Utils.readTextFileFromAssets(this, "townshipJson.txt"));
			for (int i = 0; i < array.length(); i++)
			{
				DBHelper.insertTownShipCity(new TownShip_Model(array.getJSONObject(i)));
				al_township.add(new TownShip_Model(array.getJSONObject(i)));
			}
			String json_townlistString=	utils.convertTownListArraylistToJsonString(al_township);
			if(DBHelper.getJsonTownshipModel()==null)
			DBHelper.insertJsonTownShipModel(json_townlistString);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}



	private class AsyncTaskRunner extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {

			readForJsonContacts();

			return null;
		}

		@Override
		protected void onPostExecute(String result) {

		}

		@Override
		protected void onPreExecute() {
		}

		@Override
		protected void onProgressUpdate(String... text) {
		}
	}

	@Override
	public void onBackPressed() {

	}

	public void addSpeedKyatContactsInPhone() {
		Bitmap mBitMapIcon = BitmapFactory.decodeResource(getResources(), R.drawable.ok_icon);
		String val = Utils.CheckinContact(getApplicationContext(), SPEEDKYAT_NO_RECIEVING);
		if (val.equals("null")) {
			Utils.addNumberInContact(this, getResources().getString(R.string.speed_kyat), SPEEDKYAT_NO_RECIEVING, SPEEDKYAT_NO_SENDING, null, mBitMapIcon);
		}
		String val2 = Utils.CheckinContact(getApplicationContext(), OK_DOLLAR_CUSTOMER_CARE1);
		if (val2.equals("null")) {
			Utils.addNumberInContact(this, getResources().getString(R.string.ok_care), OK_DOLLAR_CUSTOMER_CARE2, OK_DOLLAR_CUSTOMER_CARE1, OK_DOLLAR_CUSTOMER_CARE3, mBitMapIcon);
		}
	}
}
