package mm.dummymerchant.sk.merchantapp;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;


import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.customView.CustomButton;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
import mm.dummymerchant.sk.merchantapp.db.DBHelper;
import mm.dummymerchant.sk.merchantapp.model.TransationModel;


/**
 * Created by Dell on 6/29/2016.
 */
public class TransactionSummary extends BaseActivity
{

    CustomTextView txt_tot,txt_no_of_trns;
    CustomButton edt_from,edt_to;


    static final int DATE_PICKER_ID_FROM = 1111;
    static final int DATE_PICKER_ID_TO = 2222;

    DatePickerDialog.OnDateSetListener myDateListener_from;
    DatePickerDialog.OnDateSetListener myDateListener_To;

    private int year;
    private int month;
    private int day;

    DBHelper db;
    ArrayList<TransationModel> al_trans;
    Date dt_frm=null;
    Date dt_to=null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.transaction_summary);

        init();

        final Calendar c = Calendar.getInstance();
        year  = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day   = c.get(Calendar.DAY_OF_MONTH);

        loadDates(day,month,year);

        callDBAndLoadList();


        myDateListener_from = new DatePickerDialog.OnDateSetListener() {


            @Override
            public void onDateSet(DatePicker arg0, int year, int month, int day) {

                Log.e("dt_frm","..."+dt_frm);
                Log.e("dt_to","..."+dt_to);

                    if(dt_frm!=null&&dt_to!=null)
                    {
                        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
                        Calendar newDate = Calendar.getInstance();
                        month = month + 1;
                        newDate.set(year, month, day);

                        String s_day = "";
                        String s_month = "";
                        String s_year = "";

                        if (month < 10)
                            s_month = "0" + month;
                        else
                            s_month=""+month;

                        if (day < 10)
                            s_day = "0" + day;
                        else
                            s_day=""+day;


                        s_year=""+year;
                        String s_date="" + s_day + "-" + s_month + "-" + s_year;

                        dt_frm=getDateFromString(s_date);
                        edt_from.setText(s_date);

                        if(dt_frm.before(dt_to))
                            getTotalAmountAndTransactions(dt_frm,dt_to);
                    }

                }


        };

        myDateListener_To=new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day)
            {

                    if(dt_frm!=null&&dt_to!=null)
                    {
                        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
                        Calendar newDate = Calendar.getInstance();
                        month = month + 1;
                        newDate.set(year, month, day);

                        String s_day = "";
                        String s_month = "";
                        String s_year = "";

                        if (month < 10)
                            s_month = "0" + month;
                        else
                        s_month=""+month;

                        if (day < 10)
                            s_day = "0" + day;
                        else
                        s_day=""+day;

                        s_year=""+year;

                        String s_date="" + s_day + "-" + s_month + "-" + s_year;
                        dt_to=getDateFromString(s_date);
                        edt_to.setText(s_date);

                        if(dt_to.after(dt_frm))
                            getTotalAmountAndTransactions(dt_frm,dt_to);
                    }

                }

        };

        edt_from.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog(DATE_PICKER_ID_FROM);
            }
        });

        edt_to.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog(DATE_PICKER_ID_TO);
            }
        });

        setTotal();
    }

    void init()
    {
        edt_from=(CustomButton)findViewById(R.id.edt_frm_date);
        edt_to=(CustomButton)findViewById(R.id.edt_to_date);
        txt_tot=(CustomTextView)findViewById(R.id.txt_total);
        txt_no_of_trns=(CustomTextView)findViewById(R.id.txt_noof_trns);
    }

    @Override
    public <T> void response(int resultCode, T data) {

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_PICKER_ID_FROM:

                // open datepicker dialog.
                // set date picker for current date
                // add pickerListener listner to date picker
                DatePickerDialog dialog1=new DatePickerDialog(this, myDateListener_from, year, month,day);
                /*String s_date="07-05-2016";
                Date dt=getDateFromString(s_date);
                dialog1.getDatePicker().setMinDate(dt.getTime());*/
                dialog1.getDatePicker().setMaxDate(new Date().getTime());
                return dialog1;

            case DATE_PICKER_ID_TO:
                DatePickerDialog dialog2=new DatePickerDialog(this, myDateListener_To, year, month,day);
                dialog2.getDatePicker().setMaxDate(new Date().getTime());
                return dialog2 ;
        }
        return null;
    }

    void callDBAndLoadList()
    {
        db=new DBHelper(this);
        db.open();

        if(AppPreference.getMasterOrCashier(this).equals(MASTER))
            al_trans=db.getTransationDetails(MASTER_TAG,this);
        else
            al_trans=db.getTransationDetails(SliderScreen.present_Cashier_Id,this);

        db.close();
    }

    public double getTotalofToday()
    {
        double total=0.0;
        int noOfTrnsactions=0;

        for(int i=0;i<al_trans.size();i++)
        {
            TransationModel model=al_trans.get(i);
            String amount=   model.getAmount();
            if(model.getDate()!=null) {
                if(!model.getDate().equalsIgnoreCase("")) {
                    Date dt = convertStringToDate(model.getDate());
                    if (checkCurrentDayorNot(dt)) {
                        try {
                            noOfTrnsactions++;
                            double amt = Double.parseDouble(amount);
                            total = total + amt;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            txt_no_of_trns.setText(""+noOfTrnsactions);

        }

        return total;
    }

    void setTotal()
    {
        txt_tot.setText(""+getTotalofToday());
    }

    public double getTotalAmountAndTransactions(Date dt_frml,Date dt_tol) {
        double total = 0.0;
        int noOfTrnsactions = 0;

        for (int i = 0; i < al_trans.size(); i++)
        {
            TransationModel model = al_trans.get(i);
            String amount = model.getAmount();
            if (model.getDate() != null)
            {
                if (!model.getDate().equalsIgnoreCase(""))
                {
                    Date dt = convertStringToDate(model.getDate());
                    Log.e("dt_frm1","..."+dt_frml);
                    Log.e("dt_to1","..."+dt_tol);
                    boolean check=dt.after(dt_frml);
                    Log.e("checkDate",""+check);

                    if (bothDatesAreSame(dt_frml, dt) || bothDatesAreSame(dt_tol, dt) || (dt.after(dt_frml) && dt.before(dt_tol)))
                    {
                        try {
                            Log.e("mDate","..."+model.getDate());
                            noOfTrnsactions++;
                            double amt = Double.parseDouble(amount);
                            total = total + amt;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

        }
        txt_no_of_trns.setText("" + noOfTrnsactions);
        txt_tot.setText(""+total);
        return total;
    }


    Date getDateFromString(String s_date)
    {
        Date d = null;
        DateFormat targetFormat = new SimpleDateFormat("dd-MM-yyyy");
        try {
            d = targetFormat.parse(s_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return d;
    }

    void loadDates(int day,int month,int year)
    {
        String s_day="";
        String s_month="";
        String s_year="";

        month=month+1;

        if(day<10)
            s_day="0"+day;
        else
        s_day=""+day;

        if(month<10)
            s_month="0"+month;
        else
        s_month=""+month;

        s_year=""+year;

        String s_date=s_day+"-"+s_month+"-"+s_year;

        Log.e("s_dat","..."+s_date);
       dt_frm= getDateFromString(s_date);
        dt_to= getDateFromString(s_date);
    }

}
