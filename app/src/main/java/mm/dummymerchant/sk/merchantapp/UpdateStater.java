package mm.dummymerchant.sk.merchantapp;

import android.app.DownloadManager;
import android.app.IntentService;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.os.Environment;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.Constant;
import mm.dummymerchant.sk.merchantapp.Utils.Utils;

/**
 * Created by Dell on 5/19/2016.
 */
public class UpdateStater  extends IntentService {

    public  String version;
    public  int current_version;
    private  String Url;
    private  String Type ;
    private static long downloadReference=0l;
    private DownloadManager downloadManager;
    private  boolean downloading=false;
    private  String status;
    private static int size=0;
    private  Thread first,second,third;
    HttpClient httpclient;


    public UpdateStater() {
        super("UpdateStater");
    }

    @Override
    protected void onHandleIntent(Intent intent) {


        first =  new Thread(new Runnable() {
            @Override
            public void run() {


                try {
                    PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                    current_version = pInfo.versionCode;
                }catch (Exception e)
                {
                    current_version=1;
                }
                after_method(CallerMethod(Constant.BASE_APP_URL + Constant.HTTP_URL_ValidateAppVersion + "?" + callLatestVersionDetails(AppPreference.getMyMobileNo(getApplicationContext()), Utils.simSerialNumber(getApplicationContext()), Utils.getSimSubscriberID(getApplicationContext()), "0", current_version,"1")));


            }
        });
        first.start();
    }


    public void after_method(String s)
    {

        if(s!=null)
        {
            try {
                JSONTokener tokener = new JSONTokener(s);
                JSONObject object = new JSONObject(tokener);
                if(object.getString("Code").equals("300"))
                {
                    String data1 = object.getString("Data");
                    JSONObject data = new JSONObject(new JSONTokener(data1));
                    JSONArray jsonArray = data.getJSONArray("MobileAppVersion");
                    JSONObject details = jsonArray.getJSONObject(0);
                    Url = details.getString("Url");
                    Type = details.getString("Type");
                    version = details.getString("LatestVersion").replace("\"", "");
                    sendBroadCastSMS1(Url,Type,version);
                    delete_Unessary();
                }else {
                    //  Utils.showToast(context,object.getString("Msg"));
                }
            }catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }



    private void sendBroadCastSMS1(String Url,String Type,String version) {
        try {
            URL url = new URL(Url);
            URLConnection urlConnection = url.openConnection();
            urlConnection.connect();
            size = urlConnection.getContentLength();
        } catch (Exception e) {

        }

        Intent broadcast = new Intent();
        broadcast.setAction(Constant.BROADCAST_INTENT_UPDATER);
        broadcast.putExtra("url", Url);
        broadcast.putExtra("type", Type);
        broadcast.putExtra("version", version);
        broadcast.putExtra("size", size);
        sendBroadcast(broadcast);
    }


    public String CallerMethod(String url)
    {

        httpclient = new DefaultHttpClient();
        HttpResponse response;
        String responseString = null;
        try {
            response = httpclient.execute(new HttpGet(url));
            Log.i("url version", url);
            StatusLine statusLine = response.getStatusLine();

            if(statusLine.getStatusCode() == HttpStatus.SC_OK){
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                response.getEntity().writeTo(out);
                responseString = out.toString();
                out.close();
            } else{
                //Closes the connection.
                response.getEntity().getContent().close();
                throw new IOException(statusLine.getReasonPhrase());
            }
        } catch (ClientProtocolException e) {
            //TODO Handle problems..
        } catch (IOException e) {
            e.printStackTrace();
        }
        return responseString;
    }


    public void delete_Unessary() {
        File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                String regexStr = "^[0-9]+$";
                if(children[i].toString().trim().contains(".apk")){
                    if (children[i].toString().trim().replace(".apk", "").replace("(", "").replace(")", "").replace(" ", "").replace("_", "").replace(".","").matches(regexStr)) {
                        if (!children[i].toString().trim().replace(".apk", "").replace("(", "").replace(")", "").replace(" ", "").replace("_", "").replace(".","").equals(String.valueOf(version))) {
                            new File(dir, children[i]).delete();
                        }
                    }
                }
            }
        }
    }

    public  String callLatestVersionDetails(String mobileNumber,String simid,String Msid,String OSType,int current_version,String AppType) {
        return Constant.MOBILE_NO+"="+mobileNumber+"&"+Constant.SIM_IDI+"="+simid+"&"+Constant.MSID+"="+Msid+"&"+Constant.OSType+"="+OSType+"&"+Constant.Version+"="+current_version+"&"+Constant.AppType+"="+AppType;
    }

}
