package mm.dummymerchant.sk.merchantapp.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.scottyab.aescrypt.AESCrypt;

import org.apache.http.Header;

import java.net.URLEncoder;
import java.security.GeneralSecurityException;
import java.util.Iterator;
import java.util.Set;

import mm.dummymerchant.sk.merchantapp.model.BalanceObject;

public class AppPreference {


    private static final String LANG = "lang";

    public static String APP_PREF_REGISTER = "speedkyat_reg";
    public static String APP_PREF = "com.jas.wallet";
    private static final String FIRST_TIME = "fist_time";
    private static final String APP_FIRST_TIME = "app_login";
    private static final String USER_NAME = "names";
    private static final String LOGIN_USER_NAME = "login";
    private static final String TOKEN = "token";
    private static final String OREDOO = "Ooredoo";
    private static final String TELENOR = "Telenor";
    private static final String MPT = "Mpt";
    private static final String MPTCDMA = "Mpt CDMA(800)";
    private static final String MECTEL = "MecTel";
    private static final String URL = "url";
    private static final String SIMID = "SIMID";
    private static final String MOBILE_NO = "MOBILENO";
    private static final String LASTTRANSACTION = "LASTTRANSACTION";
    private static final String BALANCE = "BALANCE";
    private static final String LOGIN_DATE = "LOGIN_DATE";
    private static final String LOGIN_ATTEMPT = "LOGIN_ATTEMPT";
    private static final String ENCRYPTED = "ENCRYPTED";

    private static final String TOPUP_TYPE ="TOPUP_TYPE";

    private static final String PASSWORD = "pas";
    private static final String APP_SHORT_CUT = "short_cut";
    private static final String DATE = "date";
    private static final String NUMBER = "number";
    private static final String Noti = "noti";
    private static final String RECEIVED_TRANSCATION = "received_transcation";
    private static final String NEWCONTACT = "newcontact";
    private static final String ACTIVITY_FINISH = "activity";
    private static final String URLS = "urls";
    private static final String GCM_REG_ID = "reg_id";
    private static final String INTRO_STATUS = "intro_status";
    private static final String ENCRYPTION_KEY = "encryption_key";
    private static final String ISREGISTER = "isregister";
    private static final String ISGCMREGISTER = "isGCMregister";
    private static final String SMS_PAYMENT_ACTIVE = "sms";
    private static final String ISCAR_PRESENT = "iscar";
    private static final String MORE_URL="more_url";
    private String SPEED_PWD_Key;
    private static AsyncHttpClient client;
    private static Context context_balance;
    // private AsyncHttpClient mAppserverClient = new AsyncHttpClient();
    private static final String TOPUP_SAME_NUMBER = "topupnumber";
    private static final String TOPUP_SAME_AMT = "topupamt";
    private static final String TOPUP_SAME_DURATION = "duration";
    private static final String BALANCE_LAST_UPDATED_DATE = "balanceDate";
    private static final String USER_TYPE = "usertype";
    private static final String USER_LEVEL_TYPE = "userleveltype";
    //Topup
    private static final String TOPUP_NUMBER_TEMP = "topupnumbertemp";
    private static final String TOPUP_AMT_TEMP = "topupamttemp";
    private static final String TOPUP_OPERATOR_TEMP = "topupoperatortemp";
    private static final String TOPUP_NAME_TEMP = "topupnametemp";
    private static final String TOPUP_OPERATOR_MERCHANT_NUMBER = "topupoperatormerchantnumber";
    private static final String TOPUP_OPERATOR_MERCHANT_COMMENT = "topupoperatormerchantcomment";
    private static final String INTERNET_STATE = "internet_state";
    private static final   String MASTER_MOBILENO="master_mobileno";
    private static final String MOBILE_NO_WITHOUT_COUNTRY_CODE = "MOBILENO_CC";
    private static final String USER_PROMOTION_SET = "promotion";
    private static final String TWO_MIN_DATE= "two_date";
    private static final String LOGIN_OFFLINE_FIRST = "LOGIN_OFFLINE_FIRST";
    private static final String SMS_SENDER = "sms_sender";



    public static String getLanguage(Context context) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        return mSettings.getString(LANG, "my");
    }

    public static String getSmsSender(Context context) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        return mSettings.getString(SMS_SENDER, "");
    }

    public static void setFlagFisrtOnlinePayment(Context context){
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putBoolean("IS_FIRST_ONLINE_PAYMENT", true);
        editor.commit();
    }

    public static boolean getIsFisrtOnlinePayment(Context context){
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        return mSettings.getBoolean("IS_FIRST_ONLINE_PAYMENT", false);
    }

    public static void setMyMobileNo(Context context, String check) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString(MOBILE_NO, check);
        editor.commit();
    }
    public static void setMoreUrl(Context context, boolean check) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putBoolean(MORE_URL, check);
        editor.commit();
    }

    public static boolean getMoreUrl(Context context) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        return mSettings.getBoolean(MORE_URL, false);
    }


    public static String getMyMobileNo(Context context) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        return mSettings.getString(MOBILE_NO, "");
    }

    public static void setPasword(Context context, String check) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mSettings.edit();
        String pswd = check;
        try {
            pswd = AESCrypt.encrypt(getPasswordKey(context), check);
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        }
        editor.putString(PASSWORD, pswd);
        editor.commit();
    }

    public static String getPasword(Context context) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        String pswd = mSettings.getString(PASSWORD, "");
        try {
            pswd = AESCrypt.decrypt(getPasswordKey(context), pswd);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return pswd;
    }



    public static void setDate(Context context, String check) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString(DATE, check);
        editor.commit();
    }

    public static String getDate(Context context) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        return mSettings.getString(DATE, "");
    }

    public static void setMasterMobileNo(Context context, String check) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString(MASTER_MOBILENO, check);
        editor.commit();
    }

    public static String getMasterMobileNo(Context context) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        return mSettings.getString(MASTER_MOBILENO, "");
    }


    public static void setPasswordKey(Context context, String key) {
        SharedPreferences mPwdKey = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mPwdKey.edit();

        editor.putString("SpeedPwdKey", key);
        editor.apply();
    }

    public static String getPasswordKey(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(APP_PREF, 0);
        return sharedPreferences.getString("SpeedPwdKey", "");
    }

    public static void setCountryCodeFlagName(Context context, String country, String code ) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString("COUNTRY", country);
        editor.putString("COUNTRY_CODE", code);
        editor.commit();
    }

    public static void setCountryImageID(Context context, int id) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putInt("FLAG_ID", id);
        editor.commit();
    }
    public static String getCountryCode(Context context) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        return mSettings.getString("COUNTRY_CODE", "+95");
    }

    public static String getCountryName(Context context) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        return mSettings.getString("COUNTRY", "Myanmar");
    }
    public static int getCountryImageID(Context context) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        return mSettings.getInt("FLAG_ID", -1);
    }

    public static void setAuthToken(Context context, String check) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString(TOKEN, check);
        editor.commit();
    }

    public static String getAuthTokem(Context context) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        return mSettings.getString(TOKEN, "");
    }

    public static void setIssRegister(Context context, boolean key) {
        SharedPreferences status = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = status.edit();
        editor.putBoolean(ISREGISTER, key);
        editor.apply();
    }





    public static void setVerifyNumber(Context context, String check) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mSettings.edit();

        editor.putString(NUMBER, check);
        editor.commit();
    }

    public static void setEncrptedKey(Context context, String check) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString(ENCRYPTED, check);
        editor.commit();
    }

    public static void setOTP(Context context, String check) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString("OTP", check);
        editor.commit();
    }

    public static String getOTP(Context context) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        return mSettings.getString("OTP", "");
    }

    public static void setVerifyNumberWithCountryCode(Context context, String check) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString("NUM_WITH_CODE", check);
        editor.commit();
    }

    public static void setIsAppFirstTime(Context context, boolean check) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putBoolean(APP_FIRST_TIME, check);
        editor.commit();
    }


    public static void setSimId(Context context, String check) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mSettings.edit();

        editor.putString(SIMID, check);
        editor.commit();
    }

    public static String getSimId(Context context) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        return mSettings.getString(SIMID, "");
    }






    public static void setLoginUserName(Context context, String check) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mSettings.edit();

        editor.putString(LOGIN_USER_NAME, check);
        editor.commit();
    }

    public static String getLoginuserName(Context context) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        return mSettings.getString(LOGIN_USER_NAME, "");
    }

    public static void setSmsPaymentActive(Context context, boolean check) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putBoolean(SMS_PAYMENT_ACTIVE, check);
        editor.commit();
    }

    public static boolean getSmsPaymentActive(Context context) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        return mSettings.getBoolean(SMS_PAYMENT_ACTIVE, false);
    }

    public static void setLoginTime(Context context, long check) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putLong(LOGIN_DATE, check);
        editor.commit();
    }

    public static Long getLoginTime(Context context) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        return mSettings.getLong(LOGIN_DATE, 0);
    }

    public static int getLoginAttempt(Context context) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        return mSettings.getInt(LOGIN_ATTEMPT, 0);
    }

    public static void setLoginAttempt(Context context, int check) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putInt(LOGIN_ATTEMPT, check);
        editor.commit();
    }

    public static void setBusinessName(Context context, String Name) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor prefsEditor = sharedPref.edit();
        prefsEditor.putString("BusinessName", Name);
        prefsEditor.apply();
    }

    public static String getBusinessName(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(APP_PREF, 0);
        return sharedPreferences.getString("BusinessName", "Unknown");
    }

    public static void setMerchantBLTname(Context context, String name) {
        SharedPreferences bltSettings = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = bltSettings.edit();
        editor.putString("MerchantBLTname", name);
        editor.commit();
    }

    public static String getMerchantBLTname(Context context) {
        SharedPreferences bltSettings = context.getSharedPreferences(APP_PREF, 0);
        return bltSettings.getString("MerchantBLTname", "");
    }

    public static void setBusinessMainCategory(Context context, String cat) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor prefsEditor = sharedPref.edit();
            prefsEditor.putString("BusinessMCategory", cat);
            prefsEditor.apply();

    }

    public static String getBusinessMainCategory(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(APP_PREF, 0);
        return sharedPreferences.getString("BusinessMCategory", "99");
    }

    public static void setRegsiterNumber(Context context, String check) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString("REG_NO", check);
        editor.apply();
    }

    public static String getRegisterNumber(Context context) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        return mSettings.getString("REG_NO", "");
    }

    public static String getBusinessSubCategory(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(APP_PREF, 0);
        return sharedPreferences.getString("BusinessSCategory", "00");
    }

    public static void setBusinessSubCategory(Context context, String cat) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor prefsEditor = sharedPref.edit();
        prefsEditor.putString("BusinessSCategory", cat);
        prefsEditor.apply();

    }

    public static void setOriginalBLTname(Context context, String name) {
        SharedPreferences BltName = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = BltName.edit();
        editor.putString("BLTName", name);
        editor.apply();
    }

    public static String getOriginalBLTname(Context context) {
        SharedPreferences BltName = context.getSharedPreferences(APP_PREF, 0);
        return BltName.getString("BLTName", getDeviceName());
    }

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }



    private static String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }



    public static boolean getOffNotificatino(Context context) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        return mSettings.getBoolean(Noti, false);
    }

    public static void setBalance(Context context, String check) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString(BALANCE, check);
        editor.commit();
    }

    public static void setRegistrationID(Context context, String regID) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString(GCM_REG_ID, regID);
        editor.commit();
    }

    public static void setALLUrl(Context context, String url) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString(URLS, url);
        editor.commit();
    }

    public static String getALLUrl(Context context) {
        SharedPreferences status = context.getSharedPreferences(APP_PREF, 0);
        return status.getString(URLS, "");
    }

    public static String getVerifyNumberWithCountryCode(Context context) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        return mSettings.getString("NUM_WITH_CODE", "");
    }

    public static void setGCMRegister(Context context, boolean key) {
        SharedPreferences status = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = status.edit();
        editor.putBoolean(ISGCMREGISTER, key);
        editor.apply();
    }

    public static void setEncryptionValue(Context context, String key) {
        SharedPreferences status = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = status.edit();
        editor.putString(ENCRYPTION_KEY, key);
        editor.apply();
    }

    public static String getEncryptionValue(Context context) {
        SharedPreferences status = context.getSharedPreferences(APP_PREF, 0);
        return status.getString(ENCRYPTION_KEY, "");
    }

    public static boolean getGCMRegister(Context context) {
        SharedPreferences status = context.getSharedPreferences(APP_PREF, 0);
        return status.getBoolean(ISGCMREGISTER, false);
    }

    public static boolean isOnByMerchant(Context context) {
        SharedPreferences bltSettings = context.getSharedPreferences(APP_PREF, 0);
        return bltSettings.getBoolean("BLTbyMerchant", false);
    }

    public static void setIsONbyMerchant(Context context, Boolean status) {
        SharedPreferences bltSettings = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = bltSettings.edit();
        editor.putBoolean("BLTbyMerchant", status);
        editor.commit();
    }








    public static void setMasterOrCashier(Context context,String value)
    {
        SharedPreferences mAutoPayment = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mAutoPayment.edit();
        editor.putString("checkMasterOrCashier", value);
        editor.apply();
    }

    public static String getMasterOrCashier(Context context)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences(APP_PREF, 0);
        return sharedPreferences.getString("checkMasterOrCashier","");
    }

    public static void setCashierMobileNo(Context context,String value)
    {
        SharedPreferences mAutoPayment = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mAutoPayment.edit();
        editor.putString("cashierno", value);
        editor.apply();
    }

    public static String getCashierNo(Context context)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences(APP_PREF, 0);
        return sharedPreferences.getString("cashierno","");
    }







    public static void setFinishActivity(Context context, boolean check) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putBoolean(ACTIVITY_FINISH, check);
        editor.commit();
    }



    public static String getVerifiedIMSINo(Context context){
        SharedPreferences bltSettings = context.getSharedPreferences(APP_PREF, 0);
        return bltSettings.getString("IMSI", "");
    }





    public static String getVerifiedSIMSerial(Context context){
        SharedPreferences bltSettings = context.getSharedPreferences(APP_PREF,0);
        return bltSettings.getString("SIM_SERIAL", "");
    }

    public static void setVerifiedSIMSerial(Context context, String simSerial){
        SharedPreferences bltSettings = context.getSharedPreferences(APP_PREF,0);
        SharedPreferences.Editor editor = bltSettings.edit();
        editor.putString("SIM_SERIAL", simSerial);
        editor.commit();
    }

    public static void setVerifiedIMSINo(Context context, String imsi){
        SharedPreferences bltSettings = context.getSharedPreferences(APP_PREF,0);
        SharedPreferences.Editor editor = bltSettings.edit();
        editor.putString("IMSI", imsi);
        editor.commit();
    }

    public static String getVerifyNumber(Context context) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        return mSettings.getString(NUMBER, "");
    }

    public static String getDefaultEmail(Context context){
        SharedPreferences bltSettings = context.getSharedPreferences(APP_PREF_REGISTER,0);
        return bltSettings.getString("DEFAULT_EMAIL", "");
    }

    public static void setFacebookID(Context context, String email){
        SharedPreferences bltSettings = context.getSharedPreferences(APP_PREF_REGISTER,0);
        SharedPreferences.Editor editor = bltSettings.edit();
        editor.putString("FACEBOOK_ID", email);
        editor.commit();
    }

    public static String getFacebookID(Context context){
        SharedPreferences bltSettings = context.getSharedPreferences(APP_PREF_REGISTER,0);
        return bltSettings.getString("FACEBOOK_ID", "");
    }

    public static void setDefaultEmail(Context context, String email){
        SharedPreferences bltSettings = context.getSharedPreferences(APP_PREF_REGISTER, 0);
        SharedPreferences.Editor editor = bltSettings.edit();
        editor.putString("DEFAULT_EMAIL", email);
        editor.commit();
    }


    public static void setLanguage(Context context, String check) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString(LANG, check);
        editor.commit();
    }

    public static void setSmsSender(Context context, String sender) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString(SMS_SENDER, sender);
        editor.commit();
    }


    public static void setMyMobileNoWithoutCountryCode(Context context, String mobileNoWithoutCC) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString(MOBILE_NO_WITHOUT_COUNTRY_CODE, mobileNoWithoutCC);
        editor.commit();
    }

    public static String getMyMobileNoWithoutCountryCode(Context context) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        return mSettings.getString(MOBILE_NO_WITHOUT_COUNTRY_CODE, "");
    }

    public static int getLastVersion(Context context) {
        SharedPreferences mCars = context.getSharedPreferences(APP_PREF, 0);
        return mCars.getInt("version", 1);
    }

    public static void setVersionLast(Context context, int version) {
        SharedPreferences mCars = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mCars.edit();
        editor.putInt("version", version);
        editor.apply();
    }

    public static void setInternetOn_at_login(Context context, boolean check) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putBoolean(INTERNET_STATE, check);
        editor.apply();
    }

    public static boolean getInternetOn_at_login(Context context) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        return mSettings.getBoolean(INTERNET_STATE, false);
    }



   public static void setMerchantStatus(Context context, Boolean status) {
       SharedPreferences mStatus = context.getSharedPreferences(APP_PREF, 0);
       SharedPreferences.Editor editor = mStatus.edit();
       editor.putBoolean("MerchantStatus", status);
       editor.apply();
   }

    public static void setCurrentShopID(Context context, String shopID){
        SharedPreferences mStatus = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mStatus.edit();
        editor.putString("CurrentShopID", shopID);
        editor.apply();
    }

    public static String getCurrentShopID(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(APP_PREF, 0);
        return sharedPreferences.getString("CurrentShopID", "XXXXXX");
    }

    public static Boolean getMerchantStatus(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(APP_PREF, 0);
        return sharedPreferences.getBoolean("MerchantStatus", false);
    }

    public static void setHotspotName(Context context, String name) {
        SharedPreferences mHotspot = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mHotspot.edit();
        editor.putString("MerchantHotspot", name);
        editor.apply();
    }

    public static String getHotspotName(Context context) {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        String name;
        if (model.startsWith(manufacturer)) {
            name = model;
        } else {
            name = manufacturer + " " + model;
        }
        SharedPreferences sharedPreferences = context.getSharedPreferences(APP_PREF, 0);
        return sharedPreferences.getString("MerchantHotspot", name);
    }

    public static Boolean getBLTStatus(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(APP_PREF, 0);
        return sharedPreferences.getBoolean("BltStatus", false);
    }

    public static void setBLTStatus(Context context, Boolean sts) {
        SharedPreferences mHotspot = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mHotspot.edit();
        editor.putBoolean("BltStatus", sts);
        editor.apply();

    }

    public static void setAutoPayment(Context context, Boolean sts) {
        SharedPreferences mAutoPayment = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mAutoPayment.edit();
        editor.putBoolean("AutoPayment", sts);
        editor.apply();
    }

    public static Boolean getAutoPayment(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(APP_PREF, 0);
        return sharedPreferences.getBoolean("AutoPayment", false);
    }

    public static void setCountryImageIDLogin(Context context, int id) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putInt("FLAG_ID_LOGIN", id);
        editor.apply();
    }

    public static int getCountryImageIDLogin(Context context) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        return mSettings.getInt("FLAG_ID_LOGIN", -1);
    }

    public static String getCountryNameLogin(Context context) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        return mSettings.getString("COUNTRY_LOGIN", "Myanmar");
    }


    public static String getPromotionId(Context context) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        return mSettings.getString(USER_PROMOTION_SET, "");
    }

    public static void setPromotionId(Context context, String check) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString(USER_PROMOTION_SET, check);
        editor.apply();
    }


    public static void setUserLevelType(Context context, String check) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString(USER_LEVEL_TYPE, check);
        editor.apply();
    }

    public static String getuserLevelType(Context context) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        return mSettings.getString(USER_LEVEL_TYPE, "");
    }
    public static void setIsLoginFirstTime(Context context, boolean isFirst) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putBoolean("IS_LOGIN_FIRST", isFirst);
        editor.commit();
    }
    public static boolean getIsLoginFirstTime(Context context) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        return mSettings.getBoolean("IS_LOGIN_FIRST", true);
    }

    public static void setUserType(Context context, String check) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString(USER_TYPE, check);
        editor.apply();
    }

    public static String getUserType(Context context) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        return mSettings.getString(USER_TYPE, "");
    }

    public static void setAgentAuthCode(Context context, String code){
        SharedPreferences preferences = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("AGENT_AUTH", code);
        editor.apply();
    }

    public static String getAgentAuthCode(Context context){
        SharedPreferences preferences = context.getSharedPreferences(APP_PREF,0);
        return preferences.getString("AGENT_AUTH", "");
    }

    public static void setAgentAuthCodeStatus(Context context, boolean status){
        SharedPreferences preferences = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("AGENT_AUTH_STATUS", status);
        editor.apply();
    }

    public static boolean getAgentAuthCodeStatus(Context context){
        SharedPreferences preferences = context.getSharedPreferences(APP_PREF,0);
        return preferences.getBoolean("AGENT_AUTH_STATUS", false);
    }


    public static void setCountryCodeFlagNameLogin(Context context, String country, String code ) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString("COUNTRY_LOGIN", country);
        editor.putString("COUNTRY_CODE_LOGIN", code);
        editor.apply();
    }

    public static String getCountryCodeLogin(Context context) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        return mSettings.getString("COUNTRY_CODE_LOGIN", "+95");
    }

    public static void setTwoMin_Login_Date(Context context, long check) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putLong(TWO_MIN_DATE, check);
        editor.apply();
    }



    public static long getTwoMin_login_Date(Context context) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        return mSettings.getLong(TWO_MIN_DATE, 0);
    }


    public static void setOffEmailNotification(Context context, boolean check) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putBoolean("EmailNotification", check);
        editor.apply();
    }

    public static boolean getOffEmailNotification(Context context) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        return mSettings.getBoolean("EmailNotification", false);
    }

    public static void setOffSMSNotification(Context context, boolean check) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putBoolean("SMSNotification", check);
        editor.apply();
    }

    public static boolean getOffSMSNotificatino(Context context) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        return mSettings.getBoolean("SMSNotification", false);
    }

    public static void setOffNotofication(Context context, boolean check) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putBoolean(Noti, check);
        editor.apply();
    }
    public static void setIsPayments_alertsShow(Context context, boolean status){
        SharedPreferences preferences=context.getSharedPreferences(APP_PREF,0);
        SharedPreferences.Editor editor= preferences.edit();
        editor.putBoolean("payments_alerts",status);
        editor.apply();
    }

    public static boolean getIsPayments_alertsShow(Context context){
        SharedPreferences  preferences= context.getSharedPreferences(APP_PREF,0);
        return preferences.getBoolean("payments_alerts",false);
    }

    public static void setOriginalWifiName(Context context, String name){
        SharedPreferences mHotspot = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mHotspot.edit();
        editor.putString("MerchantOriginalHotspot", name);
        editor.apply();
    }

    public static String getOriginalWifiName(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(APP_PREF, 0);
        return sharedPreferences.getString("MerchantOriginalHotspot", getDeviceName());
    }

    public static void setLanguageAlert(Context context, Boolean status){
        SharedPreferences preferences = context.getSharedPreferences(APP_PREF,0);
        SharedPreferences.Editor editor =preferences.edit();
        editor.putBoolean("MyAlert", status);
        editor.apply();
    }

    public static boolean getLanguageAlert(Context context){
        SharedPreferences preferences = context.getSharedPreferences(APP_PREF,0);
        return  preferences.getBoolean("MyAlert", false);
    }

    public static void setNewContact(Context context, String check) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString(NEWCONTACT, check);
        editor.apply();
    }

    public static String getNewContact(Context context) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        return mSettings.getString(NEWCONTACT, "");
    }

    public static void setOfflineLogged(Context context,int check) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putInt(LOGIN_OFFLINE_FIRST, check);
        editor.apply();
    }

    public static int getOfflineLogged(Context context) {
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        return mSettings.getInt(LOGIN_OFFLINE_FIRST, 0);
    }


    public static String getBalance(Context context)

    {
        client = new AsyncHttpClient();

        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);

        if (mSettings.getString(BALANCE, "") != null) {
            if (!mSettings.getString(BALANCE, "").equals("")) {
                return mSettings.getString(BALANCE, "");


            } else {
                CallBalanceAPI(AppPreference.getPasword(context), context);
                return "0";

            }


        } else {

            CallBalanceAPI(AppPreference.getPasword(context), context);
            return "0";

        }
    }


    public static void CallBalanceAPI(String mPin, Context context) {

        Bundle params = new Bundle();
        params.putString(Constant.MPI, mPin);
        params.putString(Constant.AGENT_CODE, AppPreference.getMyMobileNo(context));
        params.putString(Constant.DESTINATION, AppPreference.getMyMobileNo(context));
        params.putString(Constant.PARAM_TOKEN, AppPreference.getAuthTokem(context));
        get(Constant.BASE_URL + ";" + Constant.URL_BALANCE, params);
        context_balance = context;

    }

    public static void get(String url, Bundle params) {


        //  if(isInternetOn(context_balance))

        //  {


        try {

            params.putString(Constant.PARAM_CLIENT_TYPE, Constant.PARAM_CLIENT_TYPE_VALUE);
            params.putString(Constant.VENDOR_CODE, Constant.VENDOR_CODE_VALUE);
            params.putString(Constant.CLIENT_IP, Utils.getLocalIpAddress());
            params.putString(Constant.CLIENT_ADDRESS, "Android=" + Build.DEVICE);
            String paramsString = "";
            boolean first = false;
            Set<String> parameters = params.keySet();
            Iterator<String> it = parameters.iterator();

            while (it.hasNext()) {

                String key = it.next().trim();

                if (params.containsKey(key)) {

                    if (params.containsKey(key)) {

                        if (!first) {
                            paramsString = paramsString + key + "=" + URLEncoder.encode(params.getString(key));
                            first = true;
                        } else {
                            paramsString = paramsString + ";" + key + "=" + URLEncoder.encode(params.getString(key));
                        }

                    }
                }
            }

            url = url + ";" + paramsString;
            client.setTimeout(10000);
            client.get(url, httpHandler);


        } catch (Exception e) {
            e.printStackTrace();
        }


        //else
        //  {

        // showMessage("No Internet Available..!");

    }

    public static void deleteAppPrefrence(Context context){
        SharedPreferences mSettings = context.getSharedPreferences(APP_PREF, 0);
        mSettings.edit().clear().apply();
    }

    public static AsyncHttpResponseHandler httpHandler = new AsyncHttpResponseHandler() {

        @Override
        public void onCancel() {// cancel the Request
            super.onCancel();
        }

        @Override
        public void onStart() {// Start the Progress
            super.onStart();


        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {// validate the response

            try {


                String response = new String(responseBody, "UTF-8");

                if (response.contains("<resultcode>0</resultcode>")) {


                    BalanceObject balobj = new BalanceObject(response);
                    setBalance(context_balance, balobj.getAmount());


                }


            } catch (Exception e) {
                e.printStackTrace();
            }


        }

        @Override
        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) { // dismiss the dialog


        }
    };



}
