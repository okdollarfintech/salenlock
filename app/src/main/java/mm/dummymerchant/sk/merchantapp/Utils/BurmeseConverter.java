package mm.dummymerchant.sk.merchantapp.Utils;

/**
 * Created by abhishekmodi on 18/08/17.
 */

import android.annotation.SuppressLint;
import android.content.Context;
import android.media.MediaPlayer;
import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import mm.dummymerchant.sk.merchantapp.R;


public class BurmeseConverter {

    private ArrayList<String> marrayList, mArylistDecimal;
    private ArrayList<String> arrayList2, aryLstDcml2;
    private Long number;
    private Long number2;
    private Long digit, digit2, count, count2;
    private String final_string;
    private String final_string_decimal;
    private Context mActivity;

    private RepeatSound repeatSoundlistener = null;
    private IEnglishToBurmeseString iEnglishToBurmeseString;
    private MediaPlayerThread mediaPlayerThread = null;

    private boolean isPlay = true;
    private String cico_type = "";
    private String go_online_voice = "";

    public BurmeseConverter(IEnglishToBurmeseString iEnglishToBurmeseString, String amount, Context mContext) {
        isPlay = true;
        this.mActivity = mContext;
        this.iEnglishToBurmeseString = iEnglishToBurmeseString;
        initRepeatSound(iEnglishToBurmeseString);
    }

    private void initRepeatSound(IEnglishToBurmeseString iEnglishToBurmeseString) {
        try {
            this.repeatSoundlistener = (RepeatSound) iEnglishToBurmeseString;
        } catch (Exception ignored) {
        }
    }

    private void init() {
        arrayList2 = new ArrayList<>();
        marrayList = new ArrayList<>();
        mArylistDecimal = new ArrayList<>();
        aryLstDcml2 = new ArrayList<>();
        final_string = "";
        final_string_decimal = "";
        number = 0L;
        number2 = 0L;
        digit = 0L;
        digit2 = 0L;
        count = 0L;
        count2 = 0L;
    }

    public void burmeseAmountSoundStart(String cico_type, String amount, String go_online_voice) {
        this.cico_type = cico_type;
        this.go_online_voice = go_online_voice;
        validationStart(amount);
    }

    public void validationStart(String amount) {
        isPlay = true;
        init();
        if (!resetPlayer(amount))
            return;
        String inputWithpoint = "";
        String afterDecimal = "", beforeDecimal = "";
        String playString = "";
        if (amount.length() != 0) {
            try {
                inputWithpoint = amount;

                int position = inputWithpoint.indexOf(".");

                if (position == 0) {
                    inputWithpoint = inputWithpoint.replace(".", "");
                }

                if (inputWithpoint.contains(".")) {
                    String[] parts = inputWithpoint.split("\\.");
                    long part1 = 0L;
                    long part2 = 0L;
                    try {
                        part1 = Long.parseLong(parts[0]);
                    } catch (NumberFormatException ignored) {

                    }
                    try {
                        part2 = Long.parseLong(parts[1]);
                    } catch (NumberFormatException ignored) {
                    }

                    if (part1 == 0 && part2 == 0)
                        return;

                    if (part1 != 0) {
                        beforeDecimal = callFunction(parts[0]);
                        playString = beforeDecimal + " က်ပ္ ";
                    }

                    if (part2 != 0) {
                        afterDecimal = callFunctionForDecimal(parts[1]);
                        if (part1 == 0) {
                            playString = playString + afterDecimal + " ျပား";
                        } else {
                            playString = beforeDecimal + " က်ပ္ " + afterDecimal + " ျပား";
                        }
                    }
                    etbCallback(playString);
                    mediaPlay(playString, amount);
                } else {
                    String normal = callFunction(inputWithpoint) + " က်ပ္";
                    etbCallback(normal);
                    mediaPlay(normal, amount);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void mediaPlay(String normal, String amount) {
        boolean canPlay = false;

        if (!cico_type.isEmpty()) {
            if (!go_online_voice.isEmpty()) {
                cico_type = cico_type.replace("received", "");
                normal = cico_type + " " + normal + " " + go_online_voice;
            } else {
                normal = cico_type + " " + normal /*+ " " + go_online_voice*/;
            }
        }


        canPlay = true;

        if (canPlay) {
            mediaPlayerThread = new MediaPlayerThread(normal, amount);
            mediaPlayerThread.execute();
        }
    }

    private void etbCallback(String amtString) {
        if (null != iEnglishToBurmeseString)
            iEnglishToBurmeseString.englishToBurmeseString(amtString);
    }

    public boolean resetPlayer(String amounts) {
        boolean needPlay = false;
        if (mediaPlayerThread != null) {
            try {
                if (amounts.isEmpty()) {
                    mediaPlayerThread.cancel(true);
                    mediaPlayerThread.amount = "";
                    needPlay = false;
                    return needPlay;
                }
                if (!mediaPlayerThread.amount.equalsIgnoreCase(amounts)) {
                    mediaPlayerThread.amount = "";
                    mediaPlayerThread.cancel(true);
                    needPlay = true;
                }
            } catch (Exception ignored) {
            }
        } else
            needPlay = true;

        return needPlay;
    }


    private String callFunction(String input) throws InterruptedException {
        number = Long.parseLong(input);
        if (number == 1000) {
            final_string = "တစ္" + " ေထာင္";

        } else if (number == 2000) {
            final_string = "ႏွစ္" + " ေထာင္";

        } else if (number == 3000) {
            final_string = "သံုး" + " ေထာင္";

        } else if (number == 4000) {
            final_string = "ေလး" + " ေထာင္";

        } else if (number == 5000) {
            final_string = "ငါး" + " ေထာင္";

        } else if (number == 6000) {
            final_string = "ေျခာက္" + " ေထာင္";

        } else if (number == 7000) {
            final_string = "ခုႏွစ္" + " ေထာင္";

        } else if (number == 8000) {
            final_string = "ရွစ္" + " ေထာင္";

        } else if (number == 9000) {
            final_string = "ကိုး" + " ေထာင္";

        } else {

            do {
                digit = number % 10;

                if (digit == 0) {
                    marrayList.add("zero");
                } else if (digit == 1) {
                    marrayList.add("တစ္");
                } else if (digit == 2) {
                    marrayList.add("ႏွစ္");
                } else if (digit == 3) {
                    marrayList.add("သံုး");
                } else if (digit == 4) {
                    marrayList.add("ေလး");
                } else if (digit == 5) {
                    marrayList.add("ငါး");
                } else if (digit == 6) {
                    marrayList.add("ေျခာက္");
                } else if (digit == 7) {
                    marrayList.add("ခုႏွစ္");
                } else if (digit == 8) {
                    marrayList.add("ရွစ္");
                } else if (digit == 9) {
                    marrayList.add("ကိုး");
                }
                number = number / 10;
            } while (number != 0);
            count = Long.valueOf(marrayList.size());
            Collections.reverse(marrayList);

            if (marrayList.size() > 5) {

                int count = marrayList.size();
                for (int i = 0; i <= count - 6; i++) {

                    arrayList2.add(marrayList.get(0));
                    marrayList.remove(0);
                }

                final_string = printDigit(arrayList2, true);
                final_string = printDigit(marrayList, false);
            } else {
                final_string = printDigit(marrayList, false);

            }
        }

        return final_string;
    }


    /* ***********  for decimal  *********************/

    private String callFunctionForDecimal(String input) throws InterruptedException {


     /*   number = Integer.parseInt(input);
        char a =input.charAt(input.length() - 1);
        char b =input.charAt(input.length() - 2);
        char c =input.charAt(input.length() - 3);*/

        number2 = Long.parseLong(input);
        if (number2 == 1000) {
            final_string_decimal = "တစ္" + " ေထာင္";


        } else if (number2 == 2000) {
            final_string_decimal = "ႏွစ္" + " ေထာင္";

        } else if (number2 == 3000) {
            final_string_decimal = "သံုး" + " ေထာင္";

        } else if (number2 == 4000) {
            final_string_decimal = "ေလး" + " ေထာင္";

        } else if (number2 == 5000) {
            final_string_decimal = "ငါး" + " ေထာင္";

        } else if (number2 == 6000) {
            final_string_decimal = "ေျခာက္" + " ေထာင္";

        } else if (number2 == 7000) {
            final_string_decimal = "ခုႏွစ္" + " ေထာင္";

        } else if (number2 == 8000) {
            final_string_decimal = "ရွစ္" + " ေထာင္";

        } else if (number2 == 9000) {
            final_string_decimal = "ကိုး" + " ေထာင္";

        } else {

            do {
                digit2 = number2 % 10;

                if (digit2 == 0) {
                    mArylistDecimal.add("zero");
                } else if (digit2 == 1) {
                    mArylistDecimal.add("တစ္");
                } else if (digit2 == 2) {
                    mArylistDecimal.add("ႏွစ္");
                } else if (digit2 == 3) {
                    mArylistDecimal.add("သံုး");
                } else if (digit2 == 4) {
                    mArylistDecimal.add("ေလး");
                } else if (digit2 == 5) {
                    mArylistDecimal.add("ငါး");
                } else if (digit2 == 6) {
                    mArylistDecimal.add("ေျခာက္");
                } else if (digit2 == 7) {
                    mArylistDecimal.add("ခုႏွစ္");
                } else if (digit2 == 8) {
                    mArylistDecimal.add("ရွစ္");
                } else if (digit2 == 9) {
                    mArylistDecimal.add("ကိုး");
                }
                number2 = number2 / 10;
            } while (number2 != 0);
            count2 = Long.valueOf(marrayList.size());
            Collections.reverse(mArylistDecimal);

            if (mArylistDecimal.size() > 5) {

                int count = mArylistDecimal.size();
                for (int i = 0; i <= count - 6; i++) {

                    aryLstDcml2.add(mArylistDecimal.get(0));
                    mArylistDecimal.remove(0);
                }

                final_string_decimal = printDigitForDecimal(aryLstDcml2, true);
                final_string_decimal = printDigitForDecimal(mArylistDecimal, false);
            } else {
                final_string_decimal = printDigitForDecimal(mArylistDecimal, false);

            }
        }


        return final_string_decimal;
    }

    /************ for without decimal digit ******************/

    private String printDigit(ArrayList<String> arrayList, boolean strLakhBool) throws InterruptedException {

        if (arrayList.size() == 5) {
            for (int i = 0; i < arrayList.size(); i++) {

                if (i == 0) {

                    if (arrayList.get(i) == "zero") {

                    } else {

                        final_string = final_string + arrayList.get(i);
                        final_string = final_string + " ";
                        final_string = final_string + "ေသာင္း";
                        final_string = final_string + " ";
                    }

                } else if (i == 1) {

                    if (arrayList.get(i) == "zero") {

                    } else {

                        if (arrayList.get(2).equals("zero") && arrayList.get(3).equals("zero") && arrayList.get(4).equals("zero")) {

                            final_string = final_string + arrayList.get(i);
                            final_string = final_string + " ";
                            final_string = final_string + "ေထာင္";
                            final_string = final_string + " ";
                        } else {
                            final_string = final_string + arrayList.get(i);
                            final_string = final_string + " ";
                            final_string = final_string + "ေထာင့္";
                            final_string = final_string + " ";
                        }

                    }
                } else if (i == 2) {
                    if (arrayList.get(i) == "zero") {


                    } else {

                        final_string = final_string + arrayList.get(i);
                        final_string = final_string + " ";
                        final_string = final_string + "ရာ";
                        final_string = final_string + " ";
                    }

                } else if (i == 3) {
                    if (arrayList.get(i) == "zero") {


                    } else {
                        if (arrayList.get(arrayList.size() - 1) == "zero") {
                            final_string = final_string + arrayList.get(i);
                            final_string = final_string + " ";
                            final_string = final_string + "ဆယ္";
                            final_string = final_string + " ";
                        } else {
                            final_string = final_string + arrayList.get(i);
                            final_string = final_string + " ";
                            final_string = final_string + "ဆယ့္";
                            final_string = final_string + " ";
                        }

                    }

                } else if (i == 4) {
                    if (arrayList.get(i) == "zero") {


                    } else {

                        final_string = final_string + arrayList.get(i);

                    }
                }

            }

            if (final_string.contains("null")) {
                final_string = final_string.replace("null", "");


            } else {
                final_string = final_string.replace("", "");


            }


        } else if (arrayList.size() == 4) {

            for (int i = 0; i < arrayList.size(); i++) {

                if (i == 0) {
                    if (arrayList.get(i) == "zero") {

                    } else {

                        final_string = final_string + arrayList.get(i);
                        final_string = final_string + " ";
                        final_string = final_string + "ေထာင့္";
                        final_string = final_string + " ";
                    }

                } else if (i == 1) {

                    if (arrayList.get(i) == "zero") {

                    } else {

                        final_string = final_string + arrayList.get(i);
                        final_string = final_string + " ";
                        final_string = final_string + "ရာ";
                        final_string = final_string + " ";
                    }
                } else if (i == 2) {
                    if (arrayList.get(i) == "zero") {


                    } else {

                        if (arrayList.get(arrayList.size() - 1) == "zero") {
                            final_string = final_string + arrayList.get(i);
                            final_string = final_string + " ";
                            final_string = final_string + "ဆယ္";
                            final_string = final_string + " ";
                        } else {
                            final_string = final_string + arrayList.get(i);
                            final_string = final_string + " ";
                            final_string = final_string + "ဆယ့္";
                            final_string = final_string + " ";
                        }
                    }

                } else if (i == 3) {
                    if (arrayList.get(i) == "zero") {


                    } else {

                        final_string = final_string + arrayList.get(i);

                    }
                }

            }
            if (final_string.contains("null")) {
                final_string = final_string.replace("null", "");


            } else {
                final_string = final_string.replace("", "");

            }

        } else if (arrayList.size() == 3) {
            for (int i = 0; i < arrayList.size(); i++) {

                if (i == 0) {
                    if (arrayList.get(i) == "zero") {

                    } else {

                        final_string = final_string + arrayList.get(i);
                        final_string = final_string + " ";
                        final_string = final_string + "ရာ";
                        final_string = final_string + " ";
                    }

                } else if (i == 1) {

                    if (arrayList.get(i) == "zero") {

                    } else {
                        if (arrayList.get(arrayList.size() - 1) == "zero") {
                            final_string = final_string + arrayList.get(i);
                            final_string = final_string + " ";
                            final_string = final_string + "ဆယ္";
                            final_string = final_string + " ";
                        } else {
                            final_string = final_string + arrayList.get(i);
                            final_string = final_string + " ";
                            final_string = final_string + "ဆယ့္";
                            final_string = final_string + " ";
                        }
                    }
                } else if (i == 2) {
                    if (arrayList.get(i) == "zero") {

                    } else {
                        final_string = final_string + arrayList.get(i);
                    }
                }
            }
            if (final_string.contains("null")) {
                final_string = final_string.replace("null", "");

            } else {
                final_string = final_string.replace("", "");
            }

        } else if (arrayList.size() == 2) {

            for (int i = 0; i < arrayList.size(); i++) {

                if (i == 0) {
                    if (arrayList.get(i) == "zero") {

                    } else {
                        if (arrayList.get(arrayList.size() - 1) == "zero") {
                            final_string = final_string + arrayList.get(i);
                            final_string = final_string + " ";
                            final_string = final_string + "ဆယ္";
                            final_string = final_string + " ";
                        } else {
                            final_string = final_string + arrayList.get(i);
                            final_string = final_string + " ";
                            final_string = final_string + "ဆယ့္";
                            final_string = final_string + " ";
                        }
                    }

                } else if (i == 1) {
                    if (arrayList.get(i) == "zero") {
                    } else {
                        final_string = final_string + arrayList.get(i);
                    }
                }
            }
            if (final_string.contains("null")) {
                final_string = final_string.replace("null", "");

            } else {
                final_string = final_string.replace("", "");
            }

        } else if (arrayList.size() == 1) {

            if (arrayList.get(0) == "zero") {
                final_string = " သုည ";
            } else {
                final_string = final_string + arrayList.get(0);
                final_string = final_string + " ";
                final_string = final_string + " ";
            }

            if (final_string.contains("null")) {
                final_string = final_string.replace("null", "");

            } else {
                final_string = final_string.replace("", "");
            }
        }

        if (strLakhBool) {
            final_string = final_string + " သိန္း ";///////////////////////

        }

        return final_string;
    }

    /************ for decimal digit ******************/

    private String printDigitForDecimal(ArrayList<String> arrayList, boolean strLakhBool) throws InterruptedException {

        if (arrayList.size() == 5) {
            for (int i = 0; i < arrayList.size(); i++) {

                if (i == 0) {

                    if (arrayList.get(i) == "zero") {

                    } else {

                        final_string_decimal = final_string_decimal + arrayList.get(i);
                        final_string_decimal = final_string_decimal + " ";
                        final_string_decimal = final_string_decimal + "ေသာင္း";
                        final_string_decimal = final_string_decimal + " ";
                    }

                } else if (i == 1) {

                    if (arrayList.get(i) == "zero") {

                    } else {
                        final_string_decimal = final_string_decimal + arrayList.get(i);
                        final_string_decimal = final_string_decimal + " ";
                        final_string_decimal = final_string_decimal + "ေထာင့္";
                        final_string_decimal = final_string_decimal + " ";
                    }
                } else if (i == 2) {
                    if (arrayList.get(i) == "zero") {
                    } else {
                        final_string_decimal = final_string_decimal + arrayList.get(i);
                        final_string_decimal = final_string_decimal + " ";
                        final_string_decimal = final_string_decimal + "ရာ";
                        final_string_decimal = final_string_decimal + " ";
                    }

                } else if (i == 3) {
                    if (arrayList.get(i) == "zero") {
                    } else {
                        if (arrayList.get(arrayList.size() - 1) == "zero") {
                            final_string_decimal = final_string_decimal + arrayList.get(i);
                            final_string_decimal = final_string_decimal + " ";
                            final_string_decimal = final_string_decimal + "ဆယ္";
                            final_string_decimal = final_string_decimal + " ";
                        } else {
                            final_string_decimal = final_string_decimal + arrayList.get(i);
                            final_string_decimal = final_string_decimal + " ";
                            final_string_decimal = final_string_decimal + "ဆယ့္";
                            final_string_decimal = final_string_decimal + " ";
                        }
                    }
                } else if (i == 4) {
                    if (arrayList.get(i) == "zero") {
                    } else {
                        final_string_decimal = final_string_decimal + arrayList.get(i);
                    }
                }

            }

            if (final_string_decimal.contains("null")) {
                final_string_decimal = final_string_decimal.replace("null", "");
            } else {
                final_string_decimal = final_string_decimal.replace("", "");
            }
        } else if (arrayList.size() == 4) {

            for (int i = 0; i < arrayList.size(); i++) {

                if (i == 0) {
                    if (arrayList.get(i) == "zero") {

                    } else {

                        final_string_decimal = final_string_decimal + arrayList.get(i);
                        final_string_decimal = final_string_decimal + " ";
                        final_string_decimal = final_string_decimal + "ေထာင့္";
                        final_string_decimal = final_string_decimal + " ";
                    }

                } else if (i == 1) {

                    if (arrayList.get(i) == "zero") {

                    } else {

                        final_string_decimal = final_string_decimal + arrayList.get(i);
                        final_string_decimal = final_string_decimal + " ";
                        final_string_decimal = final_string_decimal + "ရာ";
                        final_string_decimal = final_string_decimal + " ";
                    }
                } else if (i == 2) {
                    if (arrayList.get(i) == "zero") {


                    } else {

                        if (arrayList.get(arrayList.size() - 1) == "zero") {
                            final_string_decimal = final_string_decimal + arrayList.get(i);
                            final_string_decimal = final_string_decimal + " ";
                            final_string_decimal = final_string_decimal + "ဆယ္";
                            final_string_decimal = final_string_decimal + " ";
                        } else {
                            final_string_decimal = final_string_decimal + arrayList.get(i);
                            final_string_decimal = final_string_decimal + " ";
                            final_string_decimal = final_string_decimal + "ဆယ့္";
                            final_string_decimal = final_string_decimal + " ";
                        }
                    }

                } else if (i == 3) {
                    if (arrayList.get(i) == "zero") {


                    } else {

                        final_string_decimal = final_string_decimal + arrayList.get(i);

                    }
                }

            }
            if (final_string_decimal.contains("null")) {
                final_string_decimal = final_string_decimal.replace("null", "");


            } else {
                final_string_decimal = final_string_decimal.replace("", "");


            }

        } else if (arrayList.size() == 3) {
            for (int i = 0; i < arrayList.size(); i++) {

                if (i == 0) {
                    if (arrayList.get(i) == "zero") {

                    } else {

                        final_string_decimal = final_string_decimal + arrayList.get(i);
                        final_string_decimal = final_string_decimal + " ";
                        final_string_decimal = final_string_decimal + "ရာ";
                        final_string_decimal = final_string_decimal + " ";
                    }

                } else if (i == 1) {

                    if (arrayList.get(i) == "zero") {

                    } else {
                        if (arrayList.get(arrayList.size() - 1) == "zero") {
                            final_string_decimal = final_string_decimal + arrayList.get(i);
                            final_string_decimal = final_string_decimal + " ";
                            final_string_decimal = final_string_decimal + "ဆယ္";
                            final_string_decimal = final_string_decimal + " ";
                        } else {
                            final_string_decimal = final_string_decimal + arrayList.get(i);
                            final_string_decimal = final_string_decimal + " ";
                            final_string_decimal = final_string_decimal + "ဆယ့္";
                            final_string_decimal = final_string_decimal + " ";
                        }
                    }
                } else if (i == 2) {
                    if (arrayList.get(i) == "zero") {


                    } else {

                        final_string_decimal = final_string_decimal + arrayList.get(i);

                    }
                }

            }
            if (final_string_decimal.contains("null")) {
                final_string_decimal = final_string_decimal.replace("null", "");

            } else {
                final_string_decimal = final_string_decimal.replace("", "");

            }

        } else if (arrayList.size() == 2) {

            for (int i = 0; i < arrayList.size(); i++) {

                if (i == 0) {
                    if (arrayList.get(i) == "zero") {

                    } else {

                        if (arrayList.get(arrayList.size() - 1) == "zero") {
                            final_string_decimal = final_string_decimal + arrayList.get(i);
                            final_string_decimal = final_string_decimal + " ";
                            final_string_decimal = final_string_decimal + "ဆယ္";
                            final_string_decimal = final_string_decimal + " ";
                        } else {
                            final_string_decimal = final_string_decimal + arrayList.get(i);
                            final_string_decimal = final_string_decimal + " ";
                            final_string_decimal = final_string_decimal + "ဆယ့္";
                            final_string_decimal = final_string_decimal + " ";
                        }
                    }

                } else if (i == 1) {
                    if (arrayList.get(i) == "zero") {


                    } else {

                        final_string_decimal = final_string_decimal + arrayList.get(i);

                    }
                }

            }
            if (final_string_decimal.contains("null")) {
                final_string_decimal = final_string_decimal.replace("null", "");

            } else {
                final_string_decimal = final_string_decimal.replace("", "");

            }

        } else if (arrayList.size() == 1) {

            if (arrayList.get(0) == "zero") {
                final_string_decimal = final_string_decimal + " ";
            } else {

                final_string_decimal = final_string_decimal + arrayList.get(0);
                final_string_decimal = final_string_decimal + " ";
                final_string_decimal = final_string_decimal + " ";
            }

            if (final_string_decimal.contains("null")) {
                final_string_decimal = final_string_decimal.replace("null", "");

            } else {
                final_string_decimal = final_string_decimal.replace("", "");

            }

        }


        if (strLakhBool) {
            final_string_decimal = final_string_decimal + " သိန္း ";///////////////////////

        }

        return final_string_decimal;
    }

    public interface IEnglishToBurmeseString {
        void englishToBurmeseString(String value);

    }

    public interface RepeatSound {
        void burmeseSoundRepeat(boolean b);
    }

    @SuppressLint("StaticFieldLeak")
    class MediaPlayerThread extends AsyncTask<Void, Void, Void> {
        String str;
        String amount = "";
        Map<String, List<Integer>> map;
        public MediaPlayer mp;


        MediaPlayerThread(String str, String amount) {
//            super(str);
            this.str = str;
            this.amount = amount;
            map = new LinkedHashMap<>();
            put(map, "တစ္", R.raw.one);
            put(map, "ႏွစ္", R.raw.two);
            put(map, "သံုး", R.raw.three);
            put(map, "ေလး", R.raw.four);
            put(map, "ငါး", R.raw.five);
            put(map, "ေျခာက္", R.raw.six);
            put(map, "ခုႏွစ္", R.raw.seven);
            put(map, "ရွစ္", R.raw.eight);
            put(map, "ကိုး", R.raw.nine);
            put(map, "ဆယ့္", R.raw.ten);
            put(map, "ရာ", R.raw.hundred);
            put(map, "ေထာင့္", R.raw.thousand);
            put(map, "ေသာင္း", R.raw.ten_thousand);
            put(map, "သိန္း", R.raw.lakh);
            put(map, "သန္း", R.raw.million);
            put(map, "ကုေဋ", R.raw.crore);
            put(map, "ေထာင္", R.raw.thousandtoninethousand);
            put(map, "ဆယ္", R.raw.ten_to_ninety);
            put(map, "က်ပ္", R.raw.kyat);
            put(map, "ျပား", R.raw.pya);
            put(map, "အင္တာနက္ခ်ိတ္ဆက္ပါ။", R.raw.go_online_voice); // အင္တာနက္ခ်ိတ္ဆက္ပါ။
            put(map, "ေငြသြင္း", R.raw.okcashin);
            put(map, "ေငြထုတ္", R.raw.okcashout);
            put(map, "received", R.raw.payto_received);

        }

        public void put(Map<String, List<Integer>> map, String key, Integer value) {
            if (map.get(key) == null) {
                List<Integer> list = new ArrayList<>();
                list.add(value);
                map.put(key, list);
            } else {
                map.get(key).add(value);
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                final String[] parts = str.split("\\s+");
                for (int i = 0; i < parts.length; i++) {
                    String part = parts[i];
                    if (map.get(part) != null) {
                        int ib = map.get(part).size();
                        if (ib > 0) {
                            for (int j = 0; j < map.get(part).size(); j++) {
                                mp = MediaPlayer.create(mActivity, map.get(part).get(j));
                                mp.start();
                                final int finalI = i;
                                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                    @Override
                                    public void onCompletion(MediaPlayer mp) {
//                                        Log.e("------", ":----onCompletion:");
                                        if (finalI == (parts.length - 1))
                                            isPlay = false;
                                        amount = "";
                                        mp.reset();
                                        mp.release();
                                        if (null != repeatSoundlistener)
                                            repeatSoundlistener.burmeseSoundRepeat(isPlay);
                                    }
                                });
                                try {
                                    while (mp != null && mp.isPlaying()) {
                                        if (isCancelled()) {
                                            mp.stop();
                                            mp.reset();
                                            mp.release();
                                            amount = "";
                                        }
                                        /*if (!AppPreference.getPrefsHelper().getPref(AppPreference.getPrefsHelper().VOICE_ENABLE, false)) {
                                            cancel(true);
                                            break;
                                        }*/
                                    }
                                }
                                catch (IllegalStateException ignored){
                                    ignored.printStackTrace();
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            amount = "";
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                if (mp.isPlaying())
                    mp.release();
            } catch (Exception ignored) {
            }
        }
    }

    public boolean isPlaying() {
        if (null != mediaPlayerThread && null != mediaPlayerThread.mp)
            return mediaPlayerThread.mp.isPlaying();
        return false;
    }

}