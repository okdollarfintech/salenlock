package mm.dummymerchant.sk.merchantapp.Utils;

import java.io.File;

/**
 * Created by Dell on 5/19/2016.
 */
public class CallUpdateAppInterface
{
    public static CallUpdateAppInterface appInterface;
    private callUpdateInterface updateInterface;

    public interface callUpdateInterface {
        void showNewUpdateApp(final File file, String message, final boolean enable, int version);
    }

    public void showNewUpdateApp(final File file,String message, final boolean enable, int version)
    {
        if(updateInterface!=null)
            updateInterface.showNewUpdateApp(file,message,enable,version);
    }
    public void setUpdateAppListner(callUpdateInterface appListner){
        this.updateInterface=appListner;
    }

    public static CallUpdateAppInterface getInstance(){
        if(appInterface==null)
            appInterface = new CallUpdateAppInterface();
        return appInterface;
    }
}