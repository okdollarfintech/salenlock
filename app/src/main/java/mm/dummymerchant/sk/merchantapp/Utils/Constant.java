package mm.dummymerchant.sk.merchantapp.Utils;


import mm.dummymerchant.sk.merchantapp.R;

public interface Constant {
    //


    // SubActivity

    String SPEEDKey = "m2n1shlko@$p##d";

    // Bundle Paramter
    String AGENT_CODE = "agentcode";
    String MOBILE_NO = "MobileNumber";
    String MOBILE_No = "mobile_no";
    String BUSINESS_EMOJI_ICON_FILE = "business_category_icon.txt";
    String BUSINESS_CATEGORY_FILE_MY = "business_category_my.txt";
    String BUSINESS_CATEGORY_FILE_ENG = "business_category_eng.txt";
    String MPI = "pin";
    String VENDOR_CODE = "vendorcode";
    String CLIENT_IP = "clientip";
    String CLIENT_ADDRESS = "clientos";
    String PARAM_CLIENT_TYPE = "clienttype";
    String PARAM_COMMENTS = "comments";
    String PARAM_TRANSTYPE = "TransType";
    String PARAM_SOURCE = "source";
    String PARAM_NEW_PIN = "newpin";
    String PARAM_OTP = "otp";
    String PARAM_TOKEN = "securetoken";

    String PARAM_CLIENT_TYPE_VALUE = "GPRS";
    String VENDOR_CODE_VALUE = "IPAY";

    // Intent Paramter

    String SERIALIZABLE_CLASS = "sclass";


    // TODO Production server
    String BASE_URL = "https://www.okdollar.net/WebServiceIpay/services/request";// Production Estel Server
    String BASE_APP_URL = "https://www.okdollar.co/RestService.svc"; //ProductionApp Server
    String GOOGLE_PROJ_ID = "105168504736";/// Production Server
    String BASE_TEMPALTE = "http://120.50.43.152:9090/";// Production Estel Server

    // Testing Server
    /*String BASE_URL = "http://120.50.43.164:8090/WebServiceIpay/services/request";// Test Estel Server
    String BASE_APP_URL = "http://122.248.120.221:1717/RestService.svc"; //TestApp Server
    String GOOGLE_PROJ_ID = "342445691251";
    String BASE_TEMPALTE = "http://122.248.120.221:8009/";*/

    /*String BASE_URL = "http://120.50.43.164:8090/WebServiceIpay/services/request";// Test Estel Server
    String BASE_APP_URL = "http://192.168.200.33:1717/RestService.svc/"; //TestApp Server
    String GOOGLE_PROJ_ID = "342445691251";
    String BASE_TEMPALTE = "http://122.248.120.221:8009";*/

    /*
    String BASE_URL = "http://120.50.43.164:8090/WebServiceIpay/services/request";// test  Estel Server
    String BASE_APP_URL = "http://103.242.99.234:8001/RestService.svc"; //test  Server
    String GOOGLE_PROJ_ID = "342445691251";/// Test Server
    String BASE_TEMPALTE = "http://122.248.120.221:8009/";// test  Estel Server*/


    //-------------------

    //public static String BASE_URL = "http://122.248.121.188:8090/WebServiceIpay/services/request";
    String URL_MARGE_API = "requesttype=FEELIMITKICKBACKINFO";

   // public static String BASE_APP_URL_TESTING = "http://122.248.121.189:8001/RestService.svc";
    public static String BASE_APP_URL_TESTING = "http://122.248.120.221:8001/RestService.svc";


    String URL_TRANSCATION_INFO = "requesttype=TRANSINFO";
    String URL_LANGUAGE = "/ChangeLanguage";
    String URL_CHANGE_PIN = "requesttype=CHANGEPIN";



    String URL_AUTH = "requesttype=AUTH";
    String URL_LOGIN = "requesttype=LOGIN";

    String URL_REMOTE_CONTACTS_INFO = "/RetrieveContacts?";





    String BUNDLE_IMEI = "IMEI";




    // BuS Ticketing WebService
    String HTTP_BASE = "http://www.jas.com.mm/expressbus.com.mm/webservices/";
    String HTTP_PHP_CITY = "all_city.php";

    // Request Type

    int REQUEST_TYPE_CHANGE_PIN = 1005;

    int REQUEST_TYPE_OTP = 1011;
    int REQUEST_TYPE_LOGIN = 1012;
    int REQUEST_TYPE_GET_CONTACTS= 1033;
    int REQUEST_TYPE_LANGUAGE = 1013;

    int REQUEST_TYPE_TRANSINFO_NEW = 1032;


    int REQUEST_TYPE_LOGOUT = 1017;



    int REQUEST_TYPE_CREATE_SUBSCRIBER = 1030;
    int REQUEST_TYPE_UPLOAD_PHOTO = 1053;
    int REQUEST_TYPE_RECEIVED_TRANSCATION = 1034;
    int REQUEST_TYPE_ENCRYPTED_KEY = 10342;
    int REQUEST_TYPE_TRANSACTION = 100030;
    int REQUEST_CHECK_BALANCE_LOGIN=1092;
    int REQUEST_CHECK_DESTINATION_VALIDATION=1093;




    String KEY_DATE_FORMAT = "datefrom";
    String BROADCAST_INTENT = "com.opt.request";
    String BROADCAST_INTENT_SMS = "com.rec.fail";
    String BROADCAST_INTENT_UPDATER = "com.rec.updater";



    //Gcm response
    int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    String REQUEST_TYPE_GCM = "requesttype=APPREG";
    String AGENT_CODE_GCM = "agentcode";
    String REQUEST_TYPE_GCM_APPSERVER = "/Update_GCMID?";
    String REQUEST_TYPE_AFTER_LOGIN_APPSERVER_ = "/Receive_EncryptKey?";


    String COMMENTS = "comments";
    String PIN = "pin";
    String APP_KEY = "appkey";
    String GCM_ID = "GcmID";
    String REG_ID = "regid";
    String SECURE_TOKEN = "securetoken";
    String DEVICE_TYPE = "devicetype";

   // String GOOGLE_PROJ_ID = "105168504736";/// Production Server
    String BASE_URL_GCM = "https://122.248.112.9:8443/WebServiceIpay/services/request";
    int REQUEST_TYPE_GCM_REGISTRATION = 1036;
    int REQUEST_TYPE_GCM_REGISTRATION_APPSERVER = 10360;
    int REQUEST_TYPE_AFTER_LOGIN = 103609;



    // upload device
    String BASE_URL_UPLOAD_DEVICE = "cRestService.svc/UpdateDevice";
    int REQUEST_TYPE_UPLOAD_DEVICE = 1037;
    String DUMMY_PROFILE = BASE_APP_URL+"/RegisterDummy";
    String UPDATE_DUMMY_PROFILE = BASE_APP_URL+ "/UpdateProfileDetails";

    // logout

    String REQUEST_TYPE_LOGOUT_APP= "requesttype=LOGOUT";
    String URL_SUCCESSTRANS="http://122.248.121.189:8001/RestService.svc/LogPaymentTransactions";
    String URL_FAILURETRANS="/LogTransactionFailures?";
    String URL_REQUEST_OTP = "http://122.248.121.189:8001/RestService.svc";
    String URL_RECIEVE_OTP = "/receive_otp?";
    String URL_SuccessPayment = "/LogPaymentTransactions";
    String URL_UPDATE_BUSINESS_AREA="/BroadcastGPS?";
    String URL_GET_GPS_CELLID="/GetGPSfromCellId?";




    int[] listOfCategoryImages={R.drawable.merchants, R.drawable.food_n_drink, R.drawable.restaurants, R.drawable.mobile_computers_n_electronic,
            R.drawable.travel_n_transport, R.drawable.gifts_n_flowers, R.drawable.entertainment_n_media, R.drawable.education_n_learning, R.drawable.medical_n_health,
            R.drawable.hair_n_beauty, R.drawable.media_n_communication, R.drawable.professional_services, R.drawable.automotive, R.drawable.animal_sales_n_service,
            R.drawable.manufacturing_n_agricultures, R.drawable.construction_n_contractors, R.drawable.home_n_garden, R.drawable.domestic_services, R.drawable.sport_n_recreation,
            R.drawable.real_estate, R.drawable.accmmodation, R.drawable.event_organization, R.drawable.government, R.drawable.financial_services,
            R.drawable.religion, R.drawable.miscellaneous, R.drawable.utilities, R.drawable.adult};


    int[] listOfFlags={R.drawable.abkhazia, R.drawable.afganistan, R.drawable.albania, R.drawable.algeria, R.drawable.american,
            R.drawable.andorra, R.drawable.angola, R.drawable.anguilla, R.drawable.antigua, R.drawable.argentina,
            R.drawable.armenia, R.drawable.aruba, R.drawable.australia, R.drawable.austria, R.drawable.azerbaijan,
            R.drawable.bahamas, R.drawable.bahrain, R.drawable.bangladesh, R.drawable.barbados, R.drawable.belarus,
            R.drawable.belgium, R.drawable.belize, R.drawable.benin, R.drawable.bermuda, R.drawable.bhutan,
            R.drawable.bolivia, R.drawable.bosnia, R.drawable.botswana, R.drawable.brazil, R.drawable.british,
            R.drawable.brunei, R.drawable.bulgaria, R.drawable.burkina, R.drawable.burundi, R.drawable.cambodia,
            R.drawable.cameroon, R.drawable.canada, R.drawable.cape, R.drawable.cayman, R.drawable.central,
            R.drawable.chad, R.drawable.chile, R.drawable.china, R.drawable.colombia, R.drawable.comoros,
            R.drawable.cook, R.drawable.costa, R.drawable.cote, R.drawable.croatia, R.drawable.cuba,
            R.drawable.cyprus, R.drawable.czech, R.drawable.democratic, R.drawable.denmark, R.drawable.djibouti,
            R.drawable.dominica, R.drawable.dominican, R.drawable.ecuador, R.drawable.egypt, R.drawable.elsalvador,
            R.drawable.equatorial, R.drawable.eritrea, R.drawable.estonia, R.drawable.ethiopia, R.drawable.falklandislands, R.drawable.faroeislands,
            R.drawable.fiji, R.drawable.finland, R.drawable.france, R.drawable.frenchguiana, R.drawable.frenchpolynesia,
            R.drawable.gabon, R.drawable.gambia, R.drawable.georgia, R.drawable.germany, R.drawable.ghana,
            R.drawable.gibraltar, R.drawable.greece, R.drawable.greenland, R.drawable.grenada, R.drawable.guam,
            R.drawable.guatemala, R.drawable.guinea, R.drawable.guineabissau, R.drawable.guyana, R.drawable.haiti,
            R.drawable.honduras, R.drawable.hongkong, R.drawable.hungary, R.drawable.iceland, R.drawable.india,
            R.drawable.india, R.drawable.india, R.drawable.indonesia, R.drawable.iran,
            R.drawable.iraq, R.drawable.ireland, R.drawable.israel, R.drawable.italy, R.drawable.jamaica,
            R.drawable.japan, R.drawable.japan, R.drawable.jordan, R.drawable.kazakhstan, R.drawable.kenya, R.drawable.kiribati,
            R.drawable.koreanorth, R.drawable.koreasouth, R.drawable.kuwait, R.drawable.kyrgyzstan, R.drawable.laos,
            R.drawable.latvia, R.drawable.lebanon, R.drawable.lesotho, R.drawable.liberia, R.drawable.libya,
            R.drawable.liechtenstein, R.drawable.lithuania, R.drawable.luxembourg, R.drawable.macao, R.drawable.macedonia,
            R.drawable.madagascar, R.drawable.malawi, R.drawable.malaysia, R.drawable.maldives, R.drawable.mali,
            R.drawable.malta, R.drawable.marshallisland, R.drawable.martinique, R.drawable.mauritania, R.drawable.mauritius, R.drawable.micronesia,
            R.drawable.moldova, R.drawable.monaco, R.drawable.mongolia, R.drawable.montenegro, R.drawable.montserrat,
            R.drawable.morocco, R.drawable.mozambique, R.drawable.myanmar, R.drawable.namibia, R.drawable.nauru, R.drawable.nepal,
            R.drawable.netherlands, R.drawable.netherlandsantilles, R.drawable.newcaledonia, R.drawable.newzealand, R.drawable.nicaragua,
            R.drawable.niger, R.drawable.nigeria, R.drawable.niue, R.drawable.norway, R.drawable.oman, R.drawable.pakistan,
            R.drawable.palau, R.drawable.panama, R.drawable.papua, R.drawable.paraquay, R.drawable.peru,
            R.drawable.philippines, R.drawable.poland, R.drawable.portugal, R.drawable.puerto, R.drawable.quatar,
            R.drawable.republiccongo, R.drawable.reunion, R.drawable.romania, R.drawable.russia, R.drawable.rwanda,
            R.drawable.saintkittsnevis, R.drawable.saintlucia, R.drawable.saintvincentgrenadines, R.drawable.samoa, R.drawable.sanmarino, R.drawable.saotome,
            R.drawable.saudiarabia, R.drawable.senegal, R.drawable.serbia, R.drawable.seychelles, R.drawable.sierra,
            R.drawable.singapore, R.drawable.slovakia, R.drawable.slovenia, R.drawable.solomon, R.drawable.somalia,
            R.drawable.southafrica, R.drawable.southsudan, R.drawable.spain, R.drawable.srilanka, R.drawable.sthelenaascensionandtristan,
            R.drawable.stpierreandmiquelon, R.drawable.sudan, R.drawable.suriname, R.drawable.swaziland, R.drawable.sweden,
            R.drawable.switzerland, R.drawable.syrian, R.drawable.taiwan, R.drawable.tajikistan, R.drawable.tanzania,
            R.drawable.thailand, R.drawable.timorlesteeasttimor, R.drawable.togo, R.drawable.tonga, R.drawable.trinidadtobago,
            R.drawable.tunisia, R.drawable.turkey, R.drawable.turkmenistan, R.drawable.turkscaicos, R.drawable.tuvalu, R.drawable.uganda,
            R.drawable.uk, R.drawable.ukraine, R.drawable.unitedarabemirates, R.drawable.unitedstates, R.drawable.unitedstates,
            R.drawable.unitedstates, R.drawable.unitedstates, R.drawable.uruguay, R.drawable.uzbekistan, R.drawable.vanuatu, R.drawable.venezuela,
            R.drawable.vietnam, R.drawable.wallisfutuna, R.drawable.yemen, R.drawable.zambia, R.drawable.zimbabwe
    };


    String[] listOfStringImages={"R.drawable.retail", "R.drawable.food_n_drink", "R.drawable.restaurants", "R.drawable.mobile_computers_n_electronic",
            "R.drawable.travel_n_transport", "R.drawable.gifts_n_flowers", "R.drawable.entertainment_n_media", "R.drawable.education_n_learning", "R.drawable.medical_n_health",
            "R.drawable.hair_n_beauty", "R.drawable.media_n_communication", "R.drawable.professional_services", "R.drawable.automotive", "R.drawable.animal_sales_n_service",
            "R.drawable.manufacturing_n_agricultures", "R.drawable.construction_n_contractors", "R.drawable.home_n_garden", "R.drawable.domestic_services", "R.drawable.sport_n_recreation",
            "R.drawable.real_estate", "R.drawable.accmmodation", "R.drawable.event_organization", "R.drawable.government", "R.drawable.financial_services",
            "R.drawable.religion", "R.drawable.miscellaneous", "R.drawable.utilities", "R.drawable.adult"};


    String DEFAULT_INCRYPTION_PASSWORD = "123456";




    /*String BUNDLE_MAP_CELL_ID="cell_id";
    public static final String BUNDLE_MAP_NUMBER="number";
    public static final String BUNDLE_MAP_AMOUNT="amount";*/
    String MPT = "MPT";


    // retrieve profile
    String MOBILE_NUMBER_RETRIEVE = "mobilenumber";
    int REQUEST_TYPE_RETRIVE_PROFILE = 100055;
    String HTTP_URL_RETRIEVE_PROFILE = "/RetrieveProfile?";
    String HTTP_URL_ValidateAppVersion = "/ValidateAppVersion";

    //FOR IDENTIFYING LOGIN MASTER OR CASHIER


    String MASTER="1";
    String CASHIER="2";
    String MASTER_TAG="MASTER";
    String STATUS_LOGGEDIN="s";
    String STATUS_LOGGEDOUT="n";
    String LOGOUT_PERMANENT_MSG="Permanent logout";



    // TODO calling Login API request Paramter

    int APP_SERVER_PARAMS_LOGIN=6000;
    int APP_SERVER_PARAMS_VERIFY=6001;

    String LOGIN_BY_APP = BASE_APP_URL+"/Login";
    String VERIFY_BY_APP = BASE_APP_URL+"/ReVerify";
    String UPDATE_PROFILE_AFTER= "/UpdateProfileDetails";
    String CREATE_PROFILE= "/UpdateProfile";
    int APP_SERVER_UPDATE_PROFILE= 100058;
    int APP_SERVER_CREATE_PROFILE=100088;

    String URL_FIND_NEAR_BY_MERCHANT="/FindNearByMerchants?";
    int REQUEST_TYPE_NEARBY_MERCHANT=17777;


    String EXCEL_TRANSACTION_FILE_NAME="transactiondb.csv";
    String EXCEL_RECEIVED_MONEY_FILE_NAME="receivedmoney.csv";
    String EXCEL_ACTIVITY_LOG_DETAILS="activity_log.csv";
    String EXCEL_CASH_COLLECTOR="cash_collector.csv";
    String EXCEL_CASHIER_TABLE="cashiertable.csv";


    String BUNDLE_KEY_FROM_EMAIL_ID="from_mail";
    String BUNDLE_KEY_PWD="pwd";
    String BUNDLE_KEY_TO_EMAIL_ID="to_email";
    String BUNDLE_UPDATEORINSERT="key_update_or_insert";
    String MAIL_BODY_MSG="OK Safety Cashier Android attachement details :";

    int DIGITALRECEIPT_QR_REQUESTCODE=200;
    int   CALL_CONTACT_PICKER=10002;
    String UNKNOWN="Unknown";

    String SAFETYCASHIER_TO_CASHCOLLECTOR="SC";


    String POSITION="POSITION";
    String COUNTRY_NAME="COUNTRY_NAME";
    String COUNTRY_CODE="COUNTRY_CODE";

    String KEY_NAME="name";
    String KEY_NO="no";
    String MYANMAR_COUNTRY_CODE="+95";
    String OK="OK";
    String CANCEL="Cancel";

    String TYPE_SMS="share";
    String TYPE_QRCODE="qrcode";
    int OTHER_LANGUAGE=0;
    int BURMESE_LANGUAGE=1;
    String EMPT="MPT";
    final String[] STARR_OPTIONS={"QR Code Scanner","Pick QR Image From Gallery"};
    String OPTIONS = "OPTIONS";
    String FAILURE="FAILURE";
    String NOTRECEIVED="Not Received";
    String SUCCESS="SUCCESS";
    String RECEIVED="Received";

     int SELECT_PICTURE_SC=101;
    String CONTACT_PICKER_KEY="key";
    int VALUE_DIGITAL_RECEIPT_CONTACTPICKER=1;
    int VALUE_CASHIERCREATE_CONTACT_PICKER=2;
    int VALUE_CASHCOLLECTOR_CONTACT_PICKER=3;
    String START_INDEX = "startindex";
    String LAST_INDEX = "lastindex";

    int REQUEST_TYPE_ADD_SHOPS=10096;
    int REQUEST_FOR_LUCKEY_DRAW=20099;
    String URL_ADD_SHOP_DETAILS="/AddShops?";

    String alertIcon="\u26a0";
    String infoIcon="\u2757";

    String CASHIER_ACTIVE="active";
    String CASHIER_DELETED="deleted";
    String CASHCOLLECTOR_ACTIVE="active";
    String CASHCOLLECTOR_DELETED="deleted";

    int REQUEST_TYPE_MAP_LAT_LONG = 12345678;
    String PHONE_NUMBER_LAT_LONG="MobileNumber";
    String CELL_ID = "CellID";
    String HTTP_LAT_LONG = "/GetGPSfromCellId";


    String MO_NUMBER = "mobile_no";
    String SIM_ID = "simid";
    String SIM_IDI = "Simid";
    String BUNDLE_POSITION = "bPosition";




    String MERCHANT_MOBILE_NO = "MerchantMobileNumber";
    String REQUEST_TYPE_BUSSINESS_DETAILS= "/GetBusinessDetails?";

    int REQUEST_TYPE_DELETE_SHOPS=10093;
    int REQUEST_CHECK_STATUS_SHOPS=10092;

    String URL_DELETE_SHOP="/DeleteShop?";
    String URL_CHECK_STATUS_SHOP="/CheckShopStatus?";
    String URL_VIEW_PROMOTION="/PromotionViews?";
    int REQUEST_VIEW_PROMOTION=202003;
    int REQUEST_ADD_PROMOTION=202002;
    int REQUEST_DELETE_PROMOTION=202004;
    String URL_DELETE_PROMOTION="/DeletePromotion?";
    String URL_ADD_PROMOTION="/AddPromotions?";
    int REQUEST_TYPE_ADD_EMAIL_SETTINGS=10095;
    int REQUEST_TYPE_ADD_SMS_SETTINGS=10094;
    String URL_ADD_PROFILE_SETTINGS="/AddProfileSetting?";

    String KEY_FROM="FROM";
    String KEY_FROM_INT="FROM_INT";
    String PAID="Paid";
    String LATER="later";
    String PENDING="pending";
    String SSL_CERTIFICATE = "7c338077a6174a9c.crt";

  //  public static final String BUNDLE_MAP_LAT_LONG="latLong";
    int REQUESTTYPE_GPS_CELLID=1000003;

    String FROM_MAIL = "crashreport@okdollar.com";
    String TO_MAIL = "crashreport@okdollar.com";
    String MAIL_PWD = "speed123";

    String DESTINATION = "destination";
    String URL_BALANCE = "requesttype=BALANCE";

    String BUNDLE_MAP_NUMBER = "number";
    String BUNDLE_MAP_AMOUNT = "amount";
    String BUNDLE_MAP_LAT_LONG = "latLong";
    String BUNDLE_MAP_CELL_ID = "cell_id";

    String Version = "Version";
    String MSID = "MSID";
    String OSType = "OSType";
    String AppType = "AppType";
    String COMMA_DELIMITER = ",";
    String NEW_LINE_SEPARATOR = "\n";
    String TERMS_CONDITION = "terms.html";
    String TERMS_CONDITION_AGENT = "agent_terms.html";

    int CAMERA_FOR_CREATECASHIER=301;
    int CAMERA_FOR_UPDATECASHIER=302;
    int CAMERA_FOR_NEWCASHCOLLECTOR=303;
    int CAMERA_FOR_UPDATECASHCOLLECTOR=304;
    int CAMERA_FOR_REGISTRATION=102;

    int CAMERA_FOR_BK_CREATECASHIER=109;
    int CAMERA_FOR_BK_UPDATECASHIER=110;
    int CAMERA_FOR_BK_NEWCASHCOLLECTOR=111;
    int CAMERA_FOR_BK_UPDATECASHCOLLECCTOR=112;

    String SPEEDKYAT_NO_RECIEVING = "+95930000044";
    String SPEEDKYAT_NO_FOR_OFFLINE_OUTSIDE_MYANMAR = "+85264506058";
    String SPEEDKYAT_NO_SENDING = "+95930000055";

    String OK_DOLLAR_CUSTOMER_CARE1 = "+951377888";
    String OK_DOLLAR_CUSTOMER_CARE2 = "+959450144455";
    String OK_DOLLAR_CUSTOMER_CARE3 = "+95930000066";

    String ADV_MER_NOT_APPROVED="Advance Merchant not Approved";
    String USER_NT_REG="User Not Register";
    String LOGIN_FAIL_FOR_OTHERS="Login request failed for Other than Safty Cashier";

}
