package mm.dummymerchant.sk.merchantapp.Utils;


import android.app.DatePickerDialog;


import mm.dummymerchant.sk.merchantapp.model.GPS_Location;

/**
 * Created by kapil on 20/10/15.
 */
public interface GPSLocationResponse {
    public void onSuccess(GPS_Location response);

    void onDateSet(DatePickerDialog datePickerDialog, int year, int monthOfYear, int dayOfMonth);

    public void onFail();
}
