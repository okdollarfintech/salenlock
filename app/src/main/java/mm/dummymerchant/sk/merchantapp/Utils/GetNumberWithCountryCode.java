package mm.dummymerchant.sk.merchantapp.Utils;

import android.content.Context;
import android.content.SharedPreferences;


import java.util.ArrayList;

import mm.dummymerchant.sk.merchantapp.model.NetworkOperatorModel;

/**
 * Created by abhishekmodi on 26/12/15.
 */
public class GetNumberWithCountryCode {

    //create an object of GetNumberWithCountryCode
    public static GetNumberWithCountryCode mGetNumberWithCountryCode = new GetNumberWithCountryCode();
    private ArrayList<NetworkOperatorModel> listOfCountryCodesAndNames;

    //make the constructor private so that this class cannot be
    //instantiated
    private GetNumberWithCountryCode() {
    }

    //Get the only object available
    public static GetNumberWithCountryCode getInstance() {
        return mGetNumberWithCountryCode;
    }


    public String[] getCountryCodeNameAndFlagFromNumberLogin(String no, Context mContext) {
        if (listOfCountryCodesAndNames == null) {
            listOfCountryCodesAndNames = new NetworkOperatorModel().getListOfNetworkOperatorModel(mContext);
        }
        String countryCode = "", countryName = "", flagStr = "";
        int flag = -1;
        if (no.startsWith("+"))
            no = no.replace("+", "00");
        no = convertValidNumber(no);
        int id = -1;
        if (no.startsWith("+") || no.startsWith("00")) {
            String code = no.substring(0, 6);
            for (int i = 0; i < listOfCountryCodesAndNames.size(); i++) {

                ArrayList<String> dialingCodes = listOfCountryCodesAndNames.get(i).getListOfDialingCodes();
                if (dialingCodes != null) {
                    for (int j = 0; j < dialingCodes.size(); j++) {
                        String countryId = dialingCodes.get(j).replace("+", "00");

                        if (code.contains(countryId)) {
                            System.out.println(i);
                            id = i;
                            int length = countryId.length();
                            no = no.substring(3);
                            countryCode = listOfCountryCodesAndNames.get(id).getCountryCode();
                            countryName = listOfCountryCodesAndNames.get(id).getCountryName();
                            flag = listOfCountryCodesAndNames.get(i).getCountryImageId();
                            flagStr = String.valueOf(flag);
                            break;
                        }
                    }

                } else {

                    String countryId = "00" + listOfCountryCodesAndNames.get(i).getCountryCode().substring(1);
                    if (code.contains(countryId)) {
                        System.out.println(i);
                        id = i;
                        int length = countryId.length();
                        no = no.substring(length);
                        countryCode = listOfCountryCodesAndNames.get(id).getCountryCode();
                        countryName = listOfCountryCodesAndNames.get(id).getCountryName();
                        flag = listOfCountryCodesAndNames.get(i).getCountryImageId();
                        flagStr = String.valueOf(flag);
                        break;
                    }
                }

            }
            if (countryCode.equals("+95")) {
                no = "0" + no;
            }
        }

        if (id == -1) {
            if (no.startsWith("0")) {
                if (!AppPreference.getCountryCodeLogin(mContext).contains("+95"))
                    no = no.substring(1);
            } else {
                no = "0" + no;
            }
            countryCode = AppPreference.getCountryCodeLogin(mContext);
            flag = AppPreference.getCountryImageIDLogin(mContext);
            countryName = AppPreference.getCountryNameLogin(mContext);
            flagStr = String.valueOf(flag);
        }

        return new String[]{countryCode, countryName, flagStr, no};
    }


    public String convertValidNumber(String number) {
        if (number.contains("+"))
            number = number.replace("+", "00");
        if (number.contains(" "))
            number.replace(" ", "");
        if (number.contains("-"))
            number = number.replace("-", "");
        return number;
    }


}
