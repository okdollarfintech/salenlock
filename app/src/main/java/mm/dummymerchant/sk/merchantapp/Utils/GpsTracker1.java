package mm.dummymerchant.sk.merchantapp.Utils;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;

/**
 * Created by user on 12/26/2015.
 */
public class GpsTracker1 extends Service implements LocationListener
{
    private Context mContext;
    public static   String lat,lang;
    String cellId,lAc;
    //flag for GPS Status
    boolean isGPSEnabled = false;
    //flag for network status
    boolean isNetworkEnabled = false;
    boolean canGetLocation = false;
    Location location;
    public double latitude;
    public double longitude;
    // TelephonyManager tm;
    GsmCellLocation gsmcelllocation;
    private String IMEI,lac,cid,mcc,mnc,celltower_details;

    //The minimum distance to change updates in metters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; //10 metters

    //The minimum time beetwen updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute

    //Declaring a Location Manager
    protected LocationManager locationManager;

    public GpsTracker1(Context mContext)
    {
        this.mContext = mContext;
        //   getLocation();
    }

    public Location getLocation()
    {
        try
        {
            locationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);

            //getting GPS status
            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

            //getting network status
            isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled)
            {
                // no network provider is enabled
            }
            else
            {
                this.canGetLocation = true;

                //First get location from Network Provider
                if (isNetworkEnabled)
                {
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                    Log.d("Network", "Network");

                    if (locationManager != null)
                    {
                        location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        lat=""+location.getLatitude();
                        lang=""+location.getLongitude();
                        updateGPSCoordinates();
                    }
                }

                //if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled)
                {
                    if (location == null)
                    {
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                        Log.d("GPS Enabled", "GPS Enabled");

                        if (locationManager != null)
                        {
                            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            lat=""+location.getLatitude();
                            lang=""+location.getLongitude();

                            updateGPSCoordinates();
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            //e.printStackTrace();
            Log.e("Error : Location", "Impossible to connect to LocationManager", e);
        }

        return location;
    }

    public void updateGPSCoordinates()
    {
        if (location != null)
        {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
        }
    }

    public double getLatitude()
    {
        if (location != null)
        {
            latitude = location.getLatitude();
        }

        return latitude;
    }



    public boolean canGetLocation()
    {
        return this.canGetLocation;
    }
    /**
     * Function to get longitude
     */
    public double getLongitude()
    {
        if (location != null)
        {
            longitude = location.getLongitude();
        }

        return longitude;
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void init_Location()
    {
        final TelephonyManager telephony = (TelephonyManager)mContext. getSystemService(Context.TELEPHONY_SERVICE);

        IMEI = telephony.getDeviceId();
        gsmcelllocation = (GsmCellLocation) telephony.getCellLocation();
        try {
            String networkOperator = telephony.getNetworkOperator();
            System.out.println("mcc1");
            mcc = networkOperator.substring(0, 3);
            mnc = networkOperator.substring(3);
            if (telephony.getPhoneType() == TelephonyManager.PHONE_TYPE_GSM) {
                if(location!=null) {
                    int lac1 = gsmcelllocation.getLac();
                    int cid1 = gsmcelllocation.getCid();
                    lac = String.valueOf(lac1);
                    cid = String.valueOf(cid1);
                    celltower_details = " IMEI :" + IMEI
                            + "\n lac :" + String.valueOf(lac1)
                            + "\n cid :" + String.valueOf(cid1)
                            + "\n mcc :" + mcc
                            + "\n mnc :" + mnc;

                    Log.i("celltower_details ",celltower_details);
                }
            }else{
            }

        }
        catch (Exception e){

        }
    }

}

