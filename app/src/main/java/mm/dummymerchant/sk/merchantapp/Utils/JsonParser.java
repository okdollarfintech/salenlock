package mm.dummymerchant.sk.merchantapp.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JsonParser {

	public JSONObject varifyResponse(String jsonString) throws Exception {
		try {
			JSONObject resp = new JSONObject(jsonString);
			JSONObject status = resp.getJSONObject("status");
			JSONObject data = null;

			String message = status.getString("msg");
			int status_code = status.getInt("code");
			switch (status_code) {
			case 200:
				if (resp.has("data"))
					data = resp.getJSONObject("data");
				break;
			case 400:
				throw new Exception(message);
			case 401:
				throw new Exception(message);
			case 402:
				throw new Exception(message);
			case 405:
				throw new Exception(message);

			default:
				break;
			}
			return data;
		} catch (JSONException e) {
			throw new Exception("Invalid response format.");
		}
	}

	public JSONArray varifyArrayResponse(String jsonString) throws Exception {
		try {
			JSONObject resp = new JSONObject(jsonString);
			JSONObject status = resp.getJSONObject("status");
			JSONArray data = null;

			String message = status.getString("msg");
			int status_code = status.getInt("code");
			switch (status_code) {
			case 200:
				if(resp.has("data")) {
					data = resp.getJSONArray("data");
				}


				break;
			case 400:
				throw new Exception(message);
			case 401:
				throw new Exception(message);
			case 402:
				throw new Exception(message);
			case 405:
				throw new Exception(message);

			default:
				break;
			}
			return data;
		} catch (JSONException e) {
			throw new Exception("Invalid response format.");
		}
	}

	//
	public JSONArray varifyArrayResponse_Table(String jsonString) throws Exception {
		try {
			JSONObject resp = new JSONObject(jsonString);
			JSONObject status = resp.getJSONObject("status");
			JSONObject data = null;
			JSONArray table = null;
			String message = status.getString("msg");
			int status_code = status.getInt("code");
			switch (status_code) {
				case 200:
						data = resp.getJSONObject("data");
					    table = data.getJSONArray("Table");
					break;
				case 400:
					throw new Exception(message);
				case 401:
					throw new Exception(message);
				case 402:
					throw new Exception(message);
				case 405:
					throw new Exception(message);

				default:
					break;
			}
			return table;
		} catch (JSONException e) {
			throw new Exception("Invalid response format.");
		}
	}
	///


}
