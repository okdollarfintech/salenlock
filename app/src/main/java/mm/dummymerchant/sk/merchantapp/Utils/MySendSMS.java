package mm.dummymerchant.sk.merchantapp.Utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.SmsManager;
import android.widget.Toast;

import mm.dummymerchant.sk.merchantapp.NewLoginStep2ActivityNew;
import mm.dummymerchant.sk.merchantapp.R;


/**
 * Created by kapil on 05/10/15.
 */
public class MySendSMS {
    public int flag;
    public String name;
    private Context context;
    BroadcastReceiver sentReceiver=null, receiveReceiver=null;
    public MySendSMS(Context context) {
        this.context = context;
    }

    @SuppressLint("NewApi")
    public void sendSMS(String mobileNo, String msg) {

        final String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";
        PendingIntent sentPI = PendingIntent.getBroadcast(context, 0, new Intent(SENT), PendingIntent.FLAG_CANCEL_CURRENT);
        PendingIntent deliveredPI = PendingIntent.getBroadcast(context, 0, new Intent(DELIVERED), PendingIntent.FLAG_CANCEL_CURRENT);

        sentReceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        MySendSMSStatus mySendSMSStatus = (MySendSMSStatus) context;
                        mySendSMSStatus.onSent();
                        Utils.showToast(context, context.getResources().getString(R.string.sms_sent));
                        context.unregisterReceiver(sentReceiver);
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        Utils.showToast(context, context.getResources().getString(R.string.generic_faluire));
                        context.unregisterReceiver(sentReceiver);
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Utils.showToast(context, context.getResources().getString(R.string.no_service_msg));
                        context.unregisterReceiver(sentReceiver);
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        Utils.showToast(context, context.getResources().getString(R.string.sms_null));
                        context.unregisterReceiver(sentReceiver);
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        Utils.showToast(context, context.getResources().getString(R.string.sms_radio_off));
                        context.unregisterReceiver(sentReceiver);
                        break;
                }
            }
        };


        receiveReceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {

                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        MySendSMSStatus mySendSMSStatus = (MySendSMSStatus) context;
                        mySendSMSStatus.onDelivered(null);
                        Utils.showToast(context, context.getResources().getString(R.string.sms_delivered));
                        context.unregisterReceiver(receiveReceiver);
                        break;
                    case Activity.RESULT_CANCELED:
                        Utils.showToast(context, context.getResources().getString(R.string.sms_not_delivered));
                        context.unregisterReceiver(receiveReceiver);
                        break;
                }
            }
        };

        context.registerReceiver(sentReceiver, new IntentFilter(SENT));
        context.registerReceiver(receiveReceiver, new IntentFilter(DELIVERED));

        SmsManager sms = SmsManager.getDefault();
        String mobileNoFORSMS = mobileNo;
        if (mobileNoFORSMS.startsWith("00"))
            mobileNoFORSMS = "+" + mobileNoFORSMS.substring(2);
       // Toast.makeText(context,"mobile..."+mobileNoFORSMS,Toast.LENGTH_LONG).show();
        sms.sendTextMessage(mobileNoFORSMS, null, msg, sentPI, deliveredPI);

    }


}
