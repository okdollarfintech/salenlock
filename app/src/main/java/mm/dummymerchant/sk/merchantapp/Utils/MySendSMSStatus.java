package mm.dummymerchant.sk.merchantapp.Utils;

/**
 * Created by kapil on 19/10/15.
 */
public interface MySendSMSStatus {

    public void onSent();
    public void onError(String error);
    public void onDelivered(String msg);
}
