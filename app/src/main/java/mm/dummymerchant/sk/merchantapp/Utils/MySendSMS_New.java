package mm.dummymerchant.sk.merchantapp.Utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.SmsManager;

import mm.dummymerchant.sk.merchantapp.R;


/**
 * Created by kapil on 05/10/15.
 */
public class MySendSMS_New {
    public static int flag=0;
    private Context context;
    private BroadcastReceiver sentReceiver=null, receiveReceiver=null;
    public MySendSMS_New(Context context) {
        this.context = context;
    }

    @SuppressLint("NewApi")
    public void sendSMS(String mobileNo, String msg) {


        final String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";
        PendingIntent sentPI = PendingIntent.getBroadcast(context, 0, new Intent(SENT), PendingIntent.FLAG_CANCEL_CURRENT);
        PendingIntent deliveredPI = PendingIntent.getBroadcast(context, 0, new Intent(DELIVERED), PendingIntent.FLAG_CANCEL_CURRENT);

        sentReceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        flag=0;
                        try {
                            MySendSMSStatus mySendSMSStatus = (MySendSMSStatus) context;
                            mySendSMSStatus.onSent();
                            Utils.showCustomToastMsg(context, R.string.sms_sent, true);
                            context.unregisterReceiver(sentReceiver);
                            }catch(Exception e){

                        }
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        flag=0;
                        try{
                        Utils.showCustomToastMsg(context, R.string.generic_faluire, false);
                        context.unregisterReceiver(sentReceiver);
                        }catch(Exception e){

                        }
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        flag=0;
                        try{
                        Utils.showCustomToastMsg(context, R.string.no_service_msg, false);
                        context.unregisterReceiver(sentReceiver);
                        }catch(Exception e){

                        }
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        flag=0;
                        try{
                        Utils.showCustomToastMsg(context, R.string.sms_null, false);
                        context.unregisterReceiver(sentReceiver);
                        }catch(Exception e){

                        }
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        flag=0;
                        try {
                        Utils.showCustomToastMsg(context, R.string.sms_radio_off, false);
                        context.unregisterReceiver(sentReceiver);
                        }catch(Exception e){

                        }
                        break;
                }
            }
        };


        receiveReceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {

                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        flag=0;
                        try {
                            MySendSMSStatus mySendSMSStatus = (MySendSMSStatus) context;
                            mySendSMSStatus.onDelivered(null);
                            Utils.showCustomToastMsg(context, R.string.sms_delivered, true);
                            context.unregisterReceiver(receiveReceiver);
                        }catch (Exception e){

                        }
                        break;
                    case Activity.RESULT_CANCELED:
                        flag=0;
                        try {
                            Utils.showCustomToastMsg(context, R.string.sms_not_delivered, false);
                            context.unregisterReceiver(receiveReceiver);
                        }catch (Exception e){

                        }
                        break;
                }
            }
        };

        context.registerReceiver(sentReceiver, new IntentFilter(SENT));
        context.registerReceiver(receiveReceiver, new IntentFilter(DELIVERED));

        SmsManager sms = SmsManager.getDefault();
        String mobileNoFORSMS = mobileNo;
        if (mobileNoFORSMS.startsWith("00"))
            mobileNoFORSMS = "+" + mobileNoFORSMS.substring(2);
        sms.sendTextMessage(mobileNoFORSMS, null, msg, sentPI, deliveredPI);
        if (flag>1){
            Utils.showAlert(context, context.getString(R.string.sms_permission_error));
        }

    }
}
