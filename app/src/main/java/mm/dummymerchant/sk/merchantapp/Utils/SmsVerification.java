package mm.dummymerchant.sk.merchantapp.Utils;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Keerthivasan on 28-Apr-18.
 */
public class SmsVerification {

    private static Context context;

    public static SmsVerification with(Context context){
        SmsVerification.context = context;
        return new SmsVerification();
    }

    public ArrayList<String> getMsgData1(String addMyChangeNo) {

        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("+95930000055");
        arrayList.add("OKDollar");
        arrayList.add("0930000055");
        arrayList.add("OK $");
        arrayList.add("+95930000066");
        arrayList.add("+959450144455");
        arrayList.add("+951377888");
        arrayList.add("+930000055");
        arrayList.add("+959969696000");
        arrayList.add("0930000044");
        arrayList.add("+95930000044");
        arrayList.add("+95936948450");

        if (!addMyChangeNo.equals("")) {
            String[] addMyChangeNos = addMyChangeNo.split(",");
            for (int i = 0; i < addMyChangeNos.length; i++) {
                arrayList.add(addMyChangeNos[i].replace("r", "").replace("n", "").replace("\\", "").trim());
            }
        }
        Log.i("Size Verify : ", "" + arrayList.size());
        return arrayList;
    }


    public boolean isAddressPresent(String s) {
        ArrayList<String> arrayList = getMsgData1(AppPreference.getSmsSender(context));
        if (arrayList.contains(s)) {
            return true;
        } else {
            return false;
        }
    }
}
