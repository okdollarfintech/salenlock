package mm.dummymerchant.sk.merchantapp.Utils;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Dialog;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Vibrator;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import android.text.InputFilter;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.UnderlineSpan;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.scottyab.aescrypt.AESCrypt;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.security.SecureRandom;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;

import mm.dummymerchant.sk.merchantapp.GPSTracker;
import mm.dummymerchant.sk.merchantapp.R;
import mm.dummymerchant.sk.merchantapp.customView.CustomButton;
import mm.dummymerchant.sk.merchantapp.customView.CustomEdittext;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
import mm.dummymerchant.sk.merchantapp.db.DBHelper;
import mm.dummymerchant.sk.merchantapp.httpConnection.AllInerfaces;
import mm.dummymerchant.sk.merchantapp.model.CashierModel;
import mm.dummymerchant.sk.merchantapp.model.CityList_Model;
import mm.dummymerchant.sk.merchantapp.model.ConstactModel;
import mm.dummymerchant.sk.merchantapp.model.LaterModel;
import mm.dummymerchant.sk.merchantapp.model.Remarks;
import mm.dummymerchant.sk.merchantapp.model.TownShip_Model;
import mm.dummymerchant.sk.merchantapp.model.UserInctivityModel;

import static mm.dummymerchant.sk.merchantapp.Utils.AppPreference.getInternetOn_at_login;

/**
 * Created by user on 10/26/2015.
 */
public class Utils implements Constant {

    public static int prevLenght=0;
    String contactNo="";
    AllInerfaces.CallFinishInterface cInterface;
    AllInerfaces.CallSplashInterface callSplash;
    AllInerfaces.CallRemarkInterface callRemarkInterface;

    public static void showToast(Context context, String message) {
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        toast.setGravity(Gravity.TOP, 0, 0);
        v.setTypeface(Utils.getFont(context));
        toast.show();
    }
    public static Typeface getFont(Context context) {
        if (AppPreference.getLanguage(context).equals("my"))
            return Typeface.createFromAsset(context.getAssets(), "ZawgyiOne.ttf");
        else
            return Typeface.createFromAsset(context.getAssets(), "Roboto-Light.ttf");
    }

    public static boolean isEmpty(String key) {

        Log.e("utilskey", "..." + key);

        if(key==null)
            return true;
       else if (key.toString().trim().equals("") || key.toString().trim().equals("null"))
        return true;

        else
         return false;
    }

    public static SpannableString formatedAmountwithoutMMK(String amountva, Context context) {

        SpannableString ss1 = new SpannableString("");
        try {

            if (amountva.contains(context.getResources().getString(R.string.one)) || amountva.contains(context.getResources().getString(R.string.two)) || amountva.contains(context.getResources().getString(R.string.three)) || amountva.contains(context.getResources().getString(R.string.four)) || amountva.contains(context.getResources().getString(R.string.five)) || amountva.contains(context.getResources().getString(R.string.six)) || amountva.contains(context.getResources().getString(R.string.seven)) || amountva.contains(context.getResources().getString(R.string.eight)) || amountva.contains(context.getResources().getString(R.string.nine)) || amountva.contains(context.getResources().getString(R.string.ten))) {
                amountva = convertBurmeseToEnglishNumber(amountva);
            }

            BigDecimal amount = new BigDecimal(amountva);
            amount = amount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
            String am = amount.toString();
            if (am.contains(".00")) {
                am = am.replace(".00", "");
            }
            if (am.contains(".")) {

                System.out.println(am);
                int index = am.indexOf(".");
                ss1 = new SpannableString(am);
                ss1.setSpan(new RelativeSizeSpan(0.8f), index, ss1.length(), 0); // set size
            } else {

                if (am.contains("၁") || am.contains("၂") || am.contains("၃") || am.contains("၄") || am.contains("၅") || am.contains("၆") || am.contains("၇") || am.contains("၈") || am.contains("၉") || am.contains("၀")) {
                    am = convertBurmeseToEnglishNumber(amountva);
                }
                if (am.contains(".00")) {
                    am = am.replace(".00", "");
                }
                ss1 = new SpannableString(am);

            }
        } catch (Exception e) {
            e.printStackTrace();

        }

        return ss1;

    }
    public static String convertBurmeseToEnglishNumber(String burmseNumber) {


        StringBuilder englishNumber = new StringBuilder();

        if (burmseNumber.contains("1") || burmseNumber.contains("2") || burmseNumber.contains("3") || burmseNumber.contains("4") || burmseNumber.contains("5") || burmseNumber.contains("6") || burmseNumber.contains("7") || burmseNumber.contains("8") || burmseNumber.contains("9") || burmseNumber.contains("0")) {


            return burmseNumber;

        } else {

            for (int i = 0; i < burmseNumber.length(); i++) {
                char c = burmseNumber.charAt(i);
                switch (c) {
                    case '၁':
                        englishNumber.append("1");
                        break;
                    case '၂':
                        englishNumber.append("2");
                        break;
                    case '၃':
                        englishNumber.append("3");
                        break;
                    case '၄':
                        englishNumber.append("4");
                        break;
                    case '၅':
                        englishNumber.append("5");
                        break;
                    case '၆':
                        englishNumber.append("6");
                        break;
                    case '၇':
                        englishNumber.append("7");
                        break;
                    case '၈':
                        englishNumber.append("8");
                        break;
                    case '၉':
                        englishNumber.append("9");
                        break;
                    case '၀':
                        englishNumber.append("0");
                        break;
                    case '.':
                        englishNumber.append(".");
                        break;

                }
            }
        }
        return englishNumber.toString();
    }




    public static ArrayList<Integer> getAllMonthsBetweenDates(Date firstDate, Date secondDate)
    {
        ArrayList<Integer>totalMonths=new ArrayList<>();
        int diffMonths=0;
        Calendar cal = Calendar.getInstance();
        cal.setTime(firstDate);
        int month1= cal.get(Calendar.MONTH)+1;
        cal.setTime(secondDate);
        int month2=cal.get(Calendar.MONTH)+1;
        diffMonths=month2-month1;
        if(diffMonths>0) {
            for (int i = month1; i <= month2; i++)
                totalMonths.add(i);
        }
        else
            totalMonths.add(month1);
        return totalMonths;
    }

    public static ArrayList<Integer> getHoursBetweenDates(Date firstDate, Date secondDate)
    {
        ArrayList<Integer>totalHours=new ArrayList<>();
        int diffHours=0;
        Calendar cal = Calendar.getInstance();
        cal.setTime(firstDate);
        int hour1= cal.get(Calendar.HOUR_OF_DAY);
        cal.setTime(secondDate);
        int hour2=cal.get(Calendar.HOUR_OF_DAY);
        diffHours=hour2-hour1;
        if(diffHours>0) {
            for (int i = hour1; i <= hour2; i++)
                totalHours.add(i);
        }
        else
            totalHours.add(hour1);
        return totalHours;
    }


    public static SpannableString formatedAmount(String amountva, Context context) {

        SpannableString ss1 = new SpannableString("");
        try {

            if (amountva.contains(context.getResources().getString(R.string.one)) || amountva.contains(context.getResources().getString(R.string.two)) || amountva.contains(context.getResources().getString(R.string.three)) || amountva.contains(context.getResources().getString(R.string.four)) || amountva.contains(context.getResources().getString(R.string.five)) || amountva.contains(context.getResources().getString(R.string.six)) || amountva.contains(context.getResources().getString(R.string.seven)) || amountva.contains(context.getResources().getString(R.string.eight)) || amountva.contains(context.getResources().getString(R.string.nine)) || amountva.contains(context.getResources().getString(R.string.ten))) {
                amountva = convertBurmeseToEnglishNumber(amountva);
            }
            if (amountva.contains(".00")) {
                amountva = amountva.replace(".00", "");
            }
            if (amountva.contains(".")) {
                BigDecimal amount = new BigDecimal(amountva);
                amount = amount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
                String am = amount.toString();
                System.out.println(am);
                int index = am.indexOf(".");
                ss1 = new SpannableString(am + " " + context.getResources().getString(R.string.kyats));

                ss1.setSpan(new RelativeSizeSpan(0.8f), index, ss1.length(), 0); // set size


            } else {

                if (amountva.contains("၁") || amountva.contains("၂") || amountva.contains("၃") || amountva.contains("၄") || amountva.contains("၅") || amountva.contains("၆") || amountva.contains("၇") || amountva.contains("၈") || amountva.contains("၉") || amountva.contains("၀")) {
                    amountva = convertBurmeseToEnglishNumber(amountva);
                }

                double amount = Double.parseDouble(amountva);
                DecimalFormat formatter = new DecimalFormat("#,###");
                String am = formatter.format(amount);
                if (am.contains(".00")) {
                    am = am.replace(".00", "");
                }
                ss1 = new SpannableString(am + " " + context.getResources().getString(R.string.kyats));

            }
        } catch (Exception e) {
            e.printStackTrace();

        }

        return ss1;
    }

    public static int getHourFromDateTime(String date){
        String inputFormat = "dd-MMM-yyyy hh:mm:ss";
        Date parsed = new Date();
        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat,  Locale.ENGLISH);
        try {
            parsed  = df_input.parse(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(parsed);
        return (calendar.get(Calendar.HOUR_OF_DAY));
    }
    public static String readTextFileFromAssets(Context context, String filePath) throws IOException {
        AssetManager assetManager = context.getAssets();
        InputStream is = null;
        switch (filePath) {
            case BUSINESS_EMOJI_ICON_FILE:
                is = assetManager.open(BUSINESS_EMOJI_ICON_FILE);
                break;
            case BUSINESS_CATEGORY_FILE_MY:
                is = assetManager.open(BUSINESS_CATEGORY_FILE_MY);
                break;
            case BUSINESS_CATEGORY_FILE_ENG:
                is = assetManager.open(BUSINESS_CATEGORY_FILE_ENG);
                break;
           /* case BUSLIST_JSON:
                is = assetManager.open(BUSLIST_JSON);
                break;
            case CITYLIST_JSON:
                is = assetManager.open(CITYLIST_JSON);
                break;
            case MCC_MNC:
                is = assetManager.open(MCC_MNC);
                break;
            case MYANMARCITY:
                is = assetManager.open(MYANMARCITY);
                break;
            case TERMS:
                is = assetManager.open(TERMS);
                break;
            case TOWNSHIP_JSON:
                is = assetManager.open(TOWNSHIP_JSON);
                break;
            case TRAIN_FEE_CAL:
                is = assetManager.open(TRAIN_FEE_CAL);
                break;
            case TRAIN_STATION_NAME:
                is = assetManager.open(TRAIN_STATION_NAME);
                break;*/
            case SSL_CERTIFICATE:
                is = assetManager.open(SSL_CERTIFICATE);
                break;
            default:
                is = assetManager.open(filePath);
                break;
        }
        return streamToString(is);
    }

    public String convertArraylistToJsonString(ArrayList<ConstactModel> al_contacts)
    {
        String jsonNames=null;
        List<ConstactModel> list=al_contacts;
        Gson gson = new Gson();
        jsonNames = gson.toJson(list);
        return jsonNames;
    }

    public ArrayList<ConstactModel> getContactListFromJsonString(String jsonString)
    {
        ArrayList<ConstactModel> al_contacts=new ArrayList<ConstactModel>();
        Gson gson = new Gson();
        Type type = new TypeToken<List<ConstactModel>>() {
        }.getType();
        List<ConstactModel> list_contacts = gson.fromJson(jsonString, type);
        al_contacts.addAll(list_contacts);

        return al_contacts;
    }

    public String convertCityListArraylistToJsonString(ArrayList<CityList_Model> al_city)
    {
        String jsonNames=null;
        List<CityList_Model> list=al_city;
        Gson gson = new Gson();
        jsonNames = gson.toJson(list);
        return jsonNames;
    }

    public ArrayList<CityList_Model> getCityListFromJsonString(String jsonString)
    {
        ArrayList<CityList_Model> al_city=new ArrayList<CityList_Model>();
        Gson gson = new Gson();
        Type type = new TypeToken<List<CityList_Model>>() {
        }.getType();
        List<CityList_Model> list_contacts = gson.fromJson(jsonString, type);
        al_city.addAll(list_contacts);

        return al_city;
    }

    public String convertTownListArraylistToJsonString(ArrayList<TownShip_Model> al_town)
    {
        String jsonNames=null;
        List<TownShip_Model> list=al_town;
        Gson gson = new Gson();
        jsonNames = gson.toJson(list);
        return jsonNames;
    }

    public ArrayList<TownShip_Model> getTownListFromJsonString(String jsonString)
    {
        ArrayList<TownShip_Model> al_city=new ArrayList<TownShip_Model>();
        Gson gson = new Gson();
        Type type = new TypeToken<List<TownShip_Model>>() {
        }.getType();
        List<TownShip_Model> list_contacts = gson.fromJson(jsonString, type);
        al_city.addAll(list_contacts);

        return al_city;
    }


    public static String streamToString(InputStream is) {
        InputStreamReader isr = new InputStreamReader(is);// this class is used
        // to convert byte
        // into charater
        StringBuilder br = new StringBuilder();// modifing sequece of charater
        // for build the string
        try {
            char buf[] = new char[10000];
            int len = 0;
            while ((len = isr.read(buf)) != -1)// read byte data utill reach the
            // end location
            {
                br.append(buf, 0, len);// add byte data and converted into
                // string

            }
        } catch (Exception ee) {
            ee.printStackTrace();

        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        return br.toString();

    }

    public static CharSequence getMyanmarNumber() {
        SpannableString ss1 = new SpannableString("0");
        ss1.setSpan(new ForegroundColorSpan(R.color.secongry_text_color), 0, ss1.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        SpannableString ss2 = new SpannableString("9");
        ss2.setSpan(new ForegroundColorSpan(Color.BLACK), 0, ss2.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return TextUtils.concat(ss1, ss2);
    }

    public static String getUniversalFormattedMobileNO(Context context, String mobileNo) {
        if (!Utils.isEmpty(mobileNo)) {
            String number = "";
            if (AppPreference.getCountryCode(context).equalsIgnoreCase("+95") && mobileNo.startsWith("0"))
            {
                number = "00" + AppPreference.getCountryCode(context).substring(1) + mobileNo.substring(1);
            } else {
                number = "00" + AppPreference.getCountryCode(context).substring(1) + mobileNo;
            }
            return number;
        } else
            return "";

    }

    public static String getDeviceIMEI(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (telephonyManager.getDeviceId() != null)
            return telephonyManager.getDeviceId();
        else
            return getMacID(context);
    }

    public static String getMacID(Context context) {
        WifiManager manager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiInfo info = manager.getConnectionInfo();

        return info.getMacAddress();
    }

    public static String simSerialNumber(Context mContext) {

    try {
            TelephonyManager telephonyManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
            String simId = telephonyManager.getSimSerialNumber();
          /*  if (simId == null)
                simId = "null";
            System.out.println(simId);
*/
            return telephonyManager.getSimSerialNumber();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getSimSubscriberID(Context mContext) {
        TelephonyManager telMgr = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        return telMgr.getSubscriberId();
    }



    public static String Encrypt(String text) {

        String encryptedValue = "";
        SecretKeySpec sks = null;

        try {
            SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
            sr.setSeed("any data used as random seed".getBytes());
            KeyGenerator kg = KeyGenerator.getInstance("AES");
            kg.init(128, sr);

            sks = new SecretKeySpec((kg.generateKey()).getEncoded(), "AES");

        } catch (Exception e) {
            e.printStackTrace();
        }

        // Encode the original data with AES
        byte[] encodedBytes = null;
        try {
            Cipher c = Cipher.getInstance("AES");
            c.init(Cipher.ENCRYPT_MODE, sks);
            encodedBytes = c.doFinal(text.getBytes());
        } catch (Exception e) {
      }

        encryptedValue =
                Base64.encodeToString(encodedBytes, Base64.DEFAULT);


        return encryptedValue;
    }


    public static String getAppID(Context context) {
        return context.getPackageName();
    }

    public static String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        return inetAddress.getHostAddress().toString();
                    }
                }
            }
        } catch (Exception ex) {
            Log.e("IP Address", ex.toString());
        }
        return null;
    }



    public static String getTimeFormat() {
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        int hour = c.get(Calendar.HOUR);
        int min = c.get(Calendar.MINUTE);
        int sec = c.get(Calendar.SECOND);
        int amORPM=c.get(Calendar.AM_PM);

        month = month + 1;
        if (hour == 0) {
            hour = 24;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(year);
        if (month > 9)
            sb.append(month);
        else
            sb.append(0).append(month);
        if (day > 9)
            sb.append(day);
        else
            sb.append(0).append(day);
        if (hour > 9)
            sb.append(hour);
        else
            sb.append(0).append(hour);
        if (min > 9)
            sb.append(min);
        else
            sb.append(0).append(min);
        if (sec > 9)
            sb.append(sec);
        else
            sb.append(0).append(sec);

        return sb.toString();
    }


    public static String getSimOperater(Context mContext)
    {
        TelephonyManager telMgr = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        return telMgr.getSimOperator();

    }

    public static String getPhoneNumber(Context mContext) {
        TelephonyManager telMgr = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        String lineno=null;
        //Log.e("nois...",telMgr.getLine1Number());
        if(telMgr.getLine1Number()==null)
            lineno="";
        else
        lineno=telMgr.getLine1Number();
        Log.e("nois...",lineno);
        //return telMgr.getLine1Number();
        return lineno;
    }


    public static boolean VerifySuccessResponse(String data) {
        return data.contains("Transaction Successful");
    }

    public static String getUniversalFormattedNoWithPlus(String mobileNo) {
        if (!Utils.isEmpty(mobileNo))
            return "+" + mobileNo.substring(2);
        else
            return "";
    }


    public static String CheckinContact(Context applicationContext, String phoneNumber) {
        String res = "null";
        ContentResolver resolver = applicationContext.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
        Cursor c = resolver.query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);

        if (c != null) { // cursor not null means number is found contactsTable
            if (c.moveToFirst()) {  // so now find the contact Name
                res = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            }
            c.close();
        }
        return res;
    }
    public static String getOldNumber(String oldNum) {
        int l = oldNum.length();
        String oldNumber = "";
        for (int i = 0; i < l; i++) {
            char temp = oldNum.charAt(i);
            String tempChar = "";
            switch (temp) {
                case 'A':
                    tempChar = "" + (0 - i);
                    break;
                case 'B':
                    tempChar = "" + (1 - i);
                    break;
                case 'C':
                    tempChar = "" + (2 - i);
                    break;
                case 'D':
                    tempChar = "" + (3 - i);
                    break;
                case 'E':
                    tempChar = "" + (4 - i);
                    break;
                case 'F':
                    tempChar = "" + (5 - i);
                    break;
                case 'G':
                    tempChar = "" + (6 - i);
                    break;
                case 'H':
                    tempChar = "" + (7 - i);
                    break;
                case 'I':
                    tempChar = "" + (8 - i);
                    break;
                case 'J':
                    tempChar = "" + (9 - i);
                    break;
                case 'a':
                    tempChar = "" + (10 - i);
                    break;
                case 'b':
                    tempChar = "" + (11 - i);
                    break;
                case 'c':
                    tempChar = "" + (12 - i);
                    break;
                case 'd':
                    tempChar = "" + (13 - i);
                    break;
                case 'e':
                    tempChar = "" + (14 - i);
                    break;
                case 'f':
                    tempChar = "" + (15 - i);
                    break;
                case 'g':
                    tempChar = "" + (16 - i);
                    break;
                case 'h':
                    tempChar = "" + (17 - i);
                    break;
                case 'i':
                    tempChar = "" + (18 - i);
                    break;
                case 'j':
                    tempChar = "" + (19 - i);
                    break;
                case 'k':
                    tempChar = "" + (20 - i);
                    break;
            }
            oldNumber = oldNumber + tempChar;




        }
        return oldNumber;

    }

    public static String getNewNumber(String num) {
        int l = num.length();
        String newNumber = "";
        for (int i = 0; i < l; i++) {
            int temp = Character.getNumericValue(num.charAt(i)) + i;
            String tempChar = "";
            switch (temp) {
                case 0:
                    tempChar = "A";
                    break;
                case 1:
                    tempChar = "B";
                    break;
                case 2:
                    tempChar = "C";
                    break;
                case 3:
                    tempChar = "D";
                    break;
                case 4:
                    tempChar = "E";
                    break;
                case 5:
                    tempChar = "F";
                    break;
                case 6:
                    tempChar = "G";
                    break;
                case 7:
                    tempChar = "H";
                    break;
                case 8:
                    tempChar = "I";
                    break;
                case 9:
                    tempChar = "J";
                    break;
                case 10:
                    tempChar = "a";
                    break;
                case 11:
                    tempChar = "b";
                    break;
                case 12:
                    tempChar = "c";
                    break;
                case 13:
                    tempChar = "d";
                    break;
                case 14:
                    tempChar = "e";
                    break;
                case 15:
                    tempChar = "f";
                    break;
                case 16:
                    tempChar = "g";
                    break;
                case 17:
                    tempChar = "h";
                    break;
                case 18:
                    tempChar = "i";
                    break;
                case 19:
                    tempChar = "j";
                    break;
                case 20:
                    tempChar = "k";
                    break;
            }
            newNumber = newNumber + tempChar;

        }
        return newNumber;
    }

    public static String getEncryptionValue(Context mContext) {
        try {
            String ApplicationID = AppPreference.getSimId(mContext) + Utils.getDeviceIMEI(mContext) + Utils.getSimSubscriberID(mContext);
            String encypted = AESCrypt.encrypt(Constant.DEFAULT_INCRYPTION_PASSWORD, ApplicationID);
            return Encrypt(encypted);
        } catch (Exception e) {

            e.printStackTrace();
            return "";
        }

    }


    public static boolean isReallyConnectedToInternet(Context _context) {
        ConnectivityManager connectivity = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }

        }

        return false;
    }

    public static SpannableString formatedAmount_t(String amountva, Context context) {

        try {
            if (amountva.contains("၁") || amountva.contains("၂") || amountva.contains("၃") || amountva.contains("၄") || amountva.contains("၅") || amountva.contains("၆") || amountva.contains("၇") || amountva.contains("၈") || amountva.contains("၉") || amountva.contains("၀")) {
                amountva = convertBurmeseToEnglishNumber(amountva);
            }
            BigDecimal amount = new BigDecimal(amountva);
            amount = amount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
            String am = amount.toString();
            if (am.contains(".00")) {
                am = am.replace(".00", "");
            }
            if (am.contains(".")) {
                System.out.println(am);
                int index = am.indexOf(".");
                SpannableString ss1 = new SpannableString(am);
                ss1.setSpan(new RelativeSizeSpan(0.8f), index, am.length(), 0); // set size
                System.out.println("ss1--" + ss1);

                return ss1;
            } else {
                System.out.println(am);
                if (am.contains(".00")) {
                    am = am.replace(".00", "");
                }
                SpannableString ss1 = new SpannableString(am);
                return ss1;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new SpannableString("");
        }

    }

    public static String getUniversalFormattedMobileNOWithCountryCode(String countryCode, String mobileNo) {
        if (!Utils.isEmpty(mobileNo)) {
            String number;
            if (countryCode.equalsIgnoreCase("+95") && mobileNo.startsWith("0")) {
                number = countryCode + mobileNo.substring(1);
            } else {
                number = countryCode + mobileNo;
            }
            return number;
        } else
            return "";
    }

    public static boolean isValidForAutoFire(int lenght, String countryCode, String operatorName) {
        if (lenght == 10 && countryCode.equals("+95") && prevLenght < lenght) {
            prevLenght = lenght;
            return true;
        } else if (lenght == 9 && countryCode.equals("+95") && prevLenght < lenght && operatorName.contains("CDMA")) {
            prevLenght = lenght;
            return true;
        } else if (lenght == 8 && countryCode.equals("+65") && prevLenght < lenght) {        //singapore
            prevLenght = lenght;
            return true;
        } else if (lenght == 9 && countryCode.equals("+66") && prevLenght < lenght) {        //Thailand
            prevLenght = lenght;
            return true;
        } else if (lenght == 9 && countryCode.equals("+60") && prevLenght < lenght) {        // Malaysia
            prevLenght = lenght;
            return true;
        } else if (lenght == 10 && countryCode.equals("+91") && prevLenght < lenght) {        //India
            prevLenght = lenght;
            return true;
        } else if (lenght == 11 && countryCode.equals("+86") && prevLenght < lenght) {        //China
            prevLenght = lenght;
            return true;
        } else if (lenght == 10 && countryCode.equals("+1") && prevLenght < lenght) {        //USA
            prevLenght = lenght;
            return true;
        }
        prevLenght = lenght;
        return false;
    }

    public static String readHTMLFile(Context context) {
        String json;
        json = "";
        try {
            InputStream is;
            is = context.getAssets().open("terms.html");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException e) {
        }
        return json.toString();
    }

    public static String[] getAddressFromGPSLocation(Context context, Double lat, Double lang){
        Geocoder geocoder;
        List<Address> address;
        geocoder = new Geocoder(context, Locale.getDefault());
        try {
            String address0="";
            address = geocoder.getFromLocation(lat, lang, 3); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            for(int i=0; i<address.get(0).getMaxAddressLineIndex(); i++){
                address0 = address0+" "+address.get(0).getAddressLine(i) ; // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            }
            String city = address.get(0).getLocality();
            String state = address.get(0).getAdminArea();
            String postalCode = address.get(0).getPostalCode();
            return new String[]{address0, city, state, postalCode};
        }catch (Exception e){
            return new String[]{};
        }
    }

    public static String getUniversalFormattedNoWithoutPlus(String mobileNo) {
        if (!Utils.isEmpty(mobileNo))
            return "00" + mobileNo.substring(1);
        else
            return "";
    }

    public static boolean mobileNumberValidation(String number) {

        return number.length() > 5 && number.length() <= 17;
    }

    public static long getDaysBetweenDates(Date firstDate, Date secondDate)
    {
        long diffDays=-1;
        //if(checkValidDate(secondDate) && checkValidDate(firstDate))
        try {
            //in milliseconds
            long diff = secondDate.getTime() - firstDate.getTime();
            diffDays = diff / (24 * 60 * 60 * 1000);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return diffDays;
    }

    public static int getSimState(Context mContext) {
        int isSim = 0;
        TelephonyManager telMgr = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        int state = telMgr.getSimState();
        switch (state) {
            case TelephonyManager.SIM_STATE_ABSENT:
                isSim = 0;
                break;
            case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
                break;
            case TelephonyManager.SIM_STATE_PIN_REQUIRED:
                break;
            case TelephonyManager.SIM_STATE_PUK_REQUIRED:
                break;
            case TelephonyManager.SIM_STATE_READY:
                isSim = 1;
                // do something
                break;
            case TelephonyManager.SIM_STATE_UNKNOWN:
                break;
        }
        return isSim;
    }

    public static boolean isValidSIm(Context context)
    {
        DBHelper db= new DBHelper(context);
        if(!db.getSimId().equals(""))
        {
            if(db.getSimId().equals(simSerialNumber(context)))
            {
                return  true;
            }
            else
            {
                return  false;
            }
        }
        else
        {
            return true;
        }
    }

    public static int getMinutesFromDateTime(String date) {
        String inputFormat = "dd-MMM-yyyy HH:mm:ss";
        Date parsed = new Date();
        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, Locale.ENGLISH);
        try {
            parsed = df_input.parse(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(parsed);
        return (calendar.get(Calendar.MINUTE));
    }



    public static void showCustomToastMsg(Context mContext, int msg, Boolean status) {

        LayoutInflater inflater = LayoutInflater.from(mContext);
        View layout = inflater.inflate(R.layout.toast_layout, null);
        TextView textAlrt = (TextView) layout.findViewById(R.id.alertText);
        LinearLayout layoutToast = (LinearLayout) layout.findViewById(R.id.layoutToast);
        CustomTextView textMsg = (CustomTextView) layout.findViewById(R.id.alertMsg);
        if (status) {
            layoutToast.setBackgroundResource(R.drawable.custom_layout_bakn);
            //textMsg.setTextColor(mContext.getResources().getColor(R.color.AppColor_blue));
            textAlrt.setText(infoIcon);
        } else {
            layoutToast.setBackgroundResource(R.drawable.custom_layout_bak);
            //textMsg.setTextColor(mContext.getResources().getColor(R.color.white));
            textAlrt.setText(alertIcon);
        }
        textMsg.setText(msg);
        textMsg.setTypeface(getFont(mContext), Typeface.BOLD);
        Toast customtoast = new Toast(mContext);
        customtoast.setView(layout);
        customtoast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL, 0, 0);
        customtoast.setDuration(Toast.LENGTH_SHORT);
        customtoast.show();
    }




   /* public static boolean isConnectedToInternet(Context _context) {

        return getInternetOn_at_login(_context);
    }*/

    public static boolean isConnectedToInternet(Context _context) {
        return getInternetOn_at_login(_context);
    }


    public static SpannableString formatedAmountAbhi(String amountva, Context context) {

        SpannableString ss1 = new SpannableString("");
        try {

            if (amountva.contains(context.getResources().getString(R.string.one)) || amountva.contains(context.getResources().getString(R.string.two)) || amountva.contains(context.getResources().getString(R.string.three)) || amountva.contains(context.getResources().getString(R.string.four)) || amountva.contains(context.getResources().getString(R.string.five)) || amountva.contains(context.getResources().getString(R.string.six)) || amountva.contains(context.getResources().getString(R.string.seven)) || amountva.contains(context.getResources().getString(R.string.eight)) || amountva.contains(context.getResources().getString(R.string.nine)) || amountva.contains(context.getResources().getString(R.string.ten))) {
                amountva = convertBurmeseToEnglishNumber(amountva);
            }
            BigDecimal roundoff = new BigDecimal(amountva);
            roundoff = roundoff.setScale(2, BigDecimal.ROUND_HALF_EVEN);
            amountva = roundoff.toString();
            if (amountva.contains(".00")) {
                amountva = amountva.replace(".00", "");
            }
            if (amountva.contains(".")) {
                BigDecimal amount = new BigDecimal(amountva);
                amount = amount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
                DecimalFormat formatter = new DecimalFormat("#,###.00");
                String amounttest = formatter.format(amount);
                String am = amounttest.toString();
                System.out.println(am);
                int index = am.indexOf(".");
                am = convertBurmeseToEnglishNumber(am);
                ss1 = new SpannableString(am);
                ss1.setSpan(new RelativeSizeSpan(0.8f), index, ss1.length(), 0); // set size


            } else {

                if (amountva.contains("၁") || amountva.contains("၂") || amountva.contains("၃") || amountva.contains("၄") || amountva.contains("၅") || amountva.contains("၆") || amountva.contains("၇") || amountva.contains("၈") || amountva.contains("၉") || amountva.contains("၀")) {
                    amountva = convertBurmeseToEnglishNumber(amountva);
                }

                double amount = Double.parseDouble(amountva);
                DecimalFormat formatter = new DecimalFormat("#,###");
                String am = formatter.format(amount);
                if (am.contains(".00")) {
                    am = am.replace(".00", "");
                }
                am = convertBurmeseToEnglishNumber(am);
                ss1 = new SpannableString(am);

            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        if (ss1.toString().startsWith(".")) {
            SpannableString sp = new SpannableString("0");
            SpannableString sp1 = new SpannableString(TextUtils.concat(sp, ss1));
            return sp1;
        } else {
            return ss1;
        }
    }

    public static void openCallDialer(String mobileNo, Context context) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + mobileNo));
        context.startActivity(intent);
    }

    public static void setEditTextFilter(EditText editText){
        InputFilter filter = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++) {
                    String character= String.valueOf(source.charAt(i));
                    if (character.equalsIgnoreCase("%")  || character.equalsIgnoreCase("#") || character.equalsIgnoreCase("&") || character.equalsIgnoreCase("\\") || character.equalsIgnoreCase(";")) {
                        return "";
                    }
                }
                return null;
            }
        };
        editText.setFilters(new InputFilter[]{filter});
    }

    public static String getViberNumber(Context context) {
        AccountManager am = AccountManager.get(context);
        Account[] accounts = am.getAccounts();
        String acname="";
        for (Account ac : accounts) {
            acname = ac.name;
            String actype = ac.type;
            if (actype.equals("com.viber.voip") && acname.startsWith(AppPreference.getCountryCode(context))) {
                break;
            }else{
                acname="";
            }
        }
        return acname;
    }

    public static String getDefaultEmail(Context context){
        String acname="";
        AccountManager am = AccountManager.get(context);
        Account[] accounts = am.getAccounts();
        for (Account ac : accounts) {
            acname = ac.name;
            String actype = ac.type;
            if (actype.equals("com.google") && acname.length()>0){
                break;
            }else{
                acname="";
            }
        }
        return acname;
    }

    public static void setUnderLine(String msg, TextView textView){
        SpannableString content = new SpannableString(msg);
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        content.setSpan(new ForegroundColorSpan(R.color.green_trans), 0, content.length(), 0);
        textView.setText(content);
        textView.setTextSize(20);
    }
    public static void openEmail(Context context){
        final Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{context.getResources().getString(R.string.email)});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, context.getResources().getString(R.string.customer_care_feedback));
        //emailIntent.putExtra(Intent.EXTRA_TEXT, "Some body");
        context.startActivity(Intent.createChooser(emailIntent, "Send E-mail"));
    }

    public String convertLaterModelToJsonString(LaterModel model)
    {
        String jsonNames=null;
        LaterModel late_model=model;

        Gson gson = new Gson();
        jsonNames = gson.toJson(late_model);

        return jsonNames;
    }

    public LaterModel getLaterModelFromJson(String jsonString)
    {
        LaterModel model=null;
        Gson gson = new Gson();
        Type type = new TypeToken<LaterModel>() {
        }.getType();

        model=gson.fromJson(jsonString, type);

        return model;
    }

    public static String getNameFromatedString(String name){

        if(name.contains("|"))
        {
            name=name.replace("|", "\n");
            return name;
        }else
            return name;
    }

    public static int getCellId(Context mContext) {
        try {
            TelephonyManager tm = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
            GsmCellLocation location = (GsmCellLocation) tm.getCellLocation();
            return location.getCid();
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public final static boolean isValidEmail(String target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

 public   ConstactModel  getSelectedContactModel(String number,List<ConstactModel> alist)
    {
        ConstactModel model=new ConstactModel();
        try {

            number = number.substring(3);
            for (int i = 0; i < alist.size(); i++) {

                Log.e("receivednumber", "..." + number + "...modelno" + "...." + model.getMobileNo());

        if(!alist.get(i).getMobileNo().equals(""))
        {
            if (alist.get(i).getMobileNo().startsWith(number)) {
                model = alist.get(i);
                Log.e("matchnumber", "...mno" + model.getMobileNo().startsWith(number) + "...rno" + number);
                return model;
            } else if (alist.get(i).getMobileNo().substring(1).startsWith(number)) {
                model = alist.get(i);
                Log.e("matchnumber", "...mno" + model.getMobileNo().startsWith(number) + "...rno" + number);
                return model;
            } else if (alist.get(i).getMobileNo().substring(2).startsWith(number)) {
                model = alist.get(i);
                Log.e("matchnumber", "...mno" + model.getMobileNo().startsWith(number) + "...rno" + number);
                return model;
            } else if (alist.get(i).getMobileNo().substring(3).startsWith(number)) {
                model = alist.get(i);
                Log.e("matchnumber", "...mno" + model.getMobileNo().startsWith(number) + "...rno" + number);
                return model;
            } else if (alist.get(i).getMobileNo().substring(4).startsWith(number)) {
                model = alist.get(i);
                Log.e("matchnumber", "...mno" + model.getMobileNo().startsWith(number) + "...rno" + number);
                return model;
            } else if (alist.get(i).getMobileNo().substring(5).startsWith(number)) {
                model = alist.get(i);
                Log.e("matchnumber", "...mno" + model.getMobileNo().startsWith(number) + "...rno" + number);
                return model;
            }

            if (i == alist.size() - 1) {
                return model;
            }
        }

            }
        }
        catch(Exception e)
        {
            model=new ConstactModel();
            e.printStackTrace();
        }

        return model;
    }

    @SuppressWarnings("deprecation")
    public static void showAlert(final Context context, String msg) {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.show_message_registration);
        CustomTextView userInput = (CustomTextView) dialog.findViewById(R.id.dialog_editext);
        CustomTextView tvOK = (CustomTextView) dialog.findViewById(R.id.tv_ok);
        userInput.setText(msg);
        userInput.setTypeface(getFont(context), Typeface.BOLD);
        dialog.setCancelable(true);
        tvOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public static String getUniversalFormattedMobileNOLogin(Context context, String mobileNo) {
        if (!Utils.isEmpty(mobileNo)) {
            String number = "";
            if (AppPreference.getCountryCodeLogin(context).trim().equalsIgnoreCase("+95") && mobileNo.trim().startsWith("0")) {
                number = "00" + AppPreference.getCountryCodeLogin(context).substring(1) + mobileNo.substring(1);
            } else {
                number = "00" + AppPreference.getCountryCodeLogin(context).substring(1) + mobileNo;
            }
            return number;
        } else
            return "";

    }

    public static void playAudio(Context context, int audioFile) {
        ((Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE)).vibrate(500);
        MediaPlayer p = MediaPlayer.create(context, audioFile);
        if (!p.isPlaying())
            p.start();
    }

    public static String getAndroidDeviceUniqueID(Context context){
        try {
            return Settings.Secure.getString(context.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
        }catch (Exception e){
            return "";
        }
    }
    public static String getPhoneModel(){
        try {
            return Build.MODEL;
        }catch (Exception e){
            return "";
        }
    }

    public static String getPhoneBrand(){
        try {
            return Build.BRAND;
        }catch (Exception e){
            return "";
        }
    }
    public static String getOSVersion() {
        try {
            return Build.VERSION.RELEASE;
        } catch (Exception e) {
            return "";
        }
    }

    public static String getVersionCode(Context context) {
        String version = "";
        try {
            PackageManager pm = context.getPackageManager();
            PackageInfo pi = pm.getPackageInfo(context.getPackageName(), 0);
            version = String.valueOf(pi.versionCode);
            return version;
        } catch (Exception e) {
            return "";
        }
    }


    public static boolean getAge(String dateOfBirth) {

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, -18);
        SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
        String dateCurrent = format.format(calendar.getTime());
        Date d1;
        Date d2;

        try {
            d1 = format.parse(dateCurrent);
            d2 = format.parse(dateOfBirth);
            long diff = d1.getTime() - d2.getTime();
            long diffDays = diff / (24 * 60 * 60 * 1000);
            if (diffDays >= 1) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }

    public static String readHTMLFile(Context context, String file) {
        String json;
        json = "";
        try {
            InputStream is;
            is = context.getAssets().open(file);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
            Log.e("Terms and condiation", json);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return json.toString();
    }

    public static void addNumberInContact(Context context, String displayName, String mobileNo, String homeNumber, String mobileNumber2, Bitmap mBitmap) {
        ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();

        ops.add(ContentProviderOperation.newInsert(
                ContactsContract.RawContacts.CONTENT_URI)
                .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
                .build());
        if (displayName != null) {
            ops.add(ContentProviderOperation.newInsert(
                    ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                    .withValue(ContactsContract.Data.MIMETYPE,
                            ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                    .withValue(
                            ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME,
                            displayName).build());

        }

        if (mobileNo != null) {
            ops.add(ContentProviderOperation.
                    newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                    .withValue(ContactsContract.Data.MIMETYPE,
                            ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, mobileNo)
                    .withValue(ContactsContract.CommonDataKinds.Phone.TYPE,
                            ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
                    .build());
        }

        if (homeNumber != null) {

            ops.add(ContentProviderOperation
                    .newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(
                            ContactsContract.Data.RAW_CONTACT_ID, 0)
                    .withValue(
                            ContactsContract.Data.MIMETYPE,
                            ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER,
                            homeNumber)
                    .withValue(ContactsContract.CommonDataKinds.Phone.TYPE,
                            ContactsContract.CommonDataKinds.Phone.TYPE_HOME)
                    .build());
        }
        if (mobileNumber2 != null) {

            ops.add(ContentProviderOperation.
                    newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                    .withValue(ContactsContract.Data.MIMETYPE,
                            ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, mobileNumber2)
                    .withValue(ContactsContract.CommonDataKinds.Phone.TYPE,
                            ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
                    .build());
        }


        if (mBitmap != null) {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            mBitmap.compress(Bitmap.CompressFormat.PNG, 75, stream);
            ops.add(ContentProviderOperation
                    .newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(
                            ContactsContract.Data.RAW_CONTACT_ID, 0)
                    .withValue(
                            ContactsContract.Data.MIMETYPE,
                            ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Photo.PHOTO, stream.toByteArray())
                    .build());
        }


        try {
            context.getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
            if (mBitmap == null)
                showToast(context, context.getResources().getString(R.string.add_contact_msg));
        } catch (Exception e) {
            e.printStackTrace();
            showToast(context, e.toString());
        }
    }

    public boolean insertContact(ContentResolver contactAdder, String firstName, String mobileNumber)
    {
        ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
        ops.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI).withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null).withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null).build());
        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0).withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE).withValue(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME, firstName).build());
        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0).withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE).withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, mobileNumber).withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE).build());
        try {
            contactAdder.applyBatch(ContactsContract.AUTHORITY, ops);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public void addContactDialog(final Context context,String mobileNo, final AllInerfaces.CallFinishInterface cInterface,final AllInerfaces.CallSplashInterface callSplash,final int from)
    {

        final Dialog dialog=new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(450, 450);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setBackgroundDrawableResource(R.color.white);
        dialog.setContentView(R.layout.custom_contact_adder_dialog);
        dialog.setTitle(R.string.contactadd_business_name);

        this.cInterface=cInterface;
        this.callSplash=callSplash;


        CustomTextView ctv_mobileNo=(CustomTextView)dialog.findViewById(R.id.tv_contactadd_mobileno);
        final CustomEdittext edt_businesName=(CustomEdittext)dialog.findViewById(R.id.edt_contactadd_businessname);
        CustomButton btn_ok=(CustomButton)dialog.findViewById(R.id.btn_contactadd_ok);
        CustomButton btn_cancel=(CustomButton)dialog.findViewById(R.id.btn_contactadd_cancel);

        Log.e("notcontactNobefore","..."+mobileNo);

        if(mobileNo.startsWith("0095"))
            contactNo="0"+mobileNo.substring(4);
        ctv_mobileNo.setText(contactNo);

        Log.e("notcontactNo","..."+contactNo);

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = edt_businesName.getText().toString().trim();

                if (!name.equalsIgnoreCase("")) {
                    insertContact(context.getContentResolver(), name, contactNo);
                    dialog.dismiss();

                    if(from==1) {
                        readForJsonContacts(context);
                        cInterface.callFinish();
                    }
                    else
                        callSplash.callSplash();

                } else {
                    String print=context.getString(R.string.contactadd_name_missing);
                    showToast(context, print);
                }
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if(from==1)
                    cInterface.callFinish();
                else
                    callSplash.callSplash();

            }
        });

        dialog.show();

    }


    public void addRemarksDialog(final Context context, final Remarks model, final AllInerfaces.CallRemarkInterface callRemark)
    {

        final Dialog dialog=new Dialog(context);
        this.callRemarkInterface=callRemark;
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_remarks);

        dialog.getWindow().setLayout(1200, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setBackgroundDrawableResource(R.color.white);

        dialog.setTitle(R.string.remarks);


        final CustomEdittext edt_businesName=(CustomEdittext)dialog.findViewById(R.id.edt_remarks);
        CustomButton btn_ok=(CustomButton)dialog.findViewById(R.id.btn_remaks_ok);
        CustomButton btn_cancel=(CustomButton)dialog.findViewById(R.id.btn_remarks_cancel);

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = edt_businesName.getText().toString().trim();
                if (!name.equalsIgnoreCase("")) {
                    DBHelper db = new DBHelper(context);
                    db.open();
                    model.setComments(edt_businesName.getText().toString().trim());
                    db.insertRemarkDetails(model);
                    db.close();
                    dialog.dismiss();
                    callRemark.callRemarks();
                } else
                    showToast(context, "Please Enter Remarks");

            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();


            }
        });

        dialog.show();
        Window window = dialog.getWindow();
      //  window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

    }

    public String getLACAndCID(Context context) {
        TelephonyManager telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (telephony.getPhoneType() == TelephonyManager.PHONE_TYPE_GSM)
        {
            GsmCellLocation location = (GsmCellLocation) telephony.getCellLocation();
            if (location != null) {
                return String.valueOf(location.getCid());
                //return null;
            }
        }
        if (telephony.getPhoneType() == TelephonyManager.PHONE_TYPE_CDMA) {
            CdmaCellLocation location = (CdmaCellLocation) telephony.getCellLocation();
            if (location != null) {
                return String.valueOf(location.getBaseStationId());
                //return null;
            }
        }
        return "";
    }

    public  void readForJsonContacts(Context con) {
        ConstactModel model = null;
        Cursor phones = con.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
        Log.d("Contact new size", "" + phones.getCount());

        ArrayList<ConstactModel> list_jsonContacts=new ArrayList<ConstactModel>();
        while (phones.moveToNext())
        {
            String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            Log.e("dispname","..."+name);
            if (name != null)
            {
                String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                Log.e("dispno","..."+phoneNumber);

                String contactUri = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));

                String id = phones.getString(phones.getColumnIndex(ContactsContract.Contacts._ID));
                String email="";

                if (phoneNumber.contains(" "))
                    phoneNumber = phoneNumber.replace(" ", "");
                if (phoneNumber.contains("-"))
                    phoneNumber = phoneNumber.replace("-", "");
                if (contactUri == null)
                    contactUri = "";

                model=new ConstactModel(name,phoneNumber,email,contactUri,id,"false");
                Log.e("mPhno","...."+model.getMobileNo());
                list_jsonContacts.add(model);
            }

        }
        //Toast.makeText(this,"size is.."+list_jsonContacts.size(),Toast.LENGTH_SHORT).show();
        String jsonContacts = new Utils().convertArraylistToJsonString(list_jsonContacts);

        if(DBHelper.getJsonContacts()==null)
            DBHelper.insertJsonContact(jsonContacts);
        else {
            DBHelper.deleteJsonContacts();
            DBHelper.insertJsonContact(jsonContacts);
        }
        phones.close();

    }

    public UserInctivityModel getUserInctivityModel(Context context,String cashierId,String duration)
    {
        UserInctivityModel model=null;
        String mcc=
                "",mnc="";
        GPSTracker gps=new GPSTracker(context);
        Location loc= gps.getLocation();
        double lat=0.0;
        double longi=0.0;
        if(loc!=null)
        {
            lat = loc.getLatitude();
            longi = loc.getLongitude();
        }
        String cellId=new Utils().getLACAndCID(context);

        TelephonyManager telephonyMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String networkOperator = telephonyMgr.getNetworkOperator();
        String operator=telephonyMgr.getNetworkOperatorName();


        if (networkOperator != null) {
            mcc = "" + Integer.parseInt(networkOperator.substring(0, 3));
            mnc = "" + Integer.parseInt(networkOperator.substring(3));
        }

        if(cashierId!=null)
        {
            if(!cashierId.equals(MASTER_TAG))
            {
                DBHelper db=new DBHelper(context);
                db.open();
                CashierModel cm=db.getCashierModel(cashierId);
                db.close();

              model=new UserInctivityModel(cashierId,cm.getCashier_Number(),cm.getCashier_Name(),cellId,lat,longi,mcc,mnc,operator,duration);
            }
        }
    return model;
    }


    public static String randomGeneratorNumber(){
        long min = 10000L;
        long max = 999999999999999L;

        int min2 = 5;
        int max2 = 16;
        /*Random r = new Random();
        long number = x+((long)(r.nextDouble()*(y-x)));
        return number;*/
        String msg= min + (long) (Math.random() * (max - min))+"";
        int random= min2 + (int) (Math.random() * (max2 - min2));
        if (msg.length()<random)
            return msg;
        else
            return msg.substring(0, random);

    }

}
