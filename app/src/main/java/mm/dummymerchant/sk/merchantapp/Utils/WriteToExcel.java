package mm.dummymerchant.sk.merchantapp.Utils;

import android.content.Context;
import android.database.Cursor;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.opencsv.CSVWriter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

import mm.dummymerchant.sk.merchantapp.db.DBHelper;
import mm.dummymerchant.sk.merchantapp.model.CashierModel;
import mm.dummymerchant.sk.merchantapp.model.TransationModel;

/**
 * Created by Dell on 11/25/2015.
 */
public class WriteToExcel implements Constant{

    DBHelper db=null;
    File exportDir;
    File file;
    public  void convertToExcelFile(Context con)
    {
         db=new DBHelper(con);
        db.open();

        writeTransactionFile();
        writeReceivedMoneyFile();
        writeToActivityDetailsFile();
        writeCashCollectorFile();
        writeCashierTable();

        db.close();
    }


    void writeTransactionFile()
    {
        exportDir = new File(Environment.getExternalStorageDirectory(), "");

        if (!exportDir.exists())
        { exportDir.mkdirs(); }

        file = new File(exportDir, DBHelper.EXCEL_TRANSACTION_FILE_NAME);
        try {
            file.createNewFile();
            CSVWriter csvWrite = new CSVWriter(new FileWriter(file));
            Cursor curCSV = db.getTransationDetailsCursor();
            if(curCSV!=null)
            {
                csvWrite.writeNext(curCSV.getColumnNames());
                while (curCSV.moveToNext()) {
                    String arrStr[] = {curCSV.getString(0), curCSV.getString(1), curCSV.getString(2),
                            curCSV.getString(3), curCSV.getString(4), curCSV.getString(5), curCSV.getString(6),
                            curCSV.getString(7), curCSV.getString(8), curCSV.getString(9), curCSV.getString(10),
                            curCSV.getString(11), curCSV.getString(12),curCSV.getString(13),curCSV.getString(14),curCSV.getString(15),curCSV.getString(16),curCSV.getString(17),curCSV.getString(18),curCSV.getString(19),curCSV.getString(20),curCSV.getString(21),curCSV.getString(22),curCSV.getString(23),curCSV.getString(24),curCSV.getString(25),curCSV.getString(26),curCSV.getString(27),curCSV.getString(28),curCSV.getString(29),curCSV.getString(30),curCSV.getString(31),curCSV.getString(32)};
                    csvWrite.writeNext(arrStr);
                }
                csvWrite.close();
                curCSV.close();
            }

        } catch (IOException e) {
            Log.e("MainActivity", e.getMessage(), e);

        }
    }


    void writeReceivedMoneyFile()
    {
        exportDir = new File(Environment.getExternalStorageDirectory(), "");

        if (!exportDir.exists())
        { exportDir.mkdirs(); }

        file = new File(exportDir, DBHelper.EXCEL_RECEIVED_MONEY_FILE_NAME);
        try {
            file.createNewFile();
            CSVWriter csvWrite = new CSVWriter(new FileWriter(file));
            Cursor curCSV = db.getReceivedMoneyCursor();
            if(curCSV!=null)
            {
                csvWrite.writeNext(curCSV.getColumnNames());
                while (curCSV.moveToNext()) {
                    String arrStr[] = {curCSV.getString(0), curCSV.getString(1), curCSV.getString(2),
                            curCSV.getString(3), curCSV.getString(4), curCSV.getString(5), curCSV.getString(6),
                            curCSV.getString(7), curCSV.getString(8), curCSV.getString(9), curCSV.getString(10),
                            curCSV.getString(11), curCSV.getString(12),curCSV.getString(13),curCSV.getString(14),curCSV.getString(15),curCSV.getString(16),curCSV.getString(17),curCSV.getString(18),curCSV.getString(19),curCSV.getString(20),curCSV.getString(21),curCSV.getString(22),curCSV.getString(23),curCSV.getString(24),curCSV.getString(25), curCSV.getString(26),curCSV.getString(27),curCSV.getString(28),curCSV.getString(29),curCSV.getString(30),curCSV.getString(31),curCSV.getString(32)};
                    csvWrite.writeNext(arrStr);
                }
                csvWrite.close();
                curCSV.close();
            }

        } catch (IOException e) {
            Log.e("MainActivity", e.getMessage(), e);

        }
    }

    void writeToActivityDetailsFile() {

        exportDir = new File(Environment.getExternalStorageDirectory(), "");

        if (!exportDir.exists()) {
            exportDir.mkdirs();
        }

        file = new File(exportDir, DBHelper.EXCEL_ACTIVITY_LOG_DETAILS);
        try {
            file.createNewFile();
            CSVWriter csvWrite = new CSVWriter(new FileWriter(file));
            Cursor curCSV = db.getActivityDetailsCursor();
            if (curCSV != null)
            {
                csvWrite.writeNext(curCSV.getColumnNames());
                while (curCSV.moveToNext()) {
                    String arrStr[] = {curCSV.getString(0), curCSV.getString(1), curCSV.getString(2),
                            curCSV.getString(3), curCSV.getString(4), curCSV.getString(5), curCSV.getString(6),
                            curCSV.getString(7)};
                    csvWrite.writeNext(arrStr);
                }
                csvWrite.close();
                curCSV.close();
            }

        } catch (IOException e) {
            Log.e("MainActivity", e.getMessage(), e);

        }
    }

    void writeCashCollectorFile()
    {
        exportDir = new File(Environment.getExternalStorageDirectory(), "");

        if (!exportDir.exists()) {
            exportDir.mkdirs();
        }

        file = new File(exportDir, DBHelper.EXCEL_CASH_COLLECTOR);
        try {
            file.createNewFile();
            CSVWriter csvWrite = new CSVWriter(new FileWriter(file));
            Cursor curCSV = db.getCashCollector_Cursor();
            if (curCSV != null)
            {
                csvWrite.writeNext(curCSV.getColumnNames());
                curCSV.moveToFirst();
                do
               {
                    String arrStr[] = {curCSV.getString(0),"", curCSV.getString(2),
                            curCSV.getString(3), curCSV.getString(4),""};
                    Log.e("checkcursor",curCSV.getString(2));
                    csvWrite.writeNext(arrStr);
                } while (curCSV.moveToNext());
                csvWrite.close();
                curCSV.close();
            }

        } catch (IOException e) {
            Log.e("MainActivity", e.getMessage(), e);

        }
    }

    void writeCashierTable()
    {
        exportDir = new File(Environment.getExternalStorageDirectory(), "");

        if (!exportDir.exists()) {
            exportDir.mkdirs();
        }

        file = new File(exportDir, DBHelper.EXCEL_CASHIER_TABLE);
        try {
            file.createNewFile();
            CSVWriter csvWrite = new CSVWriter(new FileWriter(file));
            Cursor curCSV = db.getAllCashiersCursor();
            if (curCSV != null)
            {
                csvWrite.writeNext(curCSV.getColumnNames());
                while (curCSV.moveToNext()) {
                    String arrStr[] = {curCSV.getString(0), curCSV.getString(1), curCSV.getString(2),
                            curCSV.getString(3), curCSV.getString(4), curCSV.getString(5),curCSV.getString(6)};
                    csvWrite.writeNext(arrStr);
                }
                csvWrite.close();
                curCSV.close();
            }

        } catch (IOException e) {
            Log.e("MainActivity", e.getMessage(), e);

        }
    }

    //converting Excel File to SqliteDB

/*   public void convertReceivedMoneyExcelTODb(Context con)
    {
        db=new DBHelper(con);
        db.open();

        File file=null;

        exportDir = new File(Environment.getExternalStorageDirectory(), "");

        if(exportDir.exists())
        {
            file= new File(exportDir, DBHelper.EXCEL_RECEIVED_MONEY_FILE_NAME);

            if(file.exists())
            {
                //Toast.makeText(con, "received moneyfile exists", Toast.LENGTH_LONG).show();
                String path=file.getAbsolutePath();
                FileReader filereader = null;
                try {
                    filereader = new FileReader(path);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                BufferedReader buffer = new BufferedReader(filereader);

                String line = "";

                try {

                    while ((line = buffer.readLine()) != null)
                    {
                        String[] str = line.split(",", 34);
                        TransationModel model=null;

                        //getting all values

                        String _id=str[0];
                        String transid=str[1];
                        String description=str[2];
                        String oldamount=str[3];
                        String newAmount=str[4];
                        String istrans=str[5];
                        String date=str[6];
                        String destination=str[7];
                        String tType=str[8];
                        String carNo=str[9];
                        String message=str[10];
                        String fee=str[11];
                        String agentname=str[12];
                        String agentnumber=str[13];
                        String amount=str[14];
                        String servicetax=str[15];
                        String read=str[16];
                        String count=str[17];
                        String comment=str[18];
                        String name=str[19];
                        String lat_long=str[20];
                        String imagePath=str[21];
                        String survey=str[22];
                        String walletbalance=str[23];
                        String transctionDate=str[24];
                        String hours=str[25];
                        String city_code=str[26];
                        String Gender=str[27];
                        String age=str[28];
                        String cashier_id=str[29];
                        String cashier_name=str[30];
                        String cashier_number=str[31];
                        String cellid=str[32];

                        String open="";
                        String resultcode="";
                        String responsects="23-Nov-2015 11:12:12";
                        String loyaltypoints="";
                        String merchantname="";
                        String uniqueId="";

                        String source=agentnumber;
                        String balance=newAmount;
                        String dbAmount=amount;
                        String destinationPersonName=name;

                    // long l_trans=   Long.parseLong(transctionDate);
                        Date d_trans=new Date();


                      //(String descripton, String transatId, String newamount, String serviceTax, String open, String fee, String name, String destination, String trnastionTypetype, String date, String source, String oldAmount, String amount, String resultcode, String isCredit, String comments, String responsects, String balance, String count, String dbAmount, String loyalty_ponits, String merchantName, String destinationPersonName, String photoUrl, String cellID, String latLong, Date mTranscationDate, String mSurvery, String mWalletbalance, String cashierId, String cashierName, String cashierNumber, String city, String age, String gender, String hours, String unique_id)
                        model=new TransationModel(description,transid,newAmount,servicetax,read,fee,name,destination,tType,date,source,oldamount,amount,resultcode,istrans,comment,responsects,balance,count,dbAmount,loyaltypoints,merchantname,destinationPersonName,imagePath,cellid,lat_long,d_trans,survey,walletbalance,cashier_id,cashier_name,cashier_number,city_code,age,Gender,hours,uniqueId);

                        db.insertRECEIVEDTransacation(model);

                        Toast.makeText(con,"written to db",Toast.LENGTH_LONG).show();


                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else
                Toast.makeText(con,"received moneyfile not exists",Toast.LENGTH_LONG).show();
        }

        db.close();
    }

    public void convertCashierExcelToDB(Context con)
    {
        db=new DBHelper(con);
        db.open();

        File file=null;

        exportDir = new File(Environment.getExternalStorageDirectory(), "");

        if(exportDir.exists())
        {
            file = new File(exportDir, DBHelper.EXCEL_CASHIER_TABLE);

            if (file.exists())
            {

                String path=file.getAbsolutePath();
                FileReader filereader = null;
                try {
                    filereader = new FileReader(path);
                } catch (FileNotFoundException e)
                {
                    e.printStackTrace();
                }
                BufferedReader buffer = new BufferedReader(filereader);

                String line = "";

                try {

                    int i=0;
                    while ((line = buffer.readLine()) != null)
                    {

                        {
                            String[] str = line.split(",", 7);
                          //  TransationModel model = null;
                            //getting all values
                            //create table " + DATABASE_NAME_CASHIER + " (" + COLUMN_PRIMARY_KEY_ID + " integer primary key autoincrement," + KEY_CASHIER_NAME + " TEXT, " + KEY_CASHIER_NUMBER + " TEXT," + KEY_CASHIER_PASSWORD + " TEXT," + KEY_CASHIER_CONFIRM_PASSWORD + " TEXT," + KEY_CASHIER_COLOR + " TEXT," + KEY_CASHIER_CREATED_DATE + " TEXT,"+KEY_CASHIER_PHOTO+" blob not null );


                            String _id = str[0];
                            String cashierName = str[1];
                            String cashierNumber = str[2];
                            String cashierPassword = str[3];
                            String cashierConfirmPassword = str[4];
                            String cashierColor = str[5];
                            String createDate = str[6];
                            Toast.makeText(con,"cashier name..."+cashierName,Toast.LENGTH_LONG).show();
                            if(i>0) {
                                CashierModel csm = new CashierModel(_id, cashierName, cashierNumber, cashierPassword, cashierConfirmPassword, cashierColor, createDate, null);
                                db.insertCashierDetails(csm);
                            }

                            i++;
                        }

                       *//* Toast.makeText(con,"written to db cashier Table",Toast.LENGTH_LONG).show();*//*


                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
            else
                Toast.makeText(con,"Cashier Model file not exists",Toast.LENGTH_LONG).show();
        }
        db.close();
    }*/

}
