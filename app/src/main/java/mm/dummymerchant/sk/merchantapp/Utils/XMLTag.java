package mm.dummymerchant.sk.merchantapp.Utils;

/**
 * Created by Dell on 10/27/2015.
 */
public interface XMLTag
{
	String TAG_MAIN = "estel";
	String TAG_HEADER = "header";
	String TAG_RESPONSE = "response";
	String TAG_RESPONSE_TYPE = "responsetype";
	String TAG_RESPONSE_CODE = "resultcode";
	String TAG_BALANCE = "walletbalance";
	String TAG_AGENTCODE = "agentcode";
	String TAG_TRANSACID="";
	String TAG_AGENT_NAME = "agentname";
	String TAG_SOURCE = "source";
	String TAG_DESTINATION = "destination";
	String TAG_VEDNDOR_CODE = "vendorcode";
	String TAG_AMOUNT = "amount";
	String TAG_TRANSID = "transid";
	String TAG_RESULTP_CODE = "resultcode";
	String TAG_DESCRIPTION = "resultdescription";
	String TAG_PERWALLET_BALANCE = "prewalletbalance";
	String TAG_FEE = "fee";
	String TAG_TRANSTYPE = "transtype";
	String TAG_DATE = "responsects";
	String TAG_NEW_FEE = "fee";
	String TAG_SERVICE_TAX = "servicetax";
	String TAG_NEW_BALANCE = "walletbalance";
	String TAG_OLD_BALANCE = "prewalletbalance";
	String TAG_OTP = "otp";
	String TAG_LANG = "language";
	String TAG_PIN = "pin";
	String TAG_LIMIT = "allow";
	String TAG_TOKEN = "securetoken";
	String TAG_RESPONSEACT = "responsects";
	String TAG_RECORD_COUNT = "recordcount";
	String TAG_RECORDS = "records";
	String TAG_PERIOD = "period";
	String TAG_SENT = "sent";
	String TAG_WALLET_BALANCE = "walletbalance";


	String TAG_BILLER_CODE = "customer";
	String TAG_AMOUN_PRE = "amountpre";
	String TAG_AMOUN_POST = "amountpost";
	String TAG_BILLER_NAME = "billername";
	String TAG_BILL_DUE_DATE = "billduedate";
	String TAG_INVOICE = "invoiceno";
	String TAG_RECORD = "record";
	String TAG_CREDIT_OR_DEBIT = "acctranstype";
	String TAG_COMMENTS = "comments";
	String TAG_STRING = "string";
	String TAG_REQ_CTS = "requestcts";
	String TAG_MINRANGE = "minrange";
	String TAG_MAXRANGE = "maxrange";
	String TAG_DISBTYPE = "disbtype";
	String TAG_PREAMT = "peramt";
	String TAG_KICKBACK = "kickback";


	// loyalty tag

	String TAG_MERCHANT_NAME = "merchant";
	String TAG_MERCHANT_NUMBER = "merchantid";
	String TAG_BONUSPOINT = "loyaltypoints";
	String TAG_EX_DATE = "expirydate";
	String TAG_DISCOUNT_AMOUNT = "discountamount";
	String TAG_DISCOUNT_SUMMEY = "discountsummary";

	String TAG_EXPIRE_DISCOUNT = "expdate";
	String TAG_AGENT_ADDRESS = "address";

	String TAG_AG_CODE = "agcode";
	String TAG_CLIENT_TYPE="clienttype";


	// KickBack tag
	String TAG_KICK_BACK_BALANCE="udv1";
	//View KickBack
	String TAG_KickBack_MIN_RANGE="minrange";
	String TAG_KickBack_MAX_RANGE="maxrange";
	String TAG_KickBack_DISBTYPE="disbtype";
	String TAG_KickBack_PERAMT="peramt";
	String TAG_KickBack_AMT="amount";
	String TAG_KickBack_STATUS="status";
	//Dummy merchant list
	String TAG_DummyMerchantList_Name="name";
	String TAG_DummyMerchantList_MobileNo="mobileno";
	String TAG_DummyMerchantList_Country="country";
	String TAG_DummyMerchantList_State="state";
	String TAG_DummyMerchantList_City="city";
	String TAG_DummyMerchantList_Status="status";

	// Agent_Info tag
	String TAG_AI_AGENT_CODE="agentcode";
	String TAG_AI_AGENT_SOURCE="source";
	String TAG_AI_AGENT_TYPE="agenttype";
	String TAG_AI_AGENT_LEVEL="agentlevel";
	String TAG_AI_AGENT_BOB="dob";
	String TAG_AI_AGENT_EMAIL="email";
	String TAG_AI_AGENT_IDPROOFTYPE="idprooftype";
	String TAG_AI_AGENT_IDPROOF="idproof";
	String TAG_AI_AGENT_OCCUPASSION="occupation";
	String TAG_AI_AGENT_CUSTOMERSTATUS="customerstatus";
	String TAG_AI_AGENT_STATUS="status";
	String TAG_AI_AGENT_BUSINESSNAME="businessname";
	String TAG_AI_AGENT_STATE="state";
	String TAG_AI_AGENT_COUNTRY="country";
	String TAG_AI_AGENT_CITY="city";
	String TAG_AI_AGENT_LANGUAGE="language";
	String TAG_AI_AGENT_TRANSID="transid";
	String TAG_AI_AGENT_CAR="car";
	String TAG_AI_AGENT_ADDRESS1="address1";
	String TAG_AI_AGENT_ACCOUNTNO1="accountno1";
	String TAG_AI_AGENT_ACCOUNTNO2="accountno2";
	String TAG_AI_AGENT_MSISDN1="msisdn1";
	String TAG_AI_AGENT_MSISDN2="msisdn2";
	String TAG_AI_AGENT_MSISDN3="msisdn3";
	String TAG_AI_AGENT_MSISDN4="msisdn4";
	String TAG_AI_AGENT_MSISDN5="msisdn5";
	String TAG_AI_AGENT_GENDER="gender";



}

