package mm.dummymerchant.sk.merchantapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;



import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.Utils;
import mm.dummymerchant.sk.merchantapp.customView.CustomEdittext;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
import mm.dummymerchant.sk.merchantapp.httpConnection.CallingApIS;
import mm.dummymerchant.sk.merchantapp.httpConnection.HttpNewRequest;
import mm.dummymerchant.sk.merchantapp.model.LoginAPI;
import mm.dummymerchant.sk.merchantapp.model.Profile;

public class VerifyUser extends BaseActivity implements View.OnClickListener, HttpNewRequest.LoginApiListener {

    boolean isLanguageChanged = false;
    private CustomEdittext mName, mMobileNo, mPassword, mFatherName, otpVerification;
    private CustomTextView mDateOfBirth, mCountryCode, mPrefixNameText, textName, mFatherNamePrefix;
    private LinearLayout mPhotoLayout, mLinearUserName, mMaleFemaleRegfix, mFemaleRegfixLinear;
    private ImageView userPhoto, mImageCountryCode;
    private boolean isDateSelected = false;
    private boolean isPhotoTaken = false;
    LoginAPI mLoginModel;
    String mobileNo = "";
    private boolean gender = false;

    CustomTextView mMale, mFemale, mClear;
    static String mMalePrefixData[];
    static String mFemalePrefixData[];
    public static String mPrefixName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //AppPreference.setAuthToken(this, "XX");
        setContentView(R.layout.activity_verification_register);
        setHeadingVerifyUser();
        mDateOfBirth = (CustomTextView) findViewById(R.id.dob_verification);
        mMobileNo = (CustomEdittext) findViewById(R.id.phone_verification);
        mName = (CustomEdittext) findViewById(R.id.name_verification);
        mFatherName = (CustomEdittext) findViewById(R.id.father_name_verification);
        mFatherNamePrefix = (CustomTextView) findViewById(R.id.registration_page_edittext_fatherName_prefix);
        mPhotoLayout = (LinearLayout) findViewById(R.id.take_your_pic);
        mPassword = (CustomEdittext) findViewById(R.id.password_verification);
        userPhoto = (ImageView) findViewById(R.id.user_photo);
        mMale = (CustomTextView) findViewById(R.id.verification_textview_text_gender_male);
        mFemale = (CustomTextView) findViewById(R.id.verification_textview_text_gender_female);
        mMaleFemaleRegfix = (LinearLayout) findViewById(R.id.verification_linear_layout_add_name_prefix);
        mFemaleRegfixLinear = (LinearLayout) findViewById(R.id.verification_linear_layout_add_name__femail);
        mLinearUserName = (LinearLayout) findViewById(R.id.verification_linear_username);
        mPrefixNameText = (CustomTextView) findViewById(R.id.verification_text_gender_prefix_name);
        textName = (CustomTextView) findViewById(R.id.name);
        mClear = (CustomTextView) findViewById(R.id.registration_page_clear);

        mCountryCode = (CustomTextView) findViewById(R.id.tv_country_code);
        mImageCountryCode = (ImageView) findViewById(R.id.img_tv_country_code);
        otpVerification = (CustomEdittext) findViewById(R.id.otp_verification);

        mDateOfBirth.setOnClickListener(this);
        mPhotoLayout.setOnClickListener(this);
        buttonLogout.setOnClickListener(this);
        textName.setTypeface(Utils.getFont(this), Typeface.BOLD);
        textName.setOnClickListener(this);
        mMale.setOnClickListener(this);
        mFemale.setOnClickListener(this);
        mClear.setOnClickListener(this);
        mMalePrefixData = getResources().getStringArray(R.array.male_prefix);
        mFemalePrefixData = getResources().getStringArray(R.array.female_prefix);

        mDateOfBirth.setTypeface(Utils.getFont(this), Typeface.BOLD);
        mFatherName.setTypeface(Utils.getFont(this), Typeface.BOLD);

        getIntentData();

        backFinish();
    }

    private void getIntentData() {
        Intent data = getIntent();
        if (data != null) {
            mImageCountryCode.setImageResource(data.getIntExtra("IMAGE_ID", -1));
            mCountryCode.setText(data.getStringExtra("COUNTRY_CODE"));
            mMobileNo.setText(data.getStringExtra(MO_NUMBER));
            mobileNo = data.getStringExtra("mo");
        }
    }

    @Override
    public <T> void response(int resultCode, T data) {

        switch (resultCode) {
            case REQUEST_TYPE_RETRIVE_PROFILE:
                Profile profile = (Profile) data;
                db.deleteProfile();
                db.deleteBankDetails();
                db.deleteCashBankOutNo();
                if (profile != null) {
                    if (db.insertProfile(profile)) {
                        //Utils.showToast(this, "Profile Data inserted");
                    }
                    db.insertCashBankOutNo(profile.getMsisdn());
                    db.insertBankDetail(profile.getBankDetails());
                }
                AppPreference.setCountryCodeFlagNameLogin(this, AppPreference.getCountryName(this), AppPreference.getCountryCode(this));
                AppPreference.setCountryImageIDLogin(this, AppPreference.getCountryImageID(this));
                Intent call = new Intent(this, SliderScreen.class);
                call.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(call);
                finish();
                Utils.showCustomToastMsg(this, R.string.login_success, true);
                break;
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.dob_verification:
                showDatePicker();
                break;
            case R.id.take_your_pic:
                Intent   intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, 402);

                break;
            case R.id.custom_actionbar_application_right_image:
                if (Utils.isConnectedToInternet(this)) {
                    String name = mName.getText().toString();
                    String fatherName = mFatherName.getText().toString();
                    String password = mPassword.getText().toString();
                    String dob = mDateOfBirth.getText().toString();
                    String otp = otpVerification.getText().toString();
                    if (isValid(name, dob, fatherName, otp, password)) {
                        name = mPrefixNameText.getText().toString() + "," + name;
                        fatherName = mFatherNamePrefix.getText().toString() + "," + fatherName;
                        //fatherName="Mr ,ramki";
                        mLoginModel = new LoginAPI();
                        Bitmap bitmapImage = ((BitmapDrawable) userPhoto.getDrawable()).getBitmap();
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        bitmapImage.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                        byte[] byteArrayImage = stream.toByteArray();
                        mLoginModel.setProfilePic(Base64.encodeToString(byteArrayImage, Base64.DEFAULT));
                        mLoginModel.setSimId(Utils.simSerialNumber(VerifyUser.this));
                        mLoginModel.setMsid(Utils.getSimSubscriberID(VerifyUser.this));
                        mLoginModel.setMobileNumber(mobileNo);
                        mLoginModel.setDateOfBirth(dob);
                        mLoginModel.setFatherName(fatherName);
                        mLoginModel.setName(name);
                        mLoginModel.setPassword(password);
                        mLoginModel.setOsType(0);
                        mLoginModel.setReRegisterOtp(otp);
                        HttpNewRequest call = new HttpNewRequest(mLoginModel, VerifyUser.this, VerifyUser.this, APP_SERVER_PARAMS_VERIFY);
                        call.start();
                    }
                } else {
                    Utils.showCustomToastMsg(this, R.string.async_task_no_internet, false);
                }
                break;
            case R.id.name:
                textName.setVisibility(View.GONE);
                break;
            case R.id.verification_textview_text_gender_male:
                gender = true;

                mMale.setVisibility(View.GONE);
                mFemale.setVisibility(View.GONE);
                mLinearUserName = (LinearLayout) findViewById(R.id.verification_linear_username);
                mMaleFemaleRegfix.setVisibility(View.VISIBLE);
                mFemaleRegfixLinear.setVisibility(View.GONE);
                addMalePrefixIntheList();

                break;
            case R.id.verification_textview_text_gender_female:
                gender = false;

                mMale.setVisibility(View.GONE);
                mFemale.setVisibility(View.GONE);
                mLinearUserName = (LinearLayout) findViewById(R.id.verification_linear_username);
                mMaleFemaleRegfix.setVisibility(View.GONE);
                mFemaleRegfixLinear.setVisibility(View.VISIBLE);
                addFeMalePrefixIntheList();

                break;
            case R.id.registration_page_clear:
                mMale.setVisibility(View.VISIBLE);
                mFemale.setVisibility(View.VISIBLE);
                mMaleFemaleRegfix.setVisibility(View.GONE);
                mFemaleRegfixLinear.setVisibility(View.GONE);
                mLinearUserName.setVisibility(View.GONE);
                mPrefixName = "";
                mName.setText("");
                mName.clearFocus();
                textName.setVisibility(View.VISIBLE);
                mDateOfBirth.setText(null);
                mFatherName.setText(null);
                userPhoto.setImageDrawable(null);
                mPassword.setText(null);
                otpVerification.setText(null);
                hideKeyboard();
                break;
        }
    }


    private void addMalePrefixIntheList() {

        mMaleFemaleRegfix.removeAllViews();
        ImageView imageView = new ImageView(this);
        imageView.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT, 1f));
        imageView.setImageResource(R.drawable.male);
        imageView.setBackgroundResource(R.drawable.registration_gender_bg);
        imageView.setPadding(5, 5, 5, 5);
        mMaleFemaleRegfix.addView(imageView);
        for (int i = 0; i < mMalePrefixData.length; i++) {
            CustomTextView textView = new CustomTextView(this);
            textView.setText(mMalePrefixData[i]);
            textView.setTextSize(18);
            textView.setGravity(Gravity.CENTER);
            textView.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT, 1f));
            textView.setBackgroundResource(R.drawable.registration_gender_bg);
            textView.setPadding(5, 5, 5, 5);
            mMaleFemaleRegfix.addView(textView);
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CustomTextView text = (CustomTextView) v;
                    mPrefixName = text.getText().toString();
                    mLinearUserName.setVisibility(View.VISIBLE);
                    mMaleFemaleRegfix.setVisibility(View.GONE);
                    mPrefixNameText.setText(mPrefixName);
                    mName.requestFocus();
                    //mDateOfBirth.setVisibility(View.VISIBLE);
                    //llFatherName.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    private void addFeMalePrefixIntheList() {

        mFemaleRegfixLinear.removeAllViews();
        ImageView imageView = new ImageView(this);
        imageView.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT, 1f));
        imageView.setImageResource(R.drawable.female);
        imageView.setBackgroundResource(R.drawable.registration_gender_bg);
        imageView.setPadding(5, 5, 5, 5);

        mFemaleRegfixLinear.addView(imageView);

        for (int i = 0; i < mFemalePrefixData.length; i++) {

            CustomTextView textView = new CustomTextView(this);
            textView.setText(mFemalePrefixData[i]);
            textView.setTextSize(18);
            textView.setGravity(Gravity.CENTER);
            textView.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT, 1f));
            textView.setBackgroundResource(R.drawable.registration_gender_bg);
            textView.setPadding(5, 5, 5, 5);
            mFemaleRegfixLinear.addView(textView);
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CustomTextView text = (CustomTextView) v;
                    mPrefixName = text.getText().toString();
                    mLinearUserName.setVisibility(View.VISIBLE);
                    mFemaleRegfixLinear.setVisibility(View.GONE);
                    mPrefixNameText.setText(mPrefixName);
                    mName.requestFocus();
                    //mDateOfBirth.setVisibility(View.VISIBLE);
                    // llFatherName.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    private boolean isValid(String userName, String DOB, String fatherName, String otp, String password) {

        String errorMessage = getResources().getString(R.string.pls_enter_your) + "\n";
        String oldErrorMessage = getResources().getString(R.string.pls_enter_your) + "\n";
        if (Utils.isEmpty(userName)) {
            errorMessage = errorMessage + getResources().getString(R.string.name) + ",\n";
        }
        if (Utils.isEmpty(fatherName)) {
            errorMessage = errorMessage + getResources().getString(R.string.father_name) + ",\n";
        }
        if (Utils.isEmpty(DOB)) {
            errorMessage = errorMessage + getResources().getString(R.string.dob) + ",\n";
        }
        if (userPhoto.getDrawable() == null) {
            errorMessage = errorMessage + getResources().getString(R.string.take_your_photo) + ",\n";
        }
        if (Utils.isEmpty(otp)) {
            errorMessage = errorMessage + getResources().getString(R.string.otp) + ",\n";
        }
        if (Utils.isEmpty(password)) {
            errorMessage = errorMessage + getResources().getString(R.string.password) + ",\n";
        }
        if (oldErrorMessage.equalsIgnoreCase(errorMessage)) {
            return true;
        } else {
            showAlert(errorMessage.substring(0, errorMessage.length() - 2));
            return false;
        }
    }

    public void showDatePicker() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, -5);

        final AlertDialog.Builder mDateTimeDialog = new AlertDialog.Builder(this);
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.custom_date_picker, null);
        final DatePicker datePickerView = (DatePicker) view.findViewById(R.id.datePickerView);
        datePickerView.updateDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        mDateTimeDialog.setView(view);
        mDateTimeDialog.setPositiveButton("Set", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                int day = datePickerView.getDayOfMonth();
                int month = datePickerView.getMonth();
                int year = datePickerView.getYear();

                Calendar calendar = Calendar.getInstance();
                calendar.set(year, month, day);
                Calendar preCalendar = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);
                calendar.set(Calendar.DAY_OF_MONTH, day);

                preCalendar.add(Calendar.YEAR, -5);

                if (calendar.getTimeInMillis() <= preCalendar.getTimeInMillis()) {
                    mDateOfBirth.setText(sdf.format(calendar.getTime()));
                    isDateSelected = true;
                } else {
                    languageReset();
                    mDateOfBirth.setText(getResources().getString(R.string.dob_text));
                    showToast(R.string.dob_error);
                    return;
                }

            }
        });
        mDateTimeDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        mDateTimeDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                languageReset();
            }
        });
        mDateTimeDialog.show();

    }

    private void languageReset() {
        if (isLanguageChanged) {
            changeLang("my");
            isLanguageChanged = false;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("requestcode", ".." + requestCode);
        try {
            if (requestCode == 402) {
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                userPhoto.setImageBitmap(photo);
                return;
            }

            if (requestCode == 108) {
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();
                userPhoto.setImageBitmap(BitmapFactory.decodeFile(picturePath));
            }

            if (resultCode == 106) {
                Intent cameraIntent = new Intent(this, AndroidSurfaceviewExample.class);

                startActivityForResult(cameraIntent, 102);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void responseListener(String res, int resultCode) {
        try {
            String secureToken = "", agentName = "", agentType = "", agentLevel = "", udv1 = "", agentAuthCode = "";
            boolean authCodeStatus = false;
            if (resultCode == APP_SERVER_PARAMS_VERIFY) {
                JSONObject jsonObject = new JSONObject(res);
                String statusCode = jsonObject.getString("Code");
                String msg = jsonObject.getString("Msg");
                if (statusCode.equalsIgnoreCase("200")) {

                    CallingApIS api = new CallingApIS(this, this, 0);
                    api.callRetriveProfile(mobileNo, Utils.simSerialNumber(this));

                    String data = jsonObject.getString("Data");
                    JSONObject jsonObject1 = new JSONObject(data);
                    JSONArray jsonArray = jsonObject1.getJSONArray("AgentDetails");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                        secureToken = jsonObject2.getString("securetoken");
                        agentName = jsonObject2.getString("agentname");
                        agentType = jsonObject2.getString("agenttype");
                        agentLevel = jsonObject2.getString("agentLevel");
                        udv1 = jsonObject2.getString("udv1");
                        agentAuthCode = jsonObject2.getString("AuthCode");
                        authCodeStatus = jsonObject2.getBoolean("AuthCodeStatus");
                    }

                    AppPreference.setIsLoginFirstTime(this, false);
                    AppPreference.setMyMobileNoWithoutCountryCode(this, mMobileNo.getText().toString());
                    AppPreference.setUserLevelType(this, agentLevel);
                    AppPreference.setUserType(this, agentType);
                    AppPreference.setAgentAuthCode(this, agentAuthCode);
                    AppPreference.setAgentAuthCodeStatus(this, authCodeStatus);
                    AppPreference.setMyMobileNo(getApplicationContext(), mobileNo);
                    AppPreference.setLoginUserName(getApplicationContext(), agentName);
                    AppPreference.setAuthToken(getApplicationContext(), secureToken);
                    AppPreference.setPasword(getApplicationContext(), mPassword.getText().toString());
                    Calendar calender = Calendar.getInstance();
                    AppPreference.setSmsPaymentActive(getApplicationContext(), true);
                    AppPreference.setLoginTime(getApplicationContext(), calender.getTimeInMillis());
                    //mPassword.setText("");

                } else if (statusCode.equalsIgnoreCase("-1")) {
                    Utils.showCustomToastMsg(this, R.string.api_error2, false);
                } else {
                    showToast(msg);
                }
            }
        } catch (Exception e) {
        }

    }

    private void backFinish() {
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                finish();
            }
        });
    }


}
