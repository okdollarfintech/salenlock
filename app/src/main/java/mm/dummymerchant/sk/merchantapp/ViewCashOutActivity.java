package mm.dummymerchant.sk.merchantapp;

import android.app.ProgressDialog;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import java.util.ArrayList;

import mm.dummymerchant.sk.merchantapp.adapter.CashoutLog_Adapter;
import mm.dummymerchant.sk.merchantapp.db.DBHelper;
import mm.dummymerchant.sk.merchantapp.model.CashOutModel;

/**
 * Created by user on 11/28/2015.
 */
public class ViewCashOutActivity extends BaseActivity {

    DBHelper db;
    CashoutLog_Adapter adapter;
    private ListView lv;
    CashOutModel cellvalue;
    private ArrayList<CashOutModel> AlertList=new ArrayList<CashOutModel>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewcashout);
        setExistingCashcollectorActionBar(String.valueOf(getResources().getText(R.string.viewcashout)));

        lv = (ListView)findViewById(R.id.listView1);
        db = new DBHelper(getApplicationContext());

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                new AsyncTaskLoading().execute();
            }
        });

        }

    @Override
    public <T> void response(int resultCode, T data) {

    }
    private class AsyncTaskLoading extends AsyncTask<Void, Void, Void> {
        /// private GameResultsAdapter adapter;
        private final ProgressDialog dialogue =new ProgressDialog(ViewCashOutActivity.this);
        public AsyncTaskLoading() {
        }
        @Override
        protected void onPreExecute()
        {
            this.dialogue.setMessage("Loading...please wait.");
            this.dialogue.setIndeterminate(true);
            this.dialogue.setCancelable(false);
            this.dialogue.setCanceledOnTouchOutside(false);
            this.dialogue.show();
        }
        @Override
        protected Void doInBackground(Void... params)
        {
            try {

                db.open();
                Cursor c = db.Get_CASHOUT_TABLE();
                System.out.println("Cursor Count() " +c.getCount());
                db.close();

                if (c.getCount() > 0) {
                    Log.i("AsyncTaskLoading IF ", "Enter IF Contion");

                    if (c.moveToFirst()) {
                        while (c.isAfterLast() == false) {
                            cellvalue = new CashOutModel();
                            Log.i("CashCollectorModel", "READIN-Get_CashCollectorModel");

                            /*System.out.println("Id " + c.getString(0));
                            System.out.println("Time " + c.getString(1));
                            System.out.println("Name " + c.getString(2));
                            System.out.println("Amount  " + c.getString(3));
                            System.out.println("Commistion Type " + c.getString(5));
                            System.out.println("Commistion Amount" + c.getString(4));
                            System.out.println("Final Amount " + c.getString(6));*/

                            cellvalue.setTime(c.getString(1));
                            cellvalue.setName(c.getString(2));
                            cellvalue.setamount(c.getString(3));
                            cellvalue.setcomm_Type(c.getString(5));
                            cellvalue.setcomm_amt(c.getString(4));
                            cellvalue.setFinalAmout(c.getString(6));

                            AlertList.add(cellvalue);
                            c.moveToNext();
                        }
                    }
                }
                else {
                    // Toast.makeText(getApplicationContext(), "Record Not Found", Toast.LENGTH_SHORT).show();
                    showToast(getString(R.string.no_record));
                }
            }catch(Exception e){

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void res)
        {
            System.out.println("AlertList.size() "+AlertList.size());
            if (AlertList.size() != 0)
            {
                adapter =new CashoutLog_Adapter(ViewCashOutActivity.this, AlertList);
                lv.setAdapter(adapter);
            }
            else
            {
                showToast(getString(R.string.no_record));
            }

            if (this.dialogue.isShowing())
            {
                this.dialogue.dismiss();
            }
        }
    }

}
