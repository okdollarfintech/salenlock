package mm.dummymerchant.sk.merchantapp;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;

import java.util.ArrayList;

import mm.dummymerchant.sk.merchantapp.adapter.ViewCashierAdapter;
import mm.dummymerchant.sk.merchantapp.db.DBHelper;
import mm.dummymerchant.sk.merchantapp.model.CashierModel;

/**
 * Created by Dell on 11/3/2015.
 */
public class ViewCashierActivity extends BaseActivity
{
    SwipeMenuListView lv;
    ViewCashierAdapter adapter=null;
    public static CashierModel cashierModel;
    Context context;
    DBHelper dbb;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewcashier);

        dbb=new DBHelper(this);
        context=this;
        init();
        setViewCashierActionBar();
        processListFromActionBar();

        lv.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index)
            {
                //showToast("delete clicked");
             cashierModel = (CashierModel) adapter.getItem(position);
             dbb.open();
             dbb.deleteUpdateCashierDetails(cashierModel);
             dbb.close();
             processListFromActionBar();


                return false;
            }
        });

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                cashierModel = (CashierModel) adapter.getItem(i);
                Intent intent = new Intent(context, EditCashierProfileActivity.class);
                startActivity(intent);
                finish();
            }
        });

       lv .setOnSwipeListener(new SwipeMenuListView.OnSwipeListener() {

           @Override
           public void onSwipeStart(int position) {
               // swipe start
           }

           @Override
           public void onSwipeEnd(int position) {
               // swipe end
           }
       });
    }

    void init()
    {
        lv=(SwipeMenuListView)findViewById(R.id.lv_viewcashier);
    }

    void processListFromActionBar()
    {
        dbb=new DBHelper(this);
       dbb.open();
        ArrayList<CashierModel> al_cashiers=dbb.getCashierDetails();
        dbb.close();

        if(al_cashiers!=null)
        {
             adapter = new ViewCashierAdapter(this, al_cashiers);
            lv.setAdapter(adapter);
            lv.setMenuCreator(creator);

            if(al_cashiers.size()==0)
                showToast("0");
        }
        else {
            al_cashiers=new ArrayList<CashierModel>();
            adapter = new ViewCashierAdapter(this, al_cashiers);
            lv.setAdapter(adapter);
            lv.setMenuCreator(creator);
             finish();
        }

    }

    @Override
    public <T> void response(int resultCode, T data)
    {

    }

    @Override
    public void onBackPressed()
    {
        finish();
    }

    SwipeMenuCreator creator = new SwipeMenuCreator() {

        @Override
        public void create(SwipeMenu menu) {
            // create "open" item
            SwipeMenuItem openItem = new SwipeMenuItem(
                    getApplicationContext());
            // set item background


            // create "delete" item
            SwipeMenuItem deleteItem = new SwipeMenuItem(
                    getApplicationContext());
            // set item background
            deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                    0x3F, 0x25)));
            // set item width
            deleteItem.setWidth(dp2px(90));

            // set a icon
            deleteItem.setIcon(R.drawable.ic_delete);
            // add to menu
            menu.addMenuItem(deleteItem);
        }
    };


}
