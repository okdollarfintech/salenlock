package mm.dummymerchant.sk.merchantapp.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import mm.dummymerchant.sk.merchantapp.R;
import mm.dummymerchant.sk.merchantapp.model.CashierActivityLogModel;

/**
 * Created by user on 11/7/2015.
 */
public class ActivityLog_Adapter extends BaseAdapter
{
    //Variable Declaration
    Context context;
    Activity activity;
    // int User_id;
    ArrayList<CashierActivityLogModel> AlertList;
    // private int[] bgColors = new int[] { R.color.list_bg_1, R.color.list_bg_2 };

    public ActivityLog_Adapter(Context context, ArrayList<CashierActivityLogModel> AlertList)
    {
        this.context = context;
        this.AlertList = AlertList;
    }
    private class ViewHolder
    {
        TextView Name,No,Login,Logout,Reason;
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return AlertList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return AlertList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return AlertList.indexOf(getItem(position));
    }


    @SuppressLint("InflateParams")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        final ViewHolder holder;

        LayoutInflater mInflater = (LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
        {
            convertView = mInflater.inflate(R.layout.row_item_activitylog, null);
            holder = new ViewHolder();

            holder.Name = (TextView) convertView.findViewById(R.id.name);
            holder.No = (TextView) convertView.findViewById(R.id.no);
            holder.Login = (TextView) convertView.findViewById(R.id.login);
            holder.Logout = (TextView) convertView.findViewById(R.id.logout);
            holder.Reason = (TextView) convertView.findViewById(R.id.reason);

            convertView.setTag(holder);
        }

        else
        {
            holder = (ViewHolder) convertView.getTag();
        }

        CashierActivityLogModel m = AlertList.get(position);

        holder.Name.setText(m.getCashier_Name());
        holder.No.setText(m.getCashier_Number());
        holder.Login.setText(m.getLogin_Time());
        holder.Logout.setText(m.getLogout_Time());
        holder.Reason.setText(m.getLogout_Reason());

        return convertView;

    }

}
