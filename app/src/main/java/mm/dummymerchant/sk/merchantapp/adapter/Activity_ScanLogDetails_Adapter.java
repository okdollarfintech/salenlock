package mm.dummymerchant.sk.merchantapp.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import mm.dummymerchant.sk.merchantapp.R;
import mm.dummymerchant.sk.merchantapp.customView.CustomButton;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
import mm.dummymerchant.sk.merchantapp.model.RowItem_cashcollector;

/**
 * Created by user on 1/20/2016.
 */
public class Activity_ScanLogDetails_Adapter extends BaseAdapter
{
    //Variable Declaration
    Context context;
    Activity activity;
    ArrayList<RowItem_cashcollector> AlertList;
    String textvalue,templatevalue;

    public Activity_ScanLogDetails_Adapter(Context context, ArrayList<RowItem_cashcollector> AlertList)
    {
        this.context = context;
        this.AlertList = AlertList;
    }
    private class ViewHolder
    {
        //TextView Name,No,Login,Logout,Reason;
        TextView Status,Cashiername,Cashierno,Cashcollectorname,Cashcollectorno,Amount,ScanTime,GenerateTime,/*OTPEnterTime,*/Transid;
        TextView Textvalue,TemplateValue;
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return AlertList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return AlertList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return AlertList.indexOf(getItem(position));
    }


    @SuppressLint("InflateParams")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        final ViewHolder holder;

        LayoutInflater mInflater = (LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
        {
            convertView = mInflater.inflate(R.layout.row_item_scan_logdetails, null);
            holder = new ViewHolder();

            holder.Transid = (TextView) convertView.findViewById(R.id.transid);
            holder.Cashiername = (TextView) convertView.findViewById(R.id.Cashiername);
            holder.Cashierno = (TextView) convertView.findViewById(R.id.no);
            holder.Cashcollectorname = (TextView) convertView.findViewById(R.id.Cashiername1);
            holder.Cashcollectorno = (TextView) convertView.findViewById(R.id.no1);
            holder.Amount = (TextView) convertView.findViewById(R.id.Amount);
            holder.Status = (TextView) convertView.findViewById(R.id.Status);
            holder.ScanTime = (TextView) convertView.findViewById(R.id.scanning_time);
            holder.GenerateTime = (TextView) convertView.findViewById(R.id.generate_time);
            //holder.OTPEnterTime = (TextView) convertView.findViewById(R.id.otpenter_time);
            holder.Textvalue = (TextView) convertView.findViewById(R.id.Textvalue);
            holder.TemplateValue = (TextView) convertView.findViewById(R.id.TemplateValue);
            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }

       final RowItem_cashcollector m = AlertList.get(position);

        holder.Transid.setText(m.GetLog_Trans_Id());
        holder.ScanTime.setText(m.GetScanning_Time());
        holder.GenerateTime.setText(m.GetGenerate_Time());
       // holder.OTPEnterTime.setText(m.GetOTP_Enter_Time());
        holder.Cashiername.setText(m.GetGenerater_Name());
        holder.Cashierno.setText(m.GetGenerater_No());
        holder.Cashcollectorname.setText(m.GetScanner_Name());
        holder.Cashcollectorno.setText(m.GetScanner_No());
        holder.Status.setText(m.GetLog_Status());

        if(m.GetLog_Status().equals("FAILURE"))
        {
            holder.Status.setTextColor(Color.RED);
        }
        else
        {
            holder.Status.setTextColor(Color.GREEN);
        }


        if(m.GetLog_Amount()== null ||  m.GetLog_Amount().equals("") )
        {
            holder.Amount.setText("-");
            holder.Amount.setSelected(true);
        }
        else {
            holder.Amount.setText(m.GetLog_Amount());
            holder.Amount.setSelected(true);
        }

        textvalue = m.getTextValue();
        templatevalue = m.getTemplateValue();

        if(textvalue == null || textvalue.equals("") || textvalue.equals("null") || textvalue.equals("?") )
        {
            holder.Textvalue.setText("-");
            holder.Textvalue.setSelected(true);
        }
        else {
            holder.Textvalue.setText(m.getTextValue());
            holder.Textvalue.setSelected(true);
        }

        if(templatevalue.equals("[]")|| templatevalue.equals("") || templatevalue.equals("null") )
        {
            holder.TemplateValue.setText("-");
            holder.TemplateValue.setSelected(true);
        }
        else {
            holder.TemplateValue.setText(m.getTemplateValue());
            holder.TemplateValue.setSelected(true);
        }

        holder.TemplateValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                if(!holder.TemplateValue.getText().equals("-"))
                {
                    final Dialog dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.dialog);
                    dialog.show();
                    LinearLayout container = (LinearLayout) dialog.findViewById(R.id.container);
                    CustomTextView close = (CustomTextView) dialog.findViewById(R.id.close);
                    System.out.println("TemplateValue " + m.getTemplateValue());
                    String i = m.getTemplateValue().replaceAll("\\[", "").replaceAll("\\]", "");
                    System.out.println("TemplateValue remove [ ] " + i);
                    String dataText[] = i.split(",");
                    int len = dataText.length;
                    System.out.println("dataText length" + len);

                    for (int j = 0; j < len; j++) {
                        System.out.println(dataText[j]);
                        String dataText1[] = dataText[j].split("/");
                        System.out.println(dataText1[0]);
                        System.out.println(dataText1[1]);
                        System.out.println(dataText1[2]);
                        LinearLayout Container_id = container;
                        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View addView = layoutInflater.inflate(R.layout.row_dialog, null);
                        CustomTextView item1 = (CustomTextView) addView.findViewById(R.id.itemvalue);
                        CustomTextView qty1 = (CustomTextView) addView.findViewById(R.id.qtyvalue);
                        CustomTextView price1 = (CustomTextView) addView.findViewById(R.id.pricevalue);
                        Container_id.addView(addView);
                        item1.setText(dataText1[0]);
                        qty1.setText(dataText1[1]);
                        price1.setText(dataText1[2]);
                    }
                    close.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                  }
            }
        });


        holder.Textvalue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(!holder.Textvalue.getText().equals("-"))
                {
                    final Dialog dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.dialog_textvalue);
                    dialog.show();

                    CustomTextView close = (CustomTextView) dialog.findViewById(R.id.close);
                    CustomButton ok = (CustomButton) dialog.findViewById(R.id.ok);
                    CustomTextView input = (CustomTextView) dialog.findViewById(R.id.input);
                    input.setText(m.getTextValue());
                    ok.setVisibility(View.GONE);
                    close.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    System.out.println("m.getTextValue " + m.getTextValue());
                }
            }
        });

        return convertView;

    }

}
