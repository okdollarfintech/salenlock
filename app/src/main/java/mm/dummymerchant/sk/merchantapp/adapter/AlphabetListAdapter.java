package mm.dummymerchant.sk.merchantapp.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;

import mm.dummymerchant.sk.merchantapp.R;

public class AlphabetListAdapter extends BaseAdapter {

    ContactOnitemClick contactOnitemClick;


    public static abstract class Row {
    }

    public static final class Section extends Row {
        public final String text;

        public Section(String text) {
            this.text = text;
        }
    }

    public static final class Item extends Row {
        public final String name, number, photo;

        public Item(String text, String number, String photo) {

            this.name = text;
            this.number = number;
            this.photo = photo;

        }
    }

    private List<Row> rows;

    public void setRows(List<Row> rows) {
        this.rows = rows;
    }

    public void setContactClickListerner(ContactOnitemClick listerner) {
        if (contactOnitemClick == null) {
            this.contactOnitemClick = listerner;
        }
    }

    @Override
    public int getCount() {
        return rows.size();
    }

    @Override
    public Row getItem(int position) {
        return rows.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        if (getItem(position) instanceof Section) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (getItemViewType(position) == 0) { // Item
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = (LinearLayout) inflater.inflate(R.layout.row_item, parent, false);
            }

            Item item = (Item) getItem(position);
            LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.llinear_contact_item_row);
            TextView tvName = (TextView) view.findViewById(R.id.tv_name);
            TextView tvContact = (TextView) view.findViewById(R.id.tv_contact);
            ImageView imageView = (ImageView) view.findViewById(R.id.image);
         //   imageView.setImageResource(R.drawable.ic_launcher);
            tvName.setText(item.name);
            tvContact.setText(item.number);
            linearLayout.setTag(tvContact);
            Pattern numberPattern = Pattern.compile("[0-9]");
            char[] chars1 = "ABCDEF012GHIJKL345MNOPQR678STUVWXYZ9".toCharArray();
            StringBuilder sb1 = new StringBuilder();
            Random random1 = new Random();
            for (int i = 0; i < 6; i++)
            {
                char c1 = chars1[random1.nextInt(chars1.length)];
                sb1.append(c1);
            }
            String random_string = sb1.toString();
            String firstLetter = item.name.substring(0, 1);
            ColorGenerator mGenerator1 = ColorGenerator.DEFAULT;
            if (numberPattern.matcher(firstLetter).matches()) {
                firstLetter = "#";
            }
            TextDrawable drawable = TextDrawable.builder()
                            .buildRound(firstLetter, mGenerator1.getColor(random_string));
           //         .buildRound(firstLetter, parent.getContext().getResources().getColor(R.color.light_cyan));
            imageView.setImageDrawable(drawable);
            // Group numbers together in the scroller
            RoundImage roundedImage;
            if(item.photo!=""){
                Uri uri = Uri.parse(item.photo);
                if (uri != null)

                try {
                    Bitmap bm = MediaStore.Images.Media.getBitmap(parent.getContext().getContentResolver(), uri);
                    Bitmap bMapScaled = Bitmap.createScaledBitmap(bm, 130, 180, true);
                    roundedImage = new RoundImage(bMapScaled);
                    imageView.setImageDrawable(roundedImage);
                    imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                } catch (IOException e) {
                    e.printStackTrace();
                    imageView.setBackgroundResource(R.drawable.profile_page_row_no_profile_pic);
                }
            }

            linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    contactOnitemClick.onContactIntemClick(view);
                }
            });
        } else { // Section
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = (LinearLayout) inflater.inflate(R.layout.row_section, parent, false);
            }

            Section section = (Section) getItem(position);
            TextView textView = (TextView) view.findViewById(R.id.textView1);
            textView.setText(section.text);
        }

        return view;
    }

    public interface ContactOnitemClick {
        void onContactIntemClick(View requestCode);
    }

}
