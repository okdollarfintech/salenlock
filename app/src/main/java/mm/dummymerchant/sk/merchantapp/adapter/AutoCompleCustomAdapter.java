package mm.dummymerchant.sk.merchantapp.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.LinearLayout;



import java.util.ArrayList;
import java.util.HashSet;

import mm.dummymerchant.sk.merchantapp.R;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
import mm.dummymerchant.sk.merchantapp.model.ConstactModel;

/**
 * Created by abhishekmodi on 20/10/15.
 */
public class AutoCompleCustomAdapter extends ArrayAdapter<ConstactModel> {

    LayoutInflater inflater;
    ArrayList<ConstactModel> mList;
    ArrayList<ConstactModel> mOrifinal;
    ArrayList<ConstactModel> suggestionList= new ArrayList<ConstactModel>();
    AutoCompleteTextItemSelected selectedInterface;

    public interface AutoCompleteTextItemSelected
    {
        void getItemSelectedValues(String name, String number);
    }


    Context mContext;
    int layoutReferenceId;

    public AutoCompleCustomAdapter(Context context, int resource, ArrayList<ConstactModel> objects, AutoCompleteTextItemSelected object) {
        super(context, resource, objects);
        mList = new ArrayList<ConstactModel>(objects.size());
        mList.addAll(objects);
        this.mContext=context;
        layoutReferenceId=resource;
        inflater=(LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.selectedInterface=object;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final Holder holder;
        if(convertView==null)
        {
            holder= new Holder();
            convertView=inflater.inflate(layoutReferenceId,parent,false);
            holder.mName=(CustomTextView)convertView.findViewById(R.id.custom_auto_completTextView_name);
            holder.mNumber=(CustomTextView)convertView.findViewById(R.id.custom_auto_completTextView_number);
            holder.mRow=(LinearLayout)convertView.findViewById(R.id.contact_auto_list_row);
            convertView.setTag(holder);
        }
        else
        {
            holder=(Holder)convertView.getTag();
        }
        try {
            Log.e("position", "Position=" + position);
            if (position >= 0) {
                holder.mName.setText(suggestionList.get(position).getName());
                holder.mNumber.setText(suggestionList.get(position).getMobileNo());
            }
            holder.mRow.setTag(position);

            holder.mRow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                if(selectedInterface!=null)
                {
                    int position=(int)holder.mRow.getTag();
                    selectedInterface.getItemSelectedValues(suggestionList.get(position).getName(),suggestionList.get(position).getMobileNo());
                }
                }
            });
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        return convertView;
    }

   public  class Holder
    {
        CustomTextView mName,mNumber;
        LinearLayout mRow;
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    private Filter mFilter = new Filter() {
        @Override
        public String convertResultToString(Object resultValue) {
            return ((ConstactModel)resultValue).getMobileNo();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint != null) {
                ArrayList<ConstactModel> suggestions = new ArrayList<ConstactModel>();
                HashSet hs = new HashSet();
                for (ConstactModel customer : mList) {
                    // Note: change the "contains"  to "startsWith" if you only want starting matches
                    if (customer.getMobileNo().contains(constraint.toString())) {
                        if(!suggestions.contains(customer))
                        suggestions.add(customer);
                    }
                }
                hs.addAll(suggestions);
                suggestions.clear();
                suggestions.addAll(hs);
                results.values = suggestions;
                results.count = suggestions.size();
            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results)
        {
            clear();
            if (results != null && results.count > 0) {
                // we have filtered results
                suggestionList.clear();
                suggestionList=(ArrayList)results.values;
                addAll(suggestionList);
            } /*else {
                // no filter, add entire original list back in
                addAll(mList);
            }*/
            notifyDataSetChanged();
        }
    };
}
