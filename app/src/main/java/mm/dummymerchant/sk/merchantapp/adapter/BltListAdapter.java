package mm.dummymerchant.sk.merchantapp.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.HashMap;

import mm.dummymerchant.sk.merchantapp.BaseActivity;
import mm.dummymerchant.sk.merchantapp.R;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
import mm.dummymerchant.sk.merchantapp.model.NetworkOperatorModel;

/**
 * Created by user on 7/23/2015.
 */
public class BltListAdapter extends BaseAdapter {
    Context context;
    ArrayList<HashMap<String, String>> deviceList;
    LayoutInflater inflater;
    ArrayList<NetworkOperatorModel> listOfCountryCodesAndNames;
    public BltListAdapter(Context context, ArrayList<HashMap<String, String>> deviceList)
    {
        this.context=context;
        this.deviceList=deviceList;
        this.inflater = LayoutInflater.from(context);
        listOfCountryCodesAndNames= new NetworkOperatorModel().getListOfNetworkOperatorModel(context);
    }

    @Override
    public int getCount() {
        return deviceList.size();
    }

    @Override
    public Object getItem(int position) {
        return deviceList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ViewHolder holder;
        if (convertView == null) {

            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.blt_device_list,parent, false);

            holder.txtName = (CustomTextView) convertView.findViewById(R.id.textViewName);
            holder.txtPhone = (CustomTextView) convertView.findViewById(R.id.textViewPhoneNo);
            holder.txtStatus = (CustomTextView) convertView.findViewById(R.id.textViewStatus);
            holder.imgType = (TextView) convertView.findViewById(R.id.imageViewType);
            holder.rowLayout=(RelativeLayout)convertView.findViewById(R.id.bltLayout);
            holder.flag=(ImageView)convertView.findViewById(R.id.imageViewFlag);
            holder.txtName.setSelected(true);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        String cCode=deviceList.get(position).get("CCode");
        if(cCode.equals("+95")){
            String phone="("+cCode+") "+"<font color='gray'>0</font>"+deviceList.get(position).get("Phone");
            holder.txtPhone.setText(Html.fromHtml(phone), TextView.BufferType.SPANNABLE);
            holder.flag.setBackgroundResource(R.drawable.myanmar);
        }else {
            holder.txtPhone.setText("("+cCode+") "+deviceList.get(position).get("Phone"));

            for (int i = 0; i < listOfCountryCodesAndNames.size(); i++)
            {
                if (cCode.equals(listOfCountryCodesAndNames.get(i).getCountryCode()))
                {
                    holder.flag.setBackgroundResource(listOfCountryCodesAndNames.get(i).getCountryImageId());
                    break;
                }
            }
        }
        holder.txtName.setText(deviceList.get(position).get("Name"));
        String type = deviceList.get(position).get("Type");
        holder.imgType.setVisibility(View.VISIBLE);
        if(type.equals("9900")){
        //when no category
            holder.imgType.setText("\ud83d\udc64");
            holder.rowLayout.setBackgroundResource(R.color.white);
        }else {
            holder.imgType.setText(BaseActivity.getReleventImage(type.substring(0, 2), type.substring(2, 4)));
            switch (type) {

                case "0400":
                    //holder.imgType.setText("\ud83d\ude8c");//Bus
                    //holder.imgType.setWidth(20);
                    //holder.imgType.setBackgroundResource(R.drawable.bus);
                    //holder.imgType.setText("");
                    holder.rowLayout.setBackgroundResource(R.color.intercity_bus_ticket);
                    break;
                case "0402":
                    //holder.imgType.setText("\ud83d\udea2");//Ship
//                holder.imgType.setBackgroundResource(R.drawable.ferry);
//                holder.imgType.setText("");
                    holder.rowLayout.setBackgroundResource(R.color.bus_train_ferry_button);
                    break;
                case "1219":
                    //holder.imgType.setText("\ud83d\udea7");//Toll
//                holder.imgType.setBackgroundResource(R.drawable.troll);
//                holder.imgType.setText("");
                    holder.rowLayout.setBackgroundResource(R.color.parking_toll_entraince_button);
                    break;
                case "1218":
                    //holder.imgType.setText("\ud83d\udea7");//Parking
//                holder.imgType.setBackgroundResource(R.drawable.parking);
//                holder.imgType.setText("");
                    holder.rowLayout.setBackgroundResource(R.color.parking_toll_entraince_button);
                    break;
                case "0635":
                    //Entrance
                    //holder.imgType.setText("\ud83c\udfad");
//                holder.imgType.setBackgroundResource(R.drawable.entrance);
//                holder.imgType.setText("");
                    holder.rowLayout.setBackgroundResource(R.color.parking_toll_entraince_button);
                    break;
                case "0200":
                    //Restaurants
                    //holder.imgType.setText("\ud83c\udf74" + "\ud83c\udf5c");
//                holder.imgType.setBackgroundResource(R.drawable.restaurant);
//                holder.imgType.setText("");
                    holder.rowLayout.setBackgroundResource(R.color.restaurant_hotel_taxi_bg);
                    break;
                case "2000":
                    //Hotels
                    //holder.imgType.setText("\ud83c\udfe9");
//                holder.imgType.setBackgroundResource(R.drawable.hotel);
//                holder.imgType.setText("");
                    holder.rowLayout.setBackgroundResource(R.color.restaurant_hotel_taxi_bg);
                    break;
                case "0401":
                    //Taxi
                    //holder.imgType.setText("\ud83d\ude96");
//                holder.imgType.setBackgroundResource(R.drawable.taxi);
//                holder.imgType.setText("");
                    holder.rowLayout.setBackgroundResource(R.color.restaurant_hotel_taxi_bg);
                    break;
                case "1500":
                    //Shop
                    //holder.imgType.setText("\ud83c\udfea");
//                holder.imgType.setBackgroundResource(R.drawable.shop);
//                holder.imgType.setText("");
                    holder.rowLayout.setBackgroundResource(R.color.shop_market_fuel_bg);
                    break;
                case "0001":
                    //Supermarkets
                    //holder.imgType.setText("\ud83c\udfea");
//                holder.imgType.setBackgroundResource(R.drawable.supermarket);
//                holder.imgType.setText("");
                    holder.rowLayout.setBackgroundResource(R.color.shop_market_fuel_bg);
                    break;
                case "1209":
                    //Fuel
                    //holder.imgType.setText("\u26fd");
//                holder.imgType.setBackgroundResource(R.drawable.fuel);
//                holder.imgType.setText("");
                    holder.rowLayout.setBackgroundResource(R.color.shop_market_fuel_bg);
                    break;
                case "9955"://when no category
                    holder.imgType.setText("\ud83d\udc64");
                    holder.rowLayout.setBackgroundResource(R.color.white);
                    break;
                default:
                    //Other
                    holder.rowLayout.setBackgroundResource(R.color.white);
                    // holder.imgType.setText("\ud83d\udd33");
                    break;

            }
        }
        return convertView;
    }



    private class ViewHolder
    {
        CustomTextView txtName;
        CustomTextView txtPhone;
        CustomTextView txtStatus;
        TextView imgType;
        RelativeLayout rowLayout;
        ImageView flag;
    }
}
