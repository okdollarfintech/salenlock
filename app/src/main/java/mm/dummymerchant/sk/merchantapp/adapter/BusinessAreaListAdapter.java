package mm.dummymerchant.sk.merchantapp.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;



import java.util.ArrayList;

import mm.dummymerchant.sk.merchantapp.R;
import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.Utils;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
import mm.dummymerchant.sk.merchantapp.db.DBHelper;
import mm.dummymerchant.sk.merchantapp.httpConnection.CallingApIS;
import mm.dummymerchant.sk.merchantapp.model.MerchantShopDetailsModel;
import mm.dummymerchant.sk.merchantapp.wirelessService.AutoPaymentInterface;

/**
 * Created by user on 10/26/2015.
 */
public class BusinessAreaListAdapter extends BaseAdapter {
    Context context;
    ArrayList<MerchantShopDetailsModel> areaList;
    LayoutInflater inflater;
    public static String APP_PREF = "com.jas.wallet";
    CallingApIS callingApIS;
    Dialog dialogBusinessView;
    public BusinessAreaListAdapter(Context context, ArrayList<MerchantShopDetailsModel> arrayList, CallingApIS callingApIS, Dialog dialogBusinessView){
        this.context=context;
        this.areaList=arrayList;
        this.inflater= LayoutInflater.from(context);
        this.callingApIS =callingApIS;
        this.dialogBusinessView=dialogBusinessView;
    }

    @Override
    public int getCount() {
        return areaList.size();
    }

    @Override
    public Object getItem(int position) {
        return areaList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
       // if(convertView==null){
            holder = new ViewHolder();
            convertView=inflater.inflate(R.layout.business_area_name_layout,parent,false);
            holder.txtName=(CustomTextView)convertView.findViewById(R.id.textViewName);
            holder.toggleButton=(ToggleButton)convertView.findViewById(R.id.toggleButton);
            holder.deleteButton=(ImageView)convertView.findViewById(R.id.buttonDelete);
            convertView.setTag(holder);
      //  }else{
            holder = (ViewHolder) convertView.getTag();
        holder.txtName.setSelected(true);
       // }
        String name = Utils.getNameFromatedString(areaList.get(position).getShopName());
        if(areaList.get(position).getShopStatus().equalsIgnoreCase("ON")) {
            holder.toggleButton.setChecked(true);
            holder.txtName.setText(name);
        }
        else if (areaList.get(position).getShopStatus().equalsIgnoreCase("OFF")) {
            holder.toggleButton.setChecked(false);
            holder.txtName.setText(name);
        }else  if (areaList.get(position).getShopStatus().equalsIgnoreCase("NOT_APPROVED")) {
            holder.toggleButton.setChecked(false);
            holder.txtName.setText(String.format("%s- NOT APPROVED", name));
        }else{
            holder.toggleButton.setChecked(false);
            holder.txtName.setText(name);
        }
        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Data", "Delete Clicked");
                askDeleteAreaDialog(areaList.get(position).getShopID());
            }
        });
        holder.toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.d("Data", "Toggle Clicked");
                if (isChecked) {
                    renameShopStatus(areaList.get(position).getShopID(), "ON");

                }
                else {
                    renameShopStatus(areaList.get(position).getShopID(), "OFF");

                }
            }
        });
        return convertView;
    }
    private class ViewHolder
    {
        CustomTextView txtName;
        ToggleButton toggleButton;
        ImageView deleteButton;
    }
    public void askDeleteAreaDialog(final String s) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.auto_payment_dialog);
        CustomTextView title = (CustomTextView) dialog.findViewById(R.id.textView19Title);
        CustomTextView alert_message = (CustomTextView) dialog.findViewById(R.id.textView20Message);
        CustomTextView yes = (CustomTextView) dialog.findViewById(R.id.buttonYes);
        CustomTextView no = (CustomTextView) dialog.findViewById(R.id.buttonNo);
        title.setTypeface(Utils.getFont(context), Typeface.BOLD);
        //ImageView imageViewClose = (ImageView) promptsView.findViewById(R.id.imageViewClose);
        //imageViewClose.setVisibility(View.INVISIBLE);
        title.setText(context.getString(R.string.remove_button));
        alert_message.setText(context.getString(R.string.autopayment_msg3));
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(s.equals("XXXXX")) {
                    if (DBHelper.removeShopArea()) {
                        dialogBusinessView.dismiss();
                    }
                }
                else
                callingApIS.deleteMerchantShop(s);
                dialog.dismiss();
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    public void renameShopStatus(String id, String status)
    {
        if(!id.equals("XXXXX")) {
            if (DBHelper.updateShopStatus(id, status) == 0)
                Utils.showCustomToastMsg(context, R.string.delete_shop_error, false);
            else {
                AppPreference.setCurrentShopID(context, id);
                AppPreference.setMerchantStatus(context, true);
                AutoPaymentInterface.getInstance().findMerchantStatus();
            }

        }else{
            //SHOP CHECK STATUS API WILL CALL HERE
            callingApIS.getMerchantShopStatus(AppPreference.getMyMobileNo(context));
        }

    }
    protected void showToast(Context context, String message) {
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        v.setTypeface(Utils.getFont(context));
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }
}
