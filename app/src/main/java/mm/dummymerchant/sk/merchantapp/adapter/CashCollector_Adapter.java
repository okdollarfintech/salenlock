package mm.dummymerchant.sk.merchantapp.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import mm.dummymerchant.sk.merchantapp.MapShowActivity;
import mm.dummymerchant.sk.merchantapp.R;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
import mm.dummymerchant.sk.merchantapp.model.RowItem_cashcollector;

/**
 * Created by user on 11/20/2015.
 */
public class CashCollector_Adapter extends BaseAdapter {
    //Variable Declaration
    Context context;
    Activity activity;
    ArrayList<RowItem_cashcollector> AlertList;
    RowItem_cashcollector m;
    private String getloc1,getloc2,getloc3,templatevalue;
    public CashCollector_Adapter(Context context, ArrayList<RowItem_cashcollector> AlertList)
    {
        this.context = context;
        this.AlertList = AlertList;
    }
    private class ViewHolder
    {
        TextView time,name,phone,amount,commission,type,loc1,loc2,loc3;
        ImageView mapview;
        TextView generatorname,generatorno,templete,templete_gettext,text_gettext;
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return AlertList.size();
    }
    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return AlertList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return AlertList.indexOf(getItem(position));
    }
    @SuppressLint("InflateParams")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        final ViewHolder holder;
        LayoutInflater mInflater = (LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.rowitem_scandetails, null);
            holder = new ViewHolder();
            holder.time = (TextView) convertView.findViewById(R.id.time);
            holder.name = (TextView) convertView.findViewById(R.id.name);
            holder.phone = (TextView) convertView.findViewById(R.id.phone);
            holder.generatorname = (TextView) convertView.findViewById(R.id.name1);
            holder.generatorno = (TextView) convertView.findViewById(R.id.phone1);
            holder.amount = (TextView) convertView.findViewById(R.id.amount);
            holder.commission = (TextView) convertView.findViewById(R.id.commission);
            holder.type = (TextView) convertView.findViewById(R.id.type);
            holder.loc1 = (TextView) convertView.findViewById(R.id.loc1);
            holder.loc2 = (TextView) convertView.findViewById(R.id.loc2);
            holder.loc3 = (TextView) convertView.findViewById(R.id.loc3);
            holder.mapview = (ImageView) convertView.findViewById(R.id.mapview);
            holder.templete = (TextView) convertView.findViewById(R.id.templete);
            holder.templete_gettext = (TextView) convertView.findViewById(R.id.templete_gettext);
            holder.text_gettext = (TextView) convertView.findViewById(R.id.text_gettext);

            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }

        m = AlertList.get(position);
        holder.time.setText(m.GetGenerate_Time());
        holder.name.setText(m.GetScanner_Name());
        holder.phone.setText(m.GetScanner_No());
        holder.generatorname.setText(m.GetGenerater_Name());
        holder.generatorno.setText(m.GetGenerater_No());
        holder.amount.setText(m.GetLog_Amount());
        holder.commission.setText("STATUS : " + m.GetLog_Status());
        holder.type.setText("TYPE : " + m.GetLog_Paid_Status());
        holder.loc1.setText(m.GetScanning_Location());
        holder.loc2.setText(m.GetGenerate_Location());
        holder.loc3.setText(m.GetOTP_Enter_Location());
        holder.templete_gettext.setText(m.getTemplateValue());
        holder.text_gettext.setText(m.getTextValue());


        final String Get_Temp = holder.templete_gettext.getText().toString();
        String Get_Text = holder.text_gettext.getText().toString();
        System.out.println(" Get TemplateValue " + Get_Temp);
        System.out.println(" Get TextValue " + Get_Text);

            if(Get_Temp.equals("[]")|| Get_Temp.equals("") || Get_Temp.equals("null") )
            {
                holder.templete.setText("[]");
                holder.templete.setVisibility(View.GONE);
            }
            else
            {
                String output = divider(Get_Temp);
                holder.templete.setText(output);
                holder.templete.setSelected(true);
                holder.templete.setVisibility(View.VISIBLE);
            }

        if( Get_Text.equals("") || Get_Text.equals("null") || Get_Text.equals("?") )
        {
            holder.text_gettext.setVisibility(View.GONE);
            holder.text_gettext.setText("-");
        }
        else {
            holder.text_gettext.setSelected(true);
            holder.text_gettext.setVisibility(View.VISIBLE);
        }

        getloc1 = holder.loc1.getText().toString();
        getloc2 = holder.loc2.getText().toString();
        getloc3 = holder.loc3.getText().toString();
        final String Split1[] = getloc1.split(",");
        final String Split2[] = getloc2.split(",");
        final String Split3[] = getloc3.split(",");

        int textColor = m.GetLog_Paid_Status().equals("Received") ? R.color.green : R.color.nav_normal_color;
        convertView.setBackgroundColor(context.getResources().getColor(textColor));
        holder.mapview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //System.out.println("Mapview Clicked..!");
                if (!Split1[0].equals("0.0") || !Split2[0].equals("0.0")) {
                    Intent i = new Intent(context, MapShowActivity.class);
                    i.putExtra("Loc1", holder.loc1.getText().toString());
                    i.putExtra("Loc2", holder.loc2.getText().toString());
                    i.putExtra("Loc3", "0.0,0.0");
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(i);
                } else {
                    Toast.makeText(context, "Location Not Available", Toast.LENGTH_SHORT).show();
                }
            }
        });


        convertView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                    if(!holder.templete.getText().equals("[]"))
                    {
                        final Dialog dialog = new Dialog(context);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.dialog);
                        dialog.show();
                        LinearLayout container = (LinearLayout) dialog.findViewById(R.id.container);
                        CustomTextView close = (CustomTextView) dialog.findViewById(R.id.close);
                        String i = Get_Temp.replaceAll("\\[", "").replaceAll("\\]", "");
                        String k = i.replaceAll("\\....", "").replaceAll("\\....", "");
                        String dataText[] = k.split(",");
                        int len = dataText.length;

                        for (int j = 0; j < len; j++) {
                            String dataText1[] = dataText[j].split("/");
                            LinearLayout Container_id = container;
                            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            View addView = layoutInflater.inflate(R.layout.row_dialog, null);
                            CustomTextView item1 = (CustomTextView) addView.findViewById(R.id.itemvalue);
                            CustomTextView qty1 = (CustomTextView) addView.findViewById(R.id.qtyvalue);
                            CustomTextView price1 = (CustomTextView) addView.findViewById(R.id.pricevalue);
                            Container_id.addView(addView);
                            item1.setText(dataText1[0]);
                            qty1.setText(dataText1[1]);
                            price1.setText(dataText1[2]);
                        }
                        close.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });
            }
            }
        });

        return convertView;
    }

    private String divider(String temp)
    {
        String i = temp.replaceAll("\\[", "").replaceAll("\\]", "");
        String k = i.replaceAll("\\....", "").replaceAll("\\....", "");
        String dataText[] = k.split(",");
        int len = dataText.length;
        String output = "" ;
       /* for (int j = 0; j < len; j++)
        {
            String dataText1[] = dataText[j].split("/");
            output = " ITEM : " + dataText1[0] + "  QUANTITY : " + dataText1[1] + " PRICE : " + dataText1[2] + "....";
        }*/
        return output;
    }

    private void PrintLogDetails()
    {
        Log.d("time ", m.GetGenerate_Time());
        Log.d("name ", m.GetScanner_Name());
        Log.d("phone ", m.GetScanner_No());
        Log.d("generatorname ", m.GetGenerater_Name());
        Log.d("generatorno ", m.GetGenerater_No());
        Log.d("amount ", m.GetLog_Amount());
        Log.d("STATUS ", m.GetLog_Status());
        Log.d("type ", m.GetLog_Paid_Status());
        Log.d("loc1 ", m.GetScanning_Location());
        Log.d("loc2 ", m.GetGenerate_Location());
        Log.d("loc3", m.GetOTP_Enter_Location());
        Log.d("Textdata", m.getTextValue());
        Log.d("Templatedata", m.getTemplateValue());
    }
}
