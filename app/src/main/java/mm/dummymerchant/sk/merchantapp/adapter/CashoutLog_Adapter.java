package mm.dummymerchant.sk.merchantapp.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import mm.dummymerchant.sk.merchantapp.R;
import mm.dummymerchant.sk.merchantapp.model.CashOutModel;

/**
 * Created by user on 11/28/2015.
 */
public class CashoutLog_Adapter extends BaseAdapter
{
    //Variable Declaration
    Context context;
    Activity activity;
    // int User_id;
    ArrayList<CashOutModel> AlertList;
    // private int[] bgColors = new int[] { R.color.list_bg_1, R.color.list_bg_2 };

    public CashoutLog_Adapter(Context context, ArrayList<CashOutModel> AlertList)
    {
        this.context = context;
        this.AlertList = AlertList;
    }
    private class ViewHolder
    {
        TextView Time,Name,Amount,Com_type,Com_Amt,FinalAmt;
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return AlertList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return AlertList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return AlertList.indexOf(getItem(position));
    }


    @SuppressLint("InflateParams")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        final ViewHolder holder;

        LayoutInflater mInflater = (LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
        {
            convertView = mInflater.inflate(R.layout.row_item_cashoutlog, null);
            holder = new ViewHolder();

            holder.Name = (TextView) convertView.findViewById(R.id.name);
            holder.Time = (TextView) convertView.findViewById(R.id.time);
            holder.Amount = (TextView) convertView.findViewById(R.id.amount);
            holder.Com_type = (TextView) convertView.findViewById(R.id.comtype);
            holder.Com_Amt = (TextView) convertView.findViewById(R.id.comamt);
            holder.FinalAmt = (TextView) convertView.findViewById(R.id.finalamt);

            convertView.setTag(holder);
        }

        else
        {
            holder = (ViewHolder) convertView.getTag();
        }

        CashOutModel m = AlertList.get(position);

        /*System.out.println("Time : " +m.getTime());
        System.out.println("Name : "+m.getName());
        System.out.println("Amount : "+m.getamount());
        System.out.println("Com_type : " +m.getComm_Type());
        System.out.println("Com_Amt : "+m.getcomm_amt());
        System.out.println("FinalAmt : "+m.getFinalAmout());*/

        holder.Time.setText(m.getTime());
        holder.Name.setText(m.getName());
        holder.Amount.setText(m.getamount());
        holder.Com_type.setText(m.getComm_Type());
        holder.Com_Amt.setText(m.getcomm_amt());
        holder.FinalAmt.setText(m.getFinalAmout());

        return convertView;

    }

}
