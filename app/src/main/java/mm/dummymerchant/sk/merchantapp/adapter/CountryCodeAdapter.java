package mm.dummymerchant.sk.merchantapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

import mm.dummymerchant.sk.merchantapp.R;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
import mm.dummymerchant.sk.merchantapp.model.NetworkOperatorModel;

/**
 * Created by kapil on 14/09/15.
 */
public class CountryCodeAdapter extends BaseAdapter {
    private Context context;
    private int[] listOfImages;
    private ArrayList<NetworkOperatorModel> listOfCountryCodeAndName;
    public CountryCodeAdapter(Context context, int[] listOfImages, ArrayList<NetworkOperatorModel> listOfCountryCodeAndName) {
        this.context=context;
        this.listOfCountryCodeAndName=listOfCountryCodeAndName;
        this.listOfImages=listOfImages;
    }

    @Override
    public int getCount() {
        return listOfCountryCodeAndName.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        ViewHolder holder = null;
        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.country_row, null);
            holder = new ViewHolder();
            holder.tvCountryCode = (CustomTextView) convertView.findViewById(R.id.tv_country_code);
            holder.tvCountryName = (CustomTextView) convertView.findViewById(R.id.tv_country_name);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }
        //holder.tvCountryCode.setText(listOfCountryCodeAndName.get(position).getCountryCode());
        holder.tvCountryName.setText(listOfCountryCodeAndName.get(position).getCountryName()+"   ("+listOfCountryCodeAndName.get(position).getCountryCode()+")");
        holder.tvCountryCode.setBackgroundResource(listOfCountryCodeAndName.get(position).getCountryImageId());
        return convertView;
    }
    private static class ViewHolder{
        CustomTextView tvCountryName, tvCountryCode;

    }
}
