package mm.dummymerchant.sk.merchantapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;


import mm.dummymerchant.sk.merchantapp.OkDollarTransactionsActivity;
import mm.dummymerchant.sk.merchantapp.R;
import mm.dummymerchant.sk.merchantapp.SliderScreen;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
import mm.dummymerchant.sk.merchantapp.model.dashboard_model;

/**
 * Created by Dell on 6/16/2016.
 */
public class Dashboard_adapter extends BaseAdapter
{
    Context context;
    ArrayList<dashboard_model> alist;
    dashboard_model model;
    int from;

    public Dashboard_adapter(Context context, ArrayList<dashboard_model> alist,int from) {
        this.context = context;
        this.alist = alist;
        this.from=from;

    }

    @Override
    public int getCount() {
        return alist.size();
    }

    @Override
    public Object getItem(int i) {
        return alist.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View convertView , ViewGroup viewGroup)
    {
        ViewHolder holder = null;
        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        if(convertView ==null)
        {
            convertView = mInflater.inflate(R.layout.custom_dashboard_gridview, null);
            holder = new ViewHolder();
            holder.ll_grid=(LinearLayout)convertView.findViewById(R.id.ll_custom_grid);
            holder.txt_name=(CustomTextView)convertView.findViewById(R.id.cus_grid_name);
            holder.imageView=(ImageView)convertView.findViewById(R.id.cus_grid_image);
            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }
        Drawable d=null;
int alpos=Integer.parseInt(alist.get(i).getId());

        if(alpos==0)
            d=context.getResources().getDrawable(R.drawable.gridview_rectangle_drawable1);
        else  if(alpos==1)
            d=context.getResources().getDrawable(R.drawable.gridview_rectangle_drawble2);
       else if(alpos==2)
            d=context.getResources().getDrawable(R.drawable.gridview_rectangle_drawable3);
       else if(alpos==3)
            d=context.getResources().getDrawable(R.drawable.gridview_rectangle_drawable4);
        else if(alpos==4)
            d=context.getResources().getDrawable(R.drawable.gridview_rectangle_drawable5);
        else if(alpos==5)
            d=context.getResources().getDrawable(R.drawable.gridview_rectangle_drawable6);
        else if(alpos==6)
            d=context.getResources().getDrawable(R.drawable.gridview_rectangle_drawable7);
        else if(alpos==7)
            d=context.getResources().getDrawable(R.drawable.gridview_rectangle_drawable8);
        else if(alpos==8) {
            d = context.getResources().getDrawable(R.drawable.gridview_rectangle_drawable9);
            holder.txt_name.setTextColor(context.getResources().getColor(R.color.white));
        }

        holder.ll_grid.setBackground(d);

        model=alist.get(i);
        holder.txt_name.setSelected(true);
        holder.txt_name.setText(model.getName());
        holder.imageView.setImageResource(model.getImage());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(from==1)
                ((SliderScreen)context).callBasedOnIndex(alist.get(i).getId());
                else
                {
                    ((OkDollarTransactionsActivity)context).callBasedOnIndex(alist.get(i).getId());
                }
            }
        });

        return convertView;
    }

    private static class ViewHolder {
        CustomTextView txt_name;
        ImageView imageView;
        LinearLayout ll_grid;
    }


}
