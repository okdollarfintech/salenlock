package mm.dummymerchant.sk.merchantapp.adapter;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;


import java.util.ArrayList;

import mm.dummymerchant.sk.merchantapp.model.TransationModel;

public abstract class EndlessRecyclerOnScrollListener extends
        RecyclerView.OnScrollListener {
    public static String TAG = EndlessRecyclerOnScrollListener.class
            .getSimpleName();
    int firstVisibleItem, visibleItemCount, totalItemCount;
    private boolean loading = false;
  

    ArrayList<TransationModel> list;

    private LinearLayoutManager mLinearLayoutManager;
    private int pastVisiblesItems=0;
    private int previousTotal=0;
    private int visibleThreshold=2;

    public EndlessRecyclerOnScrollListener(
            LinearLayoutManager linearLayoutManager,ArrayList<TransationModel> list) {
        this.mLinearLayoutManager = linearLayoutManager;
        this.list=list;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        if(dy > 0) //check for scroll down
        {
            visibleItemCount = mLinearLayoutManager.getChildCount();
            totalItemCount = mLinearLayoutManager.getItemCount();
            pastVisiblesItems = mLinearLayoutManager.findLastVisibleItemPosition();
            if (totalItemCount <= pastVisiblesItems + visibleThreshold) {
                onLoadMore();
            }
        }
    }

    public abstract void onLoadMore();


}