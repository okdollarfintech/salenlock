package mm.dummymerchant.sk.merchantapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

import mm.dummymerchant.sk.merchantapp.R;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;

/**
 * Created by Dell on 1/13/2016.
 */
public class PopupAdapter extends BaseAdapter
{
    Context con;
    String[] starr_options;

    public PopupAdapter(Context con, String[] starr_options)
    {
        this.con = con;
        this.starr_options = starr_options;
    }

    @Override
    public int getCount()
    {
        return starr_options.length;
    }

    @Override
    public Object getItem(int i) {
        return starr_options[i];
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup)
    {
        ViewHolder holder=null;
        if(view==null)
        {
            holder=new ViewHolder();
            view= LayoutInflater.from(con).inflate(R.layout.custom_popup,viewGroup,false);
            holder.tv_name=(CustomTextView)view.findViewById(R.id.tv_popup_name);
            view.setTag(holder);
        }
        else
        {
           holder=(ViewHolder)view.getTag();
        }

        holder.tv_name.setText(this.starr_options[i]);

        return view;
    }

    static class ViewHolder
    {
        CustomTextView tv_name;
    }
}
