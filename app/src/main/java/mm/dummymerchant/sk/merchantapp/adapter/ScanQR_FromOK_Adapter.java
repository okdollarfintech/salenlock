package mm.dummymerchant.sk.merchantapp.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import mm.dummymerchant.sk.merchantapp.MapShowActivity;
import mm.dummymerchant.sk.merchantapp.R;
import mm.dummymerchant.sk.merchantapp.model.RowItem_ScanFrom_OK;

/**
 * Created by user on 1/22/2016.
 */
public class ScanQR_FromOK_Adapter extends BaseAdapter
{
    Context context;
    Activity activity;
    ArrayList<RowItem_ScanFrom_OK> AlertList;
    RowItem_ScanFrom_OK m;
    private String getloc1,getloc2,getloc3;

public ScanQR_FromOK_Adapter(Context context, ArrayList<RowItem_ScanFrom_OK> AlertList)
{
        this.context = context;
        this.AlertList = AlertList;
}

private class ViewHolder
{
    TextView time,name,phone,amount,commission,type,loc1,loc2,loc3;
    ImageView mapview;
    TextView generatorname,generatorno;
}
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return AlertList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return AlertList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return AlertList.indexOf(getItem(position));
    }
    @SuppressLint("InflateParams")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        final ViewHolder holder;
        LayoutInflater mInflater = (LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.row_item_okscandetails, null);
            holder = new ViewHolder();

            holder.time = (TextView) convertView.findViewById(R.id.time);
            holder.name = (TextView) convertView.findViewById(R.id.name);
            holder.phone = (TextView) convertView.findViewById(R.id.phone);
            holder.generatorname = (TextView) convertView.findViewById(R.id.name1);
            holder.generatorno = (TextView) convertView.findViewById(R.id.phone1);
            holder.amount = (TextView) convertView.findViewById(R.id.amount);
            holder.commission = (TextView) convertView.findViewById(R.id.commission);
            holder.loc1 = (TextView) convertView.findViewById(R.id.loc1);
            holder.loc2 = (TextView) convertView.findViewById(R.id.loc2);
            holder.mapview = (ImageView) convertView.findViewById(R.id.mapview);

            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }

        m = AlertList.get(position);
        holder.time.setText(m.GetOK_DATE());
        holder.name.setText(m.GetOK_RECEIVER_NAME_No());
        holder.phone.setText(m.GetOK_SENDER_NO());
        holder.generatorname.setText("GENDER : "+m.GetOK_GENDER());
        holder.generatorno.setText("AGE : " + m.GetOK_AGE());
        holder.amount.setText(m.GetOK_AMOUNT());
        holder.commission.setText("STATUS : " + m.GetOK_STATUS());
        holder.loc1.setText( m.GetSCANNING_LOCATION());
        holder.loc2.setText(m.GetGENERATE_LOCATION());

        getloc1 = holder.loc1.getText().toString();
        getloc2 = holder.loc2.getText().toString();
        final String Split1[] = getloc1.split(",");
        final String Split2[] = getloc2.split(",");

        int textColor = m.GetOK_STATUS().equals("SUCCESS") ? R.color.green : R.color.nav_normal_color;
        convertView.setBackgroundColor(context.getResources().getColor(textColor));
        holder.mapview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                System.out.println("Mapview Clicked..!");
                if (!Split1[0].equals("0.0") || !Split2[0].equals("0.0"))
                {
                        Intent i = new Intent(context, MapShowActivity.class);
                        i.putExtra("Loc1", holder.loc1.getText().toString());
                        i.putExtra("Loc2", holder.loc2.getText().toString());
                        i.putExtra("Loc3", "0.0,0.0");
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(i);
                }
                else {
                    Toast.makeText(context, "Location Not Available", Toast.LENGTH_SHORT).show();
                }
            }
        });
        return convertView;
    }
}
