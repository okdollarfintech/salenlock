package mm.dummymerchant.sk.merchantapp.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import mm.dummymerchant.sk.merchantapp.R;
import mm.dummymerchant.sk.merchantapp.model.RowItem_ScanFrom_OK;

/**
 * Created by user on 1/22/2016.
 */
public class ScanQR_FromOK_LogDetails_Adapter extends BaseAdapter
{
    Context context;
    Activity activity;
    ArrayList<RowItem_ScanFrom_OK> AlertList;

    public ScanQR_FromOK_LogDetails_Adapter(Context context, ArrayList<RowItem_ScanFrom_OK> AlertList)
    {
        this.context = context;
        this.AlertList = AlertList;
    }
    private class ViewHolder
    {
        TextView transid,Amount,receiverno,receivername,sendername,transtype,scanning_time,generate_time,gender,age,kickback_item,loyality_item;
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return AlertList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return AlertList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return AlertList.indexOf(getItem(position));
    }


    @SuppressLint("InflateParams")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        final ViewHolder holder;

        LayoutInflater mInflater = (LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
        {
            convertView = mInflater.inflate(R.layout.row_item_ok_logdetails, null);
            holder = new ViewHolder();

            holder.transid = (TextView) convertView.findViewById(R.id.transid);
            holder.Amount = (TextView) convertView.findViewById(R.id.Amount);
            holder.receiverno = (TextView) convertView.findViewById(R.id.receiverno);
            holder.receivername = (TextView) convertView.findViewById(R.id.receivername);
            holder.sendername = (TextView) convertView.findViewById(R.id.sendername);
            holder.transtype = (TextView) convertView.findViewById(R.id.transtype);
            holder.scanning_time = (TextView) convertView.findViewById(R.id.scanning_time);
            holder.generate_time = (TextView) convertView.findViewById(R.id.generate_time);
            holder.gender = (TextView) convertView.findViewById(R.id.gender_item);
            holder.age = (TextView) convertView.findViewById(R.id.age_item);
            holder.kickback_item = (TextView) convertView.findViewById(R.id.kickback_item);
            holder.loyality_item = (TextView) convertView.findViewById(R.id.loyality_item);

            convertView.setTag(holder);
        }

        else
        {
            holder = (ViewHolder) convertView.getTag();
        }

        RowItem_ScanFrom_OK m = AlertList.get(position);

        holder.transid.setText(m.GetOK_TRANSID());
        holder.Amount.setText(m.GetOK_AMOUNT());
        holder.receiverno.setText(m.GetOK_RECEIVER_NO());
        holder.receivername.setText(m.GetOK_RECEIVER_NAME_No());
        holder.sendername.setText(m.GetOK_SENDER_NO());
        holder.transtype.setText(m.GetOK_TRANS_TYPE());
        holder.scanning_time.setText(m.GetOK_TIME_STAMP());
        holder.generate_time.setText(m.GetOK_DATE());
        holder.gender.setText(m.GetOK_GENDER());
        holder.age.setText(m.GetOK_AGE());
        holder.kickback_item.setText(m.GetOK_KICK());
        holder.loyality_item.setText(m.GetOK_LOCALITY());

        return convertView;

    }

}
