package mm.dummymerchant.sk.merchantapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import mm.dummymerchant.sk.merchantapp.MapActivity;
import mm.dummymerchant.sk.merchantapp.R;
import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.Constant;
import mm.dummymerchant.sk.merchantapp.Utils.GetNumberWithCountryCode;
import mm.dummymerchant.sk.merchantapp.Utils.Utils;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
import mm.dummymerchant.sk.merchantapp.customView.RoundedImageView;
import mm.dummymerchant.sk.merchantapp.db.DBHelper;
import mm.dummymerchant.sk.merchantapp.model.CashierModel;
import mm.dummymerchant.sk.merchantapp.model.ConstactModel;
import mm.dummymerchant.sk.merchantapp.model.NetworkOperatorModel;
import mm.dummymerchant.sk.merchantapp.model.TransationModel;

public class SimpleAdapter extends RecyclerView.Adapter<SimpleAdapter.SimpleViewHolder> implements Constant {

    public static OnItemClickListener onItemClickListener;
    private final Context mContext;
    private final GetNumberWithCountryCode mCountryCode;
    public ArrayList<Integer> header = new ArrayList<>();
    public ArrayList<Integer> marked = new ArrayList<>();
    public ArrayList<NetworkOperatorModel> listOfCountryCodesAndNames;
    public ArrayList<TransationModel> list;
    DBHelper db;
    SpannableString span2;
    String last_item = "";
    private List<String> mData;
    private List<String> mDates;

    public SimpleAdapter(Context context, ArrayList<TransationModel> list, List<String> myDateList) {
        mContext = context;
        this.list = list;
        mDates = myDateList;
        db = new DBHelper(mContext);
        span2 = new SpannableString(mContext.getResources().getString(R.string.kyats));
        header.clear();
        mCountryCode = GetNumberWithCountryCode.getInstance();
        listOfCountryCodesAndNames = new NetworkOperatorModel().getListOfNetworkOperatorModel(context);

    }

    public static String convertValidNumber(String number) {
        if (number.contains("+"))
            number = number.replace("+", "00");
        if (number.contains(" "))
            number= number.replace(" ", "");
        if (number.contains("-"))
            number = number.replace("-", "");
        return number;
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        onItemClickListener = listener;
    }

    public void add(String s, int position) {
        position = position == -1 ? getItemCount() : position;
        mData.add(position, s);
        notifyItemInserted(position);
    }

    public void remove(int position) {
        if (position < getItemCount()) {
            mData.remove(position);
            notifyItemRemoved(position);
        }
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.row_layout_section, viewGroup, false);
        return new SimpleViewHolder(v, viewType);
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder viewHolder, final int i) {

        viewHolder.mTransType.setTag(i);
        //System.out.println(list.get(i).getLatLong());
        Converter(list.get(i).getDestination(), viewHolder.flag, viewHolder.mNumber);
        if (!list.get(i).getTrnastionType().equals("PAYTO")) {
            int length = list.get(i).getDestination().length();
            String number = list.get(i).getDestination();
            int limit = length - 5;
            StringBuilder stringBuilder = new StringBuilder("");
            for (int j = length - 1; j > 0; j--) {
                if (j > limit) {
                    stringBuilder.append(number.charAt(j));
                } else {
                    stringBuilder.append("X");
                }
            }
            viewHolder.mNumber.setText("    " + stringBuilder.reverse().toString());
        } else {
            Converter(list.get(i).getDestination(), viewHolder.flag, viewHolder.mNumber);
        }
        viewHolder.mTransID.setText(list.get(i).getTransatId());
        viewHolder.mTransType.setVisibility(View.GONE);
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", Locale.ENGLISH);
        viewHolder.mDatess.setText(dateFormatter.format(list.get(i).getmTranscationDate()));
        /*if (list.get(i).getOpen().equals("close")) {
            viewHolder.mCommentLayoutRow.setVisibility(View.GONE);
        } else {
            viewHolder.mCommentLayoutRow.setVisibility(View.VISIBLE);
        }*/
        if (!Utils.isEmpty(list.get(i).getDestinationPersonName().trim()))
            viewHolder.tvNmae.setText(list.get(i).getDestinationPersonName());
        else
            viewHolder.tvNmae.setText("Unknown");
        /*if (!list.get(i).getReceivedKickback().isEmpty()) {
            viewHolder.tvchashback.setText(mContext.getResources().getString(R.string.kickback) + ": " + Utils.formatedAmountAbhi(list.get(i).getReceivedKickback(), mContext) + " " + mContext.getResources().getString(R.string.kyats));
            viewHolder.tvchashback.setVisibility(View.VISIBLE);
        }else
            viewHolder.mCommentLayoutRow.setVisibility(View.VISIBLE);*/ //Please donot touch it
            String number = list.get(i).getDestination().substring(4);
           // ConstactModel contactModel = db.getSelectedContact(number);
        ///////
        List<ConstactModel> listt = new ArrayList<ConstactModel>();
        Utils utils=new Utils();
        String jsondata=DBHelper.getJsonContacts();
        listt=utils.getContactListFromJsonString(jsondata);
        ////////////
       ConstactModel contactModel = utils.getSelectedContactModel(number,listt);

        if (!contactModel.getPhotoUrl().equals("")) {
            Picasso.with(mContext).load(Uri.parse(contactModel.getPhotoUrl())).placeholder(mContext.getResources().getDrawable(R.drawable.default_user_icon)).into(viewHolder.mUserPhoto);
        } else {
            Drawable d = mContext.getResources().getDrawable(R.drawable.default_user_icon);
            d.setBounds(0, 0, 80, 80);
            viewHolder.mUserPhoto.setImageDrawable(d);
        }
        if (!contactModel.getName().trim().equals("")) {
           // viewHolder.mMap.setVisibility(View.GONE);
            viewHolder.tvNmae.setText(contactModel.getName());
        } else {
            viewHolder.tvNmae.setText("Unknown");
        }
        if (list.get(i).getLatLong().trim().equals("") && list.get(i).getCellID().isEmpty()) {
            Log.e("map", list.get(i).getCellID());
            viewHolder.mMap.setVisibility(View.GONE);
        } else {
            if (!list.get(i).getTrnastionType().equals("PAYTO")) {
                viewHolder.mMap.setVisibility(View.GONE);
            }else
            {
                viewHolder.mMap.setVisibility(View.VISIBLE);
            }
        }
        if (list.get(i).getCount().equals("1")) {
            viewHolder.mLinearLayout.setBackgroundColor(mContext.getResources().getColor(R.color.lightt_blue));
        } else {
            viewHolder.mLinearLayout.setBackgroundColor(mContext.getResources().getColor(R.color.lightt_green));
        }

        viewHolder.mLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (list.get(i).getOpen().equals("close")) {
//                    list.get(i).setOpen("open");
//                    viewHolder.mCommentLayoutRow.setVisibility(View.VISIBLE);
//                } else if (list.get(i).getReceivedKickback().isEmpty()) {
//                    list.get(i).setOpen("close");
//                    viewHolder.mCommentLayoutRow.setVisibility(View.GONE);
//                }
                viewHolder.mLinearLayout.setBackgroundColor(mContext.getResources().getColor(R.color.lightt_blue));
                list.get(i).setCount("1");
                db.updateColum(list.get(i));
                notifyDataSetChanged();

            }
        });

        viewHolder.mMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent call = new Intent(mContext, MapActivity.class);
                call.putExtra(BUNDLE_MAP_NUMBER, list.get(i).getDestination());
                call.putExtra(BUNDLE_MAP_AMOUNT, list.get(i).getAmount());
                call.putExtra(BUNDLE_MAP_LAT_LONG, list.get(i).getLatLong());
                call.putExtra(BUNDLE_MAP_CELL_ID, list.get(i).getCellID());
                call.putExtra(Constant.MERCHANT_MOBILE_NO,list.get(i).getDestination());
                mContext.startActivity(call);
            }
        });


        //for setting view background based on cashierId
        if(AppPreference.getMasterOrCashier(mContext).equalsIgnoreCase(MASTER))
        {
            String cashierId = list.get(i).getCashierId();

            if (!cashierId.equalsIgnoreCase(MASTER_TAG))
            {
                DBHelper dbhelper = new DBHelper(mContext);
                CashierModel cashmodel = dbhelper.getCashierModel(cashierId);
                dbhelper.close();

                if (cashmodel != null)
                {
                    String color = cashmodel.getColorCode();
                    int colorCode = Integer.parseInt(color);

                    String cashierno=cashmodel.getCashier_Number();
                    cashierno=cashierno.substring(4);
                    cashierno="0"+cashierno;

                    viewHolder.tv_cashierNo.setText(cashierno);
                    viewHolder.tv_cashiername.setText(cashmodel.getCashier_Name());

                    //viewHolder.mLinearLayout.setBackgroundColor(colorCode);
                }

            }
            else
            {
                viewHolder.tv_cashierNo.setText("");
                viewHolder.tv_cashiername.setText("");
                viewHolder.tv_cashier_details.setText("");
            }

        }
        else
        {
            viewHolder.tv_cashier_details.setVisibility(View.GONE);
            viewHolder.tv_cashierNo.setVisibility(View.GONE);
            viewHolder.tv_cashiername.setVisibility(View.GONE);
        }

        //.......................


        if (list.get(i).getDbAmount().equals("")) {

        } else {
            viewHolder.tvAmount.setText(Utils.formatedAmountAbhi(list.get(i).getDbAmount(), mContext) + " " + mContext.getResources().getString(R.string.kyats));
        }

          viewHolder.tvCommenRow.setText("Comments: " + list.get(i).getComments().replace("~comments~",""));


       // if (!list.get(i).getmWalletbalance().equals("")) {
            viewHolder.tvBalance.setText(Utils.formatedAmountAbhi(list.get(i).getmWalletbalance(), mContext));
            //iewHolder.tvBalance.setText("");
      //  } else {
      //      viewHolder.tvBalance.setText("");
      //  }


        for (String date : mDates) {
            if (date.equals(list.get(i).getTransatId())) {

                View v = LayoutInflater.from(mContext)
                        .inflate(R.layout.header_list_layout, null);

                TextView headers = (TextView) v.findViewById(R.id.section_header_text);

                if (viewHolder.addHeaderlayout.getChildCount() > 0) {
                } else {

                }


            }

        }


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    private void setAnimation(View viewToAnimate, int position) {
        int lastPosition = 0;
        if (position > lastPosition) {
        }
    }

    public void Converter(String no, ImageView v, CustomTextView ctv) {

        if (no == null) {

            ctv.setText(no);
            Drawable img = mContext.getResources().getDrawable(R.drawable.transparent_img);
            img.setBounds(0, 0, 30, 30);;
            ctv.setCompoundDrawables(img, null, null, null);
            return;

        }
        try {

            if (!(no.contains("X") || no.contains("x"))) {

                String data[] = mCountryCode.getCountryCodeNameAndFlagFromNumberLogin(no, mContext);
                String countryCode = data[0];
                String countryName = data[1];
                int flag = Integer.parseInt(data[2]);
                String number = data[3];
                ctv.setText("  (" + countryCode + ")" + number);
                Drawable img = mContext.getResources().getDrawable(flag);
                if (img != null) {
                    img.setBounds(0, 0, 60, 60);
                }
                ctv.setCompoundDrawables(img, null, null, null);
            } else {

                ctv.setText("XXXXXXXXXX");
                Drawable imgg = mContext.getResources().getDrawable(R.drawable.transparent_img);
                if (imgg != null) {
                    imgg.setBounds(0, 0, 60, 60);
                }
                ctv.setCompoundDrawables(imgg, null, null, null);
            }


            Drawable img = mContext.getResources().getDrawable(R.drawable.blu);
            if (img != null) {
                img.setBounds(0, 0, 40, 40);
            }
            v.setImageDrawable(img);

        }catch (Exception e){}
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public CustomTextView mNumber, mTransType, mTransID, mDatess, tvAmount, tvBalance, tvNmae, tvCommenRow, tvchashback;
        public LinearLayout mLinearLayout, addHeaderlayout, hide_open;
        public ImageView flag, mMap;
        public RoundedImageView mUserPhoto;
        private CardView mCard;
        RelativeLayout mCommentLayoutRow;
        CustomTextView tv_cashiername,tv_cashierNo,tv_cashier_details;


        public SimpleViewHolder(View v, int position) {
            super(v);
            mNumber = (CustomTextView) v.findViewById(R.id.tv_mob_no);
            flag = (ImageView) v.findViewById(R.id.flag);

            mUserPhoto = (RoundedImageView) v.findViewById(R.id.received_money_imagge_View_user_image);
            tvNmae = (CustomTextView) v.findViewById(R.id.tv_trans_name);
            mTransType = (CustomTextView) v.findViewById(R.id.tv_trans_type);
            mTransID = (CustomTextView) v.findViewById(R.id.tv_trans_id);
            mDatess = (CustomTextView) v.findViewById(R.id.tv_date);
            mLinearLayout = (LinearLayout) v.findViewById(R.id.linear);
            tvAmount = (CustomTextView) v.findViewById(R.id.tv_amount);
            tvBalance = (CustomTextView) v.findViewById(R.id.tv_comment);
            addHeaderlayout = (LinearLayout) v.findViewById(R.id.addHeaderlayout);
            mMap = (ImageView) v.findViewById(R.id.transcation_history_google_map);
            mCommentLayoutRow = (RelativeLayout) v.findViewById(R.id.transcation_history_comment_hide_and_show_layout);
            tvCommenRow = (CustomTextView) v.findViewById(R.id.last_trasncation_textview_row_comment);
            tvchashback = (CustomTextView) v.findViewById(R.id.last_trasncation_textview_row_cashback);
            mCard = (CardView) v.findViewById(R.id.cv);
            tvchashback.setTypeface(Utils.getFont(mContext), Typeface.BOLD);
            tvchashback.setVisibility(View.INVISIBLE);

            tv_cashier_details=(CustomTextView)v.findViewById(R.id.cashier_details);
            tv_cashiername=(CustomTextView)v.findViewById(R.id.cashier_name);
            tv_cashierNo=   (CustomTextView)v.findViewById(R.id.cashier_contact);

        }


        @Override
        public void onClick(View view) {
            if (onItemClickListener != null) {

                onItemClickListener.onItemClick(view, getAdapterPosition());
            }
        }
    }
} 