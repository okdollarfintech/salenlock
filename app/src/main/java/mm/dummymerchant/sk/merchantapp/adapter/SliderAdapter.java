package mm.dummymerchant.sk.merchantapp.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import mm.dummymerchant.sk.merchantapp.R;
import mm.dummymerchant.sk.merchantapp.SliderScreen;
import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.Constant;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
import mm.dummymerchant.sk.merchantapp.db.DBHelper;
import mm.dummymerchant.sk.merchantapp.model.CashierActivityLogModel;
import mm.dummymerchant.sk.merchantapp.model.CashierLoginModel;
import mm.dummymerchant.sk.merchantapp.model.CashierModel;

/**
 * Created by Dell on 11/17/2015.
 */
public class SliderAdapter extends BaseAdapter implements Constant
{
    ArrayList<CashierActivityLogModel> al_cashierLogin;
    Context mContext;

    public SliderAdapter(ArrayList<CashierActivityLogModel> al_cashierLogin, Context mContext)
    {
        this.al_cashierLogin = al_cashierLogin;
        this.mContext = mContext;
    }


    @Override
    public int getCount() {
        return al_cashierLogin.size();
    }

    @Override
    public Object getItem(int i) {
        return al_cashierLogin.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup)
    {
        ViewHolder holder=null;
        if (view == null )
        {
            holder=new ViewHolder();
            view= LayoutInflater.from(mContext).inflate(R.layout.custom_menu_list,viewGroup,false);

            holder.rel=(RelativeLayout)view.findViewById(R.id.rel1);
            holder.tv_name=(CustomTextView)view.findViewById(R.id.title);
            holder.img_icon=(ImageView)view.findViewById(R.id.icon);
            holder.tv_phoneNo=(CustomTextView)view.findViewById(R.id.title_bottom_phoneno);

         view.setTag(holder);
        }
        else
        {
         holder=(ViewHolder)view.getTag();
        }
if(AppPreference.getMasterOrCashier(mContext).equalsIgnoreCase(CASHIER))
{
    CashierActivityLogModel cshm = (CashierActivityLogModel) getItem(i);

    String id = cshm.getCashier_Id();
    DBHelper db = new DBHelper(mContext);
    db.open();
    CashierModel model = db.getCashierObject(id);
    db.close();

String cashierno=model.getCashier_Number();

    String phoneNo="";
    if(cashierno.startsWith("0095"))
        phoneNo = "0" + model.getCashier_Number().substring(4);
    else
    phoneNo=model.getCashier_Number().substring(4);

    holder.tv_name.setText(model.getCashier_Name());
    holder.tv_phoneNo.setText(phoneNo);
    Bitmap bmp=model.getCashierPhoto();
    holder.img_icon.setImageBitmap(bmp);

    if (model.getCashier_Id().equalsIgnoreCase(SliderScreen.present_Cashier_Id))
        holder.rel.setBackgroundColor(Color.GRAY);
    else
        holder.rel.setBackgroundColor(Color.TRANSPARENT);
}
        else
{
    CashierActivityLogModel csm=(CashierActivityLogModel) getItem(i);
    holder.tv_name.setText(csm.getCashier_Name());
    holder.tv_phoneNo.setText(csm.getCashier_Number());

}


        return view;
    }

    static class ViewHolder
    {
      ImageView img_icon;
      CustomTextView tv_name;
      RelativeLayout rel;
       CustomTextView tv_phoneNo;
    }
}
