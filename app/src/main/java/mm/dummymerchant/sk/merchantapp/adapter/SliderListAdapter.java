package mm.dummymerchant.sk.merchantapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.ArrayList;

import mm.dummymerchant.sk.merchantapp.R;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;

/**
 * Created by kapil on 27/07/15.
 */
public class SliderListAdapter extends BaseAdapter {

    private int[] colorsCode;
    private Context context;
    private ArrayList<String> list, listOfEmoji;
    private boolean isColor;
    private int[] listOfCategoryImages;

    public SliderListAdapter(Context context, ArrayList<String> list, int[] colorsCode, boolean isColor, int[] listOfCategoryImages, ArrayList<String> listOfEmoji) {
        this.context = context;
        this.list = list;
        this.colorsCode = colorsCode;
        this.isColor = isColor;
        this.listOfCategoryImages = listOfCategoryImages;
        this.listOfEmoji = listOfEmoji;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup)
    {
        ViewHolder holder = null;
        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.drawer_list_item1, null);
            holder = new ViewHolder();
            holder.textView = (CustomTextView) convertView.findViewById(android.R.id.text1);
            holder.view = (View) convertView.findViewById(R.id.color_view);
            holder.imageView = (ImageView) convertView.findViewById(R.id.image);
            holder.tvIcon = (CustomTextView) convertView.findViewById(R.id.tv_icon);
            holder.view.setVisibility(View.GONE);
            holder.imageView.setVisibility(View.GONE);
            holder.tvIcon.setVisibility(View.GONE);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.textView.setText(list.get(position));
        if (isColor) {
            holder.view.setVisibility(View.VISIBLE);
            holder.view.setBackgroundColor(colorsCode[position]);
        }

        if (listOfCategoryImages != null) {
            holder.tvIcon.setVisibility(View.GONE);
            holder.imageView.setVisibility(View.VISIBLE);
            holder.imageView.setImageResource(listOfCategoryImages[position]);
        }

        if (listOfEmoji != null) {
            holder.tvIcon.setVisibility(View.VISIBLE);
            holder.tvIcon.setText(listOfEmoji.get(position));
        }

        return convertView;
    }

    private static class ViewHolder {
        CustomTextView textView, tvIcon;
        View view;
        ImageView imageView;
    }
}
