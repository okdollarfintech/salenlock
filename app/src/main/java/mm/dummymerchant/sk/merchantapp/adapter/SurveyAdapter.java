package mm.dummymerchant.sk.merchantapp.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import mm.dummymerchant.sk.merchantapp.R;
import mm.dummymerchant.sk.merchantapp.Utils.Utils;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
import mm.dummymerchant.sk.merchantapp.model.CustomerSurveyModel;
import mm.dummymerchant.sk.merchantapp.model.NetworkOperatorModel;

/**
 * Created by user on 11/9/2015.
 */
public class SurveyAdapter extends BaseAdapter {
    Context context;
    LayoutInflater inflater;
    ArrayList<CustomerSurveyModel> surveyList = new ArrayList<>();
    ArrayList<NetworkOperatorModel> listOfCountryCodesAndNames;
    int redColor = 0;
    int blackColor =0;
    public SurveyAdapter(Context context, ArrayList<CustomerSurveyModel> questionList) {
        this.context = context;
        this.surveyList = questionList;
        this.inflater = LayoutInflater.from(context);
        this.listOfCountryCodesAndNames = new NetworkOperatorModel().getListOfNetworkOperatorModel(context);
        redColor = context.getResources().getColor(R.color.red);
        blackColor = context.getResources().getColor(R.color.black);
    }



    @Override
    public int getCount() {
        return surveyList.size();
    }

    @Override
    public Object getItem(int position) {
        return surveyList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        String num = "";
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.survey_items_layout, parent, false);
            holder.txtQue1 = (CustomTextView) convertView.findViewById(R.id.textViewQ1);
            holder.txtQue2 = (CustomTextView) convertView.findViewById(R.id.textViewQ2);
            holder.txtQue3 = (CustomTextView) convertView.findViewById(R.id.textViewQ3);
            holder.txtQue4 = (CustomTextView) convertView.findViewById(R.id.textViewQ4);
            holder.txtQue5 = (CustomTextView) convertView.findViewById(R.id.textViewQ5);
            holder.txtQue6 = (CustomTextView) convertView.findViewById(R.id.textViewQ6);
            holder.txtQue7 = (CustomTextView) convertView.findViewById(R.id.textViewQ7);
            holder.txtQue8 = (CustomTextView) convertView.findViewById(R.id.textViewQ8);
            holder.txtUserN = (CustomTextView) convertView.findViewById(R.id.textViewUser);
            holder.txtUserN.setTypeface(Typeface.DEFAULT_BOLD);
            holder.txtTotal = (CustomTextView) convertView.findViewById(R.id.textViewTotal);
            holder.txtTotal.setTypeface(Typeface.DEFAULT_BOLD);
            holder.imageFlag = (ImageView) convertView.findViewById(R.id.imageViewFlag);
            holder.txtCCode = (TextView) convertView.findViewById(R.id.ccCode);
            holder.flagLayout = (RelativeLayout) convertView.findViewById(R.id.flagLayout);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        //surveyModelArrayList = surveyList.get(position).getSurveyModelArrayList();
        num = surveyList.get(position).getQ1();
        if (num.equals("0")|| num.equals("X")) {
            holder.txtQue1.setTextColor(redColor);
        } else {
            holder.txtQue1.setTextColor(blackColor);
        }
        holder.txtQue1.setText(num);

        num = surveyList.get(position).getQ2();
        if (num.equals("0")|| num.equals("X")) {
            holder.txtQue2.setTextColor(redColor);
        } else {
            holder.txtQue2.setTextColor(blackColor);
        }
        holder.txtQue2.setText(num);

        num = surveyList.get(position).getQ3();
        if (num.equals("0")|| num.equals("X")) {
            holder.txtQue3.setTextColor(redColor);
        } else {
            holder.txtQue3.setTextColor(blackColor);
        }
        holder.txtQue3.setText(num);

        num = surveyList.get(position).getQ4();
        if (num.equals("0")|| num.equals("X")) {
            holder.txtQue4.setTextColor(redColor);
        } else {
            holder.txtQue4.setTextColor(blackColor);
        }
        holder.txtQue4.setText(num);

        num = surveyList.get(position).getQ5();
        if (num.equals("0")|| num.equals("X")) {
            holder.txtQue5.setTextColor(redColor);
        } else {
            holder.txtQue5.setTextColor(blackColor);
        }
        holder.txtQue5.setText(num);

        num = surveyList.get(position).getQ6();
        if (num.equals("0")|| num.equals("X")) {
            holder.txtQue6.setTextColor(redColor);
        } else {
            holder.txtQue6.setTextColor(blackColor);
        }
        holder.txtQue6.setText(num);

        // holder.txtQue7.setText(surveyModelArrayList.get(2).getMarks());
        num = surveyList.get(position).getQ7();
        if (num.equals("0")|| num.equals("X")) {
            holder.txtQue7.setTextColor(redColor);
        } else {
            holder.txtQue7.setTextColor(blackColor);
        }
        holder.txtQue7.setText(num);

        //holder.txtQue8.setText(surveyModelArrayList.get(1).getMarks());
        num = surveyList.get(position).getQ8();
        if (num.equals("0")|| num.equals("X")) {
            holder.txtQue8.setTextColor(redColor);
        } else {
            holder.txtQue8.setTextColor(blackColor);
        }
        holder.txtQue8.setText(num);

        String phone = surveyList.get(position).getPhone();
        if (!phone.contains("X") && phone.startsWith("00")) {
            String code = phone.substring(0, 6);
            for (int i = 0; i < listOfCountryCodesAndNames.size(); i++) {
                String countryId = listOfCountryCodesAndNames.get(i).getCountryCode().replace("+", "00");
                if (code.contains(countryId)) {
                    int length = countryId.length();
                    String cCode = listOfCountryCodesAndNames.get(i).getCountryCode();
                    if(cCode.equals("+95"))
                        holder.txtUserN.setText("0"+phone.substring(length));
                    holder.txtCCode.setText("("+cCode+")");
                    holder.imageFlag.setImageResource(listOfCountryCodesAndNames.get(i).getCountryImageId());
                    break;
                }

            }
            holder.flagLayout.setVisibility(View.VISIBLE);
        } else {
            holder.txtUserN.setText(phone);
            holder.flagLayout.setVisibility(View.INVISIBLE);
        }
        holder.txtTotal.setText(Utils.formatedAmount_t(surveyList.get(position).getTotal()+"",context));
        return convertView;
    }

    private class ViewHolder {
        CustomTextView txtQue1, txtQue2, txtQue3, txtQue4, txtQue5, txtQue6, txtQue7, txtQue8, txtUserN, txtTotal;
        ImageView imageFlag;
        TextView txtCCode;
        RelativeLayout flagLayout;
    }
}