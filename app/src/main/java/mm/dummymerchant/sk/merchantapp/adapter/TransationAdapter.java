package mm.dummymerchant.sk.merchantapp.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;

import com.scottyab.aescrypt.AESCrypt;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import mm.dummymerchant.sk.merchantapp.MapActivity;
import mm.dummymerchant.sk.merchantapp.R;
import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.Constant;
import mm.dummymerchant.sk.merchantapp.Utils.GetNumberWithCountryCode;
import mm.dummymerchant.sk.merchantapp.Utils.Utils;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
import mm.dummymerchant.sk.merchantapp.customView.RoundedImageView;
import mm.dummymerchant.sk.merchantapp.db.DBHelper;
import mm.dummymerchant.sk.merchantapp.model.CashierModel;
import mm.dummymerchant.sk.merchantapp.model.ConstactModel;
import mm.dummymerchant.sk.merchantapp.model.NetworkOperatorModel;
import mm.dummymerchant.sk.merchantapp.model.TransationModel;


public class TransationAdapter extends BaseAdapter implements Constant{

    private final ArrayList<NetworkOperatorModel> listOfCountryCodesAndNames;
    List<TransationModel> mTranstionList;
    Context context;
    DBHelper db;
    GetNumberWithCountryCode mCountryCode;

    public TransationAdapter(List<TransationModel> transtionList, Context context) {
        mTranstionList = transtionList;
        this.context = context;
        db = new DBHelper(context);
        mCountryCode = GetNumberWithCountryCode.getInstance();
        listOfCountryCodesAndNames = new NetworkOperatorModel().getListOfNetworkOperatorModel(context);
    }

    @Override
    public int getCount() {
        return mTranstionList.size();
    }

    @Override
    public Object getItem(int arg0) {
        return arg0;
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    @Override
    public View getView(final int arg0, View conertView, ViewGroup arg2) {

        final Holder holder;

        if (conertView == null) {
            holder = new Holder();
            conertView = LayoutInflater.from(context).inflate(R.layout.row_layout_section, null);
            holder.mLinearrRow = (LinearLayout) conertView.findViewById(R.id.hide_open);
           /* holder.qrcode = (ImageView) conertView.findViewById(R.id.qrcode);*/
            holder.mMap = (ImageView) conertView.findViewById(R.id.transcation_history_google_map);
            holder.mCommentLayout = (RelativeLayout) conertView.findViewById(R.id.transcation_history_comment_hide_and_show_layout);
            holder.tvDestination = (CustomTextView) conertView.findViewById(R.id.tv_trans_id);
            holder.tvComment = (CustomTextView) conertView.findViewById(R.id.last_trasncation_textview_row_comment);
            holder.tvSource = (CustomTextView) conertView.findViewById(R.id.tv_mob_no);
            holder.tvTransationType = (CustomTextView) conertView.findViewById(R.id.tv_trans_type);
            holder.tvTvDate = (CustomTextView) conertView.findViewById(R.id.tv_date);
            holder.tvCashback = (CustomTextView) conertView.findViewById(R.id.last_trasncation_textview_row_cashback);
            holder.tvAmount = (CustomTextView) conertView.findViewById(R.id.tv_amount);
            holder.tvAmountOftranscation = (CustomTextView) conertView.findViewById(R.id.tv_comment);
            holder.li = (LinearLayout) conertView.findViewById(R.id.linear);


            holder.tvName = (CustomTextView) conertView.findViewById(R.id.tv_trans_name);
            holder.flag = (ImageView) conertView.findViewById(R.id.flag);
            holder.photo = (RoundedImageView) conertView.findViewById(R.id.received_money_imagge_View_user_image);

            holder.tv_cashiername=(CustomTextView)conertView.findViewById(R.id.cashier_name);
            holder.tv_cashierNo=(CustomTextView)conertView.findViewById(R.id.cashier_contact);
            holder.tv_cashier_details=(CustomTextView)conertView.findViewById(R.id.cashier_details);

            holder.tvAmount.setTypeface(Utils.getFont(context));
            holder.tvDestination.setTypeface(Utils.getFont(context));
            holder.tvTransationType.setTypeface(Utils.getFont(context));
            holder.tvTvDate.setTypeface(Utils.getFont(context));
            holder.tvSource.setTypeface(Utils.getFont(context));
            holder.tvCashback.setTypeface(Utils.getFont(context), Typeface.BOLD);
            holder.tvAmountOftranscation.setTypeface(Utils.getFont(context));
            conertView.setTag(holder);
        } else {
            holder = (Holder) conertView.getTag();
        }

        if (mTranstionList.get(arg0).getComments().contains("~comments~") || mTranstionList.get(arg0).getComments().isEmpty()) {
            holder.tvComment.setText("");
        } else {
            holder.tvComment.setText(context.getResources().getString(R.string.comment) + ":" + mTranstionList.get(arg0).getComments().replace("~comments~", ""));
        }
        holder.tvDestination.setText(mTranstionList.get(arg0).getTransatId());
        DecimalFormat f = new DecimalFormat("##.00");
        if (!mTranstionList.get(arg0).getReceivedKickback().isEmpty()) {
            holder.tvCashback.setText(context.getResources().getString(R.string.kickback) + ": " + Utils.formatedAmountAbhi(mTranstionList.get(arg0).getReceivedKickback(), context) + " " + context.getResources().getString(R.string.kyats));
            //holder.mCommentLayout.setVisibility(View.VISIBLE);
            if(mTranstionList.get(arg0).getIsCredit().equals("Dr")) {
                holder.tvCashback.setVisibility(View.VISIBLE);
            }else
            {
                holder.tvCashback.setVisibility(View.INVISIBLE);
            }
        } else {
            holder.tvCashback.setVisibility(View.INVISIBLE);
        }


        if (!Utils.isEmpty(mTranstionList.get(arg0).getAmount())) {
            double amount = Double.parseDouble(mTranstionList.get(arg0).getAmount());
            String amoun = f.format(amount);
            if  (mTranstionList.get(arg0).getIsCredit().equals("LOYALTY")||mTranstionList.get(arg0).getIsCredit().equals("DISCOUNT")) {
                holder.tvComment.setVisibility(View.INVISIBLE)  ;
                holder.mMap.setVisibility(View.INVISIBLE);
                //    holder.qrcode.setVisibility(View.INVISIBLE);
                holder.tvAmountOftranscation.setVisibility(View.INVISIBLE);
                holder.tvAmount.setText(Utils.formatedAmountAbhi(amoun, context) + " " + context.getResources().getString(R.string.mmk_point));
            } else {
                holder.tvAmount.setText(Utils.formatedAmountAbhi(amoun, context) + " " + context.getResources().getString(R.string.kyats));
                holder.tvComment.setVisibility(View.VISIBLE)  ;
                holder.mMap.setVisibility(View.VISIBLE);
                //  holder.qrcode.setVisibility(View.VISIBLE);
                holder.tvAmountOftranscation.setVisibility(View.VISIBLE);
            }
        }
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", Locale.ENGLISH);
        holder.tvTvDate.setText(dateFormatter.format(mTranstionList.get(arg0).getmTranscationDate()));
//        if (!Utils.isEmpty(mTranstionList.get(arg0).getDate())) {
//            holder.tvTvDate.setText(mTranstionList.get(arg0).getDate());
//        } else {
//            holder.tvTvDate.setText(mTranstionList.get(arg0).getDate());
//        }

        if (!Utils.isEmpty(mTranstionList.get(arg0).getDestinationPersonName().trim())) {
            holder.tvName.setText(mTranstionList.get(arg0).getDestinationPersonName());
        } else {
            holder.tvName.setText("Unknown");
        }

        /*if (mTranstionList.get(arg0).getOpen().equals("close")) {
            holder.mCommentLayout.setVisibility(View.GONE);
        } else {
            holder.mCommentLayout.setVisibility(View.VISIBLE);
        }*/

        try {
            String number = "";
            if (mTranstionList.get(arg0).getIsCredit().equals("Cr"))
                number = mTranstionList.get(arg0).getDestination().substring(4);
            else {
                if (!mTranstionList.get(arg0).getOperator().isEmpty() && !mTranstionList.get(arg0).getTrnastionType().equals("PAYTOKICKBACK"))
                    number = mTranstionList.get(arg0).getOperator().substring(4);
                else
                    number = mTranstionList.get(arg0).getDestination().substring(4);

            }
          //  ConstactModel contactModel = db.getSelectedContact(number);
            ///////
            List<ConstactModel> list = new ArrayList<ConstactModel>();
            Utils utils=new Utils();
            String jsondata=DBHelper.getJsonContacts();
            list=utils.getContactListFromJsonString(jsondata);
            ////////////
            ConstactModel contactModel = utils.getSelectedContactModel(number,list);
            if (!contactModel.getPhotoUrl().equals("")) {
                Picasso.with(context).load(Uri.parse(contactModel.getPhotoUrl())).placeholder(context.getResources().getDrawable(R.drawable.default_user_icon)).into(holder.photo);
            } else {
                Drawable d = context.getResources().getDrawable(R.drawable.default_user_icon);
                if (d != null) {
                    d.setBounds(0, 0, 60, 60);
                }
                holder.photo.setImageDrawable(d);
            }
            if (!contactModel.getName().equals("") && !contactModel.getMobileNo().equals("")) {
                holder.tvName.setText(contactModel.getName());
            } else {
                holder.tvName.setText("Unknown");
            }

        } catch (Exception e) {
            e.printStackTrace();
            Drawable d = context.getResources().getDrawable(R.drawable.default_user_icon);
            if (d != null) {
                d.setBounds(0, 0, 60, 60);
            }
            holder.photo.setImageDrawable(d);
        }


       /* holder.qrcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String kb = "na";
                String loyalty = "na";
                if (!mTranstionList.get(arg0).getReceivedKickback().isEmpty()) {
                    kb = mTranstionList.get(arg0).getReceivedKickback();
                } else {
                    kb = "na";
                }

                if (mTranstionList.get(arg0).getLoyality().equals("")) {
                    loyalty = "na";
                } else {

                    loyalty = mTranstionList.get(arg0).getLoyality();
                }


                String encryptedMsgTime = parseDateToddMMyyyy(mTranstionList.get(arg0).getDate());
                String encryptedMsg = "";
                String num = "";


                if (!mTranstionList.get(arg0).getOperator().equals("")) {
                    if (mTranstionList.get(arg0).getDestination().equals(mTranstionList.get(arg0).getOperator())) {
                        num = mTranstionList.get(arg0).getDestination();

                    } else {
                        num = mTranstionList.get(arg0).getOperator();
                    }
                } else {
                    num = mTranstionList.get(arg0).getDestination();

                }

                String locations = "";
                String CellID = "";
                String gender = "";
                String src_name = "";
                String src_no = "";

                if (!mTranstionList.get(arg0).getDestination().trim().equals("") && (!mTranstionList.get(arg0).getDestination().trim().contains("XXXX") || !mTranstionList.get(arg0).getDestination().trim().contains("xxxx"))) {

                    if (!mTranstionList.get(arg0).getTrnastionType().equals("") && mTranstionList.get(arg0).getTrnastionType().equals("PAYTO")) {

                        locations = mTranstionList.get(arg0).getLatLong();
                        CellID = mTranstionList.get(arg0).getCellID();
                        gender = mTranstionList.get(arg0).getGender();
                        src_name = AppPreference.getLoginuserName(context);
                        src_no = AppPreference.getMyMobileNo(context);

                    } else if (!mTranstionList.get(arg0).getTrnastionType().equals("") && mTranstionList.get(arg0).getTrnastionType().equals("PAYTOID")) {
                        src_no = "XXXXXXXXXX";
                        locations = mTranstionList.get(arg0).getLatLong();
                        CellID = mTranstionList.get(arg0).getCellID();
                        gender = mTranstionList.get(arg0).getGender();
                        src_name = "XXXXX";

                    } else {
                        src_no = "XXXXXXXXXX";
                        locations = mTranstionList.get(arg0).getLatLong();
                        CellID = mTranstionList.get(arg0).getCellID();
                        gender = mTranstionList.get(arg0).getGender();
                        src_name = "XXXXX";
                    }


                    encryptedMsg = "OK" + "-" + src_name + "-" + mTranstionList.get(arg0).getAmount() + "-" + encryptedMsgTime + "-" + mTranstionList.get(arg0).getTransatId() + "-" + src_no + "-" + mTranstionList.get(arg0).getmWalletbalance() + "-des-" + num + "-" + mTranstionList.get(arg0).getTrnastionType() + "-" + kb + "-" + loyalty + "-" + locations + "-" + CellID + "-" + gender + "-" + mTranstionList.get(arg0).getAge() + "-" + mTranstionList.get(arg0).getCity();
                }
                encryptedMsgTime = encryptedMsgTime + "----" + mTranstionList.get(arg0).getDestination();
                //System.out.println("encryptedMsgTime--" + encryptedMsgTime);
                //System.out.println("encryptedMsg--" + encryptedMsg);
                String qrInputTextTime = "";
                String qrInputText = "";
                String encryp_data = "";

              *//*  try {
                    qrInputTextTime = AESCrypt.encrypt(Constant.SPEEDKey, encryptedMsgTime);
                    qrInputText = AESCrypt.encrypt(encryptedMsgTime, encryptedMsg);
                    encryp_data = qrInputTextTime + "---" + qrInputText;
                    qrcode(encryp_data);

                } catch (Exception e) {
                    e.printStackTrace();
                }*//*
            }
        });
*/
        if (!mTranstionList.get(arg0).getmWalletbalance().isEmpty())
            holder.tvAmountOftranscation.setText(Utils.formatedAmountAbhi(mTranstionList.get(arg0).getmWalletbalance(), context));

        if (mTranstionList.get(arg0).getIsCredit().equals("Dr")) {
            String number = "";
            if (!mTranstionList.get(arg0).getOperator().isEmpty() && !mTranstionList.get(arg0).getTrnastionType().equals("PAYTOKICKBACK"))
                number = mTranstionList.get(arg0).getOperator();
            else
                number = mTranstionList.get(arg0).getDestination();
            Converter(number, holder.flag, holder.tvSource, "Dr");
            holder.li.setBackgroundColor(context.getResources().getColor(R.color.DR));
            holder.tvTransationType.setVisibility(View.GONE);
            holder.tvDestination.setTextColor(context.getResources().getColor(R.color.white));
            holder.tvTvDate.setTextColor(context.getResources().getColor(R.color.white));
            holder.tvSource.setTextColor(context.getResources().getColor(R.color.white));
            holder.tvName.setTextColor(context.getResources().getColor(R.color.white));
            holder.tvComment.setTextColor(context.getResources().getColor(R.color.white));
            holder.tvAmountOftranscation.setTextColor(context.getResources().getColor(R.color.white));
            //  holder.tvCashback.setVisibility(View.VISIBLE);
            // holder.qrcode.setVisibility(View.VISIBLE);
        } else if (mTranstionList.get(arg0).getIsCredit().equals("Cr")) {
            holder.li.setBackgroundColor(context.getResources().getColor(R.color.cr));
            holder.tvTransationType.setVisibility(View.GONE);
            //   holder.qrcode.setVisibility(View.GONE);
            holder.tvDestination.setTextColor(context.getResources().getColor(R.color.white));
            holder.tvTvDate.setTextColor(context.getResources().getColor(R.color.white));
            holder.tvSource.setTextColor(context.getResources().getColor(R.color.white));
            holder.tvName.setTextColor(context.getResources().getColor(R.color.white));
            holder.tvComment.setTextColor(context.getResources().getColor(R.color.white));
            holder.tvAmountOftranscation.setTextColor(context.getResources().getColor(R.color.white));
            // holder.tvCashback.setVisibility(View.GONE);
            Converter(mTranstionList.get(arg0).getDestination(), holder.flag, holder.tvSource, "Cr");

            if (!mTranstionList.get(arg0).getDestination().contains("X")) {
                if (!mTranstionList.get(arg0).getTrnastionType().equals("PAYTO")) {
                    holder.mMap.setVisibility(View.GONE);
                    int length = mTranstionList.get(arg0).getDestination().length();
                    String number = mTranstionList.get(arg0).getDestination();
                    int limit = length - 5;
                    StringBuilder stringBuilder = new StringBuilder("");
                    for (int i = length - 1; i > 0; i--) {
                        if (i > limit) {
                            stringBuilder.append(number.charAt(i));
                        } else {
                            stringBuilder.append("x");
                        }
                    }
                    holder.tvSource.setText(stringBuilder.reverse().toString());
                } else {
                    holder.mMap.setVisibility(View.VISIBLE);
                    Converter(mTranstionList.get(arg0).getDestination(), holder.flag, holder.tvSource, "Cr");
                }
            } else {
                Converter("XXXXXXXXX", holder.flag, holder.tvSource, "Cr");
            }
        } else if (mTranstionList.get(arg0).getIsCredit().equals("LOYALTY")) {
            holder.li.setBackgroundColor(context.getResources().getColor(R.color.Loyalty));
            Converter(mTranstionList.get(arg0).getDestination(), holder.flag, holder.tvSource, "LOYALTY");
            holder.tvDestination.setTextColor(context.getResources().getColor(R.color.white));
            holder.tvTvDate.setTextColor(context.getResources().getColor(R.color.white));
            holder.tvSource.setTextColor(context.getResources().getColor(R.color.white));
            holder.tvName.setTextColor(context.getResources().getColor(R.color.white));
            holder.tvComment.setTextColor(context.getResources().getColor(R.color.white));
            holder.tvAmountOftranscation.setTextColor(context.getResources().getColor(R.color.white));
        } else if (mTranstionList.get(arg0).getIsCredit().equals("DISCOUNT")) {
            holder.li.setBackgroundColor(context.getResources().getColor(R.color.discount));
            Converter(mTranstionList.get(arg0).getDestination(), holder.flag, holder.tvSource, "LOYALTY");
            holder.tvDestination.setTextColor(context.getResources().getColor(R.color.white));
            holder.tvTvDate.setTextColor(context.getResources().getColor(R.color.white));
            holder.tvSource.setTextColor(context.getResources().getColor(R.color.white));
            holder.tvName.setTextColor(context.getResources().getColor(R.color.white));
            holder.tvComment.setTextColor(context.getResources().getColor(R.color.white));
            holder.tvAmountOftranscation.setTextColor(context.getResources().getColor(R.color.white));
        }

        holder.mLinearrRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               /* if (mTranstionList.get(arg0).getOpen().equals("close")) {
                    mTranstionList.get(arg0).setOpen("open");
                    holder.mCommentLayout.setVisibility(View.VISIBLE);
                } else if (mTranstionList.get(arg0).getReceivedKickback().isEmpty())  {
                    mTranstionList.get(arg0).setOpen("close");
                    holder.mCommentLayout.setVisibility(View.GONE);
                }*/
            }
        });

        //for setting background for specific cashierId
        if(AppPreference.getMasterOrCashier(context).equalsIgnoreCase(MASTER))
        {
            TransationModel model = mTranstionList.get(arg0);
            String cashierId = model.getCashierId();

            if (!cashierId.equalsIgnoreCase(MASTER_TAG))
            {
                DBHelper dbhelper = new DBHelper(context);
                CashierModel cashmodel = dbhelper.getCashierModel(cashierId);

                if(cashmodel!=null)
                {
                    String color = cashmodel.getColorCode();
                    int colorCode = Integer.parseInt(color);
                    holder.li.setBackgroundColor(colorCode);

                    String cashierno=cashmodel.getCashier_Number();
                    cashierno=cashierno.substring(4);
                    cashierno="0"+cashierno;

                    holder.tv_cashierNo.setText(cashierno);
                    holder.tv_cashiername.setText(cashmodel.getCashier_Name());
                }


            }
            else
            {
                holder.tv_cashierNo.setText("");
                holder.tv_cashiername.setText("");
                holder.tv_cashier_details.setText("");
            }

        }

        else
        {
            holder.tv_cashiername.setVisibility(View.GONE);
            holder.tv_cashierNo.setVisibility(View.GONE);
            holder.tv_cashier_details.setVisibility(View.GONE);
        }

        //................................................

        holder.mMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent call = new Intent(context, MapActivity.class);
                call.putExtra(Constant.BUNDLE_MAP_NUMBER, mTranstionList.get(arg0).getDestination());
                call.putExtra(Constant.BUNDLE_MAP_AMOUNT, mTranstionList.get(arg0).getAmount());
                call.putExtra(Constant.BUNDLE_MAP_LAT_LONG, mTranstionList.get(arg0).getLatLong());
                call.putExtra(Constant.BUNDLE_MAP_CELL_ID, mTranstionList.get(arg0).getCellID());
                call.putExtra(Constant.MERCHANT_MOBILE_NO, mTranstionList.get(arg0).getDestination());
                context.startActivity(call);
            }
        });

        return conertView;
    }

    private String numberReplace(String number) {
        if (number.startsWith("+"))
            number = number.replace("+", "00");
        if (number.startsWith("00")) {
            String code = number.substring(0, 6);
            //System.out.println("code--" + code);
            for (int i = 0; i < listOfCountryCodesAndNames.size(); i++) {
                String countryId = "00" + listOfCountryCodesAndNames.get(i).getCountryCode().substring(1);
                if (code.contains(countryId)) {
                    //System.out.println("countryId--" + countryId);
                    //System.out.println(i);
                    int length = countryId.length();
                    number = number.substring(length);
                    if (listOfCountryCodesAndNames.get(i).getCountryCode().contains("+95"))
                        number = "0" + number;
                    return number;
                }
            }
        }
        return number;
    }

    public String parseDateToddMMyyyy(String time) {

        String date = time.replace("-", "/");
//        String inputPattern = "dd-MMM-yyyy HH:mm:ss";
//        String outputPattern = "dd/MMM/yyyy HH:mm:ss";
//        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
//        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
//
//        Date date = null;
//        String str = null;
//
//        try {
//            date = inputFormat.parse(time);
//            str = outputFormat.format(date);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
        return date;
    }
/*
    private Bitmap generateQRCode(String qrInputText) {
        QRCodeEncoder qrCodeEncoder = new QRCodeEncoder(qrInputText,
                null,
                Contents.Type.TEXT,
                BarcodeFormat.QR_CODE.toString(),
                150);
        try {
            return qrCodeEncoder.encodeAsBitmap();
        } catch (WriterException e) {
            e.printStackTrace();
            return null;
        }
    }*/

 /*   private void qrcode(String message) {
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.qrcode_shower, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(promptsView);
        final ImageView userInput = (ImageView) promptsView.findViewById(R.id.alert);
        userInput.setImageBitmap(generateQRCode(message));
        CustomButton ok = (CustomButton) promptsView.findViewById(R.id.btn_settings);
        final AlertDialog dialog1 = alertDialogBuilder.create();
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog1.dismiss();
            }
        });
        dialog1.show();
    }*/


    public void Converter(String no, ImageView v, CustomTextView ctv, String type) {
        // ArrayList<NetworkOperatorModel> listOfCountryCodesAndNames = new NetworkOperatorModel().getListOfNetworkOperatorModel(context);
        try {

            if (!(no.contains("X") || no.contains("x"))) {

                String data[] = mCountryCode.getCountryCodeNameAndFlagFromNumberLogin(no, context);
                String countryCode = data[0];
                String countryName = data[1];
                int flag = Integer.parseInt(data[2]);
                String number = data[3];
                ctv.setText("  (" + countryCode + ")" + number);
                Drawable img = context.getResources().getDrawable(flag);
                if (img != null) {
                    img.setBounds(0, 0, 60, 60);
                }
                ctv.setCompoundDrawables(img, null, null, null);
            } else {

                ctv.setText("XXXXXXXXXX");
                Drawable imgg = context.getResources().getDrawable(R.drawable.transparent_img);
                if (imgg != null) {
                    imgg.setBounds(0, 0, 60, 60);
                }
                ctv.setCompoundDrawables(imgg, null, null, null);
            }

            if (type.equals("Dr")) {
                Drawable img = context.getResources().getDrawable(R.drawable.get);
                if (img != null) {
                    img.setBounds(0, 0, 40, 40);
                }
                v.setImageDrawable(img);
            } else if (type.equals("Cr")) {
                Drawable img = context.getResources().getDrawable(R.drawable.blu);
                if (img != null) {
                    img.setBounds(0, 0, 40, 40);
                }
                v.setImageDrawable(img);
            }


//            String countryCode = "";
//            if (!(no.contains("X") || no.contains("x"))) {
//                int flag;
//                if (no.startsWith("+"))
//                    no = no.replace("+", "00");
//                no = convertValidNumber(no);
//                int id = -1;
//                if (no.startsWith("+") || no.startsWith("00")) {
//                    if (no.length() > 6) {
//                        String code = no.substring(0, 6);
//                        for (int i = 0; i < listOfCountryCodesAndNames.size(); i++) {
//                            ArrayList<String> dialingCodes = listOfCountryCodesAndNames.get(i).getListOfDialingCodes();
//                            if (dialingCodes != null) {
//                                for (int j = 0; j < dialingCodes.size(); j++) {
//                                    String countryId = dialingCodes.get(j).replace("+", "00");
//
//                                    if (code.contains(countryId)) {
//                                        System.out.println(i);
//                                        id = i;
//
//                                        no = no.substring(3);
//                                        countryCode = listOfCountryCodesAndNames.get(id).getCountryCode();
//                                        flag = listOfCountryCodesAndNames.get(i).getCountryImageId();
//                                        ctv.setText("  (" + countryCode + ")" + no);
//                                        Drawable img = context.getResources().getDrawable(flag);
//                                        if (img != null) {
//                                            img.setBounds(0, 0, 60, 60);
//                                        }
//                                        ctv.setCompoundDrawables(img, null, null, null);
//
//                                        break;
//                                    }
//                                }
//
//                            } else {
//
//                                String countryId = "00" + listOfCountryCodesAndNames.get(i).getCountryCode().substring(1);
//
//                                if (code.contains(countryId)) {
//                                    System.out.println(i);
//                                    id = i;
//                                    int length = countryId.length();
//                                    no = no.substring(length);
//                                    countryCode = listOfCountryCodesAndNames.get(id).getCountryCode();
//                                    flag = listOfCountryCodesAndNames.get(i).getCountryImageId();
//                                    ctv.setText("  (" + countryCode + ")" + no);
//                                    Drawable img = context.getResources().getDrawable(flag);
//                                    if (img != null) {
//                                        img.setBounds(0, 0, 60, 60);
//                                    }
//                                    ctv.setCompoundDrawables(img, null, null, null);
//
//                                    break;
//                                }
//                            }
//
//
//                        }
//                    }
//
//                    if (countryCode.equals("+95")) {
//                        no = "0" + no;
//                    }
//
//                    if (id == -1) {
//                        if (no.startsWith("0")) {
//                            if (!AppPreference.getCountryCode(context).contains("+95"))
//                                no = no.substring(1);
//                        } else {
//                            no = "0" + no;
//                        }
//
//                        flag = AppPreference.getCountryImageID(context);
//                        Drawable img = context.getResources().getDrawable(flag);
//                        if (img != null) {
//                            img.setBounds(0, 0, 60, 60);
//                        }
//                        ctv.setCompoundDrawables(img, null, null, null);
//                        ctv.setText("(" + AppPreference.getCountryCode(context) + ")" + no);
//                    }
//
//
//                    if (type.equals("Dr")) {
//                        Drawable img = context.getResources().getDrawable(R.drawable.get);
//                        if (img != null) {
//                            img.setBounds(0, 0, 40, 40);
//                        }
//                        v.setImageDrawable(img);
//                    } else if (type.equals("Cr")) {
//                        Drawable img = context.getResources().getDrawable(R.drawable.blu);
//                        if (img != null) {
//                            img.setBounds(0, 0, 40, 40);
//                        }
//                        v.setImageDrawable(img);
//                    }
//
//                }
//            } else {
//
//                ctv.setText("XXXXXXXXXX");
//                Drawable imgg = context.getResources().getDrawable(R.drawable.transparent_img);
//                if (imgg != null) {
//                    imgg.setBounds(0, 0, 60, 60);
//                }
//                ctv.setCompoundDrawables(imgg, null, null, null);
//
//
//                if (type.equals("Dr")) {
//                    Drawable img = context.getResources().getDrawable(R.drawable.get);
//                    if (img != null) {
//                        img.setBounds(0, 0, 40, 40);
//                    }
//                    v.setImageDrawable(img);
//                } else if (type.equals("Cr")) {
//                    Drawable img = context.getResources().getDrawable(R.drawable.blu);
//                    if (img != null) {
//                        img.setBounds(0, 0, 40, 40);
//                    }
//                    v.setImageDrawable(img);
//                }
//
//
//            }
        } catch (Exception e) {
            e.printStackTrace();
            ctv.setText("XXXXXXXXXX");
        }
    }


    class Holder {

        CustomTextView tvSource, tvDestination, tvTransationType, tvTvDate, tvAmount, tvAmountOftranscation, tvName, tvComment, tvCashback;

        LinearLayout li;
        ImageView flag, mMap;
        RoundedImageView photo;
        LinearLayout mLinearrRow;
        RelativeLayout mCommentLayout;

        CustomTextView tv_cashiername,tv_cashierNo,tv_cashier_details;

    }

}