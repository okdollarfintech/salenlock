package mm.dummymerchant.sk.merchantapp.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.ArrayList;

import mm.dummymerchant.sk.merchantapp.R;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
import mm.dummymerchant.sk.merchantapp.model.CashierModel;

/**
 * Created by Dell on 11/3/2015.
 */
public class ViewCashierAdapter extends BaseAdapter
{
    Context context;
    ArrayList<CashierModel> al_cashierModel;

    public ViewCashierAdapter(Context context, ArrayList<CashierModel> al_cashierModel)
    {
        this.context = context;
        this.al_cashierModel = al_cashierModel;
    }


    @Override
    public int getCount()
    {
        return al_cashierModel.size();
    }

    @Override
    public Object getItem(int i)
    {
        return al_cashierModel.get(i);
    }

    @Override
    public long getItemId(int i)
    {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup)
    {
        ViewHolder holder=null;
        if(view ==null)
        {
            holder=new ViewHolder();
            view=  LayoutInflater.from(context).inflate(R.layout.view_viewcashier,viewGroup,false);
            holder.tv_cashiername=(CustomTextView)view.findViewById(R.id.view_viewcashier_cashiername);
            holder.tv_cashiernumber=(CustomTextView)view.findViewById(R.id.view_viewcashier_cashiernumber);
            holder.img_color=(ImageView)view.findViewById(R.id.img_viewcashier_cashiercolor);
            holder.tv_colorcode=(CustomTextView)view.findViewById(R.id.view_viewcashier_cashiercolor);
            holder.img_photo=(ImageView)view.findViewById(R.id.imageView1);

            view.setTag(holder);

        }else
        {
            holder=(ViewHolder)view.getTag();
        }
        CashierModel model=(CashierModel)getItem(i);

      //  long color_code=Long.parseLong(model.getColorCode());

        String cashierNo=model.getCashier_Number();
        if(cashierNo.startsWith("0095"))
        {
            cashierNo = cashierNo.substring(4);
            cashierNo = "0" + cashierNo;
        }
        else
        {
            cashierNo = cashierNo.substring(4);
        }

        Bitmap bmp=model.getCashierPhoto();

        holder.tv_cashiername.setText(model.getCashier_Name());
        holder.tv_cashiernumber.setText(cashierNo);
      //  holder.img_color.setBackgroundColor(color_code);

        if(bmp!=null)
        holder.img_photo.setImageBitmap(bmp);



        return view;
    }

    class ViewHolder
    {
        CustomTextView tv_cashiername;
        CustomTextView tv_cashiernumber;
        CustomTextView tv_colorcode;
        ImageView img_color;
        ImageView img_photo;

    }



}
