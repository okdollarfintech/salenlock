package mm.dummymerchant.sk.merchantapp.customView;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

import mm.dummymerchant.sk.merchantapp.Utils.Utils;


public class CustomButton extends Button
{

	public CustomButton(Context context)
    {
	    super(context);
    }


	public CustomButton(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
		setTypeface(Utils.getFont(context), Typeface.BOLD);
	}

	public CustomButton(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		setTypeface(Utils.getFont(context), Typeface.BOLD);

	}

}
