package mm.dummymerchant.sk.merchantapp.customView;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import mm.dummymerchant.sk.merchantapp.R;

public class CustomDialog extends Dialog implements android.view.View.OnClickListener
{
	public Context activity;
	public Dialog d;
	public Button yes;
	TextView tvTitle, tvMessage;
	String msg;
	DialogListener mListener;

	public interface DialogListener
	{
		void yesClcked();
	}

	public CustomDialog(Context a, String msg)
	{
		super(a);
		this.activity = a;
		this.msg = msg;
	}

	public void setListener(DialogListener listener)
	{
		if (listener != null)
			this.mListener = listener;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.confirmation_dialog);
		tvTitle = (TextView) findViewById(R.id.confirmation_dialog_screen_tvTitle);
		tvMessage = (TextView) findViewById(R.id.confirmation_dialog_screen_message);
		yes = (Button) findViewById(R.id.confirmation_dialog_screen_ok_button);
		yes.setOnClickListener(this);
		tvMessage.setText(msg);
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId()) {
			case R.id.confirmation_dialog_screen_ok_button:
				mListener.yesClcked();
				break;
			default:
				break;
		}
	}
}
