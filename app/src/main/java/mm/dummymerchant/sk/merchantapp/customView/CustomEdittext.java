package mm.dummymerchant.sk.merchantapp.customView;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

import mm.dummymerchant.sk.merchantapp.Utils.Utils;


public class CustomEdittext extends EditText
{

	public CustomEdittext(Context context)
    {
	    super(context);
    }


	public CustomEdittext(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
		setTypeface(Utils.getFont(context));
	}

	public CustomEdittext(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		setTypeface(Utils.getFont(context));

	}

}
