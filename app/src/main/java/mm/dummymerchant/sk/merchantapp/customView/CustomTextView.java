package mm.dummymerchant.sk.merchantapp.customView;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import mm.dummymerchant.sk.merchantapp.Utils.Utils;


public class CustomTextView extends TextView
{

	public CustomTextView(Context context)
	{
		super(context);
		setTypeface(Utils.getFont(context));
	}

	public CustomTextView(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);

		setTypeface(Utils.getFont(context));


	}

	public CustomTextView(Context context, AttributeSet attrs) throws RuntimeException
	{
		super(context, attrs);
		setTypeface(Utils.getFont(context));

	}


}
