package mm.dummymerchant.sk.merchantapp.db;

/**
 * Created by Dell on 10/27/2015.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import mm.dummymerchant.sk.merchantapp.MerchantSurvey.TransactionStatics;
import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.Constant;
import mm.dummymerchant.sk.merchantapp.Utils.Utils;
import mm.dummymerchant.sk.merchantapp.model.BankDetailsModel;
import mm.dummymerchant.sk.merchantapp.model.CashBankOutNoModel;
import mm.dummymerchant.sk.merchantapp.model.CashCollectorModel;
import mm.dummymerchant.sk.merchantapp.model.CashOutModel;
import mm.dummymerchant.sk.merchantapp.model.CashierActivityLogModel;
import mm.dummymerchant.sk.merchantapp.model.CashierModel;
import mm.dummymerchant.sk.merchantapp.model.CityList_Model;
import mm.dummymerchant.sk.merchantapp.model.ConstactModel;
import mm.dummymerchant.sk.merchantapp.model.CustomerSurveyModel;
import mm.dummymerchant.sk.merchantapp.model.MailDetails;
import mm.dummymerchant.sk.merchantapp.model.MasterDetails;
import mm.dummymerchant.sk.merchantapp.model.MerchantShopDetailsModel;
import mm.dummymerchant.sk.merchantapp.model.OTPCashierModel;
import mm.dummymerchant.sk.merchantapp.model.Profile;
import mm.dummymerchant.sk.merchantapp.model.Profile_;
import mm.dummymerchant.sk.merchantapp.model.Remarks;
import mm.dummymerchant.sk.merchantapp.model.TownShip_Model;
import mm.dummymerchant.sk.merchantapp.model.TransactionSurvey;
import mm.dummymerchant.sk.merchantapp.model.TransationModel;
import mm.dummymerchant.sk.merchantapp.model.PromotionModel;
import mm.dummymerchant.sk.merchantapp.model.UserInctivityModel;

public class DBHelper implements Constant {

  /*  public static String path= Environment.getExternalStorageDirectory().getAbsolutePath();
    public static final String DATABASE_NAME =path+ "/DummyMerchant/com.jas.money";*/
  public static final String DATABASE_NAME = "com.jas.money";
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_TABLE_TRANSATION = "transation_table";

    public static final String DATABASE_TABLE_ACTIVIY_lOG_CASHIER = "Activity_Log_Cashier";

    public static final String DATABASE_TABLE_JSON_CITY="json_city_list";
    public static final String DATABASE_TABLE_JSON_TOWNSHIP="json_town_list";
    public static final String DATABASE_TABLE_JSON_CONTACTS="json_contacts_table";
    public static final String DATABASE_OFFLINE_PAYMENT = "offline_payment";
    public static final String DATABASE_TABLE_TOPUP = "topup_details";
    public static final String DATABASE_TABLE_LOYALTY = "loyalty";
    public static final String DATABASE_TABLE_TRAIN_STATION_NAME = "trainStationName";
    public static final String DATABASE_TABLE_CONTACTS = "Contacts";
    public static final String DATABASE_TABLE_CASH_OUT = "Cashout";
    public static final String DATABASE_TABLE_PROMOTION_ADDVIEW = "promotion";


    public static final String DATABASE_TABLE_TICKET_IMAGES = "TicketImages";
    public static final String DATABASE_TABLE_RECEIVED_MONEY = "ReceivedMoney";
    public static final String DATABASE_TABLE_DEVICE_INFORMATION = "DeviceInformation";

    public static final String DATABASE_MASTER_MOBILE_NO = "master_mobileno_table";
    public static final String DATABASE_TABLE_BUS_CITY_DATA = "BUSCITY";
    public static final String DATABASE_TABLE_AGENT_CITY = "cityTable";
    public static final String DATABASE_TABLE_AGENT_TOWNSHIP = "tonshipTable";
    public static final String DATABASE_TABLE_MAIL_DETAILS="mail_details";
    public static final String DATABASE_TABLE_REMARKS="remarks";
    public static final String DATABASE_TABLE_USERINACTIVE="user_inactive";

    public static final String KEY_BUSINESS_TYPE_LOGO = "businesstype_logo";
    public static final String KEY_AGENT_CITY_ID = "aCityId";
    public static final String KEY_AGENT_CITY_NAME = "aCityname";
    public static final String KEY_AGENT_CITY_NAME_BURMESE = "aCityname_Burmese";
    public static final String KEY_AGENT_TOWNSHIP_NAME = "aTownshipName";
    public static final String KEY_AGENT_TOWNSHIP_NAME_BURMESE = "aTownshipName_Burmese";

    public static final String KEY_AGENT_IS_DEFAULT_CITY = "is_default_city";
    public static final String KEY_AGENT_IS_DEFAULT_CITY_NAME = "default_city_name";
    public static final String KEY_AGENT_IS_DEFAULT_CITY_NAME_BURMESE = "default_city_bname";
    public static final String KEY_AGENT_IS_DEFAULT_CITY_NAME_D = "default_city_d";
    public static final String KEY_AGENT_IS_DEFAULT_CITY_NAME_D_BURMESE = "default_city_db";
    public static final String KEY_AGENT_TOWNSHIP_ID = "aTownshipId";
    public static final String KEY_BUS_CITY_ID = "buscityid";
    public static final String KEY_BUS_CITY_NAME = "name";
    public static final String ADVANCE_MERCHANT="Advance_Merchant";


    public static final String KEY_JSON_CITY_MODELS="json_city_models";
    public static final String KEY_JSON_TOWN_MODELS="json_town_models";
    // Train Station table colum
    public static final String KEY_TRAIN_STATION_NAME = "TstationName";
    public static final String KEY_TRAIN_STATION_ID = "TstationID";
    public static final String KEY_TRAIN_STATION_CELL_TOWER = "TStationTower";

//Activity table column name

    public static final String KEY_CASHIER_AUTO_PRIMARY_KEY = "auto_id";
    public static final String KEY_CASHIER_LOGIN_TIME = "cashier_logintime";
    public static final String KEY_CASHIER_LOGOUT_TIME = "cashier_logouttime";
    public static final String KEY_CASHIER_LOGOUT_REASON = "cashier_logout_reason";
    public static final String KEY_CASHIER_NUM_OF_TRANSACTIONS = "cashier_no_of_transactions";

    public static final String KEY_NAME = "name";
    public static final String KEY_EMAIL_ID = "email";
    public static final String KEY_KEY_COUNT = "count";
    public static final String KEY_COMMEENT = "comment";

    public static final String KEY_REFERENCE_ID = "referenceID";
    public static final String KEY_AMOUNT = "amount";
    public static final String KEY_MOBILE_NUMBER = "mobileNumber";
    public static final String KEY_ISCONTACT_UPLOAED = "isContactUpload";
    public static final String KEY_CAR_NUMBER = "carNo";
    public static final String KEY_TICKET_NUMBER = "ticketNumber";
    public static final String KEY_MESSAGE = "message";
    public static final String KEY_DATE = "date";
    public static final String KEY_TRANACATION_TYPE = "tType";
    public static final String KEY_TRANSTYPE = "istrans";
    public static final String KEY_TRANSID = "transid";
    public static final String UNIQUE_ID = "unique_id";
    public static final String KEY_DESCRIPTION = "description";
    public static final String KEY_AGENT_NUM = "agentnumber";
    public static final String KEY_BUS_TICKET_NUM = "busticketnum";
    public static final String KEY_FEE = "fee";
    public static final String KEY_SERVICE_TAX = "servicetax";
    public static final String KEY_DESTINATION_NUM = "destination";
    public static final String KEY_OLD_AMOUNT = "oldamount";
    public static final String KEY_NEW_AMOUNT = "newamount";
    public static final String KEY_AGENT_NAME = "agentname";
    public static final String KEY_TRANSCATION_SURVERY = "survery";
    public static final String KEY_TRANSCATION_WALLET_BALANCE = "walletbalance";



    public static final String DATABASE_TABLE_CITY = "CITY";
    public static final String DATABASE_TABLE_STATE = "STATE";
    public static final String COLUMN_PRIMARY_KEY_ID = "_id";


    // Key
    public static final String KEY_STATE_ID = "state_id";
    public static final String KEY_STATE_CODE = "state_code";
    public static final String KEY_STATE_DESCRIPTION = "state_description";
    public static final String KEY_CITY_ID = "city_id";
    public static final String KEY_CITY_CODE = "city_code";
    public static final String KEY_CITY_DESCRIPTION = "city_description";
    public static final String KEY_CITY_STATE_ID = "city_state_id";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_PHOTO_URL = "photourl";

    public static final String COLUMN_DEVICE_ID = "device_id";
    public static final String COLUMN_MOBILE_NO = "mobile_no";
    public static final String COLUMN_SIM_ID = "SIM_ID";
    public static final String COLUMN_CUS_ID = "customerID";
    public static final String COLUMN_ISRead = "read";


    public static final String COLUMN_BENIFITIARY = "benifitiary_no";
    public static final String COLUMN_SOURCE_NO = "source_no";
    public static final String COLUMN_AMOUNT = "amount";
    public static final String COLUMN_DATE = "date";


    /**
     * ******************************************************************************
     */

    public static final String COLUMN_TOPUP_DESTINATION_NO = "destination_no";
    public static final String COLUMN_TOPUP_MOBILE_NO = "mobile_no";
    public static final String COLUMN_TOPUP_AMOUNT = "amount_no";
    public static final String COLUMN_TOPUP_TRANSTYPE = "transtype";
    public static final String COLUMN_TOPUP_TRANSTIME = "transtime";
    public static final String COLUMN_TOPUP_BALANCE = "balance";
    public static final String COLUMN_TOPUP_TRANSID = "transid";

    /**
     * ******************************************************************************
     */
    public static final String COMMENTS = "comments";
     public static final String  AGENTCODE="AgentCode";

    // loyalty
    public static final String KEY_LOYALTY_POINT = "points";
    public static final String KEY_LOYALTY_MERCHANT = "merchant";

    public static final String KEY_IMAGE_PATH = "imagePath";


    public static final String KEY_R_IS_STATE_DIVISION = "is_state_dicision";

    public static final String KEY_CELL_ID = "cell_id";
    public static final String KEY_LAT_LONG = "lat_long";
    public static final String KEY_STATUS_PENDING = "status";
    public static final String DATABASE_TABLE_DISTRIBUTOR = "distributor";
    public static final String DATABASE_TABLE_TOPUP_TRANSACTION = "transaction";
    public static final String COLUMN_DISTRIBUTOR_TELECOM = "telecom";
    public static final String COLUMN_DISTRIBUTOR_NAME = "distributor_name";
    public static final String COLUMN_DISTRIBUTOR_NUMBER = "distributor_number";
    public static final String COLUMN_DESTINATION = "des";
    public static final String TRAIN_STATION_LEFT_VALUE = "left_value";
    public static final String DATABASE_NAME_CASHIER = "Cashier";
    public static final String DATABASE_CASHIER_TABLE_TRANSACTION = "cashier_transaction_table";
    public static final String DATABASE_CASHIER_TABLE_RECEIVED_MONEY = "cashier_ReceivedMoney";


    // Train Station offline calculation table column
    public static final String TRAIN_STATION_UID = "trainUID";
    public static final String TRAIN_STATION_RIGHT_VALUE = "right_value";
    public static final String TRAIN_STATION_SUBWAY_ID = "trainSubWayId";
    public static final String TRAIN_STATION_JUNCTION_NAME = "train_junction_id";
    // Cell ID table column
    public static final String CELL_ID_TABLE = "AutoPayment";
    public static final String CELL_ID_NUMBER = "cell_no";
    public static final String BUSINESS_NAME = "name";
    ////////KICKBACK
    public static final String VIEW_KICKBACK = "viewkickback";
    public static final String KICKBACK_MINRANGE = "minrange";
    public static final String KICKBACK_MAXRANGE = "maxrange";
    public static final String KICKBACK_DISBTYPE = "disbtype";
    public static final String KICKBACK_PERAMT = "peramt";
    public static final String KICKBACK_AMT = "amount";
    public static final String KICKBACK_Status = "status";
    public static final String KICKBACK_UPDATE_BIT = "update_bit";
    //////DUMMYMERCHANTLIST
    public static final String DUMMYMERCHANTLIST_TABLE = "dummymerchantlist_table";
    public static final String DUMMYMERCHANTLIST_NAME = "name";
    public static final String DUMMYMERCHANTLIST_MOBILENO = "mobileno";
    public static final String DUMMYMERCHANTLIST_COUNTRY = "country";
    public static final String DUMMYMERCHANTLIST_STATE = "state";
    public static final String DUMMYMERCHANTLIST_CITY = "city";
    public static final String DUMMYMERCHANTLIST_STATUS = "status";
    //CountryCodeTable
    public static final String TABLE_COUNTRY_LIST = "mCountryList";
    public static final String COLUMN_COUNTRY_CODE_FLAG = "mCountryCodeFlag";
    public static final String COLUMN_COUNTRY_CODE = "mCountryCode";
    public static final String COLUMAN_COUNTRY_NAME = "mCountryName";
    public static final String COLUMN_MCC_CODE = "mMccCode";
    //NetWorkList
    public static final String TABLE_NETWORK_LIST = "mNetworkList";
    public static final String COLUMN_MNC = "mMnc";
    public static final String COLUMN_NETWORK_NAME = "mNetworkName";
    public static final String COLUMN_OPERATOR_NAME = "mOperatorName";
    public static final String COLUMN_OPERATOR_STATUS = "mOperatorStatus";
    // DIALINGCODE
    public static final String TABLE_DIALING_CODE = "mDialingCode";
    public static final String COLUMN_DIALING_CITY = "mDialingCity";
    public static final String COLUMN_DIALING_CODE = "mDialingCODE";
    /////Agent
    public static final String Agent_TABLE = "agent";
    public static final String Agent_agentcode = "agentcode";
    public static final String Agent_source = "source";
    public static final String Agent_agenttype = "agenttype";
    public static final String Agent_userpack = "userpack";
    public static final String Agent_agentname = "agentname";
    public static final String Agent_fathername = "fathername";
    public static final String Agent_mothername = "mothername";
    public static final String Agent_email = "email";
    public static final String Agent_dob = "dob";
    public static final String Agent_idprooftype = "idprooftype";
    public static final String Agent_dmsrefid = "dmsrefid";
    public static final String Agent_occupation = "occupation";
    public static final String Agent_businessname = "businessname";
    public static final String Agent_country = "country";
    public static final String Agent_city = "city";
    public static final String Agent_address1 = "address1";
    public static final String Agent_village = "village";
    public static final String Agent_district = "district";
    public static final String Agent_status = "status";
    public static final String Agent_customerstatus = "customerstatus";
    public static final String Agent_language = "language";
    public static final String Agent_resultcode = "resultcode";
    public static final String Agent_resultdescription = "resultdescription";
    public static final String Agent_requestcts = "requestcts";
    public static final String Agent_responsects = "responsects";
    public static final String Agent_vendorcode = "vendorcode";
    public static final String Agent_transid = "transid";
    public static final String Agent_agenttransid = "agenttransid";
    public static final String Agent_vendortransid = "vendortransid";
    public static final String Agent_clienttype = "clienttype";
    public static final String Agent_comments = "comments";
    public static final String Agent_mobileno = "mobileno";
    public static final String Agent_macaddress = "macaddress";
    public static final String Agent_securetoken = "securetoken";
    public static final String Agent_attemptcounter = "attemptcounter";
    public static final String Agent_rgts = "rgts";
    public static final String Agent_phoneno = "phoneno";
    public static final String Agent_pgstatus = "pgstatus";
    public static final String Agent_gender = "gender";
    public static final String Agent_nominee = "nominee";
    public static final String Agent_nomineerelation = "nomineerelation";
    public static final String Agent_clientip = "clientip";
    public static final String Agent_udv1 = "udv1";
    public static final String Agent_age = "age";
    ///////////////////train offline Stations table
    public static final String TRAIN_STATION_TABLE = "train_station";
    public static final String TRAIN_ID = "id";
    public static final String TRAIN_StationName = "stationname";
    public static final String TRAIN_StationNumber = "stationnumber";
    public static final String TRAIN_Is_Active = "is_active";
    public static final String TRAIN_Is_Circular = "is_circular";
    public static final String TRAIN_CreatedDate = "createddate";
    public static final String TRAIN_UpdatedDate = "updateddate";
    public static final String TRAIN_MobileNumber = "mobilenumber";
    public static final String TRAIN_Sector = "sector";
    ///////////////////train offline Stations RouteLimits
    public static final String TRAIN_ROUTELIMITS_TABLE = "train_station_routelimit";
    public static final String TRAIN_RouteLimits_ID = "id";
    public static final String TRAIN_RouteLimits_StationID = "stationid";
    public static final String TRAIN_RouteLimits_LeftLimit = "leftlimit";
    ///////////////////////////////
    public static final String TRAIN_RouteLimits_RightLimit = "rightlimit";
    public static final String TRAIN_RouteLimitsIs_SubLeft = "subleft";
    public static final String TRAIN_RouteLimits_SubRight = "subright";
    public static final String TRAIN_RouteLimits_Is_Active = "is_active";
    public static final String TRAIN_RouteLimits_CreatedDate = "createddate";
    public static final String TRAIN_RouteLimits_UpdatedDate = "UpdatedDate";

    // retrieve profile

    public static final String PHONENUMBER = "phoneNumber";
    public static final String AGENTTYPE = "agentType";
    public static final String AGENTID = "agentID";
    public static final String FATHERNAME = "FatherName";
    public static final String DOB = "DOB";
    public static final String PREDEFINENUMBER = "PredefineNumber";
    public static final String ADDRESSLINE1 = "addressLine1";
    public static final String ADDRESSLINE2 = "addressLine2";
    public static final String ISACTIVE = "isActive";
    public static final String RECOMMENDEDNO = "RecommendedNo";

    public static final String KEY_CASHIER_CREATED_DATE = "cashier_created_date";
    public static final String KEY_CASHIER_NAME = "cashier_name";
    public static final String KEY_CASHIER_NUMBER = "cashier_number";
    public static final String KEY_CASHIER_COLOR = "cashier_color";
    public static final String KEY_CASHIER_PASSWORD = "cashier_pwd";
    public static final String KEY_CASHIER_CONFIRM_PASSWORD = "cashier_confirm_pwd";
    public static final String KEY_CASHIER_PHOTO="cashier_photo";
    public static final String KEY_CASHIER_STATUS="cashier_status";
    public static final String KEY_CASHIER_JOBTYPE="cashier_jobtype";

    public static final String KEY_CASHIER_ID = "cashier_id";
    public static final String KEY_MASTER_MOBILENO = "key_master_mobile";
    public static final String KEY_MASTER_PASSWORD = "key_master_password";
    public static final String KEY_HOURS = "hours";
    public static final String KEY_FROM_MAIL_ID="from_emailid";
    public static final String KEY_FROM_MAIL_PASS="password";
    public static final String KEY_TO_MAIL_ID="to_emailid";

    ///////////////////////////////

    public static final String KEY_LOGIN_TIME="login_time";
    public static final String KEY_LOGOUT_TIME="logout_time";
    public static final String KEY_STATUS="status";

    public static final String TRAIN_STATION_TABLE_ALL = "train_station_all";

    //Survey Table
    public static final String SURVEY_DETAILS = "CustomerSurvey";
    public static final String SURVEY_QUES1 = "ques1";
    public static final String SURVEY_QUES2 = "ques2";
    public static final String SURVEY_QUES3 = "ques3";
    public static final String SURVEY_QUES4 = "ques4";
    public static final String SURVEY_QUES5 = "ques5";
    public static final String SURVEY_QUES6 = "ques6";
    public static final String SURVEY_QUES7 = "ques7";
    public static final String SURVEY_QUES8 = "ques8";
    public static final String SURVEY_TOTAL = "total";
    public static final String SURVEY_PHONE = "phone";
    public static final String SURVEY_SUGGESTION = "suggestion";
    public static final String SURVEY_GENDER = "gender";
    public static final String SURVEY_AGE = "age";
    public static final String SURVEY_LOCATION = "city";
    public static final String SURVEY_DAY="day";
    public static final String SURVEY_HOUR="hour";
    public static final String SURVEY_MINUTE="minutes";
    public static final String SURVEY_WEEK="week";
    public static final String SURVEY_MONTH="month";
    public static final String SURVEY_YEAR="year";
    public static final String SURVEY_DATE="date";
    public static final String SURVEY_AMOUNT="amount";
    public static final String SURVEY_DATE_HOUR="date_hour";
    public static final String SURVEY_TRANS_TYPE="type";

    public static final String KEY_TRANSCATION_DATE = "transcationDate";
    public static final String KEY_TRANSCATION_RECEIVED_KIKBACK = "receivedKickabck";
  public static final String KEY_TRANSACTION_OPERATOR = "operator";


    public static final String CASHCOLLECTOR_TABLE = "cashcollector";
    public static final String CASHCOLLEC_ID = "cashcollec_id";
    public static final String CASHCOLLEC_PHOTO = "cashcollec_photo";
    public static final String CASHCOLLEC_NAME = "cashcollec_name";
    public static final String CASHCOLLEC_SOURCE = "cashcollec_source";
    public static final String CASHCOLLEC_STATUS="cashcollector_status";
    public static final String CASHCOLLEC_NO = "cashcollec_no";
    public static final String CASHCOLLEC_SIGN = "cashcollec_sign";




    public static final String OTP_LOG_TIME = "Otp_log_Time";
    public static final String OTP_LOG_CASHCOLLECTOR_NAME = "Otp_log_Cashcollectorname";
    public static final String OTP_LOG_CASHCOLLECTOR_NO = "Otp_log_Cashcollectorno";
    public static final String OTP_LOG_CASHIER_ID = "Otp_cashier_id";
    public static final String OTP_LOG_TextValue = "Otp_TextValue";
    public static final String OTP_LOG_TemplateValue = "Otp_TemplateValue";
    public static final String DATABASE_TABLE_OTP_lOG_DETAILS_FOR_QR_GENERATE  = "Log_Details_Generate_QRcode";



    public static final String CASH_OUT_ID = "cashout_id";
    public static final String CASH_OUT_TIME = "cashout_time";
    public static final String CASH_OUT_CUS_NAME = "cashout_customername";
    public static final String CASH_OUT_CUS_AMT = "cashout_customeramount";
    public static final String CASH_OUT_COM_TYPE = "cashout_comision_type";
    public static final String CASH_OUT_COM_AMT = "cashout_comision_amount";
    public static final String CASH_OUT_COM_FINAL_AMT = "cashout_final_amount";
    public static final String CASH_OUT_CUS_SIGN = "cashout_customersign";

    // retrieve profile

    public static final String USERPROFILE = "UserProfile";
    public static final String MOBILENUMBER="MobileNumber";
    public static final String NAME="Name";
    public static final String GCMID="GcmID";
    public static final String ENCRYPTED="Encrypted";
    public static final String SIMID="SimID";
    public static final String MSID="MSID";
    public static final String IMEI="IMEI";
    public static final String LTYPE="lType";
    public static final String APPID="AppID";
    public static final String RECOMMENDED="Recommended";
    public static final String STATE="State";
    public static final String TOWNSHIP="Township";
    public static final String FATHER="Father";
    public static final String GENDER="Gender";
    public static final String DATEOFBIRTH="DateOfBirth";
    public static final String NRC="NRC";
    public static final String IDTYPE="IDType";
    public static final String PHONE="Phone";
    public static final String BUSINESSTYPE="BusinessType";
    public static final String BUSINESSCATEGORY="BusinessCategory";
    public static final String CAR="Car";
    public static final String CARTYPE="CarType";
    public static final String LATITUDE="Latitude";
    public static final String LONGITUDE="Longitude";
    public static final String ADDRESS1="Address1";
    public static final String ADDRESS2="Address2";
    public static final String ACCOUNTTYPE="AccountType";
    public static final String OSTYPE="OSType";
    public static final String PROFILEPIC="ProfilePic";
    public static final String SIGNATURE="signature";
    public static final String PASSWORD="Password";
    public static final String EMAILID="EmailID";
    public static final String ADDRESSTYPE="AddressType";
    public static final String CELLTOWERID="cell_id";
    public static final String BUSINESSNAME="BusinessName";
    public static final String KICKBACK_COLUMN="Kickback";
    public static final String CODEALTERNATE="CodeAlternate";
    public static final String COUNTRY="Country";
    public static final String PARENTACCOUNT="ParentAccount";
    public static final String CODERECOMMENDED="CodeRecommended";
    public static final String PAYMENTGATEWAY="PaymentGateway";
    public static final String LOYALTY="Loyalty";
    public static final String SECURETOKEN="SecureToken";
    public static final String ACCOUNTNUMBER="AccountNumber";
    public static final String BANKNAME="BankName";
    public static final String FBEMAILID="FBEmailId";
    public static final String AGE="age";

    public static final String KEY_JSON_CONTACT_DATA="json_contacts_column";

    public static int TRANSACTION_COUNT = 0;
    private static SQLiteDatabase db = null;
    private static Context mContext = null;
    private DatabaseHelper dbHelper = null;

  public static final String OTP_LOG_ID = "log_Trans_Id";
  public static final String SCANNING_TIME = "Scanning_Time";
  public static final String GENERATE_TIME = "Generate_Time";
  public static final String OTP_ENTER_TIME = "OTP_Enter_Time";
  public static final String SCANNING_LOCATION = "Scanning_Location";
  public static final String GENERATE_LOCATION = "Generate_Location";
  public static final String OTP_ENTER_LOCATION = "OTP_Enter_Location";
  public static final String OTP_LOG_CASHIER_NAME = "Otp_log_Cashiername";
  public static final String OTP_LOG_CASHIER_NO = "Otp_log_Cashierno";
  public static final String OTP_LOG_AMT = "Otp_log_Amount";
  public static final String OTP_LOG_STATUS = "Otp_log_Status";
  public static final String OTP_LOG_PaidSTATUS = "Otp_paid_Status";
  public static final String FROM_WHICH_SCAN = "From_which_Scan";
  public static final String GENERATER_NO = "Generater_No";
  public static final String GENERATER_NAME = "Generater_Name";
  public static final String SCAN_LOG_TEXTVALUE = "Text_value";
  public static final String SCAN_LOG_TEMPLATEVALUE = "Template_value";
  public static final String SCAN_LOG_OPEN_OR_CLOSE = "OpenORclose";
  public static final String DATABASE_TABLE_OTP_lOG_DETAILS_FOR_QR_SCAN = "Log_Details_Scan_QRcode";
  public static final String OTP_LOG_JSON_DATA="jsondata";


  //db.AddQRcode_data(timeStamp, Date, Receiver_Name,Amount,Transid,Sender_No,Receiver_No,Trans_Type,Kick_Back,Locality,Location,Cellid,Gender,Age);
  public static final String DATABASE_TABLE_QR_SCAN_FROM_OKAPP = "Scan_QRcode_From_Okapp";
  public static final String OK_ID = "Ok_Id";
  public static final String OK_MasterORCashier_ID = "MasterORCashier_ID";
  public static final String OK_TIME_STAMP = "TimeStamp";
  public static final String OK_DATE = "Date";
  public static final String OK_RECEIVER_NAME = "Receiver_Name";
  public static final String OK_AMOUNT = "Amount";
  public static final String OK_TRANSID = "Trans_id";
  public static final String OK_SENDER_NO = "Sender_No";
  public static final String OK_RECEIVER_NO = "Receiver_No";
  public static final String OK_TRANS_TYPE = "Trans_Type";
  public static final String OK_KICK= "Kick";
  public static final String OK_LOCALITY = "Locality";
  public static final String OK_LOCATION = "Location";
  public static final String OK_CELL_ID = "Cellid";
  public static final String OK_GENDER = "Gender";
  public static final String OK_AGE = "Age";
  public static final String OK_STATUS = "SuccessORFail";


  // Merchant Shop Details
  public static final String MerchantShopTable="merchantShopDetails";
  public static final String ShopID="shopid";
  public static final String ShopName="shopName";
  public static final String ShopCellId="shopCellid";
  public static final String ShopStatus="shopStatus";
  public static final String ShopWifiName="shopWifi";
  public static final String ShopLat="lat";
  public static final String ShopLang="lang";


  //TODO PRomotion database colom
  public static final String PROMOTION_TITLE = "pTitle";
  public static final String PROMOTION_DESCRIPTION = "pDescriotion";
  public static final String PROMOTION_MOBILENO = "pMObileNo";
  public static final String PROMOTION_WEBSITE = "pWebsite";
  public static final String PROMOTION_FACEBOOK = "pFacebook";
  public static final String PROMOTIONID = "pID";
  public static final String LAST_UPDATED = "last_updated";
  public static final String EMAIL = "email";
  public static final String CONTACT_NAME = "name";


  //CashBankOut Table
  public static final String CASHBANKOUTNUMBER = "CashBankOutNumber";
  public static final String MSISIDN = "Msisidn";
  public static final String COUNTRYCODE = "Countrycode";
  ///////////////////////////////


  //// BankDetail Table
  public static final String BANKDETAIL = "BankDetail";
  public static final String BRANCH = "Branch";
  public static final String BRANCH_ADDRESS = "Address";
  public static final String BANK_PHONE = "Phone";
  public static final String IDNO = "ID_NO";
  public static final String BRANCH_ID = "Branch_Id";
  public static final String BANKNAME_BURMESE = "BankNameBurmese";
  public static final String BRANCHNAME_BURMESE = "BranchNameBurmese";
  public static final String BANK_ID = "BankId";



  public static final String REMARKS_TRANSID="remarks_transid";
  public static final String REMARKS_CELLID="remarks_cellid";
  public  static final String REMARKS_LAT="remarks_latitude";
  public static final String REMARKS_LONG="remarks_longitude";
  public static final String REMARKS_MCC="remarks_mcc";
  public static final String REMARKS_MNC="remarks_mnc";
  public static final String REMARKS_TELEPHONE_PERATOR="remarks_operator";
  public static final String  REMARKS_COMMENTS="comments";

  public  static final String USERINACTIV_CASHIERID="user_cashId";
  public static final String USERINACTIV_CASHIERNAME="user_cashname";
  public static final String USERINACTIV_CASHIERNO="user_cashierNo";
  public static final String USERINACTIV_CELLID="user_cellid";
  public static final String USERINACTIV_LAT="user_lat";
  public static final String USERINACTIV_LONG="user_long";
  public static final String USERINACTIV_MCC="mcc";
  public static final String USERINACTIV_MNC="mnc";
  public static final String USERINACTIV_TELEPHONE_OPERATOR="userinactiv_operator";
  public static final String USERACTIV_DURATION="useractiv_duraion";



  public DBHelper(Context ctx) {
        mContext = ctx;
        dbHelper = new DatabaseHelper(ctx);
        open();
    }


  public void AddCollectortoCollectorScanDetails(String MasterORCashier_ID,String ScanningTime,String GenerateTime,String OTPEnterTime,String ScanningLocation,String GenerateLocation,String OTPEnterLocation,String name, String no, String amt, String status,String receivedorpaidstatus,String which_scan,String generateName,String generateNo,String Text,String Template,String openorclose) {

    Log.d("Values ", ScanningTime + GenerateTime + OTPEnterTime + ScanningLocation + GenerateLocation + OTPEnterLocation + name + no + amt + name + status + generateNo + generateName + openorclose);

    ContentValues values = new ContentValues();
    values.put(OK_MasterORCashier_ID, MasterORCashier_ID);
    values.put(SCANNING_TIME, ScanningTime);
    values.put(GENERATE_TIME, GenerateTime);
    values.put(OTP_ENTER_TIME, OTPEnterTime);
    values.put(SCANNING_LOCATION, ScanningLocation);
    values.put(GENERATE_LOCATION, GenerateLocation);
    values.put(OTP_ENTER_LOCATION, OTPEnterLocation);
    values.put(OTP_LOG_CASHIER_NAME, name);
    values.put(OTP_LOG_CASHIER_NO, no);
    values.put(OTP_LOG_AMT, amt);
    values.put(OTP_LOG_STATUS, status);
    values.put(OTP_LOG_PaidSTATUS, receivedorpaidstatus);
    values.put(FROM_WHICH_SCAN, which_scan);
    values.put(GENERATER_NO, generateNo);
    values.put(GENERATER_NAME, generateName);
    values.put(SCAN_LOG_TEXTVALUE, Text);
    values.put(SCAN_LOG_TEMPLATEVALUE, Template);
    values.put(SCAN_LOG_OPEN_OR_CLOSE, openorclose);

    db.insert(DATABASE_TABLE_OTP_lOG_DETAILS_FOR_QR_SCAN, null, values);
  }


  public void Add_OkToSafetyCashier_ScanDetails(String MasterORCashier_ID,String timeStamp,String Date,String Receiver_Name,String Amount,String Transid,String Sender_No,String Receiver_No, String Trans_Type, String Kick_Back, String Locality,String Location,String Cellid,String Gender,String Age,String Generate_Loc,String Scan_Loc,String SuccessORFail) {

    Log.d("Values ", MasterORCashier_ID + timeStamp + Date + Receiver_Name + Amount + Transid + Sender_No + Receiver_No + Trans_Type + Kick_Back + Locality + Location + Cellid + Gender + Age + Generate_Loc + Scan_Loc + SuccessORFail);

    ContentValues values = new ContentValues();
    values.put(OK_MasterORCashier_ID, MasterORCashier_ID);
    values.put(OK_TIME_STAMP, timeStamp);
    values.put(OK_DATE, Date);
    values.put(OK_RECEIVER_NAME, Receiver_Name);
    values.put(OK_AMOUNT, Amount);
    values.put(OK_TRANSID, Transid);
    values.put(OK_SENDER_NO, Sender_No);
    values.put(OK_RECEIVER_NO, Receiver_No);
    values.put(OK_TRANS_TYPE, Trans_Type);
    values.put(OK_KICK, Kick_Back);
    values.put(OK_LOCALITY, Locality);
    values.put(OK_LOCATION, Location);
    values.put(OK_CELL_ID, Cellid);
    values.put(OK_GENDER, Gender);
    values.put(OK_AGE, Age);
    values.put(GENERATE_LOCATION, Generate_Loc);
    values.put(SCANNING_LOCATION, Scan_Loc);
    values.put(OK_STATUS, SuccessORFail);
    db.insert(DATABASE_TABLE_QR_SCAN_FROM_OKAPP, null, values);
  }


  public Cursor Get_Details(String MasterORCashier_ID,String whichscan) {
    String where = OK_MasterORCashier_ID + " = "+ "'"+ MasterORCashier_ID +"'"  + " AND " + FROM_WHICH_SCAN + " = " + "'"+ whichscan +"'"  ;
    String query = "SELECT * FROM " + DATABASE_TABLE_OTP_lOG_DETAILS_FOR_QR_SCAN  + " where " + where + "  ORDER BY " + OTP_LOG_ID + " DESC";
    Log.i("Get_Details_Query", query);
    Cursor c = db.rawQuery(query, null);
    if (c != null) {
      c.moveToFirst();
    }
    return c;
  }


    public Cursor Get_Details_From_OK_ScanDetails(String MasterORCashier_ID) {
    String where = OK_MasterORCashier_ID + " = "+ "'"+ MasterORCashier_ID +"'" ;
    String query = "SELECT * FROM " + DATABASE_TABLE_QR_SCAN_FROM_OKAPP + " where " + where + "  ORDER BY " +OK_TIME_STAMP + " DESC";
    //String query = "SELECT * FROM " + DATABASE_TABLE_QR_SCAN_FROM_APP  + "  ORDER BY " +OK_TIME_STAMP + " DESC";
    Log.i("OK_Scan_Query", query);
    Cursor c = db.rawQuery(query, null);
    if (c != null) {
      c.moveToFirst();
    }
    return c;
  }

  public static boolean isTranscationPresent_QRcode(String transId) {
    boolean check = false;
    Cursor cursor = db.rawQuery("select  * from " + DATABASE_TABLE_QR_SCAN_FROM_OKAPP + " where " + OK_TRANSID + " ='" + transId + "'", null);
    if (cursor != null) {
      if (cursor.getCount() > 0) {
        check = true;
      }
      cursor.close();
    }
    return check;
  }

  public boolean Delete(String fail)
  {
    int doneDelete_DS = 0;
    String where = OK_STATUS + " = "+ "'"+ fail +"'" ;
    doneDelete_DS = db.delete(DATABASE_TABLE_QR_SCAN_FROM_OKAPP,where, null);
    return doneDelete_DS > 0;
  }

    public Cursor Get_Log_Details_From_OK(String status,String MasterORCashier_ID)
    {
    String where = OK_STATUS + " = "+ "'"+ status +"'" + " AND " + OK_MasterORCashier_ID + " = " + "'"+ MasterORCashier_ID +"'"  ;
    String query = "SELECT * FROM " + DATABASE_TABLE_QR_SCAN_FROM_OKAPP + " where " + where + "  ORDER BY " +OK_TIME_STAMP + " DESC";
    Log.i("OK_Scan_Query", query);
    Cursor c = db.rawQuery(query, null);
    if (c != null) {
      c.moveToFirst();
    }
    return c;
  }


  public Cursor Get_Generate_QR_Log_Details(String status,String MasterORCashier_ID)
  {
    String where = OTP_LOG_STATUS + " = "+ "'"+ status +"'" + " AND " + OTP_LOG_CASHIER_ID + " = " + "'"+ MasterORCashier_ID +"'"  ;
  //  String query = "SELECT * FROM " + DATABASE_TABLE_OTP_lOG_DETAILS_FOR_QR_GENERATE + " where " + where + " ORDER BY " + OTP_LOG_TIME + " DESC";
   String query="SELECT * FROM  DATABASE_TABLE_OTP_lOG_DETAILS_FOR_QR_GENERATE";
    Log.i("Generate_Query", query);
    Cursor c = db.rawQuery(query, null);
    if (c != null) {
      c.moveToFirst();
    }
    return c;
  }


  public Cursor Get_SUCCESS_Scan_Log_Details(String status,String MasterORCashier_ID)
  {
    String where = OTP_LOG_STATUS + " = "+ "'"+ status +"'" + " AND " + OK_MasterORCashier_ID + " = " + "'"+ MasterORCashier_ID +"'"  ;
    String query = "SELECT * FROM "+ DATABASE_TABLE_OTP_lOG_DETAILS_FOR_QR_SCAN + " where " + where  + "  ORDER BY " + OTP_LOG_ID + " DESC";
    Log.i("Scan_Log_Details_Query", query);
    Cursor c = db.rawQuery(query, null);
    if (c != null) {
      c.moveToFirst();
    }
    return c;
  }


    public static void insertJsonCityModel(String jsonCityModel)
    {
        ContentValues insertValues = new ContentValues();
        insertValues.put(KEY_JSON_CITY_MODELS,jsonCityModel);
        db.insert(DATABASE_TABLE_JSON_CITY, null, insertValues);

    }

  public static int getJsonCityCount() {

    Cursor cursor = db.rawQuery("select  * from " + DATABASE_TABLE_JSON_CITY, null);
    if (cursor == null)
      return 0;
    else {
      if (cursor.getCount() > 0)
      {
        cursor.moveToFirst();
        int count=cursor.getCount();
        if (!cursor.isClosed())
          cursor.close();
        return count;
      } else
        return 0;

    }
  }

  public static int getJsonTownshipCount() {

    Cursor cursor = db.rawQuery("select  * from " + DATABASE_TABLE_JSON_TOWNSHIP, null);
    if (cursor == null)
      return 0;
    else {
      if (cursor.getCount() > 0)
      {
        cursor.moveToFirst();
        int count=cursor.getCount();
        if (!cursor.isClosed())
          cursor.close();
        return count;
      } else
        return 0;

    }
  }

  public static int getJsonContactCount() {

    Cursor cursor = db.rawQuery("select  * from " + DATABASE_TABLE_JSON_CONTACTS, null);
    if (cursor == null)
      return 0;
    else {
      if (cursor.getCount() > 0)
      {
        cursor.moveToFirst();
        int count=cursor.getCount();
        if (!cursor.isClosed())
          cursor.close();
        return count;
      } else
        return 0;

    }
  }

    public static String getJsonCity()
    {
        String jsonData=null;
        Cursor cursor = db.rawQuery("select  * from " + DATABASE_TABLE_JSON_CITY, null);
        if(cursor==null)
            return jsonData;
        else
        {
            if(cursor.getCount()>0)
            {
                cursor.moveToFirst();
                jsonData=cursor.getString(cursor.getColumnIndex(KEY_JSON_CITY_MODELS));
            }
            else
                return jsonData;

            if(!cursor.isClosed())
                cursor.close();
        }

        return  jsonData;
    }

    public static void insertJsonTownShipModel(String jsonTownshipModel)
    {
        ContentValues insertValues = new ContentValues();
        insertValues.put(KEY_JSON_TOWN_MODELS,jsonTownshipModel);
        db.insert(DATABASE_TABLE_JSON_TOWNSHIP, null, insertValues);

    }

    public static String getJsonTownshipModel()
    {
        String jsonData=null;
        Cursor cursor = db.rawQuery("select  * from " + DATABASE_TABLE_JSON_TOWNSHIP, null);
        if(cursor==null)
            return jsonData;
        else
        {
            if(cursor.getCount()>0)
            {
                cursor.moveToFirst();
                jsonData=cursor.getString(cursor.getColumnIndex(KEY_JSON_TOWN_MODELS));
            }
            else
                return jsonData;

            if(!cursor.isClosed())
                cursor.close();
        }

        return  jsonData;
    }


    public static void insertJsonContact(String jsondata)
    {
        ContentValues insertValues = new ContentValues();
        insertValues.put(KEY_JSON_CONTACT_DATA,jsondata);
        db.insert(DATABASE_TABLE_JSON_CONTACTS,null,insertValues);
    }

    public static  void updateJsonContacts(String jsondata)
    {
        ContentValues insertValues = new ContentValues();
        insertValues.put(KEY_JSON_CONTACT_DATA,jsondata);
        db.update(DATABASE_TABLE_JSON_CONTACTS, insertValues, null, null);
    }

  public static void deleteJsonContacts()
  {
    db.delete(DATABASE_TABLE_JSON_CONTACTS, null, null);
  }


  public static String getJsonContacts()
    {
        String jsonData=null;
        Cursor cursor = db.rawQuery("select  * from " + DATABASE_TABLE_JSON_CONTACTS, null);
        if(cursor==null)
            return jsonData;
        else
        {
            if(cursor.getCount()>0)
            {
                cursor.moveToFirst();
                jsonData=cursor.getString(cursor.getColumnIndex(KEY_JSON_CONTACT_DATA));
            }
            else
                return jsonData;

            if(!cursor.isClosed())
                cursor.close();
        }

        return  jsonData;
    }

    public static boolean isReceivedMoneyPResent(String transId) {
        boolean check = false;
        Cursor cursor = db.rawQuery("select  * from " + DATABASE_TABLE_RECEIVED_MONEY + " where " + KEY_TRANSID + " ='" + transId + "'", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                check = true;
            }
            cursor.close();
        }
        return check;
    }


    public static boolean insertTransation(TransationModel model) {

        try {
            int hours=0;
            String date;
            ContentValues insertValues = new ContentValues();
            /*insertValues.put(KEY_CASHIER_ID, model.getCashierId());
            insertValues.put(KEY_CASHIER_NAME, model.getCashierName());
            insertValues.put(KEY_CASHIER_NUMBER, model.getCashierNumber());*/
            insertValues.put(KEY_AGENT_NAME, model.getName());
            insertValues.put(KEY_DESCRIPTION, model.getDescripton());
            insertValues.put(KEY_DESTINATION_NUM, model.getDestination());
            insertValues.put(KEY_TRANSID, model.getTransatId().replace(".", ""));
            insertValues.put(KEY_NEW_AMOUNT, model.getNewamount());
            insertValues.put(KEY_OLD_AMOUNT, model.getOldAmount());
            insertValues.put(KEY_AMOUNT, model.getAmount());
            insertValues.put(KEY_TRANSCATION_RECEIVED_KIKBACK, model.getReceivedKickback());
            insertValues.put(KEY_TRANSCATION_DATE,model.getmTranscationDate().getTime());
                        if (model.getDate().trim().equals("")) {
                insertValues.put(KEY_DATE, model.getResponsects());
                date=model.getResponsects();
                hours= Utils.getHourFromDateTime(model.getResponsects());

            } else {
                insertValues.put(KEY_DATE, model.getDate());
                hours= Utils.getHourFromDateTime(model.getDate());
                date=model.getDate();
            }
            insertValues.put(KEY_HOURS,""+hours);
            insertValues.put(KEY_FEE, model.getFee());
            insertValues.put(KEY_TRANSTYPE, model.getIsCredit());
            insertValues.put(KEY_SERVICE_TAX, model.getServiceTax());
            insertValues.put(KEY_COMMEENT, model.getComments());
            insertValues.put(KEY_TRANACATION_TYPE, model.getTrnastionType());
            if (!Utils.isEmpty(model.getDestinationPersonName())) {
                insertValues.put(KEY_NAME, model.getDestinationPersonName());
            } else {
                insertValues.put(KEY_NAME, "Unknown");
            }
            if (model.getBalance().isEmpty())
                insertValues.put(KEY_NEW_AMOUNT, "");
            else
                insertValues.put(KEY_NEW_AMOUNT, model.getBalance());

            insertValues.put(KEY_IMAGE_PATH, model.getPhotoUrl());

            insertValues.put(KEY_LAT_LONG, model.getLatLong());
            insertValues.put(KEY_STATUS_PENDING, "false");

            insertValues.put(KEY_TRANSCATION_SURVERY, model.getmSurvery());
            insertValues.put(KEY_TRANSCATION_WALLET_BALANCE,model.getmWalletbalance());
            insertValues.put(KEY_CITY_CODE,model.getCity());
            insertValues.put(GENDER,model.getGender());
            insertValues.put(AGE,model.getAge());
            insertValues.put(CELLTOWERID,model.getCellID());
            Cursor cursor = db.rawQuery("select " + KEY_TRANSID + " from " + DATABASE_TABLE_TRANSATION + " where " + KEY_TRANSID + " ='" + model.getTransatId() + "'", null);
            if (cursor != null) {
                if (cursor.getCount() > 0) {

          //----------------------------
                    if (!model.getComments().contains("")) {
                        insertValues.put(KEY_COMMEENT, model.getComments());
                    }
                    if (!model.getBalance().trim().isEmpty()) {
                        insertValues.put(KEY_NEW_AMOUNT, model.getBalance());
                    }
                    if (!model.getDestination().trim().isEmpty() && !model.getDestination().contains("X")) {
                        insertValues.put(KEY_DESTINATION_NUM, model.getDestination());
                    }

                    if(!model.getComments().isEmpty()||!model.getBalance().trim().isEmpty()||(!model.getDestination().trim().isEmpty() && !model.getDestination().contains("X"))) {
                        //    Toast.makeText(mContext,"calling",Toast.LENGTH_LONG).show();
                        int i= db.update(DATABASE_TABLE_TRANSATION, insertValues, KEY_TRANSID + "='" + model.getTransatId() + "'", null);

                        //Toast.makeText(mContext,"insert result" +i,Toast.LENGTH_LONG).show();
                    }

                  //------------------------------
                } else {

                  insertValues.put(KEY_CASHIER_ID, model.getCashierId());
                  insertValues.put(KEY_CASHIER_NAME, model.getCashierName());
                  insertValues.put(KEY_CASHIER_NUMBER, model.getCashierNumber());

                    db.insert(DATABASE_TABLE_TRANSATION, null, insertValues);
                    AppPreference.setFlagFisrtOnlinePayment(mContext);
                    insertSurvey(model.getIsCredit(),model.getmSurvery(), date, model.getAge(), model.getGender(), model.getCity(), model.getTransatId(), model.getAmount());

                }
            }

            return true;

        } catch (Exception e) {

            Log.d("DataBase", "" + e.toString());

            return false;
        }

    }







    public static String getLastSuccsesfull_Phone_No() {

        String Mobile_no = null;


        try {


            Cursor cursor1 = db.rawQuery("SELECT COUNT(*) FROM " + DATABASE_TABLE_TRANSATION, null);


            Cursor cursor = db.rawQuery("select " + KEY_DESTINATION_NUM + " from " + DATABASE_TABLE_TRANSATION + " where " + COLUMN_PRIMARY_KEY_ID + " = " + cursor1.getCount(), null);

            if (cursor != null) {

                if (cursor.getCount() > 0) {

                    cursor.moveToFirst();


                    Mobile_no = cursor.getString(cursor.getColumnIndex(KEY_DESTINATION_NUM));


                }
                cursor.close();
            }

        } catch (Exception e) {

            e.printStackTrace();


        }
        return Mobile_no;
    }


    public DBHelper open() throws SQLException {
        if (db == null || !db.isOpen())
            db = dbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        if (db == null && db.isOpen())
            dbHelper.close();
    }

    public ArrayList<TransationModel> getTransationDetails(String type, String data) {
        ArrayList<TransationModel> list = new ArrayList<TransationModel>();
        Cursor cursor = db.rawQuery("select  * from " + DATABASE_TABLE_TRANSATION + " where " + type + " = '" + data + "'", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    list.add(new TransationModel(cursor));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return list;
    }

    public ArrayList<TransationModel> getTransationDetailsByDate(String type, String data) {
        ArrayList<TransationModel> list = new ArrayList<TransationModel>();
        Cursor cursor = db.rawQuery("select  * from " + DATABASE_TABLE_TRANSATION + " where " + type + " LIKE '" + data + "%'", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    list.add(new TransationModel(cursor));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return list;
    }


    public ArrayList<TransationModel> getTransationPendingStatusdetails() {

        try {

            ArrayList<TransationModel> list = new ArrayList<TransationModel>();
            Cursor cursor = db.rawQuery("select  * from " + DATABASE_TABLE_TRANSATION + " WHERE " + KEY_STATUS_PENDING + " =" + "?", new String[]{"false"});
            if (cursor != null) {
                if (cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    do {
                        list.add(new TransationModel(cursor));
                    } while (cursor.moveToNext());
                }
                cursor.close();
            }

            return list;

        } catch (Exception e) {
        }

        return null;

    }


    //for getting transaction details and received details cursor

    public Cursor getTransationDetailsCursor()
    {
        ArrayList<TransationModel> list = new ArrayList<TransationModel>();
        Cursor cursor = null;

        cursor = db.rawQuery("select  * from " + DATABASE_TABLE_TRANSATION + " ORDER BY " + KEY_DATE+ " DESC", null);

        if (cursor != null) {
            if (cursor.getCount() > 0) {
                return cursor;
            }

        }
        return cursor;
    }


    public Cursor getReceivedMoneyCursor()
    {
        ArrayList<TransationModel> list = new ArrayList<TransationModel>();
        Cursor cursor = null;

        cursor = db.rawQuery("select  * from " + DATABASE_TABLE_RECEIVED_MONEY + " ORDER BY " + KEY_DATE+ " DESC", null);

        if (cursor != null) {
            if (cursor.getCount() > 0) {
                return cursor;
            }

        }
        return cursor;
    }

    public Cursor getActivityDetailsCursor()
    {
        ArrayList<TransationModel> list = new ArrayList<TransationModel>();
        Cursor cursor = null;

        cursor = db.rawQuery("select  * from " + DATABASE_TABLE_ACTIVIY_lOG_CASHIER, null);

        if (cursor != null) {
            if (cursor.getCount() > 0) {
                return cursor;
            }

        }
        return cursor;
    }


    //--------------------for getting transaction details and received details cursor

    public ArrayList<TransationModel> getReceivedTransactionDetails(String type, String data) {

        ArrayList<TransationModel> list = new ArrayList<TransationModel>();
        Cursor cursor = db.rawQuery("select  * from " + DATABASE_TABLE_RECEIVED_MONEY + " where " + type + " = '" + data + "'", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    list.add(new TransationModel(cursor));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return list;
    }

    public ArrayList<TransationModel> getReceivedTransactionDetailsByDate(String type, String data) {

        ArrayList<TransationModel> list = new ArrayList<TransationModel>();
        Cursor cursor = db.rawQuery("select  * from " + DATABASE_TABLE_RECEIVED_MONEY + " where " + type + " like '" + data + "%'", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    list.add(new TransationModel(cursor));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return list;
    }

    public void updateColum(TransationModel model) {
        ContentValues insertValues = new ContentValues();
        insertValues.put(KEY_AGENT_NAME, model.getName());
        insertValues.put(KEY_DESCRIPTION, model.getDescripton());
        insertValues.put(KEY_DESTINATION_NUM, model.getDestination());
        insertValues.put(KEY_TRANSID, model.getTransatId());
        insertValues.put(KEY_NEW_AMOUNT, model.getNewamount());
        insertValues.put(KEY_OLD_AMOUNT, model.getOldAmount());
        insertValues.put(KEY_AMOUNT, model.getAmount());
        insertValues.put(KEY_DATE, model.getDate());
        insertValues.put(KEY_FEE, model.getFee());
        insertValues.put(KEY_TRANSTYPE, model.getIsCredit());
        insertValues.put(KEY_SERVICE_TAX, model.getServiceTax());
        insertValues.put(KEY_TRANACATION_TYPE, model.getTrnastionType());
        insertValues.put(KEY_KEY_COUNT, model.getCount());
        insertValues.put(KEY_COMMEENT, model.getComments());
        int record = db.update(DATABASE_TABLE_RECEIVED_MONEY, insertValues, KEY_TRANSID + "='" + model.getTransatId() + "'", null);
        System.out.println(record);
    }


    public void updateTransitionCoulumn(TransationModel model, String status) {

        ContentValues insertValues = new ContentValues();
        insertValues.put(KEY_AGENT_NAME, model.getName());
        insertValues.put(KEY_DESCRIPTION, model.getDescripton());
        insertValues.put(KEY_DESTINATION_NUM, model.getDestination());
        insertValues.put(KEY_TRANSID, model.getTransatId());
        insertValues.put(KEY_NEW_AMOUNT, model.getNewamount());
        insertValues.put(KEY_OLD_AMOUNT, model.getOldAmount());
        insertValues.put(KEY_AMOUNT, model.getAmount());
        insertValues.put(KEY_DATE, model.getDate());
        insertValues.put(KEY_FEE, model.getFee());
        insertValues.put(KEY_TRANSTYPE, model.getIsCredit());
        insertValues.put(KEY_SERVICE_TAX, model.getServiceTax());
        insertValues.put(KEY_TRANACATION_TYPE, model.getTrnastionType());
        insertValues.put(KEY_KEY_COUNT, model.getCount());
        insertValues.put(KEY_COMMEENT, model.getComments());
        insertValues.put(KEY_STATUS_PENDING, status);


        int record = db.update(DATABASE_TABLE_RECEIVED_MONEY, insertValues, KEY_TRANSID + "='" + model.getTransatId() + "'", null);
        System.out.println(record);

    }

    public ArrayList<TransationModel> getTransationDetails(String cashierId, Context context) {
        ArrayList<TransationModel> list = new ArrayList<TransationModel>();
        Cursor cursor = null;

        if (AppPreference.getMasterOrCashier(context).equalsIgnoreCase(CASHIER))
            cursor = db.rawQuery("select  distinct "+KEY_TRANSID+", * from "+ DATABASE_TABLE_TRANSATION + " where " + KEY_CASHIER_ID + "='" + cashierId + "'  ORDER BY " + KEY_TRANSCATION_DATE + " DESC", null);
        else
            cursor = db.rawQuery("select  distinct "+KEY_TRANSID+", * from " + DATABASE_TABLE_TRANSATION + " ORDER BY " + KEY_TRANSCATION_DATE+ " DESC", null);

        if (cursor != null) {
            if (cursor.getCount() > 0)
            {
                cursor.moveToFirst();
                do {

                    list.add(new TransationModel(cursor));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return list;
    }





    public ArrayList<TransationModel> getReceivedTransationDetails(String cashierId, Context context) {
        ArrayList<TransationModel> list = new ArrayList<TransationModel>();
        Cursor cursor = null;

        if (AppPreference.getMasterOrCashier(context).equalsIgnoreCase(CASHIER))
            cursor = db.rawQuery("select  distinct "+KEY_TRANSID+", * from " + DATABASE_TABLE_RECEIVED_MONEY + " where " + KEY_CASHIER_ID + "='" + cashierId + "'  ORDER BY " + KEY_TRANSCATION_DATE + " DESC", null);
        else
           cursor = db.rawQuery("select distinct "+KEY_TRANSID+", * from " + DATABASE_TABLE_RECEIVED_MONEY + " ORDER BY " + KEY_TRANSCATION_DATE + " DESC", null);


        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    list.add(new TransationModel(cursor));

                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return list;
    }


    public ArrayList<TransationModel> getReceivedTransationDetails() {
        ArrayList<TransationModel> list = new ArrayList<TransationModel>();

      Cursor cursor = db.rawQuery("select distinct "+KEY_TRANSID+", * from " + DATABASE_TABLE_RECEIVED_MONEY + " ORDER BY " + KEY_TRANSCATION_DATE + " DESC", null);
      if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    list.add(new TransationModel(cursor));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return list;
    }


    public int getUnreadtranscationCount() {
        int count = 0;
        Cursor cursor = db.rawQuery("select  * from " + DATABASE_TABLE_RECEIVED_MONEY + " where count='0'", null);
        if (cursor != null) {
            count = cursor.getCount();
        }
        cursor.close();
        return count;
    }

    public ArrayList<TransationModel> receivedUnreadTranscation() {
        ArrayList<TransationModel> list = new ArrayList<TransationModel>();
        Cursor cursor = db.rawQuery("select  * from " + DATABASE_TABLE_RECEIVED_MONEY + " where count='0'" + " ORDER BY " + COLUMN_PRIMARY_KEY_ID + " DESC", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    list.add(new TransationModel(cursor));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return list;
    }







    public static boolean insertRECEIVEDTransacation(TransationModel model) {
      try {
        ContentValues insertValues = new ContentValues();
        int hours = 0;
        String date;
        if (!isReceivedMoneyPResent(model.getTransatId()))
        {
          insertValues.put(KEY_CASHIER_ID,model.getCashierId());
          insertValues.put(KEY_CASHIER_NAME, model.getCashierName());
          insertValues.put(KEY_CASHIER_NUMBER, model.getCashierNumber());
          insertValues.put(KEY_AGENT_NAME, model.getName());
          insertValues.put(KEY_DESCRIPTION, model.getDescripton());
          insertValues.put(KEY_DESTINATION_NUM, model.getDestination());
          insertValues.put(KEY_TRANSID, model.getTransatId().replace(".", "").trim());
          insertValues.put(KEY_NEW_AMOUNT, model.getNewamount());
          insertValues.put(KEY_OLD_AMOUNT, model.getOldAmount());
          insertValues.put(KEY_AMOUNT, model.getAmount());

          insertValues.put(KEY_TRANSCATION_RECEIVED_KIKBACK, model.getReceivedKickback());
          if (!model.getDate().trim().equals("")) {
            insertValues.put(KEY_DATE, model.getDate());
            hours = Utils.getHourFromDateTime(model.getDate());
            date = model.getDate();
          } else {
            insertValues.put(KEY_DATE, model.getResponsects());
            hours = Utils.getHourFromDateTime(model.getResponsects());
            date = model.getResponsects();
          }

          insertValues.put(KEY_TRANSCATION_DATE, model.getmTranscationDate().getTime());
          insertValues.put(KEY_HOURS, "" + hours);
          insertValues.put(KEY_FEE, model.getFee());
          insertValues.put(KEY_TRANSTYPE, model.getIsCredit());
          insertValues.put(KEY_SERVICE_TAX, model.getServiceTax());
          insertValues.put(KEY_TRANACATION_TYPE, model.getTrnastionType());
          insertValues.put(KEY_TRANSCATION_SURVERY, model.getmSurvery());

          insertValues.put(KEY_TRANSCATION_WALLET_BALANCE, model.getmWalletbalance());
          if (!Utils.isEmpty(model.getDestinationPersonName())) {
            insertValues.put(KEY_NAME, model.getDestinationPersonName());
          } else {
            insertValues.put(KEY_NAME, "Unknown");
          }

          insertValues.put(KEY_IMAGE_PATH, model.getPhotoUrl());
          if (model.getCount().equals("")) {
            insertValues.put(KEY_KEY_COUNT, "0");
          } else {
            insertValues.put(KEY_KEY_COUNT, model.getCount());
          }
          if (model.getBalance().isEmpty())
            insertValues.put(KEY_NEW_AMOUNT, "");
          else
            insertValues.put(KEY_NEW_AMOUNT, model.getBalance());

          insertValues.put(KEY_COMMEENT, model.getComments());
          insertValues.put(KEY_LAT_LONG, model.getLatLong());
          insertValues.put(KEY_CITY_CODE, model.getCity());
          insertValues.put(GENDER, model.getGender());
          insertValues.put(AGE, model.getAge());
          insertValues.put(CELLTOWERID, model.getCellID());

          int record = db.update(DATABASE_TABLE_RECEIVED_MONEY, insertValues, KEY_TRANSID + "='" + model.getTransatId() + "'", null);
          if (record == 0) {

            db.insert(DATABASE_TABLE_RECEIVED_MONEY, null, insertValues);
          } else {
            //  db.delete(DATABASE_TABLE_RECEIVED_MONEY,  KEY_TRANSID + "='" + model.getTransatId() + "'", null);
            // db.insert(DATABASE_TABLE_RECEIVED_MONEY, null, insertValues);

          }


          try {
            insertSurvey(model.getIsCredit(), model.getmSurvery(), date, model.getAge(), model.getGender(), model.getCity(), model.getTransatId(), model.getAmount());
          } catch (Exception e) {
          }
          TRANSACTION_COUNT = TRANSACTION_COUNT + 1;
          return true;
        } else
        {

          if (!model.getComments().contains("")) {
            insertValues.put(KEY_COMMEENT, model.getComments());
          }
          if (!model.getBalance().trim().isEmpty()) {
            insertValues.put(KEY_NEW_AMOUNT, model.getBalance());
          }
          if (!model.getDestination().trim().isEmpty() && !model.getDestination().contains("X")) {
            insertValues.put(KEY_DESTINATION_NUM, model.getDestination());
          }

          if (!model.getmWalletbalance().isEmpty())

            insertValues.put(KEY_TRANSCATION_WALLET_BALANCE, model.getmWalletbalance());

          if (!model.getComments().isEmpty() || !model.getBalance().trim().isEmpty() || (!model.getDestination().trim().isEmpty() && !model.getDestination().contains("X"))) {
            //    Toast.makeText(mContext,"calling",Toast.LENGTH_LONG).show();
            int i = db.update(DATABASE_TABLE_RECEIVED_MONEY, insertValues, KEY_TRANSID + "='" + model.getTransatId() + "'", null);

            //Toast.makeText(mContext,"insert result" +i,Toast.LENGTH_LONG).show();
          }
          try {
            insertSurvey(model.getIsCredit(), model.getmSurvery(), model.getResponsects(), model.getAge(), model.getGender(), model.getCity(), model.getTransatId(), model.getAmount());
          } catch (Exception e) {
            e.printStackTrace();
          }
          return false;
        }

      } catch (Exception e) {
        //   Toast.makeText(mContext,e.toString(),Toast.LENGTH_LONG).show();
        e.printStackTrace();
        return false;
      }

    }

    public ConstactModel getSelectedContact(String contact) {
        if (contact.startsWith("0"))
            contact = contact.substring(2);
        ConstactModel list = new ConstactModel();
        Cursor cursor = db.rawQuery("select  * from " + DATABASE_TABLE_CONTACTS + " where " + KEY_MOBILE_NUMBER + " like '%" + contact + "%'", null);
        System.out.println("Cusrsor--" + cursor);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    list = new ConstactModel(cursor);
                    System.out.print(list);
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return list;
    }

    public ConstactModel getSelectedContact_check(String contact) {
        String Contact_c = contact.substring(1);
        ConstactModel list = new ConstactModel();
        Cursor cursor = db.rawQuery("select  * from " + DATABASE_TABLE_CONTACTS + " where " + KEY_MOBILE_NUMBER + " like '%" + Contact_c + "%'", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    list = new ConstactModel(cursor);
                    System.out.print(list);
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return list;
    }

    public static boolean insertContact(ConstactModel model) {
        try {

            ContentValues insertValues = new ContentValues();
            insertValues.put(KEY_NAME, model.getName());
            insertValues.put(KEY_MOBILE_NUMBER, model.getMobileNo());
            insertValues.put(KEY_EMAIL_ID, model.getEmail());
            insertValues.put(COLUMN_ID, model.getId());
            insertValues.put(COLUMN_PHOTO_URL, model.getPhotoUrl());
            insertValues.put(KEY_ISCONTACT_UPLOAED, model.getIsContactUpload());
            int record = db.update(DATABASE_TABLE_CONTACTS, insertValues, COLUMN_ID + "='" + model.getId() + "'", null);
            Log.d("Name id", String.valueOf(record));
            if (record == 0) {
                long in_record = db.insert(DATABASE_TABLE_CONTACTS, null, insertValues);
                Log.d("Values of I = ", "******************* " + in_record + " ***************");
                Log.d("Name id", String.valueOf(in_record));
                Log.d("Name id", model.getName() + " " + model.getId());

            } else {
                // db.delete(DATABASE_TABLE_CONTACTS, KEY_MOBILE_NUMBER + "='" + model.getMobileNo() + "'", null);
                //  db.insert(DATABASE_TABLE_CONTACTS, null, insertValues);
            }

            return true;
        } catch (Exception e) {
            return false;
        }

    }

  /*  public static boolean insertContact_syn_server(ConstactModel model) {
        try {
            {
                ContentValues insertValues = new ContentValues();
                insertValues.put(KEY_NAME, model.getName());
                insertValues.put(KEY_MOBILE_NUMBER, model.getMobileNo());
                insertValues.put(KEY_EMAIL_ID, model.getEmail());
                insertValues.put(COLUMN_ID, model.getId());
                insertValues.put(COLUMN_PHOTO_URL, model.getPhotoUrl());
                insertValues.put(KEY_ISCONTACT_UPLOAED, model.getIsContactUpload());
                int record = db.update(DATABASE_TABLE_CONTACTS, insertValues, KEY_MOBILE_NUMBER + "='" + model.getMobileNo() + "'", null);
                Log.d("Name id", String.valueOf(record));
                if (record == 0) {
                    long in_record = db.insert(DATABASE_TABLE_CONTACTS, null, insertValues);
                    Log.d("Values of I = ", "******************* " + in_record + " ***************");
                    Log.d("Name id", String.valueOf(in_record));
                    Log.d("Name id", model.getName() + " " + model.getId());

                } else {
                    //  db.delete(DATABASE_TABLE_CONTACTS, KEY_MOBILE_NUMBER + "='" + model.getMobileNo() + "'", null);
                    //  db.insert(DATABASE_TABLE_CONTACTS, null, insertValues);
                }
            }
            return true;
        } catch (Exception e) {
            return false;
        }

    }
*/
    public String getLastTransactionNumberFromTransactionTable() {

        String Last_number = "";
        Cursor cursor = db.rawQuery("SELECT " + KEY_DESTINATION_NUM + " FROM " + DATABASE_TABLE_TRANSATION +
                " ORDER BY " + COLUMN_PRIMARY_KEY_ID + " DESC LIMIT 1", null);


        if (cursor != null) {

            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                Last_number = cursor.getString(0);
            }
            cursor.close();
        }
        return Last_number;
    }


//    public static boolean insertTicketImage(String path) {
//        try {
//            {
//                ContentValues insertValues = new ContentValues();
//                insertValues.put(KEY_IMAGE_PATH, path);
//                db.insert(DATABASE_TABLE_TICKET_IMAGES, null, insertValues);
//            }
//            return true;
//        } catch (Exception e) {
//            return false;
//        }
//
//    }

//    public ArrayList<String> getAllTicket() {
//        ArrayList<String> list = new ArrayList<String>();
//        Cursor cursor = db.rawQuery("select  * from " + DATABASE_TABLE_TICKET_IMAGES, null);
//        if (cursor != null) {
//            if (cursor.getCount() > 0) {
//                cursor.moveToFirst();
//                do {
//                    list.add(cursor.getString(cursor.getColumnIndex(KEY_IMAGE_PATH)));
//                } while (cursor.moveToNext());
//            }
//            cursor.close();
//        }
//        return list;
//    }


//    public boolean insertSuccessfull_Payment(String destination, String mobile_no, String amount, String transtype, String transtime , String balance , String transtimeID) {
//
//
//        try {
//
//            dbHelper = new DatabaseHelper(mContext);
//
//            SQLiteDatabase sqLiteOpenHelper = dbHelper.getWritableDatabase();
//
//            ContentValues insertValues = new ContentValues();
//
//            insertValues.put(COLUMN_TOPUP_DESTINATION_NO, destination);
//            insertValues.put(COLUMN_TOPUP_MOBILE_NO, mobile_no);
//            insertValues.put(COLUMN_TOPUP_AMOUNT, amount);
//            insertValues.put(COLUMN_TOPUP_TRANSTYPE, transtype);
//            insertValues.put(COLUMN_TOPUP_TRANSTIME, transtime);
//            insertValues.put(COLUMN_TOPUP_BALANCE, balance);
//            insertValues.put(COLUMN_TOPUP_TRANSID, transtimeID);
//
//            //  int record = db.update(DATABASE_TABLE_AGENT_TOWNSHIP, insertValues, KEY_AGENT_TOWNSHIP_ID + "='" + model.getTownId() + "'", null);
//            // if (record == 0) {
//            long val = sqLiteOpenHelper.insert(DATABASE_TABLE_TOPUP_TRANSACTION, null, insertValues);
//
//            System.out.print(val);
//            // } else {
//            //      db.delete(DATABASE_TABLE_AGENT_TOWNSHIP, KEY_AGENT_TOWNSHIP_ID + "='" + model.getTownId() + "'", null);
//            //     db.insert(DATABASE_TABLE_AGENT_TOWNSHIP, null, insertValues);
//            // }
//
//            return true;
//
//        } catch (Exception e) {
//
//            return false;
//
//        }
//
//    }


    ///// INSERTING TRAIN STATION NAME


    //////////////////// Train Station price calculation table Insert Functionn//////


    //Check based on mobile number and amount..
  /*  public long check_kickback_available(String m_no, String amount,String transid) {
        long cnt=0;

        //String selectQuery = "SELECT * FROM mobile_table  ORDER BY id DESC LIMIT 1";
        String selectQuery = "SELECT * FROM "+VIEW_KICKBACK+" where "+KICKBACK_MINRANGE+"='"+m_no+"' AND "+KICKBACK_MAXRANGE+"='"+amount+"' AND "+KIC+"='"+transid+"'";
        Log.d("-----selectQuery--", selectQuery);
        Cursor cursor = db.rawQuery(selectQuery, null);
        cnt = cursor.getCount();
        cursor.close();
        return cnt;
    }
    */
    public void kb_delete() {
        db.delete(VIEW_KICKBACK, null, null);
    }

    public void dummy_merchant_list_delete() {
        db.delete(DUMMYMERCHANTLIST_TABLE, null, null);
    }

    public void insert_kickback_data(String min, String max, String dis, String peramt, String amt, String st) {
        ContentValues values = new ContentValues();
        values.put(KICKBACK_MINRANGE, min);
        values.put(KICKBACK_MAXRANGE, max);
        values.put(KICKBACK_DISBTYPE, dis);
        values.put(KICKBACK_PERAMT, peramt);
        values.put(KICKBACK_AMT, amt);
        values.put(KICKBACK_Status, st);
        long i = db.insert(VIEW_KICKBACK,
                null,
                values);
        Log.d("Values of I = ", "******************* " + i + " ***************");
        db.close();
    }



    public void insert_Dummy_MerchantList_data(String nm, String mno, String country, String state, String city, String status) {
        ContentValues values = new ContentValues();
        values.put(DUMMYMERCHANTLIST_NAME, nm);
        values.put(DUMMYMERCHANTLIST_MOBILENO, mno);
        values.put(DUMMYMERCHANTLIST_COUNTRY, country);
        values.put(DUMMYMERCHANTLIST_STATE, state);
        values.put(DUMMYMERCHANTLIST_CITY, city);
        values.put(DUMMYMERCHANTLIST_STATUS, status);
        long i = db.insert(DUMMYMERCHANTLIST_TABLE, // table
                null,
                values);
        Log.d("Values of I = ", "******************* " + i + " ***************");
        db.close();
    }

    /*   String Agent = "CREATE TABLE "+Agent_TABLE+" ( " + COLUMN_PRIMARY_KEY_ID +
               " INTEGER PRIMARY KEY AUTOINCREMENT, "+Agent_agentcode + " TEXT, " +Agent_source + " TEXT, "+Agent_agenttype+
               " TEXT, "+Agent_userpack+
               " TEXT, "+Agent_agentname+" TEXT, "+Agent_email+" TEXT, "+ Agent_dob +" TEXT, " +Agent_idprooftype +" TEXT, "+ Agent_dmsrefid +" TEXT, "+ Agent_occupation + " TEXT,"+ Agent_customerstatus + " TEXT, "+ Agent_language + " TEXT, "+ Agent_resultdescription +" TEXT, "+ Agent_transid + " TEXT )";

   */


    public boolean insertDistributor(String name, String phone_Number, String Telecom) {
        boolean exist = false;
        try {
            dbHelper = new DatabaseHelper(mContext);
            SQLiteDatabase sqLiteOpenHelper = dbHelper.getWritableDatabase();
            ContentValues insertValues = new ContentValues();
            insertValues.put(COLUMN_DISTRIBUTOR_NAME, name);
            insertValues.put(COLUMN_DISTRIBUTOR_NUMBER, phone_Number);
            insertValues.put(COLUMN_DISTRIBUTOR_TELECOM, Telecom);
            //  int record = db.update(DATABASE_TABLE_AGENT_TOWNSHIP, insertValues, KEY_AGENT_TOWNSHIP_ID + "='" + model.getTownId() + "'", null);
            // if (record == 0) {
            try {


                Cursor cursor = db.rawQuery("select " + COLUMN_DISTRIBUTOR_NAME + " , " + COLUMN_DISTRIBUTOR_NUMBER + " , " + COLUMN_DISTRIBUTOR_TELECOM + " " + "from " + DATABASE_TABLE_DISTRIBUTOR + " " + "where" + " " + COLUMN_DISTRIBUTOR_TELECOM + " = " + "'" + Telecom + "'", null);
                if (cursor != null) {
                    if (cursor.getCount() > 0) {

                        cursor.moveToFirst();

                        do {

                            if (cursor.getString(cursor.getColumnIndex(COLUMN_DISTRIBUTOR_NUMBER)).trim().equals(phone_Number)) {
                                exist = true;
                                break;
                            }


                        } while (cursor.moveToNext());
                    }
                    cursor.close();
                } else {


                }

            } catch (Exception e) {

                e.printStackTrace();


            }

            long val;


            if (exist == false) {


                val = sqLiteOpenHelper.insert(DATABASE_TABLE_DISTRIBUTOR, null, insertValues);

            } else {

                val = -1;

            }
            System.out.print(val);

            return val != -1;


        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    public boolean deleteDistributor(String name, String phone_Number, String Telecom) {


        try {

            dbHelper = new DatabaseHelper(mContext);
            SQLiteDatabase sqLiteOpenHelper = dbHelper.getWritableDatabase();

            ContentValues insertValues = new ContentValues();
            insertValues.put(COLUMN_DISTRIBUTOR_NAME, name);
            insertValues.put(COLUMN_DISTRIBUTOR_NUMBER, phone_Number);
            insertValues.put(COLUMN_DISTRIBUTOR_TELECOM, Telecom);
            long val = sqLiteOpenHelper.delete(DATABASE_TABLE_DISTRIBUTOR, COLUMN_DISTRIBUTOR_NAME + "='" + name + "'" + " and " + COLUMN_DISTRIBUTOR_NUMBER + "='" + phone_Number + "'" + " and " + COLUMN_DISTRIBUTOR_TELECOM + "='" + Telecom + "'", null);
            System.out.print(val);
            return val != -1;
        } catch (Exception e) {
            return false;
        }
    }

    public ArrayList<HashMap<String, String>> getDistributorContacts(String Telecom) {


        ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();


        try {


            Cursor cursor = db.rawQuery("select " + COLUMN_DISTRIBUTOR_NAME + " , " + COLUMN_DISTRIBUTOR_NUMBER + " , " + COLUMN_DISTRIBUTOR_TELECOM + " " + "from " + DATABASE_TABLE_DISTRIBUTOR + " " + "where" + " " + COLUMN_DISTRIBUTOR_TELECOM + " = " + "'" + Telecom + "'", null);

            if (cursor != null) {

                if (cursor.getCount() > 0) {

                    cursor.moveToFirst();

                    do {

                        HashMap<String, String> temp = new HashMap<String, String>();
                        temp.put(COLUMN_DISTRIBUTOR_NAME, cursor.getString(cursor.getColumnIndex(COLUMN_DISTRIBUTOR_NAME)));
                        temp.put(COLUMN_DISTRIBUTOR_NUMBER, cursor.getString(cursor.getColumnIndex(COLUMN_DISTRIBUTOR_NUMBER)));
                        temp.put(COLUMN_DISTRIBUTOR_TELECOM, cursor.getString(cursor.getColumnIndex(COLUMN_DISTRIBUTOR_TELECOM)));


                        list.add(temp);

                    } while (cursor.moveToNext());
                }
                cursor.close();
            }

        } catch (Exception e) {

            e.printStackTrace();


        }
        return list;
    }


    public ArrayList<String> get_sourceDestination(String source_st) {


        String selectQuery = "SELECT " + TRAIN_StationNumber + " , " + TRAIN_Is_Circular + " FROM " + TRAIN_STATION_TABLE_ALL + " where " + TRAIN_StationName + " = " + source_st + "";
        Log.d("-----selectQuery--", selectQuery);

        ArrayList<String> temp = new ArrayList<String>();
        try {
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor != null) {
                if (cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    do {
                        //  list.add(cursor.getString(0));

                        temp.add(cursor.getString(0));
                        temp.add(cursor.getString(1));

                    } while (cursor.moveToNext());
                }
                cursor.close();
            }
        } catch (Exception e) {
            System.out.print("--" + e);
        }

        return temp;
    }


    public ArrayList<String> getNAME_OF_DIVISIONLISTNAME_BURMESE(String division) {
        ArrayList<String> list = new ArrayList<String>();

        Cursor cursor = db.rawQuery("select  aCityname_Burmese from " + DATABASE_TABLE_AGENT_CITY + " where " + KEY_R_IS_STATE_DIVISION + " ='" + division + "'", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {

                    list.add(cursor.getString(cursor.getColumnIndex(KEY_AGENT_CITY_NAME_BURMESE)));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return list;
    }

    public ArrayList<CityList_Model> getNAME_OF_DIVISIONLIST(String division) {
        ArrayList<CityList_Model> list = new ArrayList<CityList_Model>();
        Cursor cursor = db.rawQuery("select  * from " + DATABASE_TABLE_AGENT_CITY + " where " + KEY_R_IS_STATE_DIVISION + " ='" + division + "'", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    list.add(new CityList_Model(cursor));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }


        return list;
    }


    public ArrayList<String> getNAME_OF_DIVISIONLISTNAME(String division) {
        ArrayList<String> list = new ArrayList<String>();
        Cursor cursor = db.rawQuery("select  aCityname from " + DATABASE_TABLE_AGENT_CITY + " where " + KEY_R_IS_STATE_DIVISION + " ='" + division + "'", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {

                    list.add(cursor.getString(cursor.getColumnIndex(KEY_AGENT_CITY_NAME)));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return list;
    }

    public ArrayList<String> getSELECTED_TOWNSHIP_NAME_BURMESE(String id) {
        ArrayList<String> list = new ArrayList<String>();
        Cursor cursor = db.rawQuery("select  aTownshipName_Burmese from " + DATABASE_TABLE_AGENT_TOWNSHIP + " where " + KEY_AGENT_CITY_ID + "='" + id + "'", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {


                    list.add(cursor.getString(cursor.getColumnIndex(KEY_AGENT_TOWNSHIP_NAME_BURMESE)));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return list;
    }

    public ArrayList<TownShip_Model> getTownList(String id) {
        ArrayList<TownShip_Model> list = new ArrayList<TownShip_Model>();
        Cursor cursor = db.rawQuery("select  * from " + DATABASE_TABLE_AGENT_TOWNSHIP + " where " + KEY_AGENT_CITY_ID + "='" + id + "'", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {


                    list.add(new TownShip_Model(cursor));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return list;
    }

    public static boolean insertTownShipCity(TownShip_Model model) {
        try {
            {
                ContentValues insertValues = new ContentValues();
                insertValues.put(KEY_AGENT_CITY_ID, model.getId());
                insertValues.put(KEY_AGENT_CITY_NAME, model.getName());
                insertValues.put(KEY_AGENT_TOWNSHIP_ID, model.getTownId());
                insertValues.put(KEY_AGENT_TOWNSHIP_NAME, model.getToenName());
                insertValues.put(KEY_AGENT_TOWNSHIP_NAME_BURMESE, model.getCityBName());
                insertValues.put(KEY_AGENT_IS_DEFAULT_CITY, model.getIsDefaultCity());

                insertValues.put(KEY_AGENT_IS_DEFAULT_CITY_NAME, model.getDefaultCityName());
                insertValues.put(KEY_AGENT_IS_DEFAULT_CITY_NAME_BURMESE, model.getDefaultCityBName());
                insertValues.put(KEY_AGENT_IS_DEFAULT_CITY_NAME_BURMESE, model.getDefaultCityBName());
                insertValues.put(KEY_AGENT_IS_DEFAULT_CITY_NAME_D, model.getBCityName());
                insertValues.put(KEY_AGENT_IS_DEFAULT_CITY_NAME_D_BURMESE, model.getBCityBName());

                int record = db.update(DATABASE_TABLE_AGENT_TOWNSHIP, insertValues, KEY_AGENT_TOWNSHIP_ID + "='" + model.getTownId() + "'", null);
                if (record == 0) {
                    long val = db.insert(DATABASE_TABLE_AGENT_TOWNSHIP, null, insertValues);
                    System.out.print(val);
                } else {
                    db.delete(DATABASE_TABLE_AGENT_TOWNSHIP, KEY_AGENT_TOWNSHIP_ID + "='" + model.getTownId() + "'", null);
                    db.insert(DATABASE_TABLE_AGENT_TOWNSHIP, null, insertValues);
                }
            }
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    public ArrayList<String> getAllTransationPhoneNo() {
        ArrayList<String> list = new ArrayList<>();
        Cursor cursor = db.rawQuery("select DISTINCT " + KEY_DESTINATION_NUM + " from " + DATABASE_TABLE_TRANSATION + " ORDER BY " + KEY_DATE + " DESC", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    list.add(cursor.getString(0));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return list;
    }

    public static boolean insertDeviceInfor(String deviceId, String SimId, String mobile, String csutomerId) {
        try {
            {
                ContentValues insertValues = new ContentValues();
                insertValues.put(COLUMN_CUS_ID, csutomerId);
                insertValues.put(COLUMN_DEVICE_ID, deviceId);
                insertValues.put(COLUMN_MOBILE_NO, mobile);
                insertValues.put(COLUMN_SIM_ID, SimId);
                int record = db.update(DATABASE_TABLE_DEVICE_INFORMATION, insertValues, COLUMN_SIM_ID + "='" + SimId + "'", null);
                if (record == 0) {
                    db.insert(DATABASE_TABLE_DEVICE_INFORMATION, null, insertValues);
                } else {
                    return false;
                }
            }
            return true;
        } catch (Exception e) {

            return false;
        }

    }


    public String getMobileNumber() {
        String simid = "";
        Cursor cursor = db.rawQuery("select  mobile_no from " + DATABASE_TABLE_DEVICE_INFORMATION, null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    simid = cursor.getString(cursor.getColumnIndex("mobile_no"));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return simid;
    }
//-------------------get ActivityLogArraylist
    public ArrayList<CashierActivityLogModel> Cashier_Activity(String id)
    {
        ArrayList<CashierActivityLogModel> al_cashierActivity=null;
        System.out.println("ID Values :"+id);
        String query;
        String id_master = "Master";
        if(id_master.equals(id))
        {
            System.out.println("IF Master Login " +id);
            query = "select * from Activity_Log_Cashier";
        }
        else
        {
            System.out.println("Else Cashier Login " +id);
            query = "select * from Activity_Log_Cashier where cashier_id = " + id;
        }
        Log.i("Cashier_Activity_Query", query);
        Cursor c = db.rawQuery(query, null);

        if (c == null)
        {
            return al_cashierActivity;
        }
        else
        {
          if(c.getCount()>0)
            {
                c.moveToFirst();
                al_cashierActivity=new ArrayList<CashierActivityLogModel>();
                do{
                   String sno=c.getString(c.getColumnIndex(COLUMN_PRIMARY_KEY_ID));
                   String cashier_Id=c.getString(c.getColumnIndex(KEY_CASHIER_ID));
                   String cashier_Name=c.getString(c.getColumnIndex(KEY_CASHIER_NAME));
                   String cashier_No=c.getString(c.getColumnIndex(KEY_CASHIER_NUMBER));
                   String logInTime=c.getString(c.getColumnIndex(KEY_CASHIER_LOGIN_TIME));
                   String logOutTime=c.getString(c.getColumnIndex(KEY_CASHIER_LOGOUT_TIME));
                   String logout_reason=c.getString(c.getColumnIndex(KEY_CASHIER_LOGOUT_REASON));
                   String status=c.getString(c.getColumnIndex(KEY_STATUS));

                    CashierActivityLogModel model=new CashierActivityLogModel(sno,cashier_Id,cashier_Name,cashier_No,logInTime,logOutTime,logout_reason,status);
                    al_cashierActivity.add(model);
                }
                while(c.moveToNext());
            }
        }

        return al_cashierActivity;
    }


/*    public ArrayList<OTPCashierModel> OTPLogDetails(String id,String statusvalue)
    {
        ArrayList<OTPCashierModel> al_cashierActivity=null;
        System.out.println("ID Values :"+id);
        String query;
        String id_master = "Master";
        String Status = statusvalue ;
        String KEY_SUCCESS = "Success" ;
        String KEY_Failure = "Failure" ;
        if(id_master.equals(id))
        {
            System.out.println("IF Master Login " +id);
            if(Status.equals("Success"))
            {
                String where = OTP_LOG_STATUS + " = "+ "'"+ KEY_SUCCESS +"'" ;
                query = "SELECT * FROM " + DATABASE_TABLE_OTP_lOG_DETAILS_FOR_QR_GENERATE + " where " + where  + "  ORDER BY " + OTP_LOG_TIME + " DESC";
            }
            else
            {
                String where = OTP_LOG_STATUS + " = "+ "'"+ KEY_Failure +"'" ;
                query = "SELECT * FROM " + DATABASE_TABLE_OTP_lOG_DETAILS_FOR_QR_GENERATE + " where " + where + "  ORDER BY " + OTP_LOG_TIME + " DESC";
            }
        }
        else
        {
            System.out.println("Else Cashier Login " +id);
            if(Status.equals("Success"))
            {
                String where = OTP_LOG_STATUS + " = "+ "'"+ KEY_SUCCESS +"'" + " AND " + OTP_LOG_CASHIER_ID + " = " + id ;
                query = "SELECT * FROM " + DATABASE_TABLE_OTP_lOG_DETAILS_FOR_QR_GENERATE + " where " + where + "  ORDER BY " + OTP_LOG_TIME + " DESC";
            }
            else
            {
                String where = OTP_LOG_STATUS + " = "+ "'"+ KEY_Failure +"'" + " AND " + OTP_LOG_CASHIER_ID + " = " + id ;
                query = "SELECT * FROM " + DATABASE_TABLE_OTP_lOG_DETAILS_FOR_QR_GENERATE + " where " + where + "  ORDER BY " + OTP_LOG_TIME + " DESC";
            }
        }
        Log.i("Cashier_Activity_Query", query);
        Cursor c = db.rawQuery(query, null);

        if (c == null)
        {
            return al_cashierActivity;
        }
        else
        {
            if(c.getCount()>0)
            {
                c.moveToFirst();
                al_cashierActivity=new ArrayList<OTPCashierModel>();
                do{
                    String time=c.getString(c.getColumnIndex(OTP_LOG_TIME));
                    String cashier_Name=c.getString(c.getColumnIndex(OTP_LOG_CASHIER_NAME));
                    String cashier_No=c.getString(c.getColumnIndex(OTP_LOG_CASHIER_NO));
                    String cashcollectorname=c.getString(c.getColumnIndex(OTP_LOG_CASHCOLLECTOR_NAME));
                    String cashcollectorno=c.getString(c.getColumnIndex(OTP_LOG_CASHCOLLECTOR_NO));
                    String amount=c.getString(c.getColumnIndex(OTP_LOG_AMT));
                    String status=c.getString(c.getColumnIndex(OTP_LOG_STATUS));
                    String cashierid=c.getString(c.getColumnIndex(OTP_LOG_CASHIER_ID));
                    String transid=c.getString(c.getColumnIndex(OTP_LOG_ID));
                    String textvalue=c.getString(c.getColumnIndex(OTP_LOG_TextValue));
                    String templatevalue=c.getString(c.getColumnIndex(OTP_LOG_TemplateValue));

                    OTPCashierModel model=new OTPCashierModel(time,cashier_Name,cashier_No,cashcollectorname,cashcollectorno,amount,status,cashierid,transid,textvalue,templatevalue);
                    al_cashierActivity.add(model);
                }
                while(c.moveToNext());
            }
        }

        return al_cashierActivity;
    }*/


    //*****************-------------------get ActivityLogArraylist
    public ArrayList<TransationModel> getTransferTransationDetails() {
        ArrayList<TransationModel> list = new ArrayList<TransationModel>();
        String type = "Dr";
        Cursor cursor = db.rawQuery("select  * from " + DATABASE_TABLE_TRANSATION + " where " + KEY_TRANSTYPE + "='" + type + "'" + " ORDER BY " + KEY_DATE + " DESC", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {

                    list.add(new TransationModel(cursor));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return list;
    }

    public ArrayList<CustomerSurveyModel>getAllSurvey(){
        ArrayList<CustomerSurveyModel> list = new ArrayList<>();
        String table="Cr";
        Cursor cursor = db.rawQuery("select * from " + SURVEY_DETAILS +" where "+SURVEY_TRANS_TYPE+" = '"+table+"' AND "+SURVEY_TOTAL+" >= "+0+ " ORDER BY " + KEY_TRANSCATION_DATE + " DESC", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    list.add(new CustomerSurveyModel(cursor));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return list;
    }

    public ArrayList<CashierModel> getCashierDetails() {
        ArrayList<CashierModel> al_cashier = null;
        Cursor cursor = db.rawQuery("select  * from " + DATABASE_NAME_CASHIER+" where "+KEY_CASHIER_STATUS+" ='"+CASHIER_ACTIVE+"'", null);
        if (cursor == null) {
            return al_cashier;
        } else {
            if (cursor.getCount() > 0) {
                al_cashier = new ArrayList<CashierModel>();
                cursor.moveToFirst();
                do {
                    String id = cursor.getString(cursor.getColumnIndex(COLUMN_PRIMARY_KEY_ID));
                    String cashiername = cursor.getString(cursor.getColumnIndex(KEY_CASHIER_NAME));
                    String cashiernumber = cursor.getString(cursor.getColumnIndex(KEY_CASHIER_NUMBER));
                    String password = cursor.getString(cursor.getColumnIndex(KEY_CASHIER_PASSWORD));
                    String confirmpassword = cursor.getString(cursor.getColumnIndex(KEY_CASHIER_CONFIRM_PASSWORD));
                    String color = cursor.getString(cursor.getColumnIndex(KEY_CASHIER_COLOR));
                   String jobType=cursor.getString(cursor.getColumnIndex(KEY_CASHIER_JOBTYPE));
                    int colorCode = Integer.parseInt(color);
                    String createdDate = cursor.getString(cursor.getColumnIndex(KEY_CASHIER_CREATED_DATE));
                   byte[] b = cursor.getBlob(7);
                    Bitmap bmp=Utility.getPhoto(b);
                  String status=cursor.getString(cursor.getColumnIndex(KEY_CASHIER_STATUS));
                    CashierModel model = new CashierModel(id, cashiername, cashiernumber, password, confirmpassword, color, createdDate,bmp,status,jobType);
                    al_cashier.add(model);
                } while (cursor.moveToNext());
                cursor.close();
            }
        }
        return al_cashier;

    }

    public ArrayList<Profile> getProfileData() {
        ArrayList<Profile> modelList = new ArrayList<Profile>();
        String selectQuery = "";
        selectQuery = "SELECT * FROM " + USERPROFILE;
        Cursor cursor = db.rawQuery(selectQuery, null);
        Log.e("Cursor Count",cursor.getCount()+"");
        if (cursor.moveToFirst()) {
            do {
              try {
                Profile mobile = new Profile();
                mobile.setMobileNumber(cursor.getString(cursor.getColumnIndex(MOBILENUMBER)));
                mobile.setName(cursor.getString(cursor.getColumnIndex(NAME)));
                mobile.setGcmID(cursor.getString(cursor.getColumnIndex(GCMID)));
                mobile.setEncrypted(cursor.getString(cursor.getColumnIndex(ENCRYPTED)));
                mobile.setSimID(cursor.getString(cursor.getColumnIndex(SIMID)));
                mobile.setMSID(cursor.getString(cursor.getColumnIndex(MSID)));
                mobile.setIMEI(cursor.getString(cursor.getColumnIndex(IMEI)));
                mobile.setlType(cursor.getString(cursor.getColumnIndex(LTYPE)));
                mobile.setAppID(cursor.getString(cursor.getColumnIndex(APPID)));
                mobile.setRecommended(cursor.getString(cursor.getColumnIndex(RECOMMENDED)));
                mobile.setState(cursor.getString(cursor.getColumnIndex(STATE)));
                mobile.setTownship(cursor.getString(cursor.getColumnIndex(TOWNSHIP)));
                mobile.setFather(cursor.getString(cursor.getColumnIndex(FATHER)));
                mobile.setGender(Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex(GENDER))));
                mobile.setDateOfBirth(cursor.getString(cursor.getColumnIndex(DATEOFBIRTH)));
                mobile.setNRC(cursor.getString(cursor.getColumnIndex(NRC)));
                mobile.setIDType(cursor.getString(cursor.getColumnIndex(IDTYPE)));
                mobile.setPhone(cursor.getString(cursor.getColumnIndex(PHONE)));
                mobile.setBusinessType(cursor.getString(cursor.getColumnIndex(BUSINESSTYPE)));
                mobile.setBusinessCategory(cursor.getString(cursor.getColumnIndex(BUSINESSCATEGORY)));
                mobile.setCar(cursor.getString(cursor.getColumnIndex(CAR)));
                mobile.setCarType(Integer.parseInt(cursor.getString(cursor.getColumnIndex(CARTYPE))));
                mobile.setLatitude(cursor.getString(cursor.getColumnIndex(LATITUDE)));
                mobile.setLongitude(cursor.getString(cursor.getColumnIndex(LONGITUDE)));
                mobile.setAddress1(cursor.getString(cursor.getColumnIndex(ADDRESS1)));
                mobile.setAddress2(cursor.getString(cursor.getColumnIndex(ADDRESS2)));
                  try {
                      mobile.setFBEmailId(cursor.getString(cursor.getColumnIndex(FBEMAILID)));
                  }catch (Exception e){}
                if(cursor.getString(cursor.getColumnIndex(ACCOUNTTYPE))!=null) {
                  if(!cursor.getString(cursor.getColumnIndex(ACCOUNTTYPE)).trim().equals("")) {
                    mobile.setAccountType(Integer.parseInt(cursor.getString(cursor.getColumnIndex(ACCOUNTTYPE))));
                  }
                }else
                {
                  mobile.setAccountType(0);
                }
                mobile.setOSType(Integer.parseInt(cursor.getString(cursor.getColumnIndex(OSTYPE))));
                mobile.setProfilePic(cursor.getString(cursor.getColumnIndex(PROFILEPIC)));
                mobile.setsignature(cursor.getString(cursor.getColumnIndex(SIGNATURE)));
                mobile.setPassword(cursor.getString(cursor.getColumnIndex(PASSWORD)));
                mobile.setEmailID(cursor.getString(cursor.getColumnIndex(EMAILID)));
                mobile.setAddressType(cursor.getString(cursor.getColumnIndex(ADDRESSTYPE)));
                mobile.setCellTowerID(cursor.getString(cursor.getColumnIndex(CELLTOWERID)));
                mobile.setBusinessName(cursor.getString(cursor.getColumnIndex(BUSINESSNAME)));
                mobile.setKickback(Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex(KICKBACK_COLUMN))));
                mobile.setCodeAlternate(cursor.getString(cursor.getColumnIndex(CODEALTERNATE)));
                mobile.setCountry(cursor.getString(cursor.getColumnIndex(COUNTRY)));
                mobile.setParentAccount(cursor.getString(cursor.getColumnIndex(PARENTACCOUNT)));
                mobile.setCodeRecommended(cursor.getString(cursor.getColumnIndex(CODERECOMMENDED)));
                mobile.setPaymentGateway(Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex(PAYMENTGATEWAY))));
                mobile.setLoyalty(Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex(LOYALTY))));
                mobile.setSecureToken(cursor.getString(cursor.getColumnIndex(SECURETOKEN)));
                  //mobile.setAccountNumber(cursor.getString(44));
                //mobile.setBankName(cursor.getString(45));
                modelList.add(mobile);
              }catch (Exception e)
              {

                e.printStackTrace();
              }
            } while (cursor.moveToNext());
        }
        return modelList;
    }

    public void deleteProfile()
    {
        db.execSQL("delete from " + USERPROFILE);
    }



    public boolean getContacts(String number) {

        boolean exist = false;
        //  ArrayList<String> list = new ArrayList<String>();
        Cursor cursor = db.rawQuery("select  * from " + DATABASE_TABLE_CONTACTS, null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    if (number.equals(cursor.getString(cursor.getColumnIndex(KEY_MOBILE_NUMBER)))) {
                        exist = true;
                        break;
                    }
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return exist;
    }

    public String[] getDivisionORStateNameFromID(String id) {

        Cursor cursor = db.rawQuery("select " + KEY_AGENT_CITY_NAME +","+ KEY_R_IS_STATE_DIVISION + ","+ KEY_AGENT_CITY_NAME_BURMESE + " from " + DATABASE_TABLE_AGENT_CITY + " where " + KEY_AGENT_CITY_ID + "='" + id + "'", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    return new String[]{cursor.getString(0), cursor.getString(1), cursor.getString(2)};
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return new String[]{};
    }

    public String[] getTownshipNameFromID(String id) {

        Cursor cursor = db.rawQuery("select * from " + DATABASE_TABLE_AGENT_TOWNSHIP + " where " + KEY_AGENT_TOWNSHIP_ID + "='" + id + "'", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    return new String[]{cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getString(7), cursor.getString(8), cursor.getString(9), cursor.getString(10)};
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return new String[]{};
    }

    public boolean insertProfile(Profile mobile) {
        try {
          int mCategoryPos = -1;
          int mSubCategoryPos = -1;
          if (mobile.getBusinessType().length() == 0) {
            mSubCategoryPos = -1;
          } else {
            mSubCategoryPos = Integer.parseInt(mobile.getBusinessType());
          }
          if (mobile.getBusinessCategory().length() == 0) {
            mCategoryPos = -1;
          } else {
            mCategoryPos = Integer.parseInt(mobile.getBusinessCategory());
          }
          if (mCategoryPos < 10 && mCategoryPos >= 0) {
            AppPreference.setBusinessMainCategory(mContext, "0" + String.valueOf(mCategoryPos));
          } else {
            AppPreference.setBusinessMainCategory(mContext, String.valueOf(mCategoryPos));
          }
          if (mSubCategoryPos < 10 && mSubCategoryPos >= 0) {
            AppPreference.setBusinessSubCategory(mContext, "0" + String.valueOf(mSubCategoryPos));
          } else {
            AppPreference.setBusinessSubCategory(mContext, String.valueOf(mSubCategoryPos));
          }

            AppPreference.setBusinessName(mContext, mobile.getBusinessName());
            //AppPreference.setUserName(mContext, mobile.getName());
            ContentValues insertValues = new ContentValues();
            insertValues.put(MOBILENUMBER,mobile.getMobileNumber());
            insertValues.put(NAME, mobile.getName());
            insertValues.put(GCMID,mobile.getGcmID());
            insertValues.put(FBEMAILID,mobile.getFBEmailId());
            insertValues.put(ENCRYPTED,mobile.getEncrypted());
            insertValues.put(SIMID,mobile.getSimID());
            insertValues.put(MSID,mobile.getMSID());
            insertValues.put(IMEI,mobile.getIMEI());
            insertValues.put(LTYPE,mobile.getlType());
            insertValues.put(APPID,mobile.getAppID());
            insertValues.put(RECOMMENDED,mobile.getRecommended());
            insertValues.put(STATE,mobile.getState());
            insertValues.put(TOWNSHIP,mobile.getTownship());
            insertValues.put(FATHER,mobile.getFather());
            insertValues.put(GENDER,mobile.getGender());
            insertValues.put(DATEOFBIRTH,mobile.getDateOfBirth());
            insertValues.put(NRC,mobile.getNRC());
            insertValues.put(IDTYPE,mobile.getIDType());
            insertValues.put(PHONE,mobile.getPhone());
            insertValues.put(BUSINESSTYPE,mobile.getBusinessType());
            insertValues.put(BUSINESSCATEGORY,mobile.getBusinessCategory());
            insertValues.put(CAR,mobile.getCar());
            insertValues.put(CARTYPE,mobile.getCarType());
            insertValues.put(LATITUDE,mobile.getLatitude());
            insertValues.put(LONGITUDE,mobile.getLongitude());
            insertValues.put(ADDRESS1,mobile.getAddress1());
            insertValues.put(ADDRESS2,mobile.getAddress2());
            insertValues.put(ACCOUNTTYPE,mobile.getAccountType());
            insertValues.put(OSTYPE,mobile.getOSType());
            insertValues.put(PROFILEPIC,mobile.getProfilePic());
            insertValues.put(SIGNATURE,mobile.getsignature());
            insertValues.put(PASSWORD,mobile.getPassword());
            insertValues.put(EMAILID,mobile.getEmailID());
            insertValues.put(ADDRESSTYPE,mobile.getAddressType());
            insertValues.put(CELLTOWERID,mobile.getCellTowerID());
            insertValues.put(BUSINESSNAME,mobile.getBusinessName());
            insertValues.put(KICKBACK_COLUMN,mobile.getKickback());
            insertValues.put(CODEALTERNATE,mobile.getCodeAlternate());
            insertValues.put(COUNTRY,mobile.getCountry());
            insertValues.put(PARENTACCOUNT,mobile.getParentAccount());
            insertValues.put(CODERECOMMENDED,mobile.getCodeRecommended());
            insertValues.put(PAYMENTGATEWAY,mobile.isPaymentGateway());
            insertValues.put(LOYALTY,mobile.isLoyalty());
            insertValues.put(SECURETOKEN,mobile.getSecureToken());
      //      insertValues.put(ACCOUNTNUMBER,"");
        //    insertValues.put(BANKNAME,"");
            Cursor c = db.rawQuery("SELECT * FROM " + USERPROFILE + " WHERE " + MOBILENUMBER + "='" + mobile.getMobileNumber() + "'", null);
            Log.e("Profile Cursor", c.getCount() + "");
            if (c.getCount() == 0) {
            long i=  db.insertOrThrow(USERPROFILE, null, insertValues);
                Log.e("Insert", "Success");
                return true;
            } else {
                db.execSQL("DELETE FROM "+USERPROFILE);
                long i=  db.insertOrThrow(USERPROFILE, null, insertValues);
                return true;
            }
        } catch (Exception e) {
            return false;
        }
    }


    public CashierModel getCashierModel(String cashierId)
    {
        CashierModel model=null;
        Cursor cursor = db.rawQuery("select  * from " + DATABASE_NAME_CASHIER+" where "+COLUMN_PRIMARY_KEY_ID+" ='"+cashierId+"'", null);

        if (cursor == null) {
            return model;
        } else
        {
            if (cursor.getCount() > 0)
            {
                cursor.moveToFirst();
                do {
                    String id = cursor.getString(cursor.getColumnIndex(COLUMN_PRIMARY_KEY_ID));
                    String cashiername = cursor.getString(cursor.getColumnIndex(KEY_CASHIER_NAME));
                    String cashiernumber = cursor.getString(cursor.getColumnIndex(KEY_CASHIER_NUMBER));
                    String password = cursor.getString(cursor.getColumnIndex(KEY_CASHIER_PASSWORD));
                    String confirmpassword = cursor.getString(cursor.getColumnIndex(KEY_CASHIER_CONFIRM_PASSWORD));
                    String color = cursor.getString(cursor.getColumnIndex(KEY_CASHIER_COLOR));
                    int colorCode = Integer.parseInt(color);
                    String createdDate = cursor.getString(cursor.getColumnIndex(KEY_CASHIER_CREATED_DATE));
                    byte [] b=cursor.getBlob(7);
                    Bitmap bmp=Utility.getPhoto(b);
                  String status=cursor.getString(cursor.getColumnIndex(KEY_CASHIER_STATUS));
                  String jobType=cursor.getString(cursor.getColumnIndex(KEY_CASHIER_JOBTYPE));

                  model = new CashierModel(id, cashiername, cashiernumber, password, confirmpassword, color, createdDate,bmp,status,jobType);

                } while (cursor.moveToNext());
                cursor.close();
            }
        }
        return model;

    }


    public static class DatabaseHelper extends SQLiteOpenHelper {




      public DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {


            try {
                String stored_maildocuments = "create table " + DATABASE_TABLE_MAIL_DETAILS + " (" + KEY_FROM_MAIL_ID + " TEXT," + KEY_FROM_MAIL_PASS + " TEXT,"+KEY_TO_MAIL_ID+" TEXT );";
                String transationTable = "create table " + DATABASE_TABLE_TRANSATION + " (" + COLUMN_PRIMARY_KEY_ID + " integer primary key autoincrement," + KEY_TRANSID + ", "+KEY_TRANSCATION_RECEIVED_KIKBACK +", " + KEY_DESCRIPTION + ", " + KEY_OLD_AMOUNT + ", " + KEY_NEW_AMOUNT + ", " + KEY_TRANSTYPE + ", " + KEY_DATE + ", " + KEY_DESTINATION_NUM + ", " + KEY_TRANACATION_TYPE + ", " + KEY_CAR_NUMBER + ", " + KEY_MESSAGE + ", " + KEY_FEE + ", " + KEY_AGENT_NAME + ", " + KEY_AGENT_NUM + ", " + KEY_AMOUNT + ", " + KEY_SERVICE_TAX + ", " + KEY_COMMEENT + ", " + KEY_NAME + ", " + KEY_IMAGE_PATH + ", " + KEY_LAT_LONG + ", " + KEY_STATUS_PENDING + ", " + KEY_BUSINESS_TYPE_LOGO+ ", " + KEY_TRANSCATION_SURVERY + ", " + KEY_TRANSCATION_WALLET_BALANCE
                        +", " + KEY_TRANSCATION_DATE +" date , " + KEY_HOURS + ","+KEY_CITY_CODE+","+GENDER+","+AGE+","+KEY_CASHIER_ID+","+KEY_CASHIER_NAME+","+KEY_CASHIER_NUMBER+","+CELLTOWERID+")";

                String Receivedmoney = "create table " + DATABASE_TABLE_RECEIVED_MONEY + " (" + COLUMN_PRIMARY_KEY_ID + " integer primary key autoincrement," + KEY_TRANSID + ", " +KEY_TRANSCATION_RECEIVED_KIKBACK +", "+ KEY_DESCRIPTION + ", " + KEY_OLD_AMOUNT + ", " + KEY_NEW_AMOUNT + ", " + KEY_TRANSTYPE + ", " + KEY_DATE + ", " + KEY_DESTINATION_NUM + ", " + KEY_TRANACATION_TYPE + ", " + KEY_CAR_NUMBER + ", " + KEY_MESSAGE + ", " + KEY_FEE + ", " + KEY_AGENT_NAME + ", " + KEY_AGENT_NUM + ", " + KEY_AMOUNT + ", " + KEY_SERVICE_TAX + ", " + COLUMN_ISRead + ", " + KEY_KEY_COUNT + ", " + KEY_COMMEENT + ", " + KEY_NAME + ", " + KEY_LAT_LONG + ", " + KEY_IMAGE_PATH +", " + KEY_TRANSCATION_SURVERY +", " + KEY_TRANSCATION_WALLET_BALANCE + ", " + KEY_TRANSCATION_DATE +" date , " + KEY_HOURS +","+KEY_CITY_CODE+","+GENDER+","+AGE+","+KEY_CASHIER_ID+","+KEY_CASHIER_NAME+","+KEY_CASHIER_NUMBER+","+CELLTOWERID+" );";

                String cityName = "create table " + DATABASE_TABLE_CITY + " (" + COLUMN_PRIMARY_KEY_ID + " integer primary key autoincrement," + KEY_CITY_ID + ", " + KEY_CITY_CODE + ", " + KEY_CITY_DESCRIPTION + ", " + KEY_CITY_STATE_ID + ");";
                String StateName = "create table " + DATABASE_TABLE_STATE + " (" + COLUMN_PRIMARY_KEY_ID + " integer primary key autoincrement," + KEY_STATE_ID + ", " + KEY_STATE_CODE + ", " + KEY_STATE_DESCRIPTION + ");";
                String loyalty = "create table " + DATABASE_TABLE_LOYALTY + " (" + COLUMN_PRIMARY_KEY_ID + " integer primary key autoincrement," + KEY_LOYALTY_MERCHANT + ", " + KEY_LOYALTY_POINT + ", " + KEY_DATE + ");";
                String contacts = "create table " + DATABASE_TABLE_CONTACTS + " (" + COLUMN_PRIMARY_KEY_ID + " integer primary key autoincrement," + KEY_NAME + ", " + KEY_EMAIL_ID + ", " + COLUMN_ID + ", " + COLUMN_PHOTO_URL + ", " + KEY_MOBILE_NUMBER + ", " + KEY_ISCONTACT_UPLOAED + ");";
                String ticketGenrate = "create table " + DATABASE_TABLE_TICKET_IMAGES + " (" + COLUMN_PRIMARY_KEY_ID + " integer primary key autoincrement," + KEY_LOYALTY_MERCHANT + ");";
                String Businformation = "create table " + DATABASE_TABLE_BUS_CITY_DATA + " (" + COLUMN_PRIMARY_KEY_ID + " integer primary key autoincrement," + KEY_BUS_CITY_ID + ", " + KEY_BUS_CITY_NAME + ");";
                String StateList = "create table " + DATABASE_TABLE_AGENT_CITY + " (" + COLUMN_PRIMARY_KEY_ID + " integer primary key autoincrement," + KEY_AGENT_CITY_ID + ", " + KEY_AGENT_CITY_NAME + ", " + KEY_R_IS_STATE_DIVISION + ", " + KEY_AGENT_CITY_NAME_BURMESE + ");";
                String twonShip = "create table " + DATABASE_TABLE_AGENT_TOWNSHIP + " (" + COLUMN_PRIMARY_KEY_ID + " integer primary key autoincrement," + KEY_AGENT_CITY_ID + ", " + KEY_AGENT_CITY_NAME + "," + KEY_AGENT_TOWNSHIP_ID + ", " + KEY_AGENT_TOWNSHIP_NAME + ", " + KEY_AGENT_TOWNSHIP_NAME_BURMESE + ", " + KEY_AGENT_IS_DEFAULT_CITY + ", " + KEY_AGENT_IS_DEFAULT_CITY_NAME + ", " + KEY_AGENT_IS_DEFAULT_CITY_NAME_BURMESE + ", " + KEY_AGENT_IS_DEFAULT_CITY_NAME_D + ", " + KEY_AGENT_IS_DEFAULT_CITY_NAME_D_BURMESE + ");";
                String deviceInfo = "create table " + DATABASE_TABLE_DEVICE_INFORMATION + " (" + COLUMN_PRIMARY_KEY_ID + " integer primary key autoincrement," + COLUMN_CUS_ID + ", " + COLUMN_DEVICE_ID + ", " + COLUMN_SIM_ID + ", " + COLUMN_MOBILE_NO + ");";
                String topup_table = "create table " + DATABASE_TABLE_TOPUP + " (" + COLUMN_PRIMARY_KEY_ID + " integer primary key autoincrement," + COLUMN_SOURCE_NO + ", " + COLUMN_DESTINATION + ", " + COLUMN_AMOUNT + ", " + COLUMN_DATE + ", " + COLUMN_BENIFITIARY + ");";
                String TrainStationName = "create table " + DATABASE_TABLE_TRAIN_STATION_NAME + " (" + COLUMN_PRIMARY_KEY_ID + " integer primary key autoincrement," + KEY_TRAIN_STATION_NAME + ", " + KEY_TRAIN_STATION_ID + ", " + KEY_TRAIN_STATION_CELL_TOWER + ");";
                String Distributor_Table = "create table " + DATABASE_TABLE_DISTRIBUTOR + " (" + COLUMN_PRIMARY_KEY_ID + " integer primary key autoincrement," + COLUMN_DISTRIBUTOR_NAME + ", " + COLUMN_DISTRIBUTOR_NUMBER + ", " + COLUMN_DISTRIBUTOR_TELECOM + ");";
                // String Top_up_Successfull_payment = "create table " + DATABASE_TABLE_TOPUP_TRANSACTION + " (" + COLUMN_PRIMARY_KEY_ID + " integer primary key autoincrement," + COLUMN_TOPUP_DESTINATION_NO + ", " + COLUMN_TOPUP_MOBILE_NO +", "+ COLUMN_TOPUP_AMOUNT + ", " + COLUMN_TOPUP_TRANSTYPE +", " + COLUMN_TOPUP_BALANCE + ", " + COLUMN_TOPUP_TRANSTYPE + ", " + COLUMN_TOPUP_TRANSTIME + ", " + COLUMN_TOPUP_BALANCE + ", " + COLUMN_TOPUP_TRANSID + ");";
                String cellIDTable = "create table " + CELL_ID_TABLE + " (" + COLUMN_PRIMARY_KEY_ID + " integer primary key autoincrement," + CELL_ID_NUMBER + ", " + BUSINESS_NAME + ");";
                String mastermobiletable = "create table " + DATABASE_MASTER_MOBILE_NO + " (" + KEY_MASTER_MOBILENO + " TEXT," + KEY_MASTER_PASSWORD + " TEXT );";

                String trainPrice = "create table " + DATABASE_NAME + " (" + COLUMN_PRIMARY_KEY_ID + " integer primary key autoincrement," + KEY_TRAIN_STATION_NAME + ", " + KEY_TRAIN_STATION_ID + ", " + TRAIN_STATION_JUNCTION_NAME + ", " + TRAIN_STATION_LEFT_VALUE + ", " + TRAIN_STATION_RIGHT_VALUE + ", " + TRAIN_STATION_SUBWAY_ID + ", " + TRAIN_STATION_UID + ");";
                String smsoffline = "create table " + DATABASE_OFFLINE_PAYMENT + " (" + COLUMN_PRIMARY_KEY_ID + " integer primary key autoincrement," + COLUMN_DESTINATION + ", " + KEY_NAME + ", " + COLUMN_AMOUNT + ", " + COMMENTS + ", " + KEY_TRANSID + ", " + UNIQUE_ID + ", " + COLUMN_DATE + ", " + KEY_NEW_AMOUNT + ");";

                String KICKBACK = "CREATE TABLE " + VIEW_KICKBACK + " ( " + COLUMN_PRIMARY_KEY_ID +
                        " INTEGER PRIMARY KEY AUTOINCREMENT, " + KICKBACK_MINRANGE + " TEXT, " + KICKBACK_MAXRANGE + " TEXT, " + KICKBACK_DISBTYPE +
                        " TEXT, " + KICKBACK_PERAMT +
                        " TEXT, " + KICKBACK_AMT + " TEXT, " + KICKBACK_Status + " TEXT, " + KICKBACK_UPDATE_BIT + " TEXT)";
                String DUMMYMERCHANTLIST = "CREATE TABLE " + DUMMYMERCHANTLIST_TABLE + " ( " + COLUMN_PRIMARY_KEY_ID +
                        " INTEGER PRIMARY KEY AUTOINCREMENT, " + DUMMYMERCHANTLIST_NAME + " TEXT, " + DUMMYMERCHANTLIST_MOBILENO + " TEXT, " + DUMMYMERCHANTLIST_COUNTRY +
                        " TEXT, " + DUMMYMERCHANTLIST_STATE +
                        " TEXT, " + DUMMYMERCHANTLIST_CITY + " TEXT, " + DUMMYMERCHANTLIST_STATUS + " TEXT)";
                String Cashier_Activity_Log_Table = "create table " + DATABASE_TABLE_ACTIVIY_lOG_CASHIER + " (" + COLUMN_PRIMARY_KEY_ID + " integer primary key autoincrement," + KEY_CASHIER_ID + " TEXT, " + KEY_CASHIER_NAME + " TEXT," + KEY_CASHIER_NUMBER + " TEXT," + KEY_CASHIER_LOGIN_TIME + " TEXT," + KEY_CASHIER_LOGOUT_TIME + " TEXT," + KEY_CASHIER_LOGOUT_REASON + " TEXT,"+KEY_STATUS+" TEXT)";

                String countryCodeList = "create table " + TABLE_COUNTRY_LIST + " (" + COLUMN_PRIMARY_KEY_ID + " integer primary key autoincrement," + COLUMAN_COUNTRY_NAME + ", " + COLUMN_COUNTRY_CODE + ", " + COLUMN_COUNTRY_CODE_FLAG + ", " + COLUMN_MCC_CODE + ");";
                String networkList = "create table " + TABLE_NETWORK_LIST + " (" + COLUMN_PRIMARY_KEY_ID + " integer primary key autoincrement," + COLUMN_MCC_CODE + ", " + COLUMN_COUNTRY_CODE + ", " + COLUMN_MNC + ", " + COLUMN_NETWORK_NAME + ", " + COLUMN_OPERATOR_NAME + ", " + COLUMN_OPERATOR_STATUS + ");";
                String dialingCode = "create table " + TABLE_DIALING_CODE + " (" + COLUMN_PRIMARY_KEY_ID + " integer primary key autoincrement," + COLUMN_MCC_CODE + ", " + COLUMN_DIALING_CITY + ", " + COLUMN_DIALING_CODE + ");";

             /*   String Agent = "CREATE TABLE "+Agent_TABLE+" ( " + COLUMN_PRIMARY_KEY_ID +
                        " INTEGER PRIMARY KEY AUTOINCREMENT, "+Agent_agentcode + " TEXT, " + Agent_source + " TEXT, "+Agent_agenttype+
                        " TEXT, "+Agent_userpack+
                        " TEXT, "+Agent_agentname+" TEXT, "+Agent_email+" TEXT, "+ Agent_dob +" TEXT, " +Agent_idprooftype +" TEXT, "+ Agent_dmsrefid +" TEXT, "+ Agent_occupation + " TEXT, "+ Agent_customerstatus + " TEXT, "+ Agent_language + " TEXT, "+ Agent_resultdescription +" TEXT, "+ Agent_transid + " TEXT )";

               */
                String Agent = "CREATE TABLE " + Agent_TABLE + " ( " + COLUMN_PRIMARY_KEY_ID +
                        " INTEGER PRIMARY KEY AUTOINCREMENT, " + Agent_agentcode + " TEXT, " + Agent_source + " TEXT, " + Agent_agenttype +
                        " TEXT, " + Agent_agentname + " TEXT, " + Agent_email + " TEXT, " + Agent_dob + " TEXT, " + Agent_idprooftype + " TEXT , " + Agent_occupation + " TEXT, " + Agent_customerstatus + " TEXT, " + Agent_status + " TEXT," + Agent_language + " TEXT, " + Agent_transid + " TEXT, " + Agent_businessname + " TEXT, " + Agent_gender + " TEXT," + Agent_age + " Text );";

                //Train Station table
                String Train_Station = "CREATE TABLE " + TRAIN_STATION_TABLE + " ( " + COLUMN_PRIMARY_KEY_ID +
                        " INTEGER PRIMARY KEY AUTOINCREMENT, " + TRAIN_ID + " INT, " + TRAIN_StationName + " TEXT, " + TRAIN_StationNumber + " TEXT, " + TRAIN_Is_Active +
                        " TEXT, " + TRAIN_Is_Circular + " TEXT, " + TRAIN_CreatedDate + " TEXT, " + TRAIN_UpdatedDate + " TEXT, " + TRAIN_MobileNumber + " TEXT , " + TRAIN_Sector + " TEXT);";

                String Train_Station_RouteLimits = "CREATE TABLE " + TRAIN_ROUTELIMITS_TABLE + " ( " + COLUMN_PRIMARY_KEY_ID +
                        " INTEGER PRIMARY KEY AUTOINCREMENT, " + TRAIN_RouteLimits_ID + " INT, " + TRAIN_RouteLimits_StationID + " INT, " + TRAIN_RouteLimits_LeftLimit + " TEXT, " + TRAIN_RouteLimits_RightLimit +
                        " TEXT, " + TRAIN_RouteLimitsIs_SubLeft + " TEXT, " + TRAIN_RouteLimits_SubRight + " TEXT, " + TRAIN_RouteLimits_Is_Active + " TEXT, " + TRAIN_RouteLimits_CreatedDate + " TEXT , " + TRAIN_RouteLimits_UpdatedDate + " TEXT);";

                //Train Station table
                String Train_Station_all = "CREATE TABLE " + TRAIN_STATION_TABLE_ALL + " ( " + TRAIN_ID +
                        " INTEGER PRIMARY KEY AUTOINCREMENT, " + TRAIN_StationName + " TEXT, " + TRAIN_StationNumber + " TEXT, " + TRAIN_Is_Active +
                        " TEXT, " + TRAIN_Is_Circular + " TEXT, " + TRAIN_MobileNumber + " TEXT , " + TRAIN_RouteLimits_LeftLimit + " TEXT ," + TRAIN_RouteLimits_RightLimit + " TEXT, " + TRAIN_RouteLimitsIs_SubLeft + " TEXT, " + TRAIN_RouteLimits_SubRight + " TEXT);";

              String profile = "CREATE TABLE " + USERPROFILE + " ( " + COLUMN_PRIMARY_KEY_ID +
                      " INTEGER PRIMARY KEY AUTOINCREMENT, " + MOBILENUMBER + " TEXT, " + NAME + " TEXT, " + GCMID + " TEXT, " + ENCRYPTED + " TEXT, " + SIMID + " TEXT, " + MSID + " TEXT, " + IMEI + " TEXT, " +
                      LTYPE + " TEXT, " + APPID + " TEXT, " + RECOMMENDED + " TEXT, " + STATE + " TEXT, " + TOWNSHIP + " TEXT, " + FATHER + " TEXT, " + GENDER + " INTEGER, " +
                      DATEOFBIRTH + " TEXT, " + NRC + " TEXT, " + IDTYPE + " TEXT, " + PHONE + " TEXT, " + BUSINESSTYPE + " TEXT, " + BUSINESSCATEGORY + " TEXT, " + CAR + " TEXT, " +
                      CARTYPE + " TEXT, " + LATITUDE + " TEXT, " + LONGITUDE + " TEXT, " + ADDRESS1 + " TEXT, " + ADDRESS2 + " TEXT, " + ACCOUNTTYPE + " TEXT, " + OSTYPE + " TEXT, " +
                      PROFILEPIC + " TEXT, " + SIGNATURE + " TEXT, " + PASSWORD + " TEXT, " + EMAILID + " TEXT, " + ADDRESSTYPE + " TEXT, " + CELLTOWERID + " TEXT, " + BUSINESSNAME + " TEXT, " +
                      KICKBACK_COLUMN + " TEXT, " + CODEALTERNATE + " TEXT, " + COUNTRY + " TEXT, " + PARENTACCOUNT + " TEXT, " + CODERECOMMENDED + " TEXT, " + PAYMENTGATEWAY + " TEXT, " +
                      LOYALTY + " TEXT, " + SECURETOKEN + " TEXT, " + AGENTCODE + " TEXT, " + BANKNAME + " TEXT, " + FBEMAILID + " TEXT, " + COUNTRY_CODE + " TEXT);";


              String cashierTable = "create table " + DATABASE_NAME_CASHIER + " (" + COLUMN_PRIMARY_KEY_ID + " integer primary key autoincrement," + KEY_CASHIER_NAME + " TEXT, " + KEY_CASHIER_NUMBER + " TEXT," + KEY_CASHIER_PASSWORD + " TEXT," + KEY_CASHIER_CONFIRM_PASSWORD + " TEXT," + KEY_CASHIER_COLOR + " TEXT," + KEY_CASHIER_CREATED_DATE + " TEXT,"+KEY_CASHIER_PHOTO+" blob not null,"+ KEY_CASHIER_STATUS+" TEXT,"+KEY_CASHIER_JOBTYPE+" TEXT );";

                String surveyRecords="CREATE TABLE " + SURVEY_DETAILS + " ( " + COLUMN_PRIMARY_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+SURVEY_QUES1 + " TEXT, " +SURVEY_QUES2 + " TEXT, " +SURVEY_QUES3 + " TEXT, " +SURVEY_QUES4 + " TEXT, " +SURVEY_QUES5 + " TEXT, " +SURVEY_QUES6 + " TEXT, " +SURVEY_QUES7 + " TEXT, " +SURVEY_QUES8 + " TEXT, " +SURVEY_TOTAL + " INTEGER, " +SURVEY_GENDER + " TEXT, " +
                        SURVEY_LOCATION + " TEXT, " + SURVEY_AGE + " INTEGER, " +SURVEY_DAY + " INTEGER, " +SURVEY_HOUR + " INTEGER, " +SURVEY_MINUTE + " INTEGER, " +SURVEY_SUGGESTION + " TEXT, " +SURVEY_PHONE + " TEXT, " +SURVEY_YEAR + " INTEGER,"+SURVEY_MONTH + " INTEGER, "+SURVEY_WEEK + " INTEGER, "+KEY_TRANSID +" TEXT, "+KEY_TRANSCATION_DATE+" date, "+SURVEY_DATE+" TEXT, "+SURVEY_AMOUNT+" INTEGER, "+SURVEY_DATE_HOUR+" TEXT, "+SURVEY_TRANS_TYPE+" TEXT );";

                String CREATE_CASHCOLLECTOR_TABLE = "create table "
                        + CASHCOLLECTOR_TABLE + " ("
                        + CASHCOLLEC_ID + " integer primary key autoincrement, "
                        + CASHCOLLEC_PHOTO + " blob not null, "
                        + CASHCOLLEC_NAME + " text, "
                        + CASHCOLLEC_NO + " text not null , "
                        + CASHCOLLEC_SOURCE + " text, "
                        +CASHCOLLEC_STATUS+" text, "
                        + CASHCOLLEC_SIGN + " blob not null );";

                String CREATE_CASH_OUT_TABLE = "create table "
                        + DATABASE_TABLE_CASH_OUT + " ("
                        + CASH_OUT_ID + " integer primary key autoincrement, "
                        + CASH_OUT_TIME + " text, "
                        + CASH_OUT_CUS_NAME+ " text, "
                        + CASH_OUT_CUS_AMT+ " text, "
                        + CASH_OUT_COM_AMT+  " text, "
                        + CASH_OUT_COM_TYPE+ " text, "
                        + CASH_OUT_COM_FINAL_AMT + " text );";

              //  OTP_LOG_AMT OTP_LOG_STATUS

                String CREATE_OTP_LOG_TABLE = "create table "
                        + DATABASE_TABLE_OTP_lOG_DETAILS_FOR_QR_GENERATE + " ("
                        + OTP_LOG_ID + " integer primary key autoincrement, "
                        + OTP_LOG_TIME + " text, "
                        + OTP_LOG_CASHIER_NAME+ " text, "
                        + OTP_LOG_CASHIER_NO+ " text, "
                        + OTP_LOG_CASHCOLLECTOR_NAME+  " text, "
                        + OTP_LOG_CASHCOLLECTOR_NO+ " text, "
                        + OTP_LOG_AMT+ " text, "
                        + OTP_LOG_STATUS+ " text, "
                        + OTP_LOG_CASHIER_ID+ " text, "
                        + OTP_LOG_TextValue+ " text, "
                        + OTP_LOG_TemplateValue + " text, "
                         +OTP_LOG_JSON_DATA+ " text );";

              String otp_log_details = "CREATE TABLE if not exists " + DATABASE_TABLE_OTP_lOG_DETAILS_FOR_QR_SCAN + " (" +

                      OTP_LOG_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                      OK_MasterORCashier_ID + " TEXT," +
                      SCANNING_TIME + " DATETIME," +
                      GENERATE_TIME + " DATETIME," +
                      OTP_ENTER_TIME + " DATETIME," +
                      OTP_LOG_CASHIER_NAME + " TEXT," +
                      OTP_LOG_CASHIER_NO + " TEXT," +
                      OTP_LOG_AMT + " TEXT," +
                      OTP_LOG_STATUS + " TEXT," +
                      OTP_LOG_PaidSTATUS + " TEXT," +
                      FROM_WHICH_SCAN + " TEXT," +
                      SCANNING_LOCATION + " TEXT," +
                      GENERATE_LOCATION + " TEXT," +
                      OTP_ENTER_LOCATION + " TEXT," +
                      GENERATER_NAME + " TEXT," +
                      GENERATER_NO + " TEXT," +
                      SCAN_LOG_TEXTVALUE + " TEXT," +
                      SCAN_LOG_TEMPLATEVALUE + " TEXT," +
                      SCAN_LOG_OPEN_OR_CLOSE + " TEXT" +
                      ");";


              String okscan_details = "CREATE TABLE " + DATABASE_TABLE_QR_SCAN_FROM_OKAPP + " (" +
                      OK_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                      OK_MasterORCashier_ID + " TEXT," +
                      OK_TIME_STAMP + " DATETIME," +
                      OK_DATE + " DATETIME," +
                      OK_RECEIVER_NAME+ " TEXT," +
                      OK_AMOUNT+ " TEXT," +
                      OK_TRANSID+ " TEXT," +
                      OK_SENDER_NO+ " TEXT," +
                      OK_RECEIVER_NO+ " TEXT," +
                      OK_TRANS_TYPE + " TEXT," +
                      OK_KICK + " TEXT," +
                      OK_LOCALITY + " TEXT," +
                      OK_LOCATION + " TEXT," +
                      OK_CELL_ID + " TEXT," +
                      OK_GENDER+ " TEXT," +
                      OK_AGE+ " TEXT," +
                      GENERATE_LOCATION + " TEXT," +
                      SCANNING_LOCATION + " TEXT," +
                      OK_STATUS + " TEXT );";

                String json_contact_table="create table "+DATABASE_TABLE_JSON_CONTACTS+" ("+KEY_JSON_CONTACT_DATA+" );";
                String json_city_table="create table "+DATABASE_TABLE_JSON_CITY+" ("+KEY_JSON_CITY_MODELS+" );";
                String json_township_table="create table "+DATABASE_TABLE_JSON_TOWNSHIP+" ("+KEY_JSON_TOWN_MODELS+" )";
                //String merchantShopDetails=" CREATE TABLE "+ MerchantShopTable +" ( "+ COLUMN_PRIMARY_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + ShopID +", "+ShopName+", "+ShopCellId+", "+ShopWifiName+", "+ShopStatus+");";
              String merchantShopDetails = " CREATE TABLE " + MerchantShopTable + " ( " + COLUMN_PRIMARY_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + ShopID + ", " + ShopName + ", " + ShopCellId + ", " + ShopWifiName + ", " + ShopStatus +", "+ShopLat + ", "+ShopLang+");";
               // String promotionDetails = " CREATE TABLE " + DATABASE_TABLE_PROMOTION_ADDVIEW + " ( " + COLUMN_PRIMARY_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + PROMOTIONID + ", " + PROMOTION_TITLE + ", " + PROMOTION_DESCRIPTION + ", " + PROMOTION_MOBILENO + ", " + PROMOTION_WEBSITE + ", " + PROMOTION_FACEBOOK + ", " + EMAIL + ", " + CONTACT_NAME + ", " + LAST_UPDATED + ");";
              String promotionDetails = " CREATE TABLE " + DATABASE_TABLE_PROMOTION_ADDVIEW + " ( " + COLUMN_PRIMARY_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + PROMOTIONID + ", " + PROMOTION_TITLE + ", " + PROMOTION_DESCRIPTION + ", " + PROMOTION_MOBILENO + ", " + PROMOTION_WEBSITE + ", " + PROMOTION_FACEBOOK + ", " + EMAIL + ", " + CONTACT_NAME + ", " + LAST_UPDATED + ");";


              String bankDetail = "CREATE TABLE " + BANKDETAIL + " ( " + COLUMN_PRIMARY_KEY_ID +
                      " INTEGER PRIMARY KEY AUTOINCREMENT, " + ACCOUNTNUMBER + " TEXT, " + BANKNAME + " TEXT, " + BRANCH + " TEXT, " + ACCOUNTTYPE + " TEXT, " + IDTYPE + " TEXT, " + IDNO + " TEXT, " + BANK_PHONE + " TEXT, " + BRANCH_ADDRESS + " TEXT, " + STATE + " TEXT, " + TOWNSHIP + " TEXT, " + ADDRESS2 +
                      " TEXT, " + BRANCH_ID + " TEXT, " + BANK_ID + " TEXT, " + BANKNAME_BURMESE + " TEXT, " + BRANCHNAME_BURMESE + " TEXT);";

              String cashBankOut = "CREATE TABLE " + CASHBANKOUTNUMBER + " ( " + COLUMN_PRIMARY_KEY_ID +
                      " INTEGER PRIMARY KEY AUTOINCREMENT, " + MSISIDN + " TEXT, " + COUNTRYCODE + " TEXT);";

              String remarks="CREATE TABLE "+DATABASE_TABLE_REMARKS+" ( "+COLUMN_PRIMARY_KEY_ID+ " INTEGER PRIMARY KEY AUTOINCREMENT, "+
                             REMARKS_TRANSID+" TEXT,"+
                             REMARKS_CELLID+" TEXT,"+
                             REMARKS_LAT+" TEXT," +
                             REMARKS_LONG+" TEXT," +
                              REMARKS_MCC+" TEXT," +
                              REMARKS_MNC+" TEXT," +
                            REMARKS_COMMENTS+" TEXT,"+
                              REMARKS_TELEPHONE_PERATOR +" TEXT);";

              String userInactivityTable="CREATE TABLE "+DATABASE_TABLE_USERINACTIVE+" ( "+COLUMN_PRIMARY_KEY_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+
                                         USERINACTIV_CASHIERID+" TEXT,"+
                                         USERINACTIV_CASHIERNAME+" TEXT,"+
                                         USERINACTIV_CASHIERNO+" TEXT,"+
                                         USERINACTIV_CELLID+" TEXT,"+
                                         USERINACTIV_LAT+" TEXT,"+
                                         USERINACTIV_LONG+" TEXT,"+
                                         USERINACTIV_MCC+" TEXT,"+
                                         USERINACTIV_MNC+" TEXT,"+
                      USERINACTIV_TELEPHONE_OPERATOR+" TEXT,"+
                      USERACTIV_DURATION+" TEXT);";

               db.execSQL(userInactivityTable);
               db.execSQL(remarks);
               db.execSQL(otp_log_details);
               db.execSQL(json_contact_table);
               db.execSQL(twonShip);
               db.execSQL(json_city_table);
               db.execSQL(json_township_table);
               db.execSQL(stored_maildocuments);
               db.execSQL(Cashier_Activity_Log_Table);
               db.execSQL(mastermobiletable);
               db.execSQL(cashierTable);
               db.execSQL(okscan_details);
               db.execSQL(deviceInfo);
               db.execSQL(Receivedmoney);
               db.execSQL(CREATE_CASHCOLLECTOR_TABLE);
               db.execSQL(surveyRecords);
               db.execSQL(profile);
               db.execSQL(CREATE_CASH_OUT_TABLE);
               db.execSQL(CREATE_OTP_LOG_TABLE);
               db.execSQL(merchantShopDetails);
               db.execSQL(promotionDetails);

                db.execSQL(Train_Station_all);
                db.execSQL(Agent);
                db.execSQL(networkList);
                db.execSQL(dialingCode);
                db.execSQL(countryCodeList);

              db.execSQL(StateList);
              Log.e("cashtable", "..." + cashierTable);

                db.execSQL(DUMMYMERCHANTLIST);
                db.execSQL(smsoffline);
                db.execSQL(KICKBACK);
                db.execSQL(Distributor_Table);
                db.execSQL(TrainStationName);
                db.execSQL(topup_table);
                db.execSQL(transationTable);
                db.execSQL(cityName);
                db.execSQL(StateName);
                db.execSQL(loyalty);
                db.execSQL(contacts);
                db.execSQL(Businformation);


              db.execSQL(profile);
              db.execSQL(bankDetail);
              db.execSQL(cashBankOut);

              Log.e("okscan_details", "..." + okscan_details);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

           /* try {
                db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_TRANSATION);
                db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_CITY);
                db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_ACTIVIY_lOG_CASHIER);
                db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_CONTACTS);
                db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_TOPUP);
                db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_TRAIN_STATION_NAME);
                db.execSQL("DROP TABLE IF EXISTS " + TRAIN_STATION_TABLE);
                db.execSQL("DROP TABLE IF EXISTS " + TRAIN_ROUTELIMITS_TABLE);
                onCreate(db);

            } catch (Exception e) {
                e.printStackTrace();
            }*/
        }
    }

    public String getSimId() {
        String simid = "";
        Cursor cursor = db.rawQuery("select  SIM_ID from " + DATABASE_TABLE_DEVICE_INFORMATION, null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    simid = cursor.getString(cursor.getColumnIndex("SIM_ID"));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return simid;
    }

    public void insertMasterMobileNumber(String mobileNumber, String password) {
        ContentValues insertValues = new ContentValues();

        insertValues.put(KEY_MASTER_MOBILENO, mobileNumber);
        insertValues.put(KEY_MASTER_PASSWORD, password);
        long i = db.insertWithOnConflict(DATABASE_MASTER_MOBILE_NO, null, insertValues, SQLiteDatabase.CONFLICT_IGNORE);
    }




    public void updateMasterMobileNumber(String mobileNumber,String pwd) {
        ContentValues insertValues = new ContentValues();

        insertValues.put(KEY_MASTER_MOBILENO, mobileNumber);
        insertValues.put(KEY_MASTER_PASSWORD,pwd);
        long i = db.update(DATABASE_MASTER_MOBILE_NO, insertValues, null, null);
    }



    //----------for storing mail and password

    public void insertMailDetails(MailDetails md) {
        ContentValues insertValues = new ContentValues();

        insertValues.put(KEY_FROM_MAIL_ID, md.getFromMailId());
        insertValues.put(KEY_FROM_MAIL_PASS, md.getPassword());
        insertValues.put(KEY_TO_MAIL_ID,md.getToMailId());
        long i = db.insertWithOnConflict(DATABASE_TABLE_MAIL_DETAILS, null, insertValues, SQLiteDatabase.CONFLICT_IGNORE);
    }

  public void insertRemarkDetails(Remarks model)
  {
    ContentValues insertValues = new ContentValues();

    insertValues.put(REMARKS_TRANSID,model.getTransId());
    insertValues.put(REMARKS_CELLID, model.getCellId());
    insertValues.put(REMARKS_LAT,model.getLatitude());
    insertValues.put(REMARKS_LONG,model.getLongitude());
    insertValues.put(REMARKS_MCC,model.getMcc());
    insertValues.put(REMARKS_MNC,model.getMnc());
    insertValues.put(REMARKS_TELEPHONE_PERATOR,model.getTelePhoneOperator());
    insertValues.put(REMARKS_COMMENTS,model.getComments());

    long i = db.insert(DATABASE_TABLE_REMARKS, null, insertValues);

  }

  public void insertUserInactivityModel(UserInctivityModel model)
  {
    ContentValues insertValues = new ContentValues();

    insertValues.put(USERINACTIV_CASHIERID,model.getCashierId());
    insertValues.put(USERINACTIV_CASHIERNO,model.getCashierNo());
    insertValues.put(USERINACTIV_CASHIERNAME,model.getCashierName());
    insertValues.put(USERINACTIV_CELLID,model.getCellId());
    insertValues.put(USERINACTIV_LAT,model.getLat());
    insertValues.put(USERINACTIV_LONG,model.getLongi());
    insertValues.put(USERINACTIV_MCC,model.getMcc());
    insertValues.put(USERINACTIV_MNC,model.getMnc());
    insertValues.put(USERINACTIV_TELEPHONE_OPERATOR,model.getTelephoneOperator());
    insertValues.put(USERACTIV_DURATION,model.getDuration());

    long i=db.insert(DATABASE_TABLE_USERINACTIVE,null,insertValues);


  }

  public int UserInactiveCount()
  {
    Cursor cursor = db.rawQuery("select  * from " + DATABASE_TABLE_USERINACTIVE , null);
    if (cursor != null)
    {
      if (cursor.getCount() > 0)
      {
        cursor.close();
        return cursor.getCount();
      } else
      cursor.close();
    }
    return 0;

  }

    public static boolean isContactPresent(String name, String number) {
        boolean isPrensent = false;
        Cursor cursor = db.rawQuery("select  * from " + DATABASE_TABLE_CONTACTS + " where " + KEY_NAME + "='" + name + "' and " + KEY_MOBILE_NUMBER + "='" + number + "'", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    isPrensent = true;
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return isPrensent;
    }





    public void updateMailDetails(MailDetails md) {
        ContentValues insertValues = new ContentValues();

        insertValues.put(KEY_FROM_MAIL_ID, md.getFromMailId());
        insertValues.put(KEY_FROM_MAIL_PASS, md.getPassword());
        insertValues.put(KEY_TO_MAIL_ID,md.getToMailId());// KEY_FROM_MAIL_ID + " ='" + md.getFromMailId() + "'"
        long i = db.update(DATABASE_TABLE_MAIL_DETAILS, insertValues,null, null);
    }


    public MailDetails getMaildetails()
    {
        MailDetails md=null;
        Cursor cursor=db.rawQuery("select * from " + DATABASE_TABLE_MAIL_DETAILS, null);
        if(cursor!=null)
        {
            if(cursor.getCount()>0)
            {
                cursor.moveToFirst();
                do {
                    String fromMailId=cursor.getString(cursor.getColumnIndex(KEY_FROM_MAIL_ID));
                    String pwd=cursor.getString(cursor.getColumnIndex(KEY_FROM_MAIL_PASS));
                    String toMailId=cursor.getString(cursor.getColumnIndex(KEY_TO_MAIL_ID));

                    md=new MailDetails(fromMailId,toMailId,pwd);

                }while(cursor.moveToNext());
            }
            cursor.close();
        }
        return md;
    }


    //--------------------------------

    public MasterDetails getMasterDetails() {
        MasterDetails md = null;
        Cursor cursor = db.rawQuery("select  * from " + DATABASE_MASTER_MOBILE_NO, null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    String master_mobileno = cursor.getString(cursor.getColumnIndex(KEY_MASTER_MOBILENO));
                    String password = cursor.getString(cursor.getColumnIndex(KEY_MASTER_PASSWORD));
                    md = new MasterDetails(master_mobileno, password);
                }

                while (cursor.moveToNext());
            }
            cursor.close();
        }
        return md;
    }


    public void insertCashierDetails(CashierModel model) {
        ContentValues insertValues = new ContentValues();

        insertValues.put(KEY_CASHIER_NAME, model.getCashier_Name());
        insertValues.put(KEY_CASHIER_NUMBER, model.getCashier_Number());
        insertValues.put(KEY_CASHIER_PASSWORD, model.getPassword());
        insertValues.put(KEY_CASHIER_CONFIRM_PASSWORD, model.getConfirmPassword());
        insertValues.put(KEY_CASHIER_COLOR, model.getColorCode());
        insertValues.put(KEY_CASHIER_CREATED_DATE, model.getCreatedDate());
        insertValues.put(KEY_CASHIER_PHOTO,Utility.getBytes(model.getCashierPhoto()));
        insertValues.put(KEY_CASHIER_CREATED_DATE, model.getCreatedDate());
        insertValues.put(KEY_CASHIER_STATUS,model.getStatus());
      insertValues.put(KEY_CASHIER_JOBTYPE,model.getJobType());

        long i = db.insertWithOnConflict(DATABASE_NAME_CASHIER, null, insertValues, SQLiteDatabase.CONFLICT_IGNORE);
        Log.e("insertvalue", "..." + i);
    }

    public void insertOTPCashierDetails(OTPCashierModel model) {
        ContentValues insertValues = new ContentValues();

        insertValues.put(OTP_LOG_TIME, model.getTime());
        insertValues.put(OTP_LOG_CASHIER_NAME, model.getCashiername());
        insertValues.put(OTP_LOG_CASHIER_NO, model.getCashierno());
        insertValues.put(OTP_LOG_CASHCOLLECTOR_NAME, model.getCashcollectorname());
        insertValues.put(OTP_LOG_CASHCOLLECTOR_NO, model.getCashcollectorno());
        insertValues.put(OTP_LOG_AMT, model.getAmout());
        insertValues.put(OTP_LOG_STATUS,model.getStatus());
        insertValues.put(OTP_LOG_CASHIER_ID,model.getCashierid());
        insertValues.put(OTP_LOG_TextValue,model.getTextValue());
        insertValues.put(OTP_LOG_TemplateValue, model.getTemplateValue());
        insertValues.put(OTP_LOG_JSON_DATA, model.getJsondata());

        long i = db.insert(DATABASE_TABLE_OTP_lOG_DETAILS_FOR_QR_GENERATE, null, insertValues);
        Log.e("insertvalue", "..." + i);
    }

    public void updateCashierDetails(CashierModel model) {
        ContentValues insertValues = new ContentValues();
        insertValues.put(KEY_CASHIER_NAME, model.getCashier_Name());
        insertValues.put(KEY_CASHIER_NUMBER, model.getCashier_Number());
        insertValues.put(KEY_CASHIER_PASSWORD, model.getPassword());
        insertValues.put(KEY_CASHIER_CONFIRM_PASSWORD, model.getConfirmPassword());
        insertValues.put(KEY_CASHIER_COLOR, model.getColorCode());
        insertValues.put(COLUMN_PRIMARY_KEY_ID, model.getCashier_Id());
        insertValues.put(KEY_CASHIER_CREATED_DATE, model.getCreatedDate());
      insertValues.put(KEY_CASHIER_PHOTO,Utility.getBytes(model.getCashierPhoto()));
        insertValues.put(KEY_CASHIER_STATUS,model.getStatus());
        insertValues.put(KEY_CASHIER_CREATED_DATE, model.getCreatedDate());
      insertValues.put(KEY_CASHIER_JOBTYPE,model.getJobType());
        long i = db.update(DATABASE_NAME_CASHIER, insertValues, COLUMN_PRIMARY_KEY_ID + "='" + model.getCashier_Id() + "'", null);
        Log.e("insertvalue", "..." + model.getCashier_Id());
    }

  public void updateCollectortoCollectorScanDetails( String no, String jsondat) {

    Log.d("Values ", no + jsondat);

    ContentValues values = new ContentValues();


    values.put(OTP_LOG_CASHIER_NO, no);
    values.put(OTP_LOG_JSON_DATA, jsondat);


    try {
      long stat=    db.update(DATABASE_TABLE_OTP_lOG_DETAILS_FOR_QR_GENERATE, values, OTP_LOG_CASHIER_NO + "= '" + no + "'", null);
      Log.e("stat",""+stat);
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }

  }


  public void deleteUpdateCashierDetails(CashierModel model)
    {
        ContentValues insertValues = new ContentValues();
        insertValues.put(KEY_CASHIER_NAME, model.getCashier_Name());
        insertValues.put(KEY_CASHIER_NUMBER, model.getCashier_Number());
        insertValues.put(KEY_CASHIER_PASSWORD, model.getPassword());
        insertValues.put(KEY_CASHIER_CONFIRM_PASSWORD, model.getConfirmPassword());
        insertValues.put(KEY_CASHIER_COLOR, model.getColorCode());
        insertValues.put(COLUMN_PRIMARY_KEY_ID, model.getCashier_Id());
        insertValues.put(KEY_CASHIER_CREATED_DATE, model.getCreatedDate());

        insertValues.put(KEY_CASHIER_STATUS,CASHIER_DELETED);
        insertValues.put(KEY_CASHIER_CREATED_DATE, model.getCreatedDate());
        long i = db.update(DATABASE_NAME_CASHIER, insertValues, COLUMN_PRIMARY_KEY_ID + "='" + model.getCashier_Id() + "'", null);
        Log.e("insertvalue", "..." + model.getCashier_Id());
    }

    //insertCashierAttendance................. and Update Cashier Attendance

    public void insertCashierDetails(CashierActivityLogModel model) {
        ContentValues insertValues = new ContentValues();

        insertValues.put(KEY_CASHIER_ID, model.getCashier_Id());
        insertValues.put(KEY_CASHIER_NAME,model.getCashier_Name());
        insertValues.put(KEY_CASHIER_NUMBER,model.getCashier_Number());
        insertValues.put(KEY_CASHIER_LOGIN_TIME, model.getLogin_Time());
        insertValues.put(KEY_CASHIER_LOGOUT_TIME, model.getLogout_Time());
        insertValues.put(KEY_CASHIER_LOGOUT_REASON,model.getLogout_Reason());
        insertValues.put(KEY_STATUS, model.getStatus());

        long i = db.insert(DATABASE_TABLE_ACTIVIY_lOG_CASHIER, null, insertValues);
        Log.e("insertvalue", "..." + i);
    }




    public void updateCashierAttendanceDetails(String cashierId,String reason)
    {
        ContentValues insertValues = new ContentValues();

        DateFormat targetFormat = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss");
        String date=targetFormat.format(new Date());

        insertValues.put(KEY_CASHIER_LOGOUT_TIME, date);
        insertValues.put(KEY_STATUS, STATUS_LOGGEDOUT);
        insertValues.put(KEY_CASHIER_ID,cashierId);
        insertValues.put(KEY_CASHIER_LOGOUT_REASON, reason);


       long i = db.update(DATABASE_TABLE_ACTIVIY_lOG_CASHIER, insertValues,  KEY_CASHIER_ID +" ='" + cashierId+"'"+" AND "+KEY_STATUS+" = '"+STATUS_LOGGEDIN+"'", null);

    }

    //insertCashierAttendance.................

    public void insertEmpDetails(CashCollectorModel employee) {
        ContentValues cv = new ContentValues();
        cv.put(CASHCOLLEC_PHOTO, Utility.getBytes(employee.getPhoto()));
        cv.put(CASHCOLLEC_NAME, employee.getName());
        cv.put(CASHCOLLEC_NO, employee.getNumber());
        cv.put(CASHCOLLEC_SOURCE, employee.getSource());
        cv.put(CASHCOLLEC_SIGN, Utility.getBytes(employee.getBitmap_Sign()));
        cv.put(CASHCOLLEC_STATUS,employee.getStatus());

        db.insert(CASHCOLLECTOR_TABLE, null, cv);
    }


  public void updateCashcollectorDetails(CashCollectorModel employee)
  {
    ContentValues cv = new ContentValues();
    cv.put(CASHCOLLEC_PHOTO, Utility.getBytes(employee.getPhoto()));
    cv.put(CASHCOLLEC_NAME, employee.getName());
    cv.put(CASHCOLLEC_NO, employee.getNumber());
    cv.put(CASHCOLLEC_SOURCE, employee.getSource());
    cv.put(CASHCOLLEC_SIGN, Utility.getBytes(employee.getBitmap_Sign()));
    cv.put(CASHCOLLEC_STATUS,employee.getStatus());
    cv.put(CASHCOLLEC_ID,employee.getId());

    db.update(CASHCOLLECTOR_TABLE, cv, CASHCOLLEC_ID + " ='" + employee.getId() + "'", null);
  }

    public void deleteEmpDetails(CashCollectorModel employee) {
        ContentValues cv = new ContentValues();
      /*  cv.put(CASHCOLLEC_PHOTO, Utility.getBytes(employee.getPhoto()));
        cv.put(CASHCOLLEC_NAME, employee.getName());
        cv.put(CASHCOLLEC_NO, employee.getNumber());
        cv.put(CASHCOLLEC_SOURCE, employee.getSource());
        cv.put(CASHCOLLEC_SIGN, Utility.getBytes(employee.getBitmap_Sign()));*/
        cv.put(CASHCOLLEC_STATUS,CASHCOLLECTOR_DELETED);

        db.update(CASHCOLLECTOR_TABLE, cv, CASHCOLLEC_ID + " = '" + employee.getId() + "'", null);
    }

    public void insertCashOutDetails(CashOutModel employee) {
        ContentValues cv = new ContentValues();
        cv.put(CASH_OUT_TIME, employee.getTime());
        cv.put(CASH_OUT_CUS_NAME, employee.getName());
        cv.put(CASH_OUT_CUS_AMT, employee.getamount());
        cv.put(CASH_OUT_COM_TYPE, employee.getComm_Type());
        cv.put(CASH_OUT_COM_AMT, employee.getcomm_amt());
        cv.put(CASH_OUT_COM_FINAL_AMT, employee.getFinalAmout());
       // cv.put(CASHCOLLEC_SIGN, Utility.getBytes(employee.getBitmap_Sign()));

        db.insert(DATABASE_TABLE_CASH_OUT, null, cv);
    }


    SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", Locale.US);
    public ArrayList<CustomerSurveyModel> getSelectedSurveyDataByRange(char gender, int scoreRange1, int scoreRange2, int ageRange1, int ageRange2, Date dateRange1, Date dateRange2)
    {
        ArrayList<CustomerSurveyModel> list = new ArrayList<>();
        Cursor cursor = db.rawQuery("select  * from " + SURVEY_DETAILS + " where " + SURVEY_GENDER + " LIKE '"+gender+"' AND "+SURVEY_TOTAL +" BETWEEN "+ scoreRange1 +" AND "+scoreRange2+" AND "+SURVEY_AGE +" BETWEEN "+ageRange1+" AND "+ageRange2+" AND "+KEY_TRANSCATION_DATE +" BETWEEN '"+df.format(dateRange1) +"' AND '"+df.format(dateRange2)+"'", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    list.add(new CustomerSurveyModel(cursor));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return list;
    }


    public static boolean insert_Activity_Log(String cash_id, String cash_name, String cash_num, String login_time, String logout_time, String logout_reason) {
        try {
            {
                ContentValues insertValues = new ContentValues();
                insertValues.put(KEY_CASHIER_ID, cash_id);
                insertValues.put(KEY_CASHIER_NAME, cash_name);
                insertValues.put(KEY_CASHIER_NUMBER, cash_num);
                insertValues.put(KEY_CASHIER_LOGIN_TIME, login_time);
                insertValues.put(KEY_CASHIER_LOGOUT_TIME, logout_time);
                insertValues.put(KEY_CASHIER_LOGOUT_REASON, logout_reason);

                //int record = db.update(DATABASE_TABLE_DEVICE_INFORMATION, insertValues, COLUMN_SIM_ID + "='" + SimId + "'", null);
                //if (record == 0) {
                db.insert(DATABASE_TABLE_ACTIVIY_lOG_CASHIER, null, insertValues);
                //} else {
                //  return false;
                // }

            }
            return true;
        } catch (Exception e) {
            return false;

        }
    }

    public boolean checkColorExist(String color) {
        Cursor cursor = db.rawQuery("select  " + KEY_CASHIER_COLOR + " from " + DATABASE_NAME_CASHIER +" where "+KEY_CASHIER_STATUS+" ='"+CASHIER_ACTIVE+"'", null);
        boolean status = false;
        if (cursor == null)
            return status;
        else {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                status = false;
                do {
                    String color_code = cursor.getString(cursor.getColumnIndex(KEY_CASHIER_COLOR));
                    if (color.equalsIgnoreCase(color_code)) {
                        status = true;
                        cursor.close();
                        return status;
                    }
                } while (cursor.moveToNext());
                cursor.close();
            }
        }
        return status;
    }

    public boolean isItCashierNumber(String number) {
        Cursor cursor = db.rawQuery("select  " + KEY_CASHIER_NUMBER + " from " + DATABASE_NAME_CASHIER+" where "+KEY_CASHIER_STATUS+" ='"+CASHIER_ACTIVE+"'", null);
        boolean status = false;
        if (cursor == null)
            return status;
        else {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                status = false;
                do {
                    String cashierNumber = cursor.getString(cursor.getColumnIndex(KEY_CASHIER_NUMBER));
                    if (cashierNumber.equals(number)) {
                        status = true;
                        cursor.close();
                        return status;
                    }
                } while (cursor.moveToNext());
                cursor.close();
            }
        }
        return status;
    }//isItCashierNumberActive

  public boolean isItCashierNumberActive(String number) {
    Cursor cursor = db.rawQuery("select  " + KEY_CASHIER_NUMBER + " from " + DATABASE_NAME_CASHIER+" where "+KEY_CASHIER_STATUS+" ='"+CASHIER_ACTIVE+"'", null);
    boolean status = false;
    if (cursor == null)
      return status;
    else {
      if (cursor.getCount() > 0) {
        cursor.moveToFirst();
        status = false;
        do {
          String cashierNumber = cursor.getString(cursor.getColumnIndex(KEY_CASHIER_NUMBER));
          if (cashierNumber.equals(number)) {
            status = true;
            cursor.close();
            return status;
          }
        } while (cursor.moveToNext());
        cursor.close();
      }
    }
    return status;
  }

    public CashierModel checkCashierLogin(String userName, String password) {
        CashierModel model = null;
        Cursor cursor = db.rawQuery("select *  from " + DATABASE_NAME_CASHIER + " where " + KEY_CASHIER_NUMBER + " ='" + userName + "' and " + KEY_CASHIER_PASSWORD + "='" + password + "'"+" and "+KEY_CASHIER_STATUS+" ='"+CASHIER_ACTIVE+"'", null);

      if (cursor == null)
            return model;
        else {
            if (cursor.getCount() > 0)
            {
                cursor.moveToFirst();
                String id = cursor.getString(cursor.getColumnIndex(COLUMN_PRIMARY_KEY_ID));
                String cashiername = cursor.getString(cursor.getColumnIndex(KEY_CASHIER_NAME));
                String cashiernumber = cursor.getString(cursor.getColumnIndex(KEY_CASHIER_NUMBER));
                String pwd = cursor.getString(cursor.getColumnIndex(KEY_CASHIER_PASSWORD));
                String confirmpassword = cursor.getString(cursor.getColumnIndex(KEY_CASHIER_CONFIRM_PASSWORD));
                String color = cursor.getString(cursor.getColumnIndex(KEY_CASHIER_COLOR));
                String date = cursor.getString(cursor.getColumnIndex(KEY_CASHIER_CREATED_DATE));
                byte[] b=cursor.getBlob(7);
                Bitmap bmp=Utility.getPhoto(b);
              String status = cursor.getString(cursor.getColumnIndex(KEY_CASHIER_STATUS));
              String jobType=cursor.getString(cursor.getColumnIndex(KEY_CASHIER_JOBTYPE));

                model = new CashierModel(id, cashiername, cashiernumber, pwd, confirmpassword, color, date,bmp,status,jobType);
                cursor.close();
            }
        }
        return model;
    }

    //getting cashier object with cashierid
    public CashierModel getCashierObject(String cashierId)
    {
        CashierModel model = null;
        Cursor cursor = db.rawQuery("select *  from " +DATABASE_NAME_CASHIER  + " where " + COLUMN_PRIMARY_KEY_ID + " ='" + cashierId + "'"+" and "+KEY_CASHIER_STATUS+" ='"+CASHIER_ACTIVE+"'", null);
        if (cursor == null)
            return model;
        else {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                String id = cursor.getString(cursor.getColumnIndex(COLUMN_PRIMARY_KEY_ID));
                String cashiername = cursor.getString(cursor.getColumnIndex(KEY_CASHIER_NAME));
                String cashiernumber = cursor.getString(cursor.getColumnIndex(KEY_CASHIER_NUMBER));
                String pwd = cursor.getString(cursor.getColumnIndex(KEY_CASHIER_PASSWORD));
                String confirmpassword = cursor.getString(cursor.getColumnIndex(KEY_CASHIER_CONFIRM_PASSWORD));
                String color = cursor.getString(cursor.getColumnIndex(KEY_CASHIER_COLOR));
                String date = cursor.getString(cursor.getColumnIndex(KEY_CASHIER_CREATED_DATE));
                byte[] b=cursor.getBlob(7);
                Bitmap bmp=Utility.getPhoto(b);
              String status = cursor.getString(cursor.getColumnIndex(KEY_CASHIER_STATUS));
              String jobType=cursor.getString(cursor.getColumnIndex(KEY_CASHIER_JOBTYPE));

                model = new CashierModel(id, cashiername, cashiernumber, pwd, confirmpassword, color, date,bmp,status,jobType);

                model = new CashierModel(id, cashiername, cashiernumber, pwd, confirmpassword, color, date,bmp,status,jobType);
                cursor.close();
            }
        }
        return model;
    }
    //.....................................

    //get Number of Cashiers logged in Currently
    public ArrayList<CashierActivityLogModel> getNumberOfCashiers()
    {
        ArrayList<CashierActivityLogModel> al_cashierLoginModel=null;

        Cursor cursor = db.rawQuery("select *  from " + DATABASE_TABLE_ACTIVIY_lOG_CASHIER +" where "+KEY_STATUS+"='"+STATUS_LOGGEDIN+"'", null);

        if (cursor == null)
            return al_cashierLoginModel;
        else
        {
            if (cursor.getCount() > 0)
            {
                al_cashierLoginModel=new ArrayList<CashierActivityLogModel>();
                cursor.moveToFirst();
                do {
                    String sno = cursor.getString(cursor.getColumnIndex(COLUMN_PRIMARY_KEY_ID));
                    String cashierId = cursor.getString(cursor.getColumnIndex(KEY_CASHIER_ID));
                    String cashierName=cursor.getString(cursor.getColumnIndex(KEY_CASHIER_NAME));
                    String cashierNumber=cursor.getString(cursor.getColumnIndex(KEY_CASHIER_NUMBER));
                    String loginTime = cursor.getString(cursor.getColumnIndex(KEY_CASHIER_LOGIN_TIME));
                    String logoutTime = cursor.getString(cursor.getColumnIndex(KEY_CASHIER_LOGOUT_TIME));
                    String logoutReason=cursor.getString(cursor.getColumnIndex(KEY_CASHIER_LOGOUT_REASON));
                    String status = cursor.getString(cursor.getColumnIndex(KEY_STATUS));

                    CashierActivityLogModel model=new CashierActivityLogModel(sno,cashierId,cashierName,cashierNumber,loginTime,logoutTime,logoutReason,status);
                    al_cashierLoginModel.add(model);

                }while (cursor.moveToNext());

                cursor.close();
            }
        }
        return al_cashierLoginModel;
    }

    //................................

    //-------------checking cashier already logged in or not

    //get Number of Cashiers logged in Currently
    public int isCashierPresent(String cashierId)
    {
     int status=0;
        Cursor cursor = db.rawQuery("select *  from " + DATABASE_TABLE_ACTIVIY_lOG_CASHIER +" where "+KEY_STATUS+"='"+STATUS_LOGGEDIN+"' AND "+KEY_CASHIER_ID+"='"+cashierId+"'", null);

        if (cursor == null)
            return status;
        else
        {
          if(cursor.getCount()==0)
            status=99;
            else
              status=1;

        }

        return status;
    }


    //--------------checking cashier alreadylogged in or not

    public int getCashierDetailsCount() {
        ArrayList<CashierModel> al_cashier = null;
        Cursor cursor = db.rawQuery("select  * from " + DATABASE_NAME_CASHIER+" where "+KEY_CASHIER_STATUS+" ='"+CASHIER_ACTIVE+"'", null);
        if (cursor == null) {
            return 0;
        } else {
            if (cursor.getCount() > 0) {
                return cursor.getCount();
            }
        }
        return 0;
    }

    public static boolean insertCity(CityList_Model model) {
        try {

            ContentValues insertValues = new ContentValues();
            insertValues.put(KEY_AGENT_CITY_ID, model.getId());
            insertValues.put(KEY_AGENT_CITY_NAME, model.getName());
            insertValues.put(KEY_R_IS_STATE_DIVISION, model.getIsState());
            insertValues.put(KEY_AGENT_CITY_NAME_BURMESE, model.getBName());

            int record = db.update(DATABASE_TABLE_AGENT_CITY, insertValues, KEY_AGENT_CITY_ID + "='" + model.getId() + "'", null);
            if (record == 0) {
                db.insert(DATABASE_TABLE_AGENT_CITY, null, insertValues);
            } else {
                db.delete(DATABASE_TABLE_AGENT_CITY, KEY_AGENT_CITY_ID + "='" + model.getId() + "'", null);
                db.insert(DATABASE_TABLE_AGENT_CITY, null, insertValues);
            }

            return true;
        } catch (Exception e) {
            return false;
        }

    }

    public Cursor Get_CASHCOLLECTOR_TABLE()

    {
        String query = "SELECT * FROM "+CASHCOLLECTOR_TABLE+" where "+CASHCOLLEC_STATUS+" = '"+CASHCOLLECTOR_ACTIVE+"'" ;
        Log.i("Get_CASHCOLLECTOR_Query",query);


        Cursor c = db.rawQuery(query, null);
        if(c != null)
        {
            c.moveToFirst();
        }

        return c;
    }

  //check cashcollector phone no is present or not
  public  boolean isCashCollectorPhoneNo_Present(String phoneno) {
    boolean check = false;
    Cursor cursor = db.rawQuery("select  * from " + CASHCOLLECTOR_TABLE + " where " + CASHCOLLEC_NO + " ='" + phoneno + "'", null);
    if (cursor != null) {
      if (cursor.getCount() > 0)
      {
        check = true;
      }
      cursor.close();
    }
    return check;
  }

    public Cursor Get_CASHOUT_TABLE()
    {
        String query = "SELECT * FROM "+DATABASE_TABLE_CASH_OUT +" ORDER BY cashout_time DESC";
        Log.i("Get_CASHCOLLECTOR_Query",query);

        Cursor c = db.rawQuery(query, null);
        if(c != null)
        {
            c.moveToFirst();
        }

        return c;
    }

    public ArrayList<String> getSELECTED_TOWNSHIP_NAME(String id) {
        ArrayList<String> list = new ArrayList<String>();
        Cursor cursor = db.rawQuery("select  aTownshipName from " + DATABASE_TABLE_AGENT_TOWNSHIP + " where " + KEY_AGENT_CITY_ID + "='" + id + "'", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    list.add(cursor.getString(cursor.getColumnIndex(KEY_AGENT_TOWNSHIP_NAME)));
                } while (cursor.moveToNext());

                cursor.close();

            }
            return list;
        }
        return list;
    }




    public static boolean isTranscationPresent(String transId) {
        boolean check = false;
        Cursor cursor = db.rawQuery("select  * from " + DATABASE_TABLE_TRANSATION + " where " + KEY_TRANSID + " ='" + transId + "'", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                check = true;
            }
            cursor.close();
        }
        return check;
    }


    public ArrayList<CustomerSurveyModel> getSelectedSurveyData(int month, int year, String day, char gender, int scoreRange1, int scoreRange2, int ageRange1, int ageRange2)
    {
        ArrayList<CustomerSurveyModel> list = new ArrayList<>();
        Cursor cursor = db.rawQuery("select  * from " + SURVEY_DETAILS + " where " + SURVEY_GENDER + " LIKE '"+gender+"' AND "+SURVEY_DAY + " LIKE '"+day +"' AND "+SURVEY_MONTH+ " = "+month+" AND "+SURVEY_YEAR+" ="+year+" AND "+SURVEY_TOTAL +" BETWEEN "+ scoreRange1 +" AND "+scoreRange2+" AND "+SURVEY_AGE +" BETWEEN "+ageRange1+" AND "+ageRange2, null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    list.add(new CustomerSurveyModel(cursor));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return list;
    }

    public String getLastSurveyDate(){
        String date="";
        HashMap<String,TransactionSurvey> list = new HashMap<>();
        Cursor cursor = db.rawQuery("select * from " + SURVEY_DETAILS +" ORDER BY " + KEY_TRANSCATION_DATE + " DESC", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToLast();
                date = cursor.getString(cursor.getColumnIndex(DBHelper.KEY_TRANSCATION_DATE));
                cursor.close();
            }
        }
        return date;
    }
    public static void insertSurvey(String type,String survey, String date, String age, String gender, String city, String transatId, String amount){
        if(!isSurveyPresent(transatId))
        {

            try {
                Double dAmount= Double.parseDouble(amount);
                ContentValues insertValues = new ContentValues();
                if(survey.contains("{")) {
                    JSONArray jsonSurvey = new JSONArray("[" + survey + "]");
                    JSONObject jsonObjQue = new JSONObject();
                    jsonObjQue = (JSONObject) jsonSurvey.get(0);
                    insertValues.put(SURVEY_QUES1, jsonObjQue.getString("Q"));
                    jsonObjQue = (JSONObject) jsonSurvey.get(1);
                    insertValues.put(SURVEY_QUES2, jsonObjQue.getString("Q"));
                    jsonObjQue = (JSONObject) jsonSurvey.get(2);
                    insertValues.put(SURVEY_QUES3, jsonObjQue.getString("Q"));
                    jsonObjQue = (JSONObject) jsonSurvey.get(3);
                    insertValues.put(SURVEY_QUES4, jsonObjQue.getString("Q"));
                    jsonObjQue = (JSONObject) jsonSurvey.get(4);
                    insertValues.put(SURVEY_QUES5, jsonObjQue.getString("Q"));
                    jsonObjQue = (JSONObject) jsonSurvey.get(5);
                    insertValues.put(SURVEY_QUES6, jsonObjQue.getString("Q"));
                    jsonObjQue = (JSONObject) jsonSurvey.get(6);
                    insertValues.put(SURVEY_QUES7, jsonObjQue.getString("Q"));
                    jsonObjQue = (JSONObject) jsonSurvey.get(7);
                    insertValues.put(SURVEY_QUES8, jsonObjQue.getString("Q"));
                    jsonObjQue = (JSONObject) jsonSurvey.get(8);
                    insertValues.put(SURVEY_TOTAL, Math.round(Double.parseDouble(jsonObjQue.getString("T"))));
                    insertValues.put(SURVEY_SUGGESTION, jsonObjQue.getString("S"));
                    insertValues.put(SURVEY_PHONE, jsonObjQue.getString("P"));
                }
                int hours=Utils.getHourFromDateTime(date);
                insertValues.put(SURVEY_HOUR,hours );
                insertValues.put(SURVEY_MINUTE,Utils.getMinutesFromDateTime(date));
                insertValues.put(KEY_TRANSCATION_DATE,date);
                date=TransactionStatics.formatDateFromString(date);
                insertValues.put(SURVEY_DATE,date);
                insertValues.put(SURVEY_DAY,TransactionStatics.getDAY_OF_MONTH(date));
                insertValues.put(SURVEY_WEEK, TransactionStatics.getWeekOfMonth(date));
                insertValues.put(SURVEY_MONTH,TransactionStatics.getMonthFromDate(date));
                insertValues.put(SURVEY_YEAR, TransactionStatics.getYearOfDate(date));
                insertValues.put(SURVEY_AGE,age);
                insertValues.put(SURVEY_GENDER,gender);
                insertValues.put(SURVEY_LOCATION,city);
                insertValues.put(KEY_TRANSID,transatId);
                insertValues.put(SURVEY_AMOUNT,dAmount);
                insertValues.put(SURVEY_DATE_HOUR,date+" "+hours);
                insertValues.put(SURVEY_TRANS_TYPE,type);
                db.insert(SURVEY_DETAILS,null,insertValues);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
    public static boolean isSurveyPresent(String transId) {
        boolean check = false;
        Cursor cursor = db.rawQuery("select  * from " + SURVEY_DETAILS + " where " + KEY_TRANSID + " ='" + transId + "'", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                check = true;
            }
            cursor.close();
        }
        return check;
    }

    public HashMap<String,TransactionSurvey> getSelectedTransactionData(String table, char gender, int amountRange1, int amountRange2, int ageRange1, int ageRange2, Date dateRange1, Date dateRange2) {

        HashMap<String, TransactionSurvey> list = new HashMap<>();
        Cursor cursor = db.rawQuery("SELECT sum(amount) AS Total, " + SURVEY_HOUR + "," + SURVEY_DAY + ", " + SURVEY_MONTH + ", " + SURVEY_YEAR + ", " + SURVEY_DATE + " FROM " + SURVEY_DETAILS + " where " + SURVEY_TRANS_TYPE + " = '" + table + "' AND gender LIKE '" + gender + "' AND amount BETWEEN " + amountRange1 + " AND " + amountRange2 + " AND age BETWEEN " + ageRange1 + " AND " + ageRange2 + " AND transcationDate BETWEEN '" + df.format(dateRange1) + "' AND '" + df.format(dateRange2) + "' GROUP BY date ", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    list.put(cursor.getString(cursor.getColumnIndex(SURVEY_DATE)), new TransactionSurvey(cursor));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return list;
    }
    public HashMap<String,TransactionSurvey> getHourlyTransactionData(String table,char gender, int amountRange1, int amountRange2, int ageRange1, int ageRange2, Date dateRange1, Date dateRange2){

        HashMap<String,TransactionSurvey> list = new HashMap<>();
        Cursor cursor = db.rawQuery("SELECT sum(amount) AS Total, "+SURVEY_HOUR+","+SURVEY_DAY+", "+SURVEY_MONTH+", "+SURVEY_YEAR+", "+SURVEY_DATE+", "+SURVEY_DATE_HOUR+ " FROM " + SURVEY_DETAILS + " where "+SURVEY_TRANS_TYPE+" = '"+table+"' AND gender LIKE '" + gender + "' AND amount BETWEEN " + amountRange1 + " AND " + amountRange2 + " AND age BETWEEN " + ageRange1 + " AND " + ageRange2 + " AND transcationDate BETWEEN '" + df.format(dateRange1) + "' AND '" + df.format(dateRange2) + "' GROUP BY date_hour ", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    list.put(cursor.getString(cursor.getColumnIndex(SURVEY_DATE_HOUR)),new TransactionSurvey(cursor));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return list;
    }

    public Cursor getAllCashiersCursor()
    {
        Cursor c=db.rawQuery("select * from "+DATABASE_NAME_CASHIER +" where "+KEY_CASHIER_STATUS+" ='"+CASHIER_ACTIVE+"'",null);
        if(c!=null)
        {
            if(c.getCount()>0)
                return c;
            else
                return c;
        }
        else
            return c;
    }

    public Cursor getCashCollector_Cursor()
    {
        String query = "SELECT * FROM "+CASHCOLLECTOR_TABLE+" where "+CASHCOLLEC_STATUS+" = '"+CASHCOLLECTOR_ACTIVE+"'" ;
        Log.i("Get_CASHCOLLECTOR_Query",query);


        Cursor c = db.rawQuery(query, null);
        if(c != null)
        {
            if(c.getCount()>0)
                return c;
            else
                return null;
        }

        return c;
    }

    public   String [] getTransactionColumnCount()
  {
      Cursor dbCursor = db.query(DATABASE_TABLE_RECEIVED_MONEY, null, null, null, null, null, null);
      String[] columnNames = dbCursor.getColumnNames();

      return columnNames;
  }

    public ArrayList<ConstactModel> getContactsDetails() {
        ArrayList<ConstactModel> list = new ArrayList<ConstactModel>();
        Cursor cursor = db.rawQuery("select  * from " + DATABASE_TABLE_CONTACTS + " where " + KEY_ISCONTACT_UPLOAED + "='" + "false" + "'", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    list.add(new ConstactModel(cursor));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return list;
    }


    public ArrayList<ConstactModel> getContactsDetailsDB() {
        ArrayList<ConstactModel> list = new ArrayList<ConstactModel>();
        Cursor cursor = db.rawQuery("select  * from " + DATABASE_TABLE_CONTACTS, null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    list.add(new ConstactModel(cursor));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return list;
    }


  public static boolean insertPromotion(PromotionModel model) {
    try {

      ContentValues insertValues = new ContentValues();
      insertValues.put(PROMOTIONID, model.getId());
      insertValues.put(PROMOTION_TITLE, model.getTitle());
      insertValues.put(PROMOTION_DESCRIPTION, model.getDescripton());
      insertValues.put(PROMOTION_MOBILENO, model.getMobileNo());
      insertValues.put(PROMOTION_WEBSITE, model.getWebsite());
      insertValues.put(PROMOTION_FACEBOOK, model.getFacebook());
      insertValues.put(EMAIL, model.getEmail());
      insertValues.put(CONTACT_NAME, model.getName());
      insertValues.put(LAST_UPDATED, model.getLast_updated());

      int record = db.update(DATABASE_TABLE_PROMOTION_ADDVIEW, insertValues, PROMOTIONID + " ='" + model.getId() + "'", null);
      if (record == 0) {
        db.insert(DATABASE_TABLE_PROMOTION_ADDVIEW, null, insertValues);
      } else {
        db.delete(DATABASE_TABLE_PROMOTION_ADDVIEW, PROMOTIONID + "='" + model.getId() + "'", null);
        db.insert(DATABASE_TABLE_PROMOTION_ADDVIEW, null, insertValues);
      }

      return true;
    } catch (Exception e) {
      Log.v("Error", e.toString());
      return false;
    }

  }


  //TODO Promotion get value from the database


  public PromotionModel getPromotionData() {
    PromotionModel model= new PromotionModel();
    Cursor cursor = db.rawQuery("select  * from " + DATABASE_TABLE_PROMOTION_ADDVIEW , null);
    if (cursor != null) {
      if (cursor.getCount() > 0) {
        cursor.moveToFirst();
        do {
          model=new PromotionModel(cursor);
        } while (cursor.moveToNext());
      }
      cursor.close();
    }
    return model;
  }

  public static void insertShopDetails(String shopId, String name, String shopCellId, String status, String wifiName, String lat, String lang) {

    ContentValues insertValues = new ContentValues();
    insertValues.put(ShopID, shopId);
    insertValues.put(ShopName, name);
    insertValues.put(ShopCellId, shopCellId);
    insertValues.put(ShopStatus, status);
    insertValues.put(ShopWifiName, wifiName);
    insertValues.put(ShopLat,lat);
    insertValues.put(ShopLang,lang);
    db.insert(MerchantShopTable, null, insertValues);
  }


  public static String getShopName(String id )
  {
    String sName="";
    Cursor cursor = db.rawQuery("select "+ShopName+" from "+MerchantShopTable+ " where "+ShopCellId +" = '"+id+"' OR "+ShopID+" = '"+id+"' OR "+ ShopWifiName+" = '"+id+"'",null);

    if(cursor.getCount()>0)
    {
      cursor.moveToFirst();
      sName=cursor.getString(cursor.getColumnIndex(ShopName));
      cursor.close();
    }
    return sName;
  }

  public static String getShopID(String name)
  {
    String sID="";
    Cursor cursor = db.rawQuery("select " + ShopID + " from " + MerchantShopTable + " where " + ShopCellId + " = '" + name + "' OR " + ShopName + " = '" + name + "' OR " + ShopWifiName + " = '" + name + "'", null);

    if(cursor.getCount()>0)
    {
      cursor.moveToFirst();
      sID=cursor.getString(cursor.getColumnIndex(ShopID));
      cursor.close();
    }
    return sID;
  }

  public static boolean getIsShopLocationExist(String id)
  {
    Cursor cursor = db.rawQuery("select * from "+MerchantShopTable+ " where "+ShopCellId +" = '"+id+"' OR "+ ShopWifiName+" = '"+id+"'",null);
    int count = cursor.getCount();
    cursor.close();
    return count > 0;
  }

  public static boolean getIsShopNameExist(String name)
  {
    Cursor cursor = db.rawQuery("select "+ShopName+" from "+MerchantShopTable+ " where "+ShopName +" = '"+name+"'",null);
    int count = cursor.getCount();
    cursor.close();
    return count > 0;

  }

  public static boolean removeShopArea()
  {
    int count =db.delete(MerchantShopTable, null, null);
    return count>0;
  }

  public static int getAllAreaCount()
  {
    Cursor cursor = db.rawQuery("select " + ShopName + " from " + MerchantShopTable, null);
    int count = cursor.getCount();
    cursor.close();
    return count;
  }

  public static ArrayList<MerchantShopDetailsModel> getShopList() {
    Cursor cursor = db.rawQuery("select distinct " + ShopName + "," + ShopID + "," + ShopStatus + "," + ShopCellId + " from " + MerchantShopTable, null);
    ArrayList<MerchantShopDetailsModel> shopDetailsList = new ArrayList<>();
    if (cursor.getCount() > 0) {
      cursor.moveToFirst();
      do {
        shopDetailsList.add(new MerchantShopDetailsModel(cursor));
      } while (cursor.moveToNext());
    }
    cursor.close();
    return shopDetailsList;
  }

  public static int updateShopStatus(String shopID, String status) {
    ContentValues values = new ContentValues();
    values.put(ShopStatus, status);

    return db.update(MerchantShopTable, values, ShopID + "= '" + shopID + "'", null);
  }

  public static boolean getIsShopCellIDActive(String id)
  {
    Cursor cursor = db.rawQuery("select * from "+MerchantShopTable+ " where "+ShopCellId +" = '"+id+"' AND "+ShopStatus+" = 'ON'",null);
    int count = cursor.getCount();
    cursor.close();
    return count > 0;
  }

  public static boolean getIsShopWifiIDActive(String id)
  {
    Cursor cursor = db.rawQuery("select * from "+MerchantShopTable+ " where "+ShopWifiName +" = '"+id+"' AND "+ShopStatus+" = 'ON'",null);
    int count = cursor.getCount();
    cursor.close();
    return count > 0;
  }

  public static boolean getShopStatus(String id)
  {
    Cursor cursor = db.rawQuery("select "+ShopStatus+" from "+MerchantShopTable+ " where "+ShopID +" = '"+id+"'",null);
    int count = cursor.getCount();
    cursor.close();
    return count > 0;
  }

  public static int updateNotApprovedShopStatus(String name, String shopId, String status){
    ContentValues values = new ContentValues();
    values.put(ShopStatus, status);
    values.put(ShopID, shopId);
    return db.update(MerchantShopTable,values,ShopName+"= '"+name+"'",null);
  }

  public static String getShopLatLang(String shopId){
    String latlang="0.0";
    Cursor cursor = db.rawQuery("select "+ShopLat+", "+ShopLang +" from "+MerchantShopTable+ " where " + ShopID + " = '" + shopId + "' LIMIT 1",null);
    if(cursor.getCount()>0)
    {
      cursor.moveToFirst();
      latlang = cursor.getString(cursor.getColumnIndex(ShopLat))+","+cursor.getString(cursor.getColumnIndex(ShopLang));
    }
    cursor.close();
    return latlang;
  }


  public void deletePromotion() {
    db.execSQL("delete from " + DATABASE_TABLE_PROMOTION_ADDVIEW);
  }


  public static boolean getIsShopIDActive(String id) {
    Cursor cursor = db.rawQuery("select * from " + MerchantShopTable + " where " + ShopID + " = '" + id + "' AND " + ShopStatus + " = 'ON'", null);
    int count = cursor.getCount();
    cursor.close();
    return count > 0;
  }


  public void insertCashBankOutNo(List<CashBankOutNoModel> listOfModel) {
    try {
      ContentValues insertValues = new ContentValues();
      for (CashBankOutNoModel model : listOfModel) {
        insertValues.put(MSISIDN, model.getMsisidn());
        insertValues.put(COUNTRYCODE, model.getCountrycode());
        db.insert(CASHBANKOUTNUMBER, null, insertValues);
        Log.e("Insert", "Success");
      }
    } catch (Exception e) {
      e.printStackTrace();
    }


  }

  public void insertBankDetail(List<BankDetailsModel> listOfModel) {
    try {
      ContentValues insertValues = new ContentValues();
      for (BankDetailsModel model : listOfModel) {
        insertValues.put(ACCOUNTNUMBER, model.getAccountNumber());
        insertValues.put(BANKNAME, model.getBankName());
        insertValues.put(BRANCH, model.getBranch());
        insertValues.put(ACCOUNTTYPE, model.getAccountType());
        insertValues.put(BRANCH_ADDRESS, model.getBranchAddress());
        insertValues.put(IDTYPE, model.getIDType());
        insertValues.put(IDNO, model.getIDNo());
        insertValues.put(BANK_PHONE, model.getBankPhone());
        insertValues.put(STATE, model.getState());
        insertValues.put(TOWNSHIP, model.getTownship());
        insertValues.put(ADDRESS2, model.getBranchAddress2());
        insertValues.put(BRANCH_ID, model.getBranchId());
        insertValues.put(BANK_ID, model.getBankId());
        insertValues.put(BRANCHNAME_BURMESE, model.getBranchBurmeseName());
        insertValues.put(BANKNAME_BURMESE, model.getBankBurmeseName());
        db.insert(BANKDETAIL, null, insertValues);
      }

    } catch (Exception e) {

    }
  }

  public void deleteBankDetails() {
    db.execSQL("delete from " + BANKDETAIL);
  }


  public void deleteCashBankOutNo() {
    db.execSQL("delete from " + CASHBANKOUTNUMBER);
    Log.e("Delete", "Success");
  }

  public static ArrayList<String> getLast4Transaction(){
    ArrayList<String> sID = new ArrayList<>();
    Cursor cursor = db.rawQuery("SELECT "+KEY_TRANSID+" FROM " + DATABASE_TABLE_TRANSATION +" ORDER BY " + COLUMN_PRIMARY_KEY_ID + " DESC LIMIT 4", null);
    if (cursor.getCount() > 0) {
      cursor.moveToFirst();
      do {
        sID.add(cursor.getString(cursor.getColumnIndex(KEY_TRANSID)));
      }while (cursor.moveToNext());
      cursor.close();
    }
    return sID;
  }


  public void deleteAllTableData()
  {
    try {
      Log.e("deleteAllTableCalled",".........");
      db.execSQL("delete from " + USERPROFILE);
      db.execSQL("delete from " + DATABASE_MASTER_MOBILE_NO);
      db.execSQL("delete from " + DATABASE_TABLE_JSON_CONTACTS);
      db.execSQL("delete from " + DATABASE_TABLE_MAIL_DETAILS);
      db.execSQL("delete from " + DATABASE_TABLE_TRANSATION);
      db.execSQL("delete from " + DATABASE_TABLE_RECEIVED_MONEY);
      db.execSQL("delete from " + DATABASE_NAME_CASHIER);
      db.execSQL("delete from " + DATABASE_TABLE_ACTIVIY_lOG_CASHIER);
      db.execSQL("delete from " + DATABASE_TABLE_DEVICE_INFORMATION);
      db.execSQL("delete from " + DATABASE_TABLE_CASH_OUT);
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }

  }


}

