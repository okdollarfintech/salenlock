package mm.dummymerchant.sk.merchantapp.fragments;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TableRow;

import java.util.ArrayList;

import mm.dummymerchant.sk.merchantapp.ActivityLogDetails;
import mm.dummymerchant.sk.merchantapp.CashCollector;
import mm.dummymerchant.sk.merchantapp.CashierMenuActivity;
import mm.dummymerchant.sk.merchantapp.Cashout;
import mm.dummymerchant.sk.merchantapp.NewSettingActivity;
import mm.dummymerchant.sk.merchantapp.OkDollarTransactionsActivity;
import mm.dummymerchant.sk.merchantapp.QRcodeActivity;
import mm.dummymerchant.sk.merchantapp.R;
import mm.dummymerchant.sk.merchantapp.SliderScreen;
import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.Constant;
import mm.dummymerchant.sk.merchantapp.ViewCashierActivity;
import mm.dummymerchant.sk.merchantapp.adapter.Dashboard_adapter;
import mm.dummymerchant.sk.merchantapp.customView.AlertDialogRadio;
import mm.dummymerchant.sk.merchantapp.customView.CustomButton;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
import mm.dummymerchant.sk.merchantapp.db.DBHelper;
import mm.dummymerchant.sk.merchantapp.model.CashierModel;
import mm.dummymerchant.sk.merchantapp.model.dashboard_model;
import mm.dummymerchant.sk.merchantapp.wirelessService.WirelessUtils;

/**
 * Created by Dell on 11/18/2015.
 */
public class DashBoardFragment extends Fragment implements View.OnClickListener,Constant
{
    LinearLayout mCreateCashier,mactivityLog, mCash_out, mPermanentLogout,mCashcollector_Qrcode, mTemporaryLogout,Setting_page,mCashcollector,mOkdollartransaction;
    int position = 0;
    View.OnClickListener listener;
    View rootView=null;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        rootView=inflater.inflate(R.layout.new_dashboard2,container,false);
       // init();
        callNewInit();
        return rootView;
    }

    void init() {

       /* mCreateCashier = (TableRow)rootView.findViewById(R.id.createcahier);
        mactivityLog = (TableRow)rootView.findViewById(R.id.activitylog);
        mOkdollartransaction =(TableRow)rootView.findViewById(R.id.okdollartransaction);
        mViewCashier = (TableRow)rootView.findViewById(R.id.viewcashier);
        mPermanentLogout = (TableRow)rootView.findViewById(R.id.permanent_logout);
        mTemporaryLogout = (TableRow)rootView.findViewById(R.id.temporary_logout);
        Setting_page = (TableRow) rootView.findViewById(R.id.Setting_page);
        mCashcollector= (TableRow)rootView.findViewById(R.id.cashcollector);
        mCashcollector_Qrcode = (TableRow) rootView.findViewById(R.id.cashcollector_qrcode);
        mCash_out = (TableRow) rootView.findViewById(R.id.cash_out);*/

        mCreateCashier = (LinearLayout)rootView.findViewById(R.id.createcahier);
        mactivityLog = (LinearLayout)rootView.findViewById(R.id.activitylog);
        mOkdollartransaction =(LinearLayout)rootView.findViewById(R.id.okdollartransaction);
        mPermanentLogout = (LinearLayout)rootView.findViewById(R.id.permanent_logout);
        mTemporaryLogout = (LinearLayout)rootView.findViewById(R.id.temporary_logout);
        Setting_page = (LinearLayout) rootView.findViewById(R.id.Setting_page);
        mCashcollector= (LinearLayout)rootView.findViewById(R.id.cashcollector);
        mCashcollector_Qrcode = (LinearLayout) rootView.findViewById(R.id.cashcollector_qrcode);
        mCash_out = (LinearLayout) rootView.findViewById(R.id.cash_out);

        mCash_out.setSelected(true);

        listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /** Getting the fragment manager */
                FragmentManager manager = getFragmentManager();
                /** Instantiating the DialogFragment class */
                AlertDialogRadio alert = new AlertDialogRadio();
                /** Creating a bundle object to store the selected item's index */
                Bundle b  = new Bundle();
                /** Storing the selected item's index in the bundle object */
                b.putInt("position", position);
                /** Setting the bundle object to the dialog fragment object */
                alert.setArguments(b);
                /** Creating the dialog fragment object, which will in turn open the alert dialog window */
                alert.show(manager, "alert_dialog_radio");
                //Toast.makeText(getApplicationContext(), "position " + position, Toast.LENGTH_SHORT).show();
            }
        };

       // callAllListeners();

     /*   if(AppPreference.getMasterOrCashier(getActivity()).equalsIgnoreCase(MASTER))
        {
            mCreateCashier.setOnClickListener(this);
            mTemporaryLogout.setVisibility(View.GONE);
            rootView.findViewById(R.id.view_templogout).setVisibility(View.GONE);
        }
        else
        {
            mCreateCashier.setVisibility(View.GONE);
            rootView.findViewById(R.id.line_bottom_settings).setVisibility(View.GONE);
            rootView.findViewById(R.id.Setting_page).setVisibility(View.GONE);
           // rootView.findViewById(R.id.view_vcashierline).setVisibility(View.GONE);
            rootView.findViewById(R.id.createcashierLine).setVisibility(View.GONE);
        }
*/
        mactivityLog.setOnClickListener(this);
        //mTransactionHistory.setOnClickListener(this);
        mPermanentLogout.setOnClickListener(this);
        //mReceivedMoney.setOnClickListener(this);
        mOkdollartransaction.setOnClickListener(this);
        mTemporaryLogout.setOnClickListener(((SliderScreen)getActivity()).listener);
        mCashcollector.setOnClickListener(this);
        mCashcollector_Qrcode.setOnClickListener(this);
        Setting_page.setOnClickListener(this);
        mCash_out.setOnClickListener(this);

      /*  if(AppPreference.getMasterOrCashier(this).equalsIgnoreCase(CASHIER))
        {
            mCreateCashier.setVisibility(View.GONE);
            mViewCashier.setVisibility(View.GONE);
            findViewById(R.id.view_vcashierline).setVisibility(View.GONE);
            findViewById(R.id.createcashierLine).setVisibility(View.GONE);
            //showToast(NewLoginStep2Activity.cashierModel_login2.getCashier_Name());
        }*/
    }

    void callAllListeners() {
        Context con=((SliderScreen)getActivity()).getContextforFragment();

    }

    @Override
    public void onClick(View v)
    {
        Intent intent;
        switch (v.getId())
        {
            case R.id.createcahier:
                intent =new Intent(getActivity(),CashierMenuActivity.class);
                startActivity(intent);
                break;

            case R.id.activitylog:
                intent =new Intent(getActivity(),ActivityLogDetails.class);
                startActivity(intent);
                break;

            case R.id.okdollartransaction:
                intent =new Intent(getActivity(),OkDollarTransactionsActivity.class);
                startActivity(intent);
                break;

            case R.id.viewcashier:
                DBHelper dbb=new DBHelper(getActivity());
                dbb.open();
                ArrayList<CashierModel> al_cashiers=dbb.getCashierDetails();
                dbb.close();
                if(al_cashiers!=null)
                {
                    if(al_cashiers.size()>0)
                    {
                        intent = new Intent(getActivity(), ViewCashierActivity.class);
                        startActivity(intent);
                    }
                    else
                    {
                        ((SliderScreen)getActivity()).callCashierNotcreatedToastMsg();
                    }
                }
                else
                    ((SliderScreen)getActivity()).callCashierNotcreatedToastMsg();
                break;

            case R.id.permanent_logout:
                exitAPp();
                break;

            case R.id.Setting_page:
                //finish();
                intent =new Intent(getActivity(),NewSettingActivity.class);
                startActivity(intent);
                break;

            case R.id.cashcollector:
                intent =new Intent(getActivity(),CashCollector.class);
                startActivity(intent);
                break;

            case R.id.cashcollector_qrcode:
                intent =new Intent(getActivity(),QRcodeActivity.class);
                startActivity(intent);
                break;

            case R.id.cash_out:
                intent =new Intent(getActivity(),Cashout.class);
                startActivity(intent);
                break;

            default:
                break;
        }
    }

    public void exitAPp() {
        LayoutInflater li = LayoutInflater.from(getActivity());
        View promptsView = li.inflate(R.layout.gps_alert_dialog, null);

        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(promptsView);

        CustomTextView title = (CustomTextView) promptsView.findViewById(R.id.title);
        CustomTextView alert = (CustomTextView) promptsView.findViewById(R.id.alert);
        title.setText(R.string.logout_title);
        alert.setText(R.string.logout_msg);
        CustomButton yes = (CustomButton) promptsView.findViewById(R.id.btn_settings);
        CustomButton no = (CustomButton) promptsView.findViewById(R.id.btn_cancel);
        yes.setText(R.string.dailog_yes);
        no.setText(R.string.dailog_no);

        final AlertDialog dialog1 = alertDialogBuilder.create();
        yes.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                AppPreference.setAuthToken(getActivity(), "");
                AppPreference.setFinishActivity(getActivity(), true);
                if (AppPreference.getMerchantStatus(getActivity()))
                {
                    WirelessUtils.getInstance().setBluetooth(false, getActivity());
                    WirelessUtils.getInstance().changeBluetoothName(false, getActivity());
                    WirelessUtils.getInstance().setHotspotOFF(getActivity());

                } else {
                    WirelessUtils.getInstance().setBluetooth(false,getActivity());
                }

                ((SliderScreen)getActivity()).callLogout(LOGOUT_PERMANENT_MSG);
                  ((SliderScreen) getActivity()).callCallingApIS();
                dialog1.dismiss();
            }
        });
        no.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog1.dismiss();
            }
        });
        dialog1.show();

    }

    void callNewInit()
    {

        ArrayList<dashboard_model> alist=new ArrayList<dashboard_model>();
        GridView gv=(GridView)rootView.findViewById(R.id.gridView);

        if(AppPreference.getMasterOrCashier(getActivity()).equalsIgnoreCase(MASTER))
        alist.add(new dashboard_model(getString(R.string.cashier_title), "0", R.drawable.create_cashier));

        alist.add(new dashboard_model(getString(R.string.activitylog),"1",R.drawable.activity_log));
        alist.add(new dashboard_model(getString(R.string.okdollar), "2", R.drawable.ok_icon));

        alist.add(new dashboard_model(getString(R.string.cashcollector), "3", R.drawable.cash_collector));
        alist.add(new dashboard_model(getString(R.string.cashcollector_qrcode),"4",R.drawable.qr_code_generator));
        alist.add(new dashboard_model(getString(R.string.permanentlogout), "5", R.drawable.permanent_logout));
        alist.add(new dashboard_model(getString(R.string.templogout), "6", R.drawable.temporary_logout));

        alist.add(new dashboard_model(getString(R.string.cashout), "7", R.drawable.cash_out));
        if(AppPreference.getMasterOrCashier(getActivity()).equalsIgnoreCase(MASTER))
        alist.add(new dashboard_model(getString(R.string.setting), "8", R.drawable.setting));

        Dashboard_adapter adapter=new Dashboard_adapter(getActivity(),alist,1);
        gv.setAdapter(adapter);


    }

}
