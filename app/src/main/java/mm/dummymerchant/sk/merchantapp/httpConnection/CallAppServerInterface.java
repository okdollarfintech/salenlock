package mm.dummymerchant.sk.merchantapp.httpConnection;

import com.loopj.android.http.RequestParams;

/**
 * Created by abhishekmodi on 20/10/15.
 */
public interface CallAppServerInterface {

    public abstract <T> void callRequestAPIAppServer(String url, RequestParams mPrams,int address);
}
