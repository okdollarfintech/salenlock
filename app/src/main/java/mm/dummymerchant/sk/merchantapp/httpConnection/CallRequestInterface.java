package mm.dummymerchant.sk.merchantapp.httpConnection;

import android.os.Bundle;

/**
 * Created by abhishekmodi on 19/10/15.
 */
public interface CallRequestInterface
{

    public abstract <T> void callRequestAPI(String url, Bundle mPrams, int address);
}
