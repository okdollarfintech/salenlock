package mm.dummymerchant.sk.merchantapp.httpConnection;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.loopj.android.http.RequestParams;

import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.Constant;
import mm.dummymerchant.sk.merchantapp.Utils.Utils;
import mm.dummymerchant.sk.merchantapp.db.DBHelper;
import mm.dummymerchant.sk.merchantapp.model.MasterDetails;
import mm.dummymerchant.sk.merchantapp.model.PromotionModel;


/**c
 * Created by abhishekmodi on 19/10/15.
 */
public class CallingApIS implements Constant {


    Context mContext;
    public static int address = -1;
    CallRequestInterface callRequestInterface;
    CallAppServerInterface callAppserverInterface;

    public CallingApIS(Context mContext, CallRequestInterface callRequestInterface) {

        this.mContext = mContext;
        this.callRequestInterface = callRequestInterface;
    }

    public void CallChangePinAPI(String mOld, String newPin){
        Bundle params = new Bundle();
        params.putString(MPI, mOld);
        params.putString(PARAM_NEW_PIN, newPin);
        params.putString(PARAM_TOKEN, AppPreference.getAuthTokem(mContext));
        params.putString(AGENT_CODE, AppPreference.getMyMobileNo(mContext));
        params.putString(PARAM_SOURCE, AppPreference.getMyMobileNo(mContext));
        callRequestInterface.callRequestAPI(BASE_URL + ";" + URL_CHANGE_PIN, params, REQUEST_TYPE_CHANGE_PIN);
    }


    public CallingApIS(Context mContext, CallAppServerInterface callRequestInterface,int test) {

        this.mContext = mContext;
        this.callAppserverInterface = callRequestInterface;
    }

    public void callOtpRequest(String mobileNo) {
        Bundle params = new Bundle();
        params.putString(AGENT_CODE, mobileNo);
        callRequestInterface.callRequestAPI(BASE_URL + ";" + URL_AUTH, params, REQUEST_TYPE_OTP);
    }

    public void callLoginRequest(String mobileNo, String mpin) {
        Bundle params = new Bundle();
        params.putString(AGENT_CODE, mobileNo);
        params.putString(MPI, mpin);
        callRequestInterface.callRequestAPI(BASE_URL + ";" + URL_LOGIN, params, REQUEST_TYPE_LOGIN);
    }


    public void addMerchantShopDetails(String shopName, String lat, String lng, String cellID) {
        RequestParams params = new RequestParams();
        params.put("ShopName", shopName);
        params.put("Simid", Utils.simSerialNumber(mContext));
        params.put("MobileNumber", AppPreference.getMyMobileNo(mContext));
        params.put("CellID", cellID);
        params.put("MSID", Utils.getSimSubscriberID(mContext));
        params.put("Lat", lat);
        params.put("Long", lng);
        params.put("OSType", 0);
        params.put("Country", AppPreference.getCountryName(mContext));
        callAppserverInterface.callRequestAPIAppServer(BASE_APP_URL + URL_ADD_SHOP_DETAILS, params, REQUEST_TYPE_ADD_SHOPS);
    }

    public void addProfileEmailAlerts(Boolean status) {
        RequestParams params = new RequestParams();
        params.put("Simid", Utils.simSerialNumber(mContext));
        params.put("MobileNumber", AppPreference.getMyMobileNo(mContext));
        params.put("MSID", Utils.getSimSubscriberID(mContext));
        params.put("Tag", "emailTransNotification");
        params.put("Status", status);
        params.put("OSType", 0);
        callAppserverInterface.callRequestAPIAppServer(BASE_APP_URL + URL_ADD_PROFILE_SETTINGS, params, REQUEST_TYPE_ADD_EMAIL_SETTINGS);
    }

    public void addProfileSMSAlerts(Boolean status) {
        RequestParams params = new RequestParams();
        params.put("Simid", Utils.simSerialNumber(mContext));
        params.put("MobileNumber", AppPreference.getMyMobileNo(mContext));
        params.put("MSID", Utils.getSimSubscriberID(mContext));
        params.put("Tag", "smsTransNotification");
        params.put("Status", status);
        params.put("OSType", 0);
        callAppserverInterface.callRequestAPIAppServer(BASE_APP_URL + URL_ADD_PROFILE_SETTINGS, params, REQUEST_TYPE_ADD_SMS_SETTINGS);
    }



    public void CallLastTransationHistoryNEW(String mPin,String start_index,String end_index)
    {
        Bundle params= new Bundle();

        params.putString(PARAM_TOKEN, AppPreference.getAuthTokem(mContext));

        if(AppPreference.getMasterOrCashier(mContext).equalsIgnoreCase(MASTER)) {
            params.putString(AGENT_CODE, AppPreference.getMyMobileNo(mContext));
            params.putString(MPI, mPin);

        }
        else
        {
            DBHelper db=new DBHelper(mContext);
            db.open();
            MasterDetails mm= db.getMasterDetails();
            if(mm!=null)
            {
                params.putString(AGENT_CODE, mm.getUsername());
                params.putString(MPI, mm.getPassword());
            }
            db.close();
        }
        params.putString(START_INDEX,start_index);
        params.putString(LAST_INDEX, end_index);

        callRequestInterface.callRequestAPI(BASE_URL + ";" + URL_TRANSCATION_INFO, params, REQUEST_TYPE_TRANSINFO_NEW);
    }

    public void CallReceivedTransation(int startindex,int lastindex)
    {

            Bundle params = new Bundle();

            params.putString(KEY_DATE_FORMAT, Utils.getTimeFormat());

            if(AppPreference.getMasterOrCashier(mContext).equalsIgnoreCase(MASTER))
            {
                params.putString(AGENT_CODE, AppPreference.getMyMobileNo(mContext));
                params.putString(MPI, AppPreference.getPasword(mContext));

            }
            else
            {
                DBHelper db=new DBHelper(mContext);
                db.open();
                MasterDetails mm= db.getMasterDetails();
                if(mm!=null)
                {
                    // Toast.makeText(mContext,mm.getUsername()+".."+mm.getPassword(), Toast.LENGTH_LONG).show();
                    params.putString(AGENT_CODE, mm.getUsername());
                    params.putString(MPI, mm.getPassword());
                }
                db.close();
            }
            params.putString("criteria", "y");
            params.putString(START_INDEX, String.valueOf(startindex));
            params.putString(LAST_INDEX, String.valueOf(lastindex));

            callRequestInterface.callRequestAPI(BASE_URL + ";" + URL_TRANSCATION_INFO, params, REQUEST_TYPE_RECEIVED_TRANSCATION);
    }










    public void callLogoutAPI() {
        Bundle params = new Bundle();
        params.putString(AGENT_CODE, AppPreference.getMyMobileNo(mContext));
        params.putString(PIN, AppPreference.getPasword(mContext));
        params.putString(PARAM_TOKEN, AppPreference.getAuthTokem(mContext));
        callRequestInterface.callRequestAPI(BASE_URL + ";" + REQUEST_TYPE_LOGOUT_APP, params, REQUEST_TYPE_LOGOUT);
    }

    public void callGPSFromCellID(String mobileNo, String cellTowerID) {
        RequestParams params = new RequestParams();
        params.put("MobileNumber", mobileNo);
        //params.put("CellID", cellTowerID);
        params.put("CellID", "9911931");
        callAppserverInterface.callRequestAPIAppServer(BASE_APP_URL_TESTING + URL_GET_GPS_CELLID, params, REQUESTTYPE_GPS_CELLID);
    }


    public void deleteMerchantShop(String shopId){
        RequestParams params = new RequestParams();
        params.put("ShopId", shopId);
        callAppserverInterface.callRequestAPIAppServer(BASE_APP_URL + URL_DELETE_SHOP, params, REQUEST_TYPE_DELETE_SHOPS);
    }

    public void getMerchantShopStatus(String phoneNo){
        RequestParams params = new RequestParams();
        params.put("MobileNumber",phoneNo);
        callAppserverInterface.callRequestAPIAppServer(BASE_APP_URL+URL_CHECK_STATUS_SHOP, params, REQUEST_CHECK_STATUS_SHOPS);
    }

    public void viewPromotion(String  id) {
        RequestParams params = new RequestParams();
        params.put("MobileNumber", AppPreference.getMyMobileNo(mContext));
        params.put("Simid", Utils.simSerialNumber(mContext));
        params.put("MSID", Utils.getSimSubscriberID(mContext));
        params.put("OSType", 0);
        params.put("PromotionId",id);
        callAppserverInterface.callRequestAPIAppServer(BASE_APP_URL + URL_VIEW_PROMOTION, params, REQUEST_VIEW_PROMOTION);
    }

    public void deletePromotion() {
        RequestParams params = new RequestParams();
        params.put("PromotionId", AppPreference.getPromotionId(mContext));
        callAppserverInterface.callRequestAPIAppServer(BASE_APP_URL + URL_DELETE_PROMOTION, params, REQUEST_DELETE_PROMOTION);
    }

    public void addPromotionMsg(PromotionModel model) {
        RequestParams params = new RequestParams();
        params.put("MobileNumber", AppPreference.getMyMobileNo(mContext));
        params.put("Simid", Utils.simSerialNumber(mContext));
        params.put("MSID", Utils.getSimSubscriberID(mContext));
        params.put("OSType", 0);
        params.put("PromotionHeader", model.getTitle());
        params.put("Description", model.getDescripton());
        params.put("WebUrl", model.getWebsite());
        params.put("PhoneNumber", model.getMobileNo());
        params.put("FacebookId", model.getFacebook());
        params.put("ShopId", AppPreference.getCurrentShopID(mContext));
        Log.i("ShopId ", AppPreference.getCurrentShopID(mContext));
        callAppserverInterface.callRequestAPIAppServer(BASE_APP_URL + URL_ADD_PROMOTION, params, REQUEST_ADD_PROMOTION);
    }

    public void callWebService_ForApp_Service(String regID) {
        RequestParams params = new RequestParams();
        params.put(MOBILE_No, AppPreference.getMyMobileNo(mContext));
        params.put(SIM_ID, Utils.simSerialNumber(mContext));
        params.put(BUNDLE_IMEI, Utils.getDeviceIMEI(mContext));
        params.put(GCM_ID, regID);
        //REQUEST_ADDRESSS = REQUEST_TYPE_GCM_REGISTRATION_APPSERVER;
        callAppserverInterface.callRequestAPIAppServer(BASE_APP_URL + REQUEST_TYPE_GCM_APPSERVER, params, REQUEST_TYPE_GCM_REGISTRATION_APPSERVER);
    }

    public void callWebServiceForGCM(String comments, String appKey, String regID) {
        Bundle params = new Bundle();
        params.putString(AGENT_CODE_GCM, AppPreference.getMyMobileNo(mContext));
        /*params.putString(VENDOR_CODE_GCM, "IPAY");
        params.putString(CLIENT_TYPE_GCM, "GPRS");*/
        params.putString(COMMENTS, "LOGIN");
        params.putString(PIN, AppPreference.getPasword(mContext));
        params.putString(APP_KEY, appKey);
        params.putString(REG_ID, regID);
        params.putString(PARAM_TOKEN, AppPreference.getAuthTokem(mContext));
        params.putString(DEVICE_TYPE, "android");
        callRequestInterface.callRequestAPI(BASE_URL + ";" + REQUEST_TYPE_GCM, params, REQUEST_TYPE_GCM_REGISTRATION);
        Log.i("Gcm", "callWebServiceForGCM: Gcm Server call");
    }


    public void callRetriveProfile(String mobileNumber, String simid) {
        RequestParams params = new RequestParams();
        params.put(MOBILE_NUMBER_RETRIEVE, mobileNumber);
        //params.put(SIM_ID, "89950101523835648915");
        params.put(SIM_ID, simid);
        callAppserverInterface.callRequestAPIAppServer(BASE_APP_URL + HTTP_URL_RETRIEVE_PROFILE, params, REQUEST_TYPE_RETRIVE_PROFILE);
    }

}
