package mm.dummymerchant.sk.merchantapp.httpConnection;

/**
 * Created by abhishekmodi on 19/10/15.
 */
public interface HttpInterface {

    public abstract <T> void asyncResponse(int resultCode, T data);
    public abstract <T> void asyncResponseFail(int resultCode, T data);
}
