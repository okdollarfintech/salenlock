package mm.dummymerchant.sk.merchantapp.httpConnection;

import android.content.Context;
import android.content.res.AssetManager;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.google.gson.Gson;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManagerFactory;

import mm.dummymerchant.sk.merchantapp.R;
import mm.dummymerchant.sk.merchantapp.Utils.Constant;

/**
 * Created by abhishekmodi on 01/02/16.
 */
public class HttpNewRequest extends Thread implements Constant, HostnameVerifier {


    RequestProgressDialog mProgDailog;
    String response;
    Context mContext;
    LoginApiListener listener;
    Object login;
    int resultCode=-1;


    public HttpNewRequest(Object objects, Context mContext, LoginApiListener listener, int resultCODE)
    {
        this.login = objects;
        this.listener = listener;
        this.mContext = mContext;
        this.resultCode=resultCODE;
        showDialog();
    }


    private void showDialog()
    {
        mProgDailog = new RequestProgressDialog(mContext);
        mProgDailog.setMessage(mContext.getResources().getString(R.string.loading_msg));
        mProgDailog.setCancelable(true);
        mProgDailog.setCanceledOnTouchOutside(false);
        mProgDailog.show();
    }

    public interface LoginApiListener {
        void responseListener(String res, int resultCode);
    }

    @Override
    public void run() {
        super.run();
        //Calling Metod here
        Log.v("API", new Gson().toJson(login).toString());
        System.out.println("test param-->"+new Gson().toJson(login).toString());
        switch (resultCode) {
            case APP_SERVER_PARAMS_LOGIN:

                response = sendSerializableData(Constant.LOGIN_BY_APP, new Gson().toJson(login).toString());
                System.out.println(response);
                calingHandler.sendEmptyMessage(0);
                break;
            case APP_SERVER_PARAMS_VERIFY:
                Log.e("Tets", "enter verify");
                response = sendSerializableData(Constant.VERIFY_BY_APP, new Gson().toJson(login).toString());
                System.out.println(response);
                calingHandler.sendEmptyMessage(0);
                break;
            case Constant.APP_SERVER_UPDATE_PROFILE:
                Log.e("UpdateProfile", login.toString());
                response = sendSerializableData(Constant.UPDATE_DUMMY_PROFILE, new Gson().toJson(login).toString());
                System.out.println(response);
                calingHandler.sendEmptyMessage(0);
                break;
            case Constant.APP_SERVER_CREATE_PROFILE:
                response = sendSerializableData(Constant.DUMMY_PROFILE, new Gson().toJson(login).toString());
                System.out.println(response);
                calingHandler.sendEmptyMessage(0);
                break;
            case Constant.REQUEST_TYPE_RETRIVE_PROFILE:
                response = sendSerializableData(Constant.BASE_APP_URL + Constant.HTTP_URL_RETRIEVE_PROFILE, new Gson().toJson(login).toString());
                System.out.println(response);
                calingHandler.sendEmptyMessage(0);
                break;

            case Constant.REQUEST_TYPE_LANGUAGE:
                response = sendSerializableData(Constant.BASE_APP_URL + Constant.URL_LANGUAGE, new Gson().toJson(login).toString());
                System.out.println("Change Language BASE_APP_URL:"+Constant.BASE_APP_URL + Constant.URL_LANGUAGE);
                System.out.println("Change Language:"+response);
                calingHandler.sendEmptyMessage(0);
                break;
            default:
                break;
        }

    }

    private Handler calingHandler= new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {

            if (mProgDailog != null) {
                mProgDailog.dismiss();
            }
            listener.responseListener(response,resultCode);
            return false;
        }
    });


    public String sendSerializableData(String url, String request) {

        try {
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            AssetManager asstManager = mContext.getAssets();
            InputStream is = null;
            try {
                is = asstManager.open("4e7d8d1360f312be.crt");
            } catch (IOException e) {
                e.printStackTrace();
            }
            InputStream caInput = new BufferedInputStream(is);
            Certificate ca;
            try {
                ca = cf.generateCertificate(caInput);
                System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
            } finally {
                caInput.close();
            }
// Create a KeyStore containing our trusted CAs
            String keyStoreType = KeyStore.getDefaultType();
            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
            keyStore.load(null, null);
            keyStore.setCertificateEntry("ca", ca);

// Create a TrustManager that trusts the CAs in our KeyStore
            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
            tmf.init(keyStore);

// Create an SSLContext that uses our TrustManager
            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, tmf.getTrustManagers(), null);

            URL url1 = new URL(url);
            HttpURLConnection conn= (HttpURLConnection)url1.openConnection();

            if(conn instanceof HttpsURLConnection) {

                ((HttpsURLConnection)conn).setHostnameVerifier(this);
                //HttpsURLConnection.setDefaultHostnameVerifier(this);
                ((HttpsURLConnection)conn).setSSLSocketFactory(context.getSocketFactory());
            }
            Log.v("API", conn.toString());
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setDoOutput(true);
            conn.setConnectTimeout(10000);
            Log.v("API", conn.toString());
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(request);
            wr.flush();
            wr.close();

            int responseCode = conn.getResponseCode();
            Log.e("API", "resultCode" + responseCode);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(conn.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            Log.e("API", "response" + response.toString());
            return response.toString();

        } catch (Exception e) {
            Log.v("API", e.toString());
            if (mProgDailog != null) {
                mProgDailog.dismiss();
            }
            return "{\n" +
                    "  \t\"Code\": \"-1\",\n" +
                    "\t\"Msg\": \"No Active Internet Connection\"\n" +
                    "}";
        }

    }

    @Override
    public boolean verify(String hostname, SSLSession session) {
        HostnameVerifier hv =
                HttpsURLConnection.getDefaultHostnameVerifier();
        return hv.verify(hostname, session);
    }
}
