package mm.dummymerchant.sk.merchantapp.httpConnection;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import mm.dummymerchant.sk.merchantapp.BaseActivity;
import mm.dummymerchant.sk.merchantapp.NewRegistrationActivity1;
import mm.dummymerchant.sk.merchantapp.R;
import mm.dummymerchant.sk.merchantapp.Utils.Constant;
import mm.dummymerchant.sk.merchantapp.db.DBHelper;
import mm.dummymerchant.sk.merchantapp.model.Profile;

/**
 * Created by abhishekmodi on 30/10/15.
 */
public class MyLoginApi extends AsyncTask implements Constant {


    public interface LoginApiListener {
        void responseListener(String res, int resultCode);
    }

    Context mContext;
    LoginApiListener listener;
    Object login;
    int resultCode=-1;
    StringBuffer responses;

    public MyLoginApi(Object objects, Context mContext, LoginApiListener listener, int resultCODE) {
        this.login = objects;
        this.listener = listener;
        this.mContext = mContext;
        this.resultCode=resultCODE;
    }

    RequestProgressDialog mProgDailog;
    String response;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mProgDailog = new RequestProgressDialog(mContext);
        mProgDailog.setMessage(mContext.getResources().getString(R.string.loading_msg));
        mProgDailog.setCancelable(false);
        mProgDailog.show();

    }

    @Override
    protected Object doInBackground(Object[] objects) {

        switch (resultCode)
        {
            case Constant.APP_SERVER_CREATE_PROFILE:
                Log.v("API", new Gson().toJson(login).toString());
                response = sendSerializableData(Constant.DUMMY_PROFILE, new Gson().toJson(login).toString());
                System.out.println(response);
                break;

            case Constant.APP_SERVER_UPDATE_PROFILE:
                Log.v("API", new Gson().toJson(login).toString());
                response = sendSerializableData(Constant.UPDATE_DUMMY_PROFILE, new Gson().toJson(login).toString());
                System.out.println(response);
                break;

        }

       /* switch (resultCode)
        {
            case APP_SERVER_PARAMS_LOGIN:
                response = sendSerializableData(Constant.LOGIN_BY_APP, new Gson().toJson(login).toString());
                System.out.println(response);
                break;
            case APP_SERVER_PARAMS_VERIFY:
                Log.e("Tets", "enter verify");
                response = sendSerializableData(Constant.VERIFY_BY_APP, new Gson().toJson(login).toString());
                System.out.println(response);
                break;
            case Constant.APP_SERVER_UPDATE_PROFILE:
                response = sendSerializableData(Constant.BASE_APP_URL+Constant.UPDATE_PROFILE_AFTER, new Gson().toJson(login).toString());
                System.out.println(response);
                break;
            case Constant.APP_SERVER_CREATE_PROFILE:
                response = sendSerializableData(Constant.BASE_APP_URL+Constant.CREATE_PROFILE, new Gson().toJson(login).toString());
                System.out.println(response);
                break;
            case 200:
                response = sendSerializableData(Constant.DUMMY_PROFILE, new Gson().toJson(login).toString());
                System.out.println(response);
                break;
            default:
                break;
        }*/
        return null;
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        if (mProgDailog != null) {
            mProgDailog.dismiss();
        }
        try {
            if(resultCode==Constant.APP_SERVER_UPDATE_PROFILE) {
                JSONObject object = new JSONObject(responses.toString());
                if (object.getString("Code").equals("200") && object.getString("Msg").equals("success")) {
                    DBHelper db = new DBHelper(mContext);
                    db.deleteProfile();
                    db.insertProfile((Profile) login);
                    Toast.makeText(mContext, "Profile Updated Sucussesfully", Toast.LENGTH_LONG).show();
                    NewRegistrationActivity1 rew = (NewRegistrationActivity1) mContext;
                    rew.finish();
                }
            }
        }catch (Exception e)
        {}
        listener.responseListener(response,resultCode);
    }

    public String sendSerializableData(String url, String request) {

        try {
            URL url1 = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) url1.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(request);
            wr.flush();
            wr.close();

            int responseCode = conn.getResponseCode();
            Log.e("API", "resultCode" + responseCode);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(conn.getInputStream()));
            String inputLine;
            responses = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                responses.append(inputLine);
            }
            in.close();
            Log.e("API", "response" + responses.toString());
            return responses.toString();
        } catch (Exception e) {
            //return e.toString();
            if (mProgDailog != null) {
                mProgDailog.dismiss();
            }

            return "{\n" +
                    "  \t\"Code\": \"-1\",\n" +
                    "\t\"Msg\": \"No Active Internet Connection\"\n" +
                    "}";
        }

    }

}
