package mm.dummymerchant.sk.merchantapp.httpConnection;

import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mm.dummymerchant.sk.merchantapp.R;
import mm.dummymerchant.sk.merchantapp.SliderScreen;
import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.Constant;
import mm.dummymerchant.sk.merchantapp.Utils.GPSLocationResponse;
import mm.dummymerchant.sk.merchantapp.Utils.Utils;
import mm.dummymerchant.sk.merchantapp.Utils.WriteToExcel;
import mm.dummymerchant.sk.merchantapp.db.DBHelper;
import mm.dummymerchant.sk.merchantapp.model.BalanceObject;
import mm.dummymerchant.sk.merchantapp.model.CashierModel;
import mm.dummymerchant.sk.merchantapp.model.ConstactModel;
import mm.dummymerchant.sk.merchantapp.model.GCM_RegistrationModel;

import mm.dummymerchant.sk.merchantapp.model.GPS_Location;
import mm.dummymerchant.sk.merchantapp.model.LocationModel;
import mm.dummymerchant.sk.merchantapp.model.LoginModel;
import mm.dummymerchant.sk.merchantapp.model.OTPModel;
import mm.dummymerchant.sk.merchantapp.model.Profile;
import mm.dummymerchant.sk.merchantapp.model.TransationInfoModel;
import mm.dummymerchant.sk.merchantapp.model.TransationModel;


/**
 * Created by abhishekmodi on 13/10/15.
 */
public class NewAsycnTask implements Constant, DialogInterface.OnCancelListener {

    Context mContext;
    public AsyncHttpClient client = new AsyncHttpClient();
    public RequestProgressDialog mProgDailog;
    private static int REQUEST_ADDRESS = 0;
    private AsyncHttpClient mNewTaskEstelServer = new AsyncHttpClient();
    private HttpInterface httpResponse = null;
    private CallAppServerInterface appServerInterface;
    DBHelper db;

    //ToDo Asycn task constratror
    public NewAsycnTask(Context mContext, HttpInterface listener, int address) {
        this.mContext = mContext;
        db = new DBHelper(mContext);
        httpResponse = listener;
        REQUEST_ADDRESS = address;
    }


    public AsyncHttpResponseHandler handlerEstel = new AsyncHttpResponseHandler() {

        @Override
        public void onCancel() {
            super.onCancel();
        }

        @Override
        public void onStart() {
            super.onStart();
            try
            {
            if (REQUEST_ADDRESS == REQUEST_TYPE_GCM_REGISTRATION_APPSERVER || REQUEST_ADDRESS == REQUEST_TYPE_UPLOAD_DEVICE || REQUEST_ADDRESS == REQUEST_TYPE_ENCRYPTED_KEY || REQUEST_ADDRESS == 19292 || REQUEST_ADDRESS == 100030 || REQUEST_ADDRESS == REQUEST_TYPE_GET_CONTACTS) {


            } else if (mProgDailog == null) {

                mProgDailog = new RequestProgressDialog(mContext, mContext.getString(R.string.loading_msg), REQUEST_ADDRESS);
                mProgDailog.setOnCancelListener(NewAsycnTask.this);
                mProgDailog.setIndeterminate(true);

                    mProgDailog.show();
                }
            }

                catch (Exception e)
                {
                    e.printStackTrace();
                }


        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

            try {
                String response = new String(responseBody, "UTF-8");
                Log.e("requestaddress_NewAsync", "" + REQUEST_ADDRESS);
                Log.e("response NewAsync", "" + response);

                if (response.contains("<resultcode>0</resultcode>"))
                {

                    switch (REQUEST_ADDRESS) {

                        case REQUEST_TYPE_ENCRYPTED_KEY:
                            System.out.println(response);
                            httpResponse.asyncResponse(REQUEST_TYPE_ENCRYPTED_KEY, response);
                            dismissDialog();
                            break;

                        case REQUEST_TYPE_RECEIVED_TRANSCATION:

                            DBHelper dbb=new DBHelper(mContext);
                            dbb.open();


                            TransationInfoModel transinfo2 = new TransationInfoModel(response);

                            ArrayList<TransationModel> mTransationModel2 = transinfo2.getmTransationModel();
                            if (mTransationModel2.size() > 0)
                                AppPreference.setDate(mContext, Utils.getTimeFormat());

                            for (int j = 0; j < mTransationModel2.size(); j++)
                            {
                                if(AppPreference.getMasterOrCashier(mContext).equalsIgnoreCase(MASTER))
                                {
                                    TransationModel t=mTransationModel2.get(j);

                                    t.setCashierId(MASTER_TAG);
                                    t.setCashierName(MASTER_TAG);
                                    t.setCashierNumber(MASTER_TAG);

                                    DBHelper.insertTransation(t);
                                    if (mTransationModel2.get(j).getIsCredit().equals("Cr"))
                                    {
                                        DBHelper.insertRECEIVEDTransacation(mTransationModel2.get(j));
                                    }
                                }

                                else {
                                    TransationModel t=mTransationModel2.get(j);

                                    String cashierId= SliderScreen.present_Cashier_Id;

                                    CashierModel csm=dbb.getCashierModel(cashierId);

                                    t.setCashierId(cashierId);
                                    t.setCashierName(csm.getCashier_Name());
                                    t.setCashierNumber(csm.getCashier_Number());

                                    DBHelper.insertTransation(t);

                                    if (mTransationModel2.get(j).getIsCredit().equals("Cr")) {
                                       DBHelper.insertRECEIVEDTransacation(mTransationModel2.get(j));
                                    }
                                }

                            }
                            WriteToExcel wf=new WriteToExcel();//for writing activity log on the  excel file
                            wf.convertToExcelFile(mContext);

                            httpResponse.asyncResponse(REQUEST_TYPE_RECEIVED_TRANSCATION, transinfo2);
                            dismissDialog();
                            dbb.close();
                            break;

                        case REQUEST_TYPE_TRANSINFO_NEW:

                            DBHelper dbb1=new DBHelper(mContext);
                            dbb1.open();
                            TransationInfoModel transinfo1 = new TransationInfoModel(response);
                            ArrayList<TransationModel> mTransationModel1 = transinfo1.getmTransationModel();
                            for (int j = 0; j < mTransationModel1.size(); j++) {

                                TransationModel t = mTransationModel1.get(j);
                                String number = "";
                                if (t.getDestination().startsWith("+95")) {
                                    number = number.replace("+95", "");
                                }

                                number = t.getDestination().substring(1);
                            //  ConstactModel contactModel = db.getSelectedContact(number);
                                ///////
                                List<ConstactModel> list = new ArrayList<ConstactModel>();
                                Utils utils=new Utils();
                                String jsondata=DBHelper.getJsonContacts();
                                list=utils.getContactListFromJsonString(jsondata);
                                ////////////
                                ConstactModel contactModel = utils.getSelectedContactModel(number,list);
                                Log.e("cname","..."+contactModel.getName());
                                if (!contactModel.getName().equals("")) {
                                    t.setDestinationPersonName(contactModel.getName());
                                    t.setPhotoUrl(contactModel.getPhotoUrl());
                                }

                                if(AppPreference.getMasterOrCashier(mContext).equalsIgnoreCase(MASTER))
                                {

                                    t.setCashierId(MASTER_TAG);
                                    t.setCashierName(MASTER_TAG);
                                    t.setCashierNumber(MASTER_TAG);

                                    DBHelper.insertTransation(t);
                                    if (t.getIsCredit().equals("Cr")) {

                                        DBHelper.insertRECEIVEDTransacation(mTransationModel1.get(j));
                                    }
                                }

                                else
                                {

                                    CashierModel cm=dbb1.getCashierModel(SliderScreen.present_Cashier_Id);

                                    t.setCashierId(SliderScreen.present_Cashier_Id);
                                    t.setCashierName(cm.getCashier_Name());
                                    t.setCashierNumber(cm.getCashier_Number());

                                    DBHelper.insertTransation(t);
                                    if (t.getIsCredit().equals("Cr"))
                                    {
                                        String id=SliderScreen.present_Cashier_Id;
                                        /*  mTransationModel1.get(j).setCashierId(id);
                                          DBHelper.insertRECEIVEDTransacation(mTransationModel1.get(j));*/
                                        DBHelper.insertRECEIVEDTransacation(mTransationModel1.get(j));
                                    }

                                }

                            }
                            WriteToExcel wff=new WriteToExcel();//for writing activity log on the  excel file
                            wff.convertToExcelFile(mContext);

                            dismissDialog();

                            httpResponse.asyncResponse(REQUEST_TYPE_TRANSINFO_NEW, transinfo1);
                            dbb1.close();
                            break;
                        case REQUEST_TYPE_LOGIN:
                            dismissDialog();
                            LoginModel login = new LoginModel(response);
                            httpResponse.asyncResponse(REQUEST_TYPE_LOGIN, login);
                            break;

                        case REQUEST_TYPE_OTP:
                            dismissDialog();
                            OTPModel otp = new OTPModel(response);
                            httpResponse.asyncResponse(REQUEST_TYPE_OTP, otp);
                            break;
                        ///
                        case REQUEST_TYPE_LOGOUT:
                            dismissDialog();
                            TransationModel logout = new TransationModel(response);
                            httpResponse.asyncResponse(REQUEST_TYPE_LOGOUT, logout);
                            break;

                        case REQUEST_TYPE_GCM_REGISTRATION:
                            dismissDialog();
                            GCM_RegistrationModel modelgcm = new GCM_RegistrationModel(response);
                            httpResponse.asyncResponse(REQUEST_TYPE_GCM_REGISTRATION, modelgcm);
                            break;

                      case REQUEST_TYPE_LANGUAGE:
                            dismissDialog();
                            BalanceObject language = new BalanceObject(response);
                            httpResponse.asyncResponse(REQUEST_TYPE_LANGUAGE, language);
                            break;

                        case REQUEST_TYPE_CHANGE_PIN:
                            dismissDialog();
                            BalanceObject modelpin = new BalanceObject(response);
                            httpResponse.asyncResponse(REQUEST_TYPE_CHANGE_PIN, modelpin);
                            break;

                        case REQUEST_TYPE_ADD_EMAIL_SETTINGS:
                            httpResponse.asyncResponse(REQUEST_TYPE_ADD_EMAIL_SETTINGS, response);
                            dismissDialog();
                            break;

                        case REQUEST_TYPE_ADD_SMS_SETTINGS:
                            httpResponse.asyncResponse(REQUEST_TYPE_ADD_SMS_SETTINGS, response);
                            dismissDialog();
                            break;

                        default:
                            break;
                    }

                } else {
                    dismissDialog();
                    httpResponse.asyncResponseFail(REQUEST_ADDRESS, response);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error)
        {
            {
                String statusCodes = statusCode + "";
                dismissDialog();
                if (error.getMessage() != null)
                    if (error.getMessage().equals("Connect to .. timed out")) {
                        Utils.showCustomToastMsg(mContext, R.string.api_error3, false);
                    }
                // When Http response code is '404'
                if (statusCodes.startsWith("4")) {
                    Utils.showCustomToastMsg(mContext, R.string.api_error, false);
                }
                // When Http response code is '500'
                else if (statusCodes.startsWith("5")) {
                    Utils.showCustomToastMsg(mContext, R.string.api_error1, false);
                }
                // When Http response code other than 404, 500
                else {
                    Utils.showCustomToastMsg(mContext, R.string.api_error2, false);
                }
            }

        }
    };


    @Override
    public void onCancel(DialogInterface dialog) {
        if (mProgDailog != null && mProgDailog.isShowing())
            dismissDialog();
    }

    void dismissDialog() {
        try {
            if (mProgDailog != null && mProgDailog.isShowing())
                mProgDailog.dismiss();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }



    AsyncHttpResponseHandler mAppServerHandler = new AsyncHttpResponseHandler() {

        @Override
        public void onStart() {
            super.onStart();


            if (REQUEST_ADDRESS == REQUEST_TYPE_GCM_REGISTRATION_APPSERVER || REQUEST_ADDRESS == REQUEST_TYPE_UPLOAD_DEVICE || REQUEST_ADDRESS == REQUEST_TYPE_ENCRYPTED_KEY || REQUEST_ADDRESS == 19292 || REQUEST_ADDRESS == 100030 || REQUEST_ADDRESS == REQUEST_TYPE_GET_CONTACTS) {


            } else if (mProgDailog != null) {

                mProgDailog = new RequestProgressDialog(mContext, mContext.getString(R.string.loading_msg), REQUEST_ADDRESS);
                mProgDailog.setOnCancelListener(NewAsycnTask.this);
                mProgDailog.setIndeterminate(true);
                mProgDailog.show();
            }
        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {


            try {
                String response = new String(responseBody, "UTF-8");
                Log.d("RESPONSE--", response);
                //response = response.replace("<string xmlns=\"http://schemas.microsoft.com/2003/10/Serialization/\">", "");
                //response = response.replace("</string>", "");
                System.out.println("--" + response);
                switch (REQUEST_ADDRESS) {

                    case REQUEST_TYPE_OTP:
                        dismissDialog();
                        OTPModel otp = new OTPModel(response);
                        httpResponse.asyncResponse(REQUEST_TYPE_OTP, otp);
                        break;


                    case REQUEST_TYPE_TRANSACTION:
                        dismissDialog();
                        JSONTokener tokener = new JSONTokener(response);
                        JSONObject object = new JSONObject(tokener);
                        JSONObject status = object.getJSONObject("status");
                        DBHelper db = new DBHelper(mContext);
                        dismissDialog();
                        break;

                    default:
                        dismissDialog();
                        break;
                }

            } catch (Exception e) {

                e.printStackTrace();

            }
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            dismissDialog();
            //Toast.makeText(mContext,"rece failurecalled",Toast.LENGTH_LONG).show();
            if (statusCode == 404) {
               // showToast(R.string.failure_msg1);
            } else if (statusCode == 500) {
                //   showToast(getString(R.string.failure_msg2));
            } else {
            }
        }
    };


    boolean getFormatteddate(String transDate,String createDate)
    {
        boolean status=false;

        DateFormat targetFormat = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss");

        try {
            Date d1=  targetFormat.parse(transDate);
            Date d2=targetFormat.parse(createDate);

            if(d1.after(d2))
            {
                status=true;
                return status;
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }


        return status;
    }

    public AsyncHttpResponseHandler handlerAppServer= new AsyncHttpResponseHandler() {

        @Override
        public void onStart() {
            super.onStart();
            try {
                if (REQUEST_ADDRESS == REQUEST_TYPE_GCM_REGISTRATION_APPSERVER || REQUEST_ADDRESS == REQUEST_TYPE_UPLOAD_DEVICE || REQUEST_ADDRESS == REQUEST_TYPE_ENCRYPTED_KEY || REQUEST_ADDRESS == 19292 || REQUEST_ADDRESS == 100030 || REQUEST_ADDRESS == REQUEST_TYPE_GET_CONTACTS ||  REQUEST_ADDRESS == REQUESTTYPE_GPS_CELLID ) {


                } else if (mProgDailog == null) {
                    mProgDailog = new RequestProgressDialog(mContext, mContext.getString(R.string.loading_msg), REQUEST_ADDRESS);
                    mProgDailog.setOnCancelListener(NewAsycnTask.this);
                    mProgDailog.setIndeterminate(true);
                     mProgDailog.show();
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

            try {
                String response = new String(responseBody, "UTF-8");
                Log.d("RESPONSE--", response);
                System.out.println("--" + response);
                switch (REQUEST_ADDRESS) {


                    case REQUEST_TYPE_ADD_SHOPS:
                        httpResponse.asyncResponse(REQUEST_TYPE_ADD_SHOPS, response);
                        dismissDialog();
                        break;
                    case REQUEST_TYPE_DELETE_SHOPS:
                        httpResponse.asyncResponse(REQUEST_TYPE_DELETE_SHOPS, response);
                        dismissDialog();
                        break;
                    case REQUEST_CHECK_STATUS_SHOPS:
                        httpResponse.asyncResponse(REQUEST_CHECK_STATUS_SHOPS, response);
                        dismissDialog();
                        break;

                    case REQUEST_TYPE_GCM_REGISTRATION_APPSERVER:
                        Log.d("reg:: app server--", response);

                        try {
                          JSONObject  jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("Code");
                            String Msg = jsonObject.getString("Msg");
                            if (status.equals("200")) {
                                Log.e("Status", "Code 200");
                                if (jsonObject.has("Data")) {
                                    Log.e("Status", "Has Data");
                                    String data = jsonObject.getString("Data");
                                    JSONObject jsonObject_table = new JSONObject(data);
                                    System.out.println("status--" + status + "--" + Msg);

                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        httpResponse.asyncResponse(REQUEST_TYPE_GCM_REGISTRATION_APPSERVER, response);
                        dismissDialog();

                        break;

                    case REQUEST_VIEW_PROMOTION:
                        response = response.replace("<string xmlns=\"http://schemas.microsoft.com/2003/10/Serialization/\">", "");
                        response = response.replace("</string>", "");
                        httpResponse.asyncResponse(REQUEST_VIEW_PROMOTION, response);
                        dismissDialog();
                        break;
                    case REQUEST_DELETE_PROMOTION:
                        response = response.replace("<string xmlns=\"http://schemas.microsoft.com/2003/10/Serialization/\">", "");
                        response = response.replace("</string>", "");
                        httpResponse.asyncResponse(REQUEST_DELETE_PROMOTION, response);
                        dismissDialog();
                        break;

                    case REQUEST_ADD_PROMOTION:
                        dismissDialog();
                        response = response.replace("<string xmlns=\"http://schemas.microsoft.com/2003/10/Serialization/\">", "");
                        response = response.replace("</string>", "");
                        httpResponse.asyncResponse(REQUEST_ADD_PROMOTION, response);

                    case REQUEST_TYPE_RETRIVE_PROFILE:
                        response = response.replace("<string xmlns=\"http://schemas.microsoft.com/2003/10/Serialization/\">", "");
                        response = response.replace("</string>", "");
                        Profile profile = new Profile();
                        profile = profile.getDataIntoModel(response, mContext);
                        httpResponse.asyncResponse(REQUEST_TYPE_RETRIVE_PROFILE, profile);
                        dismissDialog();
                        break;

                    case REQUEST_TYPE_MAP_LAT_LONG:
                        dismissDialog();
                        response = response.replace("<string xmlns=\"http://schemas.microsoft.com/2003/10/Serialization/\">", "");
                        response = response.replace("</string>", "");
                        LocationModel locationModel = new LocationModel(response);
                        httpResponse.asyncResponse(REQUEST_TYPE_MAP_LAT_LONG, locationModel);

                        break;

                    case REQUEST_TYPE_OTP:
                        dismissDialog();
                        OTPModel otp = new OTPModel(response);
                        httpResponse.asyncResponse(REQUEST_TYPE_OTP, otp);
                        break;

                    case REQUESTTYPE_GPS_CELLID:
                        GPS_Location gpsLocation = new GPS_Location();
                        gpsLocation.setGPSLocation(response);
                        GPSLocationResponse gpsLocationResponse = (GPSLocationResponse) mContext;
                        gpsLocationResponse.onSuccess(gpsLocation.getGpsLocation());
                        //showToast(response);
                        break;

                    default:
                        dismissDialog();
                        break;
                }

            } catch (Exception e) {

                e.printStackTrace();

            }

        }

        @Override
        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            String statusCodes= statusCode+"";
            System.out.println("statusCodes : " +statusCodes);
            dismissDialog();
            if(error.getMessage()!=null)
                if ( error.getMessage().equals("Connect to .. timed out")) {
                    Utils.showCustomToastMsg(mContext,R.string.api_error3, false);

                }
            // When Http response code is '404'
            if (statusCodes.startsWith("4")) {
                Utils.showCustomToastMsg(mContext, R.string.api_error, false);
            }
            // When Http response code is '500'
            else if (statusCodes.startsWith("5")) {
                Utils.showCustomToastMsg(mContext, R.string.api_error1, false);
            }
            // When Http response code other than 404, 500
            else {
                Utils.showCustomToastMsg(mContext, R.string.api_error2, false);
            }

        }

    };


}
