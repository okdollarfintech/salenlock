package mm.dummymerchant.sk.merchantapp.httpConnection;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.ImageView;

import mm.dummymerchant.sk.merchantapp.R;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;


public class RequestProgressDialog extends ProgressDialog
{
	public int reqCode = 0;
	public String message;
	Context mContext;
	ImageView loadingImage;
	public CustomTextView progress_dialog_circle;

	public RequestProgressDialog(Context context) {
		super(context, R.style.NewDialog);
	}
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.progress_dialog_layout);
		//loadingImage = (ImageView) findViewById(R.id.progress_image_loading);
		progress_dialog_circle=(CustomTextView) findViewById(R.id.text_id);
	}

	public RequestProgressDialog(Context context, int requesCode)
	{
		super(context);
		this.mContext = context;
		reqCode = requesCode;
		setCancelable(true);
		setCanceledOnTouchOutside(false);
	}

	public RequestProgressDialog(Context context, String message, int requesCode)
	{
		super(context, R.style.NewDialog);
		this.mContext = context;
		reqCode = requesCode;
		this.message = message;
		setCancelable(true);
		setCanceledOnTouchOutside(false);
	}


}
