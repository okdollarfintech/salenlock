package mm.dummymerchant.sk.merchantapp.httpConnection;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import mm.dummymerchant.sk.merchantapp.R;
import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.ContactResponse;
import mm.dummymerchant.sk.merchantapp.Utils.Utils;

/**
 * Created by Keerthivasan on 28-Apr-18.
 */

public class StoreOkContacts extends AsyncTask<String, Void, String> {

    public static final String REQUEST_METHOD = "GET";
    public static final int READ_TIMEOUT = 15000;
    public static final int CONNECTION_TIMEOUT = 15000;

    private final Context mContext;

    private Gson gson;

    public StoreOkContacts(Context mContext) {
        this.mContext = mContext;
        gson = new Gson();
    }


    @Override
    protected String doInBackground(String... params) {

        String operator = getOperator();
        String stringUrl = "http://www.okdollar.co/RestService.svc/GetMessageReadingMobileNumbers?Teleco=" + operator;
        String result;
        String inputLine;

        try {
            //Create a URL object holding our url
            URL myUrl = new URL(stringUrl);

            //Create a connection
            HttpURLConnection connection = (HttpURLConnection)
                    myUrl.openConnection();

            //Set methods and timeouts
            connection.setRequestMethod(REQUEST_METHOD);
            connection.setReadTimeout(READ_TIMEOUT);
            connection.setConnectTimeout(CONNECTION_TIMEOUT);

            //Connect to our url
            connection.connect();

            //Create a new InputStreamReader
            InputStreamReader streamReader = new
                    InputStreamReader(connection.getInputStream());

            //Create a new buffered reader and String Builder
            BufferedReader reader = new BufferedReader(streamReader);
            StringBuilder stringBuilder = new StringBuilder();

            //Check if the line we are reading is not null
            while ((inputLine = reader.readLine()) != null) {
                stringBuilder.append(inputLine);
            }

            //Close our InputStream and Buffered reader
            reader.close();
            streamReader.close();

            //Set our result equal to our stringBuilder
            result = stringBuilder.toString();
        } catch (IOException e) {
            e.printStackTrace();
            result = null;
        }

        return result;

    }

    /**
     * This method is used to get the mobile operator.
     *
     * As of now, for operator, passing default Telco
     *
     * @return Strings
     */
    private String getOperator() {
        return "Telco";
    }

    @Override
    protected void onPostExecute(String response) {

        super.onPostExecute(response);
        Log.i("Contact", "addOkContactInPhone: "+response);
        ContactResponse contactResponse = gson.fromJson(response, ContactResponse.class);
        if (contactResponse != null && contactResponse.getCode() == 200) {
            AppPreference.setSmsSender(mContext,contactResponse.getData());
            String contacts[] = contactResponse.getData().split(",");

            for (String contact : contacts) {
                if (!contact.isEmpty()) {
                    contact = contact.replace("\"","");
                    addOkContactInPhone(contact);
                }
            }
        }
    }

    public void addOkContactInPhone(String contactPhone) {

        Log.i("Contact", "addOkContactInPhone: "+contactPhone);
        Bitmap mBitMapIcon = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ok_icon);
        String val = Utils.CheckinContact(mContext, contactPhone);
        if (val.equals("null")) {
            Utils.addNumberInContact(mContext, mContext.getResources().getString(R.string.ok_dollar), contactPhone, null, null, mBitMapIcon);
        }
    }
}
