package mm.dummymerchant.sk.merchantapp.introduction;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;

import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;
import mm.dummymerchant.sk.merchantapp.R;


public class CircularProgressBarLayout extends ProgressDialog {

   public static CircularProgressBar cpr;
    Context context;
   public static CustomTextView tv_progress;

    public static CountDownTimer condownTimer;

    public static OnRetriveClickListener onRetrive_refrence;

    int address=-1;

    public CircularProgressBarLayout(Context context, int id) {
        super(context, R.style.Circular);
        setCancelable(true);
        setCanceledOnTouchOutside(false);
        this.context = context;
        address=id;

    }


   @Override
    public void onBackPressed() {
        AppPreference.setOTP(context, "");
        condownTimer.cancel();
        dismiss();
        onRetrive_refrence.backKeyPressed();
    }

    public void Set(OnRetriveClickListener onRetirive)
    {

        if(onRetrive_refrence==null) {
            onRetrive_refrence = onRetirive;
        }
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_circular_progress_bar_layout);
        cpr = (CircularProgressBar) findViewById(R.id.circularprogressbar1);
        tv_progress =(CustomTextView) findViewById(R.id.tv_progress);
        tv_progress.setVisibility(View.VISIBLE);
        tv_progress.setText(context.getResources().getString(R.string.verifying_no));
        timer();
        cpr.setSubTitle("");
        cpr.setProgress(0);

    }


   public  interface OnRetriveClickListener
    {
        public void onRetrive(int id);
        public void backKeyPressed();


    }

    private void timer() {

        condownTimer =   new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {

                cpr.setProgress((int) (30 - (int) millisUntilFinished / 1000));
                cpr.setTitle(String.valueOf((int) millisUntilFinished / 1000));
                cpr.setTitleSize(60f);
                cpr.setEnabled(false);
                tv_progress.setVisibility(View.VISIBLE);

            }

            public void onFinish() {

                cpr.setProgress(100);
                cpr.setTitleSize(40f);
                cpr.setTitle("Try Again");
                cpr.setEnabled(true);
                ///cpr.setSubTitle("Try Again");
                cpr.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        timer();
                        cpr.setSubTitle("");
                        onRetrive_refrence.onRetrive(address);


                    }
                });
                AppPreference.setOTP(context, "");
                tv_progress.setVisibility(View.INVISIBLE);
            }

        };

        condownTimer.start();

    }

}

