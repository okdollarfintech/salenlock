package mm.dummymerchant.sk.merchantapp.maills;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.os.StrictMode;
import android.util.Log;

import java.io.File;

import mm.dummymerchant.sk.merchantapp.Utils.Constant;

/**
 * Created by user on 8/20/2015.
 */
public class MailBroadCast  extends IntentService implements Constant
{
    private static final String TAG = "Mail";
    String temp3="";
    String message="";
    String from_mail="";
    String from_pwd="";
    String mailTo="";

    public MailBroadCast(){
        super("MailBroadCast");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //    sendMail();
        Log.d(TAG, "MailBroadCast Intent onCreate..");

    }
    public IBinder onBind(Intent intent) {
        // Wont be called as service is not bound
        //   Log.i(LOG_TAG, "In onBind");
        Log.d(TAG, "MailBroadCast Intent bind..");
        //   sendMail();
        return null;
    }

    @Override
    protected void onHandleIntent(Intent intent)
    {
        Log.e("mailcalled", "mailcalled");

        Bundle b= intent.getExtras();
        String fromMailId=b.getString(BUNDLE_KEY_FROM_EMAIL_ID);
        String pwd=b.getString(BUNDLE_KEY_PWD);
        String toMailId=b.getString(BUNDLE_KEY_TO_EMAIL_ID);

        sendMail(fromMailId,pwd,toMailId);
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        Log.d(TAG, "MailBroadCast Intent onStart..");

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    private void sendMail(String fromMail,String pwd,String toMailId) {



        from_mail=fromMail;
        from_pwd=pwd;
        mailTo=toMailId;

        Mail m = new Mail(from_mail, from_pwd);
        String[] toArr = { mailTo };
        m._to = toArr;
        m.set_from(mailTo);

        m.set_subject(temp3+"Transaction and Received Money CSV Files");
        m.setBody(temp3+MAIL_BODY_MSG + message);
        try {
            if (android.os.Build.VERSION.SDK_INT > 9) {
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
            Log.i("MailRecord", "Attachments Start");
            File extStore = Environment.getExternalStorageDirectory();
            File myFile = new File(extStore.getAbsolutePath() + "/"+EXCEL_TRANSACTION_FILE_NAME);
            if(myFile.exists())
            {
                m.addAttachment(Environment.getExternalStorageDirectory() + "/"+EXCEL_TRANSACTION_FILE_NAME);
                Log.i("MailRecord", "Attachments 1 Complete");
            }
            File myFile1 = new File(extStore.getAbsolutePath() + "/"+EXCEL_RECEIVED_MONEY_FILE_NAME);
            if(myFile1.exists())
            {
                m.addAttachment(Environment.getExternalStorageDirectory() + "/"+EXCEL_RECEIVED_MONEY_FILE_NAME);
                Log.i("MailRecord", "Attachments 2 Complete");
            }

            File myFile2 = new File(extStore.getAbsolutePath() + "/"+EXCEL_ACTIVITY_LOG_DETAILS);
            if(myFile2.exists())
            {
                m.addAttachment(Environment.getExternalStorageDirectory() + "/"+EXCEL_ACTIVITY_LOG_DETAILS);
                Log.i("MailRecord", "Attachments 2 Complete");
            }

            File myFile3 = new File(extStore.getAbsolutePath() + "/"+EXCEL_CASHIER_TABLE);
            if(myFile3.exists()) {
                m.addAttachment(Environment.getExternalStorageDirectory() + "/"+EXCEL_CASHIER_TABLE);
                Log.i("MailRecord", "Attachments 2 Complete");
            }

            File myFile4 = new File(extStore.getAbsolutePath() + "/"+EXCEL_CASH_COLLECTOR);
            if(myFile4.exists()) {
                m.addAttachment(Environment.getExternalStorageDirectory() + "/"+EXCEL_CASH_COLLECTOR);
                Log.i("MailRecord", "Attachments 2 Complete");
            }
            if (m.send())
            {
                Log.i("MailRecord", "Email was sent successfully.");
                //    deletefiles(toLocation);
                //    deletefiles(filepath);
            } else {
                Log.i("MailRecord", "Email was not sent.");
            }


        } catch (Exception e) {
            Log.i("MailRecord", e.toString());
            Log.e("MailRecord", "Could not send email", e);
        }
    }
}
