package mm.dummymerchant.sk.merchantapp.model;



import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import mm.dummymerchant.sk.merchantapp.Utils.XMLTag;

/**
 * Created by user on 8/28/15.
 */
public class Agent_Info implements XMLTag,Serializable {

    String agentcode="";
    String agentsource="";
    String status="";
    String agentName="";
    String agentLevel="";
    String agentType="";
    String dob="";
    String email="";
    String idProofType="";
    String idProof="";
    String occupassion="";
    String customerstatus="";
    String transid="";
    String businessName="";
    String state="";
    String country="";
    String city="";
    String address1="";
    String language="";
    String car="";
    String accountno1="";
    String accountno2="";
    String msisdn1="";
    String msisdn2="";
    String msisdn3="";
    String msisdn4="";
    String msisdn5="";
    String resulCode="";
    private String gender;
    private int age;


    public Agent_Info(String response) throws XmlPullParserException, IOException
    {
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        XmlPullParser myparser = factory.newPullParser();
        myparser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        InputStream stream = new ByteArrayInputStream(response.getBytes());
        myparser.setInput(stream, null);
        parseXML(myparser);
        stream.close();
    }

    void parseXML(XmlPullParser myParser) throws XmlPullParserException, IOException
    {
        int event;
        String text = null;
        event = myParser.getEventType();
        while (event != XmlPullParser.END_DOCUMENT)
        {
            String name = myParser.getName();
            switch (event) {
                case XmlPullParser.START_TAG:
                    break;
                case XmlPullParser.TEXT:
                    text = myParser.getText();
                    break;
                case XmlPullParser.END_TAG:

                    switch (name){
                        case TAG_AGENT_NAME:
                            setAgentName(text);
                            break;
                        case TAG_AI_AGENT_CODE:
                            setAgentCode(text);
                            break;
                        case TAG_AI_AGENT_SOURCE:
                            setAgentSource(text);
                            break;
                        case TAG_AI_AGENT_LEVEL:
                            setAgentLevel(text);
                            break;
                        case TAG_AI_AGENT_TYPE:
                            setAgentType(text);
                            break;
                        case TAG_RESULTP_CODE:
                            setResulCode(text);
                            break;
                        case TAG_AI_AGENT_BOB:
                            setDob(text);
                            setAge(text);
                            break;
                        case TAG_AI_AGENT_EMAIL:
                            setEmail(text);
                            break;
                        case TAG_AI_AGENT_IDPROOFTYPE:
                            setIdProofType(text);
                            break;
                        case TAG_AI_AGENT_IDPROOF:
                            setIdProof(text);
                            break;
                        case TAG_AI_AGENT_OCCUPASSION:
                            setOccupassion(text);
                            break;
                        case TAG_AI_AGENT_STATUS:
                            setStatus(text);
                            break;
                        case TAG_AI_AGENT_CUSTOMERSTATUS:
                            setCustomerstatus(text);
                            break;
                        case TAG_AI_AGENT_BUSINESSNAME:
                            setBusinessName(text);
                            break;
                        case TAG_AI_AGENT_STATE:
                            setState(text);
                            break;
                        case TAG_AI_AGENT_COUNTRY:
                            setCountry(text);
                            break;
                        case TAG_AI_AGENT_CITY:
                            setCity(text);
                            break;
                        case TAG_AI_AGENT_LANGUAGE:
                            setLanguage(text);
                            break;
                        case TAG_AI_AGENT_TRANSID:
                            setTransid(text);
                            break;
                        case TAG_AI_AGENT_CAR:
                            setCar(text);
                            break;
                        case TAG_AI_AGENT_ADDRESS1:
                            setAddress1(text);
                            break;
                        case TAG_AI_AGENT_ACCOUNTNO1:
                            setAccountno1(text);
                            break;
                        case TAG_AI_AGENT_ACCOUNTNO2:
                            setAccountno2(text);
                            break;
                        case TAG_AI_AGENT_MSISDN1:
                            setMsisdn1(text);
                            break;
                        case TAG_AI_AGENT_MSISDN2:
                            setMsisdn2(text);
                            break;
                        case TAG_AI_AGENT_MSISDN3:
                            setMsisdn3(text);
                            break;
                        case TAG_AI_AGENT_MSISDN4:
                            setMsisdn4(text);
                            break;
                        case TAG_AI_AGENT_MSISDN5:
                            setMsisdn5(text);
                            break;
                        case TAG_AI_AGENT_GENDER:
                            setGender(text);


                    }
/*
                    if (name.equals(TAG_AGENT_NAME))
                        setAgentName(text);
                    else if (name.equals(TAG_AI_AGENT_CODE))
                        setAgentCode(text);
                    else if (name.equals(TAG_AI_AGENT_SOURCE))
                        setAgentSource(text);

                    else if (name.equals(TAG_AI_AGENT_LEVEL))
                        setAgentLevel(text);
                    else if (name.equals(TAG_AI_AGENT_TYPE))
                        setAgentType(text);
                    else if (name.equals(TAG_RESULTP_CODE))
                        setResulCode(text);
                    else if (name.equals(TAG_AI_AGENT_BOB))
                        setDob(text);
                    else if (name.equals(TAG_AI_AGENT_EMAIL))
                        setEmail(text);
                    else if (name.equals(TAG_AI_AGENT_IDPROOFTYPE))
                        setIdProofType(text);
                    else if (name.equals(TAG_AI_AGENT_IDPROOF))
                        setIdProof(text);
                    else if (name.equals(TAG_AI_AGENT_OCCUPASSION))
                        setOccupassion(text);
                    else if (name.equals(TAG_AI_AGENT_STATUS))
                        setStatus(text);
                    else if (name.equals(TAG_AI_AGENT_CUSTOMERSTATUS))
                        setCustomerstatus(text);
                    else if (name.equals(TAG_AI_AGENT_BUSINESSNAME))
                        setBusinessName(text);
                    else if (name.equals(TAG_AI_AGENT_STATE))
                        setState(text);
                    else if (name.equals(TAG_AI_AGENT_COUNTRY))
                        setCountry(text);
                    else if (name.equals(TAG_AI_AGENT_CITY))
                        setCity(text);
                    else if (name.equals(TAG_AI_AGENT_LANGUAGE))
                        setLanguage(text);
                    else if (name.equals(TAG_AI_AGENT_TRANSID))
                        setTransid(text);
                    else if (name.equals(TAG_AI_AGENT_CAR))
                        setCar(text);
                    else if (name.equals(TAG_AI_AGENT_ADDRESS1))
                        setAddress1(text);
                    else if (name.equals(TAG_AI_AGENT_ACCOUNTNO1))
                        setAccountno1(text);
                    else if (name.equals(TAG_AI_AGENT_ACCOUNTNO2))
                        setAccountno2(text);
                    else if (name.equals(TAG_AI_AGENT_MSISDN1))
                        setMsisdn1(text);
                    else if (name.equals(TAG_AI_AGENT_MSISDN2))
                        setMsisdn2(text);
                    else if (name.equals(TAG_AI_AGENT_MSISDN3))
                        setMsisdn3(text);
                    else if (name.equals(TAG_AI_AGENT_MSISDN4))
                        setMsisdn4(text);
                    else if (name.equals(TAG_AI_AGENT_MSISDN5))
                        setMsisdn5(text);
                    break;*/
            }
            event = myParser.next();
        }
    }




    public String getAgentCode() {
        return agentcode;
    }

    public void setAgentCode(String agentcode) {
        this.agentcode = agentcode;
    }
    public String getAgentSource() {
        return agentsource;
    }

    public void setAgentSource(String agentsource) {
        this.agentsource = agentsource;
    }


    public String getResulCode() {
        return resulCode;
    }

    public void setResulCode(String resulCode) {
        this.resulCode = resulCode;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getAgentLevel() {
        return agentLevel;
    }

    public void setAgentLevel(String agentLevel) {
        this.agentLevel = agentLevel;
    }

    public String getAgentType() {
        return agentType;
    }

    public void setAgentType(String agentType) {
        this.agentType = agentType;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdProofType() {
        return idProofType;
    }

    public void setIdProofType(String idProofType) {
        this.idProofType = idProofType;
    }

    public String getIdProof() {
        return idProof;
    }

    public void setIdProof(String idProof) {
        this.idProof = idProof;
    }

    public String getOccupassion() {
        return occupassion;
    }

    public void setOccupassion(String occupassion) {
        this.occupassion = occupassion;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCustomerstatus() {
        return customerstatus;
    }

    public void setCustomerstatus(String customerstatus) {
        this.customerstatus = customerstatus;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getTransid() {
        return transid;
    }

    public void setTransid(String transid) {
        this.transid = transid;
    }

    public String getCar() {
        return car;
    }

    public void setCar(String car) {
        this.car = car;
    }

    public String getAccountno1() {
        return accountno1;
    }

    public void setAccountno1(String accountno1) {
        this.accountno1 = accountno1;
    }

    public String getAccountno2() {
        return accountno2;
    }

    public void setAccountno2(String accountno2) {
        this.accountno2 = accountno2;
    }

    public String getMsisdn1() {
        return msisdn1;
    }

    public void setMsisdn1(String msisdn1) {
        this.msisdn1 = msisdn1;
    }

    public String getMsisdn2() {
        return msisdn2;
    }

    public void setMsisdn2(String msisdn2) {
        this.msisdn2 = msisdn2;
    }

    public String getMsisdn3() {
        return msisdn3;
    }

    public void setMsisdn3(String msisdn3) {
        this.msisdn3 = msisdn3;
    }

    public String getMsisdn4() {
        return msisdn4;
    }

    public void setMsisdn4(String msisdn4) {
        this.msisdn4 = msisdn4;
    }

    public String getMsisdn5() {
        return msisdn5;
    }

    public void setMsisdn5(String msisdn5) {
        this.msisdn5 = msisdn5;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
    public String getGender(){
        return gender;
    }

    public void setAge(String age) {
        String dateOfBirth = age;
        String dateCurrent = getCurrentDate();

        //HH converts hour in 24 hours format (0-23), day calculation
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

        Date d1 = null;
        Date d2 = null;
        int year =0;
        try {
            d1 = format.parse(dateOfBirth);
            d2 = format.parse(dateCurrent);
            //in milliseconds
            long diff = d2.getTime() - d1.getTime();
            long diffDays = diff / (24 * 60 * 60 * 1000);
            year=(int)(diffDays/365);

        } catch (Exception e) {
            e.printStackTrace();
        }
        this.age = year;
    }

    private String getCurrentDate() {
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        return  df.format(Calendar.getInstance().getTime());
    }

    public long getAge() {
        return age;
    }


}
