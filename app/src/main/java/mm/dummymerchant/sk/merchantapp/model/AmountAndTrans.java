package mm.dummymerchant.sk.merchantapp.model;

/**
 * Created by Dell on 6/30/2016.
 */
public class AmountAndTrans
{
    String totalAmount;
    String noOfTransactions;

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getNoOfTransactions() {
        return noOfTransactions;
    }

    public void setNoOfTransactions(String noOfTransactions) {
        this.noOfTransactions = noOfTransactions;
    }



    public AmountAndTrans(String totalAmount, String noOfTransactions) {
        this.totalAmount = totalAmount;
        this.noOfTransactions = noOfTransactions;
    }
}
