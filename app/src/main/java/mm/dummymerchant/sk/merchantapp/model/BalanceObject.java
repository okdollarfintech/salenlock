package mm.dummymerchant.sk.merchantapp.model;



import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

import mm.dummymerchant.sk.merchantapp.Utils.XMLTag;

public class BalanceObject implements XMLTag,Serializable
{
	String responseType = "";
	String agentcode = "";
	String agentname = "";
	String source = "";
	String destination = "";
	String vendorcode = "";
	String amount = "";
	String transid = "";
	String resultcode = "";
	String resultdescription = "";
	String requestcts = "";
	String prewalletbalance = "";
    String kickbackBalance="";


	public BalanceObject(String response) throws XmlPullParserException, IOException
	{
		XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
		XmlPullParser myparser = factory.newPullParser();
		myparser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
		InputStream stream = new ByteArrayInputStream(response.getBytes());
		myparser.setInput(stream, null);
		parseXML(myparser);
		stream.close();
	}

	void parseXML(XmlPullParser myParser) throws XmlPullParserException, IOException
	{
		int event;
		String text = null;
		event = myParser.getEventType();
		while (event != XmlPullParser.END_DOCUMENT)
		{
			String name = myParser.getName();
			switch (event) {
				case XmlPullParser.START_TAG:
					break;
				case XmlPullParser.TEXT:
					text = myParser.getText();
					break;
				case XmlPullParser.END_TAG:
					if (name.equals(TAG_RESPONSE_TYPE))
						setResponseType(text);
					else if (name.equals(TAG_AGENT_NAME))
						setAgentname(text);
					else if (name.equals(TAG_AGENTCODE))
						setAgentcode(text);
					else if (name.equals(TAG_RESULTP_CODE))
						setResultcode(text);
					else if (name.equals(TAG_DESCRIPTION))
						setDestination(text);
					else if (name.equals(TAG_DESTINATION))
						setDestination(text);
					else if (name.equals(TAG_SOURCE))
						setSource(text);
					else if (name.equals(TAG_AMOUNT))
						setAmount(text);
					else if (name.equals(TAG_TRANSID))
						setTransid(text);
                    else if (name.equals(TAG_KICK_BACK_BALANCE))
                        setKickbackBalance(text);
                    else if(name.equals(TAG_RESPONSEACT))
                        setRequestcts(text);
					break;
			}
			event = myParser.next();
		}
	}

    public String getKickbackBalance() {
        return kickbackBalance;
    }

    public void setKickbackBalance(String kickbackBalance) {
        this.kickbackBalance = kickbackBalance;
    }

    public String getResponseType()
	{
		return responseType;
	}

	public void setResponseType(String responseType)
	{
		this.responseType = responseType;
	}

	public String getAgentcode()
	{
		return agentcode;
	}

	public void setAgentcode(String agentcode)
	{
		this.agentcode = agentcode;
	}

	public String getAgentname()
	{
		return agentname;
	}

	public void setAgentname(String agentname)
	{
		this.agentname = agentname;
	}

	public String getSource()
	{
		return source;
	}

	public void setSource(String source)
	{
		this.source = source;
	}

	public String getDestination()
	{
		return destination;
	}

	public void setDestination(String destination)
	{
		this.destination = destination;
	}

	public String getVendorcode()
	{
		return vendorcode;
	}

	public void setVendorcode(String vendorcode)
	{
		this.vendorcode = vendorcode;
	}

	public String getAmount()
	{
		return amount;
	}

	public void setAmount(String amount)
	{
		this.amount = amount;
	}

	public String getTransid()
	{
		return transid;
	}

	public void setTransid(String transid)
	{
		this.transid = transid;
	}

	public String getResultcode()
	{
		return resultcode;
	}

	public void setResultcode(String resultcode)
	{
		this.resultcode = resultcode;
	}

	public String getResultdescription()
	{
		return resultdescription;
	}

	public void setResultdescription(String resultdescription)
	{
		this.resultdescription = resultdescription;
	}

	public String getRequestcts()
	{
		return requestcts;
	}

	public void setRequestcts(String requestcts)
	{
		this.requestcts = requestcts;
	}

	public String getPrewalletbalance()
	{
		return prewalletbalance;
	}

	public void setPrewalletbalance(String prewalletbalance)
	{
		this.prewalletbalance = prewalletbalance;
	}

}