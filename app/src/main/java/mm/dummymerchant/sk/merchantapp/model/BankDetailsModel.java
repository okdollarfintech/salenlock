package mm.dummymerchant.sk.merchantapp.model;

/**
 * Created by kapil on 24/10/15.
 */
public class BankDetailsModel {

    public String AccountNumber = "";
    public String BankName = "";
    public String Branch="";
    public String AccountType="";
    public String BranchAddress="";
    public String IDType="";
    public String IDNo="";
    public String BankPhone="";
    public String BranchAddress2="";
    public String State="";
    public String Township="";
    public int BranchId;
    public int BankId;
    public String BankBurmeseName="";
    public String BranchBurmeseName="";

    public String getBankPhone() {
        return BankPhone;
    }

    public void setBankPhone(String bankPhone) {
        BankPhone = bankPhone;
    }

    public String getIDType() {
        return IDType;
    }

    public void setIDType(String IDType) {
        this.IDType = IDType;
    }

    public String getAccountNumber() {
        return AccountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        AccountNumber = accountNumber;
    }

    public String getBankName() {
        return BankName;
    }

    public void setBankName(String bankName) {
        BankName = bankName;
    }

    public String getBranch() {
        return Branch;
    }

    public void setBranch(String branch) {
        Branch = branch;
    }

    public String getAccountType() {
        return AccountType;
    }

    public void setAccountType(String accountType) {
        AccountType = accountType;
    }

    public String getBranchAddress() {
        return BranchAddress;
    }

    public void setBranchAddress(String branchAddress) {
        BranchAddress = branchAddress;
    }

    public String getIDNo() {
        return IDNo;
    }

    public void setIDNo(String IDNo) {
        this.IDNo = IDNo;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getTownship() {
        return Township;
    }

    public void setTownship(String township) {
        Township = township;
    }

    public String getBranchAddress2() {
        return BranchAddress2;
    }

    public void setBranchAddress2(String branchAddress2) {
        BranchAddress2 = branchAddress2;
    }

    public int getBranchId() {
        return BranchId;
    }

    public void setBranchId(int branchId) {
        BranchId = branchId;
    }

    public int getBankId() {
        return BankId;
    }

    public void setBankId(int bankId) {
        BankId = bankId;
    }

    public String getBankBurmeseName() {
        return BankBurmeseName;
    }

    public void setBankBurmeseName(String bankBurmeseName) {
        BankBurmeseName = bankBurmeseName;
    }

    public String getBranchBurmeseName() {
        return BranchBurmeseName;
    }

    public void setBranchBurmeseName(String branchBurmeseName) {
        BranchBurmeseName = branchBurmeseName;
    }
}
