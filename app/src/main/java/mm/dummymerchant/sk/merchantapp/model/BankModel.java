package mm.dummymerchant.sk.merchantapp.model;

import android.content.Context;
import android.util.Log;



import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import mm.dummymerchant.sk.merchantapp.Utils.JsonParser;
import mm.dummymerchant.sk.merchantapp.Utils.Utils;

/**
 * Created by ThawZinAung on 10/16/2015.
 */
public class BankModel {

    String BankName = "", BranchName = "", BranchAddress = "", BranchDivState = "", BranchTownship = "";
    private ArrayList<BankModel> listOfBankModel= new ArrayList<>();
    private ArrayList<HashMap> listOfBankBranches= new ArrayList<>();
    private boolean isTownshipBranches=false, isDivBranches=false;
    public BankModel() {
    }

    public String getBankName() {
        return BankName;
    }

    public void setBankName(String bankName) {
        BankName = bankName;
    }

    public ArrayList<HashMap> getListOfBankBranches() {
        return listOfBankBranches;
    }

    public void setListOfBankBranches(ArrayList<HashMap> listOfBankBranches) {
        this.listOfBankBranches = listOfBankBranches;
    }

    public ArrayList<BankModel> getBankModel(Context context, String parseData, boolean isTownship, boolean isDiv) {
        try {
            Log.e("Status", "getBankModel");
            JSONArray array = null;
            array = new JsonParser().varifyArrayResponse(Utils.readTextFileFromAssets(context, "bank_details.txt"));
            Log.e("JSON Array Length", "" + array.length());
            boolean flag = true;
            for (int i = 0; i < array.length(); i++) {
                isDivBranches=false;
                isTownshipBranches=false;
                Log.e("Status", "In json array loop");
                BankModel model = new BankModel();
                JSONObject jsonObject = (JSONObject) array.get(i);
                BankName = jsonObject.getString("BankName");
                //Log.e("Bank Name",""+BankName);
                JSONArray branchArray = jsonObject.getJSONArray("Banches");

                    for (int j = 0; j < branchArray.length(); j++) {
                        Log.e("Status", "In branch array loop");
                        listOfBankBranches= new ArrayList<>();
                        HashMap<String, String> map= new HashMap<>();
                        JSONObject jsonObjectBank = (JSONObject) branchArray.get(j);
                        Log.e("BranchTownshipCode", jsonObjectBank.getString("BranchTownshipCode") + "");
                        Log.e("branchTownship", parseData);

                        /*if(jsonObjectBank.getString("BranchTownshipCode").equals(parseData)){
                            testData = "township";
                        }else if(jsonObjectBank.getString("BranchDivStateCode").equals(parseData)){
                            testData = "divstate";
                        }
*/

                        if(jsonObjectBank.getString("BranchTownshipCode").equals(parseData) && isTownship) {
                            map.put("BranchName", jsonObjectBank.getString("BranchName"));
                            map.put("BranchAddress", jsonObjectBank.getString("BranchAddress"));
                            map.put("BranchDivState", jsonObjectBank.getString("BranchDivState"));
                            map.put("BranchTownship", jsonObjectBank.getString("BranchTownship"));
                            map.put("BranchDivStateCode", jsonObjectBank.getString("BranchDivStateCode"));
                            map.put("BranchTownshipCode", jsonObjectBank.getString("BranchTownshipCode"));
                            map.put("PhoneNo", jsonObjectBank.getString("PhoneNo"));
                            map.put("FAX", jsonObjectBank.getString("FAX"));
                            listOfBankBranches.add(map);
                            isTownshipBranches=true;

                        }
                        if(jsonObjectBank.getString("BranchDivStateCode").equals(parseData) && isDiv) {
                            map.put("BranchName", jsonObjectBank.getString("BranchName"));
                            map.put("BranchAddress", jsonObjectBank.getString("BranchAddress"));
                            map.put("BranchDivState", jsonObjectBank.getString("BranchDivState"));
                            map.put("BranchTownship", jsonObjectBank.getString("BranchTownship"));
                            map.put("BranchDivStateCode", jsonObjectBank.getString("BranchDivStateCode"));
                            map.put("BranchTownshipCode", jsonObjectBank.getString("BranchTownshipCode"));
                            map.put("PhoneNo", jsonObjectBank.getString("PhoneNo"));
                            map.put("FAX", jsonObjectBank.getString("FAX"));
                            listOfBankBranches.add(map);
                            isDivBranches=true;
                            //isFilterBank=true;
                        }
                        /*if (!(isDiv || isTownship)){*/
                        if (!(isDiv || isTownship)){
                            map.put("BranchName", jsonObjectBank.getString("BranchName"));
                            map.put("BranchAddress", jsonObjectBank.getString("BranchAddress"));
                            map.put("BranchDivState", jsonObjectBank.getString("BranchDivState"));
                            map.put("BranchTownship", jsonObjectBank.getString("BranchTownship"));
                            map.put("BranchDivStateCode", jsonObjectBank.getString("BranchDivStateCode"));
                            map.put("BranchTownshipCode", jsonObjectBank.getString("BranchTownshipCode"));
                            map.put("PhoneNo", jsonObjectBank.getString("PhoneNo"));
                            map.put("FAX", jsonObjectBank.getString("FAX"));
                            listOfBankBranches.add(map);
                        }

                        model.setListOfBankBranches(listOfBankBranches);
                }

                if(isDivBranches || isTownshipBranches) {
                    model.setBankName(BankName);
                    listOfBankModel.add(model);
                }

                if (!(isDiv || isTownship)){
                    model.setBankName(BankName);
                    listOfBankModel.add(model);
                }
            }
            return listOfBankModel;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

}
