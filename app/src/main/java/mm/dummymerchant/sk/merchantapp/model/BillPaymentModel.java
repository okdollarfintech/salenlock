package mm.dummymerchant.sk.merchantapp.model;



import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import mm.dummymerchant.sk.merchantapp.Utils.XMLTag;

public class BillPaymentModel implements XMLTag,Serializable

{
	String resultCode = "";
	String resultDescription = "";
	String agentCode = "";
	String transId = "";
	String source = "";
	String resonsetime = "";
	String recordCount = "";
	String billerCode = "";
	String amountPr = "";
	String amountPost = "";
	String billerName = "";
	String dueDate = "";
	String invoiceno = "";

    ArrayList<HashMap<String,String>> records;
    HashMap<String,String> record;

	public BillPaymentModel(String response) throws XmlPullParserException, IOException
	{
		XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
		XmlPullParser myparser = factory.newPullParser();
		myparser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
		InputStream stream = new ByteArrayInputStream(response.getBytes());
		myparser.setInput(stream, null);
		parseXML(myparser);
		stream.close();
	}

	void parseXML(XmlPullParser myParser) throws XmlPullParserException, IOException

    {
		int event;
		String text = null;

		event = myParser.getEventType();

		while (event != XmlPullParser.END_DOCUMENT)

        {
			String name = myParser.getName();

			switch (event) {

				case XmlPullParser.START_TAG:

                     if(name.equals(TAG_RECORDS)) {

                         records = new ArrayList<>();

                     }else if(name.equals(TAG_RECORD))
                     {

                        record = new HashMap<String,String>();

                     }else if(name.equals(TAG_AMOUN_PRE))
                     {
                         record.put(TAG_AMOUN_PRE,myParser.nextText());


                     }
                     else if(name.equals(TAG_AMOUN_POST))
                     {
                         record.put(TAG_AMOUN_POST,myParser.nextText());


                     }
                     else if(name.equals(TAG_INVOICE))
                     {

                         record.put(TAG_INVOICE,myParser.nextText());

                     }
                     else if(name.equals(TAG_BILLER_CODE))
                     {

                         record.put(TAG_BILLER_CODE,myParser.nextText());


                     }else if(name.equals(TAG_PERIOD))
                     {

                         record.put(TAG_PERIOD,myParser.nextText());

                     }else if(name.equals(TAG_COMMENTS))
                     {

                         try{

                             record.put(TAG_COMMENTS,myParser.nextText());

                         }catch (Exception e){
                             e.printStackTrace();
                         }

                     }else if(name.equals(TAG_SENT))
                     {

                         try{
                             record.put(TAG_SENT,myParser.nextText());
                         }catch (Exception e){
                             e.printStackTrace();
                         }

                     }else if(name.equals(TAG_SOURCE))
                     {
                       try{

                      //   record.put(TAG_SOURCE,myParser.nextText());

                        }catch (Exception e){
                           e.printStackTrace();
                       }

                     }
                     else if(name.equals(TAG_BILL_DUE_DATE))
                     {

                         try{

                            record.put(TAG_BILL_DUE_DATE,myParser.nextText());

                         }catch (Exception e){
                             e.printStackTrace();
                         }

                     }

                    break;

				case XmlPullParser.TEXT:

                    text = myParser.getText();

					break;

				case XmlPullParser.END_TAG:

					if (name.equals(TAG_AGENTCODE))
						setAgentCode(text);
					else if (name.equals(TAG_TRANSID))
						setTransId(text);
					else if (name.equals(TAG_RESPONSE_CODE))
						setResultCode(text);
					else if (name.equals(TAG_DESCRIPTION))
						setResultDescription(text);
                    else if(name.equals(TAG_RECORD))
                          records.add(record);
                    else if (name.equals(TAG_SOURCE))
						setSource(text);
					else if (name.equals(TAG_RESPONSEACT))
						setDueDate(text);
					else if (name.equals(TAG_RECORD_COUNT))
						setRecordCount(text);
                    else if (name.equals(TAG_BILLER_CODE))
						setBillerCode(text);
					else if (name.equals(TAG_AMOUN_PRE))
						setAmountPr(text);
					else if (name.equals(TAG_AMOUN_POST))
						setAmountPost(text);
					else if (name.equals(TAG_AMOUN_POST))
						setAmountPost(text);
					else if (name.equals(TAG_BILLER_NAME))
						setBillerName(text);
					else if (name.equals(TAG_BILL_DUE_DATE))
						setDueDate(text);
					else if (name.equals(TAG_INVOICE))
						setInvoiceno(text);
					break;
			}

			event = myParser.next();
		}
	}

	public String getResultCode()
	{
		return resultCode;
	}

	public void setResultCode(String resultCode)
	{
		this.resultCode = resultCode;
	}

	public String getResultDescription()
	{
		return resultDescription;
	}

	public void setResultDescription(String resultDescription)
	{
		this.resultDescription = resultDescription;
	}

	public String getAgentCode()
	{
		return agentCode;
	}

	public void setAgentCode(String agentCode)
	{
		this.agentCode = agentCode;
	}

	public String getTransId()
	{
		return transId;
	}

	public void setTransId(String transId)
	{
		this.transId = transId;
	}

	public String getSource()
	{
		return source;
	}

	public void setSource(String source)
	{
		this.source = source;
	}

	public String getResonsetime()
	{
		return resonsetime;
	}

	public void setResonsetime(String resonsetime)
	{
		this.resonsetime = resonsetime;
	}

	public String getRecordCount()
	{
		return recordCount;
	}

	public void setRecordCount(String recordCount)
	{
		this.recordCount = recordCount;
	}

	public String getBillerCode()
	{
		return billerCode;
	}

	public void setBillerCode(String billerCode)
	{
		this.billerCode = billerCode;
	}

	public String getAmountPr()
	{
		return amountPr;
	}

	public void setAmountPr(String amountPr)
	{
		this.amountPr = amountPr;
	}

	public String getAmountPost()
	{
		return amountPost;
	}

	public void setAmountPost(String amountPost)
	{
		this.amountPost = amountPost;
	}

	public String getBillerName()
	{
		return billerName;
	}

	public void setBillerName(String billerName)
	{
		this.billerName = billerName;
	}

	public String getDueDate()
	{
		return dueDate;
	}

	public void setDueDate(String dueDate)
	{
		this.dueDate = dueDate;
	}

	public String getInvoiceno()
	{
		return invoiceno;
	}

	public void setInvoiceno(String invoiceno)
	{
		this.invoiceno = invoiceno;
	}

    public ArrayList<HashMap<String,String>> getRecords()
    {
        return records;
    }


}
