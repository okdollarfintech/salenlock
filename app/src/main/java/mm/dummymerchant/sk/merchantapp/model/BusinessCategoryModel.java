package mm.dummymerchant.sk.merchantapp.model;

import android.content.Context;



import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.Constant;
import mm.dummymerchant.sk.merchantapp.Utils.JsonParser;
import mm.dummymerchant.sk.merchantapp.Utils.Utils;

/**
 * Created by kapil on 15/07/15.
 */
public class BusinessCategoryModel {

    String mainCategory;

    ArrayList<String> listOfSubCategory, listOfSubCategoryEmoji;
    ArrayList<BusinessCategoryModel> listOfModel=new ArrayList<BusinessCategoryModel>();

    public ArrayList<String> getListOfSubCategory() {
        return listOfSubCategory;
    }

    public ArrayList<String> getListOfSubCategoryEmoji() {
        return listOfSubCategoryEmoji;
    }

    public void setListOfSubCategoryEmoji(ArrayList<String> listOfSubCategoryEmoji) {
        this.listOfSubCategoryEmoji = listOfSubCategoryEmoji;
    }

    public void setListOfSubCategory(ArrayList<String> listOfSubCategory) {
        this.listOfSubCategory = listOfSubCategory;
    }

    public String getMainCategory() {
        return mainCategory;
    }

    public void setMainCategory(String mainCategory) {
        this.mainCategory = mainCategory;
    }

    public ArrayList<BusinessCategoryModel>  getBusinessCategoryModel(Context context){
        try {
            JSONArray array=null, arrayEmoji=null;
            arrayEmoji = new JsonParser().varifyArrayResponse(Utils.readTextFileFromAssets(context, Constant.BUSINESS_EMOJI_ICON_FILE));
            if (AppPreference.getLanguage(context).equalsIgnoreCase("my"))
                array = new JsonParser().varifyArrayResponse(Utils.readTextFileFromAssets(context, Constant.BUSINESS_CATEGORY_FILE_MY));
            else
                array = new JsonParser().varifyArrayResponse(Utils.readTextFileFromAssets(context, Constant.BUSINESS_CATEGORY_FILE_ENG));
            for (int i = 0; i < array.length(); i++) {
                BusinessCategoryModel model=new BusinessCategoryModel();
                JSONObject jsonObject=(JSONObject)array.get(i);
                model.setMainCategory(jsonObject.getString("category_name"));
                JSONArray jsonArray=jsonObject.getJSONArray("subcategory");
                JSONObject jsonObjectSub=(JSONObject)jsonArray.get(0);
                //
                JSONObject jsonObjectEmoji=(JSONObject)arrayEmoji.get(i);
                JSONArray jsonArrayEmoji=jsonObjectEmoji.getJSONArray("subcategory");
                JSONObject jsonObjectSubEmoji=(JSONObject)jsonArrayEmoji.get(0);
                //arrayEmoji
                listOfSubCategory=new ArrayList<String>();
                listOfSubCategoryEmoji=new ArrayList<String>();
                for (int j=0 ;j<jsonObjectSub.length(); j++){
                    listOfSubCategory.add(jsonObjectSub.getString(String.valueOf(j)));
                    if (j<10)
                        listOfSubCategoryEmoji.add(jsonObjectSubEmoji.getString("0"+String.valueOf(j)));
                    else
                        listOfSubCategoryEmoji.add(jsonObjectSubEmoji.getString(String.valueOf(j)));
                }
                model.setListOfSubCategory(listOfSubCategory);
                model.setListOfSubCategoryEmoji(listOfSubCategoryEmoji);
                listOfModel.add(model);
            }
            return listOfModel;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    public BusinessCategoryModel findBusinessCategoryAndSubCategory(String category, String subCategory, Context context){
        try {
            JSONArray array = null, arrayEmoji = null;
            arrayEmoji = new JsonParser().varifyArrayResponse(Utils.readTextFileFromAssets(context, Constant.BUSINESS_EMOJI_ICON_FILE));
            if (AppPreference.getLanguage(context).equalsIgnoreCase("my"))
                array = new JsonParser().varifyArrayResponse(Utils.readTextFileFromAssets(context, Constant.BUSINESS_CATEGORY_FILE_MY));
            else
                array = new JsonParser().varifyArrayResponse(Utils.readTextFileFromAssets(context, Constant.BUSINESS_CATEGORY_FILE_ENG));
            for (int i = 0; i < array.length(); i++) {
                BusinessCategoryModel model=new BusinessCategoryModel();
                JSONObject jsonObject=(JSONObject)array.get(i);

                JSONArray jsonArray=jsonObject.getJSONArray("subcategory");
                JSONObject jsonObjectSub=(JSONObject)jsonArray.get(0);

                JSONObject jsonObjectEmoji=(JSONObject)arrayEmoji.get(i);
                JSONArray jsonArrayEmoji=jsonObjectEmoji.getJSONArray("subcategory");
                JSONObject jsonObjectSubEmoji=(JSONObject)jsonArrayEmoji.get(0);

                listOfSubCategory=new ArrayList<String>();
                listOfSubCategoryEmoji=new ArrayList<String>();
                for (int j=0 ;j<jsonObjectSub.length(); j++){
                    if (Integer.parseInt(subCategory)==j && Integer.parseInt(category)==i) {
                        listOfSubCategory.add(jsonObjectSub.getString(String.valueOf(j)));
                        if (j < 10)
                            listOfSubCategoryEmoji.add(jsonObjectSubEmoji.getString("0" + String.valueOf(j)));
                        else
                            listOfSubCategoryEmoji.add(jsonObjectSubEmoji.getString(String.valueOf(j)));
                        model.setMainCategory(jsonObject.getString("category_name"));
                        model.setListOfSubCategory(listOfSubCategory);
                        model.setListOfSubCategoryEmoji(listOfSubCategoryEmoji);
                        return model;
                    }
                }

            }
        }catch (Exception e){
            return null;
        }
        return null;
    }
}
