package mm.dummymerchant.sk.merchantapp.model;

import android.graphics.Bitmap;

/**
 * Created by user on 11/12/2015.
 */
public class CashCollectorModel {

    private Bitmap photo;
    private String name;
    private String no;
    private String source;
    private Bitmap sign;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String id;



    String status;

    public CashCollectorModel() {
    }

    public CashCollectorModel(Bitmap p, String n, String num,String s,Bitmap si,String status,String id) {
        photo = p;
        name = n;
        no = num;
        source = s;
        sign = si;
        this.status=status;
        this.id=id;

    }

    public Bitmap getPhoto() {
        return photo;
    }


    public void setPhoto(Bitmap photo)
    {
        this.photo=photo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name)
    {
        this.name=name;
    }

    public String getNumber() {
        return no;
    }

    public void setNumber(String no)
    {
        this.no=no;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source)
    {
        this.source=source;
    }

    public Bitmap getBitmap_Sign() {
        return sign;
    }

    public void setSign(Bitmap sign)
    {
        this.sign=sign;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
