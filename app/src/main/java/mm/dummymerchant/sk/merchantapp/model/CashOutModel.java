package mm.dummymerchant.sk.merchantapp.model;

import android.graphics.Bitmap;

/**
 * Created by user on 11/28/2015.
 */
public class CashOutModel {

    private Bitmap sign;
    private String name,amount,Comm_Type,comm_amt,Time,FinalAmout;


    public CashOutModel() {
    }
   // public CashOutModel(String time,String n, String amt,String com_type,String com_amt,Bitmap si) {
        public CashOutModel(String time,String n, String amt,String com_type,String com_amt,String famt) {
        Time =time;
        name = n;
        amount = amt;
        Comm_Type = com_type;
        comm_amt = com_amt;
        FinalAmout = famt;
       // sign = si;
    }
    public String getTime() {
        return Time;
    }

    public void setTime(String Time)
    {
        this.Time=Time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name)
    {
        this.name=name;
    }

    public String getamount() {
        return amount;
    }

    public void setamount(String amount)
    {
        this.amount=amount;
    }

    public String getComm_Type() {
        return Comm_Type;
    }

    public void setcomm_Type(String comm_Type)
    {
        this.Comm_Type=comm_Type;
    }

    public String getcomm_amt() {
        return comm_amt;
    }

    public void setcomm_amt(String comm_amt)
    {
        this.comm_amt=comm_amt;
    }

    public String getFinalAmout() {
        return FinalAmout;
    }

    public void setFinalAmout(String FinalAmout)
    {
        this.FinalAmout=FinalAmout;
    }

    public Bitmap getBitmap_Sign() {
        return sign;
    }

    public void setSign(Bitmap sign)
    {
        this.sign=sign;
    }
}
