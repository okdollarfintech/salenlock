package mm.dummymerchant.sk.merchantapp.model;

/**
 * Created by user on 11/7/2015.
 */
public class CashierActivityLogModel {

    String sno;
    String cashier_Id;
    String cashier_Name;
    String cashier_Number;
    String login_Time;
    String logout_Time;
    String logout_Reason;
    String status;

    public CashierActivityLogModel(String sno, String cashier_Id, String cashier_Name, String cashier_Number, String login_Time, String logout_Time, String logout_Reason, String status) {
        this.sno = sno;
        this.cashier_Id = cashier_Id;
        this.cashier_Name = cashier_Name;
        this.cashier_Number = cashier_Number;
        this.login_Time = login_Time;
        this.logout_Time = logout_Time;
        this.logout_Reason = logout_Reason;
        this.status = status;
    }


    public String getSno() {
        return sno;
    }

    public void setSno(String sno) {
        this.sno = sno;
    }

    public String getCashier_Id() {
        return cashier_Id;
    }

    public void setCashier_Id(String cashier_Id) {
        this.cashier_Id = cashier_Id;
    }

    public String getCashier_Name() {
        return cashier_Name;
    }

    public void setCashier_Name(String cashier_Name) {
        this.cashier_Name = cashier_Name;
    }

    public String getCashier_Number() {
        return cashier_Number;
    }

    public void setCashier_Number(String cashier_Number) {
        this.cashier_Number = cashier_Number;
    }

    public String getLogin_Time() {
        return login_Time;
    }

    public void setLogin_Time(String login_Time) {
        this.login_Time = login_Time;
    }

    public String getLogout_Time() {
        return logout_Time;
    }

    public void setLogout_Time(String logout_Time) {
        this.logout_Time = logout_Time;
    }

    public String getLogout_Reason() {
        return logout_Reason;
    }

    public void setLogout_Reason(String logout_Reason) {
        this.logout_Reason = logout_Reason;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }



}

