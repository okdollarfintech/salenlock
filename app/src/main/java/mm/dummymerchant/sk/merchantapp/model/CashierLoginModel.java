package mm.dummymerchant.sk.merchantapp.model;

/**
 * Created by Dell on 11/17/2015.
 */
public class CashierLoginModel
{
    String sno;
    String cashierId;
    String logInTime;
    String logOutTime;
    String status;

    public CashierLoginModel(String sno, String cashierId, String logInTime, String logOutTime, String status) {
        this.sno = sno;
        this.cashierId = cashierId;
        this.logInTime = logInTime;
        this.logOutTime = logOutTime;
        this.status = status;
    }

    public void setSno(String sno) {
        this.sno = sno;
    }


    public String getSno() {
        return sno;
    }


    public String getCashierId() {
        return cashierId;
    }

    public String getLogInTime() {
        return logInTime;
    }

    public String getLogOutTime() {
        return logOutTime;
    }

    public String getStatus() {
        return status;
    }

}
