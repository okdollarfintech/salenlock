package mm.dummymerchant.sk.merchantapp.model;

import android.graphics.Bitmap;

/**
 * Created by Dell on 11/3/2015.
 */
public class CashierModel
{
    String cashier_Id;
    String cashier_Name;
    String cashier_Number;
    String password;
    String confirmPassword;
    String colorCode;
    String createdDate;
    Bitmap cashierPhoto;
    String status;
    String jobType;

    public CashierModel(String cashier_Id, String cashier_Name, String cashier_Number, String password, String confirmPassword, String colorCode,String createdDate,Bitmap cashierPhoto,String status, String jobType) {
        this.cashier_Id = cashier_Id;
        this.cashier_Name = cashier_Name;
        this.cashier_Number = cashier_Number;
        this.password = password;
        this.confirmPassword = confirmPassword;
        this.colorCode = colorCode;
        this.createdDate=createdDate;
        this.cashierPhoto=cashierPhoto;
        this.status=status;
        this.jobType=jobType;

    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCashier_Id() {
        return cashier_Id;
    }

    public String getCashier_Name() {
        return cashier_Name;
    }

    public String getCashier_Number() {
        return cashier_Number;
    }

    public String getPassword() {
        return password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public String getColorCode() {
        return colorCode;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public Bitmap getCashierPhoto() {
        return cashierPhoto;
    }

    public String getJobType() {
        return jobType;
    }

}
