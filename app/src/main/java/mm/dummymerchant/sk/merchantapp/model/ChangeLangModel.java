package mm.dummymerchant.sk.merchantapp.model;

import java.io.Serializable;

/**
 * Created by CGM on 2/2/2016.
 */
public class ChangeLangModel implements Serializable {

    public String MobileNumber="";
    public String SimId="";
    public int OsType =0;
    public String SecureToken="";
    public String Password ="";
    public String Language="";

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public String getSimId() {
        return SimId;
    }

    public void setSimId(String simId) {
        SimId = simId;
    }

    public int getOsType() {
        return OsType;
    }

    public void setOsType(int osType) {
        OsType = osType;
    }

    public String getSecureToken() {
        return SecureToken;
    }

    public void setSecureToken(String secureToken) {
        SecureToken = secureToken;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getLanguage() {
        return Language;
    }

    public void setLanguage(String language) {
        Language = language;
    }
}
