package mm.dummymerchant.sk.merchantapp.model;



import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

import mm.dummymerchant.sk.merchantapp.Utils.XMLTag;

public class ChangeLangObject implements XMLTag, Serializable
{

	String responseType = "";
	String agentCode = "";
	String pin = "";
	String lang = "";
	
	public ChangeLangObject(String response) throws XmlPullParserException, IOException
	{
		XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
		XmlPullParser myparser = factory.newPullParser();
		myparser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
		InputStream stream = new ByteArrayInputStream(response.getBytes());
		myparser.setInput(stream, null);
		parseXML(myparser);
		stream.close();
	}

	void parseXML(XmlPullParser myParser) throws XmlPullParserException, IOException
	{
		int event;
		String text = null;
		event = myParser.getEventType();
		while (event != XmlPullParser.END_DOCUMENT)
		{
			String name = myParser.getName();
			switch (event) {
				case XmlPullParser.START_TAG:
					break;
				case XmlPullParser.TEXT:
					text = myParser.getText();
					break;
				case XmlPullParser.END_TAG:
					if (name.equals(TAG_RESPONSE_TYPE))
						setResponseType(text);
					else if (name.equals(TAG_AGENTCODE))
						setAgentCode(text);
					else if (name.equals(TAG_PIN))
						setPin(text);
					else if (name.equals(TAG_LANG))
						setLang(text);
					break;
			}
			event = myParser.next();
		}
	}
	public String getAgentCode()
	{
		return agentCode;
	}
	public void setAgentCode(String agentCode)
	{
		this.agentCode = agentCode;
	}
	public String getPin()
	{
		return pin;
	}
	public void setPin(String pin)
	{
		this.pin = pin;
	}
	public String getLang()
	{
		return lang;
	}
	public void setLang(String lang)
	{
		this.lang = lang;
	}

	public String getResponseType()
	{
		return responseType;
	}

	public void setResponseType(String responseType)
	{
		this.responseType = responseType;
	}
	
}
