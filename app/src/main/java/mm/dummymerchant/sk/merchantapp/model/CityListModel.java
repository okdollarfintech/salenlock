package mm.dummymerchant.sk.merchantapp.model;

import android.database.Cursor;

import org.json.JSONObject;

import java.io.Serializable;

import mm.dummymerchant.sk.merchantapp.db.DBHelper;

public class CityListModel implements Serializable
{

	String name = "";
	String id = "";
    String isState="";
	String BName="";

	public CityListModel(JSONObject jsonObject) throws Exception
	{
		if (jsonObject.has("State Code"))
			setId(jsonObject.getString("State Code"));
		if (jsonObject.has("State Name"))
			setName(jsonObject.getString("State Name"));
        if (jsonObject.has("isState"))
            setIsState(jsonObject.getString("isState"));
		if (jsonObject.has("State BName"))
			setBName(jsonObject.getString("State BName"));

	}

	public CityListModel(Cursor c)
	{
		setId(c.getString(c.getColumnIndex(DBHelper.KEY_AGENT_CITY_ID)));
		setName(c.getString(c.getColumnIndex(DBHelper.KEY_AGENT_CITY_NAME)));
        setIsState(c.getString(c.getColumnIndex(DBHelper.KEY_R_IS_STATE_DIVISION)));
		setBName(c.getString(c.getColumnIndex(DBHelper.KEY_AGENT_CITY_NAME_BURMESE)));
	}

    public String getIsState() {
        return isState;
    }

    public void setIsState(String isState) {
        this.isState = isState;
    }

    public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getBName() {
		return BName;
	}

	public void setBName(String BName) {
		this.BName = BName;
	}
}
