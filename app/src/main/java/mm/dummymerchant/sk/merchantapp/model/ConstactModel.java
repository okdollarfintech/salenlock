package mm.dummymerchant.sk.merchantapp.model;

import android.database.Cursor;



import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

import mm.dummymerchant.sk.merchantapp.db.DBHelper;


public class ConstactModel implements Serializable
{

	String name = "";
	String mobileNo = "";
	String email = "";
	String photoUrl = "";
	String id = "";
    String isContactUpload="";
	ArrayList<ConstactModel> mContactModel = new ArrayList<ConstactModel>();
	public ConstactModel(String name, String moNo, String Email)
	{
		setName(name);
		setMobileNo(moNo);
		setEmail(Email);
	}
    public ConstactModel()
    {

    }
	public ConstactModel(JSONObject jsonObject) throws JSONException
	{
		if(jsonObject.has("Name"))
			setName(jsonObject.getString("Name"));
		if(jsonObject.has("PhoneNumber"))
			setMobileNo(jsonObject.getString("PhoneNumber"));
		if(jsonObject.has("EmailID"))
			setEmail(jsonObject.getString("EmailID"));
	}

	public ConstactModel(String name, String mobileNo, String email, String photoUrl, String id, String isContactUpload) {
		this.name = name;
		this.mobileNo = mobileNo;
		this.email = email;
		this.photoUrl = photoUrl;
		this.id = id;
		this.isContactUpload = isContactUpload;
	}

	public ConstactModel(Cursor c)
	{
		setName(c.getString(c.getColumnIndex(DBHelper.KEY_NAME)));
		setMobileNo(c.getString(c.getColumnIndex(DBHelper.KEY_MOBILE_NUMBER)));
		setEmail(c.getString(c.getColumnIndex(DBHelper.KEY_EMAIL_ID)));
		setId(c.getString(c.getColumnIndex(DBHelper.COLUMN_ID)));
		setPhotoUrl(c.getString(c.getColumnIndex(DBHelper.COLUMN_PHOTO_URL)));
        setIsContactUpload(c.getString(c.getColumnIndex(DBHelper.KEY_ISCONTACT_UPLOAED)));
	}

    public String getIsContactUpload() {
        return isContactUpload;
    }

    public void setIsContactUpload(String isContactUpload) {
        this.isContactUpload = isContactUpload;
    }

    public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getMobileNo()
	{
		return mobileNo;
	}

	public void setMobileNo(String mobileNo)
	{
		this.mobileNo = mobileNo;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getPhotoUrl()
	{
		return photoUrl;
	}

	public void setPhotoUrl(String photoUrl)
	{
		this.photoUrl = photoUrl;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}


	public ArrayList<ConstactModel> getmTransationModel() {
		return mContactModel;
	}

	public void setmCantactModel(ConstactModel mContactModel) {
		this.mContactModel.add(mContactModel);
	}
	
}
