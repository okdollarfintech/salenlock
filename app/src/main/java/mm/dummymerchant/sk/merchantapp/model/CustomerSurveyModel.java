package mm.dummymerchant.sk.merchantapp.model;

import android.database.Cursor;

import mm.dummymerchant.sk.merchantapp.db.DBHelper;

/**
 * Created by user on 11/11/2015.
 */
public class CustomerSurveyModel {
    String q1,q2,q3,q4,q5,q6,q7,q8;
    String suggestion;
    String phone;
    int week;
    int month;
    int year;
    int hours;
    int day;
    String date;
    Double total;
    String age;
    String city;
    String minutes;
    String trnasID;

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    String gender;

    public CustomerSurveyModel(Cursor c) {
        setQ1(c.getString(c.getColumnIndex(DBHelper.SURVEY_QUES1)));
        setQ2(c.getString(c.getColumnIndex(DBHelper.SURVEY_QUES2)));
        setQ3(c.getString(c.getColumnIndex(DBHelper.SURVEY_QUES3)));
        setQ4(c.getString(c.getColumnIndex(DBHelper.SURVEY_QUES4)));
        setQ5(c.getString(c.getColumnIndex(DBHelper.SURVEY_QUES5)));
        setQ6(c.getString(c.getColumnIndex(DBHelper.SURVEY_QUES6)));
        setQ7(c.getString(c.getColumnIndex(DBHelper.SURVEY_QUES7)));
        setQ8(c.getString(c.getColumnIndex(DBHelper.SURVEY_QUES8)));
        setSuggestion(c.getString(c.getColumnIndex(DBHelper.SURVEY_SUGGESTION)));
        setPhone(c.getString(c.getColumnIndex(DBHelper.SURVEY_PHONE)));
        setWeek(Integer.parseInt(c.getString(c.getColumnIndex(DBHelper.SURVEY_WEEK))));
        setMonth(Integer.parseInt(c.getString(c.getColumnIndex(DBHelper.SURVEY_MONTH))));
        setYear(Integer.parseInt(c.getString(c.getColumnIndex(DBHelper.SURVEY_YEAR))));
        setHours(Integer.parseInt(c.getString(c.getColumnIndex(DBHelper.SURVEY_HOUR))));
        setDay(Integer.parseInt(c.getString(c.getColumnIndex(DBHelper.SURVEY_DAY))));
        setDate(c.getString(c.getColumnIndex(DBHelper.KEY_TRANSCATION_DATE)));
        setMinutes(c.getString(c.getColumnIndex(DBHelper.SURVEY_MINUTE)));
        setAge(c.getString(c.getColumnIndex(DBHelper.SURVEY_AGE)));
        setCity(c.getString(c.getColumnIndex(DBHelper.SURVEY_LOCATION)));
        setTotal(Double.parseDouble(c.getString(c.getColumnIndex(DBHelper.SURVEY_TOTAL))));
        setTrnasID(c.getString(c.getColumnIndex(DBHelper.KEY_TRANSID)));
        setGender(c.getString(c.getColumnIndex(DBHelper.SURVEY_GENDER)));
    }

    public String getQ1() {
        return q1;
    }

    public void setQ1(String q1) {
        this.q1 = q1;
    }

    public String getQ2() {
        return q2;
    }

    public void setQ2(String q2) {
        this.q2 = q2;
    }

    public String getQ3() {
        return q3;
    }

    public void setQ3(String q3) {
        this.q3 = q3;
    }

    public String getQ4() {
        return q4;
    }

    public void setQ4(String q4) {
        this.q4 = q4;
    }

    public String getQ5() {
        return q5;
    }

    public void setQ5(String q5) {
        this.q5 = q5;
    }

    public String getQ6() {
        return q6;
    }

    public void setQ6(String q6) {
        this.q6 = q6;
    }

    public String getQ7() {
        return q7;
    }

    public void setQ7(String q7) {
        this.q7 = q7;
    }

    public String getQ8() {
        return q8;
    }

    public void setQ8(String q8) {
        this.q8 = q8;
    }

    public String getSuggestion() {
        return suggestion;
    }

    public void setSuggestion(String suggestion) {
        this.suggestion = suggestion;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getWeek() {
        return week;
    }

    public void setWeek(int week) {
        this.week = week;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getMinutes() {
        return minutes;
    }

    public void setMinutes(String minutes) {
        this.minutes = minutes;
    }

    public String getTrnasID() {
        return trnasID;
    }

    public void setTrnasID(String trnasID) {
        this.trnasID = trnasID;
    }

}
