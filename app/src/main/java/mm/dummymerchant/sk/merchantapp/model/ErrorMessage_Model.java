package mm.dummymerchant.sk.merchantapp.model;



import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

import mm.dummymerchant.sk.merchantapp.Utils.XMLTag;

public class ErrorMessage_Model implements XMLTag, Serializable
{
	String resultCode = "";
	String description = "";

	public ErrorMessage_Model(String response) throws XmlPullParserException, IOException
	{
		XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
		XmlPullParser myparser = factory.newPullParser();
		myparser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
		InputStream stream = new ByteArrayInputStream(response.getBytes());
		myparser.setInput(stream, null);
		parseXML(myparser);
		stream.close();
	}

	void parseXML(XmlPullParser myParser) throws XmlPullParserException, IOException
	{
		int event;
		String text = null;
		event = myParser.getEventType();
		while (event != XmlPullParser.END_DOCUMENT)
		{
			String name = myParser.getName();
			switch (event) {
				case XmlPullParser.START_TAG:
					break;
				case XmlPullParser.TEXT:
					text = myParser.getText();
					break;
				case XmlPullParser.END_TAG:
					if (name.equals(TAG_DESCRIPTION))
						setDescription(text);
					else if (name.equals(TAG_RESULTP_CODE))
						setResultCode(text);
					break;
			}
			event = myParser.next();
		}
	}

	public String getResultCode()
	{
		return resultCode;
	}

	public void setResultCode(String resultCode)
	{
		this.resultCode = resultCode;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

}
