package mm.dummymerchant.sk.merchantapp.model;



import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

import mm.dummymerchant.sk.merchantapp.Utils.XMLTag;

/**
 * Created by kapil on 17/06/15.
 */
public class GCMRegistrationModel implements Serializable,XMLTag {

    String resultdescription = "";
    String transatId = "";
    String agentCode = "";
    String clienttype = "";
    String responsetype = "";
    String requestcts = "";
    String resultcode = "";
    String vendorcode = "";
    String responsects = "";
    public GCMRegistrationModel(String response) throws XmlPullParserException, IOException {
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        XmlPullParser myparser = factory.newPullParser();
        myparser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        InputStream stream = new ByteArrayInputStream(response.getBytes());
        myparser.setInput(stream, null);
        parseXML(myparser);
        stream.close();
    }

    void parseXML(XmlPullParser myParser) throws XmlPullParserException, IOException {
        int event;
        String text = null;
        event = myParser.getEventType();
        while (event != XmlPullParser.END_DOCUMENT) {
            String name = myParser.getName();
            switch (event) {
                case XmlPullParser.START_TAG:
                    break;
                case XmlPullParser.TEXT:
                    text = myParser.getText();
                    break;
                case XmlPullParser.END_TAG:
                    if (name.equals("responsetype"))
                        setresponsetype(text);
                    else if (name.equals("agentcode"))
                        setAgentCode(text);
                    else if (name.equals(""))
                        setTransatId(text);
                    else if (name.equals("resultcode"))
                        setResultcode(text);
                    else if (name.equals("resultdescription"))
                        setResultdescription(text);
                    else if (name.equals("requestcts"))
                        setRequestcts(text);
                    else if (name.equals("responsects"))
                        setResponsects(text);
                    else if (name.equals("vendorcode"))
                        setVendorcode(text);
                    else if (name.equals("clienttype"))
                        setClienttype(text);
                    break;
            }
            event = myParser.next();
        }
    }

    public String getResultdescription() {
        return resultdescription;
    }

    public void setResultdescription(String resultdescription) {
        this.resultdescription = resultdescription;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getTransatId() {
        return transatId;
    }

    public void setTransatId(String transatId) {
        this.transatId = transatId;
    }

    public String getClienttype() {
        return clienttype;
    }

    public void setClienttype(String clienttype) {
        this.clienttype = clienttype;
    }

    public String getresponsetype() {
        return responsetype;
    }

    public void setresponsetype(String responsetype) {
        this.responsetype = responsetype;
    }

    public String getRequestcts() {
        return requestcts;
    }

    public void setRequestcts(String requestcts) {
        this.requestcts = requestcts;
    }

    public String getResultcode() {
        return resultcode;
    }

    public void setResultcode(String resultcode) {
        this.resultcode = resultcode;
    }

    public String getVendorcode() {
        return vendorcode;
    }

    public void setVendorcode(String vendorcode) {
        this.vendorcode = vendorcode;
    }

    public String getResponsects() {
        return responsects;
    }

    public void setResponsects(String responsects) {
        this.responsects = responsects;
    }
}
