package mm.dummymerchant.sk.merchantapp.model;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

import mm.dummymerchant.sk.merchantapp.Utils.XMLTag;

/**
 * Created by Dell on 11/4/2015.
 */
public class GCM_RegistrationModel implements Serializable,XMLTag {

    String resultdescription = "";
    String transatId = "";
    String agentCode = "";
    String clienttype = "";
    String responsetype = "";
    String requestcts = "";
    String resultcode = "";
    String vendorcode = "";
    String responsects = "";


    public GCM_RegistrationModel(String response) throws XmlPullParserException, IOException {
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        XmlPullParser myparser = factory.newPullParser();
        myparser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        InputStream stream = new ByteArrayInputStream(response.getBytes());
        myparser.setInput(stream, null);
        parseXML(myparser);
        stream.close();
    }

    void parseXML(XmlPullParser myParser) throws XmlPullParserException, IOException {
        int event;
        String text = null;
        event = myParser.getEventType();
        while (event != XmlPullParser.END_DOCUMENT) {
            String name = myParser.getName();
            switch (event) {
                case XmlPullParser.START_TAG:
                    break;
                case XmlPullParser.TEXT:
                    text = myParser.getText();
                    break;
                case XmlPullParser.END_TAG:
                    switch (name)
                    {
                        case TAG_RESPONSE_TYPE:
                            setresponsetype(text);
                            break;
                        case TAG_AGENTCODE:
                            setAgentCode(text);
                            break;
                        case TAG_TRANSACID:
                            setTransatId(text);
                            break;
                        case TAG_RESULTP_CODE:
                            setResultcode(text);
                            break;
                        case TAG_DESCRIPTION:
                            setResultdescription(text);
                            break;
                        case TAG_REQ_CTS:
                            setRequestcts(text);
                            break;
                        case TAG_RESPONSEACT:
                            setResponsects(text);
                            break;
                        case TAG_VEDNDOR_CODE:
                            setVendorcode(text);
                            break;
                        case TAG_CLIENT_TYPE:
                            setClienttype(text);
                            break;

                        default:
                            break;
                    }
                    break;
                  /*  if (name.equals("responsetype"))
                        setresponsetype(text);
                    else if (name.equals("agentcode"))
                        setAgentCode(text);
                    else if (name.equals(""))
                        setTransatId(text);
                    else if (name.equals("resultcode"))
                        setResultcode(text);
                    else if (name.equals("resultdescription"))
                        setResultdescription(text);
                    else if (name.equals("requestcts"))
                        setRequestcts(text);
                    else if (name.equals("responsects"))
                        setResponsects(text);

                    else if (name.equals("vendorcode"))
                        setVendorcode(text);
                    else if (name.equals("clienttype"))
                        setClienttype(text);
                        */
            }
            event = myParser.next();
        }
    }

    public String getResultdescription() {
        return resultdescription;
    }

    public void setResultdescription(String resultdescription) {
        this.resultdescription = resultdescription;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getTransatId() {
        return transatId;
    }

    public void setTransatId(String transatId) {
        this.transatId = transatId;
    }

    public String getClienttype() {
        return clienttype;
    }

    public void setClienttype(String clienttype) {
        this.clienttype = clienttype;
    }

    public String getresponsetype() {
        return responsetype;
    }

    public void setresponsetype(String responsetype) {
        this.responsetype = responsetype;
    }

    public String getRequestcts() {
        return requestcts;
    }

    public void setRequestcts(String requestcts) {
        this.requestcts = requestcts;
    }

    public String getResultcode() {
        return resultcode;
    }

    public void setResultcode(String resultcode) {
        this.resultcode = resultcode;
    }

    public String getVendorcode() {
        return vendorcode;
    }

    public void setVendorcode(String vendorcode) {
        this.vendorcode = vendorcode;
    }

    public String getResponsects() {
        return responsects;
    }

    public void setResponsects(String responsects) {
        this.responsects = responsects;
    }
}
