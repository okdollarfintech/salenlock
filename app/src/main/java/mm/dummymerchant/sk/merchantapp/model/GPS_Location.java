package mm.dummymerchant.sk.merchantapp.model;

import org.json.JSONObject;

/**
 * Created by kapil on 20/10/15.
 */
public class GPS_Location {
    private String addressLine1="", addressLine2="",state="", township="",
            stateBurmese="", townshipBurmese="", country="", lat="", lang="", telco="", nearByLocation="" ,stateCode="", townshipCode="";
    private boolean  is_Divison= false;
    private GPS_Location gpsLocation=null;
    public void setGPSLocation(String response){
        try{
            gpsLocation=new GPS_Location();
            JSONObject jsonObject= new JSONObject(response);
            JSONObject addressObject= jsonObject.getJSONObject("_address");
            gpsLocation.setAddressLine1(addressObject.getString("_addressLine1"));
            gpsLocation.setAddressLine2(addressObject.getString("_addressLine2"));
            gpsLocation.setCountry(addressObject.getString("_country"));
            gpsLocation.setNearByLocation(addressObject.getString("_nearLocation"));
            gpsLocation.setTelco(addressObject.getString("_telco"));
            gpsLocation.setLat(jsonObject.getString("_latitude"));
            gpsLocation.setLang(jsonObject.getString("_longitude"));

            JSONObject jsonObjectState= addressObject.getJSONObject("_state");
            gpsLocation.setStateBurmese(jsonObjectState.getString("_burmeseName"));
            gpsLocation.setStateCode(jsonObjectState.getString("_code"));
            gpsLocation.setState(jsonObjectState.getString("_name"));
            gpsLocation.setIs_Divison(jsonObjectState.getBoolean("_isDivision"));

            JSONObject jsonObjectTownship= addressObject.getJSONObject("_township");
            gpsLocation.setTownshipBurmese(jsonObjectTownship.getString("_burmeseName"));
            gpsLocation.setTownshipCode(jsonObjectTownship.getString("_code"));
            gpsLocation.setTownship(jsonObjectTownship.getString("_name"));

        }catch (Exception e){
            e.printStackTrace();
        }

    }
    public GPS_Location getGpsLocation(){
        return gpsLocation;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTownship() {
        return township;
    }

    public void setTownship(String township) {
        this.township = township;
    }


    public String getStateBurmese() {
        return stateBurmese;
    }

    public void setStateBurmese(String stateBurmese) {
        this.stateBurmese = stateBurmese;
    }

    public String getTownshipBurmese() {
        return townshipBurmese;
    }

    public void setTownshipBurmese(String townshipBurmese) {
        this.townshipBurmese = townshipBurmese;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getTelco() {
        return telco;
    }

    public void setTelco(String telco) {
        this.telco = telco;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }


    public String getNearByLocation() {
        return nearByLocation;
    }

    public void setNearByLocation(String nearByLocation) {
        this.nearByLocation = nearByLocation;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getTownshipCode() {
        return townshipCode;
    }

    public void setTownshipCode(String townshipCode) {
        this.townshipCode = townshipCode;
    }

    public boolean getIs_Divison() {
        return is_Divison;
    }

    public void setIs_Divison(boolean is_Divison) {
        this.is_Divison = is_Divison;
    }
}
