package mm.dummymerchant.sk.merchantapp.model;

import java.util.ArrayList;

/**
 * Created by Dell on 3/11/2016.
 */
public class LaterModel {

    ArrayList<String> value;
    ArrayList<String> value1;
    ArrayList<PhoneContactModel> al_phonecontacts;
    String amount;
    String text;
    String qrId;

    public byte[] getImage_qr() {
        return image_qr;
    }

    byte[] image_qr;


    public LaterModel(ArrayList<String> value, ArrayList<String> value1, ArrayList<PhoneContactModel> al_phonecontacts, String amount, String text, String qrId, byte[] image_qr) {
        this.value = value;
        this.value1 = value1;
        this.al_phonecontacts = al_phonecontacts;
        this.amount = amount;
        this.text = text;
        this.qrId = qrId;
        this.image_qr=image_qr;

    }


    public ArrayList<String> getValue() {
        return value;
    }

    public ArrayList<String> getValue1() {
        return value1;
    }

    public ArrayList<PhoneContactModel> getAl_phonecontacts() {
        return al_phonecontacts;
    }

    public String getAmount() {
        return amount;
    }

    public String getText() {
        return text;
    }

    public String getQrId() {
        return qrId;
    }




}
