package mm.dummymerchant.sk.merchantapp.model;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by user on 10/30/15.
 */
public class LocationModel implements Serializable {


    String address = "";
    String AddressLine1 = "";
    String AddressLine2 = "";
    String Country = "";

    String state_burmese = "";
    String state_code = "";
    String state_name = "";


   public LocationModel(String response)
    {
        try {

            JSONObject object = new JSONObject(response);
            JSONObject address = object.getJSONObject("_address");
            setAddressLine1(address.getString("_addressLine1"));
            setAddressLine2(address.getString("_addressLine2"));
            setCountry(address.getString("_country"));

            JSONObject state = address.getJSONObject("_state");/// state
            setState_burmese(state.getString("_burmeseName"));
            setState_code(state.getString("_code"));
            setState_name(state.getString("_name"));
           /****************************************************/
            setState_isdivision(state.getString("_isDivision"));//township
            JSONObject township = address.getJSONObject("_township");
            setTownShip_burmeseName(township.getString("_burmeseName"));
            setTownShip_code("_code");
            setTownShip_name("_name");
            /*********************************************************/  //location
            setNearLocation(address.getString("_nearLocation"));
            setTelco(address.getString("_telco"));
            setCell_id(object.getString("_cellId"));
            setLatitude(object.getString("_latitude"));
            setLongitude(object.getString("_longitude"));

        }catch (Exception e)
        {

        }
    }





    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddressLine1() {
        return AddressLine1;
    }

    public void setAddressLine1(String AddressLine1) {
        this.AddressLine1 = AddressLine1;
    }

    public String getAddressLine2() {
        return AddressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.AddressLine2 = addressLine2;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getState_burmese() {
        return state_burmese;
    }

    public void setState_burmese(String state_burmese) {
        this.state_burmese = state_burmese;
    }

    public String getState_code() {
        return state_code;
    }

    public void setState_code(String state_code) {
        this.state_code = state_code;
    }

    public String getState_name() {
        return state_name;
    }

    public void setState_name(String state_name) {
        this.state_name = state_name;
    }

    public String getState_isdivision() {
        return state_isdivision;
    }

    public void setState_isdivision(String state_isdivision) {
        this.state_isdivision = state_isdivision;
    }

    public String getTownShip_burmeseName() {
        return townShip_burmeseName;
    }

    public void setTownShip_burmeseName(String townShip_burmeseName) {
        this.townShip_burmeseName = townShip_burmeseName;
    }

    public String getTownShip_code() {
        return townShip_code;
    }

    public void setTownShip_code(String townShip_code) {
        this.townShip_code = townShip_code;
    }

    public String getTownShip_name() {
        return townShip_name;
    }

    public void setTownShip_name(String townShip_name) {
        this.townShip_name = townShip_name;
    }

    public String getNearLocation() {
        return nearLocation;
    }

    public void setNearLocation(String nearLocation) {
        this.nearLocation = nearLocation;
    }

    public String getTelco() {
        return telco;
    }

    public void setTelco(String telco) {
        this.telco = telco;
    }

    public String getCell_id() {
        return cell_id;
    }

    public void setCell_id(String cell_id) {
        this.cell_id = cell_id;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    String state_isdivision = "";

    String townShip_burmeseName = "";
    String townShip_code = "";
    String townShip_name = "";
    String nearLocation = "";
    String telco = "";
    String cell_id = "";
    String latitude = "";
    String longitude = "";

}