package mm.dummymerchant.sk.merchantapp.model;

/**
 * Created by abhishekmodi on 30/10/15.
 */
public class LoginAPI {

    private String MobileNumber="";
    private String Password="";
    private String SimId="";
    private String Msid="";
    private int OsType=-1;
    private String IosOtp="";
    private String ProfilePic="";
    private String DateOfBirth="";
    private String FatherName="";
    private String Name="";
    private String ReRegisterOtp="";



    private int AppId;

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public String getSimId() {
        return SimId;
    }

    public void setSimId(String simId) {
        SimId = simId;
    }

    public int getOsType() {
        return OsType;
    }

    public void setOsType(int osType) {
        OsType = osType;
    }

    public String getMsid() {
        return Msid;
    }

    public void setMsid(String msid) {
        Msid = msid;
    }

    public String getIosOtp() {
        return IosOtp;
    }

    public void setIosOtp(String iosOtp) {
        IosOtp = iosOtp;
    }

    public String getProfilePic() {
        return ProfilePic;
    }

    public void setProfilePic(String profilePic) {
        ProfilePic = profilePic;
    }

    public String getDateOfBirth() {
        return DateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        DateOfBirth = dateOfBirth;
    }

    public String getFatherName() {
        return FatherName;
    }

    public void setFatherName(String fatherName) {
        FatherName = fatherName;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getReRegisterOtp() {
        return ReRegisterOtp;
    }

    public void setReRegisterOtp(String reRegisterOtp) {
        ReRegisterOtp = reRegisterOtp;
    }

    public int getAppId() {
        return AppId;
    }

    public void setAppId(int appId) {
        AppId = appId;
    }
}
