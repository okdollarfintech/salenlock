package mm.dummymerchant.sk.merchantapp.model;



import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

import mm.dummymerchant.sk.merchantapp.Utils.XMLTag;

public class LoginModel implements XMLTag, Serializable
{

	String agentcode = "";
	String name = "";
	String agentType = "";
	String status = "";
	String transId = "";
	String lan = "";
	String description = "";
	String token="";

	public LoginModel(String response) throws XmlPullParserException, IOException
	{
		XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
		XmlPullParser myparser = factory.newPullParser();
		myparser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
		InputStream stream = new ByteArrayInputStream(response.getBytes());
		myparser.setInput(stream, null);
		parseXML(myparser);
		stream.close();
	}

	void parseXML(XmlPullParser myParser) throws XmlPullParserException, IOException
	{
		int event;
		String text = null;
		event = myParser.getEventType();
		while (event != XmlPullParser.END_DOCUMENT)
		{
			String name = myParser.getName();
			switch (event) {
				case XmlPullParser.START_TAG:
					break;
				case XmlPullParser.TEXT:
					text = myParser.getText();
					break;
				case XmlPullParser.END_TAG:
					if (name.equals(TAG_AGENT_NAME))
						setName(text);
					else if (name.equals(TAG_AGENTCODE))
						setAgentcode(text);
					else if (name.equals(TAG_DESCRIPTION))
						setDescription(text);
					else if (name.equals(TAG_TRANSID))
						setTransId(text);
					else if (name.equals(TAG_LANG))
						setLan(text);
					else if (name.equals(TAG_TOKEN))
						setToken(text);
					break;
			}
			event = myParser.next();
		}
	}

	public String getAgentcode()
	{
		return agentcode;
	}

	public void setAgentcode(String agentcode)
	{
		this.agentcode = agentcode;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getAgentType()
	{
		return agentType;
	}

	public void setAgentType(String agentType)
	{
		this.agentType = agentType;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public String getTransId()
	{
		return transId;
	}

	public void setTransId(String transId)
	{
		this.transId = transId;
	}

	public String getLan()
	{
		return lan;
	}

	public void setLan(String lan)
	{
		this.lan = lan;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public String getToken()
	{
		return token;
	}

	public void setToken(String token)
	{
		this.token = token;
	}

	
}
