package mm.dummymerchant.sk.merchantapp.model;

import java.io.Serializable;

public class LoyaltyListModel implements Serializable
{

	String merchantName;
	String merchantId;
	String points;
	String exDate;

	public LoyaltyListModel(String name, String id, String point, String date)
	{
		setMerchantName(name);
		setMerchantId(id);
		setPoints(point);
		setExDate(date);
	}

   public LoyaltyListModel()
    {

    }
	public String getMerchantName()
	{
		return merchantName;
	}

	public void setMerchantName(String merchantName)
	{
		this.merchantName = merchantName;
	}

	public String getMerchantId()
	{
		return merchantId;
	}

	public void setMerchantId(String merchantId)
	{
		this.merchantId = merchantId;
	}

	public String getPoints()
	{
		return points;
	}

	public void setPoints(String points)
	{
		this.points = points;
	}

	public String getExDate()
	{
		return exDate;
	}

	public void setExDate(String exDate)
	{
		this.exDate = exDate;
	}

}
