package mm.dummymerchant.sk.merchantapp.model;

/**
 * Created by Dell on 11/25/2015.
 */
public class MailDetails
{
    String fromMailId;
    String toMailId;
    String password;

    public MailDetails(String fromMailId, String toMailId, String password)
    {
        this.fromMailId = fromMailId;
        this.toMailId = toMailId;
        this.password = password;
    }

    public String getFromMailId() {
        return fromMailId;
    }

    public String getToMailId() {
        return toMailId;
    }

    public String getPassword() {
        return password;
    }

}
