package mm.dummymerchant.sk.merchantapp.model;

/**
 * Created by Dell on 11/9/2015.
 */
public class MasterDetails
{

    String username;
    String password;

    public MasterDetails(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }


}
