package mm.dummymerchant.sk.merchantapp.model;

import android.database.Cursor;

import mm.dummymerchant.sk.merchantapp.db.DBHelper;


/**
 * Created by user on 2/10/2016.
 */
public class MerchantShopDetailsModel {
    String shopName;
    String shopID;
    String shopStatus;
    String shopCellID;

    public MerchantShopDetailsModel(Cursor cursor)
    {
        setShopID(cursor.getString(cursor.getColumnIndex(DBHelper.ShopID)));
        setShopName(cursor.getString(cursor.getColumnIndex(DBHelper.ShopName)));
        setShopStatus(cursor.getString(cursor.getColumnIndex(DBHelper.ShopStatus)));
        setShopCellID(cursor.getString(cursor.getColumnIndex(DBHelper.ShopCellId)));

    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopID() {
        return shopID;
    }

    public void setShopID(String shopID) {
        this.shopID = shopID;
    }

    public String getShopStatus() {
        return shopStatus;
    }

    public void setShopStatus(String shopStatus) {
        this.shopStatus = shopStatus;
    }

    public String getShopCellID() {
        return shopCellID;
    }

    public void setShopCellID(String shopCellID) {
        this.shopCellID = shopCellID;
    }

}
