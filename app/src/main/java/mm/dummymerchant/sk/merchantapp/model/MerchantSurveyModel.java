package mm.dummymerchant.sk.merchantapp.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by user on 11/9/2015.
 */
public class MerchantSurveyModel implements Parcelable {

    String question;
    String option;
    String marks;
    String total;
    String suggestion;
    String phone;
    String date;
    String time;
    int week;
    int month;
    int year;
    int hours;
    int day;


    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public int getday() {
        return day;
    }

    public void setday(int day) {
        this.day = day;
    }

    public int getWeek() {
        return week;
    }

    public void setWeek(int week) {
        this.week = week;
    }
    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }


    protected MerchantSurveyModel(Parcel in) {
        question = in.readString();
        option = in.readString();
        marks = in.readString();
        total = in.readString();
        suggestion = in.readString();
        phone = in.readString();
        date = in.readString();
        time = in.readString();
    }

    public MerchantSurveyModel() {
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getSuggestion() {
        return suggestion;
    }

    public void setSuggestion(String suggestion) {
        this.suggestion = suggestion;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public String getMarks() {
        marks=getQuestion();
        return marks;
    }

    public void setMarks(String marks) {
        this.marks = marks;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(question);
        dest.writeString(option);
        dest.writeString(marks);
        dest.writeString(total);
        dest.writeString(suggestion);
        dest.writeString(phone);
        dest.writeString(date);
        dest.writeString(time);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<MerchantSurveyModel> CREATOR = new Parcelable.Creator<MerchantSurveyModel>() {
        @Override
        public MerchantSurveyModel createFromParcel(Parcel in) {
            return new MerchantSurveyModel(in);
        }

        @Override
        public MerchantSurveyModel[] newArray(int size) {
            return new MerchantSurveyModel[size];
        }
    };
}
