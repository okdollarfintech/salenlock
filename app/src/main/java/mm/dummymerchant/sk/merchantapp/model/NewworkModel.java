package mm.dummymerchant.sk.merchantapp.model;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by abhishekmodi on 06/10/15.
 */
public class NewworkModel implements Serializable {

    String mMccCode="";
    String mCountryCode="";
    String mMncCode="";
    String mNetworkName="";
    String mOperatorName="";
    String mOperatorStatus="";

    public NewworkModel(JSONObject jsonObject, String mCC, String mCountryCode) throws Exception
    {
        if(jsonObject.has("MNC"))
            setmMccCode(jsonObject.getString("MNC"));
        if(jsonObject.has("NetworkName"))
            setmNetworkName(jsonObject.getString("NetworkName"));
        if(jsonObject.has("OperatorName"))
            setmOperatorName(jsonObject.getString("OperatorName"));
        if(jsonObject.has("Status"))
            setmOperatorStatus(jsonObject.getString("Status"));
        setmMccCode(mCC);
        setmCountryCode(mCountryCode);
    }

    public NewworkModel()
    {


    }

    public String getmMccCode() {
        return mMccCode;
    }

    public void setmMccCode(String mMccCode) {
        this.mMccCode = mMccCode;
    }

    public String getmCountryCode() {
        return mCountryCode;
    }

    public void setmCountryCode(String mCountryCode) {
        this.mCountryCode = mCountryCode;
    }

    public String getmMncCode() {
        return mMncCode;
    }

    public void setmMncCode(String mMncCode) {
        this.mMncCode = mMncCode;
    }

    public String getmNetworkName() {
        return mNetworkName;
    }

    public void setmNetworkName(String mNetworkName) {
        this.mNetworkName = mNetworkName;
    }

    public String getmOperatorName() {
        return mOperatorName;
    }

    public void setmOperatorName(String mOperatorName) {
        this.mOperatorName = mOperatorName;
    }

    public String getmOperatorStatus() {
        return mOperatorStatus;
    }

    public void setmOperatorStatus(String mOperatorStatus) {
        this.mOperatorStatus = mOperatorStatus;
    }
}
