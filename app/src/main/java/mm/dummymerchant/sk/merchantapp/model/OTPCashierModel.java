package mm.dummymerchant.sk.merchantapp.model;

/**
 * Created by user on 12/2/2015.
 */
public class OTPCashierModel {
    String Status;
    String Cashiername;
    String Cashierno;
    String Cashcollectorname;
    String Cashcollectorno;
    String Amount;
    String Time;
    String Cashierid;
    String Transid;
    String TextValue;
    String TemplateValue;

    public String getJsondata() {
        return jsondata;
    }

    public void setJsondata(String jsondata) {
        this.jsondata = jsondata;
    }

    String jsondata;


    public OTPCashierModel() {
    }
    public OTPCashierModel(String time,String cashiername,String cashierno, String cashcollectorname,String cashcollectorno,String amount,String status,String cashierid,String transid,String textValue,String templateValue,String jsondata) {
        Cashiername =cashiername;
        Cashierno = cashierno;
        Cashcollectorname = cashcollectorname;
        Cashcollectorno = cashcollectorno;
        Amount = amount;
        Status = status;
        Time = time;
        Cashierid = cashierid;
        Transid = transid ;
        TextValue = textValue;
        TemplateValue = templateValue;
        this.jsondata=jsondata;

    }
    public String getCashiername() {
        return Cashiername;
    }

    public void setCashiername(String Cashiername)
    {
        this.Cashiername=Cashiername;
    }

    public String getCashierno() {
        return Cashierno;
    }

    public void setCashierno(String Cashierno)
    {
        this.Cashierno=Cashierno;
    }

    public String getCashcollectorname() {
        return Cashcollectorname;
    }

    public void setCashcollectorname(String Cashcollectorname)
    {
        this.Cashcollectorname=Cashcollectorname;
    }

    public String getCashcollectorno() {
        return Cashcollectorno;
    }

    public void setCashcollectorno(String Cashcollectorno)
    {
        this.Cashcollectorno=Cashcollectorno;
    }
    public void setAmount(String Amount)
    {
        this.Amount=Amount;
    }

    public String getAmout() {
        return Amount;
    }

    public void setStatus(String Status)
    {
        this.Status=Status;
    }
    public String getStatus() {
        return Status;
    }

    public void setTime(String Time)
    {
        this.Time=Time;
    }
    public String getTime() {
        return Time;
    }

    public void setCashierid(String Cashierid)
    {
        this.Cashierid=Cashierid;
    }
    public String getCashierid() {
        return Cashierid;
    }

    public void setTransid(String Transid)
    {
        this.Transid=Transid;
    }
    public String getTransid() {
        return Transid;
    }

    public void setTextValue(String TextValue)
    {
        this.TextValue=TextValue;
    }
    public String getTextValue() {
        return TextValue;
    }
    public void setTemplateValue(String TemplateValue)
    {
        this.TemplateValue=TemplateValue;
    }
    public String getTemplateValue() {
        return TemplateValue;
    }
}
