package mm.dummymerchant.sk.merchantapp.model;



import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

import mm.dummymerchant.sk.merchantapp.Utils.XMLTag;

public class OTPModel implements XMLTag, Serializable
{

	String agentNo = "";
	String transId = "";
	String description = "";
	String date = "";
	String Otp = "";

	public OTPModel(String response) throws XmlPullParserException, IOException
	{
		XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
		XmlPullParser myparser = factory.newPullParser();
		myparser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
		InputStream stream = new ByteArrayInputStream(response.getBytes());
		myparser.setInput(stream, null);
		parseXML(myparser);
		stream.close();
	}

	void parseXML(XmlPullParser myParser) throws XmlPullParserException, IOException
	{
		int event;
		String text = null;
		event = myParser.getEventType();
		while (event != XmlPullParser.END_DOCUMENT)
		{
			String name = myParser.getName();
			switch (event) {
				case XmlPullParser.START_TAG:
					break;
				case XmlPullParser.TEXT:
					text = myParser.getText();
					break;
				case XmlPullParser.END_TAG:
					if (name.equals(TAG_RESPONSE_TYPE))
						System.out.print(text);
					else if (name.equals(TAG_AGENTCODE))
						setAgentNo(text);
					else if (name.equals(TAG_DESCRIPTION))
						setDescription(text);
					else if (name.equals(TAG_OTP))
						setOtp(text);
					else if (name.equals(TAG_DATE))
						setDate(text);
					else if (name.equals(TAG_TRANSID))
						setTransId(text);
					break;
			}
			event = myParser.next();
		}
	}

	public String getAgentNo()
	{
		return agentNo;
	}

	public void setAgentNo(String agentNo)
	{
		this.agentNo = agentNo;
	}

	public String getTransId()
	{
		return transId;
	}

	public void setTransId(String transId)
	{
		this.transId = transId;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public String getDate()
	{
		return date;
	}

	public void setDate(String date)
	{
		this.date = date;
	}

	public String getOtp()
	{
		return Otp;
	}

	public void setOtp(String otp)
	{
		Otp = otp;
	}

}
