package mm.dummymerchant.sk.merchantapp.model;

/**
 * Created by Dell on 3/4/2016.
 */
public class PhoneContactModel {
    String name;
    String countryCode;
    String phoneNo;
    int int_flag_image;
    String completePhoneNo;



    String otp;
    boolean status;

    public PhoneContactModel(String name, String countryCode, String phoneNo, int int_flag_image, String completePhoneNo, String otp, boolean status)
    {
        this.name = name;
        this.countryCode = countryCode;
        this.phoneNo = phoneNo;
        this.int_flag_image = int_flag_image;
        this.completePhoneNo=completePhoneNo;
        this.otp=otp;
        this.status=status;
    }

    public String getName()
    {
        return name;
    }

    public String getCountryCode()
    {
        return countryCode;
    }

    public String getPhoneNo()
    {
        return phoneNo;
    }

    public int getInt_flag_image()
    {
        return int_flag_image;
    }

    public String getCompletePhoneNo() {
        return completePhoneNo;
    }

    public String getOtp() {
        return otp;
    }
    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

}
