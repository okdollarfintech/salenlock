package mm.dummymerchant.sk.merchantapp.model;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;

public class PortalAdminOTPModel implements Serializable
{

	String number = "";
	String otp = "";

	public PortalAdminOTPModel(JSONObject object) throws Exception
	{
        if(object.has("Table"))
        {
            JSONArray array=object.getJSONArray("Table");
            JSONObject json=array.getJSONObject(0);
            if(json.has("OTP"))
            {
                if (json.has("OTP"))
                    setOtp(json.getString("OTP"));
            }

        }
	}

	public String getNumber()
	{
		return number;
	}

	public void setNumber(String number)
	{
		this.number = number;
	}

	public String getOtp()
	{
		return otp;
	}

	public void setOtp(String otp)
	{
		this.otp = otp;
	}

}
