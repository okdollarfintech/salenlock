package mm.dummymerchant.sk.merchantapp.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Profile_ implements Serializable {

    public String MobileNumber = "";
    public String Name = "";
    public String GcmID = "";
    public String Encrypted = "";
    public String SimID = "";
    public String MSID = "";
    public String IMEI = "";
    public String lType = "";
    public String AppID = "";
    public String Recommended = "";
    public String State = "";
    public String Township = "";
    public String Father = "";
    public Boolean Gender = false;
    public String DateOfBirth = "";
    public String NRC = "";
    public String IDType = "";
    public String Phone = "";
    public String BusinessType = "";
    public String BusinessCategory = "";
    public String Car = "";
    public int CarType = -1;
    public String Latitude = "0.0";
    public String Longitude = "0.0";
    public String Address1 = "";
    public String Address2 = "";
    public int AccountType = -1;
    public int OSType = -1;
    public String ProfilePic="";
    public String signature="";
    public String Password="";
    public String EmailId="";
    public String AddressType="";
    public String CellTowerID="";
    public String BusinessName="";
    public boolean Kickback=false;
    public String CodeAlternate="";
    public String Country="";
    public String ParentAccount="";
    public String CodeRecommended="";
    public List<BankDetailsModel> BankDetails= new ArrayList<>();
    public List<CashBankOutNoModel> Msisdn= new ArrayList<>();
    public boolean PaymentGateway=false;
    public boolean Loyalty=false;
    public String SecureToken="";
    public String Language="";
    public String FBEmailId="";
    public String CountryCode="" ;


    public Profile_ getDataIntoModel(String response) {
        Profile_ model= null;
        try {

            //response = Utils.readTextFileFromAssets(context, "update_profile.txt");
            JSONObject jsonObject = new JSONObject(response);
            String status = jsonObject.getString("Code");
            if (status.equals("200")) {
                // Utils.showToast(context, status);
                String data = jsonObject.getString("Data");
                JSONObject jsonObjectData = new JSONObject(data);
                JSONArray jsonObjectTable = jsonObjectData.getJSONArray("ProfileDetails");
                for (int i = 0; i < jsonObjectTable.length(); i++) {
                    model = new Profile_();
                    JSONObject jsonObject1 = jsonObjectTable.getJSONObject(i);
                    model.setMobileNumber(jsonObject1.getString("Mobilenumber"));
                    model.setName(jsonObject1.getString("Name"));
                    model.setGcmID(jsonObject1.getString("GCMID"));
                    model.setEncrypted(jsonObject1.getString("EncryptedKey"));
                    model.setSimID(jsonObject1.getString("SimID"));
                    model.setMSID(jsonObject1.getString("MSID"));
                    model.setIMEI(jsonObject1.getString("IMEI"));
                    model.setlType(jsonObject1.getString("AgentType"));
                    model.setAppID(jsonObject1.getString("AppID"));
                    model.setRecommended(jsonObject1.getString("Recommendedby"));
                    model.setState(jsonObject1.getString("State"));
                    model.setTownship(jsonObject1.getString("Township"));
                    model.setFather(jsonObject1.getString("FatherName"));
                    model.setGender(jsonObject1.getBoolean("Gender"));
                    model.setDateOfBirth(jsonObject1.getString("DOB"));
                    model.setNRC(jsonObject1.getString("NRC"));
                    model.setIDType(jsonObject1.getString("IDType"));
                    model.setPhone(jsonObject1.getString("PhoneNumber"));
                    model.setBusinessType(jsonObject1.getString("BusinessType"));
                    model.setBusinessCategory(jsonObject1.getString("BusinessCategory"));
                    model.setCar(jsonObject1.getString("Car"));
                    model.setCarType(jsonObject1.getInt("CarType"));
                    model.setLatitude(jsonObject1.getString("Latitude"));
                    model.setLongitude(jsonObject1.getString("Longitude"));
                    model.setAddress1(jsonObject1.getString("Address1"));
                    model.setAddress2(jsonObject1.getString("Address2"));
                    model.setAccountType(jsonObject1.getInt("AccountType"));
                    model.setOSType(jsonObject1.getInt("OSType"));
                    model.setProfilePic(jsonObject1.getString("ProfilePic"));
                    model.setsignature(jsonObject1.getString("SignaturePic"));
                    //model.setPassword(jsonObject1.getString(""));
                    model.setEmailID(jsonObject1.getString("EmailId"));
                    model.setAddressType(jsonObject1.getString("AddressType"));
                    model.setCellTowerID(jsonObject1.getString("CellTowerID"));
                    model.setBusinessName(jsonObject1.getString("BusinessName"));
                    //model.setKickback(jsonObject1.getBoolean(""));
                    model.setCodeAlternate(jsonObject1.getString("CodeAlternateNumber"));
                    model.setCountry(jsonObject1.getString("Country"));
                    model.setParentAccount(jsonObject1.getString("ParentAccount"));
                    model.setCodeRecommended(jsonObject1.getString("CodeRecommended"));
                    /*model.setPaymentGateway(jsonObject1.getString("BankDetails"));
                    model.setLoyalty(jsonObject1.getString("Mobilenumber"));
                    model.setSecureToken(jsonObject1.getString("Mobilenumber"));
                    model.setAccountNumber(jsonObject1.getString("Mobilenumber"));
                    model.setBankName(jsonObject1.getString("Mobilenumber"));*/
                   /* JSONArray bankArray=jsonObject1.getJSONArray("BankDetails");
                    for (int j=0; j<bankArray.length(); j++){
                        BankDetailsModel bankDetailsModel= new BankDetailsModel();
                        JSONObject bankObject=bankArray.getJSONObject(j);
                        bankDetailsModel.setAccountNumber(bankObject.getString("AccountNumber"));
                        bankDetailsModel.setAccountType(bankObject.getString("AccountType"));
                        bankDetailsModel.setBankName(bankObject.getString("BankName"));
                        bankDetailsModel.setBranch(bankObject.getString("Branch"));
                        BankDetails.add(bankDetailsModel);
                    }
                    JSONArray msidnArray=jsonObject1.getJSONArray("MsisdnDetails");
                    for (int j=0; j<msidnArray.length(); j++){
                        CashBankOutNoModel msidn= new CashBankOutNoModel();
                        JSONObject bankObject=msidnArray.getJSONObject(j);
                        msidn.setMsisidn(bankObject.getString("Msisdn"));
                        msidn.setCountrycode(bankObject.getString("CountryCode"));
                        Msisdn.add(msidn);
                    }*/
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.v("Exception", e.toString());
        }

        return model;
    }

    public String getCellTowerID() {
        return CellTowerID;
    }

    public void setCellTowerID(String cellTowerID) {
        CellTowerID = cellTowerID;
    }

    public String getAddressType() {

        return AddressType;
    }

    public void setAddressType(String addressType) {

        AddressType = addressType;
    }

    public String getBusinessName() {
        return BusinessName;
    }

    public void setBusinessName(String businessName) {
        BusinessName = businessName;
    }

    public String getEmailID() {
        return EmailId;
    }

    public void setEmailID(String emailID) {
        EmailId = emailID;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getProfilePic() {
        return ProfilePic;
    }

    public void setProfilePic(String profilePic) {
        ProfilePic = profilePic;
    }

    public String getsignature() {
        return signature;
    }

    public void setsignature(String signature) {
        this.signature = signature;
    }

    public String getGcmID() {
        return GcmID;
    }

    public void setGcmID(String gcmID) {
        GcmID = gcmID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public String getEncrypted() {
        return Encrypted;
    }

    public void setEncrypted(String encrypted) {
        Encrypted = encrypted;
    }

    public String getSimID() {
        return SimID;
    }

    public void setSimID(String simID) {
        SimID = simID;
    }

    public String getMSID() {
        return MSID;
    }

    public void setMSID(String MSID) {
        this.MSID = MSID;
    }

    public String getIMEI() {
        return IMEI;
    }

    public void setIMEI(String IMEI) {
        this.IMEI = IMEI;
    }

    public String getlType() {
        return lType;
    }

    public void setlType(String lType) {
        this.lType = lType;
    }

    public String getAppID() {
        return AppID;
    }

    public void setAppID(String appID) {
        AppID = appID;
    }

    public String getRecommended() {
        return Recommended;
    }

    public void setRecommended(String recommended) {
        Recommended = recommended;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getTownship() {
        return Township;
    }

    public void setTownship(String township) {
        Township = township;
    }

    public String getFather() {
        return Father;
    }

    public void setFather(String father) {
        Father = father;
    }

    public Boolean getGender() {
        return Gender;
    }

    public void setGender(Boolean gender) {
        Gender = gender;
    }

    public String getNRC() {
        return NRC;
    }

    public void setNRC(String NRC) {
        this.NRC = NRC;
    }

    public String getDateOfBirth() {
        return DateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        DateOfBirth = dateOfBirth;
    }

    public String getIDType() {
        return IDType;
    }

    public void setIDType(String IDType) {
        this.IDType = IDType;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getBusinessType() {
        return BusinessType;
    }

    public void setBusinessType(String businessType) {
        BusinessType = businessType;
    }

    public String getBusinessCategory() {
        return BusinessCategory;
    }

    public void setBusinessCategory(String businessCategory) {
        BusinessCategory = businessCategory;
    }

    public String getCar() {
        return Car;
    }

    public void setCar(String car) {
        Car = car;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public int getCarType() {
        return CarType;
    }

    public void setCarType(int carType) {
        CarType = carType;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getAddress1() {
        return Address1;
    }

    public void setAddress1(String address1) {
        Address1 = address1;
    }

    public String getAddress2() {
        return Address2;
    }

    public void setAddress2(String address2) {
        Address2 = address2;
    }

    public int getAccountType() {
        return AccountType;
    }

    public void setAccountType(int accountType) {
        AccountType = accountType;
    }

    public int getOSType() {
        return OSType;
    }

    public void setOSType(int OSType) {
        this.OSType = OSType;
    }

    public void setKickback(Boolean kickback) {
        Kickback = kickback;
    }

    public Boolean getKickback() {
        return Kickback;
    }


    public boolean isKickback() {
        return Kickback;
    }

    public void setKickback(boolean kickback) {
        Kickback = kickback;
    }

    public String getCodeAlternate() {
        return CodeAlternate;
    }

    public void setCodeAlternate(String codeAlternate) {
        CodeAlternate = codeAlternate;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getParentAccount() {
        return ParentAccount;
    }

    public void setParentAccount(String parentAccount) {
        ParentAccount = parentAccount;
    }

    public String getCodeRecommended() {
        return CodeRecommended;
    }

    public void setCodeRecommended(String codeRecommended) {
        CodeRecommended = codeRecommended;
    }

    public List<BankDetailsModel> getBankDetails() {
        return BankDetails;
    }

    public void setBankDetails(List<BankDetailsModel> bankDetails) {
        BankDetails = bankDetails;
    }

    public List<CashBankOutNoModel> getMsisdn() {
        return Msisdn;
    }

    public void setMsisdn(List<CashBankOutNoModel> msisdn) {
        Msisdn = msisdn;
    }

    public boolean isPaymentGateway() {
        return PaymentGateway;
    }

    public void setPaymentGateway(boolean paymentGateway) {
        PaymentGateway = paymentGateway;
    }

    public boolean isLoyalty() {
        return Loyalty;
    }

    public void setLoyalty(boolean loyalty) {
        Loyalty = loyalty;
    }

    public String getSecureToken() {
        return SecureToken;
    }

    public void setSecureToken(String secureToken) {
        SecureToken = secureToken;
    }


    public String getFBEmailId() {
        return FBEmailId;
    }

    public void setFBEmailId(String FBEmailId) {
        this.FBEmailId = FBEmailId;
    }

    public String getLanguage() {
        return Language;
    }

    public void setLanguage(String language) {
        Language = language;
    }

    public String getCountryCode() {
        return CountryCode;
    }

    public void setCountryCode(String countryCode) {
        CountryCode = countryCode;
    }
}