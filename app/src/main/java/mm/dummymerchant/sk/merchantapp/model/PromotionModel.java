package mm.dummymerchant.sk.merchantapp.model;

import android.database.Cursor;

import org.json.JSONObject;

import mm.dummymerchant.sk.merchantapp.db.DBHelper;


/**
 * Created by abhishekmodi on 13/02/16.
 */
public class PromotionModel {

    String title = "";
    String descripton = "";
    String mobileNo = "";
    String website = "";
    String facebook = "";
    String id = "";
    String email="";
    String last_updated="";
    String name="";


    public  PromotionModel(){

    }
    public PromotionModel(Cursor c) {
        setId(c.getString(c.getColumnIndex(DBHelper.PROMOTIONID)));
        setTitle(c.getString(c.getColumnIndex(DBHelper.PROMOTION_TITLE)));
        setDescripton(c.getString(c.getColumnIndex(DBHelper.PROMOTION_DESCRIPTION)));
        setMobileNo(c.getString(c.getColumnIndex(DBHelper.PROMOTION_MOBILENO)));
        setFacebook(c.getString(c.getColumnIndex(DBHelper.PROMOTION_FACEBOOK)));
        setWebsite(c.getString(c.getColumnIndex(DBHelper.PROMOTION_WEBSITE)));
        setEmail(c.getString(c.getColumnIndex(DBHelper.EMAIL)));
        setName(c.getString(c.getColumnIndex(DBHelper.CONTACT_NAME)));
        setLast_updated(c.getString(c.getColumnIndex(DBHelper.LAST_UPDATED)));
    }

    public PromotionModel(JSONObject object) throws Exception {
        if (object.has("PromotionHeader"))
            setTitle(object.getString("PromotionHeader"));
        if (object.has("Description"))
            setDescripton(object.getString("Description"));
        if (object.has("WebUrl"))
            setWebsite(object.getString("WebUrl"));
        if (object.has("FacebookId"))
            setFacebook(object.getString("FacebookId"));
        if (object.has("PhoneNumber"))
            setMobileNo(object.getString("PhoneNumber"));
        if (object.has("ContactPersonName"))
            setName(object.getString("ContactPersonName"));
        if (object.has("EmailId"))
            setEmail(object.getString("EmailId"));
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescripton() {
        return descripton;
    }

    public void setDescripton(String descripton) {
        this.descripton = descripton;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLast_updated() {
        return last_updated;
    }

    public void setLast_updated(String last_updated) {
        this.last_updated = last_updated;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
