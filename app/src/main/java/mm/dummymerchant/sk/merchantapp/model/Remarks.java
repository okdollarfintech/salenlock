package mm.dummymerchant.sk.merchantapp.model;

/**
 * Created by Dell on 7/5/2016.
 */
public class Remarks
{
    String transId;
    String cellId;
    String latitude;
    String longitude;
    String mnc;
    String mcc;

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    String comments;

    public String getTransId() {
        return transId;
    }

    public String getCellId() {
        return cellId;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getMnc() {
        return mnc;
    }

    public String getMcc() {
        return mcc;
    }

    public String getTelePhoneOperator() {
        return telePhoneOperator;
    }



    String telePhoneOperator;


    public Remarks(String transId, String cellId, String latitude, String longitude, String mnc, String mcc, String telePhoneOperator) {
        this.transId = transId;
        this.cellId = cellId;
        this.latitude = latitude;
        this.longitude = longitude;
        this.mnc = mnc;
        this.mcc = mcc;
        this.telePhoneOperator = telePhoneOperator;

    }



}
