package mm.dummymerchant.sk.merchantapp.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by kapil on 20/10/15.
 */
public class RetrieveProfile_Model {

    public String name="", id="", phoneNumber="", password="", agentType="",
            updatedDate="", createdDate="", emailID="" , Id1="", agentID="",
            EncryptedKey="",IMEI="", SimID="", MSID="", AppID="", createddate1="",
            updatedDate1="", FatherName="", DOB="", NRC="", IDType="", PhoneNumber1="",
            BusinessType="", BusinessCategory="", BusinessName="", State="", Township="", addressLine1="", addressLine2="", Country=""
            , AddressType="", Latitude="", Longitude="", CellTowerID="", OSType="";
    public int isActive=-1, accountType=-1 ;
    public boolean Gender=false;

    private String PredefineNumber="", RecommendedNo="";


    public RetrieveProfile_Model getDataIntoModel(String response) {
        RetrieveProfile_Model model= null;
        try {

            //response = Utils.readTextFileFromAssets(context, "update_profile.txt");
            JSONObject jsonObject = new JSONObject(response);
            String status = jsonObject.getString("Code");
            if (status.equals("200")) {
               // Utils.showToast(context, status);
                    String data = jsonObject.getString("Data");
                    JSONObject jsonObjectData = new JSONObject(data);
                    JSONArray jsonObjectTable = jsonObjectData.getJSONArray("Table");
                    for (int i = 0; i < jsonObjectTable.length(); i++) {
                        model = new RetrieveProfile_Model();
                        JSONObject jsonObject1 = jsonObjectTable.getJSONObject(i);
                        model.setName(jsonObject1.getString("Name"));
                        model.setPhoneNumber(jsonObject1.getString("PhoneNumber"));
                        model.setFatherName(jsonObject1.getString("FatherName"));
                        model.setDOB(jsonObject1.getString("DOB"));
                        model.setNRC(jsonObject1.getString("NRC"));
                        model.setAddressType(jsonObject1.getString("AddressType"));
                        model.setState(jsonObject1.getString("State"));
                        model.setTownship(jsonObject1.getString("Township"));
                        model.setAddressLine1(jsonObject1.getString("Line1"));
                        model.setAddressLine2(jsonObject1.getString("Line2"));
                        //model.setEmailID(jsonObject1.getString("EmailID"));
                        model.setEmailID("");
                        //model.setPhoneNumber1(jsonObject1.getString("PhoneNumber1"));
                        //model.setBusinessType(jsonObject1.getString("BusinessType"));
                        model.setBusinessType("");
                        //model.setBusinessName(jsonObject1.getString("BusinessName"));
                        model.setBusinessName("");
                        //model.setBusinessCategory(jsonObject1.getString("BusinessCategory"));
                        model.setAccountType(jsonObject1.getInt("AccountType"));
                        model.setIsActive(jsonObject1.getInt("Is_Active"));
                        //model.setBusinessCategory(jsonObject1.getString("BusinessCategory"));
                    }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return model;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAgentType() {
        return agentType;
    }

    public void setAgentType(String agentType) {
        this.agentType = agentType;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getEmailID() {
        return emailID;
    }

    public void setEmailID(String emailID) {
        this.emailID = emailID;
    }

    public String getId1() {
        return Id1;
    }

    public void setId1(String id1) {
        Id1 = id1;
    }

    public String getAgentID() {
        return agentID;
    }

    public void setAgentID(String agentID) {
        this.agentID = agentID;
    }

    public String getEncryptedKey() {
        return EncryptedKey;
    }

    public void setEncryptedKey(String encryptedKey) {
        EncryptedKey = encryptedKey;
    }

    public String getIMEI() {
        return IMEI;
    }

    public void setIMEI(String IMEI) {
        this.IMEI = IMEI;
    }

    public String getSimID() {
        return SimID;
    }

    public void setSimID(String simID) {
        SimID = simID;
    }

    public String getAppID() {
        return AppID;
    }

    public void setAppID(String appID) {
        AppID = appID;
    }

    public String getCreateddate1() {
        return createddate1;
    }

    public void setCreateddate1(String createddate1) {
        this.createddate1 = createddate1;
    }

    public String getMSID() {
        return MSID;
    }

    public void setMSID(String MSID) {
        this.MSID = MSID;
    }

    public String getFatherName() {
        return FatherName;
    }

    public void setFatherName(String fatherName) {
        FatherName = fatherName;
    }

    public String getUpdatedDate1() {
        return updatedDate1;
    }

    public void setUpdatedDate1(String updatedDate1) {
        this.updatedDate1 = updatedDate1;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getNRC() {
        return NRC;
    }

    public void setNRC(String NRC) {
        this.NRC = NRC;
    }

    public String getIDType() {
        return IDType;
    }

    public void setIDType(String IDType) {
        this.IDType = IDType;
    }

    public String getBusinessType() {
        return BusinessType;
    }

    public void setBusinessType(String businessType) {
        BusinessType = businessType;
    }

    public String getPhoneNumber1() {
        return PhoneNumber1;
    }

    public void setPhoneNumber1(String phoneNumber1) {
        PhoneNumber1 = phoneNumber1;
    }

    public String getBusinessName() {
        return BusinessName;
    }

    public void setBusinessName(String businessName) {
        BusinessName = businessName;
    }

    public String getBusinessCategory() {
        return BusinessCategory;
    }

    public void setBusinessCategory(String businessCategory) {
        BusinessCategory = businessCategory;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getTownship() {
        return Township;
    }

    public void setTownship(String township) {
        Township = township;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getAddressType() {
        return AddressType;
    }

    public void setAddressType(String addressType) {
        AddressType = addressType;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getCellTowerID() {
        return CellTowerID;
    }

    public void setCellTowerID(String cellTowerID) {
        CellTowerID = cellTowerID;
    }

    public String getOSType() {
        return OSType;
    }

    public void setOSType(String OSType) {
        this.OSType = OSType;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public int getAccountType() {
        return accountType;
    }

    public void setAccountType(int accountType) {
        this.accountType = accountType;
    }

    public boolean isGender() {
        return Gender;
    }

    public void setGender(boolean gender) {
        Gender = gender;
    }


    public String getPredefineNumber() {
        return PredefineNumber;
    }

    public void setPredefineNumber(String predefineNumber) {
        PredefineNumber = predefineNumber;
    }

    public String getRecommendedNo() {
        return RecommendedNo;
    }

    public void setRecommendedNo(String recommendedNo) {
        RecommendedNo = recommendedNo;
    }
}
