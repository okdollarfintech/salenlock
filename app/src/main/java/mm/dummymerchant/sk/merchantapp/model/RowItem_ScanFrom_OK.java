package mm.dummymerchant.sk.merchantapp.model;

/**
 * Created by user on 1/22/2016.
 */
public class RowItem_ScanFrom_OK
{
    private String SetOK_ID,SetOK_TIME_STAMP,SetOK_DATE,SetOK_RECEIVER_NAME_No;
    private String SetOK_AMOUNT,SetOK_TRANSID,SetOK_SENDER_NO,SetOK_RECEIVER_NO,SetOK_TRANS_TYPE;
    private String SetOK_KICK,SetOK_LOCALITY,SetOK_LOCATION,SetOK_CELL_ID,SetOK_GENDER;
    private String SetOK_AGE,SetOK_STATUS,GENERATE_LOCATION,SCANNING_LOCATION;


        public RowItem_ScanFrom_OK()
        {

        }

        public void SetOK_ID(String Scanning_Time)
        {
            this.SetOK_ID=SetOK_ID;
        }
        public String GetOK_ID()
        {
            return SetOK_ID;
        }

        public void SetOK_TIME_STAMP(String SetOK_TIME_STAMP)
        {
            this.SetOK_TIME_STAMP=SetOK_TIME_STAMP;
        }
        public String GetOK_TIME_STAMP()
        {
            return SetOK_TIME_STAMP;
        }


        public void SetOK_DATE(String SetOK_DATE)
        {
            this.SetOK_DATE=SetOK_DATE;
        }
        public String GetOK_DATE()
        {
            return SetOK_DATE;
        }

        public void SetOK_RECEIVER_NAME_No(String SetOK_RECEIVER_NAME_No)
        {
            this.SetOK_RECEIVER_NAME_No=SetOK_RECEIVER_NAME_No;
        }
        public String GetOK_RECEIVER_NAME_No()
        {
            return SetOK_RECEIVER_NAME_No;
        }

        public void SetOK_AMOUNT(String SetOK_AMOUNT)
        {
            this.SetOK_AMOUNT=SetOK_AMOUNT;
        }
        public String GetOK_AMOUNT()
        {
            return SetOK_AMOUNT;
        }

        public void SetOK_TRANSID(String SetOK_TRANSID)
        {
            this.SetOK_TRANSID=SetOK_TRANSID;
        }
        public String GetOK_TRANSID()
        {
            return SetOK_TRANSID;
        }

        public void SetOK_SENDER_NO(String SetOK_SENDER_NO)
        {
            this.SetOK_SENDER_NO=SetOK_SENDER_NO;
        }
        public String GetOK_SENDER_NO()
        {
            return SetOK_SENDER_NO;
        }

        public void SetOK_RECEIVER_NO(String SetOK_RECEIVER_NO)
        {
            this.SetOK_RECEIVER_NO=SetOK_RECEIVER_NO;
        }
        public String GetOK_RECEIVER_NO()
        {
            return SetOK_RECEIVER_NO;
        }

        public void SetOK_TRANS_TYPE(String SetOK_TRANS_TYPE)
        {
            this.SetOK_TRANS_TYPE=SetOK_TRANS_TYPE;
        }
        public String GetOK_TRANS_TYPE()
        {
            return SetOK_TRANS_TYPE;
        }

        public void SetOK_KICK(String SetOK_KICK)
        {
            this.SetOK_KICK=SetOK_KICK;
        }
        public String GetOK_KICK()
        {
            return SetOK_KICK;
        }


        public void SetOK_LOCALITY(String SetOK_LOCALITY)
        {
            this.SetOK_LOCALITY=SetOK_LOCALITY;
        }
        public String GetOK_LOCALITY()
        {
            return SetOK_LOCALITY;
        }

        public void SetOK_LOCATION(String SetOK_LOCATION)
        {
            this.SetOK_LOCATION=SetOK_LOCATION;
        }
        public String GetOK_LOCATION()
        {
            return SetOK_LOCATION;
        }


        public void SetOK_CELL_ID(String SetOK_CELL_ID)
        {
            this.SetOK_CELL_ID=SetOK_CELL_ID;
        }
        public String GetOK_CELL_ID()
        {
            return SetOK_CELL_ID;
        }


        public void SetOK_GENDER(String SetOK_GENDER)
        {
            this.SetOK_GENDER=SetOK_GENDER;
        }
        public String GetOK_GENDER()
        {
            return SetOK_GENDER;
        }

        public void SetOK_AGE(String SetOK_AGE)
        {
            this.SetOK_AGE=SetOK_AGE;
        }
        public String GetOK_AGE()
        {
            return SetOK_AGE;
        }

        public void SetOK_STATUS(String SetOK_STATUS)
        {
            this.SetOK_STATUS=SetOK_STATUS;
        }
        public String GetOK_STATUS() {
            return SetOK_STATUS;
        }


    public void SetGENERATE_LOCATION(String GENERATE_LOCATION)
    {
        this.GENERATE_LOCATION=GENERATE_LOCATION;
    }
    public String GetGENERATE_LOCATION() {
        return GENERATE_LOCATION;
    }


    public void SetSCANNING_LOCATION(String SCANNING_LOCATION)
    {
        this.SCANNING_LOCATION=SCANNING_LOCATION;
    }
    public String GetSCANNING_LOCATION() {
        return SCANNING_LOCATION;
    }

}

