package mm.dummymerchant.sk.merchantapp.model;

/**
 * Created by user on 11/20/2015.
 */
public class RowItem_cashcollector
{
    private String Scanning_Time,Generate_Time,OTP_Enter_Time;
    private String Scanner_Name,Log_Trans_Id,Scanner_No,Generater_No,Generater_Name;
    private String Log_Amount,Log_Subject,Log_Status,Log_Paid_Status,openorclose;
    private String Scanning_Location,Generate_Location,OTP_Enter_Location,TextValue,TemplateValue;
    private String qr_id;

    public String getJson_data() {
        return json_data;
    }

    public void setJson_data(String json_data) {
        this.json_data = json_data;
    }

    public String getQr_id() {
        return qr_id;
    }

    public void setQr_id(String qr_id) {
        this.qr_id = qr_id;
    }

    private String json_data;

    public RowItem_cashcollector()
    {

    }

    public void SetScanning_Time(String Scanning_Time)
    {
        this.Scanning_Time=Scanning_Time;
    }
    public String GetScanning_Time()
    {
        return Scanning_Time;
    }

    public void SetGenerate_Time(String Generate_Time)
    {
        this.Generate_Time=Generate_Time;
    }
    public String GetGenerate_Time()
    {
        return Generate_Time;
    }


    public void setOTP_Enter_Time(String OTP_Enter_Time)
    {
        this.OTP_Enter_Time=OTP_Enter_Time;
    }
    public String GetOTP_Enter_Time()
    {
        return OTP_Enter_Time;
    }

    public void SetScanner_Name(String Scanner_Name)
    {
        this.Scanner_Name=Scanner_Name;
    }
    public String GetScanner_Name()
    {
        return Scanner_Name;
    }

    public void SetLog_Trans_Id(String Log_Trans_Id)
    {
        this.Log_Trans_Id=Log_Trans_Id;
    }
    public String GetLog_Trans_Id()
    {
        return Log_Trans_Id;
    }

    public void SetScanner_No(String Scanner_No)
    {
        this.Scanner_No=Scanner_No;
    }
    public String GetScanner_No()
    {
        return Scanner_No;
    }

    public void SetLog_Subject(String Log_Subject)
    {
        this.Log_Subject=Log_Subject;
    }
    public String GetLog_Subject()
    {
        return Log_Subject;
    }

    public void SetLog_Amount(String Log_Amount)
    {
        this.Log_Amount=Log_Amount;
    }
    public String GetLog_Amount()
    {
        return Log_Amount;
    }

    public void SetLog_Status(String Log_Status)
    {
        this.Log_Status=Log_Status;
    }
    public String GetLog_Status()
    {
        return Log_Status;
    }

    public void SetLog_Paid_Status(String Log_Paid_Status)
    {
        this.Log_Paid_Status=Log_Paid_Status;
    }
    public String GetLog_Paid_Status()
    {
        return Log_Paid_Status;
    }


    public void SetScanning_Location(String Scanning_Location)
    {
        this.Scanning_Location=Scanning_Location;
    }
    public String GetScanning_Location()
    {
        return Scanning_Location;
    }

    public void SetGenerate_Location(String Generate_Location)
    {
        this.Generate_Location=Generate_Location;
    }
    public String GetGenerate_Location()
    {
        return Generate_Location;
    }


    public void SetOTP_Enter_Location(String OTP_Enter_Location)
    {
        this.OTP_Enter_Location=OTP_Enter_Location;
    }
    public String GetOTP_Enter_Location()
    {
        return OTP_Enter_Location;
    }


    public void SetGenerater_No(String Generater_No)
    {
        this.Generater_No=Generater_No;
    }
    public String GetGenerater_No()
    {
        return Generater_No;
    }

    public void SetGenerater_Name(String Generater_Name)
    {
        this.Generater_Name=Generater_Name;
    }
    public String GetGenerater_Name()
    {
        return Generater_Name;
    }

    public void setTextValue(String TextValue)
    {
        this.TextValue=TextValue;
    }
    public String getTextValue() {
        return TextValue;
    }
    public void setTemplateValue(String TemplateValue)
    {
        this.TemplateValue=TemplateValue;
    }
    public String getTemplateValue() {
        return TemplateValue;
    }

    public void setopenStatus(String openorclose)
    {
        this.openorclose=openorclose;
    }
    public String getopenorclose() {
        return openorclose;
    }

}
