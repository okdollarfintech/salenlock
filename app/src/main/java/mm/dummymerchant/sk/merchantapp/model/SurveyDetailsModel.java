package mm.dummymerchant.sk.merchantapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by user on 11/9/2015.
 */
public class SurveyDetailsModel implements Parcelable
{
    ArrayList<MerchantSurveyModel> surveyModelArrayList;
    int week;
    int month;
    int year;
    int hours;
    int day;
    String date;
    Double total;

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }


    public SurveyDetailsModel() {
    }

    public int getWeek() {
        return week;
    }

    public void setWeek(int week) {
        this.week = week;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    protected SurveyDetailsModel(Parcel in) {
        if (in.readByte() == 0x01) {
            surveyModelArrayList = new ArrayList<MerchantSurveyModel>();
            in.readList(surveyModelArrayList, MerchantSurveyModel.class.getClassLoader());
        } else {
            surveyModelArrayList = null;
        }
        week = in.readInt();
        month = in.readInt();
        year = in.readInt();
        hours = in.readInt();
        day=in.readInt();
        date=in.readString();
        total=in.readDouble();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (surveyModelArrayList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(surveyModelArrayList);
        }
        dest.writeInt(week);
        dest.writeInt(month);
        dest.writeInt(year);
        dest.writeInt(hours);
        dest.writeInt(day);
        dest.writeString(date);
        dest.writeDouble(total);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<SurveyDetailsModel> CREATOR = new Parcelable.Creator<SurveyDetailsModel>() {
        @Override
        public SurveyDetailsModel createFromParcel(Parcel in) {
            return new SurveyDetailsModel(in);
        }

        @Override
        public SurveyDetailsModel[] newArray(int size) {
            return new SurveyDetailsModel[size];
        }
    };
    public ArrayList<MerchantSurveyModel> getSurveyModelArrayList() {
        return surveyModelArrayList;
    }

    public void setSurveyModelArrayList(ArrayList<MerchantSurveyModel> surveyModelArrayList) {
        this.surveyModelArrayList = surveyModelArrayList;
    }

}
