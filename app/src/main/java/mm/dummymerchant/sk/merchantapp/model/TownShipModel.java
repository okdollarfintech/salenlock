package mm.dummymerchant.sk.merchantapp.model;

import android.database.Cursor;

import org.json.JSONObject;

import java.io.Serializable;

import mm.dummymerchant.sk.merchantapp.db.DBHelper;

public class TownShipModel implements Serializable {

    String name = "";
    String id = "";
    String townId = "";
    String toenName = "";
    String cityBName = "";
    String isDefaultCity = "";
    String DefaultCityBName = "";
    String DefaultCityName = "";
    String BCityName = "", BCityBName = "";

    public TownShipModel(JSONObject jsonObject) throws Exception {
        if (jsonObject.has("StateCode"))
            setId(jsonObject.getString("StateCode"));
        if (jsonObject.has("StateName"))
            setName(jsonObject.getString("StateName"));
        if (jsonObject.has("CityName"))
            setToenName(jsonObject.getString("CityName"));
        if (jsonObject.has("CityCode"))
            setTownId(jsonObject.getString("CityCode"));
        if (jsonObject.has("CityBName"))
            setCityBName(jsonObject.getString("CityBName"));
        if (jsonObject.has("isDefaultCity"))
            setIsDefaultCity(jsonObject.getString("isDefaultCity"));
        if (jsonObject.has("DefaultCityName"))
            setDefaultCityName(jsonObject.getString("DefaultCityName"));
        if (jsonObject.has("DefaultCityBName"))
            setDefaultCityBName(jsonObject.getString("DefaultCityBName"));

        if (jsonObject.has("BCityName"))
            setBCityName(jsonObject.getString("BCityName"));
        if (jsonObject.has("BCityBName"))
            setBCityBName(jsonObject.getString("BCityBName"));
    }

    public TownShipModel(Cursor c) {
        setId(c.getString(c.getColumnIndex(DBHelper.KEY_AGENT_CITY_ID)));
        setName(c.getString(c.getColumnIndex(DBHelper.KEY_AGENT_CITY_NAME)));
        setToenName(c.getString(c.getColumnIndex(DBHelper.KEY_AGENT_TOWNSHIP_NAME)));
        setTownId(c.getString(c.getColumnIndex(DBHelper.KEY_AGENT_TOWNSHIP_ID)));
        setCityBName(c.getString(c.getColumnIndex(DBHelper.KEY_AGENT_TOWNSHIP_NAME_BURMESE)));
        setIsDefaultCity(c.getString(c.getColumnIndex(DBHelper.KEY_AGENT_IS_DEFAULT_CITY)));
        setDefaultCityName(c.getString(c.getColumnIndex(DBHelper.KEY_AGENT_IS_DEFAULT_CITY_NAME)));
        setDefaultCityBName(c.getString(c.getColumnIndex(DBHelper.KEY_AGENT_IS_DEFAULT_CITY_NAME_BURMESE)));
        setBCityName(c.getString(c.getColumnIndex(DBHelper.KEY_AGENT_IS_DEFAULT_CITY_NAME_D)));
        setBCityBName(c.getString(c.getColumnIndex(DBHelper.KEY_AGENT_IS_DEFAULT_CITY_NAME_D_BURMESE)));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTownId() {
        return townId;
    }

    public void setTownId(String townId) {
        this.townId = townId;
    }

    public String getToenName() {
        return toenName;
    }

    public void setToenName(String toenName) {
        this.toenName = toenName;
    }

    public String getCityBName() {
        return cityBName;
    }

    public void setCityBName(String cityBName) {
        this.cityBName = cityBName;
    }

    public String getIsDefaultCity() {
        return isDefaultCity;
    }

    public void setIsDefaultCity(String isDefaultCity) {
        this.isDefaultCity = isDefaultCity;
    }

    public String getDefaultCityBName() {
        return DefaultCityBName;
    }

    public void setDefaultCityBName(String defaultCityBName) {
        DefaultCityBName = defaultCityBName;
    }

    public String getDefaultCityName() {
        return DefaultCityName;
    }

    public void setDefaultCityName(String defaultCityName) {
        DefaultCityName = defaultCityName;
    }

    public String getBCityName() {
        return BCityName;
    }

    public void setBCityName(String BCityName) {
        this.BCityName = BCityName;
    }

    public String getBCityBName() {
        return BCityBName;
    }

    public void setBCityBName(String BCityBName) {
        this.BCityBName = BCityBName;
    }
}
