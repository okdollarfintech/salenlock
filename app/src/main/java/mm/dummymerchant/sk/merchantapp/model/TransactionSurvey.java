package mm.dummymerchant.sk.merchantapp.model;

import android.database.Cursor;

import java.util.Date;

import mm.dummymerchant.sk.merchantapp.db.DBHelper;

/**
 * Created by user on 11/9/2015.
 */
public class TransactionSurvey
{
    int date;
    Double totalAmount;
    int week;
    int month;
    int year;
    int hours;
    Date Transdate;
    String age;
    String city;
    String minutes;
    String trnasID;
    String transDate;

    public String getTransDate() {
        return transDate;
    }

    public void setTransDate(String transDate) {
        this.transDate = transDate;
    }


    public TransactionSurvey(Cursor c) {
//        setWeek(Integer.parseInt(c.getString(c.getColumnIndex(DBHelper.SURVEY_WEEK))));
        setMonth(Integer.parseInt(c.getString(c.getColumnIndex(DBHelper.SURVEY_MONTH))));
        setYear(Integer.parseInt(c.getString(c.getColumnIndex(DBHelper.SURVEY_YEAR))));
        setHours(Integer.parseInt(c.getString(c.getColumnIndex(DBHelper.SURVEY_HOUR))));
        setDate(Integer.parseInt(c.getString(c.getColumnIndex(DBHelper.SURVEY_DAY))));
//        setTransDate(c.getString(c.getColumnIndex(DBHelper.KEY_TRANSCATION_DATE)));
//        setMinutes(c.getString(c.getColumnIndex(DBHelper.SURVEY_MINUTE)));
//        setAge(c.getString(c.getColumnIndex(DBHelper.SURVEY_AGE)));
//        setCity(c.getString(c.getColumnIndex(DBHelper.SURVEY_LOCATION)));
//        setTrnasID(c.getString(c.getColumnIndex(DBHelper.KEY_TRANSID)));
        setTotalAmount(Double.parseDouble(c.getString(c.getColumnIndex("Total"))));
    }

    public TransactionSurvey() {

    }

    public Date getTransdate() {
        return Transdate;
    }

    public void setTransdate(Date transdate) {
        Transdate = transdate;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getMinutes() {
        return minutes;
    }

    public void setMinutes(String minutes) {
        this.minutes = minutes;
    }

    public String getTrnasID() {
        return trnasID;
    }

    public void setTrnasID(String trnasID) {
        this.trnasID = trnasID;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }


    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getWeek() {
        return week;
    }

    public void setWeek(int week) {
        this.week = week;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double balance) {
        this.totalAmount = balance;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

}
