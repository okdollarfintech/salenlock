package mm.dummymerchant.sk.merchantapp.model;



import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import mm.dummymerchant.sk.merchantapp.Utils.XMLTag;

public class TransationInfoModel implements XMLTag, Serializable {

    String agentCode = "";
    String transId = "";
    String responsects = "";
    String recordcount = "";

    ArrayList<TransationModel> mTransationModel = new ArrayList<TransationModel>();
    TransationModel mModel;

    public TransationInfoModel(String response) throws XmlPullParserException, IOException {
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        XmlPullParser myparser = factory.newPullParser();
        myparser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        InputStream stream = new ByteArrayInputStream(response.getBytes());
        myparser.setInput(stream, null);
        parseXML(myparser);
        stream.close();
    }


    void parseXML(XmlPullParser myParser) throws XmlPullParserException, IOException {
        int event;
        String text = null;
        event = myParser.getEventType();
        while (event != XmlPullParser.END_DOCUMENT) {
            String name = myParser.getName();
            switch (event) {
                case XmlPullParser.START_TAG:
                    if (name.equals(TAG_RECORD))
                    {
                        mModel = new TransationModel();
                    }
                    break;
                case XmlPullParser.TEXT:
                    text = myParser.getText();
                    break;
                case XmlPullParser.END_TAG:
                    if (name.equals(TAG_RECORD))
                    {
                        setmTransationModel(mModel);
                    }
                    else if (name.equals(TAG_AMOUNT))
                    {
                        mModel.setAmount(text);
                    }
                    else if (name.equals(TAG_DESTINATION))
                    {
                        mModel.setDestination(text);
                    }
                    else if (name.equals(TAG_TRANSID))
                    {
                        if (mModel != null)
                        {
                            mModel.setTransatId(text);
                        }
                    }
                    else if (name.equals(TAG_CREDIT_OR_DEBIT))
                    {
                        mModel.setIsCredit(text);
                    }
                    else if (name.equals(TAG_FEE))
                    {
                        mModel.setFee(text);
                        text="";
                    }
                    else if (name.equals(TAG_TRANSTYPE))
                    {
                        mModel.setTrnastionType(text);
                    }
                    else if (name.equals(TAG_WALLET_BALANCE))
                    {
                        mModel.setmWalletbalance(text);
                    }
                    else if (name.equals(TAG_COMMENTS))
                    {
                        if (mModel != null) {
                            String comment= URLDecoder.decode(text, "UTF-8");
                            if(!comment.equals(""))
                            {
                                if(comment.contains("["))
                                {
                                    String comments[]=comment.split("\\[");
                                    switch (comments.length)
                                    {
                                        case 1:
                                            String onlyLat=comments[0].replace("[","");
                                            onlyLat=onlyLat.replace("]","");
                                            mModel.setLatLong(onlyLat);
                                            break;
                                        case 2:
                                            String textWithLatLong=comments[1].replace("[", "");
                                            textWithLatLong=textWithLatLong.replace("]","");
                                            mModel.setLatLong(textWithLatLong);
                                            mModel.setComments(comments[0]);
                                            break;
                                        case 3:
                                            String text_surway_WithLatLong=comments[2].replace("[", "");
                                            text_surway_WithLatLong=text_surway_WithLatLong.replace("]","");
                                            mModel.setLatLong(text_surway_WithLatLong);
                                            mModel.setComments(comments[0]);
                                            String survery=comments[1].replace("]","");
                                            mModel.setmSurvery(survery);
                                            break;
                                        default:
                                            break;

                                    }
                                    System.out.println(comment.length());
                                    //mModel.setLatLong(comments[1]);
                                }
                            }
                        }

                    }
                    else if (name.equals(TAG_RESPONSEACT))
                    {
                        if (mModel != null)
                        {
                            String j= text;
                            mModel.setDate(text);
                            mModel.setResponsects(text);
                            try {
                                SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", Locale.US);
                                mModel.setmTranscationDate(dateFormatter.parse(text));
                            }
                            catch(Exception e)
                            {
                                e.printStackTrace();
                            }
                        }
                    }else if (name.equals(TAG_RESPONSE_CODE)){
                        if(mModel!=null)
                            mModel.setResultcode(text);
                    }else if(name.equals(TAG_DESCRIPTION)) {
                        if (mModel != null)
                            mModel.setDescripton(text);
                    }else if(name.equals(TAG_SOURCE)){
                        if(mModel!=null)
                            mModel.setSource(TAG_SOURCE);
                    }else if(name.equals(TAG_RECORD_COUNT)){
                        if(mModel!=null)
                            mModel.setCount(TAG_RECORD_COUNT);
                    }else if(name.equals(TAG_VEDNDOR_CODE)){
                        if(mModel!=null)
                            mModel.setCount(TAG_RECORD_COUNT);
                    }else if(name.equals(TAG_CLIENT_TYPE)){
                        if(mModel!=null)
                            mModel.setCount(TAG_CLIENT_TYPE);
                    }else if(name.equals(TAG_OLD_BALANCE)){
                        if(mModel!=null)
                            mModel.setCount(TAG_OLD_BALANCE);
                    }

                    break;
            }
            event = myParser.next();
        }
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    public String getResponsects() {
        return responsects;
    }

    public void setResponsects(String responsects) {
        this.responsects = responsects;
    }

    public String getRecordcount() {
        return recordcount;
    }

    public void setRecordcount(String recordcount) {
        this.recordcount = recordcount;
    }

    public ArrayList<TransationModel> getmTransationModel() {
        return mTransationModel;
    }

    public void setmTransationModel(TransationModel mTransationModel) {
        this.mTransationModel.add(mTransationModel);
    }

}
