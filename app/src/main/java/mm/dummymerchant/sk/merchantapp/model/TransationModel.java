package mm.dummymerchant.sk.merchantapp.model;

import android.database.Cursor;
import android.util.Log;


import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import mm.dummymerchant.sk.merchantapp.Utils.XMLTag;
import mm.dummymerchant.sk.merchantapp.db.DBHelper;

public class TransationModel implements XMLTag, Serializable, Comparable<TransationModel>
{
    String descripton = "";
    String transatId = "";
    String newamount = "";
    String serviceTax = "";
    String open="close";
    String fee = "";
    String name = "";
    String destination = "";
    String trnastionTypetype = "";
    String date = "";

    String source = "";
    String oldAmount = "";
    String amount = "";
    String resultcode = "";
    String isCredit = "";
    String comments = "";
    String responsects = "";
    String balance = "";
    String count = "";
    String dbAmount = "";

    String loyalty_ponits = "";
    String merchantName = "";
    String destinationPersonName = "";
    String photoUrl = "";
    String cellID = "";
    String latLong = "";
    Date mTranscationDate;
    String receivedKickback = "";
    String operator = "";

    String scanner_Name = "";
    String scanner_No ="";
    String generater_No="";
    String generater_Name="";
    String scanning_Time="";
    String generate_Time="";
    String loyality="";
    String cashback="";
    String sloc="";
    String operatorname="";


   /* public TransationModel(String descripton, String transatId, String newamount, String serviceTax, String open, String fee, String name, String destination, String trnastionTypetype, String date, String source, String oldAmount, String amount, String resultcode, String isCredit, String comments, String responsects, String balance, String count, String dbAmount, String loyalty_ponits, String merchantName, String destinationPersonName, String photoUrl, String cellID, String latLong, Date mTranscationDate, String mSurvery, String mWalletbalance, String cashierId, String cashierName, String cashierNumber, String city, String age, String gender, String hours, String unique_id)
    {
        this.descripton = descripton;
        this.transatId = transatId;
        this.newamount = newamount;
        this.serviceTax = serviceTax;
        this.open = open;
        this.fee = fee;
        this.name = name;
        this.destination = destination;
        this.trnastionTypetype = trnastionTypetype;
        this.date = date;
        this.source = source;
        this.oldAmount = oldAmount;
        this.amount = amount;
        this.resultcode = resultcode;
        this.isCredit = isCredit;
        this.comments = comments;
        this.responsects = responsects;
        this.balance = balance;
        this.count = count;
        this.dbAmount = dbAmount;
        this.loyalty_ponits = loyalty_ponits;
        this.merchantName = merchantName;
        this.destinationPersonName = destinationPersonName;
        this.photoUrl = photoUrl;
        this.cellID = cellID;
        this.latLong = latLong;
        this.mTranscationDate = mTranscationDate;
        this.mSurvery = mSurvery;
        this.mWalletbalance = mWalletbalance;
        this.cashierId = cashierId;
        this.cashierName = cashierName;
        this.cashierNumber = cashierNumber;
        this.city = city;
        this.age = age;
        this.gender = gender;
        this.hours = hours;
        this.unique_id = unique_id;
    }
*/
    String mSurvery="";
    String mWalletbalance="";
    String cashierId="";

    String cashierName="";
    String cashierNumber="";
    String city="Unknown";
    String age="0";
    String gender="X";
    String hours = "";
    String unique_id = "";


    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }




    public String getCashierId() {
        return cashierId;
    }

    public void setCashierId(String cashierId) {
        this.cashierId = cashierId;
    }


    public String getHours() {
        return hours;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }



    public String getUnique_id() {
        return unique_id;
    }

    public void setUnique_id(String unique_id) {
        this.unique_id = unique_id;
    }


    public String getReceivedKickback() {
        return receivedKickback;
    }

    public void setReceivedKickback(String receivedKickback) {
        this.receivedKickback = receivedKickback;
    }

    public TransationModel() {
    }

    public TransationModel(String response) throws XmlPullParserException, IOException {
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        XmlPullParser myparser = factory.newPullParser();
        myparser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        InputStream stream = new ByteArrayInputStream(response.getBytes());
        myparser.setInput(stream, null);
        parseXML(myparser);
        stream.close();
    }


    public String getOperator() {
        return operator;
    }

  /*  public TransationModel(Cursor cc, String data) {
        try {

            setDestination(cc.getString(cc.getColumnIndex(DBHelper.COLUMN_DESTINATION)));
            setTransatId(cc.getString(cc.getColumnIndex(DBHelper.KEY_TRANSID)));
            setAmount(cc.getString(cc.getColumnIndex(DBHelper.COLUMN_AMOUNT)));
            setName(cc.getString(cc.getColumnIndex(DBHelper.KEY_NAME)));
            setDate(cc.getString(cc.getColumnIndex(DBHelper.COLUMN_DATE)));
            setComments(cc.getString(cc.getColumnIndex(DBHelper.COMMENTS)));
            setUnique_id(cc.getString(cc.getColumnIndex(DBHelper.UNIQUE_ID)));
            setBalance(cc.getString(cc.getColumnIndex(DBHelper.KEY_NEW_AMOUNT)));
            setmSurvery(cc.getString(cc.getColumnIndex(DBHelper.KEY_TRANSCATION_SURVERY)));
            setmWalletbalance(cc.getString(cc.getColumnIndex(DBHelper.KEY_TRANSCATION_WALLET_BALANCE)));
            SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", Locale.ENGLISH);
            Date d=new Date(cc.getLong(cc.getColumnIndex(DBHelper.KEY_TRANSCATION_DATE)));
            setmTranscationDate(d);

        } catch (Exception e) {

            e.printStackTrace();
        }
    }*/


    public TransationModel(Cursor cc, String data) {
        try {
            setDestination(cc.getString(cc.getColumnIndex(DBHelper.COLUMN_DESTINATION)));
            try {
                setReceivedKickback(cc.getString(cc.getColumnIndex(DBHelper.KEY_TRANSCATION_RECEIVED_KIKBACK)));
            }catch (Exception e) {
            }
            setTransatId(cc.getString(cc.getColumnIndex(DBHelper.KEY_TRANSID)));
            setAmount(cc.getString(cc.getColumnIndex(DBHelper.COLUMN_AMOUNT)));
            setName(cc.getString(cc.getColumnIndex(DBHelper.KEY_NAME)));
            setDate(cc.getString(cc.getColumnIndex(DBHelper.COLUMN_DATE)));
            setComments(cc.getString(cc.getColumnIndex(DBHelper.COMMENTS)));
            setUnique_id(cc.getString(cc.getColumnIndex(DBHelper.UNIQUE_ID)));
            setBalance(cc.getString(cc.getColumnIndex(DBHelper.KEY_NEW_AMOUNT)));
            setmSurvery(cc.getString(cc.getColumnIndex(DBHelper.KEY_TRANSCATION_SURVERY)));
            try {
                setmWalletbalance(cc.getString(cc.getColumnIndex(DBHelper.KEY_TRANSCATION_WALLET_BALANCE)));
            }catch (Exception e)
            {}
            SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", Locale.ENGLISH);
            Date d = new Date(cc.getLong(cc.getColumnIndex(DBHelper.KEY_TRANSCATION_DATE)));
            // setmWalletbalance(cc.getString(cc.getColumnIndex(DBHelper.KEY_TRANSCATION_WALLET_BALANCE)));
            setmTranscationDate(d);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*public TransationModel(Cursor c) {

        try {
            setCashierId(c.getString(c.getColumnIndex(DBHelper.KEY_CASHIER_ID)));
            setCashierName(c.getString(c.getColumnIndex(DBHelper.KEY_CASHIER_NAME)));
            setCashierNumber(c.getString(c.getColumnIndex(DBHelper.KEY_CASHIER_NUMBER)));
            setDescripton(c.getString(c.getColumnIndex(DBHelper.KEY_DESCRIPTION)));
            setDestination(c.getString(c.getColumnIndex(DBHelper.KEY_DESTINATION_NUM)));
            setTransatId(c.getString(c.getColumnIndex(DBHelper.KEY_TRANSID)));
            setAmount(c.getString(c.getColumnIndex(DBHelper.KEY_AMOUNT)));
            setNewamount(c.getString(c.getColumnIndex(DBHelper.KEY_NEW_AMOUNT)));
            setOldAmount(c.getString(c.getColumnIndex(DBHelper.KEY_OLD_AMOUNT)));
            setServiceTax(c.getString(c.getColumnIndex(DBHelper.KEY_SERVICE_TAX)));
            setTrnastionType(c.getString(c.getColumnIndex(DBHelper.KEY_TRANACATION_TYPE)));
            setName(c.getString(c.getColumnIndex(DBHelper.KEY_AGENT_NAME)));
            setmSurvery(c.getString(c.getColumnIndex(DBHelper.KEY_TRANSCATION_SURVERY)));
            setmWalletbalance(c.getString(c.getColumnIndex(DBHelper.KEY_TRANSCATION_WALLET_BALANCE)));

            try {
                setSource(c.getString(c.getColumnIndex(DBHelper.KEY_AGENT_NUM)));
            } catch (Exception e) {
                setSource("0");
            }
            //   String date = c.getString(c.getColumnIndex(DBHelper.KEY_DATE));
            setDate(c.getString(c.getColumnIndex(DBHelper.KEY_DATE)));
            setFee(c.getString(c.getColumnIndex(DBHelper.KEY_FEE)));
            setIsCredit(c.getString(c.getColumnIndex(DBHelper.KEY_TRANSTYPE)));
            try {
                setCount(c.getString(c.getColumnIndex(DBHelper.KEY_KEY_COUNT)));
            } catch (Exception e) {
                setCount("0");
            }
            //setCount(c.getString(c.getColumnIndex(DBHelper.KEY_KEY_COUNT)));
            setComments(c.getString(c.getColumnIndex(DBHelper.KEY_COMMEENT)));
            setDbAmount(c.getString(c.getColumnIndex(DBHelper.KEY_AMOUNT)));
            setDestinationPersonName(c.getString(c.getColumnIndex(DBHelper.KEY_NAME)));
            setPhotoUrl(c.getString(c.getColumnIndex(DBHelper.KEY_IMAGE_PATH)));
            setLatLong(c.getString(c.getColumnIndex(DBHelper.KEY_LAT_LONG)));
            setCellID(c.getString(c.getColumnIndex(DBHelper.CELLTOWERID)));
            setHours(c.getString(c.getColumnIndex(DBHelper.KEY_HOURS)));
            // setHours(c.getString(c.getColumnIndex(DBHelper.KEY_HOURS)));
            // SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss", Locale.ENGLISH);
//            Date d= dateFormatter.parse(c.getString(c.getColumnIndex(DBHelper.KEY_TRANSCATION_DATE)));
//            setmTranscationDate(d);
            setmTranscationDate(new Date(c.getLong(c.getColumnIndex(DBHelper.KEY_TRANSCATION_DATE))));
        } catch (Exception e) {

            e.printStackTrace();
        }
    }*/

    public TransationModel(Cursor c) {

        try {
            setCashierId(c.getString(c.getColumnIndex(DBHelper.KEY_CASHIER_ID)));
            setCashierName(c.getString(c.getColumnIndex(DBHelper.KEY_CASHIER_NAME)));
            setCashierNumber(c.getString(c.getColumnIndex(DBHelper.KEY_CASHIER_NUMBER)));
            setDescripton(c.getString(c.getColumnIndex(DBHelper.KEY_DESCRIPTION)));
            setReceivedKickback(c.getString(c.getColumnIndex(DBHelper.KEY_TRANSCATION_RECEIVED_KIKBACK)));
            setDestination(c.getString(c.getColumnIndex(DBHelper.KEY_DESTINATION_NUM)));
            setTransatId(c.getString(c.getColumnIndex(DBHelper.KEY_TRANSID)));
            setAmount(c.getString(c.getColumnIndex(DBHelper.KEY_AMOUNT)));
            setNewamount(c.getString(c.getColumnIndex(DBHelper.KEY_NEW_AMOUNT)));
            setOldAmount(c.getString(c.getColumnIndex(DBHelper.KEY_OLD_AMOUNT)));
            setServiceTax(c.getString(c.getColumnIndex(DBHelper.KEY_SERVICE_TAX)));
            setTrnastionType(c.getString(c.getColumnIndex(DBHelper.KEY_TRANACATION_TYPE)));
            setName(c.getString(c.getColumnIndex(DBHelper.KEY_AGENT_NAME)));
            setmSurvery(c.getString(c.getColumnIndex(DBHelper.KEY_TRANSCATION_SURVERY)));
            setmWalletbalance(c.getString(c.getColumnIndex(DBHelper.KEY_TRANSCATION_WALLET_BALANCE)));

            try {
                setSource(c.getString(c.getColumnIndex(DBHelper.KEY_AGENT_NUM)));
            } catch (Exception e) {
                setSource("0");
            }
            //   String date = c.getString(c.getColumnIndex(DBHelper.KEY_DATE));
            setDate(c.getString(c.getColumnIndex(DBHelper.KEY_DATE)));
            setFee(c.getString(c.getColumnIndex(DBHelper.KEY_FEE)));
            setIsCredit(c.getString(c.getColumnIndex(DBHelper.KEY_TRANSTYPE)));
            try {
                setCount(c.getString(c.getColumnIndex(DBHelper.KEY_KEY_COUNT)));
            } catch (Exception e) {
                setCount("0");
            }
            //setCount(c.getString(c.getColumnIndex(DBHelper.KEY_KEY_COUNT)));
            setComments(c.getString(c.getColumnIndex(DBHelper.KEY_COMMEENT)));
            setDbAmount(c.getString(c.getColumnIndex(DBHelper.KEY_AMOUNT)));
            setDestinationPersonName(c.getString(c.getColumnIndex(DBHelper.KEY_NAME)));
            setPhotoUrl(c.getString(c.getColumnIndex(DBHelper.KEY_IMAGE_PATH)));
            setLatLong(c.getString(c.getColumnIndex(DBHelper.KEY_LAT_LONG)));
            setCellID(c.getString(c.getColumnIndex(DBHelper.CELLTOWERID)));
            setHours(c.getString(c.getColumnIndex(DBHelper.KEY_HOURS)));
            // setHours(c.getString(c.getColumnIndex(DBHelper.KEY_HOURS)));
            // SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss", Locale.ENGLISH);
//            Date d= dateFormatter.parse(c.getString(c.getColumnIndex(DBHelper.KEY_TRANSCATION_DATE)));
//            setmTranscationDate(d);
            setmTranscationDate(new Date(c.getLong(c.getColumnIndex(DBHelper.KEY_TRANSCATION_DATE))));
            //  if(c.getColumnIndex(DBHelper.KEY_TRANSACTION_OPERATOR)==0)
            try {
                setOperator(c.getString(c.getColumnIndex(DBHelper.KEY_TRANSACTION_OPERATOR)));
                System.out.println("model.getOperator(00)--x" + c.getString(c.getColumnIndex(DBHelper.KEY_TRANSACTION_OPERATOR)));
            }
            catch(Exception e)
            {
                Log.d("Operter tag", e.toString());
            }


        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    public String getCellID() {
        return cellID;
    }

    public void setCellID(String cellID) {
        this.cellID = cellID;
    }

    public String getLatLong() {
        return latLong;
    }

    public void setLatLong(String latLong) {
        String data[]=latLong.split(",");
        switch (data.length){
            case 1:
                //future use
                break;
            case 2:
                this.latLong=data[0]+","+data[1];
                break;
            case 3:
                //future use
                break;
            case 4:
                //future use
                break;
            case 5:
                //future use
                break;
            case 9:
                this.latLong=data[0]+","+data[1];
                this.cellID=data[2];
                this.gender=data[3];
                this.age=data[4];
                this.city=data[5];
                break;
            default:
                this.latLong=latLong;
        }
    }

    void parseXML(XmlPullParser myParser) throws XmlPullParserException, IOException {
        int event;
        String text = null;
        event = myParser.getEventType();
        while (event != XmlPullParser.END_DOCUMENT) {
            String name = myParser.getName();
            switch (event) {
                case XmlPullParser.START_TAG:
                    break;
                case XmlPullParser.TEXT:
                    text = myParser.getText();
                    break;
                case XmlPullParser.END_TAG:
                    if (name.equals(TAG_AGENTCODE))
                        setSource(text);
                    else if (name.equals(TAG_NEW_FEE))
                        setFee(text);
                    else if (name.equals(TAG_TRANSID))
                        setTransatId(text);
                    else if (name.equals(TAG_DESCRIPTION))
                        setDescripton(text);
                    else if (name.equals(TAG_DATE)) {
                        String text1 = text;
                        setDate(text);
                        try {
                            SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", Locale.ENGLISH);
                            setmTranscationDate(dateFormatter.parse(text));
                        }
                        catch(Exception e)
                        {
                            e.printStackTrace();
                        }
                    } else if (name.equals(TAG_SERVICE_TAX))
                        setServiceTax(text);
                    else if (name.equals(TAG_AGENT_NAME))
                        setName(text);
                    else if (name.equals(TAG_OLD_BALANCE))
                        setOldAmount(text);
                    else if (name.equals(TAG_NEW_BALANCE)) {
                        setNewamount(text);
                        setmWalletbalance(text);
                    }
                    else if (name.equals(TAG_AMOUNT))
                        setAmount(text);
                    else if (name.equals("acctranstype"))
                        setIsCredit(text);
                    else if (name.equals(TAG_DESTINATION))
                        setDestination(text);
                    else if (name.equals(TAG_TRANSTYPE))
                        setTrnastionType(text);
                    else if (name.equals(TAG_RESPONSE_CODE))
                        setResultcode(text);
                    else if (name.equals("walletbalance")) {
                        setBalance(text);
                        //etmWalletbalance(text);
                    }
                    else if (name.equals("comments"))
                    {

                        String comment= URLDecoder.decode(text, "UTF-8");
                        if(!comment.equals(""))
                        {
                            if(comment.contains("["))
                            {
                                String comments[]=comment.split("\\[");
                                switch (comments.length)
                                {
                                    case 1:
                                        String onlyLat=comments[0].replace("[","");
                                        onlyLat=onlyLat.replace("]","");
                                        setLatLong(onlyLat);
                                        break;
                                    case 2:
                                        String textWithLatLong=comments[1].replace("[", "");
                                        textWithLatLong=textWithLatLong.replace("]","");
                                        setLatLong(textWithLatLong);
                                        setComments(comments[0]);
                                        break;
                                    case 3:
                                        String text_surway_WithLatLong=comments[2].replace("[", "");
                                        text_surway_WithLatLong=text_surway_WithLatLong.replace("]","");
                                        String survery=comments[1].replace("]","");
                                        setmSurvery(survery);
                                        setLatLong(text_surway_WithLatLong);
                                        setComments(comments[0]);
                                        break;
                                    default:
                                        break;

                                }
                                System.out.println(comment.length());
                                //mModel.setLatLong(comments[1]);
                            }
                            else
                            {
                                setComments(text);
                            }
                        }
                    }


                    else if (name.equals("loyaltypoints"))
                        setLoyalty_ponits(text);
                    else if (name.equals("merchantname"))
                        setMerchantName(text);
                    break;
            }
            event = myParser.next();
        }
    }


    public String getCashierName() {
        return cashierName;
    }

    public void setCashierName(String cashierName) {
        this.cashierName = cashierName;
    }

    public String getCashierNumber() {
        return cashierNumber;
    }

    public void setCashierNumber(String cashierNumber) {
        this.cashierNumber = cashierNumber;
    }
    public String getDestinationPersonName() {
        return destinationPersonName;
    }

    public void setDestinationPersonName(String destinationPersonName) {
        this.destinationPersonName = destinationPersonName;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getLoyalty_ponits() {
        return loyalty_ponits;
    }

    public void setLoyalty_ponits(String loyalty_ponits) {
        this.loyalty_ponits = loyalty_ponits;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getDbAmount() {
        return dbAmount;
    }

    public void setDbAmount(String dbAmount) {
        this.dbAmount = dbAmount;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {

        this.balance = balance;
    }

    public String getDescripton() {
        return descripton;
    }

    public void setDescripton(String descripton) {
        this.descripton = descripton;
    }

    public String getTransatId() {
        return transatId;
    }

    public void setTransatId(String transatId) {
        this.transatId = transatId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getServiceTax() {
        return serviceTax;
    }
    public String getOpen() {
        return open;
    }

    public void setOpen(String open) {
        this.open = open;
    }


    public void setServiceTax(String serviceTax) {
        this.serviceTax = serviceTax;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getTrnastionType() {
        return trnastionTypetype;
    }

    public void setTrnastionType(String trnastionTypetype) {
        this.trnastionTypetype = trnastionTypetype;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getOldAmount() {
        return oldAmount;
    }

    public void setOldAmount(String oldAmount) {
        this.oldAmount = oldAmount;
    }

    public String getNewamount() {
        return newamount;
    }

    public void setNewamount(String newamount) {
        this.newamount = newamount;
    }

    public String getResultcode() {
        return resultcode;
    }

    public void setResultcode(String resultcode) {
        this.resultcode = resultcode;
    }

    public String getIsCredit() {
        return isCredit;
    }

    public void setIsCredit(String isCredit) {
        this.isCredit = isCredit;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getResponsects() {
        return responsects;
    }

    public void setResponsects(String responsects) {
        this.responsects = responsects;
    }

    public Date getmTranscationDate() {
        return mTranscationDate;
    }

    public void setmTranscationDate(Date mTranscationDate) {
        this.mTranscationDate = mTranscationDate;
    }

    @Override
    public int compareTo(TransationModel o) {

        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", Locale.US);
        Date date1 = null;
        Date date2 = null;


        try {


            if (getDate() != null) {

                if (!getDate().equals("")) {

                    date1 = dateFormatter.parse(getDate());
                    date2 = dateFormatter.parse(o.getDate());


                } else if (getResponsects() != null) {

                    if (!getResponsects().equals("")) {


                        date1 = dateFormatter.parse(getResponsects());
                        date2 = dateFormatter.parse(o.getDate());


                    } else {

                        return 0;
                    }


                } else {

                    return 0;
                }

            } else if (getResponsects() != null) {
                if (!getResponsects().equals("")) {

                    date1 = dateFormatter.parse(getResponsects());
                    date2 = dateFormatter.parse(o.getDate());
                } else {

                    return 0;
                }


            } else {

                return 0;
            }
        } catch (Exception e) {
        }

        if (date1 != null && date2 != null) {
            return date2.compareTo(date1);
        } else {
            return 0;
        }
    }


    public String getmSurvery() {
        return mSurvery;
    }

    public void setmSurvery(String mSurvery) {
        this.mSurvery = mSurvery;
    }

    public String getmWalletbalance() {
        return mWalletbalance;
    }

    public void setmWalletbalance(String mWalletbalance) {
        this.mWalletbalance = mWalletbalance;
    }
    public void setOperator(String operator) {
        this.operator = operator;
    }
    public String getOperatorName() {
        return operatorname;
    }



    public String getScanner_Name() {
        return scanner_Name;
    }

    public void setScanner_Name(String scanner_Name) {
        this.scanner_Name = scanner_Name;
    }

    public String getScanner_No() {
        return scanner_No;
    }

    public void setScanner_No(String scanner_No) {
        this.scanner_No = scanner_No;
    }

    public String getGenerater_No() {
        return generater_No;
    }

    public void setGenerater_No(String generater_No) {
        this.generater_No = generater_No;
    }

    public String getGenerater_Name() {
        return generater_Name;
    }

    public void setGenerater_Name(String generater_Name) {
        this.generater_Name = generater_Name;
    }

    public String getScanning_Time() {
        return scanning_Time;
    }

    public void setScanning_Time(String scanning_Time) {
        this.scanning_Time = scanning_Time;
    }

    public String getGenerate_Time() {
        return generate_Time;
    }

    public void setGenerate_Time(String generate_Time) {
        this.generate_Time = generate_Time;
    }

    public String getLoyality() {
        return loyality;
    }

    public void setLoyality(String loyality) {
        this.loyality = loyality;
    }

    public String getCashback() {
        return cashback;
    }

    public void setCashback(String cashback) {
        this.cashback = cashback;
    }
    public void setCashBack(String cashback)
    {
        this.cashback=cashback;
    }
    public String getCashBack()
    {
        return cashback;
    }



    public void setScanloc(String sloc)
    {
        this.sloc=sloc;
    }
    public String getScanloc()
    {
        return sloc;
    }

}

