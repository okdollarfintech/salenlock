package mm.dummymerchant.sk.merchantapp.model;

/**
 * Created by Dell on 7/25/2016.
 */
public class UserInctivityModel {
    String cashierId;
    String cashierNo;
    String cashierName;
    String cellId;
    double lat;
    double longi;
    String mcc;
    String mnc;
    String telephoneOperator;
    String duration;

    public UserInctivityModel(String cashierId, String cashierNo, String cashierName, String cellId, double lat, double longi, String mcc, String mnc, String telephoneOperator, String duration) {
        this.cashierId = cashierId;
        this.cashierNo = cashierNo;
        this.cashierName = cashierName;
        this.cellId = cellId;
        this.lat = lat;
        this.longi = longi;
        this.mcc = mcc;
        this.mnc = mnc;
        this.telephoneOperator = telephoneOperator;
        this.duration = duration;
    }

    public double getLongi() {
        return longi;
    }

    public String getCashierId() {
        return cashierId;
    }

    public String getCashierNo() {
        return cashierNo;
    }

    public String getCashierName() {
        return cashierName;
    }

    public String getCellId() {
        return cellId;
    }

    public double getLat() {
        return lat;
    }

    public String getMcc() {
        return mcc;
    }

    public String getMnc() {
        return mnc;
    }

    public String getTelephoneOperator() {
        return telephoneOperator;
    }

    public String getDuration() {
        return duration;
    }


}
