package mm.dummymerchant.sk.merchantapp.model;

/**
 * Created by Dell on 6/16/2016.
 */
public class dashboard_model
{
    String name;
    String id;
    int image;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }


    public dashboard_model(String name, String id, int image) {
        this.name = name;
        this.id = id;
        this.image = image;
    }


}
