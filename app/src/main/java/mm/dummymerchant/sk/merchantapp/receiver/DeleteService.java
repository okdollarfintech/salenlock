package mm.dummymerchant.sk.merchantapp.receiver;

import android.app.IntentService;
import android.content.Intent;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Calendar;

/**
 * Created by user on 11/2/2015.
 */
public class DeleteService extends IntentService
{
    public DeleteService(String name)
    {
        super(name);
    }
    public DeleteService()
    {
        super("DeleteService");

    }
    @Override
    protected void onHandleIntent(Intent intent)
    {

        final File dir[] = new File[2];

        dir[0] = new File(android.os.Environment.getExternalStorageDirectory(), "/SpeedKyat/.SpeedKyatRail");
        //dir[1] = new File(android.os.Environment.getExternalStorageDirectory(), "SpeedKyat");
        dir[1] = new File(android.os.Environment.getExternalStorageDirectory(), "/SpeedKyat/.SpeedKyatBill");


        for (int i=0 ; i < dir.length;i++) {
            try {

                File d[] = dir[i].listFiles(new FilenameFilter() {

                    @Override
                    public boolean accept(File dire, String filename) {
                        File f= new File(dire.getPath()+File.separator+filename);
                        // Log.d("Files",f.getName());
                        if(!filename.contains("reclaimed"))
                            if(f!=null)
                            {
                                long time=f.lastModified();
                                long difference=System.currentTimeMillis()-time;
                                //System.out.println(difference);
                            }

                        Calendar calendar = Calendar.getInstance();

                        long difference = calendar.getTimeInMillis() - f.lastModified();
                        long limit = 24*60*60*1000l;

                        if (difference >limit) {

                            try {

                                boolean result = f.delete();

                            } catch (Exception e) {
                                e.printStackTrace();

                            }
                        }




                        return false;
                    }
                });

            }catch(Exception e){
                e.printStackTrace();
            }


        }

        stopSelf();

    }
}
