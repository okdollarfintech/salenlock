package mm.dummymerchant.sk.merchantapp.receiver;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;
import android.widget.Toast;

import mm.dummymerchant.sk.merchantapp.Gcm.GCMNotificationIntentService;


public class GcmBroadcastReceiver extends WakefulBroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {


        try {
            ComponentName comp = new ComponentName(context.getPackageName(),
                    GCMNotificationIntentService.class.getName());
            Log.e("gcmcalled","...gcm called");
            startWakefulService(context, (intent.setComponent(comp)));
            setResultCode(Activity.RESULT_OK);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}