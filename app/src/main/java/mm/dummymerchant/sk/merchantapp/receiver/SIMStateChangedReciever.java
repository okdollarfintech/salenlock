package mm.dummymerchant.sk.merchantapp.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.util.Log;

import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.Utils;
import mm.dummymerchant.sk.merchantapp.db.DBHelper;


public class SIMStateChangedReciever extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        DBHelper db= new DBHelper(context);
        TelephonyManager telMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        int state = telMgr.getSimState();
        switch (state) {
            case TelephonyManager.SIM_STATE_ABSENT:
                /*Intent i = new Intent(context, SplashActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                i.putExtra(Constant.SIM_STATE_CHANGED, 0);
                context.startActivity(i);*/
                AppPreference.setAuthToken(context, "");
                break;
            case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
                break;
            case TelephonyManager.SIM_STATE_PIN_REQUIRED:
                break;
            case TelephonyManager.SIM_STATE_PUK_REQUIRED:
                break;
            case TelephonyManager.SIM_STATE_READY:
                if(!db.getSimId().equals(Utils.simSerialNumber(context))){
                    Log.v("SimStateListener", "Sim card is changed");
                    String language= AppPreference.getLanguage(context);
                   /* Intent i1 = new Intent(context, SplashActivity.class);
                    i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i1.putExtra(Constant.SIM_STATE_CHANGED, 2);
                    context.startActivity(i1);*/
                    db.deleteAllTableData();
                    AppPreference.deleteAppPrefrence(context);
                    AppPreference.setAuthToken(context, "");
                    AppPreference.setLanguage(context, language);
                }
                break;
            case TelephonyManager.SIM_STATE_UNKNOWN:
                break;
        }

    }
}
