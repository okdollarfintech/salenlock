package mm.dummymerchant.sk.merchantapp.receiver;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import mm.dummymerchant.sk.merchantapp.NotificationActivity;
import mm.dummymerchant.sk.merchantapp.SliderScreen;
import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.Constant;
import mm.dummymerchant.sk.merchantapp.Utils.SmsVerification;
import mm.dummymerchant.sk.merchantapp.Utils.Utils;
import mm.dummymerchant.sk.merchantapp.db.DBHelper;
import mm.dummymerchant.sk.merchantapp.model.CashierModel;
import mm.dummymerchant.sk.merchantapp.model.TransationModel;

/**
 * Created by user on 11/2/2015.
 */
public class SMSReceiverMobile extends BroadcastReceiver implements Constant {
    public static final String SMS_EXTRA_NAME = "pdus";
    public String strMessage;
    private String addressNumber = "";

    private ValidateSMSPayTo validatePayto;

    public void onReceive(Context context, Intent intent) {


        Log.e("smsreceived","...sms received");
        Bundle extras = intent.getExtras();
        Object[] pdus = (Object[]) extras.get("pdus");
        SmsMessage[] messages = new SmsMessage[pdus.length];

        for (int i = 0; i < messages.length; i++) {
            messages[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
            strMessage += messages[i].getMessageBody().toString();
            strMessage += "";
        }

        Object[] pdus1 = (Object[]) intent.getExtras().get("pdus");
        SmsMessage shortMessage = SmsMessage.createFromPdu((byte[]) pdus1[0]);
        StringBuilder originalSMS = new StringBuilder();
        for (int i = 0; i < pdus1.length; i++) {
            shortMessage = SmsMessage.createFromPdu((byte[]) pdus1[i]);
            originalSMS.append(shortMessage.getDisplayMessageBody());
        }

        System.out.print(originalSMS.toString());

        sendBroadCastSMS1(context, strMessage);

        if (extras != null) {
            Object[] smsExtra = (Object[]) extras.get(SMS_EXTRA_NAME);

            for (int i = 0; i < smsExtra.length; ++i) {
                SmsMessage sms = SmsMessage.createFromPdu((byte[]) smsExtra[i]);
                String body = sms.getMessageBody().toString();
                String address = sms.getOriginatingAddress();
                addressNumber = address;
                String mobileNo = AppPreference.getVerifyNumberWithCountryCode(context);
                if (mobileNo.equals("")) {
                    break;
                }
                String countryCode = AppPreference.getCountryCode(context);
                String mobileNoWithoutCountryCode = mobileNo.substring(countryCode.length());

                if (body.equals(AppPreference.getOTP(context))) {
                    if (address.equals(mobileNo) || address.equals(mobileNoWithoutCountryCode) || address.equals("0" + mobileNoWithoutCountryCode)) {
                        sendBroadCastSMS(body, context);
                    } else {
                        sendBroadCastSMSFalase(body, context);
                    }
                } else if (address.startsWith("+") || address.startsWith("00") || address.equals("OkDollar")) {
                    if (address.equals(mobileNo)) {
                        if (body.equals(AppPreference.getOTP(context))) {
                            sendBroadCastSMS(body, context);
                        }
                    }
                } else {
                    if (!mobileNo.equals("")) {
                        if (address.startsWith("0")) {
                            address = address.substring(1);
                            if (address.equals(mobileNoWithoutCountryCode)) {
                                if (body.equals(AppPreference.getOTP(context))) {
                                    sendBroadCastSMS(body, context);
                                }
                            }
                        } else {
                            if (address.equals(mobileNoWithoutCountryCode)) {
                                if (body.equals(AppPreference.getOTP(context))) {
                                    sendBroadCastSMS(body, context);
                                }
                            }
                        }
                    }
                }
            }
            validatePayto = new ValidateSMSPayTo(context);
            validatePayto.checkPayToMessageReceived(originalSMS.toString().toLowerCase().trim(), addressNumber);
            if (!Utils.isConnectedToInternet(context)) {
              //  Log.e("pass","..."+AppPreference.getPasword(context));
                String installMobileNumber = Utils.getUniversalFormattedNoWithPlus(AppPreference.getMyMobileNo(context));


                if (!AppPreference.getPasword(context).equals("")) {

                    if (SmsVerification.with(context).isAddressPresent(addressNumber)) {

                        if (originalSMS.toString().contains(installMobileNumber)) {
                            if (!originalSMS.toString().contains("(paid to)") && originalSMS.toString().contains("(Received)") && originalSMS.toString().contains("park/toll/"))// TOLL to SMS parsing
                            {
                                String lPaytoSMSArray[] = originalSMS.toString().split(" ");
                                TransationModel model = new TransationModel();



                                    //for setting the Master and Cashier data appropriately
                                    if (AppPreference.getMasterOrCashier(context).equalsIgnoreCase(MASTER) || SliderScreen.present_Cashier_Id == null) {
                                        model.setCashierId(MASTER_TAG);
                                        model.setCashierName(MASTER_TAG);
                                        model.setCashierNumber(MASTER_TAG);
                                    } else {
                                        DBHelper dbb = new DBHelper(context);
                                        CashierModel cm = dbb.getCashierModel(SliderScreen.present_Cashier_Id);
                                        dbb.close();

                                        model.setCashierId(SliderScreen.present_Cashier_Id);
                                        model.setCashierName(cm.getCashier_Name());
                                        model.setCashierNumber(cm.getCashier_Number());

                                    }
                                    //------------------------------------

                                    model.setTrnastionType("TOLL");
                                    model.setDestination("XXXXXXXXXX");
                                    model.setCount("0");
                                    model.setIsCredit("Cr");
                                    comment_getter(originalSMS.toString(), model);
                                    boolean check = parsingCommonDate(lPaytoSMSArray, model, context);
                                    if (check) {

                                    } else {
                                        if (!isForeground(context)) {
                                            Intent call = new Intent(context, NotificationActivity.class);
                                            call.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            context.startActivity(call);
                                        }

                                }
                                return;
                            }
                            if (!originalSMS.toString().contains("(paid to)") && originalSMS.toString().contains("(received)") && originalSMS.toString().contains("TicketNo:"))// Ticket to SMS parsing
                            {
                                String lPaytoSMSArray[] = originalSMS.toString().split(" ");
                                TransationModel model = new TransationModel();

                                //for setting the Master and Cashier data appropriately

                                    if (AppPreference.getMasterOrCashier(context).equalsIgnoreCase(MASTER) || SliderScreen.present_Cashier_Id == null) {
                                        model.setCashierId(MASTER_TAG);
                                        model.setCashierName(MASTER_TAG);
                                        model.setCashierNumber(MASTER_TAG);
                                    } else {
                                        DBHelper dbb = new DBHelper(context);
                                        CashierModel cm = dbb.getCashierModel(SliderScreen.present_Cashier_Id);
                                        dbb.close();

                                        model.setCashierId(SliderScreen.present_Cashier_Id);
                                        model.setCashierName(cm.getCashier_Name());
                                        model.setCashierNumber(cm.getCashier_Number());

                                    }
                                    //------------------------------------

                                    model.setTrnastionType("TICKET");
                                    model.setDestination("XXXXXXXXXX");
                                    model.setCount("0");
                                    model.setIsCredit("Cr");
                                    comment_getter(originalSMS.toString(), model);
                                    boolean check = parsingCommonDate(lPaytoSMSArray, model, context);

                                    if (check) {

                                    } else {
                                        if (!isForeground(context)) {
                                            Intent call = new Intent(context, NotificationActivity.class);
                                            call.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            context.startActivity(call);
                                        }
                                                                    }
                                return;
                            }
                            if (!originalSMS.toString().contains("(paid to)") && (originalSMS.toString().contains("(Received)") || originalSMS.toString().contains("received")))// Pay to without Id to SMS parsing
                            {
                                String lPaytoSMSArray[] = originalSMS.toString().split(" ");
                                TransationModel model = new TransationModel();


                                    //for setting the Master and Cashier data appropriately
                                    if (AppPreference.getMasterOrCashier(context).equalsIgnoreCase(MASTER) || SliderScreen.present_Cashier_Id == null) {
                                        model.setCashierId(MASTER_TAG);
                                        model.setCashierName(MASTER_TAG);
                                        model.setCashierNumber(MASTER_TAG);
                                    }
                                    else
                                    {
                                        DBHelper dbb = new DBHelper(context);
                                        CashierModel cm = dbb.getCashierModel(SliderScreen.present_Cashier_Id);
                                        dbb.close();

                                        model.setCashierId(SliderScreen.present_Cashier_Id);
                                        model.setCashierName(cm.getCashier_Name());
                                        model.setCashierNumber(cm.getCashier_Number());
                                    }
                                    //------------------------------------

                                    model.setTrnastionType("PAYWITHOUT ID");
                                    model.setDestination("XXXXXXXXXX");
                                    model.setCount("0");
                                    model.setIsCredit("Cr");
                                    comment_getter(originalSMS.toString(), model);
                                    boolean check = parsingCommonDate(lPaytoSMSArray, model, context);

                                    if (check) {

                                    } else {
                                        if (!isForeground(context)) {
                                            Intent call = new Intent(context, NotificationActivity.class);
                                            call.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            context.startActivity(call);
                                        }
                                }
                                return;
                            }

                            if (!originalSMS.toString().contains("(paid to)") && (originalSMS.toString().contains("Received From") || originalSMS.toString().contains("received from")))// Pay to SMS parsing
                            {
                                String lPaytoSMSArray[] = originalSMS.toString().split(" ");
                                TransationModel model = new TransationModel();


                                    //for setting the Master and Cashier data appropriately
                                    if (AppPreference.getMasterOrCashier(context).equalsIgnoreCase(MASTER) || SliderScreen.present_Cashier_Id == null) {
                                        model.setCashierId(MASTER_TAG);
                                        model.setCashierName(MASTER_TAG);
                                        model.setCashierNumber(MASTER_TAG);
                                    } else {
                                        DBHelper dbb = new DBHelper(context);
                                        CashierModel cm = dbb.getCashierModel(SliderScreen.present_Cashier_Id);
                                        dbb.close();

                                        model.setCashierId(SliderScreen.present_Cashier_Id);
                                        model.setCashierName(cm.getCashier_Name());
                                        model.setCashierNumber(cm.getCashier_Number());

                                    }
                                    //------------------------------------

                                    model.setTrnastionType("PAYTO");
                                    model.setCount("0");
                                    model.setIsCredit("Cr");
                                    comment_getter(originalSMS.toString(), model);
                                    boolean check = parsingCommonDate(lPaytoSMSArray, model, context);
                                    if (check) {

                                    } else {
                                        if (!isForeground(context)) {
                                            Intent call = new Intent(context, NotificationActivity.class);
                                            call.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            context.startActivity(call);
                                        }
                                    }

                                return;
                            }
                        }
                    }

                }
            }
        }

    }

    private void sendBroadCastSMS(String opt, Context mContext) {
        Intent broadcast = new Intent();
        broadcast.setAction(BROADCAST_INTENT);
        broadcast.putExtra(PARAM_OTP, opt);
        broadcast.putExtra("verify", "true");
        mContext.sendBroadcast(broadcast);
    }

    private void sendBroadCastSMSFalase(String opt, Context mContext) {
        Intent broadcast = new Intent();
        broadcast.setAction(BROADCAST_INTENT);
        broadcast.putExtra(PARAM_OTP, opt);
        broadcast.putExtra("verify", "swap");
        mContext.sendBroadcast(broadcast);
    }

    private void sendBroadCastSMS1(Context mContext, String Message) {
        Intent broadcast = new Intent();
        broadcast.setAction(Constant.BROADCAST_INTENT_SMS);
        broadcast.putExtra("MessageBody", Message);
        mContext.sendBroadcast(broadcast);
    }



    public void comment_getter(String msg,TransationModel model)
    {
        try {
            int first_occuranceof_comma = msg.indexOf("\"", 1);
            int second_occurance_of_comma = msg.indexOf("\"", msg.indexOf("\"", 1) + 1);
            String comments = msg.substring(first_occuranceof_comma, second_occurance_of_comma);

            String comment[] = comments.split(",");
            if (comment.length == 1) {
                if (!comment[0].trim().equals("")) {
                    if (comment[0].startsWith("#OK")) {

                        model.setComments("");

                    }
                }
            }
            model.setComments(comment[0].substring(0, comment[0].length() - 2).replace("\"", ""));
        }catch (Exception e)
        {

        }
    }

    public boolean isForeground(Context context) {
        boolean isActivityRunning = false;
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> runningTaskInfo = manager.getRunningTasks(1);
        for (int i = 0; i < runningTaskInfo.size(); i++) {
            if (runningTaskInfo.get(i).topActivity.getClassName().toString().contains("NotificationActivity")) {
                isActivityRunning = true;
            }
        }

        return isActivityRunning;
    }

    private boolean parsingCommonDate(String lPaytoSMSArray[], TransationModel model, Context context) {

        model.setCount("0");
        model.setIsCredit("Cr");
        for (int i = 0; i < lPaytoSMSArray.length; i++)
        {
            if (lPaytoSMSArray[i].startsWith("mmk") && i < 1) {
                model.setAmount(lPaytoSMSArray[i + 1]);
            }
            if (lPaytoSMSArray[i].endsWith("(you)"))
            {
                String source = lPaytoSMSArray[i + 1];
                if (source.startsWith("+")) {
                    source = "00" + source.substring(1);
                    model.setSource(source);
                    Log.e("smsMyno","..."+source);
                }

            }


            if (lPaytoSMSArray[i].startsWith("from)") || lPaytoSMSArray[i].startsWith("From)")) {

                String source = lPaytoSMSArray[i + 1];
                if (source.startsWith("+")) {
                    source = "00" + source.substring(1);
                    model.setDestination(source);
                }

            }
            if (lPaytoSMSArray[i].startsWith("@")) {
                model.setDate(lPaytoSMSArray[i + 1] + " " + lPaytoSMSArray[i + 2]);
                model.setResponsects(lPaytoSMSArray[i + 1] + " " + lPaytoSMSArray[i + 2]);
                try {
                    SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", Locale.US);
                    model.setmTranscationDate(dateFormatter.parse(lPaytoSMSArray[i + 1] + " " + lPaytoSMSArray[i + 2]));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (lPaytoSMSArray[i].startsWith("Ref:")) {
                model.setTransatId(lPaytoSMSArray[i + 1]);
            }

            if (lPaytoSMSArray[i].startsWith("TicketNo:")) {
                model.setTransatId(lPaytoSMSArray[i + 1]);
            }
            if (lPaytoSMSArray[i].startsWith("Number:")) {
                model.setTransatId(lPaytoSMSArray[i + 1]);
            }

            if (lPaytoSMSArray[i].startsWith("Balance:")) {
                model.setBalance(lPaytoSMSArray[i + 1]);
                model.setmWalletbalance(lPaytoSMSArray[i + 1]);
            }
//            if (lPaytoSMSArray[i].startsWith("Msg:")) {
//                String msg = lPaytoSMSArray[i + 1];
//                String components[] = msg.split(",");
//                if (components[0].length() > 1) {
//                    if(components[0].startsWith("#OK"))
//                        model.setComments("");
//                    else
//                        model.setComments(components[0].substring(0, components[0].length() - 1).replace("\"", ""));
//                    // model.setLatLong();
//                    // model.setComments(lPaytoSMSArray[i + 1]);
//
//                }
//            }
        }

        //       System.out.print(model.toString());
        DBHelper db = new DBHelper(context);
        boolean isCheck = DBHelper.isTranscationPresent(model.getTransatId());
        if (isCheck) {

        } else {
            DBHelper.insertTransation(model);
            DBHelper.insertRECEIVEDTransacation(model);
            db.close();
        }
        return isCheck;
    }

}
