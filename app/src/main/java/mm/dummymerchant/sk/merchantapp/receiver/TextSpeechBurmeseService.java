package mm.dummymerchant.sk.merchantapp.receiver;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import mm.dummymerchant.sk.merchantapp.Utils.BurmeseConverter;

public class TextSpeechBurmeseService extends Service implements BurmeseConverter.IEnglishToBurmeseString, BurmeseConverter.RepeatSound {
    public static String amount = "";
    private BurmeseConverter burmeseConverter;

    @Override
    public void onCreate() {
        burmeseConverter = new BurmeseConverter(this, "", getApplicationContext());
        burmeseConverter.burmeseAmountSoundStart("received", amount, "received");
        while (burmeseConverter.isPlaying()) {
        }
        stopSelf();
        Log.e("textplay","normalstop");
    }

    @Override
    public void onDestroy() {
        Log.e("textplay","destoryu");
        try {
            if (null != burmeseConverter)
                burmeseConverter.resetPlayer(amount);
        } catch (Exception ignored) {
        }
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public void englishToBurmeseString(String value) {

    }

    @Override
    public void burmeseSoundRepeat(boolean b) {
        Log.e("textplay","destory");
        stopSelf();
    }
}