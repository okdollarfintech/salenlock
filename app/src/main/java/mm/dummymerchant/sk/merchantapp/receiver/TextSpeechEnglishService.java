package mm.dummymerchant.sk.merchantapp.receiver;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.util.Log;

import java.util.HashMap;
import java.util.Locale;

public class TextSpeechEnglishService extends Service implements TextToSpeech.OnInitListener {
    private TextToSpeech mTts;
    public static String spokenText = "";

    @Override
    public void onCreate() {
        mTts = new TextToSpeech(this, this);
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            int result = mTts.setLanguage(Locale.ENGLISH);
            if (result != TextToSpeech.LANG_MISSING_DATA && result != TextToSpeech.LANG_NOT_SUPPORTED) {
                mTts.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                    @Override
                    public void onStart(String utteranceId) {
                        Log.e("textplay","start");
                    }

                    @Override
                    public void onDone(String utteranceId) {
                        stopSelf();
                        Log.e("textplay","done");
                    }

                    @Override
                    public void onError(String utteranceId) {
                        stopSelf();
                        Log.e("textplay","error");
                    }
                });
                HashMap<String, String> map = new HashMap<String, String>();
                map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID,"messageID");

                mTts.speak(spokenText, TextToSpeech.QUEUE_FLUSH, map);
            }
        }
    }

    @Override
    public void onDestroy() {
        Log.e("textplay","ondestroy");
        if (mTts != null) {
            mTts.stop();
            mTts.shutdown();
        }
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

}