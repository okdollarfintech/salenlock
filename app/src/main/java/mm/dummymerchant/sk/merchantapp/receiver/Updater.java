package mm.dummymerchant.sk.merchantapp.receiver;

import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.view.WindowManager;

import org.apache.http.client.HttpClient;

import java.io.File;

import mm.dummymerchant.sk.merchantapp.R;
import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.CallUpdateAppInterface;

/**
 * Created by Dell on 5/19/2016.
 */
public class Updater extends BroadcastReceiver {

    public static Context context;
    public  int version;
    public  int current_version;
    private  String Url;
    private  String Type ;
    private static long downloadReference=0l;
    private  static DownloadManager downloadManager;
    private  boolean downloading=false;
    private  String status;
    private static int size=0;
    private  Thread first,second,third;
    HttpClient httpclient;

    @Override
    public void onReceive(final Context context, Intent intent) {

        this.context=context;

        Url = intent.getExtras().getString("url");
        Type = intent.getExtras().getString("type");
        String temp = intent.getExtras().getString("version");
        version = Integer.parseInt(temp);
        size =  intent.getExtras().getInt("size");
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            current_version = pInfo.versionCode;
        }catch (Exception e)
        {
            current_version=1;
        }
        try {
            //       Toast.makeText(context,"Downloading the Version Info", Toast.LENGTH_LONG).show();
        }catch (Exception e)
        {

        }
        Download();


    }
    public void Download()
    {
        try {
            if (current_version >=version) {
                return;
            }
        }catch (Exception e){}
        String versionCode=null;
        File file_internal = new File(context.getFilesDir()+"/Download/", ""+version+".apk");
        try {
            File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), ""+version+".apk");
            System.out.println("Files : " +file);
            System.out.println("file_internal : " +file_internal);

            if (file.exists()) {
                if(version == AppPreference.getLastVersion(context))
                {
                    if(Type.equals("Critical")) {
                        common_method(file,Type);
                    }
                }else
                {
                    common_method(file,Type);
                }
            }else if(file_internal.exists()){
                if(version == AppPreference.getLastVersion(context)) {
                    if(Type.equals("Critical")) {
                        common_method(file_internal,Type);
                    }
                }else
                {
                    common_method(file_internal,Type);
                }
            }else
            {
                download_code();
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public  void download_code()
    {
        if(downloadReference==0) {
            downloadManager = (DownloadManager) context.getSystemService(context.DOWNLOAD_SERVICE);
            Uri Download_Uri = Uri.parse(Url);
            DownloadManager.Request request = new DownloadManager.Request(Download_Uri);
            //Restrict the types of networks over which this download may proceed.
            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
            //Set whether this download may proceed over a roaming connection.
            request.setAllowedOverRoaming(true);
            request.setVisibleInDownloadsUi(true);
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE);
            //Set the title of this download, to be displayed in notifications (if enabled).
            request.setTitle("Updating Sale N Lock");
            //Set a description of this download, to be displayed in notifications (if enabled)
            request.setDescription("Sale N Lock application updater");

            //Set the local destination for the downloaded file to a path within the application's external files directory
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, ""+version+".apk");
            //Enqueue a new download and same the referenceId
            downloadReference = downloadManager.enqueue(request);

        }
    }

    public  void common_method(final File file,String Type)
    {
        try {
            check_status_();
            if (status != null) {
                if(status.contains("ERROR"))
                {
                    file.delete();
                    downloadReference = 0;
                    download_code();
                    return;
                }
                if (!status.equals("STATUS_PENDING") && !status.equals("STATUS_RUNNING")&& !status.contains("PAUSED")) {
                    if (file.length() < size) {
                        file.delete();
                        downloadReference = 0;
                        download_code();
                    } else {
                        if (current_version == Integer.parseInt(file.getName().replace(".apk", ""))) {
                            // file.delete();
                            downloadReference = 0;
                            //download_code();
                        } else {
                            if(Type.equals("Critical")) {
                                showNewUpdateApp(file, context.getString(R.string.new_update_msg1), false,version);
                            }else
                            {
                                showNewUpdateApp(file, context.getString(R.string.new_update_msg2),true,version);
                            }
                            //     showAlert(file);
                            //else if(Integer.parseInt(file.getName())>versionCode)
//                            Intent intent1 = new Intent(Intent.ACTION_VIEW);
//                            intent1.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
//                            intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                            context.startActivity(intent1);
                            // downloadReference=-1;
                        }
                    }
                }
            }else if(downloadReference==0)
            {
                if (file.length() < size) {
                    file.delete();
                    downloadReference = 0;
                    download_code();
                } else {
                    if (current_version == Integer.parseInt(file.getName().replace(".apk", ""))) {
                        downloadReference = 0;
                    } else {
                        if(Type.equals("Critical")) {
                            showNewUpdateApp(file, context.getString(R.string.new_update_msg1), false,version);
                        }else
                        {
                            showNewUpdateApp(file, context.getString(R.string.new_update_msg2), true,version);
                        }
                    }
                }

            }

        }catch (Exception e)
        {
            e.printStackTrace();

        }

    }

    private void displayAlert(final File file,String message, final boolean enable) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        // builder.setCancelable(true);
        if (enable) {
            builder.setMessage(message).setCancelable(false).setPositiveButton("Yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            Intent intent1 = new Intent(Intent.ACTION_VIEW);
                            intent1.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
                            intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intent1);
                            try {
                                System.exit(1);
                            }catch (Exception e){}
                        }
                    }).setNegativeButton("No",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            AppPreference.setVersionLast(context, version);
                        }
                    });

        } else {
            builder.setMessage(message).setCancelable(false).setPositiveButton("Install",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            Intent intent1 = new Intent(Intent.ACTION_VIEW);
                            intent1.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
                            intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intent1);
                            try {
                                System.exit(1);
                            }catch (Exception e){}
                        }
                    });

        }
        AlertDialog alert = builder.create();
        alert.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR);
        alert.show();
        alert.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                if (!enable)
                    try {
                        System.exit(1);
                    } catch (Exception e) {
                    }
            }
        });
    }

    private  void check_status_()
    {
        try {
            DownloadManager.Query myDownloadQuery = new DownloadManager.Query();
            //set the query filter to our previously Enqueued download
            myDownloadQuery.setFilterById(downloadReference);
            //Query the download manager about downloads that have been requested.
            Cursor cursor = downloadManager.query(myDownloadQuery);
            if (cursor.moveToFirst()) {
                status = checkStatus(cursor);
            }
        }catch (Exception e)
        {}
    }
    private  String checkStatus(Cursor cursor){
        //column for status
        int columnIndex = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS);
        int status = cursor.getInt(columnIndex);
        //column for reason code if the download failed or paused
        int columnReason = cursor.getColumnIndex(DownloadManager.COLUMN_REASON);
        int reason = cursor.getInt(columnReason);
        //get the download filename
        int filenameIndex = cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_FILENAME);
        String filename = cursor.getString(filenameIndex);

        String statusText = "";
        String reasonText = "";

        switch(status){
            case DownloadManager.STATUS_FAILED:
                downloading=false;
                downloadReference=0;
                statusText = "STATUS_FAILED";
                switch(reason){
                    case DownloadManager.ERROR_CANNOT_RESUME:
                        reasonText = "ERROR_CANNOT_RESUME";
                        break;
                    case DownloadManager.ERROR_DEVICE_NOT_FOUND:
                        reasonText = "ERROR_DEVICE_NOT_FOUND";
                        break;
                    case DownloadManager.ERROR_FILE_ALREADY_EXISTS:
                        reasonText = "ERROR_FILE_ALREADY_EXISTS";
                        break;
                    case DownloadManager.ERROR_FILE_ERROR:
                        reasonText = "ERROR_FILE_ERROR";
                        break;
                    case DownloadManager.ERROR_HTTP_DATA_ERROR:
                        reasonText = "ERROR_HTTP_DATA_ERROR";
                        break;
                    case DownloadManager.ERROR_INSUFFICIENT_SPACE:
                        reasonText = "ERROR_INSUFFICIENT_SPACE";
                        break;
                    case DownloadManager.ERROR_TOO_MANY_REDIRECTS:
                        reasonText = "ERROR_TOO_MANY_REDIRECTS";
                        break;
                    case DownloadManager.ERROR_UNHANDLED_HTTP_CODE:
                        reasonText = "ERROR_UNHANDLED_HTTP_CODE";
                        break;
                    case DownloadManager.ERROR_UNKNOWN:
                        reasonText = "ERROR_UNKNOWN";
                        break;
                }
                break;
            case DownloadManager.STATUS_PAUSED:
                statusText = "STATUS_PAUSED";
                switch(reason){
                    case DownloadManager.PAUSED_QUEUED_FOR_WIFI:
                        reasonText = "PAUSED_QUEUED_FOR_WIFI";
                        break;
                    case DownloadManager.PAUSED_UNKNOWN:
                        reasonText = "PAUSED_UNKNOWN";
                        break;
                    case DownloadManager.PAUSED_WAITING_FOR_NETWORK:
                        reasonText = "PAUSED_WAITING_FOR_NETWORK";
                        break;
                    case DownloadManager.PAUSED_WAITING_TO_RETRY:
                        reasonText = "PAUSED_WAITING_TO_RETRY";
                        break;
                }
                break;
            case DownloadManager.STATUS_PENDING:
                reasonText = "STATUS_PENDING";
                break;
            case DownloadManager.STATUS_RUNNING:
                reasonText = "STATUS_RUNNING";
                break;
            case DownloadManager.STATUS_SUCCESSFUL:
                downloading=false;
                downloadReference=0;
                reasonText = "STATUS_SUCCESSFUL";
                //  reasonText = "Filename:\n" + filename;
                break;
        }

        return reasonText;

    }

    public void showNewUpdateApp(final File file,String message, final boolean enable, int version)
    {
        CallUpdateAppInterface.getInstance().showNewUpdateApp(file,message,enable,version);
    }

}
