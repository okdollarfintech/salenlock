package mm.dummymerchant.sk.merchantapp.receiver;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

import mm.dummymerchant.sk.merchantapp.CrashActivity;
import mm.dummymerchant.sk.merchantapp.SliderScreen;
import mm.dummymerchant.sk.merchantapp.Utils.Constant;
import mm.dummymerchant.sk.merchantapp.Utils.MySendSMS;
import mm.dummymerchant.sk.merchantapp.Utils.MySendSMSStatus;
import mm.dummymerchant.sk.merchantapp.Utils.Utils;
import mm.dummymerchant.sk.merchantapp.db.DBHelper;
import mm.dummymerchant.sk.merchantapp.maills.MailBroadCast;
import mm.dummymerchant.sk.merchantapp.model.UserInctivityModel;

/**
 * Created by Dell on 7/28/2016.
 */
public class UserInactiveService extends Service implements MySendSMSStatus,Constant{
    Thread timerThread;
    private static Timer timer;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


            new Thread(new Runnable() {
                @Override
                public void run() {

                        timer = new Timer();
                        timer.scheduleAtFixedRate(new MYTimerTask(), 60000,120000);

                }
            }).start();


        return START_STICKY;
    }

    @Override
    public void onSent() {

    }

    @Override
    public void onError(String error) {

    }

    @Override
    public void onDelivered(String msg) {

    }

    public class MYTimerTask extends TimerTask
    {

        @Override
        public void run() {
            System.out.println("Hello !!!");
           // new MySendSMS(getApplicationContext()).sendSMS("00959258819988", "user inactive  for 15 minutes") ;
            Intent intent = new Intent(getBaseContext(), MailBroadCast.class);
            Bundle b=new Bundle();
            b.putString(BUNDLE_KEY_FROM_EMAIL_ID,"infoarunmsc@gmail.com");
            b.putString(BUNDLE_KEY_PWD,"9884648228");
            b.putString(BUNDLE_KEY_TO_EMAIL_ID,"mearun2789@gmail.com");
            intent.putExtras(b);
            startService(intent);
            UserInctivityModel model = new Utils().getUserInctivityModel(getApplicationContext(), SliderScreen.present_Cashier_Id, "");
            DBHelper db = new DBHelper(getApplicationContext());
            db.open();
            db.insertUserInactivityModel(model);
            db.close();

        }
    }

    @Override
    public void onDestroy() {
    Log.e("destroy", "...destroy");
        timer.cancel();
    }

}
