package mm.dummymerchant.sk.merchantapp.receiver;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.EnglishNumberToWords;
import mm.dummymerchant.sk.merchantapp.Utils.SmsVerification;

/**
 * Created by User on 3/14/2018.
 */

public class ValidateSMSPayTo {

    private final Context mContext;
    private double amount = 0d;

    public ValidateSMSPayTo(Context context) {
        this.mContext = context;
    }

    public void checkPayToMessageReceived(String strMessage, String address) {
        if (SmsVerification.with(mContext).isAddressPresent(address)) {
            if (strMessage.startsWith("you have received")) {
                String[] data = strMessage.split("mmk");
                try {
                    if (data.length > 1)
                        amount = Double.parseDouble(data[0].replace("you have received", "").replace(",", "").trim());
                } catch (NumberFormatException ignored) {
                }
                if (amount > 0) {
                    if (AppPreference.getLanguage(mContext).equalsIgnoreCase("my")) {
                        TextSpeechBurmeseService.amount = Double.toString(amount);
                        mContext.startService(new Intent(mContext, TextSpeechBurmeseService.class));
                    } else {
                        TextSpeechEnglishService.spokenText = EnglishNumberToWords.convert((long) amount);
                        mContext.startService(new Intent(mContext, TextSpeechEnglishService.class));
                    }
                }
            }
        } else if (strMessage.contains("okname") || strMessage.contains("msg:") || strMessage.contains("your account balance is")) {
            Log.e("payto", "FRAUD " + address);
        }
    }

}
