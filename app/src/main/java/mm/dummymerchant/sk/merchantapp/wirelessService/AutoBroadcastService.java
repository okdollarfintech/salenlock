package mm.dummymerchant.sk.merchantapp.wirelessService;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.IBinder;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;



import java.util.Calendar;
import java.util.Date;
import java.util.List;

import mm.dummymerchant.sk.merchantapp.BaseActivity;
import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.db.DBHelper;


public class AutoBroadcastService extends Service implements AutoPaymentInterface.onFindMerchnatInteface, AutoPaymentInterface.onCheckMerchnatAreaInterface {
    private Context mContext;
    private WifiManager wifiManagers;
    private Boolean isFunctionCalled;
    private Boolean isCalledByMerchant;
    private String tempCellID;
    private TelephonyManager telephony;
    private PendingIntent pendingIntent;
    private AutoGPSLocationUpdate brLocationUpdate;
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        Log.i("[BroadcastReceiver]", "Service killed");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("[BroadcastReceiver]", "Service started");
        mContext=AutoBroadcastService.this;
        telephony = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        tempCellID = getLACAndCID();
        brLocationUpdate= new AutoGPSLocationUpdate(this);
        //brLocationUpdate.registerLocationListener();
        checkMerchantStatus();
        wifiManagers = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        wifiManagers.startScan();
        isFunctionCalled=false;
        isCalledByMerchant=false;
        return super.onStartCommand(intent, flags, startId);

    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public void onCreate() {
        AutoPaymentInterface.getInstance().setListenerFindMerchant(this);
        AutoPaymentInterface.getInstance().setListenerCheckMerchant(this);
        isFunctionCalled=false;
        isCalledByMerchant=false;
        registerReceiver(mybroadcast, new IntentFilter(Intent.ACTION_SCREEN_ON));
        registerReceiver(mybroadcast, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        registerReceiver(mybroadcast, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));
        registerReceiver(mybroadcast, new IntentFilter(Intent.ACTION_BATTERY_LOW));
        registerReceiver(mybroadcast, new IntentFilter(BluetoothAdapter.ACTION_LOCAL_NAME_CHANGED));
        registerReceiver(mybroadcast,new IntentFilter(Intent.ACTION_BOOT_COMPLETED));
        Log.i("[BroadcastReceiver]", "Service Created");
        super.onCreate();
    }
    private final BroadcastReceiver mybroadcast = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            Log.i("[BroadcastReceiver]", "MyReceiver");
            switch (intent.getAction()){

                case Intent.ACTION_SCREEN_ON:
                    Log.i("[BroadcastReceiver]", "Screen ON");

                        checkMerchantStatus();
                    break;
                case WifiManager.SCAN_RESULTS_AVAILABLE_ACTION:
                    Log.i("[BroadcastReceiver]", "SCAN_RESULTS_AVAILABLE_ACTION");
                    if(wifiManagers!=null)
                    BaseActivity.mWifiResult=wifiManagers.getScanResults();
                    if(!isFunctionCalled)
                    checkMerchantStatus();
                    break;
                case BluetoothAdapter.ACTION_STATE_CHANGED:
                    Log.i("[BroadcastReceiver]", "BLT Action Changed");
                    final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
                    if(state== BluetoothAdapter.STATE_OFF)
                    {
                        Log.i("[BroadcastReceiver]", "Action State OFF");
                        AppPreference.setBLTStatus(mContext, false);
                        WirelessUtils.getInstance().changeBluetoothName(false, mContext);
                    }
                    break;
                case Intent.ACTION_BATTERY_LOW:
                    Log.i("[BroadcastReceiver]", "ACTION_BATTERY_LOW");
                    //baseActivity.showAlert("Please charge your phone to keep AutoPayment function active.");
                    break;
                case BluetoothAdapter.ACTION_LOCAL_NAME_CHANGED:
                    Log.i("[BroadcastReceiver]", "BLT NAME Changed");
                    checkMerchantStatus();
                    break;
                case Intent.ACTION_BOOT_COMPLETED:
                    checkMerchantStatus();
                    break;
                default:
                    break;

            }

        }
    };
    private String getLACAndCID() {
        if (telephony.getPhoneType() == TelephonyManager.PHONE_TYPE_GSM) {
            GsmCellLocation location = (GsmCellLocation) telephony.getCellLocation();
            if (location != null) {
                String cellID= String.valueOf(location.getCid());
                if(cellID.length()>3)
                return cellID.substring(0,cellID.length()-2);
               // return null;
            }
        }
        if (telephony.getPhoneType() == TelephonyManager.PHONE_TYPE_CDMA) {
            CdmaCellLocation location = (CdmaCellLocation) telephony.getCellLocation();
            if (location != null) {
                String cellID= String.valueOf(location.getBaseStationId());
                if(cellID.length()>3)
                return cellID.substring(0,cellID.length()-2);
               // return null;
            }
        }
        return "";
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mybroadcast);
        autoPaymentCancel();
        final WifiManager wifiManagers = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        if (!wifiManagers.isWifiEnabled()) {
            WirelessUtils.getInstance().setHotspotOFF(mContext);
        }
        Log.d("TestBLT", "in Service when Distroy");
        WirelessUtils.getInstance().setBluetooth(false, mContext);
        WirelessUtils.getInstance().changeBluetoothName(false, mContext);
        new CallBackgroundAPI(AppPreference.getMyMobileNo(mContext),telephony.getSimSerialNumber(),tempCellID,brLocationUpdate.getLatitude(),brLocationUpdate.getLongitude(),0,AppPreference.getCurrentShopID(mContext)).updateMerchantBusinessArea();
        brLocationUpdate.removeLocationListener();
    }

    private void getWifiMacList() {
        autoPaymentCancel();
        List<ScanResult> scanResultList = wifiManagers.getScanResults();
        Boolean isWifiMacFound=false;
        if(scanResultList!=null) {
            int size = scanResultList.size() - 1;
            while (size >= 0) {
                String name = scanResultList.get(size).BSSID;
                        if (DBHelper.getIsShopWifiIDActive(name))
                        {
                            updateBLTstatus();
                            if (!wifiManagers.isWifiEnabled()) {
                                WirelessUtils.getInstance().setHotSpotON(mContext);
                            }
                            Log.d("TestBLT", "in Service when WifiMAC found");
                            String shopId= AppPreference.getCurrentShopID(mContext);
                            WirelessUtils.getInstance().setBluetooth(true, mContext);
                            WirelessUtils.getInstance().changeBluetoothName(true, mContext);
                            AutoPaymentInterface.getInstance().updateMerchantModule(DBHelper.getShopName(shopId));
                            //AppPreference.setCurrentShopID(mContext, shopId);
                            new CallBackgroundAPI(AppPreference.getMyMobileNo(mContext),telephony.getSimSerialNumber(),tempCellID,brLocationUpdate.getLatitude(),brLocationUpdate.getLongitude(),1,shopId).updateMerchantBusinessArea();
                            isWifiMacFound=true;
                            isFunctionCalled=true;
                            break;
                        }

                size--;
                }
            }
        if(!isWifiMacFound){
            if(AppPreference.isOnByMerchant(mContext)) {
                if (!wifiManagers.isWifiEnabled()) {
                    WirelessUtils.getInstance().setHotspotOFF(mContext);
                }
                Log.d("TestBLT", "in Service Broadcast when cell ID not found");
                WirelessUtils.getInstance().setBluetooth(false, mContext);
                WirelessUtils.getInstance().changeBluetoothName(false, mContext);
                AppPreference.setIsONbyMerchant(mContext, false);
                AutoPaymentInterface.getInstance().updateMerchantModule("Not Found");
                //if(!isFunctionCalled)
                new CallBackgroundAPI(AppPreference.getMyMobileNo(mContext),telephony.getSimSerialNumber(),tempCellID,brLocationUpdate.getLatitude(),brLocationUpdate.getLongitude(),0,AppPreference.getCurrentShopID(mContext)).updateMerchantBusinessArea();

            }
            if(isCalledByMerchant) {
                AutoPaymentInterface.getInstance().updateMerchantModule("Not in area");
                isCalledByMerchant=false;
            }
        }

    }

    private void checkMerchantStatus(){
        wifiManagers = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        Intent alarmIntent = new Intent(this, AutoPaymentReceiver.class);
        alarmIntent.putExtra("Code", "AUTO_BROADCAST_SHOP");
        pendingIntent = PendingIntent.getBroadcast(this, 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        if(AppPreference.getMerchantStatus(mContext) && DBHelper.getAllAreaCount()>0)
        {
            final String tempCellID = getLACAndCID();

            if (AppPreference.getAutoPayment(mContext) && DBHelper.getIsShopIDActive(AppPreference.getCurrentShopID(mContext))) {
                updateBLTstatus();
                if (!wifiManagers.isWifiEnabled()) {
                    WirelessUtils.getInstance().setHotSpotON(mContext);
                }
                Log.d("TestBLT", "in Service Start when AutoPayment for all location");
                WirelessUtils.getInstance().setBluetooth(true, mContext);
                WirelessUtils.getInstance().changeBluetoothName(true, mContext);
                String shopId= AppPreference.getCurrentShopID(mContext);
                AutoPaymentInterface.getInstance().updateMerchantModule(DBHelper.getShopName(shopId));
                autoPaymentStart();
                isFunctionCalled=true;
                new CallBackgroundAPI(AppPreference.getMyMobileNo(mContext),telephony.getSimSerialNumber(),tempCellID,brLocationUpdate.getLatitude(),brLocationUpdate.getLongitude(),2,shopId).updateMerchantBusinessArea();

            } else if (!tempCellID.isEmpty() && tempCellID.length() > 3) {
                if (DBHelper.getIsShopCellIDActive(tempCellID))
                {
                    autoPaymentCancel();
                    updateBLTstatus();
                    if (!wifiManagers.isWifiEnabled()) {

                        WirelessUtils.getInstance().setHotSpotON(mContext);
                    }
                    Log.d("TestBLT", "in Service Start when cell id found");
                    isFunctionCalled=true;
                    String shopId= DBHelper.getShopID(tempCellID);
                    WirelessUtils.getInstance().setBluetooth(true, mContext);
                    WirelessUtils.getInstance().changeBluetoothName(true, mContext);
                    AutoPaymentInterface.getInstance().updateMerchantModule(DBHelper.getShopName(tempCellID));
                    //AppPreference.setCurrentShopID(mContext, shopId);
                    new CallBackgroundAPI(AppPreference.getMyMobileNo(mContext),telephony.getSimSerialNumber(),tempCellID,brLocationUpdate.getLatitude(),brLocationUpdate.getLongitude(),1,shopId).updateMerchantBusinessArea();
                    Log.i("[BroadcastReceiver]", "OLD Cell ID: " + tempCellID);
                } else {
                    getWifiMacList();
                }

            } else {
                getWifiMacList();
            }
        }else {

            AutoPaymentInterface.getInstance().updateMerchantModule("Not Found");
        }
    }

    private void updateBLTstatus(){
        if(!AppPreference.isOnByMerchant(mContext)){
            BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            boolean isEnabled = bluetoothAdapter.isEnabled();
            Log.d("BLT", "BLT NEW Status:" + isEnabled);
            AppPreference.setBLTStatus(mContext, isEnabled);
            AppPreference.setIsONbyMerchant(mContext, true);
        }
    }

    @Override
    public void findMerchantStatus()
    {
        wifiManagers.startScan();
        isFunctionCalled=false;
        isCalledByMerchant=false;
        checkMerchantStatus();
    }

    private void autoPaymentStart()
    {
        AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        int interval = 1000 * 60 * 1;

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.setTime(new Date());
        //* Repeating on every 3 minutes interval *//*
        manager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),interval, pendingIntent);
        }

    private void autoPaymentCancel() {
        AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        manager.cancel(pendingIntent);
        /**/
    }


    @Override
    public void checkMerchantArea() {
        wifiManagers.startScan();
        isFunctionCalled=false;
        isCalledByMerchant=true;
        checkMerchantStatus();
    }
}
