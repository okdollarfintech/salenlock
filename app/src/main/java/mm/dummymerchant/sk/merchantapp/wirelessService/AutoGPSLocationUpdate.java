package mm.dummymerchant.sk.merchantapp.wirelessService;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;


public class AutoGPSLocationUpdate {
    private final LocationManager locationManager;
    private static final int ONE_MINUTES = 1000 * 60 * 1;
    //private final TelephonyManager telephony;
    private double latitude;
    private double longitude;
    public AutoGPSLocationUpdate(Context context) {
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        //telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        registerLocationListener();
    }

    private final LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            Log.d("Location", "Method Called");
            setLatitude(location.getLatitude());
            setLongitude(location.getLongitude());
            Log.d("New Location Lat", latitude + "");
            Log.d("New Location Lang", longitude + "");

        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };


    private void registerLocationListener() {
        if (locationManager.getAllProviders().contains(LocationManager.NETWORK_PROVIDER))
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, ONE_MINUTES, 100, locationListener);

        if (locationManager.getAllProviders().contains(LocationManager.GPS_PROVIDER))
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, ONE_MINUTES, 100, locationListener);

    }

    public void removeLocationListener(){
        if(locationListener!=null)
        locationManager.removeUpdates(locationListener);
    }

    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null || location==null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > ONE_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -ONE_MINUTES;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /** Checks whether two providers are the same */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

    public Location getLastLocation(){
        if(locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)!=null)
        return locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        else if(locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)!=null)
            return locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        else if(locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER)!=null)
            return locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
        else
            return null;
    }


    public double getLatitude() {
        if(latitude>0)
            return latitude;
        else if(getLastLocation()!=null)
            return getLastLocation().getLatitude();
        else
            return 0.0;
    }

    public double getLongitude() {
        if(longitude>0)
            return longitude;
        else if(getLastLocation()!=null)
            return getLastLocation().getLongitude();
        else
            return 0.0;
    }
    private void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    private void setLatitude(double latitude) {
        this.latitude = latitude;
    }

}
