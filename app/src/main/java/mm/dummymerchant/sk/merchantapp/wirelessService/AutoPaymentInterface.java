package mm.dummymerchant.sk.merchantapp.wirelessService;

public class AutoPaymentInterface {

    private static AutoPaymentInterface mInstance;
    private onAutoPaymentInterface mListenerUpdateM;
    private onFindMerchnatInteface mListenerFindM;
    private onCheckMerchnatAreaInterface mListenerCheckArea;

    public interface onAutoPaymentInterface {
        void updateMerchantStatus(String areaName);
        void updateBusinessName(String name);
    }

    public interface onFindMerchnatInteface {
        void findMerchantStatus();
    }

    public interface onCheckMerchnatAreaInterface {
        void checkMerchantArea();
    }

    public void setListener(onAutoPaymentInterface mListener) {
        this.mListenerUpdateM = mListener;
    }

    public void setListenerFindMerchant(onFindMerchnatInteface mListener) {
        this.mListenerFindM = mListener;
    }

    public void setListenerCheckMerchant(onCheckMerchnatAreaInterface mListener) {
        this.mListenerCheckArea = mListener;
    }

    public void updateMerchantModule(String areaName) {
        if (mListenerUpdateM != null) {
            mListenerUpdateM.updateMerchantStatus(areaName);
        }

    }

    public void updateMerchantBusinessName(String name){
        if (mListenerUpdateM != null) {
            mListenerUpdateM.updateBusinessName(name);
        }
    }

    public void findMerchantStatus() {
        if (mListenerFindM != null) {
            mListenerFindM.findMerchantStatus();
        }
    }

    public void checkMerchantArea() {
        if (mListenerCheckArea != null) {
            mListenerCheckArea.checkMerchantArea();
        }
    }


    public static AutoPaymentInterface getInstance() {
        if (mInstance == null) {
            mInstance = new AutoPaymentInterface();
        }
        return mInstance;
    }

}
