package mm.dummymerchant.sk.merchantapp.wirelessService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;

import mm.dummymerchant.sk.merchantapp.GPSTracker;
import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;


public class AutoPaymentReceiver extends BroadcastReceiver {
    private TelephonyManager telephony;
    private AutoGPSLocationUpdate brLocationUpdate;
    @Override
    public void onReceive(Context context, Intent intent) {

        telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        final String tempCellID = getLACAndCID();
        String code=intent.getStringExtra("Code");
        brLocationUpdate= new AutoGPSLocationUpdate(context);

        /*String LatLang = getLocationFromNetwork(context);*/
        String Lat = brLocationUpdate.getLatitude()+"";
        String Lang = brLocationUpdate.getLongitude()+"";
        switch (code)
        {
            case "AUTO_REFRESH_MAP":
                Log.d("AutoUpdateMap", "Working");
                String distance = "3000";
                String mCode = intent.getStringExtra("mCode");
                String sCode = intent.getStringExtra("sCode");
                Log.d("AutoUpdateMap LatLang", Lat + " " + Lang);
                new CallAutoRefreshAPI(Lat,Lang,tempCellID,distance,mCode,sCode).updateShopInBackground();
                break;
            case "AUTO_BROADCAST_SHOP":
                Log.d("AutoBoradcast", "Working");
                Log.d("AutoBoradcast LatLang", Lat + " " + Lang);
                String shopId = AppPreference.getCurrentShopID(context);
                new CallBackgroundAPI(AppPreference.getMyMobileNo(context), telephony.getSimSerialNumber(), tempCellID, Double.parseDouble(Lat), Double.parseDouble(Lang), 2, shopId).updateMerchantBusinessArea();
                break;
        }
    }



    private String getLACAndCID() {
        if (telephony.getPhoneType() == TelephonyManager.PHONE_TYPE_GSM) {
            GsmCellLocation location = (GsmCellLocation) telephony.getCellLocation();
            if (location != null) {
                String cellID= String.valueOf(location.getCid());
                if(cellID.length()>3)
                return cellID.substring(0,cellID.length()-2);
                // return null;
            }
        }
        if (telephony.getPhoneType() == TelephonyManager.PHONE_TYPE_CDMA) {
            CdmaCellLocation location = (CdmaCellLocation) telephony.getCellLocation();
            if (location != null) {
                String cellID= String.valueOf(location.getBaseStationId());
                if(cellID.length()>3)
                return cellID.substring(0,cellID.length()-2);
                // return null;
            }
        }
        return "";
    }

    public String getLocationFromNetwork(Context context) {
        GPSTracker gpsTracker = new GPSTracker(context);
        if (gpsTracker.gpsStatus()) {
            String Latitude = String.valueOf(gpsTracker.getLatitude());
            String Longitude = String.valueOf(gpsTracker.getLongitude());
            if (Latitude.equals("0.0") || (Longitude.equals("0.0"))) {
                // showToast((R.string.gps_msg));
            } else {
                return Latitude + "," + Longitude;
            }
        } else {
            LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            boolean network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            if (network_enabled) {
                Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (location != null)
                    return location.getLatitude() + "," + location.getLongitude();
            }
        }
        return "0.0,0.0";
    }
}
