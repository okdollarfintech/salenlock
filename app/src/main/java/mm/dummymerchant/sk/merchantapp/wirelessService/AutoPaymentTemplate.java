package mm.dummymerchant.sk.merchantapp.wirelessService;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.View;
import android.view.Window;
import android.widget.EditText;

import mm.dummymerchant.sk.merchantapp.R;
import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;
import mm.dummymerchant.sk.merchantapp.Utils.Utils;
import mm.dummymerchant.sk.merchantapp.customView.CustomButton;
import mm.dummymerchant.sk.merchantapp.customView.CustomEdittext;
import mm.dummymerchant.sk.merchantapp.customView.CustomTextView;


public class AutoPaymentTemplate {


    //For Bus
    private static void showBusTemplate(final Context context) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.bus_template);
        //CustomTextView title = (CustomTextView) dialog.findViewById(R.id.bus_dialogTitle);
        final CustomEdittext businessName = (CustomEdittext) dialog.findViewById(R.id.busName);
        final CustomEdittext routeDetails = (CustomEdittext) dialog.findViewById(R.id.routedetials);
        final CustomEdittext busNo = (CustomEdittext) dialog.findViewById(R.id.bus_no);
        CustomButton submit = (CustomButton) dialog.findViewById(R.id.bus_submit);
        CustomButton cancel = (CustomButton) dialog.findViewById(R.id.bus_cancel);

        setMaxLength(businessName, 50);
        setMaxLength(routeDetails,50);
        setMaxLength(busNo, 10);
        String name = AppPreference.getBusinessName(context);
        if(!name.isEmpty()) {
            businessName.setText(name);
            businessName.setSelection(name.length());
        }
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String BUSINESS_NAME = "";
                if (!busNo.getText().toString().isEmpty()) {
                    if (!businessName.getText().toString().isEmpty()) {
                        BUSINESS_NAME = String.format("%s:%s", context.getString(R.string.name), businessName.getText().toString());
                    }
                    if (!busNo.getText().toString().isEmpty()) {
                        if (!BUSINESS_NAME.isEmpty())
                            BUSINESS_NAME = BUSINESS_NAME + String.format(" | %s:%s", context.getString(R.string.bus_num), busNo.getText().toString());
                        else
                            BUSINESS_NAME = BUSINESS_NAME + String.format("%s:%s", context.getString(R.string.bus_num), busNo.getText().toString());

                    }
                    if (!routeDetails.getText().toString().isEmpty()) {
                        BUSINESS_NAME = BUSINESS_NAME + String.format(" | %s:%s", context.getString(R.string.routes), routeDetails.getText().toString());

                    }

                    AutoPaymentInterface.getInstance().updateMerchantBusinessName(BUSINESS_NAME);
                    dialog.dismiss();
                } else
                    Utils.showCustomToastMsg(context, R.string.fill_vehicle_no, false);
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    //For Taxi, Rental cars, Ferry
    private static void showVehicleTemplate(final Context context) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.car_taxi_templete);
        CustomButton submit = (CustomButton) dialog.findViewById(R.id.vachel_submit);
        CustomButton cancel = (CustomButton) dialog.findViewById(R.id.vachel_cancel);
        final CustomEdittext vachelName = (CustomEdittext) dialog.findViewById(R.id.vachelName);
        final CustomEdittext vachelType = (CustomEdittext) dialog.findViewById(R.id.vachelType);
        final CustomEdittext vachelColor = (CustomEdittext) dialog.findViewById(R.id.vachelColor);
        final CustomEdittext vachelNo = (CustomEdittext) dialog.findViewById(R.id.vachelNo);
        vachelName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vachelName.setTextSize(20.0f);
            }
        });
        setMaxLength(vachelName, 50);
        setMaxLength(vachelType, 50);
        setMaxLength(vachelColor, 20);
        setMaxLength(vachelNo, 10);
        String name = AppPreference.getBusinessName(context);
        if(!name.isEmpty()) {
            vachelName.setText(name);
            vachelName.setSelection(name.length());
        }
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String BUSINESS_NAME = "";
                if (!vachelNo.getText().toString().isEmpty()) {
                    if (!vachelName.getText().toString().isEmpty())
                        BUSINESS_NAME = String.format("%s:%s", context.getString(R.string.name), vachelName.getText().toString());
                    if (!vachelNo.getText().toString().isEmpty())
                        if (!BUSINESS_NAME.isEmpty())
                            BUSINESS_NAME = BUSINESS_NAME + String.format(" | %s:%s", context.getString(R.string.vehicle_no), vachelNo.getText().toString());
                        else
                            BUSINESS_NAME = String.format("%s:%s", context.getString(R.string.vehicle_no), vachelNo.getText().toString());

                    if (!vachelType.getText().toString().isEmpty())
                        BUSINESS_NAME = BUSINESS_NAME + String.format(" | %s:%s", context.getString(R.string.type), vachelType.getText().toString());

                    if (!vachelColor.getText().toString().isEmpty())
                        BUSINESS_NAME = BUSINESS_NAME + String.format(" | %s:%s", context.getString(R.string.color), vachelColor.getText().toString());

                    AutoPaymentInterface.getInstance().updateMerchantBusinessName(BUSINESS_NAME);
                    dialog.dismiss();
                } else
                    Utils.showCustomToastMsg(context, R.string.fill_vehicle_no, false);
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    //For ExpressTicketing

    private static void showExpressTicketTemplate(final Context context) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.express_ticketing_template);
        CustomButton submit = (CustomButton) dialog.findViewById(R.id.ticket_vachel_submit);
        CustomButton cancel = (CustomButton) dialog.findViewById(R.id.ticket_vachel_cancel);
        final CustomEdittext expressTicketName = (CustomEdittext) dialog.findViewById(R.id.ticket_businessName);
        final CustomEdittext expressTicketFrom = (CustomEdittext) dialog.findViewById(R.id.ticket_source);
        // final CustomEdittext expressTicketTo=(CustomEdittext)dialog.findViewById(R.id.ticket_dest_to);
        final CustomEdittext expressVachelNo = (CustomEdittext) dialog.findViewById(R.id.ticket_vachel_no);
        String name = AppPreference.getBusinessName(context);
        if(!name.isEmpty()) {
            expressTicketName.setText(name);
            expressTicketName.setSelection(name.length());
        }
        setMaxLength(expressTicketName,50);
        setMaxLength(expressTicketFrom, 50);
        setMaxLength(expressVachelNo, 10);
        expressTicketName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expressTicketName.setTextSize(20.0f);
            }
        });
        expressTicketName.setTextSize(18.0f);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String BUSINESS_NAME = "";
                if (!expressVachelNo.getText().toString().isEmpty()) {

                    if (!expressTicketName.getText().toString().isEmpty())
                        BUSINESS_NAME = String.format("%s:%s", context.getString(R.string.name), expressTicketName.getText().toString());
                    if (!expressVachelNo.getText().toString().isEmpty())
                        if (BUSINESS_NAME.isEmpty())
                            BUSINESS_NAME = String.format("%s:%s", context.getString(R.string.vehicle_no), expressVachelNo.getText().toString());
                        else
                            BUSINESS_NAME = BUSINESS_NAME + String.format(" | %s:%s", context.getString(R.string.vehicle_no), expressVachelNo.getText().toString());
                    if (!expressTicketFrom.getText().toString().isEmpty())
                        BUSINESS_NAME = BUSINESS_NAME + String.format(" | %s:%s", context.getString(R.string.routes), expressTicketFrom.getText().toString());

                    AutoPaymentInterface.getInstance().updateMerchantBusinessName(BUSINESS_NAME);
                    dialog.dismiss();
                } else
                    Utils.showCustomToastMsg(context, R.string.fill_vehicle_no, false);

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    //For other merchants
    private static void showDefaultTemplate(Context context) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.search_dialog);
        CustomTextView textView = (CustomTextView) dialog.findViewById(R.id.textViewTitle);
        final CustomEdittext edittext = (CustomEdittext) dialog.findViewById(R.id.et_search);
        CustomTextView btnSearch = (CustomTextView) dialog.findViewById(R.id.btn_search);
        textView.setText(context.getString(R.string.auto_payment_title));
        String name = AppPreference.getBusinessName(context);
        if(!name.isEmpty()) {
            edittext.setText(name);
            edittext.setSelection(name.length());
        }
        setMaxLength(edittext, 50);
                btnSearch.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!edittext.getText().toString().isEmpty()) {
                            AutoPaymentInterface.getInstance().updateMerchantBusinessName(edittext.getText().toString());
                            dialog.dismiss();
                        }
                    }
                });

        dialog.show();
    }

    public static void dialogAddNewMerchant(Context context) {

        if (AppPreference.getBusinessMainCategory(context).equals("04")) {
            if (AppPreference.getBusinessSubCategory(context).equals("00")) {
                showBusTemplate(context);
            } else if (AppPreference.getBusinessSubCategory(context).equals("01") || AppPreference.getBusinessSubCategory(context).equals("02")) {
                showVehicleTemplate(context);

            } else if (AppPreference.getBusinessSubCategory(context).equals("03")) {
                showExpressTicketTemplate(context);
            } else
                showDefaultTemplate(context);

        } else {
            showDefaultTemplate(context);

        }
    }
    private static void setMaxLength(EditText editText, int maxLength) {
        InputFilter filter = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {
                String blockedChars= "|";
                for (int i = start; i < end; i++) {

                    if (source != null && blockedChars.contains(("" + source))) {
                        return "";
                    }
                }
                return null;
            }
        };

        InputFilter[] fArray = new InputFilter[2];
        fArray[0] = new InputFilter.LengthFilter(maxLength);
        fArray[1]= filter;
        editText.setFilters(fArray);
    }
}
