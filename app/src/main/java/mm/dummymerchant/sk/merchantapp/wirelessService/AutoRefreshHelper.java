package mm.dummymerchant.sk.merchantapp.wirelessService;


public class AutoRefreshHelper {
    private static AutoRefreshHelper mInstance;
    private onAutoRefreshInterface mListenerAutoR;
    public interface onAutoRefreshInterface
    {
        void updateShopOnMap(String data);

    }
    public void updateShopOnMap(String data){
            if(mListenerAutoR!=null)
                mListenerAutoR.updateShopOnMap(data);
    }

    public void setAutoR_Listener(onAutoRefreshInterface mListner){
        this.mListenerAutoR=mListner;

    }

    public static AutoRefreshHelper getInstance(){
        if(mInstance==null)
            mInstance=new AutoRefreshHelper();
        return mInstance;
    }

}
