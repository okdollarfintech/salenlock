package mm.dummymerchant.sk.merchantapp.wirelessService;

import android.util.Log;


import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;

import java.io.UnsupportedEncodingException;

import mm.dummymerchant.sk.merchantapp.Utils.Constant;

class CallAutoRefreshAPI {

   private final String Lat, Lang, tempCellID, defaultDistance, mCode, sCode;

    public CallAutoRefreshAPI(String lat, String lang, String tempCellID, String defaultDistance, String mCode, String sCode) {
        Lat = lat;
        Lang = lang;
        this.tempCellID = tempCellID;
        this.defaultDistance = defaultDistance;
        this.mCode = mCode;
        this.sCode = sCode;
    }

    public void updateShopInBackground(){
        RequestParams params = new RequestParams();
        params.put("Lat", Lat);
        params.put("Long", Lang);
        params.put("CellID", tempCellID);
        params.put("NearDistance", defaultDistance);
        params.put("ShopName", "");
        params.put("BusinessType", sCode);
        params.put("BusinessCategory", mCode);
        params.put("PhoneNumber", "");
        params.put("City", "");

        AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
        asyncHttpClient.get(Constant.BASE_APP_URL + Constant.URL_FIND_NEAR_BY_MERCHANT, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                try {
                    String response = new String(responseBody, "UTF-8");
                    AutoRefreshHelper.getInstance().updateShopOnMap(response);
                    Log.d("MAP Response Success", statusCode + " " + response);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.d("MAP Response Failure", error.toString() + statusCode);
            }
        });
    }
}
