package mm.dummymerchant.sk.merchantapp.wirelessService;

import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import mm.dummymerchant.sk.merchantapp.Utils.Constant;

class CallBackgroundAPI {
    private final String lMobile, lsimid, lCellID, shopID;
    private final Double lLatitude, lLongitude;
    private final int lActive;


    public CallBackgroundAPI(String lMobile, String lsimid, String lCellID, Double lLatitude, Double lLongitude, int lActive, String shopID) {
        this.lMobile = lMobile;
        this.lsimid = lsimid;
        this.lCellID = lCellID;
        this.lLatitude = lLatitude;
        this.lLongitude = lLongitude;
        this.lActive = lActive;
        this.shopID=shopID;
    }

    void updateMerchantBusinessArea()
    {
        RequestParams params = new RequestParams();
        params.put("MobileNumber", lMobile);
        params.put("Simid", lsimid);
        params.put("Lat", lLatitude);
        params.put("Long", lLongitude);
        params.put("CellID", lCellID);
        params.put("Active",lActive);
        params.put("ShopId",shopID);
        //Log.d("AutoBoradcast", "Working");
        AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
        asyncHttpClient.get(Constant.BASE_APP_URL + Constant.URL_UPDATE_BUSINESS_AREA, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                try {
                    String response = new String(responseBody, "UTF-8");
                    response = response.replace("<string xmlns=\"http://schemas.microsoft.com/2003/10/Serialization/\">", "").replace("</string>", "");
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject jsonObject1 = jsonObject.getJSONObject("status");
                   // if(jsonObject1.getString("code").equals("200")) //future use
                   //     //AppPreference.setCurrentShopID(CallBackgroundAPI.this, shopID);
                    Log.d("AutoBResponse onSuccess", jsonObject1.getString("code") + response);
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.d("AutoBResponse Failure", error.toString() + statusCode);
            }
        });
    }

}
