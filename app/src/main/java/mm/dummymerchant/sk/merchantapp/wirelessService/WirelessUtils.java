package mm.dummymerchant.sk.merchantapp.wirelessService;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.util.Log;



import java.lang.reflect.Method;

import mm.dummymerchant.sk.merchantapp.R;
import mm.dummymerchant.sk.merchantapp.Utils.AppPreference;

import static mm.dummymerchant.sk.merchantapp.Utils.Utils.getNewNumber;


public class WirelessUtils
{
    private static WirelessUtils mInstance;

    public static WirelessUtils getInstance(){
        if(mInstance==null)
            mInstance=new WirelessUtils();
        return mInstance;
    }

    public boolean setBluetooth(boolean enable, Context context) {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        boolean isEnabled = bluetoothAdapter.isEnabled();

        if (enable && !isEnabled) {
            bluetoothAdapter.enable();
            //return true;xz

        } else if (!enable && isEnabled) {

            Log.d("BLT", "getIsBLTConnected:" + AppPreference.getBLTStatus(context));
            if (AppPreference.getBLTStatus(context)) {
                return false;
            } else {
                bluetoothAdapter.disable();
                return true;
            }

        }

        return true;
    }

    public void changeBluetoothName(Boolean status, final Context context) {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (status) {
            String btlName = bluetoothAdapter.getName();
            Log.d("BLT Name", btlName);
            if (btlName != null && !AppPreference.getMerchantBLTname(context).equals(btlName)) {
                String cate = "" + AppPreference.getBusinessMainCategory(context) + AppPreference.getBusinessSubCategory(context);
                String countryCode = AppPreference.getCountryCode(context);
                String number = AppPreference.getMyMobileNoWithoutCountryCode(context).substring(1);
                String name = AppPreference.getBusinessName(context);
                AppPreference.setOriginalBLTname(context,btlName);
                if (name.equals("Unknown") || name.isEmpty()) {
                    boolean prefixStatus= false;
                    name = AppPreference.getLoginuserName(context);
                    String[] male_prefixArray= context.getResources().getStringArray(R.array.male_prefix);
                    String[] female_prefixArray= context.getResources().getStringArray(R.array.female_prefix);

                    for (String aFemale_prefixArray : female_prefixArray) {
                        if (name.toLowerCase().startsWith(aFemale_prefixArray.toLowerCase())) {
                            name = name.substring(aFemale_prefixArray.length()+1);
                            prefixStatus = true;
                            break;
                        }
                    }
                    if(!prefixStatus)
                        for (String aMale_prefixArray : male_prefixArray) {
                            if (name.toLowerCase().startsWith(aMale_prefixArray.toLowerCase())) {
                                name = name.substring(aMale_prefixArray.length()+1);

                                break;
                            }
                        }
                }
                if (name.length() > 10) {
                    name = name.substring(0, 10);
                }

                String encryptedName = name + "β" + cate + "γ" + getNewNumber(number) + "@" + countryCode;
                bluetoothAdapter.setName(encryptedName);
                if (bluetoothAdapter.getScanMode() != BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
                    // device is not discoverable & connectable
                    Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
                    discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 0);
                    discoverableIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(discoverableIntent);
                }
                AppPreference.setMerchantBLTname(context, encryptedName);
            }
        } else {
            String blTname = AppPreference.getOriginalBLTname(context);
            if (blTname != null && !blTname.contains("β")) {
                String btName = bluetoothAdapter.getName();
                if (btName != null) {
                    if (btName.contains("β")) {
                        bluetoothAdapter.setName(blTname);
                    }
                } else {
                    bluetoothAdapter.setName(getDeviceName());
                }
            } else {
                bluetoothAdapter.setName(getDeviceName());
            }
        }
    }

    public void setHotSpotON(Context context) {
        WifiManager wifimanager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        try {
            // if WiFi is on, turn it off
            if (isApOn(context)) {
                // wifimanager.setWifiEnabled(false);
            }
            WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            Method getConfigMethod = wifiManager.getClass().getMethod("getWifiApConfiguration");
            WifiConfiguration wc = (WifiConfiguration) getConfigMethod.invoke(wifiManager);
            String wifiName = wc.SSID;
            if (wifiName != null && !AppPreference.getHotspotName(context).equals(wifiName)) {
                String cate = "" + AppPreference.getBusinessMainCategory(context) + AppPreference.getBusinessSubCategory(context);
                String countryCode = AppPreference.getCountryCode(context);
                String number = AppPreference.getMyMobileNoWithoutCountryCode(context).substring(1);
                String name = AppPreference.getBusinessName(context);
                AppPreference.setOriginalWifiName(context,wifiName);
                if (name.equals("Unknown") || name.isEmpty()) {
                    boolean prefixStatus = false;
                    name = AppPreference.getLoginuserName(context);
                    String[] male_prefixArray = context.getResources().getStringArray(R.array.male_prefix);
                    String[] female_prefixArray = context.getResources().getStringArray(R.array.female_prefix);
                    for (String aFemale_prefixArray : female_prefixArray) {
                        if (name.toLowerCase().startsWith(aFemale_prefixArray.toLowerCase())) {
                            name = name.substring(aFemale_prefixArray.length() + 1);
                            prefixStatus = true;
                            break;
                        }
                    }
                    if (!prefixStatus)
                        for (String aMale_prefixArray : male_prefixArray) {
                            if (name.toLowerCase().startsWith(aMale_prefixArray.toLowerCase())) {
                                name = name.substring(aMale_prefixArray.length() + 1);
                                break;
                            }
                        }
                }
                if (name.length() > 10) {
                    name = name.substring(0, 10);
                }
                String encryptedName = name + "β" + cate + "γ" + getNewNumber(number) + "@" + countryCode;
                AppPreference.setHotspotName(context, encryptedName);
                wc.SSID = encryptedName;
                wc.hiddenSSID = false;

                Method setConfigMethod = wifiManager.getClass().getMethod("setWifiApConfiguration", WifiConfiguration.class);
                setConfigMethod.invoke(wifiManager, wc);
                Method method = wifimanager.getClass().getMethod("setWifiApEnabled", WifiConfiguration.class, boolean.class);
                method.invoke(wifimanager, wc, true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void setHotspotOFF(Context context) {
        WifiManager wifimanager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        try {
            // if WiFi is on, turn it off
            if (isApOn(context)) {
                //wifimanager.setWifiEnabled(false);
            }
            WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            Method getConfigMethod = wifiManager.getClass().getMethod("getWifiApConfiguration");
            WifiConfiguration wc = (WifiConfiguration) getConfigMethod.invoke(wifiManager);
            if (wc.SSID.contains("β")) {
                if (AppPreference.getOriginalWifiName(context) != null) {
                    if (AppPreference.getOriginalWifiName(context).contains("β"))
                        wc.SSID = getDeviceName();
                    else
                        wc.SSID = AppPreference.getOriginalWifiName(context);
                } else {
                    wc.SSID = getDeviceName();
                }
                wc.hiddenSSID = false;

                Method setConfigMethod = wifiManager.getClass().getMethod("setWifiApConfiguration", WifiConfiguration.class);
                setConfigMethod.invoke(wifiManager, wc);
                Method method = wifimanager.getClass().getMethod("setWifiApEnabled", WifiConfiguration.class, boolean.class);
                method.invoke(wifimanager, wc, false);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }

    private static String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    private static boolean isApOn(Context context) {
        WifiManager wifimanager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        try {
            Method method = wifimanager.getClass().getDeclaredMethod("isWifiApEnabled");
            method.setAccessible(true);
            return (Boolean) method.invoke(wifimanager);
        } catch (Throwable ignored) {
        }
        return false;
    }
}
