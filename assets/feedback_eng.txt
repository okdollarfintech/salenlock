﻿{
    "status": {
        "code": "200",
        "msg": "success"
    },
    "data": [
        {
            "category_name": "00_Merchants (Retail)",
            "category_question": [
                {
                    "0": "Did the employees are friendly & take good care of the guests?",
                    "1": "How do you rate the cleanliness?",
                    "2": "How satisfied or dissatisfied are you with the quality of the merchandise?",
                    "3": "How satisfied or dissatisfied are you with the price of the items you purchased?",
                    "4": "Compare to similar stores, how broad is the selection available at this store?",
                    "5": "How fair is this store’s return policy?",
                    "6": "How do you rate our service?",
                    "7": "How easy to find parking at this store?"
                }
            ]
        },
        {
            "category_name": "01_Food & Drink",
            "category_question": [
                {
                    "0": "Did the staffs are friendly & take good care of the guests?",
                    "1": "How satisfied are you with the selection of meals available?",
                    "2": "How do you rate the cleanliness?",
                    "3": "How would you rate the food in terms of portion size?",
                    "4": "How satisfied are you with the quality of the food?",
                    "5": "Do you consider restaurant to be value for money?",
                    "6": "Do you like the interior design and decoration?",
                    "7": "How easy to find parking at this restaurant?"
                }
            ]
        },
        {
            "category_name": "02_Restaurants",
            "category_question": [
                {
                    "0": "Did the staffs are friendly & take good care of the guests?",
                    "1": "How satisfied are you with the selection of meals available?",
                    "2": "How do you rate the cleanliness?",
                    "3": "How would you rate the food in terms of portion size?",
                    "4": "How satisfied are you with the quality of the food?",
                    "5": "Do you consider restaurant to be value for money?",
                    "6": "Do you like the interior design and decoration?",
                    "7": "How easy to find parking at this restaurant?"
                }
            ]
        },
        {
            "category_name": "03_Mobile, Computers & Electronics",
            "category_question": [
                {
                    "0": "Did the staffs are friendly & take good care of the guests?",
                    "1": "How well do our products meet your needs?",
                    "2": "How satisfied are you with the timeliness of order delivery?",
                    "3": "How satisfied are you with the service provided by the technical support representative?",
                    "4": "How would you rate your overall experience with our service?",
                    "5": "How satisfied are you with the comprehensiveness of our offer?",
                    "6": "How would you rate our prices?",
                    "7": "How likely are you to purchase any of our products again?"
                }
            ]
        },
        {
            "category_name": "04_Travel & Transport",
            "category_question": [
                {
                    "0": "How was your experience of travelling with us?",
                    "1": "How easy was getting information on tickets & Schemes?",
                    "2": "Was driver or ticket collector helpful and generous?",
                    "3": "How was cleanliness maintained in the vehicle?",
                    "4": "Did the vehicle run on schedule",
                    "5": "Was passenger drop-off near to destination or on destination?",
                    "6": "How was the interior and cooling level inside the vehicle?",
                    "7": "Did you feel safe while travelling with us?",
                    "8": "Are you happy and would consider to travel again with us?"
                }
            ]
        },
        {
            "category_name": "05_Gifts & flowers",
            "category_question": [
                {
                    "0": "Did the staffs are friendly & take good care of the guests?",
                    "1": "Do you easily to find the flowers what you want?",
                    "2": "How satisfied or dissatisfied are you with the quality of the merchandise?",
                    "3": "How satisfied or dissatisfied are you with decoration of gift?",
                    "4": "Are Fruit and juice fresh from gift basket?",
                    "5": "How do you rate our service?",
                    "6": "How satisfied or dissatisfied are you shopping with us?",
                    "7": "How easy to find parking at this store?"
                }
            ]
        },
        {
            "category_name": "06_Entertainment & Media",
            "category_question": [
                {
                    "0": "Did the staffs are friendly & take good care of the guests?",
                    "1": "How do you rate the cleanliness?",
                    "2": "Can you get the popular and variety of game here?",
                    "3": "Are they show good and popular moives there?",
                    "4": "How satisfied or dissatisfied are you with the quality of the products?",
                    "5": "How fair are the price of service?",
                    "6": "How rate the training scale of our trainer?",
                    "7": "How easy to find parking here?"
                }
            ]
        },
        {
            "category_name": "07_Education and Learning",
            "category_question": [
                {
                    "0": "Did the staffs are friendly & take good care of the guests?",
                    "1": "How was the venue for the event comfortable and had sufficient space for the course activities?",
                    "2": "How well did this training session meet your research needs?",
                    "3": "Did you gain new knowledge and skills that will improve your job performance?",
                    "4": "How relevant were the search examples used during the session?",
                    "5": "How was cleanliness maintained in the training centre?",
                    "6": "Was the exam clear and covered knowledge and skills taught during the course?",
                    "7": "Everything considered, on the basis of your overall experience, how do you rate the training class?"
                }
            ]
        },
        {
            "category_name": "08_Medical & Health",
            "category_question": [
                {
                    "0": "Did All staff are Friendly and helpful to you and answer your questions?",
                    "1": "How knowledgeable was the staff here?",
                    "2": "How professionally did the staff here behave?",
                    "3": "How responsive was the staff here to your question?",
                    "4": "How clean were the areas in which you were treated here?",
                    "5": "How easy was it to talk to the staff here about your medicine condition(s)?",
                    "6": "Overall, How hygienic do you feel here?",
                    "7": "How fair cost of services, explanation of charges and collection of payment/money?"
                }
            ]
        },
        {
            "category_name": "09_Hair &  Beauty",
            "category_question": [
                {
                    "0": "Please rate the friendliness of the staff here?",
                    "1": "How would you describe the advice and style suggestions they gave?",
                    "2": "how would you rate the cleanliness of the salon?",
                    "3": "Do you like the interior design and decoration?",
                    "4": "Please rate the stylist / beautician on how well you thought they listened to your requirements?",
                    "5": "How was the quality of the style/ colour/ treatment you received?",
                    "6": "Please rate the ambience of this place?",
                    "7": "How easy to find parking at this place?"
                }
            ]
        },
        {
            "category_name": "10_Media & Communication",
            "category_question": [
                {
                    "0": "Did the staffs are friendly & take good care of the guests?",
                    "1": "Did our communication service team answer your question properly?",
                    "2": "Do you get TV broadcasting full channel?",
                    "3": "How do you think it has the accuracy of the postal industry?",
                    "4": "Please rate our service?",
                    "5": "How satisfied or dissatisfied are you with our team?",
                    "6": "Did they fulfil your needs?",
                    "7": "How likely are you contact us again?"
                }
            ]
        },
        {
            "category_name": "11_Professional Services",
            "category_question": [
                {
                    "0": "Did the staffs are friendly & take good care of the guests?",
                    "1": "How do you rate the cleanliness?",
                    "2": "Do you like our fashion design?",
                    "3": "Do they give your design order in time?",
                    "4": "How do you rate the service team?",
                    "5": "Do you think our team really have professionally knowledge?",
                    "6": "How do you rate our technical service?",
                    "7": "Do you think there are many business opportunities?"
                }
            ]
        },
        {
            "category_name": "12_Automotive",
            "category_question": [
                {
                    "0": "Did Our Sales staff work as a team to best satisfy you?",
                    "1": "Did Our Sales staff Make you confident that this was the right vehicle?",
                    "2": "Based on your overall experience, how likely would you be to purchase or lease another vehicle from us?",
                    "3": "Overall, how comfortable were you with the way the we worked with you to arrange the final price, and financing for your new vehicle?",
                    "4": "Do you understand the staff explain the vehicle's maintenance schedule to you?",
                    "5": "Please rate your satisfaction with our service department?",
                    "6": "Would you recommend our dealership to a friend or relative as a place to have their cars serviced?",
                    "7": "How satisfied were you with the process used to determine the trade-in value of your previous vehicle?"
                }
            ]
        },
        {
            "category_name": "13_Animal Sales & Service",
            "category_question": [
                {
                    "0": "Did All staff are Friendly and helpful to you?",
                    "1": "Do you think this place is safe and clean for your pet?",
                    "2": "Were you satisfied with the client service we provided you?  ",
                    "3": "How would you rate our prices?",
                    "4": "How satisfied are you with the customer support?",
                    "5": "How satisfied are you with the comprehensiveness of our offer?",
                    "6": "How would you rate your overall experience with our service?",
                    "7": "Would you recommend our product / service to other people?"
                }
            ]
        },
        {
            "category_name": "14_Manufacturing and Agricultures",
            "category_question": [
                {
                    "0": "Were you satisfied with the client service we provided you?",
                    "1": "How flexibility in product pricing?",
                    "2": "How satisfied or dissatisfied are you with the quality of the products?",
                    "3": "How well do our products meet your needs?",
                    "4": "How satisfied are you with \n Knowledge of products, Promptness in returning your call, Courtesy and professionalism, Visits your facility?",
                    "5": "How was Shipment correct as ordered?",
                    "6": "How fair is the products' return policy?",
                    "7": "How likely are you to purchase any of our products again?"
                }
            ]
        },
        {
            "category_name": "15_Construction&Contractors",
            "category_question": [
                {
                    "0": "How Quality of Workmanship on Site & Attitude of Site Staff?",
                    "1": "How Quality of Communications / Documentation?",
                    "2": "Response to Resolution of Defects?",
                    "3": "Contract Completion Agreement & Documentation?",
                    "4": " How do you think our good quality of the materials used?",
                    "5": "Please rate the provided value for money?",
                    "6": "Please rate our team service?",
                    "7": "Approach to Health, Safety & Environment?"
                }
            ]
        },
        {
            "category_name": "16_Home&Garden",
            "category_question": [
                {
                    "0": "Were you satisfied with the client service we provided you?",
                    "1": "How Quality of Communications / Documentation?",
                    "2": "Approach to Health, Safety & Environment?",
                    "3": "Please rate our service?",
                    "4": "Did our team give service for your custom work?",
                    "5": "Response to Resolution of Defects?",
                    "6": "Contract Completion Agreement & Documentation?",
                    "7": "Please rate the provided value for money?"
                }
            ]
        },
        {
            "category_name": "17_Domestic Services",
            "category_question": [
                {
                    "0": "Were you satisfied with the client service we provided you? ",
                    "1": "Can Our group process on time?",
                    "2": "How satisfied or dissatisfied are you with our service?",
                    "3": "Can they finish your work in specified time?",
                    "4": "How much can they process to satisfy your needs?",
                    "5": "Do you consider our team to be value for money?",
                    "6": "How do you rate our service?",
                    "7": "Will you contact us again?"
                }
            ]
        },
        {
            "category_name": "18_Sports & Recreation",
            "category_question": [
                {
                    "0": "Did the staffs are friendly & take good care of the guests?",
                    "1": "How do you rate the cleanliness and maintenance?",
                    "2": "How do you think you can get the variety of sporting goods in our shop?",
                    "3": "How safety and relax when you are here?",
                    "4": "Can all trainers teach you effectively?",
                    "5": "Is the water clean in the swimming pool?",
                    "6": "Were you satisfied with the client service we provided you? ",
                    "7": "How easy to find parking here?"
                }
            ]
        },
        {
            "category_name": "19_RealEstate",
            "category_question": [
                {
                    "0": "How well do you think we work at keeping you a satisfied customer?",
                    "1": "How well do we deliver what we promise?",
                    "2": "Our overall effectiveness and quality of service?",
                    "3": "Promptness in returning your calls & responding to your request for service?",
                    "4": "How likely are you to purchase any of our products again?",
                    "5": "How much confidence do you have in our service?",
                    "6": "Overall, how willing would you be willing to recommend us?",
                    "7": "Overall, how willing would you be to buy through us again"
                }
            ]
        },
        {
            "category_name": "20_Accommodation",
            "category_question": [
                {
                    "0": "Do you like the interior design and decoration?",
                    "1": "How do you rate the cleanliness?",
                    "2": "Did the staffs are friendly & take good care of the guests?",
                    "3": "How helpful was the concierge throughout your stay?",
                    "4": "Overall, how well-equipped was your room?",
                    "5": "How comfortable were your bed linens?",
                    "6": "How convenient were the hours of the food service options here?",
                    "7": "How easy to find parking here?"
                }
            ]
        },
        {
            "category_name": "21_Event Organization",
            "category_question": [
                {
                    "0": "How satisfied are you with the Help Staff of the event?",
                    "1": "How do you rate the cleanliness of halls?",
                    "2": "Did our service match your need?",
                    "3": "How well do our team Real-time and process?",
                    "4": "How do you think our products' quality is good?",
                    "5": "How do you rate our service?",
                    "6": "Is the cost fair or not?",
                    "7": "Will you say to your friends and relatives about our services?"
                }
            ]
        },
        {
            "category_name": "22_Government",
            "category_question": [
                {
                    "0": "Did the staffs are friendly & take good care of the guests?",
                    "1": "How do you rate the cleanliness?",
                    "2": "How do you think you can get so much variety of book in library?",
                    "3": "Would the court be conducted fairly?",
                    "4": "Is there a convenient visit?",
                    "5": "Do they understand your request and do a desired time.?",
                    "6": "Do you want to give any suggestions?",
                    "7": "How easy to find parking at this store?"
                }
            ]
        },
        {
            "category_name": "23_Financial Services",
            "category_question": [
                {
                    "0": "Is the teller friendly and courteous?",
                    "1": "How satisfied are you with our Professionalism/courteousness?",
                    "2": "Is our bank facility clean and orderly?",
                    "3": "Does the teller / bank officer have a good knowledge of our bank products and services?",
                    "4": "Did the teller / bank officer offer additional products?",
                    "5": "Is your business handled efficiently and in a timely manner?",
                    "6": "How satisfied are you with our understanding of request & response time?",
                    "7": "How likely are you to continue using our business in the future?"
                }
            ]
        },
        {
            "category_name": "24_Religion",
            "category_question": [
                {
                    "0": "How do you rate the cleanliness at religious place?",
                    "1": "Do you feel peace here?",
                    "2": "Can you get all materials here?",
                    "3": "Are you ok with food?",
                    "4": "Do monks preaching good?",
                    "5": " Were nuns from Church treated well?",
                    "6": "Whether are they charitable sentiments?",
                    "7": "Is Religious activity effective?"
                }
            ]
        },
        {
            "category_name": "25_Miscellaneous",
            "category_question": [
                {
                    "0": "Did the staffs are friendly & take good care of the guests?",
                    "1": "Is Our organization able to work on time when you need emergency?",
                    "2": "Did Our organization help people effectively?",
                    "3": "Whether are they charitable sentiments?",
                    "4": "Do you easily contact with our organization whenever you needed?",
                    "5": "Whether did they need to help your entire issues?",
                    "6": "How do you rate our service?",
                    "7": "Is there a cost justification?"
                }
            ]
        },
        {
            "category_name": "26_Utilities",
            "category_question": [
                {
                    "0": "Did the staffs are friendly & take good care of the guests?",
                    "1": "How do you feel our product is safe or not?",
                    "2": "How broad is the selection available at this store?",
                    "3": "How well do our products meet your needs?",
                    "4": "Do any affected environment abandoned materials present?",
                    "5": "Do affected people abandoned materials present?",
                    "6": "Did the air pollution problem occur?",
                    "7": "How do you rate our product and service?"
                }
            ]
        },
        {
            "category_name": "27_Adult",
            "category_question": [
                {
                    "0": "Did the staffs are friendly & take good care of the guests?",
                    "1": "Did you easily to find the variety of good for all ages in our shop?",
                    "2": "Did the staffs are friendly & take good care of the guests?",
                    "3": "Did our staff answer the question whenever you want to know?",
                    "4": "How satisfied or dissatisfied are you with the Dating Agency Service?",
                    "5": "How do you rate our service?",
                    "6": "Is there a cost justification?",
                    "7": "The next time do you wish to come again?"
                }
            ]
        }
    ]
}