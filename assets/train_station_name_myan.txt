{
    "status": {
        "code": "200",
        "msg": "Success"
    },
    "data": {
        "Table": [
            {
                "ID": "80872877-e9dc-42ef-a7f1-4e05d6ffb258",
                "StationName": "ရန္ကုန္ဘူတာႀကီး ",
                "cellTower": []
            },
            {
                "ID": "8db5d70c-f679-448f-861f-07f9e564b4a9",
                "StationName": "ဘုရားလမ္း",
                "cellTower": [
                    {
                        "cellId": "39480630"
                    },
                    {
                        "cellId": "39479381"
                    },
                    {
                        "cellId": "39480627"
                    }
                ]
            },
            {
                "ID": "f3a2ec3e-c1c8-4b93-8f22-e61f3dfe539f",
                "StationName": "လမ္းမေတာ္",
                "cellTower": [
                    {
                        "cellId": "39480631"
                    },
                    {
                        "cellId": "39480628"
                    }
                ]
            },
            {
                "ID": "40d42a04-20a3-48ce-9554-59d1f0cfcbda",
                "StationName": "ျပည္လမ္း",
                "cellTower": [
                    {
                        "cellId": "39479739"
                    },
                    {
                        "cellId": "39482779"
                    }
                ]
            },
            {
                "ID": "756ede0a-f3cc-42f9-b12b-de164c47b832",
                "StationName": "ရွမ္းလမ္း",
                "cellTower": [
                    {
                        "cellId": "39479738"
                    }
                ]
            },
            {
                "ID": "8b699bfe-3796-4eac-8418-31e2464dbeca",
                "StationName": "အလံုလမ္း",
                "cellTower": [
                    {
                        "cellId": "39480000"
                    }
                ]
            },
            {
                "ID": "acddb9ca-3abf-4089-9b9d-c6d2ebb0daf1",
                "StationName": "ပန္းလႈိင္လမ္း",
                "cellTower": [
                    {
                        "cellId": "39479999"
                    }
                ]
            },
            {
                "ID": "93a38aa0-a0fb-4652-80ea-945d60cd4564",
                "StationName": "ၾကည့္ျမင့္တိုင္",
                "cellTower": [
                    {
                        "cellId": "39480928"
                    }
                ]
            },
            {
                "ID": "ff7e8cea-f026-4ce0-9d07-30e37396507c",
                "StationName": "ဟံသာ၀တီ",
                "cellTower": [
                    {
                        "cellId": "39480926"
                    }
                ]
            },
            {
                "ID": "afcae104-a68d-40c3-8e29-7886858e9c35",
                "StationName": "လွည္းတမ္း",
                "cellTower": [
                    {
                        "cellId": "39479286"
                    }
                ]
            },
            {
                "ID": "280a7f93-cbce-42ff-b873-82a5358960ad",
                "StationName": "ကမာရြတ္",
                "cellTower": [
                    {
                        "cellId": "39675767"
                    },
                    {
                        "cellId": "39780251"
                    },
                    {
                        "cellId": "39480248"
                    },
                    {
                        "cellId": "39479819"
                    },
                    {
                        "cellId": "39479816"
                    },
                    {
                        "cellId": "39479107"
                    },
                    {
                        "cellId": "39482209"
                    },
                    {
                        "cellId": "39481951"
                    },
                    {
                        "cellId": "39482551"
                    },
                    {
                        "cellId": "39480446"
                    },
                    {
                        "cellId": "39479779"
                    },
                    {
                        "cellId": "39675705"
                    },
                    {
                        "cellId": "39479287"
                    },
                    {
                        "cellId": "39479289"
                    },
                    {
                        "cellId": "39676715"
                    },
                    {
                        "cellId": "39677178"
                    },
                    {
                        "cellId": "39677175"
                    },
                    {
                        "cellId": "39480911"
                    },
                    {
                        "cellId": "39482209"
                    },
                    {
                        "cellId": "39479820"
                    },
                    {
                        "cellId": "39479787"
                    }
                ]
            },
            {
                "ID": "35618be0-ed23-413c-a1ca-0109b822ebef",
                "StationName": "သီရိၿမိဳင္",
                "cellTower": [
                    {
                        "cellId": "39675767"
                    }
                ]
            },
            {
                "ID": "d9f10103-964e-445f-a3cb-e88315db50bb",
                "StationName": "အုတ္က်င္း",
                "cellTower": [
                    {
                        "cellId": "39681019"
                    }
                ]
            },
            {
                "ID": "9e7de53b-7094-4ef4-9804-230d51c7d939",
                "StationName": "သမိုင္း",
                "cellTower": [
                    {
                        "cellId": "39677144"
                    }
                ]
            },
            {
                "ID": "f604dfaf-e3e9-483b-982a-9be7ada01586",
                "StationName": "သမိုင္း ၿမိဳ႕သစ္",
                "cellTower": [
                    {
                        "cellId": "39481647"
                    }
                ]
            },
            {
                "ID": "5190a97c-c899-4d8c-a922-b5136dc7b4b2",
                "StationName": "ႀကိဳ႕ကုန္း",
                "cellTower": [
                    {
                        "cellId": "39481649"
                    },
                    {
                        "cellId": "39678035"
                    }
                ]
            },
            {
                "ID": "a4a82dd0-7b50-4ec4-99ed-0bdeb753d158",
                "StationName": "အင္းစိန္",
                "cellTower": [
                    {
                        "cellId": "39681659"
                    }
                ]
            },
            {
                "ID": "cfae211d-d516-4fc3-a6a4-686e9f9b5220",
                "StationName": "ရြာမ",
                "cellTower": []
            },
            {
                "ID": "e3d7c9f5-11f9-44f1-b66e-8b1fe4c03cc4",
                "StationName": "ေဖာ့ကန္",
                "cellTower": []
            },
            {
                "ID": "db48b61d-5d8d-4cc1-a0c1-c502424bd7d9",
                "StationName": "ေအာင္ဆန္း",
                "cellTower": []
            },
            {
                "ID": "7b4b8a39-5187-472a-ab05-2f3aa4efe103",
                "StationName": "ဒညင္းကုန္း",
                "cellTower": []
            },
            {
                "ID": "c5d879eb-e410-4b6c-8ecb-3b13fc9696d3",
                "StationName": "ေဂါက္ကြင္း",
                "cellTower": [
                    {
                        "cellId": "39693734"
                    },
                    {
                        "cellId": "39675813"
                    }
                ]
            },
            {
                "ID": "e6b8c44e-4d5a-48ff-bfb7-1a2a1ec94630",
                "StationName": "က်ိဳကၠလဲ့",
                "cellTower": []
            },
            {
                "ID": "5f2f9b8d-c1a2-4f0b-8143-2101a6cc5111",
                "StationName": "မဂၤလာဒံုေစ်း",
                "cellTower": [
                    {
                        "cellId": "39693732"
                    },
                    {
                        "cellId": "3967709"
                    }
                ]
            },
            {
                "ID": "11d59476-b2fc-4cfd-8ccd-a03d0e3ba9b5",
                "StationName": "မဂၤလာဒံု",
                "cellTower": [
                    {
                        "cellId": "39481313"
                    },
                    {
                        "cellId": "39679412"
                    },
                    {
                        "cellId": "39677209"
                    }
                ]
            },
            {
                "ID": "fa02a347-0f82-443f-bd87-3be3f5ad4018",
                "StationName": "ေ၀ဘာဂီ",
                "cellTower": [
                    {
                        "cellId": "39678291"
                    },
                    {
                        "cellId": "39681662"
                    }
                ]
            },
            {
                "ID": "f041e838-e5d1-4073-9679-13f37c137bdd",
                "StationName": "ဥကၠလာပ",
                "cellTower": [
                    {
                        "cellId": "39681591"
                    },
                    {
                        "cellId": "39381597"
                    },
                    {
                        "cellId": "39681594"
                    }
                ]
            },
            {
                "ID": "486bd3af-35e2-4801-9c7d-66cb773dfefb",
                "StationName": "Pywatsatekon",
                "cellTower": []
            },
            {
                "ID": "e366b2c9-0ec1-45c1-82fe-b74bba7a8216",
                "StationName": "ေက်ာက္ေရတြင္း",
                "cellTower": [
                    {
                        "cellId": "39675922"
                    },
                    {
                        "cellId": "39678293"
                    }
                ]
            },
            {
                "ID": "0ef3a042-e095-4bc3-ae85-e1cc117de923",
                "StationName": "တံတားကေလး",
                "cellTower": [
                    {
                        "cellId": "39675344"
                    }
                ]
            },
            {
                "ID": "09b17008-2791-45c3-8bbe-458422284dae",
                "StationName": "ေရေက်ာ္",
                "cellTower": [
                    {
                        "cellId": "39674512"
                    }
                ]
            },
            {
                "ID": "3ee93c45-b78c-49e4-94a8-bfe970ca3128",
                "StationName": "ပါရမီ",
                "cellTower": []
            },
            {
                "ID": "7643c471-6358-4bff-98ab-641b01d90523",
                "StationName": "ကံဘဲ့",
                "cellTower": []
            },
            {
                "ID": "f179f5ac-7bb4-410c-9eb4-a75a0327a44c",
                "StationName": "ေဘာက္ေထာ္",
                "cellTower": []
            },
            {
                "ID": "53e4410e-30e9-4df2-91ef-bc8839cb0ec0",
                "StationName": "တာေမြ",
                "cellTower": []
            },
            {
                "ID": "99afac17-6e1e-4049-9da3-fd1a2aec1392",
                "StationName": "ေမတၱာညြန္႔",
                "cellTower": []
            },
            {
                "ID": "01bf2f97-0b52-4286-a9a1-4b5686b8954b",
                "StationName": "မလႊကုန္း",
                "cellTower": [
                    {
                        "cellId": "39481820"
                    }
                ]
            },
            {
                "ID": "5263f43f-9dc4-4607-bb62-3b3ce41eb51e",
                "StationName": "ပုစြန္ေတာင္",
                "cellTower": [
                    {
                        "cellId": "39479861"
                    },
                    {
                        "cellId": "39479858"
                    },
                    {
                        "cellId": "39479857"
                    },
                    {
                        "cellId": "39479860"
                    },
                    {
                        "cellId": "39476001"
                    },
                    {
                        "cellId": "39479863"
                    }
                ]
            },
            {
                "ID": "d9df14d0-7838-4d9c-8e8c-9c3c0b226640",
                "StationName": "ႏွင္းဆီကုန္း",
                "cellTower": [
                    {
                        "cellId": "39482730"
                    }
                ]
            },
            {
                "ID": "bb731470-5db2-4de8-bcec-79c90793f544",
                "StationName": "သကၤန္းကၽြန္း",
                "cellTower": [
                    {
                        "cellId": "39482730"
                    }
                ]
            },
            {
                "ID": "1c4a1e93-f961-4931-88da-af2643b960a5",
                "StationName": "ငမိုးရိပ္",
                "cellTower": [
                    {
                        "cellId": "39478181"
                    }
                ]
            },
            {
                "ID": "18f7ee11-5b5d-4431-bfbb-ba8f092b6371",
                "StationName": "တိုးေၾကာင္ကေလး",
                "cellTower": [
                    {
                        "cellId": "39478181"
                    }
                ]
            },
            {
                "ID": "53eec6a4-30c7-4a77-b2e0-626f081e9e35",
                "StationName": "ဒဂံုတကၠသိုလ္",
                "cellTower": [
                    {
                        "cellId": "39674877"
                    },
                    {
                        "cellId": "3964874"
                    },
                    {
                        "cellId": "39677597"
                    }
                ]
            },
            {
                "ID": "6180da68-77df-4223-9471-7eca97b6a9a3",
                "StationName": "ရြာသာႀကီး",
                "cellTower": [
                    {
                        "cellId": "39675478"
                    },
                    {
                        "cellId": "9482071"
                    }
                ]
            },
            {
                "ID": "dddc789f-1ac9-4ce3-97cb-4843d2bf2e26",
                "StationName": "စက္မႈဇံု (၁)",
                "cellTower": [
                    {
                        "cellId": "39484811"
                    },
                    {
                        "cellId": "39481408"
                    }
                ]
            },
            {
                "ID": "a26cf355-6295-4cd1-8cd3-a877bc4a195c",
                "StationName": "ဂ်မား",
                "cellTower": [
                    {
                        "cellId": "39478470"
                    },
                    {
                        "cellId": "39477888"
                    },
                    {
                        "cellId": "39478469"
                    },
                    {
                        "cellId": "39478729"
                    }
                ]
            },
            {
                "ID": "271084e2-1b66-4446-980f-3f05ab45f009",
                "StationName": "ေအာင္သုခ",
                "cellTower": [
                    {
                        "cellId": "39677749"
                    },
                    {
                        "cellId": "39611150"
                    }
                ]
            },
            {
                "ID": "1f0f95e1-ba14-4561-95b3-903717ce16d3",
                "StationName": "အုတ္ဖိုစု",
                "cellTower": [
                    {
                        "cellId": "39610059"
                    },
                    {
                        "cellId": "39612078"
                    }
                ]
            },
            {
                "ID": "ca815e49-4bbc-416b-964a-3ed651c1fdcc",
                "StationName": "ရန္ကုန္ အေရွ႕ပိုင္း တကၠသိုလ္",
                "cellTower": [
                    {
                        "cellId": "39618338"
                    }
                ]
            },
            {
                "ID": "3cdebce4-d2de-461a-8d4a-cd2db1a50923",
                "StationName": "သီလ၀ါ",
                "cellTower": [
                    {
                        "cellId": "39626639"
                    },
                    {
                        "cellId": "39611270"
                    }
                ]
            },
            {
                "ID": "470b7a1f-7a5c-42d6-a492-dc7cd13c21ec",
                "StationName": "စက္မႈဇံု (၂)",
                "cellTower": []
            },
            {
                "ID": "4347b838-28a4-4776-8f3b-0b7e74372610",
                "StationName": "ေရႊျပည္သာ",
                "cellTower": []
            },
            {
                "ID": "5d30e1bd-ce1e-4c46-8d62-44cca8a7dd0b",
                "StationName": "သာဓုကန္",
                "cellTower": []
            },
            {
                "ID": "efc3f3c3-96c7-49a1-bc21-40bea0a0d1b9",
                "StationName": "ေလွာ္ကား",
                "cellTower": []
            },
            {
                "ID": "27fb4c5d-77f4-47eb-9ce7-13b062e0bcb7",
                "StationName": "ကြန္ပ်ဳတာ တကၠသိုလ္",
                "cellTower": []
            }
        ]
    }
    }